import cv2, time, pandas
from datetime import datetime

pierwsza_klatka = None
status_list = [None, None]
czasy = []
df = pandas.DataFrame(columns=["Poczatek", "Koniec"])

video =cv2.VideoCapture(0)

while True:
    sprawdz, klatka = video.read()
    status = 0
    szara = cv2.cvtColor(klatka, cv2.COLOR_BGR2GRAY)
    szara = cv2.GaussianBlur(szara, (21,21),0) # rozmycie klatki,
    # dla dokładniejszej możliwości obliczenia

    if pierwsza_klatka is None:
        pierwsza_klatka = szara
        continue # żeby warunek był sprawdzany tylko raz
        # ponieważ za drugim razem pierwsza_klatka nie ma wartości None

    klatka_delta = cv2.absdiff(pierwsza_klatka, szara) # sprawdzanie różnicy
    klatka_progowa = cv2.threshold(klatka_delta, 30, 255, cv2.THRESH_BINARY)[1]
    klatka_progowa = cv2.dilate(klatka_progowa, None, iterations=2)

    (_,cnts,_) = cv2.findContours(klatka_progowa.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    for kontur in cnts:
        if cv2.contourArea(kontur) < 10000:
            continue
        status = 1

        # tworzymy prostokąt wokół konturu o kolorzez zielonym i grubości 3 px
        (x, y, w, h) = cv2.boundingRect(kontur)
        cv2.rectangle(klatka, (x, y), (x+w, y+h), (0,255,0), 3)

    status_list.append(status)


    if status_list[-1] == 1 and status_list[-2] == 0:
        czasy.append(datetime.now())
    if status_list[-1] == 0 and status_list[-2] == 1:
        czasy.append(datetime.now())


    cv2.imshow("Szara klatka", szara)
    cv2.imshow("Klatka Delta",klatka_delta)
    cv2.imshow("Klatka Progowa", klatka_progowa)
    cv2.imshow("Kolorowa klatka", klatka)

    klawisz = cv2.waitKey(1)

    if klawisz == ord('q'):
        if status == 1:
            czasy.append(datetime.now())
        break

print(czasy)

for i in range(0,len(czasy),2):
    df=df.append({"Poczatek":czasy[i], "Koniec":czasy[i+1]}, ignore_index=True)

df.to_csv("Czasy.csv")
video.release()
cv2.destroyAllWindows
