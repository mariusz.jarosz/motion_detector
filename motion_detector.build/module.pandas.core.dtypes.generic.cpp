/* Generated code for Python source for module 'pandas.core.dtypes.generic'
 * created by Nuitka version 0.5.32.7
 *
 * This code is in part copyright 2018 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The _module_pandas$core$dtypes$generic is a Python object pointer of module type. */

/* Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_pandas$core$dtypes$generic;
PyDictObject *moduledict_pandas$core$dtypes$generic;

/* The module constants used, if any. */
static PyObject *const_tuple_a0b189240f62747b9388c5d9de57309a_tuple;
extern PyObject *const_str_plain_ABCSparseSeries;
extern PyObject *const_str_plain_metaclass;
extern PyObject *const_str_plain_interval;
static PyObject *const_tuple_str_plain_periodindex_tuple;
extern PyObject *const_str_plain_index;
extern PyObject *const_str_plain_comp;
static PyObject *const_str_plain_ABCFloat64Index;
extern PyObject *const_tuple_str_plain___class___tuple;
static PyObject *const_tuple_str_plain_int64index_tuple;
static PyObject *const_tuple_ca7e708550972b62b9b04c08421c34a0_tuple;
extern PyObject *const_str_plain___file__;
extern PyObject *const_str_plain_ABCExtensionArray;
extern PyObject *const_str_plain_ABCPeriod;
extern PyObject *const_str_plain_ABCPeriodIndex;
static PyObject *const_tuple_1399a59b28e589531450d810aa4197a4_tuple;
extern PyObject *const_tuple_str_plain_period_tuple;
extern PyObject *const_str_digest_30235a8809db2818daf1f4bd1339d3df;
static PyObject *const_tuple_7fa5e573061407858ec572301a680ace_tuple;
extern PyObject *const_tuple_str_plain_interval_tuple;
static PyObject *const_tuple_8cd4cfe29da562f7e2f4c3865abeabd1_tuple;
static PyObject *const_tuple_e81beef27d536d6d71359d61bfd33781_tuple;
static PyObject *const_str_plain_ABCBase;
static PyObject *const_tuple_ca0e6f32803588f0a9f0cc22aa814733_tuple;
static PyObject *const_str_digest_d5a49727799e8632405b89df92756090;
extern PyObject *const_str_plain_sparse_array;
extern PyObject *const_str_plain_ABCPanel;
extern PyObject *const_str_plain_cls;
extern PyObject *const_str_plain_ABCSparseArray;
extern PyObject *const_str_plain_panel;
static PyObject *const_tuple_str_plain_uint64index_tuple;
static PyObject *const_tuple_str_plain_timedeltaindex_tuple;
static PyObject *const_tuple_1cbbae3c47881c7d89f542e5c3eab677_tuple;
static PyObject *const_str_plain_ABCInterval;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_plain_meta;
static PyObject *const_tuple_55a06a97336d2526a52d514e01776b18_tuple;
static PyObject *const_tuple_str_plain_intervalindex_tuple;
extern PyObject *const_str_plain_ABCGeneric;
extern PyObject *const_str_plain_categoricalindex;
static PyObject *const_tuple_str_plain_cls_str_plain_inst_str_plain_attr_str_plain_comp_tuple;
extern PyObject *const_str_plain_categorical;
extern PyObject *const_str_plain___qualname__;
extern PyObject *const_tuple_str_plain_index_tuple;
static PyObject *const_tuple_6155f4f0a36b0e20cfc83c9cfa52cf96_tuple;
extern PyObject *const_str_plain_ABCRangeIndex;
static PyObject *const_tuple_a9bd744d56f40fb125578446e1149015_tuple;
static PyObject *const_tuple_498ec8f05822c48e89fb1a9ca5511e2c_tuple;
extern PyObject *const_str_plain_datetimeindex;
extern PyObject *const_tuple_str_plain_series_tuple;
extern PyObject *const_tuple_empty;
static PyObject *const_str_plain_ABCIntervalIndex;
static PyObject *const_tuple_d0c24e19633621eed623faa83836dd29_tuple;
static PyObject *const_tuple_str_plain_sparse_array_str_plain_sparse_series_tuple;
extern PyObject *const_str_plain_name;
static PyObject *const_tuple_ee46e31b6dd9a260cbcf686ed7e1d180_tuple;
extern PyObject *const_str_plain_uint64index;
extern PyObject *const_str_plain_attr;
static PyObject *const_tuple_dd895ef83b60eefcd25e7374010c2cae_tuple;
static PyObject *const_tuple_str_plain_panel_tuple;
static PyObject *const_tuple_a07eb65fd3b116acb1435fd5f485e3c0_tuple;
extern PyObject *const_str_plain_ABCIndex;
static PyObject *const_tuple_d542cd81ad9e787128b55a2524f2d1f6_tuple;
static PyObject *const_tuple_str_plain_rangeindex_tuple;
static PyObject *const_str_plain_ABCInt64Index;
static PyObject *const_tuple_str_plain_categoricalindex_tuple;
extern PyObject *const_tuple_type_type_tuple;
extern PyObject *const_str_plain_float64index;
extern PyObject *const_str_plain_ABCTimedeltaIndex;
extern PyObject *const_str_plain_ABCDateOffset;
extern PyObject *const_str_plain_timedeltaindex;
extern PyObject *const_str_plain_ABCCategorical;
extern PyObject *const_str_plain_sparse_time_series;
extern PyObject *const_str_plain_periodindex;
extern PyObject *const_str_plain_dataframe;
extern PyObject *const_str_plain_ABCIndexClass;
extern PyObject *const_str_plain_intervalindex;
static PyObject *const_tuple_str_plain_dateoffset_tuple;
static PyObject *const_tuple_str_plain_sparse_series_str_plain_sparse_time_series_tuple;
static PyObject *const_str_plain_dateoffset;
static PyObject *const_tuple_str_plain_multiindex_tuple;
extern PyObject *const_str_plain_multiindex;
extern PyObject *const_str_plain_ABCDataFrame;
static PyObject *const_str_digest_99629574af3e9a60194722e67c76327f;
extern PyObject *const_str_plain_ABCDatetimeIndex;
static PyObject *const_tuple_cdd89e57eb924c02f17f1cf30aaeceaa_tuple;
extern PyObject *const_str_plain_type;
static PyObject *const_str_plain_create_pandas_abc_type;
static PyObject *const_str_plain__ABCGeneric;
static PyObject *const_tuple_e59941f973d0c3f39d45902b6fb98a97_tuple;
extern PyObject *const_str_plain_ABCCategoricalIndex;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_plain___class__;
extern PyObject *const_str_plain_dct;
extern PyObject *const_str_plain_int64index;
static PyObject *const_str_plain___instancecheck__;
static PyObject *const_tuple_d4acdb449ce2c1fb373e321bbe598787_tuple;
extern PyObject *const_str_plain_extension;
extern PyObject *const_str_plain___module__;
static PyObject *const_str_digest_95b149e5ddb3d0bb85db3124dd3ef793;
extern PyObject *const_str_plain__typ;
static PyObject *const_tuple_bc4cb33cd97b64ecf0d05ff12856b10f_tuple;
static PyObject *const_tuple_str_plain_dataframe_tuple;
static PyObject *const_tuple_088e7186a3be3fdab3964802df549ae3_tuple;
static PyObject *const_tuple_str_plain_datetimeindex_tuple;
static PyObject *const_str_digest_607e52cf80d7e0fd83932301ca6c8266;
static PyObject *const_tuple_1b417d1399297279333d7a632ca8c8a8_tuple;
extern PyObject *const_str_plain_series;
extern PyObject *const_str_plain___prepare__;
static PyObject *const_str_plain_ABCUInt64Index;
extern PyObject *const_str_plain_inst;
static PyObject *const_tuple_3a2ca9a72da6328235c2c7b989760719_tuple;
static PyObject *const_str_plain__check;
static PyObject *const_tuple_str_plain_extension_str_plain_categorical_tuple;
extern PyObject *const_str_plain_rangeindex;
extern PyObject *const_str_plain__subtyp;
extern PyObject *const_str_plain_ABCSparseDataFrame;
static PyObject *const_str_plain___subclasscheck__;
extern PyObject *const_str_plain_ABCMultiIndex;
static PyObject *const_tuple_ecfb4f82c2a82855e2b2efd7e568ab2f_tuple;
static PyObject *const_tuple_str_plain_cls_str_plain_inst_tuple;
static PyObject *const_str_digest_6ad1fe98e4357527e1848039d70479f9;
extern PyObject *const_str_plain_period;
static PyObject *const_tuple_str_plain_float64index_tuple;
extern PyObject *const_str_plain_ABCSeries;
static PyObject *const_tuple_str_plain__ABCGeneric_tuple_type_type_tuple_tuple;
extern PyObject *const_str_plain__data;
extern PyObject *const_str_plain_sparse_series;
extern PyObject *const_tuple_str_plain_sparse_frame_tuple;
extern PyObject *const_str_plain_sparse_frame;
static PyObject *module_filename_obj;

static bool constants_created = false;

static void createModuleConstants( void )
{
    const_tuple_a0b189240f62747b9388c5d9de57309a_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_a0b189240f62747b9388c5d9de57309a_tuple, 0, const_str_plain_ABCRangeIndex ); Py_INCREF( const_str_plain_ABCRangeIndex );
    PyTuple_SET_ITEM( const_tuple_a0b189240f62747b9388c5d9de57309a_tuple, 1, const_str_plain__typ ); Py_INCREF( const_str_plain__typ );
    const_tuple_str_plain_rangeindex_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_rangeindex_tuple, 0, const_str_plain_rangeindex ); Py_INCREF( const_str_plain_rangeindex );
    PyTuple_SET_ITEM( const_tuple_a0b189240f62747b9388c5d9de57309a_tuple, 2, const_tuple_str_plain_rangeindex_tuple ); Py_INCREF( const_tuple_str_plain_rangeindex_tuple );
    const_tuple_str_plain_periodindex_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_periodindex_tuple, 0, const_str_plain_periodindex ); Py_INCREF( const_str_plain_periodindex );
    const_str_plain_ABCFloat64Index = UNSTREAM_STRING( &constant_bin[ 1922148 ], 15, 1 );
    const_tuple_str_plain_int64index_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_int64index_tuple, 0, const_str_plain_int64index ); Py_INCREF( const_str_plain_int64index );
    const_tuple_ca7e708550972b62b9b04c08421c34a0_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_ca7e708550972b62b9b04c08421c34a0_tuple, 0, const_str_plain_ABCMultiIndex ); Py_INCREF( const_str_plain_ABCMultiIndex );
    PyTuple_SET_ITEM( const_tuple_ca7e708550972b62b9b04c08421c34a0_tuple, 1, const_str_plain__typ ); Py_INCREF( const_str_plain__typ );
    const_tuple_str_plain_multiindex_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_multiindex_tuple, 0, const_str_plain_multiindex ); Py_INCREF( const_str_plain_multiindex );
    PyTuple_SET_ITEM( const_tuple_ca7e708550972b62b9b04c08421c34a0_tuple, 2, const_tuple_str_plain_multiindex_tuple ); Py_INCREF( const_tuple_str_plain_multiindex_tuple );
    const_tuple_1399a59b28e589531450d810aa4197a4_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_1399a59b28e589531450d810aa4197a4_tuple, 0, const_str_plain_ABCSparseSeries ); Py_INCREF( const_str_plain_ABCSparseSeries );
    PyTuple_SET_ITEM( const_tuple_1399a59b28e589531450d810aa4197a4_tuple, 1, const_str_plain__subtyp ); Py_INCREF( const_str_plain__subtyp );
    const_tuple_str_plain_sparse_series_str_plain_sparse_time_series_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_sparse_series_str_plain_sparse_time_series_tuple, 0, const_str_plain_sparse_series ); Py_INCREF( const_str_plain_sparse_series );
    PyTuple_SET_ITEM( const_tuple_str_plain_sparse_series_str_plain_sparse_time_series_tuple, 1, const_str_plain_sparse_time_series ); Py_INCREF( const_str_plain_sparse_time_series );
    PyTuple_SET_ITEM( const_tuple_1399a59b28e589531450d810aa4197a4_tuple, 2, const_tuple_str_plain_sparse_series_str_plain_sparse_time_series_tuple ); Py_INCREF( const_tuple_str_plain_sparse_series_str_plain_sparse_time_series_tuple );
    const_tuple_7fa5e573061407858ec572301a680ace_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_7fa5e573061407858ec572301a680ace_tuple, 0, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    PyTuple_SET_ITEM( const_tuple_7fa5e573061407858ec572301a680ace_tuple, 1, const_str_plain_attr ); Py_INCREF( const_str_plain_attr );
    PyTuple_SET_ITEM( const_tuple_7fa5e573061407858ec572301a680ace_tuple, 2, const_str_plain_comp ); Py_INCREF( const_str_plain_comp );
    const_str_plain__check = UNSTREAM_STRING( &constant_bin[ 81329 ], 6, 1 );
    PyTuple_SET_ITEM( const_tuple_7fa5e573061407858ec572301a680ace_tuple, 3, const_str_plain__check ); Py_INCREF( const_str_plain__check );
    PyTuple_SET_ITEM( const_tuple_7fa5e573061407858ec572301a680ace_tuple, 4, const_str_plain_dct ); Py_INCREF( const_str_plain_dct );
    PyTuple_SET_ITEM( const_tuple_7fa5e573061407858ec572301a680ace_tuple, 5, const_str_plain_meta ); Py_INCREF( const_str_plain_meta );
    const_tuple_8cd4cfe29da562f7e2f4c3865abeabd1_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_8cd4cfe29da562f7e2f4c3865abeabd1_tuple, 0, const_str_plain_ABCSparseArray ); Py_INCREF( const_str_plain_ABCSparseArray );
    PyTuple_SET_ITEM( const_tuple_8cd4cfe29da562f7e2f4c3865abeabd1_tuple, 1, const_str_plain__subtyp ); Py_INCREF( const_str_plain__subtyp );
    const_tuple_str_plain_sparse_array_str_plain_sparse_series_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_sparse_array_str_plain_sparse_series_tuple, 0, const_str_plain_sparse_array ); Py_INCREF( const_str_plain_sparse_array );
    PyTuple_SET_ITEM( const_tuple_str_plain_sparse_array_str_plain_sparse_series_tuple, 1, const_str_plain_sparse_series ); Py_INCREF( const_str_plain_sparse_series );
    PyTuple_SET_ITEM( const_tuple_8cd4cfe29da562f7e2f4c3865abeabd1_tuple, 2, const_tuple_str_plain_sparse_array_str_plain_sparse_series_tuple ); Py_INCREF( const_tuple_str_plain_sparse_array_str_plain_sparse_series_tuple );
    const_tuple_e81beef27d536d6d71359d61bfd33781_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_e81beef27d536d6d71359d61bfd33781_tuple, 0, const_str_plain_ABCFloat64Index ); Py_INCREF( const_str_plain_ABCFloat64Index );
    PyTuple_SET_ITEM( const_tuple_e81beef27d536d6d71359d61bfd33781_tuple, 1, const_str_plain__typ ); Py_INCREF( const_str_plain__typ );
    const_tuple_str_plain_float64index_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_float64index_tuple, 0, const_str_plain_float64index ); Py_INCREF( const_str_plain_float64index );
    PyTuple_SET_ITEM( const_tuple_e81beef27d536d6d71359d61bfd33781_tuple, 2, const_tuple_str_plain_float64index_tuple ); Py_INCREF( const_tuple_str_plain_float64index_tuple );
    const_str_plain_ABCBase = UNSTREAM_STRING( &constant_bin[ 1922163 ], 7, 1 );
    const_tuple_ca0e6f32803588f0a9f0cc22aa814733_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_ca0e6f32803588f0a9f0cc22aa814733_tuple, 0, const_str_plain_ABCDatetimeIndex ); Py_INCREF( const_str_plain_ABCDatetimeIndex );
    PyTuple_SET_ITEM( const_tuple_ca0e6f32803588f0a9f0cc22aa814733_tuple, 1, const_str_plain__typ ); Py_INCREF( const_str_plain__typ );
    const_tuple_str_plain_datetimeindex_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_datetimeindex_tuple, 0, const_str_plain_datetimeindex ); Py_INCREF( const_str_plain_datetimeindex );
    PyTuple_SET_ITEM( const_tuple_ca0e6f32803588f0a9f0cc22aa814733_tuple, 2, const_tuple_str_plain_datetimeindex_tuple ); Py_INCREF( const_tuple_str_plain_datetimeindex_tuple );
    const_str_digest_d5a49727799e8632405b89df92756090 = UNSTREAM_STRING( &constant_bin[ 1922170 ], 38, 0 );
    const_tuple_str_plain_uint64index_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_uint64index_tuple, 0, const_str_plain_uint64index ); Py_INCREF( const_str_plain_uint64index );
    const_tuple_str_plain_timedeltaindex_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_timedeltaindex_tuple, 0, const_str_plain_timedeltaindex ); Py_INCREF( const_str_plain_timedeltaindex );
    const_tuple_1cbbae3c47881c7d89f542e5c3eab677_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_1cbbae3c47881c7d89f542e5c3eab677_tuple, 0, const_str_plain_ABCDateOffset ); Py_INCREF( const_str_plain_ABCDateOffset );
    PyTuple_SET_ITEM( const_tuple_1cbbae3c47881c7d89f542e5c3eab677_tuple, 1, const_str_plain__typ ); Py_INCREF( const_str_plain__typ );
    const_tuple_str_plain_dateoffset_tuple = PyTuple_New( 1 );
    const_str_plain_dateoffset = UNSTREAM_STRING( &constant_bin[ 1922208 ], 10, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_dateoffset_tuple, 0, const_str_plain_dateoffset ); Py_INCREF( const_str_plain_dateoffset );
    PyTuple_SET_ITEM( const_tuple_1cbbae3c47881c7d89f542e5c3eab677_tuple, 2, const_tuple_str_plain_dateoffset_tuple ); Py_INCREF( const_tuple_str_plain_dateoffset_tuple );
    const_str_plain_ABCInterval = UNSTREAM_STRING( &constant_bin[ 1922218 ], 11, 1 );
    const_tuple_55a06a97336d2526a52d514e01776b18_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_55a06a97336d2526a52d514e01776b18_tuple, 0, const_str_plain_ABCSeries ); Py_INCREF( const_str_plain_ABCSeries );
    PyTuple_SET_ITEM( const_tuple_55a06a97336d2526a52d514e01776b18_tuple, 1, const_str_plain__typ ); Py_INCREF( const_str_plain__typ );
    PyTuple_SET_ITEM( const_tuple_55a06a97336d2526a52d514e01776b18_tuple, 2, const_tuple_str_plain_series_tuple ); Py_INCREF( const_tuple_str_plain_series_tuple );
    const_tuple_str_plain_intervalindex_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_intervalindex_tuple, 0, const_str_plain_intervalindex ); Py_INCREF( const_str_plain_intervalindex );
    const_tuple_str_plain_cls_str_plain_inst_str_plain_attr_str_plain_comp_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_str_plain_cls_str_plain_inst_str_plain_attr_str_plain_comp_tuple, 0, const_str_plain_cls ); Py_INCREF( const_str_plain_cls );
    PyTuple_SET_ITEM( const_tuple_str_plain_cls_str_plain_inst_str_plain_attr_str_plain_comp_tuple, 1, const_str_plain_inst ); Py_INCREF( const_str_plain_inst );
    PyTuple_SET_ITEM( const_tuple_str_plain_cls_str_plain_inst_str_plain_attr_str_plain_comp_tuple, 2, const_str_plain_attr ); Py_INCREF( const_str_plain_attr );
    PyTuple_SET_ITEM( const_tuple_str_plain_cls_str_plain_inst_str_plain_attr_str_plain_comp_tuple, 3, const_str_plain_comp ); Py_INCREF( const_str_plain_comp );
    const_tuple_6155f4f0a36b0e20cfc83c9cfa52cf96_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_6155f4f0a36b0e20cfc83c9cfa52cf96_tuple, 0, const_str_plain_ABCIndex ); Py_INCREF( const_str_plain_ABCIndex );
    PyTuple_SET_ITEM( const_tuple_6155f4f0a36b0e20cfc83c9cfa52cf96_tuple, 1, const_str_plain__typ ); Py_INCREF( const_str_plain__typ );
    PyTuple_SET_ITEM( const_tuple_6155f4f0a36b0e20cfc83c9cfa52cf96_tuple, 2, const_tuple_str_plain_index_tuple ); Py_INCREF( const_tuple_str_plain_index_tuple );
    const_tuple_a9bd744d56f40fb125578446e1149015_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_a9bd744d56f40fb125578446e1149015_tuple, 0, const_str_plain_ABCCategorical ); Py_INCREF( const_str_plain_ABCCategorical );
    PyTuple_SET_ITEM( const_tuple_a9bd744d56f40fb125578446e1149015_tuple, 1, const_str_plain__typ ); Py_INCREF( const_str_plain__typ );
    PyTuple_SET_ITEM( const_tuple_a9bd744d56f40fb125578446e1149015_tuple, 2, const_str_plain_categorical ); Py_INCREF( const_str_plain_categorical );
    const_tuple_498ec8f05822c48e89fb1a9ca5511e2c_tuple = PyTuple_New( 3 );
    const_str_plain_ABCInt64Index = UNSTREAM_STRING( &constant_bin[ 1922229 ], 13, 1 );
    PyTuple_SET_ITEM( const_tuple_498ec8f05822c48e89fb1a9ca5511e2c_tuple, 0, const_str_plain_ABCInt64Index ); Py_INCREF( const_str_plain_ABCInt64Index );
    PyTuple_SET_ITEM( const_tuple_498ec8f05822c48e89fb1a9ca5511e2c_tuple, 1, const_str_plain__typ ); Py_INCREF( const_str_plain__typ );
    PyTuple_SET_ITEM( const_tuple_498ec8f05822c48e89fb1a9ca5511e2c_tuple, 2, const_tuple_str_plain_int64index_tuple ); Py_INCREF( const_tuple_str_plain_int64index_tuple );
    const_str_plain_ABCIntervalIndex = UNSTREAM_STRING( &constant_bin[ 1922242 ], 16, 1 );
    const_tuple_d0c24e19633621eed623faa83836dd29_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_d0c24e19633621eed623faa83836dd29_tuple, 0, const_str_plain_ABCIntervalIndex ); Py_INCREF( const_str_plain_ABCIntervalIndex );
    PyTuple_SET_ITEM( const_tuple_d0c24e19633621eed623faa83836dd29_tuple, 1, const_str_plain__typ ); Py_INCREF( const_str_plain__typ );
    PyTuple_SET_ITEM( const_tuple_d0c24e19633621eed623faa83836dd29_tuple, 2, const_tuple_str_plain_intervalindex_tuple ); Py_INCREF( const_tuple_str_plain_intervalindex_tuple );
    const_tuple_ee46e31b6dd9a260cbcf686ed7e1d180_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_ee46e31b6dd9a260cbcf686ed7e1d180_tuple, 0, const_str_plain_ABCPeriod ); Py_INCREF( const_str_plain_ABCPeriod );
    PyTuple_SET_ITEM( const_tuple_ee46e31b6dd9a260cbcf686ed7e1d180_tuple, 1, const_str_plain__typ ); Py_INCREF( const_str_plain__typ );
    PyTuple_SET_ITEM( const_tuple_ee46e31b6dd9a260cbcf686ed7e1d180_tuple, 2, const_tuple_str_plain_period_tuple ); Py_INCREF( const_tuple_str_plain_period_tuple );
    const_tuple_dd895ef83b60eefcd25e7374010c2cae_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_dd895ef83b60eefcd25e7374010c2cae_tuple, 0, const_str_plain_ABCExtensionArray ); Py_INCREF( const_str_plain_ABCExtensionArray );
    PyTuple_SET_ITEM( const_tuple_dd895ef83b60eefcd25e7374010c2cae_tuple, 1, const_str_plain__typ ); Py_INCREF( const_str_plain__typ );
    const_tuple_str_plain_extension_str_plain_categorical_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_extension_str_plain_categorical_tuple, 0, const_str_plain_extension ); Py_INCREF( const_str_plain_extension );
    PyTuple_SET_ITEM( const_tuple_str_plain_extension_str_plain_categorical_tuple, 1, const_str_plain_categorical ); Py_INCREF( const_str_plain_categorical );
    PyTuple_SET_ITEM( const_tuple_dd895ef83b60eefcd25e7374010c2cae_tuple, 2, const_tuple_str_plain_extension_str_plain_categorical_tuple ); Py_INCREF( const_tuple_str_plain_extension_str_plain_categorical_tuple );
    const_tuple_str_plain_panel_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_panel_tuple, 0, const_str_plain_panel ); Py_INCREF( const_str_plain_panel );
    const_tuple_a07eb65fd3b116acb1435fd5f485e3c0_tuple = PyTuple_New( 11 );
    PyTuple_SET_ITEM( const_tuple_a07eb65fd3b116acb1435fd5f485e3c0_tuple, 0, const_str_plain_index ); Py_INCREF( const_str_plain_index );
    PyTuple_SET_ITEM( const_tuple_a07eb65fd3b116acb1435fd5f485e3c0_tuple, 1, const_str_plain_int64index ); Py_INCREF( const_str_plain_int64index );
    PyTuple_SET_ITEM( const_tuple_a07eb65fd3b116acb1435fd5f485e3c0_tuple, 2, const_str_plain_rangeindex ); Py_INCREF( const_str_plain_rangeindex );
    PyTuple_SET_ITEM( const_tuple_a07eb65fd3b116acb1435fd5f485e3c0_tuple, 3, const_str_plain_float64index ); Py_INCREF( const_str_plain_float64index );
    PyTuple_SET_ITEM( const_tuple_a07eb65fd3b116acb1435fd5f485e3c0_tuple, 4, const_str_plain_uint64index ); Py_INCREF( const_str_plain_uint64index );
    PyTuple_SET_ITEM( const_tuple_a07eb65fd3b116acb1435fd5f485e3c0_tuple, 5, const_str_plain_multiindex ); Py_INCREF( const_str_plain_multiindex );
    PyTuple_SET_ITEM( const_tuple_a07eb65fd3b116acb1435fd5f485e3c0_tuple, 6, const_str_plain_datetimeindex ); Py_INCREF( const_str_plain_datetimeindex );
    PyTuple_SET_ITEM( const_tuple_a07eb65fd3b116acb1435fd5f485e3c0_tuple, 7, const_str_plain_timedeltaindex ); Py_INCREF( const_str_plain_timedeltaindex );
    PyTuple_SET_ITEM( const_tuple_a07eb65fd3b116acb1435fd5f485e3c0_tuple, 8, const_str_plain_periodindex ); Py_INCREF( const_str_plain_periodindex );
    PyTuple_SET_ITEM( const_tuple_a07eb65fd3b116acb1435fd5f485e3c0_tuple, 9, const_str_plain_categoricalindex ); Py_INCREF( const_str_plain_categoricalindex );
    PyTuple_SET_ITEM( const_tuple_a07eb65fd3b116acb1435fd5f485e3c0_tuple, 10, const_str_plain_intervalindex ); Py_INCREF( const_str_plain_intervalindex );
    const_tuple_d542cd81ad9e787128b55a2524f2d1f6_tuple = PyTuple_New( 3 );
    const_str_plain_ABCUInt64Index = UNSTREAM_STRING( &constant_bin[ 1922258 ], 14, 1 );
    PyTuple_SET_ITEM( const_tuple_d542cd81ad9e787128b55a2524f2d1f6_tuple, 0, const_str_plain_ABCUInt64Index ); Py_INCREF( const_str_plain_ABCUInt64Index );
    PyTuple_SET_ITEM( const_tuple_d542cd81ad9e787128b55a2524f2d1f6_tuple, 1, const_str_plain__typ ); Py_INCREF( const_str_plain__typ );
    PyTuple_SET_ITEM( const_tuple_d542cd81ad9e787128b55a2524f2d1f6_tuple, 2, const_tuple_str_plain_uint64index_tuple ); Py_INCREF( const_tuple_str_plain_uint64index_tuple );
    const_tuple_str_plain_categoricalindex_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_categoricalindex_tuple, 0, const_str_plain_categoricalindex ); Py_INCREF( const_str_plain_categoricalindex );
    const_str_digest_99629574af3e9a60194722e67c76327f = UNSTREAM_STRING( &constant_bin[ 1922272 ], 92, 0 );
    const_tuple_cdd89e57eb924c02f17f1cf30aaeceaa_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_cdd89e57eb924c02f17f1cf30aaeceaa_tuple, 0, const_str_plain_ABCSparseDataFrame ); Py_INCREF( const_str_plain_ABCSparseDataFrame );
    PyTuple_SET_ITEM( const_tuple_cdd89e57eb924c02f17f1cf30aaeceaa_tuple, 1, const_str_plain__subtyp ); Py_INCREF( const_str_plain__subtyp );
    PyTuple_SET_ITEM( const_tuple_cdd89e57eb924c02f17f1cf30aaeceaa_tuple, 2, const_tuple_str_plain_sparse_frame_tuple ); Py_INCREF( const_tuple_str_plain_sparse_frame_tuple );
    const_str_plain_create_pandas_abc_type = UNSTREAM_STRING( &constant_bin[ 1922170 ], 22, 1 );
    const_str_plain__ABCGeneric = UNSTREAM_STRING( &constant_bin[ 1922364 ], 11, 1 );
    const_tuple_e59941f973d0c3f39d45902b6fb98a97_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_e59941f973d0c3f39d45902b6fb98a97_tuple, 0, const_str_plain_ABCInterval ); Py_INCREF( const_str_plain_ABCInterval );
    PyTuple_SET_ITEM( const_tuple_e59941f973d0c3f39d45902b6fb98a97_tuple, 1, const_str_plain__typ ); Py_INCREF( const_str_plain__typ );
    PyTuple_SET_ITEM( const_tuple_e59941f973d0c3f39d45902b6fb98a97_tuple, 2, const_tuple_str_plain_interval_tuple ); Py_INCREF( const_tuple_str_plain_interval_tuple );
    const_str_plain___instancecheck__ = UNSTREAM_STRING( &constant_bin[ 1922375 ], 17, 1 );
    const_tuple_d4acdb449ce2c1fb373e321bbe598787_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_d4acdb449ce2c1fb373e321bbe598787_tuple, 0, const_str_plain_ABCPeriodIndex ); Py_INCREF( const_str_plain_ABCPeriodIndex );
    PyTuple_SET_ITEM( const_tuple_d4acdb449ce2c1fb373e321bbe598787_tuple, 1, const_str_plain__typ ); Py_INCREF( const_str_plain__typ );
    PyTuple_SET_ITEM( const_tuple_d4acdb449ce2c1fb373e321bbe598787_tuple, 2, const_tuple_str_plain_periodindex_tuple ); Py_INCREF( const_tuple_str_plain_periodindex_tuple );
    const_str_digest_95b149e5ddb3d0bb85db3124dd3ef793 = UNSTREAM_STRING( &constant_bin[ 1922392 ], 35, 0 );
    const_tuple_bc4cb33cd97b64ecf0d05ff12856b10f_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_bc4cb33cd97b64ecf0d05ff12856b10f_tuple, 0, const_str_plain_ABCPanel ); Py_INCREF( const_str_plain_ABCPanel );
    PyTuple_SET_ITEM( const_tuple_bc4cb33cd97b64ecf0d05ff12856b10f_tuple, 1, const_str_plain__typ ); Py_INCREF( const_str_plain__typ );
    PyTuple_SET_ITEM( const_tuple_bc4cb33cd97b64ecf0d05ff12856b10f_tuple, 2, const_tuple_str_plain_panel_tuple ); Py_INCREF( const_tuple_str_plain_panel_tuple );
    const_tuple_str_plain_dataframe_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_dataframe_tuple, 0, const_str_plain_dataframe ); Py_INCREF( const_str_plain_dataframe );
    const_tuple_088e7186a3be3fdab3964802df549ae3_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_088e7186a3be3fdab3964802df549ae3_tuple, 0, const_str_plain_ABCTimedeltaIndex ); Py_INCREF( const_str_plain_ABCTimedeltaIndex );
    PyTuple_SET_ITEM( const_tuple_088e7186a3be3fdab3964802df549ae3_tuple, 1, const_str_plain__typ ); Py_INCREF( const_str_plain__typ );
    PyTuple_SET_ITEM( const_tuple_088e7186a3be3fdab3964802df549ae3_tuple, 2, const_tuple_str_plain_timedeltaindex_tuple ); Py_INCREF( const_tuple_str_plain_timedeltaindex_tuple );
    const_str_digest_607e52cf80d7e0fd83932301ca6c8266 = UNSTREAM_STRING( &constant_bin[ 1922427 ], 48, 0 );
    const_tuple_1b417d1399297279333d7a632ca8c8a8_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_1b417d1399297279333d7a632ca8c8a8_tuple, 0, const_str_plain_ABCIndexClass ); Py_INCREF( const_str_plain_ABCIndexClass );
    PyTuple_SET_ITEM( const_tuple_1b417d1399297279333d7a632ca8c8a8_tuple, 1, const_str_plain__typ ); Py_INCREF( const_str_plain__typ );
    PyTuple_SET_ITEM( const_tuple_1b417d1399297279333d7a632ca8c8a8_tuple, 2, const_tuple_a07eb65fd3b116acb1435fd5f485e3c0_tuple ); Py_INCREF( const_tuple_a07eb65fd3b116acb1435fd5f485e3c0_tuple );
    const_tuple_3a2ca9a72da6328235c2c7b989760719_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_3a2ca9a72da6328235c2c7b989760719_tuple, 0, const_str_plain_ABCDataFrame ); Py_INCREF( const_str_plain_ABCDataFrame );
    PyTuple_SET_ITEM( const_tuple_3a2ca9a72da6328235c2c7b989760719_tuple, 1, const_str_plain__typ ); Py_INCREF( const_str_plain__typ );
    PyTuple_SET_ITEM( const_tuple_3a2ca9a72da6328235c2c7b989760719_tuple, 2, const_tuple_str_plain_dataframe_tuple ); Py_INCREF( const_tuple_str_plain_dataframe_tuple );
    const_str_plain___subclasscheck__ = UNSTREAM_STRING( &constant_bin[ 1922475 ], 17, 1 );
    const_tuple_ecfb4f82c2a82855e2b2efd7e568ab2f_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_ecfb4f82c2a82855e2b2efd7e568ab2f_tuple, 0, const_str_plain_ABCCategoricalIndex ); Py_INCREF( const_str_plain_ABCCategoricalIndex );
    PyTuple_SET_ITEM( const_tuple_ecfb4f82c2a82855e2b2efd7e568ab2f_tuple, 1, const_str_plain__typ ); Py_INCREF( const_str_plain__typ );
    PyTuple_SET_ITEM( const_tuple_ecfb4f82c2a82855e2b2efd7e568ab2f_tuple, 2, const_tuple_str_plain_categoricalindex_tuple ); Py_INCREF( const_tuple_str_plain_categoricalindex_tuple );
    const_tuple_str_plain_cls_str_plain_inst_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_cls_str_plain_inst_tuple, 0, const_str_plain_cls ); Py_INCREF( const_str_plain_cls );
    PyTuple_SET_ITEM( const_tuple_str_plain_cls_str_plain_inst_tuple, 1, const_str_plain_inst ); Py_INCREF( const_str_plain_inst );
    const_str_digest_6ad1fe98e4357527e1848039d70479f9 = UNSTREAM_STRING( &constant_bin[ 1922492 ], 29, 0 );
    const_tuple_str_plain__ABCGeneric_tuple_type_type_tuple_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain__ABCGeneric_tuple_type_type_tuple_tuple, 0, const_str_plain__ABCGeneric ); Py_INCREF( const_str_plain__ABCGeneric );
    PyTuple_SET_ITEM( const_tuple_str_plain__ABCGeneric_tuple_type_type_tuple_tuple, 1, const_tuple_type_type_tuple ); Py_INCREF( const_tuple_type_type_tuple );

    constants_created = true;
}

#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_pandas$core$dtypes$generic( void )
{
    // The module may not have been used at all.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_e76bce2eca13a810ebdfe1a6cc216e6b;
static PyCodeObject *codeobj_947b09ac8b6198e47b6d6f38883e9a08;
static PyCodeObject *codeobj_aa080a385100a5f86e0bc74b92b72b47;
static PyCodeObject *codeobj_92b4faed18134c803aaaec58f74c045c;
static PyCodeObject *codeobj_a3bd0e63a17e020d74f65c3edcd61c92;

static void createModuleCodeObjects(void)
{
    module_filename_obj = const_str_digest_99629574af3e9a60194722e67c76327f;
    codeobj_e76bce2eca13a810ebdfe1a6cc216e6b = MAKE_CODEOBJ( module_filename_obj, const_str_digest_95b149e5ddb3d0bb85db3124dd3ef793, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_947b09ac8b6198e47b6d6f38883e9a08 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__ABCGeneric, 64, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_aa080a385100a5f86e0bc74b92b72b47 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___instancecheck__, 66, const_tuple_str_plain_cls_str_plain_inst_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_92b4faed18134c803aaaec58f74c045c = MAKE_CODEOBJ( module_filename_obj, const_str_plain__check, 7, const_tuple_str_plain_cls_str_plain_inst_str_plain_attr_str_plain_comp_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_a3bd0e63a17e020d74f65c3edcd61c92 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_create_pandas_abc_type, 6, const_tuple_7fa5e573061407858ec572301a680ace_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
}

// The module function declarations.
static PyObject *MAKE_FUNCTION_pandas$core$dtypes$generic$$$function_1_create_pandas_abc_type(  );


static PyObject *MAKE_FUNCTION_pandas$core$dtypes$generic$$$function_1_create_pandas_abc_type$$$function_1__check( struct Nuitka_CellObject *closure_attr, struct Nuitka_CellObject *closure_comp );


static PyObject *MAKE_FUNCTION_pandas$core$dtypes$generic$$$function_2___instancecheck__(  );


// The module function definitions.
static PyObject *impl_pandas$core$dtypes$generic$$$function_1_create_pandas_abc_type( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_name = python_pars[ 0 ];
    struct Nuitka_CellObject *par_attr = PyCell_NEW1( python_pars[ 1 ] );
    struct Nuitka_CellObject *par_comp = PyCell_NEW1( python_pars[ 2 ] );
    PyObject *var__check = NULL;
    PyObject *var_dct = NULL;
    PyObject *var_meta = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_args_element_name_1;
    PyObject *tmp_args_element_name_2;
    PyObject *tmp_args_element_name_3;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_bases_name_1;
    PyObject *tmp_called_name_1;
    PyObject *tmp_classmethod_arg_1;
    PyObject *tmp_dict_key_1;
    PyObject *tmp_dict_key_2;
    PyObject *tmp_dict_name_1;
    PyObject *tmp_dict_value_1;
    PyObject *tmp_dict_value_2;
    int tmp_res;
    PyObject *tmp_return_value;
    PyObject *tmp_type_name_name_1;
    static struct Nuitka_FrameObject *cache_frame_a3bd0e63a17e020d74f65c3edcd61c92 = NULL;

    struct Nuitka_FrameObject *frame_a3bd0e63a17e020d74f65c3edcd61c92;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_a3bd0e63a17e020d74f65c3edcd61c92, codeobj_a3bd0e63a17e020d74f65c3edcd61c92, module_pandas$core$dtypes$generic, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_a3bd0e63a17e020d74f65c3edcd61c92 = cache_frame_a3bd0e63a17e020d74f65c3edcd61c92;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_a3bd0e63a17e020d74f65c3edcd61c92 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_a3bd0e63a17e020d74f65c3edcd61c92 ) == 2 ); // Frame stack

    // Framed code:
    tmp_classmethod_arg_1 = MAKE_FUNCTION_pandas$core$dtypes$generic$$$function_1_create_pandas_abc_type$$$function_1__check( par_attr, par_comp );
    tmp_assign_source_1 = BUILTIN_CLASSMETHOD( tmp_classmethod_arg_1 );
    Py_DECREF( tmp_classmethod_arg_1 );
    if ( tmp_assign_source_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 7;
        type_description_1 = "occooo";
        goto frame_exception_exit_1;
    }
    assert( var__check == NULL );
    var__check = tmp_assign_source_1;

    tmp_dict_key_1 = const_str_plain___instancecheck__;
    tmp_dict_value_1 = var__check;

    CHECK_OBJECT( tmp_dict_value_1 );
    tmp_assign_source_2 = _PyDict_NewPresized( 2 );
    tmp_res = PyDict_SetItem( tmp_assign_source_2, tmp_dict_key_1, tmp_dict_value_1 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_2 = const_str_plain___subclasscheck__;
    tmp_dict_value_2 = var__check;

    CHECK_OBJECT( tmp_dict_value_2 );
    tmp_res = PyDict_SetItem( tmp_assign_source_2, tmp_dict_key_2, tmp_dict_value_2 );
    assert( !(tmp_res != 0) );
    assert( var_dct == NULL );
    var_dct = tmp_assign_source_2;

    tmp_type_name_name_1 = const_str_plain_ABCBase;
    tmp_bases_name_1 = const_tuple_type_type_tuple;
    tmp_dict_name_1 = var_dct;

    CHECK_OBJECT( tmp_dict_name_1 );
    tmp_assign_source_3 = BUILTIN_TYPE3( const_str_digest_30235a8809db2818daf1f4bd1339d3df, tmp_type_name_name_1, tmp_bases_name_1, tmp_dict_name_1 );
    if ( tmp_assign_source_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 12;
        type_description_1 = "occooo";
        goto frame_exception_exit_1;
    }
    assert( var_meta == NULL );
    var_meta = tmp_assign_source_3;

    tmp_called_name_1 = var_meta;

    CHECK_OBJECT( tmp_called_name_1 );
    tmp_args_element_name_1 = par_name;

    CHECK_OBJECT( tmp_args_element_name_1 );
    tmp_args_element_name_2 = const_tuple_empty;
    tmp_args_element_name_3 = var_dct;

    CHECK_OBJECT( tmp_args_element_name_3 );
    frame_a3bd0e63a17e020d74f65c3edcd61c92->m_frame.f_lineno = 13;
    {
        PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3 };
        tmp_return_value = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_1, call_args );
    }

    if ( tmp_return_value == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 13;
        type_description_1 = "occooo";
        goto frame_exception_exit_1;
    }
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a3bd0e63a17e020d74f65c3edcd61c92 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_a3bd0e63a17e020d74f65c3edcd61c92 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a3bd0e63a17e020d74f65c3edcd61c92 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_a3bd0e63a17e020d74f65c3edcd61c92, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_a3bd0e63a17e020d74f65c3edcd61c92->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_a3bd0e63a17e020d74f65c3edcd61c92, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_a3bd0e63a17e020d74f65c3edcd61c92,
        type_description_1,
        par_name,
        par_attr,
        par_comp,
        var__check,
        var_dct,
        var_meta
    );


    // Release cached frame.
    if ( frame_a3bd0e63a17e020d74f65c3edcd61c92 == cache_frame_a3bd0e63a17e020d74f65c3edcd61c92 )
    {
        Py_DECREF( frame_a3bd0e63a17e020d74f65c3edcd61c92 );
    }
    cache_frame_a3bd0e63a17e020d74f65c3edcd61c92 = NULL;

    assertFrameObject( frame_a3bd0e63a17e020d74f65c3edcd61c92 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$core$dtypes$generic$$$function_1_create_pandas_abc_type );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_name );
    par_name = NULL;

    CHECK_OBJECT( (PyObject *)par_attr );
    Py_DECREF( par_attr );
    par_attr = NULL;

    CHECK_OBJECT( (PyObject *)par_comp );
    Py_DECREF( par_comp );
    par_comp = NULL;

    Py_XDECREF( var__check );
    var__check = NULL;

    Py_XDECREF( var_dct );
    var_dct = NULL;

    Py_XDECREF( var_meta );
    var_meta = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_name );
    par_name = NULL;

    CHECK_OBJECT( (PyObject *)par_attr );
    Py_DECREF( par_attr );
    par_attr = NULL;

    CHECK_OBJECT( (PyObject *)par_comp );
    Py_DECREF( par_comp );
    par_comp = NULL;

    Py_XDECREF( var__check );
    var__check = NULL;

    Py_XDECREF( var_dct );
    var_dct = NULL;

    Py_XDECREF( var_meta );
    var_meta = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$core$dtypes$generic$$$function_1_create_pandas_abc_type );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$core$dtypes$generic$$$function_1_create_pandas_abc_type$$$function_1__check( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_cls = python_pars[ 0 ];
    PyObject *par_inst = python_pars[ 1 ];
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_compexpr_left_1;
    PyObject *tmp_compexpr_right_1;
    PyObject *tmp_getattr_attr_1;
    PyObject *tmp_getattr_default_1;
    PyObject *tmp_getattr_target_1;
    PyObject *tmp_return_value;
    static struct Nuitka_FrameObject *cache_frame_92b4faed18134c803aaaec58f74c045c = NULL;

    struct Nuitka_FrameObject *frame_92b4faed18134c803aaaec58f74c045c;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_92b4faed18134c803aaaec58f74c045c, codeobj_92b4faed18134c803aaaec58f74c045c, module_pandas$core$dtypes$generic, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_92b4faed18134c803aaaec58f74c045c = cache_frame_92b4faed18134c803aaaec58f74c045c;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_92b4faed18134c803aaaec58f74c045c );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_92b4faed18134c803aaaec58f74c045c ) == 2 ); // Frame stack

    // Framed code:
    tmp_getattr_target_1 = par_inst;

    CHECK_OBJECT( tmp_getattr_target_1 );
    if ( self->m_closure[0] == NULL )
    {
        tmp_getattr_attr_1 = NULL;
    }
    else
    {
        tmp_getattr_attr_1 = PyCell_GET( self->m_closure[0] );
    }

    if ( tmp_getattr_attr_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "attr" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 9;
        type_description_1 = "oocc";
        goto frame_exception_exit_1;
    }

    tmp_getattr_default_1 = const_str_plain__typ;
    tmp_compexpr_left_1 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, tmp_getattr_default_1 );
    if ( tmp_compexpr_left_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 9;
        type_description_1 = "oocc";
        goto frame_exception_exit_1;
    }
    if ( self->m_closure[1] == NULL )
    {
        tmp_compexpr_right_1 = NULL;
    }
    else
    {
        tmp_compexpr_right_1 = PyCell_GET( self->m_closure[1] );
    }

    if ( tmp_compexpr_right_1 == NULL )
    {
        Py_DECREF( tmp_compexpr_left_1 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "comp" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 9;
        type_description_1 = "oocc";
        goto frame_exception_exit_1;
    }

    tmp_return_value = SEQUENCE_CONTAINS( tmp_compexpr_left_1, tmp_compexpr_right_1 );
    Py_DECREF( tmp_compexpr_left_1 );
    if ( tmp_return_value == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 9;
        type_description_1 = "oocc";
        goto frame_exception_exit_1;
    }
    Py_INCREF( tmp_return_value );
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_92b4faed18134c803aaaec58f74c045c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_92b4faed18134c803aaaec58f74c045c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_92b4faed18134c803aaaec58f74c045c );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_92b4faed18134c803aaaec58f74c045c, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_92b4faed18134c803aaaec58f74c045c->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_92b4faed18134c803aaaec58f74c045c, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_92b4faed18134c803aaaec58f74c045c,
        type_description_1,
        par_cls,
        par_inst,
        self->m_closure[0],
        self->m_closure[1]
    );


    // Release cached frame.
    if ( frame_92b4faed18134c803aaaec58f74c045c == cache_frame_92b4faed18134c803aaaec58f74c045c )
    {
        Py_DECREF( frame_92b4faed18134c803aaaec58f74c045c );
    }
    cache_frame_92b4faed18134c803aaaec58f74c045c = NULL;

    assertFrameObject( frame_92b4faed18134c803aaaec58f74c045c );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$core$dtypes$generic$$$function_1_create_pandas_abc_type$$$function_1__check );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_cls );
    par_cls = NULL;

    Py_XDECREF( par_inst );
    par_inst = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_cls );
    par_cls = NULL;

    Py_XDECREF( par_inst );
    par_inst = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$core$dtypes$generic$$$function_1_create_pandas_abc_type$$$function_1__check );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$core$dtypes$generic$$$function_2___instancecheck__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_cls = python_pars[ 0 ];
    PyObject *par_inst = python_pars[ 1 ];
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_hasattr_attr_1;
    PyObject *tmp_hasattr_value_1;
    PyObject *tmp_return_value;
    static struct Nuitka_FrameObject *cache_frame_aa080a385100a5f86e0bc74b92b72b47 = NULL;

    struct Nuitka_FrameObject *frame_aa080a385100a5f86e0bc74b92b72b47;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_aa080a385100a5f86e0bc74b92b72b47, codeobj_aa080a385100a5f86e0bc74b92b72b47, module_pandas$core$dtypes$generic, sizeof(void *)+sizeof(void *) );
    frame_aa080a385100a5f86e0bc74b92b72b47 = cache_frame_aa080a385100a5f86e0bc74b92b72b47;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_aa080a385100a5f86e0bc74b92b72b47 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_aa080a385100a5f86e0bc74b92b72b47 ) == 2 ); // Frame stack

    // Framed code:
    tmp_hasattr_value_1 = par_inst;

    CHECK_OBJECT( tmp_hasattr_value_1 );
    tmp_hasattr_attr_1 = const_str_plain__data;
    tmp_return_value = BUILTIN_HASATTR( tmp_hasattr_value_1, tmp_hasattr_attr_1 );
    if ( tmp_return_value == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 67;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }
    Py_INCREF( tmp_return_value );
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_aa080a385100a5f86e0bc74b92b72b47 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_aa080a385100a5f86e0bc74b92b72b47 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_aa080a385100a5f86e0bc74b92b72b47 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_aa080a385100a5f86e0bc74b92b72b47, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_aa080a385100a5f86e0bc74b92b72b47->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_aa080a385100a5f86e0bc74b92b72b47, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_aa080a385100a5f86e0bc74b92b72b47,
        type_description_1,
        par_cls,
        par_inst
    );


    // Release cached frame.
    if ( frame_aa080a385100a5f86e0bc74b92b72b47 == cache_frame_aa080a385100a5f86e0bc74b92b72b47 )
    {
        Py_DECREF( frame_aa080a385100a5f86e0bc74b92b72b47 );
    }
    cache_frame_aa080a385100a5f86e0bc74b92b72b47 = NULL;

    assertFrameObject( frame_aa080a385100a5f86e0bc74b92b72b47 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$core$dtypes$generic$$$function_2___instancecheck__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    Py_XDECREF( par_inst );
    par_inst = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    Py_XDECREF( par_inst );
    par_inst = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$core$dtypes$generic$$$function_2___instancecheck__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}



static PyObject *MAKE_FUNCTION_pandas$core$dtypes$generic$$$function_1_create_pandas_abc_type(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$core$dtypes$generic$$$function_1_create_pandas_abc_type,
        const_str_plain_create_pandas_abc_type,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_a3bd0e63a17e020d74f65c3edcd61c92,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$core$dtypes$generic,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$core$dtypes$generic$$$function_1_create_pandas_abc_type$$$function_1__check( struct Nuitka_CellObject *closure_attr, struct Nuitka_CellObject *closure_comp )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$core$dtypes$generic$$$function_1_create_pandas_abc_type$$$function_1__check,
        const_str_plain__check,
#if PYTHON_VERSION >= 300
        const_str_digest_d5a49727799e8632405b89df92756090,
#endif
        codeobj_92b4faed18134c803aaaec58f74c045c,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$core$dtypes$generic,
        Py_None,
        2
    );

result->m_closure[0] = closure_attr;
Py_INCREF( result->m_closure[0] );
result->m_closure[1] = closure_comp;
Py_INCREF( result->m_closure[1] );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$core$dtypes$generic$$$function_2___instancecheck__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$core$dtypes$generic$$$function_2___instancecheck__,
        const_str_plain___instancecheck__,
#if PYTHON_VERSION >= 300
        const_str_digest_6ad1fe98e4357527e1848039d70479f9,
#endif
        codeobj_aa080a385100a5f86e0bc74b92b72b47,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$core$dtypes$generic,
        Py_None,
        0
    );


    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_pandas$core$dtypes$generic =
{
    PyModuleDef_HEAD_INIT,
    "pandas.core.dtypes.generic",   /* m_name */
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;

extern PyObject *const_str_plain___loader__;
extern PyObject *metapath_based_loader;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL.

MOD_INIT_DECL( pandas$core$dtypes$generic )
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_pandas$core$dtypes$generic );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("pandas.core.dtypes.generic: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("pandas.core.dtypes.generic: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("pandas.core.dtypes.generic: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initpandas$core$dtypes$generic" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_pandas$core$dtypes$generic = Py_InitModule4(
        "pandas.core.dtypes.generic",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_pandas$core$dtypes$generic = PyModule_Create( &mdef_pandas$core$dtypes$generic );
#endif

    moduledict_pandas$core$dtypes$generic = MODULE_DICT( module_pandas$core$dtypes$generic );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_pandas$core$dtypes$generic,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );

#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_pandas$core$dtypes$generic,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_pandas$core$dtypes$generic,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_pandas$core$dtypes$generic );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PySys_GetObject( (char *)"modules" ), const_str_digest_30235a8809db2818daf1f4bd1339d3df, module_pandas$core$dtypes$generic );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain___loader__, metapath_based_loader );
#endif

#if PYTHON_VERSION >= 340
#if 0
    UPDATE_STRING_DICT0( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain___name__ ),
            metapath_based_loader
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );

        UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );

        Py_DECREF( module_spec_class );
    }
#endif
#endif


    // Temp variables if any
    PyObject *outline_0_var___class__ = NULL;
    PyObject *tmp_class_creation_1__class_decl_dict = NULL;
    PyObject *tmp_class_creation_1__metaclass = NULL;
    PyObject *tmp_class_creation_1__prepared = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *tmp_args_name_1;
    PyObject *tmp_args_name_2;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_assign_source_5;
    PyObject *tmp_assign_source_6;
    PyObject *tmp_assign_source_7;
    PyObject *tmp_assign_source_8;
    PyObject *tmp_assign_source_9;
    PyObject *tmp_assign_source_10;
    PyObject *tmp_assign_source_11;
    PyObject *tmp_assign_source_12;
    PyObject *tmp_assign_source_13;
    PyObject *tmp_assign_source_14;
    PyObject *tmp_assign_source_15;
    PyObject *tmp_assign_source_16;
    PyObject *tmp_assign_source_17;
    PyObject *tmp_assign_source_18;
    PyObject *tmp_assign_source_19;
    PyObject *tmp_assign_source_20;
    PyObject *tmp_assign_source_21;
    PyObject *tmp_assign_source_22;
    PyObject *tmp_assign_source_23;
    PyObject *tmp_assign_source_24;
    PyObject *tmp_assign_source_25;
    PyObject *tmp_assign_source_26;
    PyObject *tmp_assign_source_27;
    PyObject *tmp_assign_source_28;
    PyObject *tmp_assign_source_29;
    PyObject *tmp_assign_source_30;
    PyObject *tmp_assign_source_31;
    PyObject *tmp_assign_source_32;
    PyObject *tmp_assign_source_33;
    PyObject *tmp_bases_name_1;
    PyObject *tmp_call_arg_element_1;
    PyObject *tmp_call_arg_element_2;
    PyObject *tmp_call_arg_element_3;
    PyObject *tmp_called_name_1;
    PyObject *tmp_called_name_2;
    PyObject *tmp_called_name_3;
    PyObject *tmp_called_name_4;
    PyObject *tmp_called_name_5;
    PyObject *tmp_called_name_6;
    PyObject *tmp_called_name_7;
    PyObject *tmp_called_name_8;
    PyObject *tmp_called_name_9;
    PyObject *tmp_called_name_10;
    PyObject *tmp_called_name_11;
    PyObject *tmp_called_name_12;
    PyObject *tmp_called_name_13;
    PyObject *tmp_called_name_14;
    PyObject *tmp_called_name_15;
    PyObject *tmp_called_name_16;
    PyObject *tmp_called_name_17;
    PyObject *tmp_called_name_18;
    PyObject *tmp_called_name_19;
    PyObject *tmp_called_name_20;
    PyObject *tmp_called_name_21;
    PyObject *tmp_called_name_22;
    PyObject *tmp_called_name_23;
    PyObject *tmp_called_name_24;
    PyObject *tmp_called_name_25;
    PyObject *tmp_called_name_26;
    int tmp_cond_truth_1;
    int tmp_cond_truth_2;
    PyObject *tmp_cond_value_1;
    PyObject *tmp_cond_value_2;
    PyObject *tmp_dict_name_1;
    PyObject *tmp_dict_name_2;
    PyObject *tmp_dict_name_3;
    PyObject *tmp_dictdel_dict;
    PyObject *tmp_dictdel_key;
    PyObject *tmp_dictset_value;
    PyObject *tmp_hasattr_attr_1;
    PyObject *tmp_hasattr_source_1;
    PyObject *tmp_key_name_1;
    PyObject *tmp_key_name_2;
    PyObject *tmp_key_name_3;
    PyObject *tmp_kw_name_1;
    PyObject *tmp_kw_name_2;
    PyObject *tmp_metaclass_name_1;
    PyObject *tmp_outline_return_value_1;
    int tmp_res;
    bool tmp_result;
    PyObject *tmp_set_locals;
    PyObject *tmp_source_name_1;
    PyObject *tmp_tuple_element_1;
    static struct Nuitka_FrameObject *cache_frame_947b09ac8b6198e47b6d6f38883e9a08_2 = NULL;

    struct Nuitka_FrameObject *frame_947b09ac8b6198e47b6d6f38883e9a08_2;

    struct Nuitka_FrameObject *frame_e76bce2eca13a810ebdfe1a6cc216e6b;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    tmp_outline_return_value_1 = NULL;
    PyObject *locals_pandas$core$dtypes$generic_64 = NULL;

    // Module code.
    tmp_assign_source_1 = const_str_digest_607e52cf80d7e0fd83932301ca6c8266;
    UPDATE_STRING_DICT0( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    tmp_assign_source_2 = const_str_digest_99629574af3e9a60194722e67c76327f;
    UPDATE_STRING_DICT0( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    tmp_assign_source_3 = Py_None;
    UPDATE_STRING_DICT0( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    tmp_assign_source_4 = MAKE_FUNCTION_pandas$core$dtypes$generic$$$function_1_create_pandas_abc_type(  );
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain_create_pandas_abc_type, tmp_assign_source_4 );
    // Frame without reuse.
    frame_e76bce2eca13a810ebdfe1a6cc216e6b = MAKE_MODULE_FRAME( codeobj_e76bce2eca13a810ebdfe1a6cc216e6b, module_pandas$core$dtypes$generic );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_e76bce2eca13a810ebdfe1a6cc216e6b );
    assert( Py_REFCNT( frame_e76bce2eca13a810ebdfe1a6cc216e6b ) == 2 );

    // Framed code:
    tmp_called_name_1 = GET_STRING_DICT_VALUE( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain_create_pandas_abc_type );

    if (unlikely( tmp_called_name_1 == NULL ))
    {
        tmp_called_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_create_pandas_abc_type );
    }

    CHECK_OBJECT( tmp_called_name_1 );
    frame_e76bce2eca13a810ebdfe1a6cc216e6b->m_frame.f_lineno = 16;
    tmp_assign_source_5 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_1, &PyTuple_GET_ITEM( const_tuple_6155f4f0a36b0e20cfc83c9cfa52cf96_tuple, 0 ) );

    if ( tmp_assign_source_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 16;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain_ABCIndex, tmp_assign_source_5 );
    tmp_called_name_2 = GET_STRING_DICT_VALUE( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain_create_pandas_abc_type );

    if (unlikely( tmp_called_name_2 == NULL ))
    {
        tmp_called_name_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_create_pandas_abc_type );
    }

    if ( tmp_called_name_2 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "create_pandas_abc_type" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 17;

        goto frame_exception_exit_1;
    }

    frame_e76bce2eca13a810ebdfe1a6cc216e6b->m_frame.f_lineno = 17;
    tmp_assign_source_6 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_2, &PyTuple_GET_ITEM( const_tuple_498ec8f05822c48e89fb1a9ca5511e2c_tuple, 0 ) );

    if ( tmp_assign_source_6 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 17;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain_ABCInt64Index, tmp_assign_source_6 );
    tmp_called_name_3 = GET_STRING_DICT_VALUE( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain_create_pandas_abc_type );

    if (unlikely( tmp_called_name_3 == NULL ))
    {
        tmp_called_name_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_create_pandas_abc_type );
    }

    if ( tmp_called_name_3 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "create_pandas_abc_type" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 19;

        goto frame_exception_exit_1;
    }

    frame_e76bce2eca13a810ebdfe1a6cc216e6b->m_frame.f_lineno = 19;
    tmp_assign_source_7 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_3, &PyTuple_GET_ITEM( const_tuple_d542cd81ad9e787128b55a2524f2d1f6_tuple, 0 ) );

    if ( tmp_assign_source_7 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 19;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain_ABCUInt64Index, tmp_assign_source_7 );
    tmp_called_name_4 = GET_STRING_DICT_VALUE( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain_create_pandas_abc_type );

    if (unlikely( tmp_called_name_4 == NULL ))
    {
        tmp_called_name_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_create_pandas_abc_type );
    }

    if ( tmp_called_name_4 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "create_pandas_abc_type" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 21;

        goto frame_exception_exit_1;
    }

    frame_e76bce2eca13a810ebdfe1a6cc216e6b->m_frame.f_lineno = 21;
    tmp_assign_source_8 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_4, &PyTuple_GET_ITEM( const_tuple_a0b189240f62747b9388c5d9de57309a_tuple, 0 ) );

    if ( tmp_assign_source_8 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 21;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain_ABCRangeIndex, tmp_assign_source_8 );
    tmp_called_name_5 = GET_STRING_DICT_VALUE( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain_create_pandas_abc_type );

    if (unlikely( tmp_called_name_5 == NULL ))
    {
        tmp_called_name_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_create_pandas_abc_type );
    }

    if ( tmp_called_name_5 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "create_pandas_abc_type" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 23;

        goto frame_exception_exit_1;
    }

    frame_e76bce2eca13a810ebdfe1a6cc216e6b->m_frame.f_lineno = 23;
    tmp_assign_source_9 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_5, &PyTuple_GET_ITEM( const_tuple_e81beef27d536d6d71359d61bfd33781_tuple, 0 ) );

    if ( tmp_assign_source_9 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 23;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain_ABCFloat64Index, tmp_assign_source_9 );
    tmp_called_name_6 = GET_STRING_DICT_VALUE( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain_create_pandas_abc_type );

    if (unlikely( tmp_called_name_6 == NULL ))
    {
        tmp_called_name_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_create_pandas_abc_type );
    }

    if ( tmp_called_name_6 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "create_pandas_abc_type" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 25;

        goto frame_exception_exit_1;
    }

    frame_e76bce2eca13a810ebdfe1a6cc216e6b->m_frame.f_lineno = 25;
    tmp_assign_source_10 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_6, &PyTuple_GET_ITEM( const_tuple_ca7e708550972b62b9b04c08421c34a0_tuple, 0 ) );

    if ( tmp_assign_source_10 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 25;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain_ABCMultiIndex, tmp_assign_source_10 );
    tmp_called_name_7 = GET_STRING_DICT_VALUE( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain_create_pandas_abc_type );

    if (unlikely( tmp_called_name_7 == NULL ))
    {
        tmp_called_name_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_create_pandas_abc_type );
    }

    if ( tmp_called_name_7 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "create_pandas_abc_type" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 27;

        goto frame_exception_exit_1;
    }

    frame_e76bce2eca13a810ebdfe1a6cc216e6b->m_frame.f_lineno = 27;
    tmp_assign_source_11 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_7, &PyTuple_GET_ITEM( const_tuple_ca0e6f32803588f0a9f0cc22aa814733_tuple, 0 ) );

    if ( tmp_assign_source_11 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 27;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain_ABCDatetimeIndex, tmp_assign_source_11 );
    tmp_called_name_8 = GET_STRING_DICT_VALUE( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain_create_pandas_abc_type );

    if (unlikely( tmp_called_name_8 == NULL ))
    {
        tmp_called_name_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_create_pandas_abc_type );
    }

    if ( tmp_called_name_8 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "create_pandas_abc_type" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 29;

        goto frame_exception_exit_1;
    }

    frame_e76bce2eca13a810ebdfe1a6cc216e6b->m_frame.f_lineno = 29;
    tmp_assign_source_12 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_8, &PyTuple_GET_ITEM( const_tuple_088e7186a3be3fdab3964802df549ae3_tuple, 0 ) );

    if ( tmp_assign_source_12 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 29;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain_ABCTimedeltaIndex, tmp_assign_source_12 );
    tmp_called_name_9 = GET_STRING_DICT_VALUE( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain_create_pandas_abc_type );

    if (unlikely( tmp_called_name_9 == NULL ))
    {
        tmp_called_name_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_create_pandas_abc_type );
    }

    if ( tmp_called_name_9 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "create_pandas_abc_type" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 31;

        goto frame_exception_exit_1;
    }

    frame_e76bce2eca13a810ebdfe1a6cc216e6b->m_frame.f_lineno = 31;
    tmp_assign_source_13 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_9, &PyTuple_GET_ITEM( const_tuple_d4acdb449ce2c1fb373e321bbe598787_tuple, 0 ) );

    if ( tmp_assign_source_13 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 31;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain_ABCPeriodIndex, tmp_assign_source_13 );
    tmp_called_name_10 = GET_STRING_DICT_VALUE( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain_create_pandas_abc_type );

    if (unlikely( tmp_called_name_10 == NULL ))
    {
        tmp_called_name_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_create_pandas_abc_type );
    }

    if ( tmp_called_name_10 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "create_pandas_abc_type" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 33;

        goto frame_exception_exit_1;
    }

    frame_e76bce2eca13a810ebdfe1a6cc216e6b->m_frame.f_lineno = 33;
    tmp_assign_source_14 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_10, &PyTuple_GET_ITEM( const_tuple_ecfb4f82c2a82855e2b2efd7e568ab2f_tuple, 0 ) );

    if ( tmp_assign_source_14 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 33;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain_ABCCategoricalIndex, tmp_assign_source_14 );
    tmp_called_name_11 = GET_STRING_DICT_VALUE( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain_create_pandas_abc_type );

    if (unlikely( tmp_called_name_11 == NULL ))
    {
        tmp_called_name_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_create_pandas_abc_type );
    }

    if ( tmp_called_name_11 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "create_pandas_abc_type" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 35;

        goto frame_exception_exit_1;
    }

    frame_e76bce2eca13a810ebdfe1a6cc216e6b->m_frame.f_lineno = 35;
    tmp_assign_source_15 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_11, &PyTuple_GET_ITEM( const_tuple_d0c24e19633621eed623faa83836dd29_tuple, 0 ) );

    if ( tmp_assign_source_15 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 35;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain_ABCIntervalIndex, tmp_assign_source_15 );
    tmp_called_name_12 = GET_STRING_DICT_VALUE( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain_create_pandas_abc_type );

    if (unlikely( tmp_called_name_12 == NULL ))
    {
        tmp_called_name_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_create_pandas_abc_type );
    }

    if ( tmp_called_name_12 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "create_pandas_abc_type" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 37;

        goto frame_exception_exit_1;
    }

    frame_e76bce2eca13a810ebdfe1a6cc216e6b->m_frame.f_lineno = 37;
    tmp_assign_source_16 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_12, &PyTuple_GET_ITEM( const_tuple_1b417d1399297279333d7a632ca8c8a8_tuple, 0 ) );

    if ( tmp_assign_source_16 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 37;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain_ABCIndexClass, tmp_assign_source_16 );
    tmp_called_name_13 = GET_STRING_DICT_VALUE( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain_create_pandas_abc_type );

    if (unlikely( tmp_called_name_13 == NULL ))
    {
        tmp_called_name_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_create_pandas_abc_type );
    }

    if ( tmp_called_name_13 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "create_pandas_abc_type" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 44;

        goto frame_exception_exit_1;
    }

    frame_e76bce2eca13a810ebdfe1a6cc216e6b->m_frame.f_lineno = 44;
    tmp_assign_source_17 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_13, &PyTuple_GET_ITEM( const_tuple_55a06a97336d2526a52d514e01776b18_tuple, 0 ) );

    if ( tmp_assign_source_17 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 44;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain_ABCSeries, tmp_assign_source_17 );
    tmp_called_name_14 = GET_STRING_DICT_VALUE( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain_create_pandas_abc_type );

    if (unlikely( tmp_called_name_14 == NULL ))
    {
        tmp_called_name_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_create_pandas_abc_type );
    }

    if ( tmp_called_name_14 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "create_pandas_abc_type" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 45;

        goto frame_exception_exit_1;
    }

    frame_e76bce2eca13a810ebdfe1a6cc216e6b->m_frame.f_lineno = 45;
    tmp_assign_source_18 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_14, &PyTuple_GET_ITEM( const_tuple_3a2ca9a72da6328235c2c7b989760719_tuple, 0 ) );

    if ( tmp_assign_source_18 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 45;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain_ABCDataFrame, tmp_assign_source_18 );
    tmp_called_name_15 = GET_STRING_DICT_VALUE( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain_create_pandas_abc_type );

    if (unlikely( tmp_called_name_15 == NULL ))
    {
        tmp_called_name_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_create_pandas_abc_type );
    }

    if ( tmp_called_name_15 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "create_pandas_abc_type" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 46;

        goto frame_exception_exit_1;
    }

    frame_e76bce2eca13a810ebdfe1a6cc216e6b->m_frame.f_lineno = 46;
    tmp_assign_source_19 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_15, &PyTuple_GET_ITEM( const_tuple_cdd89e57eb924c02f17f1cf30aaeceaa_tuple, 0 ) );

    if ( tmp_assign_source_19 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 46;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain_ABCSparseDataFrame, tmp_assign_source_19 );
    tmp_called_name_16 = GET_STRING_DICT_VALUE( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain_create_pandas_abc_type );

    if (unlikely( tmp_called_name_16 == NULL ))
    {
        tmp_called_name_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_create_pandas_abc_type );
    }

    if ( tmp_called_name_16 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "create_pandas_abc_type" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 48;

        goto frame_exception_exit_1;
    }

    frame_e76bce2eca13a810ebdfe1a6cc216e6b->m_frame.f_lineno = 48;
    tmp_assign_source_20 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_16, &PyTuple_GET_ITEM( const_tuple_bc4cb33cd97b64ecf0d05ff12856b10f_tuple, 0 ) );

    if ( tmp_assign_source_20 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 48;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain_ABCPanel, tmp_assign_source_20 );
    tmp_called_name_17 = GET_STRING_DICT_VALUE( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain_create_pandas_abc_type );

    if (unlikely( tmp_called_name_17 == NULL ))
    {
        tmp_called_name_17 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_create_pandas_abc_type );
    }

    if ( tmp_called_name_17 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "create_pandas_abc_type" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 49;

        goto frame_exception_exit_1;
    }

    frame_e76bce2eca13a810ebdfe1a6cc216e6b->m_frame.f_lineno = 49;
    tmp_assign_source_21 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_17, &PyTuple_GET_ITEM( const_tuple_1399a59b28e589531450d810aa4197a4_tuple, 0 ) );

    if ( tmp_assign_source_21 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 49;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain_ABCSparseSeries, tmp_assign_source_21 );
    tmp_called_name_18 = GET_STRING_DICT_VALUE( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain_create_pandas_abc_type );

    if (unlikely( tmp_called_name_18 == NULL ))
    {
        tmp_called_name_18 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_create_pandas_abc_type );
    }

    if ( tmp_called_name_18 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "create_pandas_abc_type" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 52;

        goto frame_exception_exit_1;
    }

    frame_e76bce2eca13a810ebdfe1a6cc216e6b->m_frame.f_lineno = 52;
    tmp_assign_source_22 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_18, &PyTuple_GET_ITEM( const_tuple_8cd4cfe29da562f7e2f4c3865abeabd1_tuple, 0 ) );

    if ( tmp_assign_source_22 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 52;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain_ABCSparseArray, tmp_assign_source_22 );
    tmp_called_name_19 = GET_STRING_DICT_VALUE( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain_create_pandas_abc_type );

    if (unlikely( tmp_called_name_19 == NULL ))
    {
        tmp_called_name_19 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_create_pandas_abc_type );
    }

    if ( tmp_called_name_19 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "create_pandas_abc_type" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 54;

        goto frame_exception_exit_1;
    }

    frame_e76bce2eca13a810ebdfe1a6cc216e6b->m_frame.f_lineno = 54;
    tmp_assign_source_23 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_19, &PyTuple_GET_ITEM( const_tuple_a9bd744d56f40fb125578446e1149015_tuple, 0 ) );

    if ( tmp_assign_source_23 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 54;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain_ABCCategorical, tmp_assign_source_23 );
    tmp_called_name_20 = GET_STRING_DICT_VALUE( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain_create_pandas_abc_type );

    if (unlikely( tmp_called_name_20 == NULL ))
    {
        tmp_called_name_20 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_create_pandas_abc_type );
    }

    if ( tmp_called_name_20 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "create_pandas_abc_type" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 56;

        goto frame_exception_exit_1;
    }

    frame_e76bce2eca13a810ebdfe1a6cc216e6b->m_frame.f_lineno = 56;
    tmp_assign_source_24 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_20, &PyTuple_GET_ITEM( const_tuple_ee46e31b6dd9a260cbcf686ed7e1d180_tuple, 0 ) );

    if ( tmp_assign_source_24 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 56;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain_ABCPeriod, tmp_assign_source_24 );
    tmp_called_name_21 = GET_STRING_DICT_VALUE( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain_create_pandas_abc_type );

    if (unlikely( tmp_called_name_21 == NULL ))
    {
        tmp_called_name_21 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_create_pandas_abc_type );
    }

    if ( tmp_called_name_21 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "create_pandas_abc_type" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 57;

        goto frame_exception_exit_1;
    }

    frame_e76bce2eca13a810ebdfe1a6cc216e6b->m_frame.f_lineno = 57;
    tmp_assign_source_25 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_21, &PyTuple_GET_ITEM( const_tuple_1cbbae3c47881c7d89f542e5c3eab677_tuple, 0 ) );

    if ( tmp_assign_source_25 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 57;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain_ABCDateOffset, tmp_assign_source_25 );
    tmp_called_name_22 = GET_STRING_DICT_VALUE( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain_create_pandas_abc_type );

    if (unlikely( tmp_called_name_22 == NULL ))
    {
        tmp_called_name_22 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_create_pandas_abc_type );
    }

    if ( tmp_called_name_22 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "create_pandas_abc_type" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 59;

        goto frame_exception_exit_1;
    }

    frame_e76bce2eca13a810ebdfe1a6cc216e6b->m_frame.f_lineno = 59;
    tmp_assign_source_26 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_22, &PyTuple_GET_ITEM( const_tuple_e59941f973d0c3f39d45902b6fb98a97_tuple, 0 ) );

    if ( tmp_assign_source_26 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 59;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain_ABCInterval, tmp_assign_source_26 );
    tmp_called_name_23 = GET_STRING_DICT_VALUE( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain_create_pandas_abc_type );

    if (unlikely( tmp_called_name_23 == NULL ))
    {
        tmp_called_name_23 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_create_pandas_abc_type );
    }

    if ( tmp_called_name_23 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "create_pandas_abc_type" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 60;

        goto frame_exception_exit_1;
    }

    frame_e76bce2eca13a810ebdfe1a6cc216e6b->m_frame.f_lineno = 60;
    tmp_assign_source_27 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_23, &PyTuple_GET_ITEM( const_tuple_dd895ef83b60eefcd25e7374010c2cae_tuple, 0 ) );

    if ( tmp_assign_source_27 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 60;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain_ABCExtensionArray, tmp_assign_source_27 );
    tmp_assign_source_28 = PyDict_New();
    assert( tmp_class_creation_1__class_decl_dict == NULL );
    tmp_class_creation_1__class_decl_dict = tmp_assign_source_28;

    // Tried code:
    tmp_key_name_1 = const_str_plain_metaclass;
    tmp_dict_name_1 = tmp_class_creation_1__class_decl_dict;

    CHECK_OBJECT( tmp_dict_name_1 );
    tmp_res = PyDict_Contains( tmp_dict_name_1, tmp_key_name_1 );
    if ( tmp_res == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 64;

        goto try_except_handler_1;
    }
    tmp_cond_value_1 = BOOL_FROM( tmp_res == 1 );
    tmp_cond_truth_1 = CHECK_IF_TRUE( tmp_cond_value_1 );
    if ( tmp_cond_truth_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 64;

        goto try_except_handler_1;
    }
    if ( tmp_cond_truth_1 == 1 )
    {
        goto condexpr_true_1;
    }
    else
    {
        goto condexpr_false_1;
    }
    condexpr_true_1:;
    tmp_dict_name_2 = tmp_class_creation_1__class_decl_dict;

    CHECK_OBJECT( tmp_dict_name_2 );
    tmp_key_name_2 = const_str_plain_metaclass;
    tmp_metaclass_name_1 = DICT_GET_ITEM( tmp_dict_name_2, tmp_key_name_2 );
    if ( tmp_metaclass_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 64;

        goto try_except_handler_1;
    }
    goto condexpr_end_1;
    condexpr_false_1:;
    tmp_metaclass_name_1 = LOOKUP_BUILTIN( const_str_plain_type );
    assert( tmp_metaclass_name_1 != NULL );
    Py_INCREF( tmp_metaclass_name_1 );
    condexpr_end_1:;
    tmp_bases_name_1 = const_tuple_type_type_tuple;
    tmp_assign_source_29 = SELECT_METACLASS( tmp_metaclass_name_1, tmp_bases_name_1 );
    Py_DECREF( tmp_metaclass_name_1 );
    if ( tmp_assign_source_29 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 64;

        goto try_except_handler_1;
    }
    assert( tmp_class_creation_1__metaclass == NULL );
    tmp_class_creation_1__metaclass = tmp_assign_source_29;

    tmp_key_name_3 = const_str_plain_metaclass;
    tmp_dict_name_3 = tmp_class_creation_1__class_decl_dict;

    CHECK_OBJECT( tmp_dict_name_3 );
    tmp_res = PyDict_Contains( tmp_dict_name_3, tmp_key_name_3 );
    if ( tmp_res == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 64;

        goto try_except_handler_1;
    }
    tmp_cond_value_2 = BOOL_FROM( tmp_res == 1 );
    tmp_cond_truth_2 = CHECK_IF_TRUE( tmp_cond_value_2 );
    if ( tmp_cond_truth_2 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 64;

        goto try_except_handler_1;
    }
    if ( tmp_cond_truth_2 == 1 )
    {
        goto branch_yes_1;
    }
    else
    {
        goto branch_no_1;
    }
    branch_yes_1:;
    tmp_dictdel_dict = tmp_class_creation_1__class_decl_dict;

    CHECK_OBJECT( tmp_dictdel_dict );
    tmp_dictdel_key = const_str_plain_metaclass;
    tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 64;

        goto try_except_handler_1;
    }
    branch_no_1:;
    tmp_hasattr_source_1 = tmp_class_creation_1__metaclass;

    CHECK_OBJECT( tmp_hasattr_source_1 );
    tmp_hasattr_attr_1 = const_str_plain___prepare__;
    tmp_res = PyObject_HasAttr( tmp_hasattr_source_1, tmp_hasattr_attr_1 );
    if ( tmp_res == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 64;

        goto try_except_handler_1;
    }
    if ( tmp_res == 1 )
    {
        goto condexpr_true_2;
    }
    else
    {
        goto condexpr_false_2;
    }
    condexpr_true_2:;
    tmp_source_name_1 = tmp_class_creation_1__metaclass;

    CHECK_OBJECT( tmp_source_name_1 );
    tmp_called_name_24 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___prepare__ );
    if ( tmp_called_name_24 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 64;

        goto try_except_handler_1;
    }
    tmp_args_name_1 = const_tuple_str_plain__ABCGeneric_tuple_type_type_tuple_tuple;
    tmp_kw_name_1 = tmp_class_creation_1__class_decl_dict;

    CHECK_OBJECT( tmp_kw_name_1 );
    frame_e76bce2eca13a810ebdfe1a6cc216e6b->m_frame.f_lineno = 64;
    tmp_assign_source_30 = CALL_FUNCTION( tmp_called_name_24, tmp_args_name_1, tmp_kw_name_1 );
    Py_DECREF( tmp_called_name_24 );
    if ( tmp_assign_source_30 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 64;

        goto try_except_handler_1;
    }
    goto condexpr_end_2;
    condexpr_false_2:;
    tmp_assign_source_30 = PyDict_New();
    condexpr_end_2:;
    assert( tmp_class_creation_1__prepared == NULL );
    tmp_class_creation_1__prepared = tmp_assign_source_30;

    tmp_set_locals = tmp_class_creation_1__prepared;

    CHECK_OBJECT( tmp_set_locals );
    locals_pandas$core$dtypes$generic_64 = tmp_set_locals;
    Py_INCREF( tmp_set_locals );
    // Tried code:
    // Tried code:
    tmp_dictset_value = const_str_digest_30235a8809db2818daf1f4bd1339d3df;
    tmp_res = PyObject_SetItem( locals_pandas$core$dtypes$generic_64, const_str_plain___module__, tmp_dictset_value );
    if ( tmp_res != 0 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 64;

        goto try_except_handler_3;
    }
    tmp_dictset_value = const_str_plain__ABCGeneric;
    tmp_res = PyObject_SetItem( locals_pandas$core$dtypes$generic_64, const_str_plain___qualname__, tmp_dictset_value );
    if ( tmp_res != 0 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 64;

        goto try_except_handler_3;
    }
    MAKE_OR_REUSE_FRAME( cache_frame_947b09ac8b6198e47b6d6f38883e9a08_2, codeobj_947b09ac8b6198e47b6d6f38883e9a08, module_pandas$core$dtypes$generic, sizeof(void *) );
    frame_947b09ac8b6198e47b6d6f38883e9a08_2 = cache_frame_947b09ac8b6198e47b6d6f38883e9a08_2;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_947b09ac8b6198e47b6d6f38883e9a08_2 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_947b09ac8b6198e47b6d6f38883e9a08_2 ) == 2 ); // Frame stack

    // Framed code:
    tmp_dictset_value = MAKE_FUNCTION_pandas$core$dtypes$generic$$$function_2___instancecheck__(  );
    tmp_res = PyObject_SetItem( locals_pandas$core$dtypes$generic_64, const_str_plain___instancecheck__, tmp_dictset_value );
    Py_DECREF( tmp_dictset_value );
    if ( tmp_res != 0 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 66;
        type_description_2 = "N";
        goto frame_exception_exit_2;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_947b09ac8b6198e47b6d6f38883e9a08_2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_2:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_947b09ac8b6198e47b6d6f38883e9a08_2 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_947b09ac8b6198e47b6d6f38883e9a08_2, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_947b09ac8b6198e47b6d6f38883e9a08_2->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_947b09ac8b6198e47b6d6f38883e9a08_2, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_947b09ac8b6198e47b6d6f38883e9a08_2,
        type_description_2,
        NULL
    );


    // Release cached frame.
    if ( frame_947b09ac8b6198e47b6d6f38883e9a08_2 == cache_frame_947b09ac8b6198e47b6d6f38883e9a08_2 )
    {
        Py_DECREF( frame_947b09ac8b6198e47b6d6f38883e9a08_2 );
    }
    cache_frame_947b09ac8b6198e47b6d6f38883e9a08_2 = NULL;

    assertFrameObject( frame_947b09ac8b6198e47b6d6f38883e9a08_2 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto nested_frame_exit_1;

    frame_no_exception_1:;

    goto skip_nested_handling_1;
    nested_frame_exit_1:;

    goto try_except_handler_3;
    skip_nested_handling_1:;
    tmp_called_name_25 = tmp_class_creation_1__metaclass;

    CHECK_OBJECT( tmp_called_name_25 );
    tmp_tuple_element_1 = const_str_plain__ABCGeneric;
    tmp_args_name_2 = PyTuple_New( 3 );
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_1 );
    tmp_tuple_element_1 = const_tuple_type_type_tuple;
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_1 );
    tmp_tuple_element_1 = locals_pandas$core$dtypes$generic_64;
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_args_name_2, 2, tmp_tuple_element_1 );
    tmp_kw_name_2 = tmp_class_creation_1__class_decl_dict;

    CHECK_OBJECT( tmp_kw_name_2 );
    frame_e76bce2eca13a810ebdfe1a6cc216e6b->m_frame.f_lineno = 64;
    tmp_assign_source_32 = CALL_FUNCTION( tmp_called_name_25, tmp_args_name_2, tmp_kw_name_2 );
    Py_DECREF( tmp_args_name_2 );
    if ( tmp_assign_source_32 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 64;

        goto try_except_handler_3;
    }
    assert( outline_0_var___class__ == NULL );
    outline_0_var___class__ = tmp_assign_source_32;

    tmp_outline_return_value_1 = outline_0_var___class__;

    CHECK_OBJECT( tmp_outline_return_value_1 );
    Py_INCREF( tmp_outline_return_value_1 );
    goto try_return_handler_3;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$core$dtypes$generic );
    return MOD_RETURN_VALUE( NULL );
    // Return handler code:
    try_return_handler_3:;
    Py_DECREF( locals_pandas$core$dtypes$generic_64 );
    locals_pandas$core$dtypes$generic_64 = NULL;
    goto try_return_handler_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_DECREF( locals_pandas$core$dtypes$generic_64 );
    locals_pandas$core$dtypes$generic_64 = NULL;
    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_2;
    // End of try:
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$core$dtypes$generic );
    return MOD_RETURN_VALUE( NULL );
    // Return handler code:
    try_return_handler_2:;
    CHECK_OBJECT( (PyObject *)outline_0_var___class__ );
    Py_DECREF( outline_0_var___class__ );
    outline_0_var___class__ = NULL;

    goto outline_result_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto outline_exception_1;
    // End of try:
    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$core$dtypes$generic );
    return MOD_RETURN_VALUE( NULL );
    outline_exception_1:;
    exception_lineno = 64;
    goto try_except_handler_1;
    outline_result_1:;
    tmp_assign_source_31 = tmp_outline_return_value_1;
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain__ABCGeneric, tmp_assign_source_31 );
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_decl_dict );
    Py_DECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_decl_dict );
    Py_DECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__metaclass );
    Py_DECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__prepared );
    Py_DECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    tmp_called_name_26 = GET_STRING_DICT_VALUE( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain__ABCGeneric );

    if (unlikely( tmp_called_name_26 == NULL ))
    {
        tmp_called_name_26 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__ABCGeneric );
    }

    if ( tmp_called_name_26 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_ABCGeneric" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 70;

        goto frame_exception_exit_1;
    }

    tmp_call_arg_element_1 = const_str_plain_ABCGeneric;
    tmp_call_arg_element_2 = const_tuple_empty;
    tmp_call_arg_element_3 = PyDict_New();
    frame_e76bce2eca13a810ebdfe1a6cc216e6b->m_frame.f_lineno = 70;
    {
        PyObject *call_args[] = { tmp_call_arg_element_1, tmp_call_arg_element_2, tmp_call_arg_element_3 };
        tmp_assign_source_33 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_26, call_args );
    }

    Py_DECREF( tmp_call_arg_element_3 );
    if ( tmp_assign_source_33 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 70;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$generic, (Nuitka_StringObject *)const_str_plain_ABCGeneric, tmp_assign_source_33 );

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_e76bce2eca13a810ebdfe1a6cc216e6b );
#endif
    popFrameStack();

    assertFrameObject( frame_e76bce2eca13a810ebdfe1a6cc216e6b );

    goto frame_no_exception_2;
    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_e76bce2eca13a810ebdfe1a6cc216e6b );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_e76bce2eca13a810ebdfe1a6cc216e6b, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_e76bce2eca13a810ebdfe1a6cc216e6b->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_e76bce2eca13a810ebdfe1a6cc216e6b, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;
    frame_no_exception_2:;

    return MOD_RETURN_VALUE( module_pandas$core$dtypes$generic );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
