/* Generated code for Python source for module 'pandas.core.dtypes.api'
 * created by Nuitka version 0.5.32.7
 *
 * This code is in part copyright 2018 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The _module_pandas$core$dtypes$api is a Python object pointer of module type. */

/* Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_pandas$core$dtypes$api;
PyDictObject *moduledict_pandas$core$dtypes$api;

/* The module constants used, if any. */
extern PyObject *const_str_plain_warn;
static PyObject *const_str_digest_3e4d71669b7092cee01a8820de818348;
extern PyObject *const_str_plain_is_datetimetz;
extern PyObject *const_str_plain_is_any_int_dtype;
extern PyObject *const_str_plain_stacklevel;
extern PyObject *const_str_plain_is_dict_like;
static PyObject *const_tuple_e919463f23025bf7d7a98538876bf5fc_tuple;
extern PyObject *const_str_plain_is_re;
extern PyObject *const_str_plain_sys;
extern PyObject *const_str_plain_is_integer;
extern PyObject *const_str_plain_is_dtype_equal;
extern PyObject *const_str_plain_warnings;
extern PyObject *const_str_plain_is_datetime64_dtype;
extern PyObject *const_str_plain_is_float_dtype;
extern PyObject *const_str_plain_outer;
extern PyObject *const_str_digest_d2cd9395054004c1a53d2c95d5a85417;
extern PyObject *const_str_plain_is_list_like;
extern PyObject *const_int_pos_1;
extern PyObject *const_str_plain_t;
extern PyObject *const_str_plain___file__;
extern PyObject *const_str_plain_is_period;
extern PyObject *const_str_plain_is_interval;
extern PyObject *const_str_plain_is_float;
extern PyObject *const_str_plain_is_interval_dtype;
extern PyObject *const_str_plain_is_re_compilable;
static PyObject *const_str_digest_1ddfb9d43522ced31b9b1d922a45a78e;
extern PyObject *const_str_plain_is_object_dtype;
extern PyObject *const_str_plain_is_unsigned_integer_dtype;
extern PyObject *const_int_0;
extern PyObject *const_str_plain_arr_or_dtype;
extern PyObject *const_str_plain_is_categorical_dtype;
extern PyObject *const_str_plain_is_categorical;
extern PyObject *const_str_plain_is_string_dtype;
extern PyObject *const_str_plain_is_named_tuple;
extern PyObject *const_int_pos_3;
extern PyObject *const_str_plain_is_int64_dtype;
extern PyObject *const_tuple_str_plain_t_str_plain_wrapper_tuple;
extern PyObject *const_str_plain_is_datetime64_ns_dtype;
extern PyObject *const_str_plain_is_array_like;
extern PyObject *const_dict_2254aca578e492e580f4e46a40647b9b;
extern PyObject *const_str_plain_pandas_dtype;
extern PyObject *const_str_plain_is_timedelta64_ns_dtype;
extern PyObject *const_str_plain_is_integer_dtype;
extern PyObject *const_str_plain_is_numeric_dtype;
extern PyObject *const_str_plain_is_complex;
extern PyObject *const_str_plain_is_datetime64tz_dtype;
extern PyObject *const_str_plain_m;
extern PyObject *const_str_plain_core;
extern PyObject *const_str_plain_is_floating_dtype;
extern PyObject *const_str_plain_modules;
extern PyObject *const_str_plain_is_period_dtype;
extern PyObject *const_str_plain_is_sparse;
extern PyObject *const_str_plain_is_complex_dtype;
extern PyObject *const_str_plain_is_sequence;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_plain_is_extension_type;
extern PyObject *const_str_plain_is_number;
extern PyObject *const_str_plain_is_scalar;
extern PyObject *const_str_digest_af80ff60d7780e61f6ad18fc641b6dcd;
extern PyObject *const_str_plain_is_datetime64_any_dtype;
extern PyObject *const_str_plain_is_hashable;
extern PyObject *const_str_plain_is_file_like;
static PyObject *const_str_digest_caaf0c340725b8bb04de3524c058bf15;
extern PyObject *const_str_plain_format;
extern PyObject *const_str_plain_FutureWarning;
extern PyObject *const_str_plain_common;
extern PyObject *const_str_plain_is_iterator;
extern PyObject *const_str_plain_wrapper;
extern PyObject *const_str_plain_is_bool_dtype;
extern PyObject *const_str_plain_pandas;
static PyObject *const_tuple_285d063b90205d31a81f51ed5fcd3567_tuple;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_plain_is_bool;
extern PyObject *const_str_plain_is_timedelta64_dtype;
extern PyObject *const_str_plain_is_signed_integer_dtype;
static PyObject *const_tuple_5c64004afbc3ee9460abd7403b0ac1d7_tuple;
extern PyObject *const_str_plain_dtypes;
static PyObject *module_filename_obj;

static bool constants_created = false;

static void createModuleConstants( void )
{
    const_str_digest_3e4d71669b7092cee01a8820de818348 = UNSTREAM_STRING( &constant_bin[ 1861888 ], 88, 0 );
    const_tuple_e919463f23025bf7d7a98538876bf5fc_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_e919463f23025bf7d7a98538876bf5fc_tuple, 0, const_str_plain_is_any_int_dtype ); Py_INCREF( const_str_plain_is_any_int_dtype );
    PyTuple_SET_ITEM( const_tuple_e919463f23025bf7d7a98538876bf5fc_tuple, 1, const_str_plain_is_floating_dtype ); Py_INCREF( const_str_plain_is_floating_dtype );
    PyTuple_SET_ITEM( const_tuple_e919463f23025bf7d7a98538876bf5fc_tuple, 2, const_str_plain_is_sequence ); Py_INCREF( const_str_plain_is_sequence );
    const_str_digest_1ddfb9d43522ced31b9b1d922a45a78e = UNSTREAM_STRING( &constant_bin[ 1861976 ], 31, 0 );
    const_str_digest_caaf0c340725b8bb04de3524c058bf15 = UNSTREAM_STRING( &constant_bin[ 1862007 ], 57, 0 );
    const_tuple_285d063b90205d31a81f51ed5fcd3567_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_285d063b90205d31a81f51ed5fcd3567_tuple, 0, const_str_plain_arr_or_dtype ); Py_INCREF( const_str_plain_arr_or_dtype );
    PyTuple_SET_ITEM( const_tuple_285d063b90205d31a81f51ed5fcd3567_tuple, 1, const_str_plain_warnings ); Py_INCREF( const_str_plain_warnings );
    PyTuple_SET_ITEM( const_tuple_285d063b90205d31a81f51ed5fcd3567_tuple, 2, const_str_plain_pandas ); Py_INCREF( const_str_plain_pandas );
    PyTuple_SET_ITEM( const_tuple_285d063b90205d31a81f51ed5fcd3567_tuple, 3, const_str_plain_t ); Py_INCREF( const_str_plain_t );
    const_tuple_5c64004afbc3ee9460abd7403b0ac1d7_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1862064 ], 696 );

    constants_created = true;
}

#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_pandas$core$dtypes$api( void )
{
    // The module may not have been used at all.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_82ff4d9d47145c79eeb3041a01a24922;
static PyCodeObject *codeobj_0913e72b9f2e6a6ec9f95b805899db27;
static PyCodeObject *codeobj_6b65ce304f5584aad0d68ba3d57aa0fc;

static void createModuleCodeObjects(void)
{
    module_filename_obj = const_str_digest_3e4d71669b7092cee01a8820de818348;
    codeobj_82ff4d9d47145c79eeb3041a01a24922 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_1ddfb9d43522ced31b9b1d922a45a78e, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_0913e72b9f2e6a6ec9f95b805899db27 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_outer, 69, const_tuple_str_plain_t_str_plain_wrapper_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_6b65ce304f5584aad0d68ba3d57aa0fc = MAKE_CODEOBJ( module_filename_obj, const_str_plain_wrapper, 71, const_tuple_285d063b90205d31a81f51ed5fcd3567_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS );
}

// The module function declarations.
static PyObject *MAKE_FUNCTION_pandas$core$dtypes$api$$$function_1_outer( PyObject *defaults );


static PyObject *MAKE_FUNCTION_pandas$core$dtypes$api$$$function_1_outer$$$function_1_wrapper( struct Nuitka_CellObject *closure_t );


// The module function definitions.
static PyObject *impl_pandas$core$dtypes$api$$$function_1_outer( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_t = PyCell_NEW1( python_pars[ 0 ] );
    PyObject *var_wrapper = NULL;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_return_value;
    tmp_return_value = NULL;

    // Actual function code.
    tmp_assign_source_1 = MAKE_FUNCTION_pandas$core$dtypes$api$$$function_1_outer$$$function_1_wrapper( par_t );
    assert( var_wrapper == NULL );
    var_wrapper = tmp_assign_source_1;

    // Tried code:
    tmp_return_value = var_wrapper;

    CHECK_OBJECT( tmp_return_value );
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$core$dtypes$api$$$function_1_outer );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_t );
    Py_DECREF( par_t );
    par_t = NULL;

    Py_XDECREF( var_wrapper );
    var_wrapper = NULL;

    goto function_return_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$core$dtypes$api$$$function_1_outer );
    return NULL;

function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$core$dtypes$api$$$function_1_outer$$$function_1_wrapper( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_arr_or_dtype = python_pars[ 0 ];
    PyObject *var_warnings = NULL;
    PyObject *var_pandas = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_args_element_name_1;
    PyObject *tmp_args_name_1;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_called_name_1;
    PyObject *tmp_called_name_2;
    PyObject *tmp_called_name_3;
    PyObject *tmp_dict_key_1;
    PyObject *tmp_dict_value_1;
    PyObject *tmp_fromlist_name_1;
    PyObject *tmp_fromlist_name_2;
    PyObject *tmp_getattr_attr_1;
    PyObject *tmp_getattr_target_1;
    PyObject *tmp_globals_name_1;
    PyObject *tmp_globals_name_2;
    PyObject *tmp_kw_name_1;
    PyObject *tmp_kw_name_2;
    PyObject *tmp_level_name_1;
    PyObject *tmp_level_name_2;
    PyObject *tmp_locals_name_1;
    PyObject *tmp_locals_name_2;
    PyObject *tmp_name_name_1;
    PyObject *tmp_name_name_2;
    int tmp_res;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    PyObject *tmp_source_name_2;
    PyObject *tmp_source_name_3;
    PyObject *tmp_source_name_4;
    PyObject *tmp_source_name_5;
    PyObject *tmp_tuple_element_1;
    NUITKA_MAY_BE_UNUSED PyObject *tmp_unused;
    static struct Nuitka_FrameObject *cache_frame_6b65ce304f5584aad0d68ba3d57aa0fc = NULL;

    struct Nuitka_FrameObject *frame_6b65ce304f5584aad0d68ba3d57aa0fc;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_6b65ce304f5584aad0d68ba3d57aa0fc, codeobj_6b65ce304f5584aad0d68ba3d57aa0fc, module_pandas$core$dtypes$api, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_6b65ce304f5584aad0d68ba3d57aa0fc = cache_frame_6b65ce304f5584aad0d68ba3d57aa0fc;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_6b65ce304f5584aad0d68ba3d57aa0fc );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_6b65ce304f5584aad0d68ba3d57aa0fc ) == 2 ); // Frame stack

    // Framed code:
    tmp_name_name_1 = const_str_plain_warnings;
    tmp_globals_name_1 = (PyObject *)moduledict_pandas$core$dtypes$api;
    tmp_locals_name_1 = Py_None;
    tmp_fromlist_name_1 = Py_None;
    tmp_level_name_1 = const_int_0;
    frame_6b65ce304f5584aad0d68ba3d57aa0fc->m_frame.f_lineno = 72;
    tmp_assign_source_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
    if ( tmp_assign_source_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 72;
        type_description_1 = "oooc";
        goto frame_exception_exit_1;
    }
    assert( var_warnings == NULL );
    var_warnings = tmp_assign_source_1;

    tmp_name_name_2 = const_str_plain_pandas;
    tmp_globals_name_2 = (PyObject *)moduledict_pandas$core$dtypes$api;
    tmp_locals_name_2 = Py_None;
    tmp_fromlist_name_2 = Py_None;
    tmp_level_name_2 = const_int_0;
    frame_6b65ce304f5584aad0d68ba3d57aa0fc->m_frame.f_lineno = 73;
    tmp_assign_source_2 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
    if ( tmp_assign_source_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 73;
        type_description_1 = "oooc";
        goto frame_exception_exit_1;
    }
    assert( var_pandas == NULL );
    var_pandas = tmp_assign_source_2;

    tmp_source_name_1 = var_warnings;

    CHECK_OBJECT( tmp_source_name_1 );
    tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_warn );
    if ( tmp_called_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 74;
        type_description_1 = "oooc";
        goto frame_exception_exit_1;
    }
    tmp_source_name_2 = const_str_digest_caaf0c340725b8bb04de3524c058bf15;
    tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_format );
    assert( !(tmp_called_name_2 == NULL) );
    tmp_dict_key_1 = const_str_plain_t;
    if ( self->m_closure[0] == NULL )
    {
        tmp_dict_value_1 = NULL;
    }
    else
    {
        tmp_dict_value_1 = PyCell_GET( self->m_closure[0] );
    }

    if ( tmp_dict_value_1 == NULL )
    {
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_called_name_2 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "t" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 75;
        type_description_1 = "oooc";
        goto frame_exception_exit_1;
    }

    tmp_kw_name_1 = _PyDict_NewPresized( 1 );
    tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
    assert( !(tmp_res != 0) );
    frame_6b65ce304f5584aad0d68ba3d57aa0fc->m_frame.f_lineno = 74;
    tmp_tuple_element_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_2, tmp_kw_name_1 );
    Py_DECREF( tmp_called_name_2 );
    Py_DECREF( tmp_kw_name_1 );
    if ( tmp_tuple_element_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_1 );

        exception_lineno = 74;
        type_description_1 = "oooc";
        goto frame_exception_exit_1;
    }
    tmp_args_name_1 = PyTuple_New( 2 );
    PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
    tmp_tuple_element_1 = GET_STRING_DICT_VALUE( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain_FutureWarning );

    if (unlikely( tmp_tuple_element_1 == NULL ))
    {
        tmp_tuple_element_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_FutureWarning );
    }

    if ( tmp_tuple_element_1 == NULL )
    {
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_name_1 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "FutureWarning" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 76;
        type_description_1 = "oooc";
        goto frame_exception_exit_1;
    }

    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_1 );
    tmp_kw_name_2 = PyDict_Copy( const_dict_2254aca578e492e580f4e46a40647b9b );
    frame_6b65ce304f5584aad0d68ba3d57aa0fc->m_frame.f_lineno = 74;
    tmp_unused = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_2 );
    Py_DECREF( tmp_called_name_1 );
    Py_DECREF( tmp_args_name_1 );
    Py_DECREF( tmp_kw_name_2 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 74;
        type_description_1 = "oooc";
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_unused );
    tmp_source_name_5 = var_pandas;

    if ( tmp_source_name_5 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "pandas" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 77;
        type_description_1 = "oooc";
        goto frame_exception_exit_1;
    }

    tmp_source_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_core );
    if ( tmp_source_name_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 77;
        type_description_1 = "oooc";
        goto frame_exception_exit_1;
    }
    tmp_source_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_dtypes );
    Py_DECREF( tmp_source_name_4 );
    if ( tmp_source_name_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 77;
        type_description_1 = "oooc";
        goto frame_exception_exit_1;
    }
    tmp_getattr_target_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_common );
    Py_DECREF( tmp_source_name_3 );
    if ( tmp_getattr_target_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 77;
        type_description_1 = "oooc";
        goto frame_exception_exit_1;
    }
    if ( self->m_closure[0] == NULL )
    {
        tmp_getattr_attr_1 = NULL;
    }
    else
    {
        tmp_getattr_attr_1 = PyCell_GET( self->m_closure[0] );
    }

    if ( tmp_getattr_attr_1 == NULL )
    {
        Py_DECREF( tmp_getattr_target_1 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "t" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 77;
        type_description_1 = "oooc";
        goto frame_exception_exit_1;
    }

    tmp_called_name_3 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, NULL );
    Py_DECREF( tmp_getattr_target_1 );
    if ( tmp_called_name_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 77;
        type_description_1 = "oooc";
        goto frame_exception_exit_1;
    }
    tmp_args_element_name_1 = par_arr_or_dtype;

    if ( tmp_args_element_name_1 == NULL )
    {
        Py_DECREF( tmp_called_name_3 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "arr_or_dtype" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 77;
        type_description_1 = "oooc";
        goto frame_exception_exit_1;
    }

    frame_6b65ce304f5584aad0d68ba3d57aa0fc->m_frame.f_lineno = 77;
    {
        PyObject *call_args[] = { tmp_args_element_name_1 };
        tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
    }

    Py_DECREF( tmp_called_name_3 );
    if ( tmp_return_value == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 77;
        type_description_1 = "oooc";
        goto frame_exception_exit_1;
    }
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6b65ce304f5584aad0d68ba3d57aa0fc );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_6b65ce304f5584aad0d68ba3d57aa0fc );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6b65ce304f5584aad0d68ba3d57aa0fc );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_6b65ce304f5584aad0d68ba3d57aa0fc, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_6b65ce304f5584aad0d68ba3d57aa0fc->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_6b65ce304f5584aad0d68ba3d57aa0fc, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_6b65ce304f5584aad0d68ba3d57aa0fc,
        type_description_1,
        par_arr_or_dtype,
        var_warnings,
        var_pandas,
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_6b65ce304f5584aad0d68ba3d57aa0fc == cache_frame_6b65ce304f5584aad0d68ba3d57aa0fc )
    {
        Py_DECREF( frame_6b65ce304f5584aad0d68ba3d57aa0fc );
    }
    cache_frame_6b65ce304f5584aad0d68ba3d57aa0fc = NULL;

    assertFrameObject( frame_6b65ce304f5584aad0d68ba3d57aa0fc );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$core$dtypes$api$$$function_1_outer$$$function_1_wrapper );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_arr_or_dtype );
    par_arr_or_dtype = NULL;

    Py_XDECREF( var_warnings );
    var_warnings = NULL;

    Py_XDECREF( var_pandas );
    var_pandas = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_arr_or_dtype );
    par_arr_or_dtype = NULL;

    Py_XDECREF( var_warnings );
    var_warnings = NULL;

    Py_XDECREF( var_pandas );
    var_pandas = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$core$dtypes$api$$$function_1_outer$$$function_1_wrapper );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}



static PyObject *MAKE_FUNCTION_pandas$core$dtypes$api$$$function_1_outer( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$core$dtypes$api$$$function_1_outer,
        const_str_plain_outer,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_0913e72b9f2e6a6ec9f95b805899db27,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$core$dtypes$api,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$core$dtypes$api$$$function_1_outer$$$function_1_wrapper( struct Nuitka_CellObject *closure_t )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$core$dtypes$api$$$function_1_outer$$$function_1_wrapper,
        const_str_plain_wrapper,
#if PYTHON_VERSION >= 300
        const_str_digest_d2cd9395054004c1a53d2c95d5a85417,
#endif
        codeobj_6b65ce304f5584aad0d68ba3d57aa0fc,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$core$dtypes$api,
        Py_None,
        1
    );

result->m_closure[0] = closure_t;
Py_INCREF( result->m_closure[0] );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_pandas$core$dtypes$api =
{
    PyModuleDef_HEAD_INIT,
    "pandas.core.dtypes.api",   /* m_name */
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;

extern PyObject *const_str_plain___loader__;
extern PyObject *metapath_based_loader;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL.

MOD_INIT_DECL( pandas$core$dtypes$api )
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_pandas$core$dtypes$api );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("pandas.core.dtypes.api: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("pandas.core.dtypes.api: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("pandas.core.dtypes.api: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initpandas$core$dtypes$api" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_pandas$core$dtypes$api = Py_InitModule4(
        "pandas.core.dtypes.api",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_pandas$core$dtypes$api = PyModule_Create( &mdef_pandas$core$dtypes$api );
#endif

    moduledict_pandas$core$dtypes$api = MODULE_DICT( module_pandas$core$dtypes$api );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_pandas$core$dtypes$api,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );

#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_pandas$core$dtypes$api,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_pandas$core$dtypes$api,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_pandas$core$dtypes$api );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PySys_GetObject( (char *)"modules" ), const_str_digest_af80ff60d7780e61f6ad18fc641b6dcd, module_pandas$core$dtypes$api );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain___loader__, metapath_based_loader );
#endif

#if PYTHON_VERSION >= 340
#if 0
    UPDATE_STRING_DICT0( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain___name__ ),
            metapath_based_loader
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );

        UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );

        Py_DECREF( module_spec_class );
    }
#endif
#endif


    // Temp variables if any
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_import_from_1__module = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *tmp_args_element_name_1;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_assign_source_5;
    PyObject *tmp_assign_source_6;
    PyObject *tmp_assign_source_7;
    PyObject *tmp_assign_source_8;
    PyObject *tmp_assign_source_9;
    PyObject *tmp_assign_source_10;
    PyObject *tmp_assign_source_11;
    PyObject *tmp_assign_source_12;
    PyObject *tmp_assign_source_13;
    PyObject *tmp_assign_source_14;
    PyObject *tmp_assign_source_15;
    PyObject *tmp_assign_source_16;
    PyObject *tmp_assign_source_17;
    PyObject *tmp_assign_source_18;
    PyObject *tmp_assign_source_19;
    PyObject *tmp_assign_source_20;
    PyObject *tmp_assign_source_21;
    PyObject *tmp_assign_source_22;
    PyObject *tmp_assign_source_23;
    PyObject *tmp_assign_source_24;
    PyObject *tmp_assign_source_25;
    PyObject *tmp_assign_source_26;
    PyObject *tmp_assign_source_27;
    PyObject *tmp_assign_source_28;
    PyObject *tmp_assign_source_29;
    PyObject *tmp_assign_source_30;
    PyObject *tmp_assign_source_31;
    PyObject *tmp_assign_source_32;
    PyObject *tmp_assign_source_33;
    PyObject *tmp_assign_source_34;
    PyObject *tmp_assign_source_35;
    PyObject *tmp_assign_source_36;
    PyObject *tmp_assign_source_37;
    PyObject *tmp_assign_source_38;
    PyObject *tmp_assign_source_39;
    PyObject *tmp_assign_source_40;
    PyObject *tmp_assign_source_41;
    PyObject *tmp_assign_source_42;
    PyObject *tmp_assign_source_43;
    PyObject *tmp_assign_source_44;
    PyObject *tmp_assign_source_45;
    PyObject *tmp_assign_source_46;
    PyObject *tmp_assign_source_47;
    PyObject *tmp_assign_source_48;
    PyObject *tmp_assign_source_49;
    PyObject *tmp_assign_source_50;
    PyObject *tmp_assign_source_51;
    PyObject *tmp_assign_source_52;
    PyObject *tmp_assign_source_53;
    PyObject *tmp_called_name_1;
    PyObject *tmp_compare_left_1;
    PyObject *tmp_compare_right_1;
    PyObject *tmp_defaults_1;
    int tmp_exc_match_exception_match_1;
    PyObject *tmp_fromlist_name_1;
    PyObject *tmp_fromlist_name_2;
    PyObject *tmp_globals_name_1;
    PyObject *tmp_globals_name_2;
    PyObject *tmp_import_name_from_1;
    PyObject *tmp_import_name_from_2;
    PyObject *tmp_import_name_from_3;
    PyObject *tmp_import_name_from_4;
    PyObject *tmp_import_name_from_5;
    PyObject *tmp_import_name_from_6;
    PyObject *tmp_import_name_from_7;
    PyObject *tmp_import_name_from_8;
    PyObject *tmp_import_name_from_9;
    PyObject *tmp_import_name_from_10;
    PyObject *tmp_import_name_from_11;
    PyObject *tmp_import_name_from_12;
    PyObject *tmp_import_name_from_13;
    PyObject *tmp_import_name_from_14;
    PyObject *tmp_import_name_from_15;
    PyObject *tmp_import_name_from_16;
    PyObject *tmp_import_name_from_17;
    PyObject *tmp_import_name_from_18;
    PyObject *tmp_import_name_from_19;
    PyObject *tmp_import_name_from_20;
    PyObject *tmp_import_name_from_21;
    PyObject *tmp_import_name_from_22;
    PyObject *tmp_import_name_from_23;
    PyObject *tmp_import_name_from_24;
    PyObject *tmp_import_name_from_25;
    PyObject *tmp_import_name_from_26;
    PyObject *tmp_import_name_from_27;
    PyObject *tmp_import_name_from_28;
    PyObject *tmp_import_name_from_29;
    PyObject *tmp_import_name_from_30;
    PyObject *tmp_import_name_from_31;
    PyObject *tmp_import_name_from_32;
    PyObject *tmp_import_name_from_33;
    PyObject *tmp_import_name_from_34;
    PyObject *tmp_import_name_from_35;
    PyObject *tmp_import_name_from_36;
    PyObject *tmp_import_name_from_37;
    PyObject *tmp_import_name_from_38;
    PyObject *tmp_import_name_from_39;
    PyObject *tmp_import_name_from_40;
    PyObject *tmp_import_name_from_41;
    PyObject *tmp_import_name_from_42;
    PyObject *tmp_import_name_from_43;
    PyObject *tmp_iter_arg_1;
    PyObject *tmp_level_name_1;
    PyObject *tmp_level_name_2;
    PyObject *tmp_locals_name_1;
    PyObject *tmp_locals_name_2;
    PyObject *tmp_name_name_1;
    PyObject *tmp_name_name_2;
    int tmp_res;
    PyObject *tmp_setattr_attr_1;
    PyObject *tmp_setattr_target_1;
    PyObject *tmp_setattr_value_1;
    PyObject *tmp_source_name_1;
    PyObject *tmp_subscribed_name_1;
    PyObject *tmp_subscript_name_1;
    PyObject *tmp_tuple_element_1;
    NUITKA_MAY_BE_UNUSED PyObject *tmp_unused;
    PyObject *tmp_value_name_1;
    struct Nuitka_FrameObject *frame_82ff4d9d47145c79eeb3041a01a24922;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;

    // Module code.
    tmp_assign_source_1 = Py_None;
    UPDATE_STRING_DICT0( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    tmp_assign_source_2 = const_str_digest_3e4d71669b7092cee01a8820de818348;
    UPDATE_STRING_DICT0( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    tmp_assign_source_3 = Py_None;
    UPDATE_STRING_DICT0( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    tmp_name_name_1 = const_str_plain_sys;
    tmp_globals_name_1 = (PyObject *)moduledict_pandas$core$dtypes$api;
    tmp_locals_name_1 = Py_None;
    tmp_fromlist_name_1 = Py_None;
    tmp_level_name_1 = const_int_0;
    tmp_assign_source_4 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
    assert( !(tmp_assign_source_4 == NULL) );
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain_sys, tmp_assign_source_4 );
    // Frame without reuse.
    frame_82ff4d9d47145c79eeb3041a01a24922 = MAKE_MODULE_FRAME( codeobj_82ff4d9d47145c79eeb3041a01a24922, module_pandas$core$dtypes$api );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_82ff4d9d47145c79eeb3041a01a24922 );
    assert( Py_REFCNT( frame_82ff4d9d47145c79eeb3041a01a24922 ) == 2 );

    // Framed code:
    tmp_name_name_2 = const_str_plain_common;
    tmp_globals_name_2 = (PyObject *)moduledict_pandas$core$dtypes$api;
    tmp_locals_name_2 = Py_None;
    tmp_fromlist_name_2 = const_tuple_5c64004afbc3ee9460abd7403b0ac1d7_tuple;
    tmp_level_name_2 = const_int_pos_1;
    frame_82ff4d9d47145c79eeb3041a01a24922->m_frame.f_lineno = 5;
    tmp_assign_source_5 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
    if ( tmp_assign_source_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 5;

        goto frame_exception_exit_1;
    }
    assert( tmp_import_from_1__module == NULL );
    tmp_import_from_1__module = tmp_assign_source_5;

    // Tried code:
    tmp_import_name_from_1 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_1 );
    if ( PyModule_Check( tmp_import_name_from_1 ) )
    {
       tmp_assign_source_6 = IMPORT_NAME_OR_MODULE(
            tmp_import_name_from_1,
            (PyObject *)MODULE_DICT(tmp_import_name_from_1),
            const_str_plain_pandas_dtype,
            const_int_pos_1
        );
    }
    else
    {
       tmp_assign_source_6 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_pandas_dtype );
    }

    if ( tmp_assign_source_6 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 5;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain_pandas_dtype, tmp_assign_source_6 );
    tmp_import_name_from_2 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_2 );
    if ( PyModule_Check( tmp_import_name_from_2 ) )
    {
       tmp_assign_source_7 = IMPORT_NAME_OR_MODULE(
            tmp_import_name_from_2,
            (PyObject *)MODULE_DICT(tmp_import_name_from_2),
            const_str_plain_is_dtype_equal,
            const_int_pos_1
        );
    }
    else
    {
       tmp_assign_source_7 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_is_dtype_equal );
    }

    if ( tmp_assign_source_7 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 5;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain_is_dtype_equal, tmp_assign_source_7 );
    tmp_import_name_from_3 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_3 );
    if ( PyModule_Check( tmp_import_name_from_3 ) )
    {
       tmp_assign_source_8 = IMPORT_NAME_OR_MODULE(
            tmp_import_name_from_3,
            (PyObject *)MODULE_DICT(tmp_import_name_from_3),
            const_str_plain_is_extension_type,
            const_int_pos_1
        );
    }
    else
    {
       tmp_assign_source_8 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_is_extension_type );
    }

    if ( tmp_assign_source_8 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 5;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain_is_extension_type, tmp_assign_source_8 );
    tmp_import_name_from_4 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_4 );
    if ( PyModule_Check( tmp_import_name_from_4 ) )
    {
       tmp_assign_source_9 = IMPORT_NAME_OR_MODULE(
            tmp_import_name_from_4,
            (PyObject *)MODULE_DICT(tmp_import_name_from_4),
            const_str_plain_is_categorical,
            const_int_pos_1
        );
    }
    else
    {
       tmp_assign_source_9 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_is_categorical );
    }

    if ( tmp_assign_source_9 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 5;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain_is_categorical, tmp_assign_source_9 );
    tmp_import_name_from_5 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_5 );
    if ( PyModule_Check( tmp_import_name_from_5 ) )
    {
       tmp_assign_source_10 = IMPORT_NAME_OR_MODULE(
            tmp_import_name_from_5,
            (PyObject *)MODULE_DICT(tmp_import_name_from_5),
            const_str_plain_is_categorical_dtype,
            const_int_pos_1
        );
    }
    else
    {
       tmp_assign_source_10 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_is_categorical_dtype );
    }

    if ( tmp_assign_source_10 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 5;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain_is_categorical_dtype, tmp_assign_source_10 );
    tmp_import_name_from_6 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_6 );
    if ( PyModule_Check( tmp_import_name_from_6 ) )
    {
       tmp_assign_source_11 = IMPORT_NAME_OR_MODULE(
            tmp_import_name_from_6,
            (PyObject *)MODULE_DICT(tmp_import_name_from_6),
            const_str_plain_is_interval,
            const_int_pos_1
        );
    }
    else
    {
       tmp_assign_source_11 = IMPORT_NAME( tmp_import_name_from_6, const_str_plain_is_interval );
    }

    if ( tmp_assign_source_11 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 5;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain_is_interval, tmp_assign_source_11 );
    tmp_import_name_from_7 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_7 );
    if ( PyModule_Check( tmp_import_name_from_7 ) )
    {
       tmp_assign_source_12 = IMPORT_NAME_OR_MODULE(
            tmp_import_name_from_7,
            (PyObject *)MODULE_DICT(tmp_import_name_from_7),
            const_str_plain_is_interval_dtype,
            const_int_pos_1
        );
    }
    else
    {
       tmp_assign_source_12 = IMPORT_NAME( tmp_import_name_from_7, const_str_plain_is_interval_dtype );
    }

    if ( tmp_assign_source_12 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 5;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain_is_interval_dtype, tmp_assign_source_12 );
    tmp_import_name_from_8 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_8 );
    if ( PyModule_Check( tmp_import_name_from_8 ) )
    {
       tmp_assign_source_13 = IMPORT_NAME_OR_MODULE(
            tmp_import_name_from_8,
            (PyObject *)MODULE_DICT(tmp_import_name_from_8),
            const_str_plain_is_datetimetz,
            const_int_pos_1
        );
    }
    else
    {
       tmp_assign_source_13 = IMPORT_NAME( tmp_import_name_from_8, const_str_plain_is_datetimetz );
    }

    if ( tmp_assign_source_13 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 5;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain_is_datetimetz, tmp_assign_source_13 );
    tmp_import_name_from_9 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_9 );
    if ( PyModule_Check( tmp_import_name_from_9 ) )
    {
       tmp_assign_source_14 = IMPORT_NAME_OR_MODULE(
            tmp_import_name_from_9,
            (PyObject *)MODULE_DICT(tmp_import_name_from_9),
            const_str_plain_is_datetime64_dtype,
            const_int_pos_1
        );
    }
    else
    {
       tmp_assign_source_14 = IMPORT_NAME( tmp_import_name_from_9, const_str_plain_is_datetime64_dtype );
    }

    if ( tmp_assign_source_14 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 5;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain_is_datetime64_dtype, tmp_assign_source_14 );
    tmp_import_name_from_10 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_10 );
    if ( PyModule_Check( tmp_import_name_from_10 ) )
    {
       tmp_assign_source_15 = IMPORT_NAME_OR_MODULE(
            tmp_import_name_from_10,
            (PyObject *)MODULE_DICT(tmp_import_name_from_10),
            const_str_plain_is_datetime64tz_dtype,
            const_int_pos_1
        );
    }
    else
    {
       tmp_assign_source_15 = IMPORT_NAME( tmp_import_name_from_10, const_str_plain_is_datetime64tz_dtype );
    }

    if ( tmp_assign_source_15 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 5;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain_is_datetime64tz_dtype, tmp_assign_source_15 );
    tmp_import_name_from_11 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_11 );
    if ( PyModule_Check( tmp_import_name_from_11 ) )
    {
       tmp_assign_source_16 = IMPORT_NAME_OR_MODULE(
            tmp_import_name_from_11,
            (PyObject *)MODULE_DICT(tmp_import_name_from_11),
            const_str_plain_is_datetime64_any_dtype,
            const_int_pos_1
        );
    }
    else
    {
       tmp_assign_source_16 = IMPORT_NAME( tmp_import_name_from_11, const_str_plain_is_datetime64_any_dtype );
    }

    if ( tmp_assign_source_16 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 5;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain_is_datetime64_any_dtype, tmp_assign_source_16 );
    tmp_import_name_from_12 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_12 );
    if ( PyModule_Check( tmp_import_name_from_12 ) )
    {
       tmp_assign_source_17 = IMPORT_NAME_OR_MODULE(
            tmp_import_name_from_12,
            (PyObject *)MODULE_DICT(tmp_import_name_from_12),
            const_str_plain_is_datetime64_ns_dtype,
            const_int_pos_1
        );
    }
    else
    {
       tmp_assign_source_17 = IMPORT_NAME( tmp_import_name_from_12, const_str_plain_is_datetime64_ns_dtype );
    }

    if ( tmp_assign_source_17 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 5;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain_is_datetime64_ns_dtype, tmp_assign_source_17 );
    tmp_import_name_from_13 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_13 );
    if ( PyModule_Check( tmp_import_name_from_13 ) )
    {
       tmp_assign_source_18 = IMPORT_NAME_OR_MODULE(
            tmp_import_name_from_13,
            (PyObject *)MODULE_DICT(tmp_import_name_from_13),
            const_str_plain_is_timedelta64_dtype,
            const_int_pos_1
        );
    }
    else
    {
       tmp_assign_source_18 = IMPORT_NAME( tmp_import_name_from_13, const_str_plain_is_timedelta64_dtype );
    }

    if ( tmp_assign_source_18 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 5;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain_is_timedelta64_dtype, tmp_assign_source_18 );
    tmp_import_name_from_14 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_14 );
    if ( PyModule_Check( tmp_import_name_from_14 ) )
    {
       tmp_assign_source_19 = IMPORT_NAME_OR_MODULE(
            tmp_import_name_from_14,
            (PyObject *)MODULE_DICT(tmp_import_name_from_14),
            const_str_plain_is_timedelta64_ns_dtype,
            const_int_pos_1
        );
    }
    else
    {
       tmp_assign_source_19 = IMPORT_NAME( tmp_import_name_from_14, const_str_plain_is_timedelta64_ns_dtype );
    }

    if ( tmp_assign_source_19 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 5;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain_is_timedelta64_ns_dtype, tmp_assign_source_19 );
    tmp_import_name_from_15 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_15 );
    if ( PyModule_Check( tmp_import_name_from_15 ) )
    {
       tmp_assign_source_20 = IMPORT_NAME_OR_MODULE(
            tmp_import_name_from_15,
            (PyObject *)MODULE_DICT(tmp_import_name_from_15),
            const_str_plain_is_period,
            const_int_pos_1
        );
    }
    else
    {
       tmp_assign_source_20 = IMPORT_NAME( tmp_import_name_from_15, const_str_plain_is_period );
    }

    if ( tmp_assign_source_20 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 5;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain_is_period, tmp_assign_source_20 );
    tmp_import_name_from_16 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_16 );
    if ( PyModule_Check( tmp_import_name_from_16 ) )
    {
       tmp_assign_source_21 = IMPORT_NAME_OR_MODULE(
            tmp_import_name_from_16,
            (PyObject *)MODULE_DICT(tmp_import_name_from_16),
            const_str_plain_is_period_dtype,
            const_int_pos_1
        );
    }
    else
    {
       tmp_assign_source_21 = IMPORT_NAME( tmp_import_name_from_16, const_str_plain_is_period_dtype );
    }

    if ( tmp_assign_source_21 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 5;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain_is_period_dtype, tmp_assign_source_21 );
    tmp_import_name_from_17 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_17 );
    if ( PyModule_Check( tmp_import_name_from_17 ) )
    {
       tmp_assign_source_22 = IMPORT_NAME_OR_MODULE(
            tmp_import_name_from_17,
            (PyObject *)MODULE_DICT(tmp_import_name_from_17),
            const_str_plain_is_string_dtype,
            const_int_pos_1
        );
    }
    else
    {
       tmp_assign_source_22 = IMPORT_NAME( tmp_import_name_from_17, const_str_plain_is_string_dtype );
    }

    if ( tmp_assign_source_22 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 5;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain_is_string_dtype, tmp_assign_source_22 );
    tmp_import_name_from_18 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_18 );
    if ( PyModule_Check( tmp_import_name_from_18 ) )
    {
       tmp_assign_source_23 = IMPORT_NAME_OR_MODULE(
            tmp_import_name_from_18,
            (PyObject *)MODULE_DICT(tmp_import_name_from_18),
            const_str_plain_is_object_dtype,
            const_int_pos_1
        );
    }
    else
    {
       tmp_assign_source_23 = IMPORT_NAME( tmp_import_name_from_18, const_str_plain_is_object_dtype );
    }

    if ( tmp_assign_source_23 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 5;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain_is_object_dtype, tmp_assign_source_23 );
    tmp_import_name_from_19 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_19 );
    if ( PyModule_Check( tmp_import_name_from_19 ) )
    {
       tmp_assign_source_24 = IMPORT_NAME_OR_MODULE(
            tmp_import_name_from_19,
            (PyObject *)MODULE_DICT(tmp_import_name_from_19),
            const_str_plain_is_sparse,
            const_int_pos_1
        );
    }
    else
    {
       tmp_assign_source_24 = IMPORT_NAME( tmp_import_name_from_19, const_str_plain_is_sparse );
    }

    if ( tmp_assign_source_24 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 5;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain_is_sparse, tmp_assign_source_24 );
    tmp_import_name_from_20 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_20 );
    if ( PyModule_Check( tmp_import_name_from_20 ) )
    {
       tmp_assign_source_25 = IMPORT_NAME_OR_MODULE(
            tmp_import_name_from_20,
            (PyObject *)MODULE_DICT(tmp_import_name_from_20),
            const_str_plain_is_scalar,
            const_int_pos_1
        );
    }
    else
    {
       tmp_assign_source_25 = IMPORT_NAME( tmp_import_name_from_20, const_str_plain_is_scalar );
    }

    if ( tmp_assign_source_25 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 5;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain_is_scalar, tmp_assign_source_25 );
    tmp_import_name_from_21 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_21 );
    if ( PyModule_Check( tmp_import_name_from_21 ) )
    {
       tmp_assign_source_26 = IMPORT_NAME_OR_MODULE(
            tmp_import_name_from_21,
            (PyObject *)MODULE_DICT(tmp_import_name_from_21),
            const_str_plain_is_sparse,
            const_int_pos_1
        );
    }
    else
    {
       tmp_assign_source_26 = IMPORT_NAME( tmp_import_name_from_21, const_str_plain_is_sparse );
    }

    if ( tmp_assign_source_26 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 5;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain_is_sparse, tmp_assign_source_26 );
    tmp_import_name_from_22 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_22 );
    if ( PyModule_Check( tmp_import_name_from_22 ) )
    {
       tmp_assign_source_27 = IMPORT_NAME_OR_MODULE(
            tmp_import_name_from_22,
            (PyObject *)MODULE_DICT(tmp_import_name_from_22),
            const_str_plain_is_bool,
            const_int_pos_1
        );
    }
    else
    {
       tmp_assign_source_27 = IMPORT_NAME( tmp_import_name_from_22, const_str_plain_is_bool );
    }

    if ( tmp_assign_source_27 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 5;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain_is_bool, tmp_assign_source_27 );
    tmp_import_name_from_23 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_23 );
    if ( PyModule_Check( tmp_import_name_from_23 ) )
    {
       tmp_assign_source_28 = IMPORT_NAME_OR_MODULE(
            tmp_import_name_from_23,
            (PyObject *)MODULE_DICT(tmp_import_name_from_23),
            const_str_plain_is_integer,
            const_int_pos_1
        );
    }
    else
    {
       tmp_assign_source_28 = IMPORT_NAME( tmp_import_name_from_23, const_str_plain_is_integer );
    }

    if ( tmp_assign_source_28 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 5;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain_is_integer, tmp_assign_source_28 );
    tmp_import_name_from_24 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_24 );
    if ( PyModule_Check( tmp_import_name_from_24 ) )
    {
       tmp_assign_source_29 = IMPORT_NAME_OR_MODULE(
            tmp_import_name_from_24,
            (PyObject *)MODULE_DICT(tmp_import_name_from_24),
            const_str_plain_is_float,
            const_int_pos_1
        );
    }
    else
    {
       tmp_assign_source_29 = IMPORT_NAME( tmp_import_name_from_24, const_str_plain_is_float );
    }

    if ( tmp_assign_source_29 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 5;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain_is_float, tmp_assign_source_29 );
    tmp_import_name_from_25 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_25 );
    if ( PyModule_Check( tmp_import_name_from_25 ) )
    {
       tmp_assign_source_30 = IMPORT_NAME_OR_MODULE(
            tmp_import_name_from_25,
            (PyObject *)MODULE_DICT(tmp_import_name_from_25),
            const_str_plain_is_complex,
            const_int_pos_1
        );
    }
    else
    {
       tmp_assign_source_30 = IMPORT_NAME( tmp_import_name_from_25, const_str_plain_is_complex );
    }

    if ( tmp_assign_source_30 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 5;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain_is_complex, tmp_assign_source_30 );
    tmp_import_name_from_26 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_26 );
    if ( PyModule_Check( tmp_import_name_from_26 ) )
    {
       tmp_assign_source_31 = IMPORT_NAME_OR_MODULE(
            tmp_import_name_from_26,
            (PyObject *)MODULE_DICT(tmp_import_name_from_26),
            const_str_plain_is_number,
            const_int_pos_1
        );
    }
    else
    {
       tmp_assign_source_31 = IMPORT_NAME( tmp_import_name_from_26, const_str_plain_is_number );
    }

    if ( tmp_assign_source_31 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 5;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain_is_number, tmp_assign_source_31 );
    tmp_import_name_from_27 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_27 );
    if ( PyModule_Check( tmp_import_name_from_27 ) )
    {
       tmp_assign_source_32 = IMPORT_NAME_OR_MODULE(
            tmp_import_name_from_27,
            (PyObject *)MODULE_DICT(tmp_import_name_from_27),
            const_str_plain_is_integer_dtype,
            const_int_pos_1
        );
    }
    else
    {
       tmp_assign_source_32 = IMPORT_NAME( tmp_import_name_from_27, const_str_plain_is_integer_dtype );
    }

    if ( tmp_assign_source_32 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 5;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain_is_integer_dtype, tmp_assign_source_32 );
    tmp_import_name_from_28 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_28 );
    if ( PyModule_Check( tmp_import_name_from_28 ) )
    {
       tmp_assign_source_33 = IMPORT_NAME_OR_MODULE(
            tmp_import_name_from_28,
            (PyObject *)MODULE_DICT(tmp_import_name_from_28),
            const_str_plain_is_int64_dtype,
            const_int_pos_1
        );
    }
    else
    {
       tmp_assign_source_33 = IMPORT_NAME( tmp_import_name_from_28, const_str_plain_is_int64_dtype );
    }

    if ( tmp_assign_source_33 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 5;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain_is_int64_dtype, tmp_assign_source_33 );
    tmp_import_name_from_29 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_29 );
    if ( PyModule_Check( tmp_import_name_from_29 ) )
    {
       tmp_assign_source_34 = IMPORT_NAME_OR_MODULE(
            tmp_import_name_from_29,
            (PyObject *)MODULE_DICT(tmp_import_name_from_29),
            const_str_plain_is_numeric_dtype,
            const_int_pos_1
        );
    }
    else
    {
       tmp_assign_source_34 = IMPORT_NAME( tmp_import_name_from_29, const_str_plain_is_numeric_dtype );
    }

    if ( tmp_assign_source_34 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 5;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain_is_numeric_dtype, tmp_assign_source_34 );
    tmp_import_name_from_30 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_30 );
    if ( PyModule_Check( tmp_import_name_from_30 ) )
    {
       tmp_assign_source_35 = IMPORT_NAME_OR_MODULE(
            tmp_import_name_from_30,
            (PyObject *)MODULE_DICT(tmp_import_name_from_30),
            const_str_plain_is_float_dtype,
            const_int_pos_1
        );
    }
    else
    {
       tmp_assign_source_35 = IMPORT_NAME( tmp_import_name_from_30, const_str_plain_is_float_dtype );
    }

    if ( tmp_assign_source_35 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 5;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain_is_float_dtype, tmp_assign_source_35 );
    tmp_import_name_from_31 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_31 );
    if ( PyModule_Check( tmp_import_name_from_31 ) )
    {
       tmp_assign_source_36 = IMPORT_NAME_OR_MODULE(
            tmp_import_name_from_31,
            (PyObject *)MODULE_DICT(tmp_import_name_from_31),
            const_str_plain_is_bool_dtype,
            const_int_pos_1
        );
    }
    else
    {
       tmp_assign_source_36 = IMPORT_NAME( tmp_import_name_from_31, const_str_plain_is_bool_dtype );
    }

    if ( tmp_assign_source_36 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 5;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain_is_bool_dtype, tmp_assign_source_36 );
    tmp_import_name_from_32 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_32 );
    if ( PyModule_Check( tmp_import_name_from_32 ) )
    {
       tmp_assign_source_37 = IMPORT_NAME_OR_MODULE(
            tmp_import_name_from_32,
            (PyObject *)MODULE_DICT(tmp_import_name_from_32),
            const_str_plain_is_complex_dtype,
            const_int_pos_1
        );
    }
    else
    {
       tmp_assign_source_37 = IMPORT_NAME( tmp_import_name_from_32, const_str_plain_is_complex_dtype );
    }

    if ( tmp_assign_source_37 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 5;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain_is_complex_dtype, tmp_assign_source_37 );
    tmp_import_name_from_33 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_33 );
    if ( PyModule_Check( tmp_import_name_from_33 ) )
    {
       tmp_assign_source_38 = IMPORT_NAME_OR_MODULE(
            tmp_import_name_from_33,
            (PyObject *)MODULE_DICT(tmp_import_name_from_33),
            const_str_plain_is_signed_integer_dtype,
            const_int_pos_1
        );
    }
    else
    {
       tmp_assign_source_38 = IMPORT_NAME( tmp_import_name_from_33, const_str_plain_is_signed_integer_dtype );
    }

    if ( tmp_assign_source_38 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 5;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain_is_signed_integer_dtype, tmp_assign_source_38 );
    tmp_import_name_from_34 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_34 );
    if ( PyModule_Check( tmp_import_name_from_34 ) )
    {
       tmp_assign_source_39 = IMPORT_NAME_OR_MODULE(
            tmp_import_name_from_34,
            (PyObject *)MODULE_DICT(tmp_import_name_from_34),
            const_str_plain_is_unsigned_integer_dtype,
            const_int_pos_1
        );
    }
    else
    {
       tmp_assign_source_39 = IMPORT_NAME( tmp_import_name_from_34, const_str_plain_is_unsigned_integer_dtype );
    }

    if ( tmp_assign_source_39 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 5;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain_is_unsigned_integer_dtype, tmp_assign_source_39 );
    tmp_import_name_from_35 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_35 );
    if ( PyModule_Check( tmp_import_name_from_35 ) )
    {
       tmp_assign_source_40 = IMPORT_NAME_OR_MODULE(
            tmp_import_name_from_35,
            (PyObject *)MODULE_DICT(tmp_import_name_from_35),
            const_str_plain_is_re,
            const_int_pos_1
        );
    }
    else
    {
       tmp_assign_source_40 = IMPORT_NAME( tmp_import_name_from_35, const_str_plain_is_re );
    }

    if ( tmp_assign_source_40 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 5;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain_is_re, tmp_assign_source_40 );
    tmp_import_name_from_36 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_36 );
    if ( PyModule_Check( tmp_import_name_from_36 ) )
    {
       tmp_assign_source_41 = IMPORT_NAME_OR_MODULE(
            tmp_import_name_from_36,
            (PyObject *)MODULE_DICT(tmp_import_name_from_36),
            const_str_plain_is_re_compilable,
            const_int_pos_1
        );
    }
    else
    {
       tmp_assign_source_41 = IMPORT_NAME( tmp_import_name_from_36, const_str_plain_is_re_compilable );
    }

    if ( tmp_assign_source_41 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 5;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain_is_re_compilable, tmp_assign_source_41 );
    tmp_import_name_from_37 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_37 );
    if ( PyModule_Check( tmp_import_name_from_37 ) )
    {
       tmp_assign_source_42 = IMPORT_NAME_OR_MODULE(
            tmp_import_name_from_37,
            (PyObject *)MODULE_DICT(tmp_import_name_from_37),
            const_str_plain_is_dict_like,
            const_int_pos_1
        );
    }
    else
    {
       tmp_assign_source_42 = IMPORT_NAME( tmp_import_name_from_37, const_str_plain_is_dict_like );
    }

    if ( tmp_assign_source_42 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 5;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain_is_dict_like, tmp_assign_source_42 );
    tmp_import_name_from_38 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_38 );
    if ( PyModule_Check( tmp_import_name_from_38 ) )
    {
       tmp_assign_source_43 = IMPORT_NAME_OR_MODULE(
            tmp_import_name_from_38,
            (PyObject *)MODULE_DICT(tmp_import_name_from_38),
            const_str_plain_is_iterator,
            const_int_pos_1
        );
    }
    else
    {
       tmp_assign_source_43 = IMPORT_NAME( tmp_import_name_from_38, const_str_plain_is_iterator );
    }

    if ( tmp_assign_source_43 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 5;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain_is_iterator, tmp_assign_source_43 );
    tmp_import_name_from_39 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_39 );
    if ( PyModule_Check( tmp_import_name_from_39 ) )
    {
       tmp_assign_source_44 = IMPORT_NAME_OR_MODULE(
            tmp_import_name_from_39,
            (PyObject *)MODULE_DICT(tmp_import_name_from_39),
            const_str_plain_is_file_like,
            const_int_pos_1
        );
    }
    else
    {
       tmp_assign_source_44 = IMPORT_NAME( tmp_import_name_from_39, const_str_plain_is_file_like );
    }

    if ( tmp_assign_source_44 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 5;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain_is_file_like, tmp_assign_source_44 );
    tmp_import_name_from_40 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_40 );
    if ( PyModule_Check( tmp_import_name_from_40 ) )
    {
       tmp_assign_source_45 = IMPORT_NAME_OR_MODULE(
            tmp_import_name_from_40,
            (PyObject *)MODULE_DICT(tmp_import_name_from_40),
            const_str_plain_is_array_like,
            const_int_pos_1
        );
    }
    else
    {
       tmp_assign_source_45 = IMPORT_NAME( tmp_import_name_from_40, const_str_plain_is_array_like );
    }

    if ( tmp_assign_source_45 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 5;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain_is_array_like, tmp_assign_source_45 );
    tmp_import_name_from_41 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_41 );
    if ( PyModule_Check( tmp_import_name_from_41 ) )
    {
       tmp_assign_source_46 = IMPORT_NAME_OR_MODULE(
            tmp_import_name_from_41,
            (PyObject *)MODULE_DICT(tmp_import_name_from_41),
            const_str_plain_is_list_like,
            const_int_pos_1
        );
    }
    else
    {
       tmp_assign_source_46 = IMPORT_NAME( tmp_import_name_from_41, const_str_plain_is_list_like );
    }

    if ( tmp_assign_source_46 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 5;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain_is_list_like, tmp_assign_source_46 );
    tmp_import_name_from_42 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_42 );
    if ( PyModule_Check( tmp_import_name_from_42 ) )
    {
       tmp_assign_source_47 = IMPORT_NAME_OR_MODULE(
            tmp_import_name_from_42,
            (PyObject *)MODULE_DICT(tmp_import_name_from_42),
            const_str_plain_is_hashable,
            const_int_pos_1
        );
    }
    else
    {
       tmp_assign_source_47 = IMPORT_NAME( tmp_import_name_from_42, const_str_plain_is_hashable );
    }

    if ( tmp_assign_source_47 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 5;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain_is_hashable, tmp_assign_source_47 );
    tmp_import_name_from_43 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_43 );
    if ( PyModule_Check( tmp_import_name_from_43 ) )
    {
       tmp_assign_source_48 = IMPORT_NAME_OR_MODULE(
            tmp_import_name_from_43,
            (PyObject *)MODULE_DICT(tmp_import_name_from_43),
            const_str_plain_is_named_tuple,
            const_int_pos_1
        );
    }
    else
    {
       tmp_assign_source_48 = IMPORT_NAME( tmp_import_name_from_43, const_str_plain_is_named_tuple );
    }

    if ( tmp_assign_source_48 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 5;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain_is_named_tuple, tmp_assign_source_48 );
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    tmp_source_name_1 = GET_STRING_DICT_VALUE( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain_sys );

    if (unlikely( tmp_source_name_1 == NULL ))
    {
        tmp_source_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
    }

    if ( tmp_source_name_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 65;

        goto frame_exception_exit_1;
    }

    tmp_subscribed_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_modules );
    if ( tmp_subscribed_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 65;

        goto frame_exception_exit_1;
    }
    tmp_subscript_name_1 = const_str_digest_af80ff60d7780e61f6ad18fc641b6dcd;
    tmp_assign_source_49 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
    Py_DECREF( tmp_subscribed_name_1 );
    if ( tmp_assign_source_49 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 65;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain_m, tmp_assign_source_49 );
    tmp_iter_arg_1 = const_tuple_e919463f23025bf7d7a98538876bf5fc_tuple;
    tmp_assign_source_50 = MAKE_ITERATOR( tmp_iter_arg_1 );
    assert( !(tmp_assign_source_50 == NULL) );
    assert( tmp_for_loop_1__for_iterator == NULL );
    tmp_for_loop_1__for_iterator = tmp_assign_source_50;

    // Tried code:
    loop_start_1:;
    // Tried code:
    tmp_value_name_1 = tmp_for_loop_1__for_iterator;

    CHECK_OBJECT( tmp_value_name_1 );
    tmp_assign_source_51 = ITERATOR_NEXT( tmp_value_name_1 );
    if ( tmp_assign_source_51 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }



        exception_lineno = 67;
        goto try_except_handler_3;
    }
    {
        PyObject *old = tmp_for_loop_1__iter_value;
        tmp_for_loop_1__iter_value = tmp_assign_source_51;
        Py_XDECREF( old );
    }

    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    tmp_compare_left_1 = exception_keeper_type_2;
    tmp_compare_right_1 = PyExc_StopIteration;
    tmp_exc_match_exception_match_1 = EXCEPTION_MATCH_BOOL( tmp_compare_left_1, tmp_compare_right_1 );
    if ( tmp_exc_match_exception_match_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

        Py_DECREF( exception_keeper_type_2 );
        Py_XDECREF( exception_keeper_value_2 );
        Py_XDECREF( exception_keeper_tb_2 );

        exception_lineno = 67;

        goto try_except_handler_2;
    }
    if ( tmp_exc_match_exception_match_1 == 1 )
    {
        goto branch_yes_1;
    }
    else
    {
        goto branch_no_1;
    }
    branch_yes_1:;
    Py_DECREF( exception_keeper_type_2 );
    Py_XDECREF( exception_keeper_value_2 );
    Py_XDECREF( exception_keeper_tb_2 );
    goto loop_end_1;
    goto branch_end_1;
    branch_no_1:;
    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto try_except_handler_2;
    branch_end_1:;
    // End of try:
    try_end_2:;
    tmp_assign_source_52 = tmp_for_loop_1__iter_value;

    CHECK_OBJECT( tmp_assign_source_52 );
    UPDATE_STRING_DICT0( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain_t, tmp_assign_source_52 );
    tmp_tuple_element_1 = GET_STRING_DICT_VALUE( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain_t );

    if (unlikely( tmp_tuple_element_1 == NULL ))
    {
        tmp_tuple_element_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_t );
    }

    CHECK_OBJECT( tmp_tuple_element_1 );
    tmp_defaults_1 = PyTuple_New( 1 );
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_defaults_1, 0, tmp_tuple_element_1 );
    tmp_assign_source_53 = MAKE_FUNCTION_pandas$core$dtypes$api$$$function_1_outer( tmp_defaults_1 );
    UPDATE_STRING_DICT1( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain_outer, tmp_assign_source_53 );
    tmp_setattr_target_1 = GET_STRING_DICT_VALUE( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain_m );

    if (unlikely( tmp_setattr_target_1 == NULL ))
    {
        tmp_setattr_target_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_m );
    }

    if ( tmp_setattr_target_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "m" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 80;

        goto try_except_handler_2;
    }

    tmp_setattr_attr_1 = GET_STRING_DICT_VALUE( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain_t );

    if (unlikely( tmp_setattr_attr_1 == NULL ))
    {
        tmp_setattr_attr_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_t );
    }

    CHECK_OBJECT( tmp_setattr_attr_1 );
    tmp_called_name_1 = GET_STRING_DICT_VALUE( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain_outer );

    if (unlikely( tmp_called_name_1 == NULL ))
    {
        tmp_called_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_outer );
    }

    CHECK_OBJECT( tmp_called_name_1 );
    tmp_args_element_name_1 = GET_STRING_DICT_VALUE( moduledict_pandas$core$dtypes$api, (Nuitka_StringObject *)const_str_plain_t );

    if (unlikely( tmp_args_element_name_1 == NULL ))
    {
        tmp_args_element_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_t );
    }

    CHECK_OBJECT( tmp_args_element_name_1 );
    frame_82ff4d9d47145c79eeb3041a01a24922->m_frame.f_lineno = 80;
    {
        PyObject *call_args[] = { tmp_args_element_name_1 };
        tmp_setattr_value_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
    }

    if ( tmp_setattr_value_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 80;

        goto try_except_handler_2;
    }
    tmp_unused = BUILTIN_SETATTR( tmp_setattr_target_1, tmp_setattr_attr_1, tmp_setattr_value_1 );
    Py_DECREF( tmp_setattr_value_1 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 80;

        goto try_except_handler_2;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 67;

        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    tmp_res = PyDict_DelItem( (PyObject *)moduledict_pandas$core$dtypes$api, const_str_plain_sys );
    if ( tmp_res == -1 ) CLEAR_ERROR_OCCURRED();

    if ( tmp_res == -1 )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 82;

        goto frame_exception_exit_1;
    }

    tmp_res = PyDict_DelItem( (PyObject *)moduledict_pandas$core$dtypes$api, const_str_plain_m );
    if ( tmp_res == -1 ) CLEAR_ERROR_OCCURRED();

    if ( tmp_res == -1 )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "m" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 82;

        goto frame_exception_exit_1;
    }

    tmp_res = PyDict_DelItem( (PyObject *)moduledict_pandas$core$dtypes$api, const_str_plain_t );
    if ( tmp_res == -1 ) CLEAR_ERROR_OCCURRED();

    if ( tmp_res == -1 )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "t" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 82;

        goto frame_exception_exit_1;
    }

    tmp_res = PyDict_DelItem( (PyObject *)moduledict_pandas$core$dtypes$api, const_str_plain_outer );
    if ( tmp_res == -1 ) CLEAR_ERROR_OCCURRED();

    if ( tmp_res == -1 )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "outer" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 82;

        goto frame_exception_exit_1;
    }


    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_82ff4d9d47145c79eeb3041a01a24922 );
#endif
    popFrameStack();

    assertFrameObject( frame_82ff4d9d47145c79eeb3041a01a24922 );

    goto frame_no_exception_1;
    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_82ff4d9d47145c79eeb3041a01a24922 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_82ff4d9d47145c79eeb3041a01a24922, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_82ff4d9d47145c79eeb3041a01a24922->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_82ff4d9d47145c79eeb3041a01a24922, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;
    frame_no_exception_1:;

    return MOD_RETURN_VALUE( module_pandas$core$dtypes$api );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
