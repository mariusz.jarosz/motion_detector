/* Generated code for Python source for module 'pandas.io.sas.sas_constants'
 * created by Nuitka version 0.5.32.7
 *
 * This code is in part copyright 2018 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The _module_pandas$io$sas$sas_constants is a Python object pointer of module type. */

/* Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_pandas$io$sas$sas_constants;
PyDictObject *moduledict_pandas$io$sas$sas_constants;

/* The module constants used, if any. */
extern PyObject *const_str_digest_2b8598a93a9d850a4fe0f9dc2b5e1d58;
extern PyObject *const_int_pos_12;
extern PyObject *const_str_plain_column_attributes_index;
extern PyObject *const_int_pos_20;
extern PyObject *const_str_plain_metaclass;
static PyObject *const_bytes_chr_51;
extern PyObject *const_str_plain_column_data_offset_offset;
extern PyObject *const_str_plain_sas_datetime_formats;
extern PyObject *const_int_pos_24;
extern PyObject *const_str_plain_align_1_checker_value;
extern PyObject *const_str_plain_encoding_names;
extern PyObject *const_tuple_str_plain___class___tuple;
extern PyObject *const_str_plain_file_type_offset;
static PyObject *const_int_pos_240;
extern PyObject *const_int_pos_22;
extern PyObject *const_str_plain_column_label_offset_offset;
extern PyObject *const_int_pos_9;
extern PyObject *const_str_plain___file__;
extern PyObject *const_str_plain_column_label_length_length;
extern PyObject *const_str_plain_header_size_length;
extern PyObject *const_str_plain_latin1;
extern PyObject *const_str_plain_object;
static PyObject *const_dict_0123bc20b88d0f6fe2421c53b17ca28e;
static PyObject *const_bytes_digest_d53251f9dd15a3ac8797e1596f709079;
static PyObject *const_bytes_digest_4b7f385f5975b72c6fc161d4079c3321;
static PyObject *const_bytes_digest_af50e0b1f8353213f0e5ece1c0699616;
extern PyObject *const_str_plain_page_meta_type;
static PyObject *const_str_plain_align_1_value;
static PyObject *const_str_plain_page_metc_type;
extern PyObject *const_str_plain_column_type_length;
static PyObject *const_int_pos_39;
extern PyObject *const_str_plain_os_name_offset;
extern PyObject *const_str_plain_subheader_pointer_length_x86;
extern PyObject *const_str_plain_page_count_length;
extern PyObject *const_str_plain_dataset_length;
extern PyObject *const_int_pos_6;
extern PyObject *const_str_plain_block_count_length;
extern PyObject *const_str_plain_column_name_length_offset;
static PyObject *const_int_pos_512;
static PyObject *const_int_pos_204;
extern PyObject *const_int_pos_5;
static PyObject *const_bytes_digest_0cedb7bd5b803de96cbad647ccfe4e03;
extern PyObject *const_int_pos_1024;
static PyObject *const_int_pos_640;
extern PyObject *const_str_plain_u64_byte_checker_value;
extern PyObject *const_int_pos_16;
extern PyObject *const_int_pos_33;
extern PyObject *const_int_pos_30;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_int_pos_32;
extern PyObject *const_str_plain_date_modified_offset;
extern PyObject *const_int_pos_70;
extern PyObject *const_str_plain_compressed_subheader_type;
extern PyObject *const_str_plain_subheader_signature_to_index;
extern PyObject *const_str_plain_column_format_length_offset;
static PyObject *const_int_pos_62;
static PyObject *const_int_pos_216;
extern PyObject *const_str_plain_date_created_length;
static PyObject *const_int_pos_172;
extern PyObject *const_str_plain_column_name_text_subheader_length;
extern PyObject *const_str_digest_ae46cea7a5d40de20d94e2097e781c74;
extern PyObject *const_str_plain___qualname__;
static PyObject *const_bytes_digest_ed708fae07749a569ac1580098f03054;
extern PyObject *const_str_plain_page_data_type;
extern PyObject *const_str_plain_sas_date_formats;
static PyObject *const_str_plain_wcyrillic;
static PyObject *const_bytes_digest_ecd887d58e8b06a6844da64595ceca92;
static PyObject *const_str_plain_page_comp_type;
static PyObject *const_str_plain_wlatin2;
extern PyObject *const_str_plain_col_count_p2_multiplier;
extern PyObject *const_str_plain_file_type_length;
extern PyObject *const_str_plain_page_size_offset;
extern PyObject *const_str_plain_column_format_offset_length;
extern PyObject *const_str_plain_column_label_text_subheader_index_offset;
extern PyObject *const_str_plain_subheader_counts_index;
static PyObject *const_bytes_digest_863bca74cb259ab109ddadd210bd835c;
extern PyObject *const_str_plain_page_mix_types;
extern PyObject *const_str_plain_align_2_length;
extern PyObject *const_int_pos_8;
extern PyObject *const_str_plain_sas_release_length;
extern PyObject *const_str_plain_date_modified_length;
extern PyObject *const_str_plain_compressed_subheader_id;
extern PyObject *const_str_plain_format_and_label_index;
extern PyObject *const_str_plain_header_size_offset;
extern PyObject *const_str_plain_encoding_offset;
extern PyObject *const_str_plain_subheader_count_length;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_plain_page_type_length;
extern PyObject *const_str_plain_align_2_offset;
static PyObject *const_int_neg_28672;
extern PyObject *const_str_plain_page_size_length;
extern PyObject *const_str_plain_column_label_text_subheader_index_length;
extern PyObject *const_str_plain_block_count_offset;
extern PyObject *const_int_pos_10;
extern PyObject *const_str_plain_rle_compression;
extern PyObject *const_str_plain_column_size_index;
static PyObject *const_bytes_digest_a3409d201e8d37abc17850c169b2101b;
static PyObject *const_str_digest_aaf704117f5708d432c89f72cfd66e39;
extern PyObject *const_str_plain_page_amd_type;
static PyObject *const_bytes_digest_0cbbc6b153b9478dbbc478823bedd0c4;
extern PyObject *const_str_plain_endianness_offset;
extern PyObject *const_str_plain_subheader_pointers_offset;
extern PyObject *const_int_pos_60;
static PyObject *const_bytes_digest_1c0ca65d5f878e589b6273f290ae8f83;
extern PyObject *const_str_plain_row_count_offset_multiplier;
extern PyObject *const_str_plain_column_name_offset_offset;
extern PyObject *const_str_plain_column_name_length_length;
static PyObject *const_bytes_digest_75f14aa6f618c971bd0e1e6976f7cce0;
static PyObject *const_int_pos_164;
extern PyObject *const_str_plain_row_size_index;
extern PyObject *const_str_plain_os_version_number_offset;
static PyObject *const_bytes_digest_1b1c6fec89889dcdc21ab47c7c40282d;
extern PyObject *const_str_plain_platform_length;
extern PyObject *const_str_digest_c075052d723d6707083e869a0e3659bb;
extern PyObject *const_str_plain_align_2_value;
extern PyObject *const_int_pos_26;
extern PyObject *const_str_plain_dataset_offset;
extern PyObject *const_str_plain_column_list_index;
extern PyObject *const_int_0;
static PyObject *const_bytes_digest_49bf45ff4d19405340c3b92671ed9a15;
static PyObject *const_str_plain_cyrillic;
extern PyObject *const_str_plain_page_bit_offset_x86;
static PyObject *const_bytes_digest_a1d80f478ab526c69ae07e5b79858942;
static PyObject *const_bytes_digest_db4b0b175ba9aef87730a428c24a1572;
extern PyObject *const_int_pos_15;
extern PyObject *const_str_plain_column_name_index;
static PyObject *const_str_plain_ebcdic870;
static PyObject *const_str_plain_wlatin1;
extern PyObject *const_str_plain_magic;
extern PyObject *const_str_plain_row_count_on_mix_page_offset_multiplier;
static PyObject *const_int_pos_37;
extern PyObject *const_str_plain_subheader_count_offset;
extern PyObject *const_str_plain_os_maker_offset;
extern PyObject *const_str_plain_compression_literals;
extern PyObject *const_str_plain_os_name_length;
static PyObject *const_bytes_digest_2c7c72ce5e6dd7afdfc5cfe2c063e3c7;
extern PyObject *const_str_plain_data_subheader_index;
static PyObject *const_bytes_digest_91e51b16becdd27a032386b2ee9ea6ed;
extern PyObject *const_int_pos_64;
extern PyObject *const_str_plain_encoding_length;
static PyObject *const_int_pos_196;
extern PyObject *const_str_plain_SASIndex;
extern PyObject *const_str_plain_align_1_offset;
extern PyObject *const_str_plain_sas_server_type_offset;
extern PyObject *const_str_plain_os_version_number_length;
extern PyObject *const_str_plain_column_data_length_offset;
extern PyObject *const_int_pos_4;
extern PyObject *const_int_pos_200;
extern PyObject *const_str_plain_column_name_pointer_length;
static PyObject *const_int_pos_224;
extern PyObject *const_str_plain_type;
extern PyObject *const_str_plain_os_maker_length;
extern PyObject *const_int_pos_16384;
static PyObject *const_bytes_digest_6eae831552cced2cc18cf13cc97984cc;
extern PyObject *const_int_pos_90;
extern PyObject *const_str_digest_d7abe4970418126ea4a162358a8d7308;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_plain_column_type_offset;
extern PyObject *const_str_plain___class__;
static PyObject *const_tuple_str_plain_SASIndex_tuple_type_object_tuple_tuple;
static PyObject *const_bytes_digest_c810f73b15dc26066186ea8612817866;
static PyObject *const_bytes_digest_440f72b63ad60e5326317d1b066b8e52;
extern PyObject *const_str_plain_column_label_length_offset;
static PyObject *const_bytes_digest_bd4cb956ae3f12a5edfd1945200324f1;
static PyObject *const_bytes_digest_ff4b4aeb620151ff8ce1a51a85c83367;
static PyObject *const_tuple_61f0b2321e950eb59f6e00174f8c491a_tuple;
static PyObject *const_int_pos_92;
extern PyObject *const_tuple_type_object_tuple;
extern PyObject *const_str_plain___module__;
extern PyObject *const_bytes_digest_f3d985e0a13e2108b4627a133e77d8a1;
extern PyObject *const_int_pos_61;
static PyObject *const_bytes_digest_84b28972237bb98ee2c1ab8ea8e07175;
static PyObject *const_bytes_digest_7d41b59312be655f2613543574f86276;
extern PyObject *const_str_plain_sas_release_offset;
extern PyObject *const_str_plain_align_1_length;
extern PyObject *const_int_pos_1;
static PyObject *const_int_pos_156;
extern PyObject *const_int_pos_256;
extern PyObject *const_str_plain_col_count_p1_multiplier;
static PyObject *const_bytes_digest_9b0c608657dd661f731eab454dbc3699;
extern PyObject *const_str_plain_page_count_offset;
extern PyObject *const_str_plain_sas_server_type_length;
extern PyObject *const_str_plain_column_name_offset_length;
extern PyObject *const_str_plain_column_text_index;
static PyObject *const_bytes_digest_371494012c6e9066fad0356c424e96ad;
extern PyObject *const_str_plain_column_format_offset_offset;
extern PyObject *const_str_plain___prepare__;
extern PyObject *const_int_pos_3;
extern PyObject *const_str_plain_page_bit_offset_x64;
static PyObject *const_bytes_digest_01bdfbd570fb89653774c25ee2eefd45;
extern PyObject *const_str_plain_truncated_subheader_id;
static PyObject *const_tuple_2f0e3f48309e1b3dacb9487878e05906_tuple;
static PyObject *const_bytes_digest_a9b059e744598d7da903858ccdd9811e;
static PyObject *const_bytes_digest_d9137199bb9ab3861ec2d1e3544d9093;
extern PyObject *const_str_plain_column_name_text_subheader_offset;
static PyObject *const_bytes_digest_86d19990f68076a18805eaeaa3cd5116;
extern PyObject *const_int_pos_28;
extern PyObject *const_int_pos_7;
extern PyObject *const_str_plain_platform_offset;
extern PyObject *const_str_plain_subheader_pointer_length_x64;
static PyObject *const_int_pos_35;
static PyObject *const_int_pos_272;
extern PyObject *const_str_plain_date_created_offset;
extern PyObject *const_int_pos_2;
extern PyObject *const_int_pos_29;
extern PyObject *const_str_plain_column_format_length_length;
static PyObject *const_bytes_digest_6870746d51334ef2099a2b9f963fd5b8;
extern PyObject *const_str_plain_text_block_size_length;
extern PyObject *const_str_plain_endianness_length;
extern PyObject *const_str_plain_row_length_offset_multiplier;
static PyObject *const_list_int_pos_512_int_pos_640_list;
static PyObject *const_str_digest_1ec9abf3b1872ccf6fd9645608b6f9b1;
extern PyObject *const_str_plain_page_type_offset;
extern PyObject *const_str_plain_column_data_length_length;
static PyObject *const_str_plain_rdc_compression;
extern PyObject *const_int_pos_14;
extern PyObject *const_str_plain_column_label_offset_length;
static PyObject *const_bytes_digest_a191b8911b51ba182c05402179c28694;
static PyObject *module_filename_obj;

static bool constants_created = false;

static void createModuleConstants( void )
{
    const_bytes_chr_51 = UNSTREAM_BYTES( &constant_bin[ 1218 ], 1 );
    const_int_pos_240 = PyLong_FromUnsignedLong( 240ul );
    const_dict_0123bc20b88d0f6fe2421c53b17ca28e = _PyDict_NewPresized( 7 );
    PyDict_SetItem( const_dict_0123bc20b88d0f6fe2421c53b17ca28e, const_int_pos_29, const_str_plain_latin1 );
    PyDict_SetItem( const_dict_0123bc20b88d0f6fe2421c53b17ca28e, const_int_pos_20, const_str_digest_c075052d723d6707083e869a0e3659bb );
    const_str_plain_cyrillic = UNSTREAM_STRING( &constant_bin[ 3166748 ], 8, 1 );
    PyDict_SetItem( const_dict_0123bc20b88d0f6fe2421c53b17ca28e, const_int_pos_33, const_str_plain_cyrillic );
    const_str_plain_wlatin2 = UNSTREAM_STRING( &constant_bin[ 3166756 ], 7, 1 );
    PyDict_SetItem( const_dict_0123bc20b88d0f6fe2421c53b17ca28e, const_int_pos_60, const_str_plain_wlatin2 );
    const_str_plain_wcyrillic = UNSTREAM_STRING( &constant_bin[ 3166763 ], 9, 1 );
    PyDict_SetItem( const_dict_0123bc20b88d0f6fe2421c53b17ca28e, const_int_pos_61, const_str_plain_wcyrillic );
    const_int_pos_62 = PyLong_FromUnsignedLong( 62ul );
    const_str_plain_wlatin1 = UNSTREAM_STRING( &constant_bin[ 3166772 ], 7, 1 );
    PyDict_SetItem( const_dict_0123bc20b88d0f6fe2421c53b17ca28e, const_int_pos_62, const_str_plain_wlatin1 );
    const_str_plain_ebcdic870 = UNSTREAM_STRING( &constant_bin[ 3166779 ], 9, 1 );
    PyDict_SetItem( const_dict_0123bc20b88d0f6fe2421c53b17ca28e, const_int_pos_90, const_str_plain_ebcdic870 );
    assert( PyDict_Size( const_dict_0123bc20b88d0f6fe2421c53b17ca28e ) == 7 );
    const_bytes_digest_d53251f9dd15a3ac8797e1596f709079 = UNSTREAM_BYTES( &constant_bin[ 3166788 ], 8 );
    const_bytes_digest_4b7f385f5975b72c6fc161d4079c3321 = UNSTREAM_BYTES( &constant_bin[ 3166796 ], 4 );
    const_bytes_digest_af50e0b1f8353213f0e5ece1c0699616 = UNSTREAM_BYTES( &constant_bin[ 3166800 ], 8 );
    const_str_plain_align_1_value = UNSTREAM_STRING( &constant_bin[ 3166808 ], 13, 1 );
    const_str_plain_page_metc_type = UNSTREAM_STRING( &constant_bin[ 3166821 ], 14, 1 );
    const_int_pos_39 = PyLong_FromUnsignedLong( 39ul );
    const_int_pos_512 = PyLong_FromUnsignedLong( 512ul );
    const_int_pos_204 = PyLong_FromUnsignedLong( 204ul );
    const_bytes_digest_0cedb7bd5b803de96cbad647ccfe4e03 = UNSTREAM_BYTES( &constant_bin[ 3166835 ], 4 );
    const_int_pos_640 = PyLong_FromUnsignedLong( 640ul );
    const_int_pos_216 = PyLong_FromUnsignedLong( 216ul );
    const_int_pos_172 = PyLong_FromUnsignedLong( 172ul );
    const_bytes_digest_ed708fae07749a569ac1580098f03054 = UNSTREAM_BYTES( &constant_bin[ 3166839 ], 8 );
    const_bytes_digest_ecd887d58e8b06a6844da64595ceca92 = UNSTREAM_BYTES( &constant_bin[ 3166847 ], 32 );
    const_str_plain_page_comp_type = UNSTREAM_STRING( &constant_bin[ 3166879 ], 14, 1 );
    const_bytes_digest_863bca74cb259ab109ddadd210bd835c = UNSTREAM_BYTES( &constant_bin[ 3166893 ], 8 );
    const_int_neg_28672 = PyLong_FromLong( -28672l );
    const_bytes_digest_a3409d201e8d37abc17850c169b2101b = UNSTREAM_BYTES( &constant_bin[ 3166901 ], 8 );
    const_str_digest_aaf704117f5708d432c89f72cfd66e39 = UNSTREAM_STRING( &constant_bin[ 3166909 ], 36, 0 );
    const_bytes_digest_0cbbc6b153b9478dbbc478823bedd0c4 = UNSTREAM_BYTES( &constant_bin[ 3166902 ], 4 );
    const_bytes_digest_1c0ca65d5f878e589b6273f290ae8f83 = UNSTREAM_BYTES( &constant_bin[ 3166945 ], 8 );
    const_bytes_digest_75f14aa6f618c971bd0e1e6976f7cce0 = UNSTREAM_BYTES( &constant_bin[ 3166792 ], 4 );
    const_int_pos_164 = PyLong_FromUnsignedLong( 164ul );
    const_bytes_digest_1b1c6fec89889dcdc21ab47c7c40282d = UNSTREAM_BYTES( &constant_bin[ 3166953 ], 8 );
    const_bytes_digest_49bf45ff4d19405340c3b92671ed9a15 = UNSTREAM_BYTES( &constant_bin[ 3166948 ], 4 );
    const_bytes_digest_a1d80f478ab526c69ae07e5b79858942 = UNSTREAM_BYTES( &constant_bin[ 3166961 ], 8 );
    const_bytes_digest_db4b0b175ba9aef87730a428c24a1572 = UNSTREAM_BYTES( &constant_bin[ 3166969 ], 8 );
    const_int_pos_37 = PyLong_FromUnsignedLong( 37ul );
    const_bytes_digest_2c7c72ce5e6dd7afdfc5cfe2c063e3c7 = UNSTREAM_BYTES( &constant_bin[ 3166977 ], 8 );
    const_bytes_digest_91e51b16becdd27a032386b2ee9ea6ed = UNSTREAM_BYTES( &constant_bin[ 3166985 ], 8 );
    const_int_pos_196 = PyLong_FromUnsignedLong( 196ul );
    const_int_pos_224 = PyLong_FromUnsignedLong( 224ul );
    const_bytes_digest_6eae831552cced2cc18cf13cc97984cc = UNSTREAM_BYTES( &constant_bin[ 3166993 ], 8 );
    const_tuple_str_plain_SASIndex_tuple_type_object_tuple_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_SASIndex_tuple_type_object_tuple_tuple, 0, const_str_plain_SASIndex ); Py_INCREF( const_str_plain_SASIndex );
    PyTuple_SET_ITEM( const_tuple_str_plain_SASIndex_tuple_type_object_tuple_tuple, 1, const_tuple_type_object_tuple ); Py_INCREF( const_tuple_type_object_tuple );
    const_bytes_digest_c810f73b15dc26066186ea8612817866 = UNSTREAM_BYTES( &constant_bin[ 3167001 ], 8 );
    const_bytes_digest_440f72b63ad60e5326317d1b066b8e52 = UNSTREAM_BYTES( &constant_bin[ 3166977 ], 4 );
    const_bytes_digest_bd4cb956ae3f12a5edfd1945200324f1 = UNSTREAM_BYTES( &constant_bin[ 3167009 ], 8 );
    const_bytes_digest_ff4b4aeb620151ff8ce1a51a85c83367 = UNSTREAM_BYTES( &constant_bin[ 3166979 ], 8 );
    const_tuple_61f0b2321e950eb59f6e00174f8c491a_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 3167017 ], 180 );
    const_int_pos_92 = PyLong_FromUnsignedLong( 92ul );
    const_bytes_digest_84b28972237bb98ee2c1ab8ea8e07175 = UNSTREAM_BYTES( &constant_bin[ 3166901 ], 4 );
    const_bytes_digest_7d41b59312be655f2613543574f86276 = UNSTREAM_BYTES( &constant_bin[ 3167197 ], 8 );
    const_int_pos_156 = PyLong_FromUnsignedLong( 156ul );
    const_bytes_digest_9b0c608657dd661f731eab454dbc3699 = UNSTREAM_BYTES( &constant_bin[ 3167205 ], 8 );
    const_bytes_digest_371494012c6e9066fad0356c424e96ad = UNSTREAM_BYTES( &constant_bin[ 3166897 ], 4 );
    const_bytes_digest_01bdfbd570fb89653774c25ee2eefd45 = UNSTREAM_BYTES( &constant_bin[ 3167213 ], 8 );
    const_tuple_2f0e3f48309e1b3dacb9487878e05906_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 3167221 ], 509 );
    const_bytes_digest_a9b059e744598d7da903858ccdd9811e = UNSTREAM_BYTES( &constant_bin[ 3166788 ], 4 );
    const_bytes_digest_d9137199bb9ab3861ec2d1e3544d9093 = UNSTREAM_BYTES( &constant_bin[ 3167730 ], 8 );
    const_bytes_digest_86d19990f68076a18805eaeaa3cd5116 = UNSTREAM_BYTES( &constant_bin[ 3005460 ], 4 );
    const_int_pos_35 = PyLong_FromUnsignedLong( 35ul );
    const_int_pos_272 = PyLong_FromUnsignedLong( 272ul );
    const_bytes_digest_6870746d51334ef2099a2b9f963fd5b8 = UNSTREAM_BYTES( &constant_bin[ 1229967 ], 4 );
    const_list_int_pos_512_int_pos_640_list = PyList_New( 2 );
    PyList_SET_ITEM( const_list_int_pos_512_int_pos_640_list, 0, const_int_pos_512 ); Py_INCREF( const_int_pos_512 );
    PyList_SET_ITEM( const_list_int_pos_512_int_pos_640_list, 1, const_int_pos_640 ); Py_INCREF( const_int_pos_640 );
    const_str_digest_1ec9abf3b1872ccf6fd9645608b6f9b1 = UNSTREAM_STRING( &constant_bin[ 3167738 ], 93, 0 );
    const_str_plain_rdc_compression = UNSTREAM_STRING( &constant_bin[ 3167831 ], 15, 1 );
    const_bytes_digest_a191b8911b51ba182c05402179c28694 = UNSTREAM_BYTES( &constant_bin[ 3166949 ], 4 );

    constants_created = true;
}

#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_pandas$io$sas$sas_constants( void )
{
    // The module may not have been used at all.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_0c5b1fd0275e283c370e24b6506ffd15;
static PyCodeObject *codeobj_615de7a2697e1671c8dc1d4384fd8117;

static void createModuleCodeObjects(void)
{
    module_filename_obj = const_str_digest_1ec9abf3b1872ccf6fd9645608b6f9b1;
    codeobj_0c5b1fd0275e283c370e24b6506ffd15 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_aaf704117f5708d432c89f72cfd66e39, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_615de7a2697e1671c8dc1d4384fd8117 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_SASIndex, 105, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
}

// The module function declarations.


// The module function definitions.



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_pandas$io$sas$sas_constants =
{
    PyModuleDef_HEAD_INIT,
    "pandas.io.sas.sas_constants",   /* m_name */
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;

extern PyObject *const_str_plain___loader__;
extern PyObject *metapath_based_loader;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL.

MOD_INIT_DECL( pandas$io$sas$sas_constants )
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_pandas$io$sas$sas_constants );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("pandas.io.sas.sas_constants: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("pandas.io.sas.sas_constants: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("pandas.io.sas.sas_constants: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initpandas$io$sas$sas_constants" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_pandas$io$sas$sas_constants = Py_InitModule4(
        "pandas.io.sas.sas_constants",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_pandas$io$sas$sas_constants = PyModule_Create( &mdef_pandas$io$sas$sas_constants );
#endif

    moduledict_pandas$io$sas$sas_constants = MODULE_DICT( module_pandas$io$sas$sas_constants );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_pandas$io$sas$sas_constants,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );

#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_pandas$io$sas$sas_constants,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_pandas$io$sas$sas_constants,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_pandas$io$sas$sas_constants );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PySys_GetObject( (char *)"modules" ), const_str_digest_d7abe4970418126ea4a162358a8d7308, module_pandas$io$sas$sas_constants );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain___loader__, metapath_based_loader );
#endif

#if PYTHON_VERSION >= 340
#if 0
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain___name__ ),
            metapath_based_loader
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );

        UPDATE_STRING_DICT1( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );

        Py_DECREF( module_spec_class );
    }
#endif
#endif


    // Temp variables if any
    PyObject *outline_0_var___class__ = NULL;
    PyObject *tmp_class_creation_1__class_decl_dict = NULL;
    PyObject *tmp_class_creation_1__metaclass = NULL;
    PyObject *tmp_class_creation_1__prepared = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *tmp_args_name_1;
    PyObject *tmp_args_name_2;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_assign_source_5;
    PyObject *tmp_assign_source_6;
    PyObject *tmp_assign_source_7;
    PyObject *tmp_assign_source_8;
    PyObject *tmp_assign_source_9;
    PyObject *tmp_assign_source_10;
    PyObject *tmp_assign_source_11;
    PyObject *tmp_assign_source_12;
    PyObject *tmp_assign_source_13;
    PyObject *tmp_assign_source_14;
    PyObject *tmp_assign_source_15;
    PyObject *tmp_assign_source_16;
    PyObject *tmp_assign_source_17;
    PyObject *tmp_assign_source_18;
    PyObject *tmp_assign_source_19;
    PyObject *tmp_assign_source_20;
    PyObject *tmp_assign_source_21;
    PyObject *tmp_assign_source_22;
    PyObject *tmp_assign_source_23;
    PyObject *tmp_assign_source_24;
    PyObject *tmp_assign_source_25;
    PyObject *tmp_assign_source_26;
    PyObject *tmp_assign_source_27;
    PyObject *tmp_assign_source_28;
    PyObject *tmp_assign_source_29;
    PyObject *tmp_assign_source_30;
    PyObject *tmp_assign_source_31;
    PyObject *tmp_assign_source_32;
    PyObject *tmp_assign_source_33;
    PyObject *tmp_assign_source_34;
    PyObject *tmp_assign_source_35;
    PyObject *tmp_assign_source_36;
    PyObject *tmp_assign_source_37;
    PyObject *tmp_assign_source_38;
    PyObject *tmp_assign_source_39;
    PyObject *tmp_assign_source_40;
    PyObject *tmp_assign_source_41;
    PyObject *tmp_assign_source_42;
    PyObject *tmp_assign_source_43;
    PyObject *tmp_assign_source_44;
    PyObject *tmp_assign_source_45;
    PyObject *tmp_assign_source_46;
    PyObject *tmp_assign_source_47;
    PyObject *tmp_assign_source_48;
    PyObject *tmp_assign_source_49;
    PyObject *tmp_assign_source_50;
    PyObject *tmp_assign_source_51;
    PyObject *tmp_assign_source_52;
    PyObject *tmp_assign_source_53;
    PyObject *tmp_assign_source_54;
    PyObject *tmp_assign_source_55;
    PyObject *tmp_assign_source_56;
    PyObject *tmp_assign_source_57;
    PyObject *tmp_assign_source_58;
    PyObject *tmp_assign_source_59;
    PyObject *tmp_assign_source_60;
    PyObject *tmp_assign_source_61;
    PyObject *tmp_assign_source_62;
    PyObject *tmp_assign_source_63;
    PyObject *tmp_assign_source_64;
    PyObject *tmp_assign_source_65;
    PyObject *tmp_assign_source_66;
    PyObject *tmp_assign_source_67;
    PyObject *tmp_assign_source_68;
    PyObject *tmp_assign_source_69;
    PyObject *tmp_assign_source_70;
    PyObject *tmp_assign_source_71;
    PyObject *tmp_assign_source_72;
    PyObject *tmp_assign_source_73;
    PyObject *tmp_assign_source_74;
    PyObject *tmp_assign_source_75;
    PyObject *tmp_assign_source_76;
    PyObject *tmp_assign_source_77;
    PyObject *tmp_assign_source_78;
    PyObject *tmp_assign_source_79;
    PyObject *tmp_assign_source_80;
    PyObject *tmp_assign_source_81;
    PyObject *tmp_assign_source_82;
    PyObject *tmp_assign_source_83;
    PyObject *tmp_assign_source_84;
    PyObject *tmp_assign_source_85;
    PyObject *tmp_assign_source_86;
    PyObject *tmp_assign_source_87;
    PyObject *tmp_assign_source_88;
    PyObject *tmp_assign_source_89;
    PyObject *tmp_assign_source_90;
    PyObject *tmp_assign_source_91;
    PyObject *tmp_assign_source_92;
    PyObject *tmp_assign_source_93;
    PyObject *tmp_assign_source_94;
    PyObject *tmp_assign_source_95;
    PyObject *tmp_assign_source_96;
    PyObject *tmp_assign_source_97;
    PyObject *tmp_assign_source_98;
    PyObject *tmp_assign_source_99;
    PyObject *tmp_assign_source_100;
    PyObject *tmp_assign_source_101;
    PyObject *tmp_assign_source_102;
    PyObject *tmp_assign_source_103;
    PyObject *tmp_assign_source_104;
    PyObject *tmp_bases_name_1;
    PyObject *tmp_called_name_1;
    PyObject *tmp_called_name_2;
    int tmp_cond_truth_1;
    int tmp_cond_truth_2;
    PyObject *tmp_cond_value_1;
    PyObject *tmp_cond_value_2;
    PyObject *tmp_dict_key_1;
    PyObject *tmp_dict_key_2;
    PyObject *tmp_dict_key_3;
    PyObject *tmp_dict_key_4;
    PyObject *tmp_dict_key_5;
    PyObject *tmp_dict_key_6;
    PyObject *tmp_dict_key_7;
    PyObject *tmp_dict_key_8;
    PyObject *tmp_dict_key_9;
    PyObject *tmp_dict_key_10;
    PyObject *tmp_dict_key_11;
    PyObject *tmp_dict_key_12;
    PyObject *tmp_dict_key_13;
    PyObject *tmp_dict_key_14;
    PyObject *tmp_dict_key_15;
    PyObject *tmp_dict_key_16;
    PyObject *tmp_dict_key_17;
    PyObject *tmp_dict_key_18;
    PyObject *tmp_dict_key_19;
    PyObject *tmp_dict_key_20;
    PyObject *tmp_dict_key_21;
    PyObject *tmp_dict_key_22;
    PyObject *tmp_dict_key_23;
    PyObject *tmp_dict_key_24;
    PyObject *tmp_dict_key_25;
    PyObject *tmp_dict_key_26;
    PyObject *tmp_dict_key_27;
    PyObject *tmp_dict_key_28;
    PyObject *tmp_dict_key_29;
    PyObject *tmp_dict_key_30;
    PyObject *tmp_dict_name_1;
    PyObject *tmp_dict_name_2;
    PyObject *tmp_dict_name_3;
    PyObject *tmp_dict_value_1;
    PyObject *tmp_dict_value_2;
    PyObject *tmp_dict_value_3;
    PyObject *tmp_dict_value_4;
    PyObject *tmp_dict_value_5;
    PyObject *tmp_dict_value_6;
    PyObject *tmp_dict_value_7;
    PyObject *tmp_dict_value_8;
    PyObject *tmp_dict_value_9;
    PyObject *tmp_dict_value_10;
    PyObject *tmp_dict_value_11;
    PyObject *tmp_dict_value_12;
    PyObject *tmp_dict_value_13;
    PyObject *tmp_dict_value_14;
    PyObject *tmp_dict_value_15;
    PyObject *tmp_dict_value_16;
    PyObject *tmp_dict_value_17;
    PyObject *tmp_dict_value_18;
    PyObject *tmp_dict_value_19;
    PyObject *tmp_dict_value_20;
    PyObject *tmp_dict_value_21;
    PyObject *tmp_dict_value_22;
    PyObject *tmp_dict_value_23;
    PyObject *tmp_dict_value_24;
    PyObject *tmp_dict_value_25;
    PyObject *tmp_dict_value_26;
    PyObject *tmp_dict_value_27;
    PyObject *tmp_dict_value_28;
    PyObject *tmp_dict_value_29;
    PyObject *tmp_dict_value_30;
    PyObject *tmp_dictdel_dict;
    PyObject *tmp_dictdel_key;
    PyObject *tmp_dictset_value;
    PyObject *tmp_hasattr_attr_1;
    PyObject *tmp_hasattr_source_1;
    PyObject *tmp_key_name_1;
    PyObject *tmp_key_name_2;
    PyObject *tmp_key_name_3;
    PyObject *tmp_kw_name_1;
    PyObject *tmp_kw_name_2;
    PyObject *tmp_list_element_1;
    PyObject *tmp_metaclass_name_1;
    PyObject *tmp_outline_return_value_1;
    int tmp_res;
    bool tmp_result;
    PyObject *tmp_set_locals;
    PyObject *tmp_source_name_1;
    PyObject *tmp_source_name_2;
    PyObject *tmp_source_name_3;
    PyObject *tmp_source_name_4;
    PyObject *tmp_source_name_5;
    PyObject *tmp_source_name_6;
    PyObject *tmp_source_name_7;
    PyObject *tmp_source_name_8;
    PyObject *tmp_source_name_9;
    PyObject *tmp_source_name_10;
    PyObject *tmp_source_name_11;
    PyObject *tmp_source_name_12;
    PyObject *tmp_source_name_13;
    PyObject *tmp_source_name_14;
    PyObject *tmp_source_name_15;
    PyObject *tmp_source_name_16;
    PyObject *tmp_source_name_17;
    PyObject *tmp_source_name_18;
    PyObject *tmp_source_name_19;
    PyObject *tmp_source_name_20;
    PyObject *tmp_source_name_21;
    PyObject *tmp_source_name_22;
    PyObject *tmp_source_name_23;
    PyObject *tmp_source_name_24;
    PyObject *tmp_source_name_25;
    PyObject *tmp_source_name_26;
    PyObject *tmp_source_name_27;
    PyObject *tmp_source_name_28;
    PyObject *tmp_source_name_29;
    PyObject *tmp_source_name_30;
    PyObject *tmp_source_name_31;
    PyObject *tmp_tuple_element_1;
    static struct Nuitka_FrameObject *cache_frame_615de7a2697e1671c8dc1d4384fd8117_2 = NULL;

    struct Nuitka_FrameObject *frame_615de7a2697e1671c8dc1d4384fd8117_2;

    struct Nuitka_FrameObject *frame_0c5b1fd0275e283c370e24b6506ffd15;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    tmp_outline_return_value_1 = NULL;
    PyObject *locals_pandas$io$sas$sas_constants_105 = NULL;

    // Module code.
    tmp_assign_source_1 = Py_None;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    tmp_assign_source_2 = const_str_digest_1ec9abf3b1872ccf6fd9645608b6f9b1;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    tmp_assign_source_3 = Py_None;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    tmp_assign_source_4 = const_bytes_digest_ecd887d58e8b06a6844da64595ceca92;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_magic, tmp_assign_source_4 );
    tmp_assign_source_5 = const_bytes_chr_51;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_align_1_checker_value, tmp_assign_source_5 );
    tmp_assign_source_6 = const_int_pos_32;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_align_1_offset, tmp_assign_source_6 );
    tmp_assign_source_7 = const_int_pos_1;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_align_1_length, tmp_assign_source_7 );
    tmp_assign_source_8 = const_int_pos_4;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_align_1_value, tmp_assign_source_8 );
    tmp_assign_source_9 = const_bytes_chr_51;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_u64_byte_checker_value, tmp_assign_source_9 );
    tmp_assign_source_10 = const_int_pos_35;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_align_2_offset, tmp_assign_source_10 );
    tmp_assign_source_11 = const_int_pos_1;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_align_2_length, tmp_assign_source_11 );
    tmp_assign_source_12 = const_int_pos_4;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_align_2_value, tmp_assign_source_12 );
    tmp_assign_source_13 = const_int_pos_37;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_endianness_offset, tmp_assign_source_13 );
    tmp_assign_source_14 = const_int_pos_1;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_endianness_length, tmp_assign_source_14 );
    tmp_assign_source_15 = const_int_pos_39;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_platform_offset, tmp_assign_source_15 );
    tmp_assign_source_16 = const_int_pos_1;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_platform_length, tmp_assign_source_16 );
    tmp_assign_source_17 = const_int_pos_70;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_encoding_offset, tmp_assign_source_17 );
    tmp_assign_source_18 = const_int_pos_1;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_encoding_length, tmp_assign_source_18 );
    tmp_assign_source_19 = const_int_pos_92;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_dataset_offset, tmp_assign_source_19 );
    tmp_assign_source_20 = const_int_pos_64;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_dataset_length, tmp_assign_source_20 );
    tmp_assign_source_21 = const_int_pos_156;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_file_type_offset, tmp_assign_source_21 );
    tmp_assign_source_22 = const_int_pos_8;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_file_type_length, tmp_assign_source_22 );
    tmp_assign_source_23 = const_int_pos_164;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_date_created_offset, tmp_assign_source_23 );
    tmp_assign_source_24 = const_int_pos_8;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_date_created_length, tmp_assign_source_24 );
    tmp_assign_source_25 = const_int_pos_172;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_date_modified_offset, tmp_assign_source_25 );
    tmp_assign_source_26 = const_int_pos_8;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_date_modified_length, tmp_assign_source_26 );
    tmp_assign_source_27 = const_int_pos_196;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_header_size_offset, tmp_assign_source_27 );
    tmp_assign_source_28 = const_int_pos_4;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_header_size_length, tmp_assign_source_28 );
    tmp_assign_source_29 = const_int_pos_200;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_page_size_offset, tmp_assign_source_29 );
    tmp_assign_source_30 = const_int_pos_4;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_page_size_length, tmp_assign_source_30 );
    tmp_assign_source_31 = const_int_pos_204;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_page_count_offset, tmp_assign_source_31 );
    tmp_assign_source_32 = const_int_pos_4;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_page_count_length, tmp_assign_source_32 );
    tmp_assign_source_33 = const_int_pos_216;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_sas_release_offset, tmp_assign_source_33 );
    tmp_assign_source_34 = const_int_pos_8;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_sas_release_length, tmp_assign_source_34 );
    tmp_assign_source_35 = const_int_pos_224;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_sas_server_type_offset, tmp_assign_source_35 );
    tmp_assign_source_36 = const_int_pos_16;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_sas_server_type_length, tmp_assign_source_36 );
    tmp_assign_source_37 = const_int_pos_240;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_os_version_number_offset, tmp_assign_source_37 );
    tmp_assign_source_38 = const_int_pos_16;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_os_version_number_length, tmp_assign_source_38 );
    tmp_assign_source_39 = const_int_pos_256;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_os_maker_offset, tmp_assign_source_39 );
    tmp_assign_source_40 = const_int_pos_16;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_os_maker_length, tmp_assign_source_40 );
    tmp_assign_source_41 = const_int_pos_272;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_os_name_offset, tmp_assign_source_41 );
    tmp_assign_source_42 = const_int_pos_16;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_os_name_length, tmp_assign_source_42 );
    tmp_assign_source_43 = const_int_pos_16;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_page_bit_offset_x86, tmp_assign_source_43 );
    tmp_assign_source_44 = const_int_pos_32;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_page_bit_offset_x64, tmp_assign_source_44 );
    tmp_assign_source_45 = const_int_pos_12;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_subheader_pointer_length_x86, tmp_assign_source_45 );
    tmp_assign_source_46 = const_int_pos_24;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_subheader_pointer_length_x64, tmp_assign_source_46 );
    tmp_assign_source_47 = const_int_0;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_page_type_offset, tmp_assign_source_47 );
    tmp_assign_source_48 = const_int_pos_2;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_page_type_length, tmp_assign_source_48 );
    tmp_assign_source_49 = const_int_pos_2;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_block_count_offset, tmp_assign_source_49 );
    tmp_assign_source_50 = const_int_pos_2;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_block_count_length, tmp_assign_source_50 );
    tmp_assign_source_51 = const_int_pos_4;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_subheader_count_offset, tmp_assign_source_51 );
    tmp_assign_source_52 = const_int_pos_2;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_subheader_count_length, tmp_assign_source_52 );
    tmp_assign_source_53 = const_int_0;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_page_meta_type, tmp_assign_source_53 );
    tmp_assign_source_54 = const_int_pos_256;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_page_data_type, tmp_assign_source_54 );
    tmp_assign_source_55 = const_int_pos_1024;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_page_amd_type, tmp_assign_source_55 );
    tmp_assign_source_56 = const_int_pos_16384;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_page_metc_type, tmp_assign_source_56 );
    tmp_assign_source_57 = const_int_neg_28672;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_page_comp_type, tmp_assign_source_57 );
    tmp_assign_source_58 = LIST_COPY( const_list_int_pos_512_int_pos_640_list );
    UPDATE_STRING_DICT1( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_page_mix_types, tmp_assign_source_58 );
    tmp_assign_source_59 = const_int_pos_8;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_subheader_pointers_offset, tmp_assign_source_59 );
    tmp_assign_source_60 = const_int_pos_1;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_truncated_subheader_id, tmp_assign_source_60 );
    tmp_assign_source_61 = const_int_pos_4;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_compressed_subheader_id, tmp_assign_source_61 );
    tmp_assign_source_62 = const_int_pos_1;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_compressed_subheader_type, tmp_assign_source_62 );
    tmp_assign_source_63 = const_int_pos_2;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_text_block_size_length, tmp_assign_source_63 );
    tmp_assign_source_64 = const_int_pos_5;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_row_length_offset_multiplier, tmp_assign_source_64 );
    tmp_assign_source_65 = const_int_pos_6;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_row_count_offset_multiplier, tmp_assign_source_65 );
    tmp_assign_source_66 = const_int_pos_9;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_col_count_p1_multiplier, tmp_assign_source_66 );
    tmp_assign_source_67 = const_int_pos_10;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_col_count_p2_multiplier, tmp_assign_source_67 );
    tmp_assign_source_68 = const_int_pos_15;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_row_count_on_mix_page_offset_multiplier, tmp_assign_source_68 );
    tmp_assign_source_69 = const_int_pos_8;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_column_name_pointer_length, tmp_assign_source_69 );
    tmp_assign_source_70 = const_int_0;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_column_name_text_subheader_offset, tmp_assign_source_70 );
    tmp_assign_source_71 = const_int_pos_2;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_column_name_text_subheader_length, tmp_assign_source_71 );
    tmp_assign_source_72 = const_int_pos_2;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_column_name_offset_offset, tmp_assign_source_72 );
    tmp_assign_source_73 = const_int_pos_2;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_column_name_offset_length, tmp_assign_source_73 );
    tmp_assign_source_74 = const_int_pos_4;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_column_name_length_offset, tmp_assign_source_74 );
    tmp_assign_source_75 = const_int_pos_2;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_column_name_length_length, tmp_assign_source_75 );
    tmp_assign_source_76 = const_int_pos_8;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_column_data_offset_offset, tmp_assign_source_76 );
    tmp_assign_source_77 = const_int_pos_8;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_column_data_length_offset, tmp_assign_source_77 );
    tmp_assign_source_78 = const_int_pos_4;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_column_data_length_length, tmp_assign_source_78 );
    tmp_assign_source_79 = const_int_pos_14;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_column_type_offset, tmp_assign_source_79 );
    tmp_assign_source_80 = const_int_pos_1;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_column_type_length, tmp_assign_source_80 );
    tmp_assign_source_81 = const_int_pos_22;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_digest_ae46cea7a5d40de20d94e2097e781c74, tmp_assign_source_81 );
    tmp_assign_source_82 = const_int_pos_2;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_digest_2b8598a93a9d850a4fe0f9dc2b5e1d58, tmp_assign_source_82 );
    tmp_assign_source_83 = const_int_pos_24;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_column_format_offset_offset, tmp_assign_source_83 );
    tmp_assign_source_84 = const_int_pos_2;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_column_format_offset_length, tmp_assign_source_84 );
    tmp_assign_source_85 = const_int_pos_26;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_column_format_length_offset, tmp_assign_source_85 );
    tmp_assign_source_86 = const_int_pos_2;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_column_format_length_length, tmp_assign_source_86 );
    tmp_assign_source_87 = const_int_pos_28;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_column_label_text_subheader_index_offset, tmp_assign_source_87 );
    tmp_assign_source_88 = const_int_pos_2;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_column_label_text_subheader_index_length, tmp_assign_source_88 );
    tmp_assign_source_89 = const_int_pos_30;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_column_label_offset_offset, tmp_assign_source_89 );
    tmp_assign_source_90 = const_int_pos_2;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_column_label_offset_length, tmp_assign_source_90 );
    tmp_assign_source_91 = const_int_pos_32;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_column_label_length_offset, tmp_assign_source_91 );
    tmp_assign_source_92 = const_int_pos_2;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_column_label_length_length, tmp_assign_source_92 );
    tmp_assign_source_93 = const_bytes_digest_af50e0b1f8353213f0e5ece1c0699616;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_rle_compression, tmp_assign_source_93 );
    tmp_assign_source_94 = const_bytes_digest_a1d80f478ab526c69ae07e5b79858942;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_rdc_compression, tmp_assign_source_94 );
    tmp_list_element_1 = GET_STRING_DICT_VALUE( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_rle_compression );

    if (unlikely( tmp_list_element_1 == NULL ))
    {
        tmp_list_element_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rle_compression );
    }

    CHECK_OBJECT( tmp_list_element_1 );
    tmp_assign_source_95 = PyList_New( 2 );
    Py_INCREF( tmp_list_element_1 );
    PyList_SET_ITEM( tmp_assign_source_95, 0, tmp_list_element_1 );
    tmp_list_element_1 = GET_STRING_DICT_VALUE( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_rdc_compression );

    if (unlikely( tmp_list_element_1 == NULL ))
    {
        tmp_list_element_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_rdc_compression );
    }

    CHECK_OBJECT( tmp_list_element_1 );
    Py_INCREF( tmp_list_element_1 );
    PyList_SET_ITEM( tmp_assign_source_95, 1, tmp_list_element_1 );
    UPDATE_STRING_DICT1( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_compression_literals, tmp_assign_source_95 );
    tmp_assign_source_96 = PyDict_Copy( const_dict_0123bc20b88d0f6fe2421c53b17ca28e );
    UPDATE_STRING_DICT1( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_encoding_names, tmp_assign_source_96 );
    tmp_assign_source_97 = PyDict_New();
    assert( tmp_class_creation_1__class_decl_dict == NULL );
    tmp_class_creation_1__class_decl_dict = tmp_assign_source_97;

    // Frame without reuse.
    frame_0c5b1fd0275e283c370e24b6506ffd15 = MAKE_MODULE_FRAME( codeobj_0c5b1fd0275e283c370e24b6506ffd15, module_pandas$io$sas$sas_constants );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_0c5b1fd0275e283c370e24b6506ffd15 );
    assert( Py_REFCNT( frame_0c5b1fd0275e283c370e24b6506ffd15 ) == 2 );

    // Framed code:
    // Tried code:
    tmp_key_name_1 = const_str_plain_metaclass;
    tmp_dict_name_1 = tmp_class_creation_1__class_decl_dict;

    CHECK_OBJECT( tmp_dict_name_1 );
    tmp_res = PyDict_Contains( tmp_dict_name_1, tmp_key_name_1 );
    if ( tmp_res == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 105;

        goto try_except_handler_1;
    }
    tmp_cond_value_1 = BOOL_FROM( tmp_res == 1 );
    tmp_cond_truth_1 = CHECK_IF_TRUE( tmp_cond_value_1 );
    if ( tmp_cond_truth_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 105;

        goto try_except_handler_1;
    }
    if ( tmp_cond_truth_1 == 1 )
    {
        goto condexpr_true_1;
    }
    else
    {
        goto condexpr_false_1;
    }
    condexpr_true_1:;
    tmp_dict_name_2 = tmp_class_creation_1__class_decl_dict;

    CHECK_OBJECT( tmp_dict_name_2 );
    tmp_key_name_2 = const_str_plain_metaclass;
    tmp_metaclass_name_1 = DICT_GET_ITEM( tmp_dict_name_2, tmp_key_name_2 );
    if ( tmp_metaclass_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 105;

        goto try_except_handler_1;
    }
    goto condexpr_end_1;
    condexpr_false_1:;
    tmp_metaclass_name_1 = LOOKUP_BUILTIN( const_str_plain_type );
    assert( tmp_metaclass_name_1 != NULL );
    Py_INCREF( tmp_metaclass_name_1 );
    condexpr_end_1:;
    tmp_bases_name_1 = const_tuple_type_object_tuple;
    tmp_assign_source_98 = SELECT_METACLASS( tmp_metaclass_name_1, tmp_bases_name_1 );
    Py_DECREF( tmp_metaclass_name_1 );
    if ( tmp_assign_source_98 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 105;

        goto try_except_handler_1;
    }
    assert( tmp_class_creation_1__metaclass == NULL );
    tmp_class_creation_1__metaclass = tmp_assign_source_98;

    tmp_key_name_3 = const_str_plain_metaclass;
    tmp_dict_name_3 = tmp_class_creation_1__class_decl_dict;

    CHECK_OBJECT( tmp_dict_name_3 );
    tmp_res = PyDict_Contains( tmp_dict_name_3, tmp_key_name_3 );
    if ( tmp_res == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 105;

        goto try_except_handler_1;
    }
    tmp_cond_value_2 = BOOL_FROM( tmp_res == 1 );
    tmp_cond_truth_2 = CHECK_IF_TRUE( tmp_cond_value_2 );
    if ( tmp_cond_truth_2 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 105;

        goto try_except_handler_1;
    }
    if ( tmp_cond_truth_2 == 1 )
    {
        goto branch_yes_1;
    }
    else
    {
        goto branch_no_1;
    }
    branch_yes_1:;
    tmp_dictdel_dict = tmp_class_creation_1__class_decl_dict;

    CHECK_OBJECT( tmp_dictdel_dict );
    tmp_dictdel_key = const_str_plain_metaclass;
    tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 105;

        goto try_except_handler_1;
    }
    branch_no_1:;
    tmp_hasattr_source_1 = tmp_class_creation_1__metaclass;

    CHECK_OBJECT( tmp_hasattr_source_1 );
    tmp_hasattr_attr_1 = const_str_plain___prepare__;
    tmp_res = PyObject_HasAttr( tmp_hasattr_source_1, tmp_hasattr_attr_1 );
    if ( tmp_res == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 105;

        goto try_except_handler_1;
    }
    if ( tmp_res == 1 )
    {
        goto condexpr_true_2;
    }
    else
    {
        goto condexpr_false_2;
    }
    condexpr_true_2:;
    tmp_source_name_1 = tmp_class_creation_1__metaclass;

    CHECK_OBJECT( tmp_source_name_1 );
    tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___prepare__ );
    if ( tmp_called_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 105;

        goto try_except_handler_1;
    }
    tmp_args_name_1 = const_tuple_str_plain_SASIndex_tuple_type_object_tuple_tuple;
    tmp_kw_name_1 = tmp_class_creation_1__class_decl_dict;

    CHECK_OBJECT( tmp_kw_name_1 );
    frame_0c5b1fd0275e283c370e24b6506ffd15->m_frame.f_lineno = 105;
    tmp_assign_source_99 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
    Py_DECREF( tmp_called_name_1 );
    if ( tmp_assign_source_99 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 105;

        goto try_except_handler_1;
    }
    goto condexpr_end_2;
    condexpr_false_2:;
    tmp_assign_source_99 = PyDict_New();
    condexpr_end_2:;
    assert( tmp_class_creation_1__prepared == NULL );
    tmp_class_creation_1__prepared = tmp_assign_source_99;

    tmp_set_locals = tmp_class_creation_1__prepared;

    CHECK_OBJECT( tmp_set_locals );
    locals_pandas$io$sas$sas_constants_105 = tmp_set_locals;
    Py_INCREF( tmp_set_locals );
    // Tried code:
    // Tried code:
    tmp_dictset_value = const_str_digest_d7abe4970418126ea4a162358a8d7308;
    tmp_res = PyObject_SetItem( locals_pandas$io$sas$sas_constants_105, const_str_plain___module__, tmp_dictset_value );
    if ( tmp_res != 0 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 105;

        goto try_except_handler_3;
    }
    tmp_dictset_value = const_str_plain_SASIndex;
    tmp_res = PyObject_SetItem( locals_pandas$io$sas$sas_constants_105, const_str_plain___qualname__, tmp_dictset_value );
    if ( tmp_res != 0 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 105;

        goto try_except_handler_3;
    }
    MAKE_OR_REUSE_FRAME( cache_frame_615de7a2697e1671c8dc1d4384fd8117_2, codeobj_615de7a2697e1671c8dc1d4384fd8117, module_pandas$io$sas$sas_constants, sizeof(void *) );
    frame_615de7a2697e1671c8dc1d4384fd8117_2 = cache_frame_615de7a2697e1671c8dc1d4384fd8117_2;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_615de7a2697e1671c8dc1d4384fd8117_2 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_615de7a2697e1671c8dc1d4384fd8117_2 ) == 2 ); // Frame stack

    // Framed code:
    tmp_dictset_value = const_int_0;
    tmp_res = PyObject_SetItem( locals_pandas$io$sas$sas_constants_105, const_str_plain_row_size_index, tmp_dictset_value );
    if ( tmp_res != 0 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 106;
        type_description_2 = "N";
        goto frame_exception_exit_2;
    }
    tmp_dictset_value = const_int_pos_1;
    tmp_res = PyObject_SetItem( locals_pandas$io$sas$sas_constants_105, const_str_plain_column_size_index, tmp_dictset_value );
    if ( tmp_res != 0 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 107;
        type_description_2 = "N";
        goto frame_exception_exit_2;
    }
    tmp_dictset_value = const_int_pos_2;
    tmp_res = PyObject_SetItem( locals_pandas$io$sas$sas_constants_105, const_str_plain_subheader_counts_index, tmp_dictset_value );
    if ( tmp_res != 0 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 108;
        type_description_2 = "N";
        goto frame_exception_exit_2;
    }
    tmp_dictset_value = const_int_pos_3;
    tmp_res = PyObject_SetItem( locals_pandas$io$sas$sas_constants_105, const_str_plain_column_text_index, tmp_dictset_value );
    if ( tmp_res != 0 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 109;
        type_description_2 = "N";
        goto frame_exception_exit_2;
    }
    tmp_dictset_value = const_int_pos_4;
    tmp_res = PyObject_SetItem( locals_pandas$io$sas$sas_constants_105, const_str_plain_column_name_index, tmp_dictset_value );
    if ( tmp_res != 0 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 110;
        type_description_2 = "N";
        goto frame_exception_exit_2;
    }
    tmp_dictset_value = const_int_pos_5;
    tmp_res = PyObject_SetItem( locals_pandas$io$sas$sas_constants_105, const_str_plain_column_attributes_index, tmp_dictset_value );
    if ( tmp_res != 0 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 111;
        type_description_2 = "N";
        goto frame_exception_exit_2;
    }
    tmp_dictset_value = const_int_pos_6;
    tmp_res = PyObject_SetItem( locals_pandas$io$sas$sas_constants_105, const_str_plain_format_and_label_index, tmp_dictset_value );
    if ( tmp_res != 0 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 112;
        type_description_2 = "N";
        goto frame_exception_exit_2;
    }
    tmp_dictset_value = const_int_pos_7;
    tmp_res = PyObject_SetItem( locals_pandas$io$sas$sas_constants_105, const_str_plain_column_list_index, tmp_dictset_value );
    if ( tmp_res != 0 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 113;
        type_description_2 = "N";
        goto frame_exception_exit_2;
    }
    tmp_dictset_value = const_int_pos_8;
    tmp_res = PyObject_SetItem( locals_pandas$io$sas$sas_constants_105, const_str_plain_data_subheader_index, tmp_dictset_value );
    if ( tmp_res != 0 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 114;
        type_description_2 = "N";
        goto frame_exception_exit_2;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_615de7a2697e1671c8dc1d4384fd8117_2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_2:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_615de7a2697e1671c8dc1d4384fd8117_2 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_615de7a2697e1671c8dc1d4384fd8117_2, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_615de7a2697e1671c8dc1d4384fd8117_2->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_615de7a2697e1671c8dc1d4384fd8117_2, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_615de7a2697e1671c8dc1d4384fd8117_2,
        type_description_2,
        NULL
    );


    // Release cached frame.
    if ( frame_615de7a2697e1671c8dc1d4384fd8117_2 == cache_frame_615de7a2697e1671c8dc1d4384fd8117_2 )
    {
        Py_DECREF( frame_615de7a2697e1671c8dc1d4384fd8117_2 );
    }
    cache_frame_615de7a2697e1671c8dc1d4384fd8117_2 = NULL;

    assertFrameObject( frame_615de7a2697e1671c8dc1d4384fd8117_2 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto nested_frame_exit_1;

    frame_no_exception_1:;

    goto skip_nested_handling_1;
    nested_frame_exit_1:;

    goto try_except_handler_3;
    skip_nested_handling_1:;
    tmp_called_name_2 = tmp_class_creation_1__metaclass;

    CHECK_OBJECT( tmp_called_name_2 );
    tmp_tuple_element_1 = const_str_plain_SASIndex;
    tmp_args_name_2 = PyTuple_New( 3 );
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_1 );
    tmp_tuple_element_1 = const_tuple_type_object_tuple;
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_1 );
    tmp_tuple_element_1 = locals_pandas$io$sas$sas_constants_105;
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_args_name_2, 2, tmp_tuple_element_1 );
    tmp_kw_name_2 = tmp_class_creation_1__class_decl_dict;

    CHECK_OBJECT( tmp_kw_name_2 );
    frame_0c5b1fd0275e283c370e24b6506ffd15->m_frame.f_lineno = 105;
    tmp_assign_source_101 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_2, tmp_kw_name_2 );
    Py_DECREF( tmp_args_name_2 );
    if ( tmp_assign_source_101 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 105;

        goto try_except_handler_3;
    }
    assert( outline_0_var___class__ == NULL );
    outline_0_var___class__ = tmp_assign_source_101;

    tmp_outline_return_value_1 = outline_0_var___class__;

    CHECK_OBJECT( tmp_outline_return_value_1 );
    Py_INCREF( tmp_outline_return_value_1 );
    goto try_return_handler_3;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$io$sas$sas_constants );
    return MOD_RETURN_VALUE( NULL );
    // Return handler code:
    try_return_handler_3:;
    Py_DECREF( locals_pandas$io$sas$sas_constants_105 );
    locals_pandas$io$sas$sas_constants_105 = NULL;
    goto try_return_handler_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_DECREF( locals_pandas$io$sas$sas_constants_105 );
    locals_pandas$io$sas$sas_constants_105 = NULL;
    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_2;
    // End of try:
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$io$sas$sas_constants );
    return MOD_RETURN_VALUE( NULL );
    // Return handler code:
    try_return_handler_2:;
    CHECK_OBJECT( (PyObject *)outline_0_var___class__ );
    Py_DECREF( outline_0_var___class__ );
    outline_0_var___class__ = NULL;

    goto outline_result_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto outline_exception_1;
    // End of try:
    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$io$sas$sas_constants );
    return MOD_RETURN_VALUE( NULL );
    outline_exception_1:;
    exception_lineno = 105;
    goto try_except_handler_1;
    outline_result_1:;
    tmp_assign_source_100 = tmp_outline_return_value_1;
    UPDATE_STRING_DICT1( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_SASIndex, tmp_assign_source_100 );
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_decl_dict );
    Py_DECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_decl_dict );
    Py_DECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__metaclass );
    Py_DECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__prepared );
    Py_DECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    tmp_dict_key_1 = const_bytes_digest_a9b059e744598d7da903858ccdd9811e;
    tmp_source_name_2 = GET_STRING_DICT_VALUE( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_SASIndex );

    if (unlikely( tmp_source_name_2 == NULL ))
    {
        tmp_source_name_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SASIndex );
    }

    if ( tmp_source_name_2 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SASIndex" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 118;

        goto frame_exception_exit_1;
    }

    tmp_dict_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_row_size_index );
    if ( tmp_dict_value_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 118;

        goto frame_exception_exit_1;
    }
    tmp_assign_source_102 = _PyDict_NewPresized( 30 );
    tmp_res = PyDict_SetItem( tmp_assign_source_102, tmp_dict_key_1, tmp_dict_value_1 );
    Py_DECREF( tmp_dict_value_1 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_2 = const_bytes_digest_db4b0b175ba9aef87730a428c24a1572;
    tmp_source_name_3 = GET_STRING_DICT_VALUE( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_SASIndex );

    if (unlikely( tmp_source_name_3 == NULL ))
    {
        tmp_source_name_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SASIndex );
    }

    if ( tmp_source_name_3 == NULL )
    {
        Py_DECREF( tmp_assign_source_102 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SASIndex" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 119;

        goto frame_exception_exit_1;
    }

    tmp_dict_value_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_row_size_index );
    if ( tmp_dict_value_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assign_source_102 );

        exception_lineno = 119;

        goto frame_exception_exit_1;
    }
    tmp_res = PyDict_SetItem( tmp_assign_source_102, tmp_dict_key_2, tmp_dict_value_2 );
    Py_DECREF( tmp_dict_value_2 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_3 = const_bytes_digest_ed708fae07749a569ac1580098f03054;
    tmp_source_name_4 = GET_STRING_DICT_VALUE( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_SASIndex );

    if (unlikely( tmp_source_name_4 == NULL ))
    {
        tmp_source_name_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SASIndex );
    }

    if ( tmp_source_name_4 == NULL )
    {
        Py_DECREF( tmp_assign_source_102 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SASIndex" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 120;

        goto frame_exception_exit_1;
    }

    tmp_dict_value_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_row_size_index );
    if ( tmp_dict_value_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assign_source_102 );

        exception_lineno = 120;

        goto frame_exception_exit_1;
    }
    tmp_res = PyDict_SetItem( tmp_assign_source_102, tmp_dict_key_3, tmp_dict_value_3 );
    Py_DECREF( tmp_dict_value_3 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_4 = const_bytes_digest_d53251f9dd15a3ac8797e1596f709079;
    tmp_source_name_5 = GET_STRING_DICT_VALUE( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_SASIndex );

    if (unlikely( tmp_source_name_5 == NULL ))
    {
        tmp_source_name_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SASIndex );
    }

    if ( tmp_source_name_5 == NULL )
    {
        Py_DECREF( tmp_assign_source_102 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SASIndex" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 121;

        goto frame_exception_exit_1;
    }

    tmp_dict_value_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_row_size_index );
    if ( tmp_dict_value_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assign_source_102 );

        exception_lineno = 121;

        goto frame_exception_exit_1;
    }
    tmp_res = PyDict_SetItem( tmp_assign_source_102, tmp_dict_key_4, tmp_dict_value_4 );
    Py_DECREF( tmp_dict_value_4 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_5 = const_bytes_digest_371494012c6e9066fad0356c424e96ad;
    tmp_source_name_6 = GET_STRING_DICT_VALUE( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_SASIndex );

    if (unlikely( tmp_source_name_6 == NULL ))
    {
        tmp_source_name_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SASIndex );
    }

    if ( tmp_source_name_6 == NULL )
    {
        Py_DECREF( tmp_assign_source_102 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SASIndex" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 122;

        goto frame_exception_exit_1;
    }

    tmp_dict_value_5 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_column_size_index );
    if ( tmp_dict_value_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assign_source_102 );

        exception_lineno = 122;

        goto frame_exception_exit_1;
    }
    tmp_res = PyDict_SetItem( tmp_assign_source_102, tmp_dict_key_5, tmp_dict_value_5 );
    Py_DECREF( tmp_dict_value_5 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_6 = const_bytes_digest_863bca74cb259ab109ddadd210bd835c;
    tmp_source_name_7 = GET_STRING_DICT_VALUE( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_SASIndex );

    if (unlikely( tmp_source_name_7 == NULL ))
    {
        tmp_source_name_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SASIndex );
    }

    if ( tmp_source_name_7 == NULL )
    {
        Py_DECREF( tmp_assign_source_102 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SASIndex" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 123;

        goto frame_exception_exit_1;
    }

    tmp_dict_value_6 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_column_size_index );
    if ( tmp_dict_value_6 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assign_source_102 );

        exception_lineno = 123;

        goto frame_exception_exit_1;
    }
    tmp_res = PyDict_SetItem( tmp_assign_source_102, tmp_dict_key_6, tmp_dict_value_6 );
    Py_DECREF( tmp_dict_value_6 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_7 = const_bytes_digest_c810f73b15dc26066186ea8612817866;
    tmp_source_name_8 = GET_STRING_DICT_VALUE( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_SASIndex );

    if (unlikely( tmp_source_name_8 == NULL ))
    {
        tmp_source_name_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SASIndex );
    }

    if ( tmp_source_name_8 == NULL )
    {
        Py_DECREF( tmp_assign_source_102 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SASIndex" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 124;

        goto frame_exception_exit_1;
    }

    tmp_dict_value_7 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_column_size_index );
    if ( tmp_dict_value_7 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assign_source_102 );

        exception_lineno = 124;

        goto frame_exception_exit_1;
    }
    tmp_res = PyDict_SetItem( tmp_assign_source_102, tmp_dict_key_7, tmp_dict_value_7 );
    Py_DECREF( tmp_dict_value_7 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_8 = const_bytes_digest_9b0c608657dd661f731eab454dbc3699;
    tmp_source_name_9 = GET_STRING_DICT_VALUE( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_SASIndex );

    if (unlikely( tmp_source_name_9 == NULL ))
    {
        tmp_source_name_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SASIndex );
    }

    if ( tmp_source_name_9 == NULL )
    {
        Py_DECREF( tmp_assign_source_102 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SASIndex" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 125;

        goto frame_exception_exit_1;
    }

    tmp_dict_value_8 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_column_size_index );
    if ( tmp_dict_value_8 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assign_source_102 );

        exception_lineno = 125;

        goto frame_exception_exit_1;
    }
    tmp_res = PyDict_SetItem( tmp_assign_source_102, tmp_dict_key_8, tmp_dict_value_8 );
    Py_DECREF( tmp_dict_value_8 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_9 = const_bytes_digest_84b28972237bb98ee2c1ab8ea8e07175;
    tmp_source_name_10 = GET_STRING_DICT_VALUE( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_SASIndex );

    if (unlikely( tmp_source_name_10 == NULL ))
    {
        tmp_source_name_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SASIndex );
    }

    if ( tmp_source_name_10 == NULL )
    {
        Py_DECREF( tmp_assign_source_102 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SASIndex" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 126;

        goto frame_exception_exit_1;
    }

    tmp_dict_value_9 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_subheader_counts_index );
    if ( tmp_dict_value_9 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assign_source_102 );

        exception_lineno = 126;

        goto frame_exception_exit_1;
    }
    tmp_res = PyDict_SetItem( tmp_assign_source_102, tmp_dict_key_9, tmp_dict_value_9 );
    Py_DECREF( tmp_dict_value_9 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_10 = const_bytes_digest_a191b8911b51ba182c05402179c28694;
    tmp_source_name_11 = GET_STRING_DICT_VALUE( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_SASIndex );

    if (unlikely( tmp_source_name_11 == NULL ))
    {
        tmp_source_name_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SASIndex );
    }

    if ( tmp_source_name_11 == NULL )
    {
        Py_DECREF( tmp_assign_source_102 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SASIndex" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 127;

        goto frame_exception_exit_1;
    }

    tmp_dict_value_10 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_subheader_counts_index );
    if ( tmp_dict_value_10 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assign_source_102 );

        exception_lineno = 127;

        goto frame_exception_exit_1;
    }
    tmp_res = PyDict_SetItem( tmp_assign_source_102, tmp_dict_key_10, tmp_dict_value_10 );
    Py_DECREF( tmp_dict_value_10 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_11 = const_bytes_digest_a3409d201e8d37abc17850c169b2101b;
    tmp_source_name_12 = GET_STRING_DICT_VALUE( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_SASIndex );

    if (unlikely( tmp_source_name_12 == NULL ))
    {
        tmp_source_name_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SASIndex );
    }

    if ( tmp_source_name_12 == NULL )
    {
        Py_DECREF( tmp_assign_source_102 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SASIndex" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 128;

        goto frame_exception_exit_1;
    }

    tmp_dict_value_11 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_subheader_counts_index );
    if ( tmp_dict_value_11 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assign_source_102 );

        exception_lineno = 128;

        goto frame_exception_exit_1;
    }
    tmp_res = PyDict_SetItem( tmp_assign_source_102, tmp_dict_key_11, tmp_dict_value_11 );
    Py_DECREF( tmp_dict_value_11 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_12 = const_bytes_digest_1c0ca65d5f878e589b6273f290ae8f83;
    tmp_source_name_13 = GET_STRING_DICT_VALUE( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_SASIndex );

    if (unlikely( tmp_source_name_13 == NULL ))
    {
        tmp_source_name_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SASIndex );
    }

    if ( tmp_source_name_13 == NULL )
    {
        Py_DECREF( tmp_assign_source_102 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SASIndex" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 129;

        goto frame_exception_exit_1;
    }

    tmp_dict_value_12 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain_subheader_counts_index );
    if ( tmp_dict_value_12 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assign_source_102 );

        exception_lineno = 129;

        goto frame_exception_exit_1;
    }
    tmp_res = PyDict_SetItem( tmp_assign_source_102, tmp_dict_key_12, tmp_dict_value_12 );
    Py_DECREF( tmp_dict_value_12 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_13 = const_bytes_digest_86d19990f68076a18805eaeaa3cd5116;
    tmp_source_name_14 = GET_STRING_DICT_VALUE( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_SASIndex );

    if (unlikely( tmp_source_name_14 == NULL ))
    {
        tmp_source_name_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SASIndex );
    }

    if ( tmp_source_name_14 == NULL )
    {
        Py_DECREF( tmp_assign_source_102 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SASIndex" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 130;

        goto frame_exception_exit_1;
    }

    tmp_dict_value_13 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain_column_text_index );
    if ( tmp_dict_value_13 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assign_source_102 );

        exception_lineno = 130;

        goto frame_exception_exit_1;
    }
    tmp_res = PyDict_SetItem( tmp_assign_source_102, tmp_dict_key_13, tmp_dict_value_13 );
    Py_DECREF( tmp_dict_value_13 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_14 = const_bytes_digest_0cedb7bd5b803de96cbad647ccfe4e03;
    tmp_source_name_15 = GET_STRING_DICT_VALUE( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_SASIndex );

    if (unlikely( tmp_source_name_15 == NULL ))
    {
        tmp_source_name_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SASIndex );
    }

    if ( tmp_source_name_15 == NULL )
    {
        Py_DECREF( tmp_assign_source_102 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SASIndex" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 131;

        goto frame_exception_exit_1;
    }

    tmp_dict_value_14 = LOOKUP_ATTRIBUTE( tmp_source_name_15, const_str_plain_column_text_index );
    if ( tmp_dict_value_14 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assign_source_102 );

        exception_lineno = 131;

        goto frame_exception_exit_1;
    }
    tmp_res = PyDict_SetItem( tmp_assign_source_102, tmp_dict_key_14, tmp_dict_value_14 );
    Py_DECREF( tmp_dict_value_14 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_15 = const_bytes_digest_7d41b59312be655f2613543574f86276;
    tmp_source_name_16 = GET_STRING_DICT_VALUE( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_SASIndex );

    if (unlikely( tmp_source_name_16 == NULL ))
    {
        tmp_source_name_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SASIndex );
    }

    if ( tmp_source_name_16 == NULL )
    {
        Py_DECREF( tmp_assign_source_102 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SASIndex" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 132;

        goto frame_exception_exit_1;
    }

    tmp_dict_value_15 = LOOKUP_ATTRIBUTE( tmp_source_name_16, const_str_plain_column_text_index );
    if ( tmp_dict_value_15 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assign_source_102 );

        exception_lineno = 132;

        goto frame_exception_exit_1;
    }
    tmp_res = PyDict_SetItem( tmp_assign_source_102, tmp_dict_key_15, tmp_dict_value_15 );
    Py_DECREF( tmp_dict_value_15 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_16 = const_bytes_digest_d9137199bb9ab3861ec2d1e3544d9093;
    tmp_source_name_17 = GET_STRING_DICT_VALUE( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_SASIndex );

    if (unlikely( tmp_source_name_17 == NULL ))
    {
        tmp_source_name_17 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SASIndex );
    }

    if ( tmp_source_name_17 == NULL )
    {
        Py_DECREF( tmp_assign_source_102 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SASIndex" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 133;

        goto frame_exception_exit_1;
    }

    tmp_dict_value_16 = LOOKUP_ATTRIBUTE( tmp_source_name_17, const_str_plain_column_text_index );
    if ( tmp_dict_value_16 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assign_source_102 );

        exception_lineno = 133;

        goto frame_exception_exit_1;
    }
    tmp_res = PyDict_SetItem( tmp_assign_source_102, tmp_dict_key_16, tmp_dict_value_16 );
    Py_DECREF( tmp_dict_value_16 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_17 = const_bytes_digest_6870746d51334ef2099a2b9f963fd5b8;
    tmp_source_name_18 = GET_STRING_DICT_VALUE( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_SASIndex );

    if (unlikely( tmp_source_name_18 == NULL ))
    {
        tmp_source_name_18 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SASIndex );
    }

    if ( tmp_source_name_18 == NULL )
    {
        Py_DECREF( tmp_assign_source_102 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SASIndex" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 134;

        goto frame_exception_exit_1;
    }

    tmp_dict_value_17 = LOOKUP_ATTRIBUTE( tmp_source_name_18, const_str_plain_column_name_index );
    if ( tmp_dict_value_17 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assign_source_102 );

        exception_lineno = 134;

        goto frame_exception_exit_1;
    }
    tmp_res = PyDict_SetItem( tmp_assign_source_102, tmp_dict_key_17, tmp_dict_value_17 );
    Py_DECREF( tmp_dict_value_17 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_18 = const_bytes_digest_ff4b4aeb620151ff8ce1a51a85c83367;
    tmp_source_name_19 = GET_STRING_DICT_VALUE( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_SASIndex );

    if (unlikely( tmp_source_name_19 == NULL ))
    {
        tmp_source_name_19 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SASIndex );
    }

    if ( tmp_source_name_19 == NULL )
    {
        Py_DECREF( tmp_assign_source_102 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SASIndex" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 135;

        goto frame_exception_exit_1;
    }

    tmp_dict_value_18 = LOOKUP_ATTRIBUTE( tmp_source_name_19, const_str_plain_column_name_index );
    if ( tmp_dict_value_18 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assign_source_102 );

        exception_lineno = 135;

        goto frame_exception_exit_1;
    }
    tmp_res = PyDict_SetItem( tmp_assign_source_102, tmp_dict_key_18, tmp_dict_value_18 );
    Py_DECREF( tmp_dict_value_18 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_19 = const_bytes_digest_0cbbc6b153b9478dbbc478823bedd0c4;
    tmp_source_name_20 = GET_STRING_DICT_VALUE( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_SASIndex );

    if (unlikely( tmp_source_name_20 == NULL ))
    {
        tmp_source_name_20 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SASIndex );
    }

    if ( tmp_source_name_20 == NULL )
    {
        Py_DECREF( tmp_assign_source_102 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SASIndex" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 136;

        goto frame_exception_exit_1;
    }

    tmp_dict_value_19 = LOOKUP_ATTRIBUTE( tmp_source_name_20, const_str_plain_column_attributes_index );
    if ( tmp_dict_value_19 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assign_source_102 );

        exception_lineno = 136;

        goto frame_exception_exit_1;
    }
    tmp_res = PyDict_SetItem( tmp_assign_source_102, tmp_dict_key_19, tmp_dict_value_19 );
    Py_DECREF( tmp_dict_value_19 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_20 = const_bytes_digest_49bf45ff4d19405340c3b92671ed9a15;
    tmp_source_name_21 = GET_STRING_DICT_VALUE( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_SASIndex );

    if (unlikely( tmp_source_name_21 == NULL ))
    {
        tmp_source_name_21 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SASIndex );
    }

    if ( tmp_source_name_21 == NULL )
    {
        Py_DECREF( tmp_assign_source_102 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SASIndex" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 137;

        goto frame_exception_exit_1;
    }

    tmp_dict_value_20 = LOOKUP_ATTRIBUTE( tmp_source_name_21, const_str_plain_column_attributes_index );
    if ( tmp_dict_value_20 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assign_source_102 );

        exception_lineno = 137;

        goto frame_exception_exit_1;
    }
    tmp_res = PyDict_SetItem( tmp_assign_source_102, tmp_dict_key_20, tmp_dict_value_20 );
    Py_DECREF( tmp_dict_value_20 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_21 = const_bytes_digest_6eae831552cced2cc18cf13cc97984cc;
    tmp_source_name_22 = GET_STRING_DICT_VALUE( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_SASIndex );

    if (unlikely( tmp_source_name_22 == NULL ))
    {
        tmp_source_name_22 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SASIndex );
    }

    if ( tmp_source_name_22 == NULL )
    {
        Py_DECREF( tmp_assign_source_102 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SASIndex" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 138;

        goto frame_exception_exit_1;
    }

    tmp_dict_value_21 = LOOKUP_ATTRIBUTE( tmp_source_name_22, const_str_plain_column_attributes_index );
    if ( tmp_dict_value_21 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assign_source_102 );

        exception_lineno = 138;

        goto frame_exception_exit_1;
    }
    tmp_res = PyDict_SetItem( tmp_assign_source_102, tmp_dict_key_21, tmp_dict_value_21 );
    Py_DECREF( tmp_dict_value_21 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_22 = const_bytes_digest_01bdfbd570fb89653774c25ee2eefd45;
    tmp_source_name_23 = GET_STRING_DICT_VALUE( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_SASIndex );

    if (unlikely( tmp_source_name_23 == NULL ))
    {
        tmp_source_name_23 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SASIndex );
    }

    if ( tmp_source_name_23 == NULL )
    {
        Py_DECREF( tmp_assign_source_102 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SASIndex" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 139;

        goto frame_exception_exit_1;
    }

    tmp_dict_value_22 = LOOKUP_ATTRIBUTE( tmp_source_name_23, const_str_plain_column_attributes_index );
    if ( tmp_dict_value_22 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assign_source_102 );

        exception_lineno = 139;

        goto frame_exception_exit_1;
    }
    tmp_res = PyDict_SetItem( tmp_assign_source_102, tmp_dict_key_22, tmp_dict_value_22 );
    Py_DECREF( tmp_dict_value_22 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_23 = const_bytes_digest_440f72b63ad60e5326317d1b066b8e52;
    tmp_source_name_24 = GET_STRING_DICT_VALUE( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_SASIndex );

    if (unlikely( tmp_source_name_24 == NULL ))
    {
        tmp_source_name_24 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SASIndex );
    }

    if ( tmp_source_name_24 == NULL )
    {
        Py_DECREF( tmp_assign_source_102 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SASIndex" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 140;

        goto frame_exception_exit_1;
    }

    tmp_dict_value_23 = LOOKUP_ATTRIBUTE( tmp_source_name_24, const_str_plain_format_and_label_index );
    if ( tmp_dict_value_23 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assign_source_102 );

        exception_lineno = 140;

        goto frame_exception_exit_1;
    }
    tmp_res = PyDict_SetItem( tmp_assign_source_102, tmp_dict_key_23, tmp_dict_value_23 );
    Py_DECREF( tmp_dict_value_23 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_24 = const_bytes_digest_75f14aa6f618c971bd0e1e6976f7cce0;
    tmp_source_name_25 = GET_STRING_DICT_VALUE( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_SASIndex );

    if (unlikely( tmp_source_name_25 == NULL ))
    {
        tmp_source_name_25 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SASIndex );
    }

    if ( tmp_source_name_25 == NULL )
    {
        Py_DECREF( tmp_assign_source_102 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SASIndex" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 141;

        goto frame_exception_exit_1;
    }

    tmp_dict_value_24 = LOOKUP_ATTRIBUTE( tmp_source_name_25, const_str_plain_format_and_label_index );
    if ( tmp_dict_value_24 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assign_source_102 );

        exception_lineno = 141;

        goto frame_exception_exit_1;
    }
    tmp_res = PyDict_SetItem( tmp_assign_source_102, tmp_dict_key_24, tmp_dict_value_24 );
    Py_DECREF( tmp_dict_value_24 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_25 = const_bytes_digest_2c7c72ce5e6dd7afdfc5cfe2c063e3c7;
    tmp_source_name_26 = GET_STRING_DICT_VALUE( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_SASIndex );

    if (unlikely( tmp_source_name_26 == NULL ))
    {
        tmp_source_name_26 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SASIndex );
    }

    if ( tmp_source_name_26 == NULL )
    {
        Py_DECREF( tmp_assign_source_102 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SASIndex" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 142;

        goto frame_exception_exit_1;
    }

    tmp_dict_value_25 = LOOKUP_ATTRIBUTE( tmp_source_name_26, const_str_plain_format_and_label_index );
    if ( tmp_dict_value_25 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assign_source_102 );

        exception_lineno = 142;

        goto frame_exception_exit_1;
    }
    tmp_res = PyDict_SetItem( tmp_assign_source_102, tmp_dict_key_25, tmp_dict_value_25 );
    Py_DECREF( tmp_dict_value_25 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_26 = const_bytes_digest_bd4cb956ae3f12a5edfd1945200324f1;
    tmp_source_name_27 = GET_STRING_DICT_VALUE( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_SASIndex );

    if (unlikely( tmp_source_name_27 == NULL ))
    {
        tmp_source_name_27 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SASIndex );
    }

    if ( tmp_source_name_27 == NULL )
    {
        Py_DECREF( tmp_assign_source_102 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SASIndex" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 143;

        goto frame_exception_exit_1;
    }

    tmp_dict_value_26 = LOOKUP_ATTRIBUTE( tmp_source_name_27, const_str_plain_format_and_label_index );
    if ( tmp_dict_value_26 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assign_source_102 );

        exception_lineno = 143;

        goto frame_exception_exit_1;
    }
    tmp_res = PyDict_SetItem( tmp_assign_source_102, tmp_dict_key_26, tmp_dict_value_26 );
    Py_DECREF( tmp_dict_value_26 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_27 = const_bytes_digest_4b7f385f5975b72c6fc161d4079c3321;
    tmp_source_name_28 = GET_STRING_DICT_VALUE( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_SASIndex );

    if (unlikely( tmp_source_name_28 == NULL ))
    {
        tmp_source_name_28 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SASIndex );
    }

    if ( tmp_source_name_28 == NULL )
    {
        Py_DECREF( tmp_assign_source_102 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SASIndex" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 144;

        goto frame_exception_exit_1;
    }

    tmp_dict_value_27 = LOOKUP_ATTRIBUTE( tmp_source_name_28, const_str_plain_column_list_index );
    if ( tmp_dict_value_27 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assign_source_102 );

        exception_lineno = 144;

        goto frame_exception_exit_1;
    }
    tmp_res = PyDict_SetItem( tmp_assign_source_102, tmp_dict_key_27, tmp_dict_value_27 );
    Py_DECREF( tmp_dict_value_27 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_28 = const_bytes_digest_f3d985e0a13e2108b4627a133e77d8a1;
    tmp_source_name_29 = GET_STRING_DICT_VALUE( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_SASIndex );

    if (unlikely( tmp_source_name_29 == NULL ))
    {
        tmp_source_name_29 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SASIndex );
    }

    if ( tmp_source_name_29 == NULL )
    {
        Py_DECREF( tmp_assign_source_102 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SASIndex" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 145;

        goto frame_exception_exit_1;
    }

    tmp_dict_value_28 = LOOKUP_ATTRIBUTE( tmp_source_name_29, const_str_plain_column_list_index );
    if ( tmp_dict_value_28 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assign_source_102 );

        exception_lineno = 145;

        goto frame_exception_exit_1;
    }
    tmp_res = PyDict_SetItem( tmp_assign_source_102, tmp_dict_key_28, tmp_dict_value_28 );
    Py_DECREF( tmp_dict_value_28 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_29 = const_bytes_digest_1b1c6fec89889dcdc21ab47c7c40282d;
    tmp_source_name_30 = GET_STRING_DICT_VALUE( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_SASIndex );

    if (unlikely( tmp_source_name_30 == NULL ))
    {
        tmp_source_name_30 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SASIndex );
    }

    if ( tmp_source_name_30 == NULL )
    {
        Py_DECREF( tmp_assign_source_102 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SASIndex" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 146;

        goto frame_exception_exit_1;
    }

    tmp_dict_value_29 = LOOKUP_ATTRIBUTE( tmp_source_name_30, const_str_plain_column_list_index );
    if ( tmp_dict_value_29 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assign_source_102 );

        exception_lineno = 146;

        goto frame_exception_exit_1;
    }
    tmp_res = PyDict_SetItem( tmp_assign_source_102, tmp_dict_key_29, tmp_dict_value_29 );
    Py_DECREF( tmp_dict_value_29 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_30 = const_bytes_digest_91e51b16becdd27a032386b2ee9ea6ed;
    tmp_source_name_31 = GET_STRING_DICT_VALUE( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_SASIndex );

    if (unlikely( tmp_source_name_31 == NULL ))
    {
        tmp_source_name_31 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SASIndex );
    }

    if ( tmp_source_name_31 == NULL )
    {
        Py_DECREF( tmp_assign_source_102 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SASIndex" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 147;

        goto frame_exception_exit_1;
    }

    tmp_dict_value_30 = LOOKUP_ATTRIBUTE( tmp_source_name_31, const_str_plain_column_list_index );
    if ( tmp_dict_value_30 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assign_source_102 );

        exception_lineno = 147;

        goto frame_exception_exit_1;
    }
    tmp_res = PyDict_SetItem( tmp_assign_source_102, tmp_dict_key_30, tmp_dict_value_30 );
    Py_DECREF( tmp_dict_value_30 );
    assert( !(tmp_res != 0) );
    UPDATE_STRING_DICT1( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_subheader_signature_to_index, tmp_assign_source_102 );

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_0c5b1fd0275e283c370e24b6506ffd15 );
#endif
    popFrameStack();

    assertFrameObject( frame_0c5b1fd0275e283c370e24b6506ffd15 );

    goto frame_no_exception_2;
    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_0c5b1fd0275e283c370e24b6506ffd15 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_0c5b1fd0275e283c370e24b6506ffd15, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_0c5b1fd0275e283c370e24b6506ffd15->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_0c5b1fd0275e283c370e24b6506ffd15, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;
    frame_no_exception_2:;
    tmp_assign_source_103 = const_tuple_2f0e3f48309e1b3dacb9487878e05906_tuple;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_sas_date_formats, tmp_assign_source_103 );
    tmp_assign_source_104 = const_tuple_61f0b2321e950eb59f6e00174f8c491a_tuple;
    UPDATE_STRING_DICT0( moduledict_pandas$io$sas$sas_constants, (Nuitka_StringObject *)const_str_plain_sas_datetime_formats, tmp_assign_source_104 );

    return MOD_RETURN_VALUE( module_pandas$io$sas$sas_constants );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
