/* Generated code for Python source for module 'pandas.util._print_versions'
 * created by Nuitka version 0.5.32.7
 *
 * This code is in part copyright 2018 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The _module_pandas$util$_print_versions is a Python object pointer of module type. */

/* Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_pandas$util$_print_versions;
PyDictObject *moduledict_pandas$util$_print_versions;

/* The module constants used, if any. */
extern PyObject *const_tuple_str_digest_c075052d723d6707083e869a0e3659bb_tuple;
static PyObject *const_str_digest_a4c8752df691cc03f4e978376b91e5d9;
extern PyObject *const_str_plain_isdir;
extern PyObject *const_str_plain_main;
static PyObject *const_str_digest_de19a4a290566dd929fc4bf0489e3aad;
static PyObject *const_str_plain_LANG;
extern PyObject *const_str_plain_html5lib;
static PyObject *const_dict_38a1dad6291f8c2b00664c08bdb18de6;
static PyObject *const_tuple_str_digest_6cfbf864adddd0a72909234ea1acf66e_tuple;
extern PyObject *const_tuple_false_tuple;
extern PyObject *const_str_plain_locale;
extern PyObject *const_str_plain_commit;
extern PyObject *const_tuple_str_space_tuple;
extern PyObject *const_str_plain___file__;
extern PyObject *const_str_plain_show_versions;
static PyObject *const_str_digest_6f9e83b2eec8456796877ff337fd9e8e;
extern PyObject *const_str_plain_args;
static PyObject *const_str_plain_LOCALE;
extern PyObject *const_str_plain___exit__;
static PyObject *const_str_plain_etree;
extern PyObject *const_str_plain_struct;
extern PyObject *const_tuple_str_chr_34_tuple;
extern PyObject *const_str_plain_json;
static PyObject *const_str_plain_deps_blob;
static PyObject *const_tuple_str_plain_LC_ALL_str_plain_None_tuple;
extern PyObject *const_str_plain_getlocale;
static PyObject *const_tuple_str_plain_pandas_tuple;
static PyObject *const_str_plain_add_option;
static PyObject *const_str_plain_OptionParser;
extern PyObject *const_str_plain_os;
extern PyObject *const_str_plain_None;
extern PyObject *const_str_plain_ver;
static PyObject *const_str_digest_e4caad594972457f9c6a9870b1cae64a;
extern PyObject *const_str_plain_strip;
extern PyObject *const_str_plain_dateutil;
extern PyObject *const_str_plain___enter__;
static PyObject *const_str_plain_stat;
static PyObject *const_str_plain_serr;
extern PyObject *const_str_plain_stdout;
extern PyObject *const_str_plain_join;
static PyObject *const_tuple_str_plain_mod_tuple;
static PyObject *const_str_plain_blob;
static PyObject *const_str_plain_pandas_datareader;
extern PyObject *const_str_plain_platform;
extern PyObject *const_tuple_none_none_none_tuple;
extern PyObject *const_str_plain_pandas_gbq;
extern PyObject *const_str_plain_PIPE;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_plain_Popen;
extern PyObject *const_str_dot;
extern PyObject *const_str_plain_extend;
extern PyObject *const_str_plain___debug__;
extern PyObject *const_str_plain_import_module;
static PyObject *const_str_digest_6cfbf864adddd0a72909234ea1acf66e;
extern PyObject *const_str_plain_environ;
extern PyObject *const_str_plain_options;
extern PyObject *const_str_plain_wb;
extern PyObject *const_str_plain_matplotlib;
extern PyObject *const_str_plain_str;
extern PyObject *const_str_plain_numpy;
extern PyObject *const_str_plain_blosc;
extern PyObject *const_str_plain_j;
extern PyObject *const_str_plain_LC_ALL;
static PyObject *const_str_digest_086976f9ec3c181ebceaa1c4f60ec335;
static PyObject *const_tuple_0852dff813ee080787a63e6ce09b134d_tuple;
extern PyObject *const_str_plain_release;
static PyObject *const_dict_4c3bfbd1be3e51fcab24b1e0e0360329;
extern PyObject *const_str_plain_python;
static PyObject *const_tuple_d25ef1c8245bb93fb19f58c544c26edd_tuple;
extern PyObject *const_str_plain_importlib;
extern PyObject *const_str_plain_path;
static PyObject *const_str_digest_9d97a020f78fe137abaa84e2f6e65538;
extern PyObject *const_str_chr_45;
static PyObject *const_str_plain_pip;
extern PyObject *const_str_plain_calcsize;
extern PyObject *const_str_plain_numexpr;
static PyObject *const_str_digest_cf9819ddcc5b499ccc0bb111341da091;
static PyObject *const_str_digest_406700c4508db69a5394f514d3c7f2b4;
static PyObject *const_str_plain_FILE;
static PyObject *const_str_plain_deps;
extern PyObject *const_int_pos_8;
extern PyObject *const_str_plain_xlsxwriter;
extern PyObject *const_str_plain_subprocess;
extern PyObject *const_str_plain_indent;
extern PyObject *const_str_plain_bs4;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_space;
extern PyObject *const_str_plain_sqlalchemy;
extern PyObject *const_str_plain_tables;
extern PyObject *const_str_plain_append;
extern PyObject *const_str_plain_stderr;
extern PyObject *const_tuple_str_plain_P_tuple;
extern PyObject *const_str_plain_VERSION;
extern PyObject *const_str_plain_nargs;
extern PyObject *const_str_plain_dump;
static PyObject *const_str_plain_pymysql;
static PyObject *const_str_digest_3c287b5bec3e65b64e8bf11becff175b;
extern PyObject *const_str_plain_feather;
static PyObject *const_str_digest_f1c6c9be039474fe95893251fd2b4000;
extern PyObject *const_str_plain_P;
static PyObject *const_tuple_str_plain_OptionParser_tuple;
extern PyObject *const_str_plain_pandas;
static PyObject *const_str_plain_setuptools;
extern PyObject *const_str_plain_parser;
extern PyObject *const_str_plain_split;
extern PyObject *const_str_plain_bottleneck;
static PyObject *const_str_digest_3f2384385fffd2e75a0fa312a90c74f5;
static PyObject *const_str_plain_returncode;
extern PyObject *const_str_plain_decode;
extern PyObject *const_str_plain_False;
extern PyObject *const_str_plain_map;
extern PyObject *const_tuple_str_empty_tuple;
extern PyObject *const_str_plain_k;
extern PyObject *const_str_digest_da95bebd2524b65728827a9e6d1504b8;
extern PyObject *const_str_digest_c075052d723d6707083e869a0e3659bb;
extern PyObject *const_str_plain_f;
static PyObject *const_str_plain_lang;
static PyObject *const_str_plain_parse_args;
extern PyObject *const_str_plain_lxml;
extern PyObject *const_str_plain_encoding;
static PyObject *const_str_digest_cadba95ec56cdfc596077286494fe5db;
extern PyObject *const_str_plain_scipy;
extern PyObject *const_str_plain___version__;
static PyObject *const_str_plain_patsy;
extern PyObject *const_str_plain_xarray;
extern PyObject *const_int_0;
static PyObject *const_str_digest_a1239f6d2e05f69fe2c2fbddab07d3c6;
extern PyObject *const_str_plain_lc;
extern PyObject *const_str_plain_xlrd;
static PyObject *const_str_digest_26078b1905f833a6f46b0c7ec844c347;
static PyObject *const_tuple_str_plain_LANG_str_plain_None_tuple;
static PyObject *const_tuple_str_digest_a1239f6d2e05f69fe2c2fbddab07d3c6_tuple;
extern PyObject *const_str_plain_byteorder;
static PyObject *const_str_plain_optparse;
extern PyObject *const_str_plain_uname;
extern PyObject *const_str_plain_communicate;
static PyObject *const_str_plain_metavar;
extern PyObject *const_str_angle_lambda;
static PyObject *const_tuple_f93c82889e8a13d5acf61e2a7b22020b_tuple;
static PyObject *const_str_plain_sys_info;
extern PyObject *const_str_plain_openpyxl;
extern PyObject *const_str_plain_IPython;
extern PyObject *const_str_plain_system;
extern PyObject *const_str_plain_fastparquet;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_plain_modules;
static PyObject *const_str_plain_machine;
extern PyObject *const_str_plain_pipe;
extern PyObject *const_str_plain_codecs;
static PyObject *const_str_plain_simplejson;
static PyObject *const_str_digest_8dd3f7e9fc0eba20fb6b94e4b0414cee;
static PyObject *const_dict_4b89d655f33250fb2670a6025092b3bf;
extern PyObject *const_str_plain_sys;
static PyObject *const_str_digest_a9f40c8606d369b0b14755e06fd5f845;
extern PyObject *const_str_plain_utf8;
static PyObject *const_tuple_48e62b1582a2052a15a7634dedd661a8_tuple;
static PyObject *const_tuple_str_digest_086976f9ec3c181ebceaa1c4f60ec335_tuple;
static PyObject *const_str_plain_dependencies;
extern PyObject *const_str_plain_xlwt;
extern PyObject *const_str_plain_pytz;
extern PyObject *const_int_pos_1;
extern PyObject *const_str_plain_OS;
static PyObject *const_str_plain_get_sys_info;
static PyObject *const_str_plain_sphinx;
extern PyObject *const_str_plain_pyarrow;
extern PyObject *const_str_plain_jinja2;
static PyObject *const_str_digest_78df4448430dabd91e9f5622640b7c0f;
extern PyObject *const_str_plain_open;
static PyObject *const_str_plain_ver_f;
extern PyObject *const_str_plain___VERSION__;
extern PyObject *const_str_plain_processor;
static PyObject *const_str_plain_so;
extern PyObject *const_str_chr_34;
extern PyObject *const_str_plain_print;
static PyObject *const_str_plain_as_json;
static PyObject *const_str_digest_1fb41749d718cbdfb4ff4a6b9485ee1a;
extern PyObject *const_str_plain_version_info;
static PyObject *const_str_digest_0a536a34bf38117b24345f72cb8edf64;
static PyObject *const_str_plain_Cython;
static PyObject *const_str_plain_psycopg2;
extern PyObject *const_str_plain_s3fs;
extern PyObject *const_str_plain_mod;
static PyObject *const_str_digest_90010f8d5811a26762e8f6283b3d16ba;
extern PyObject *const_str_plain_get;
extern PyObject *const_str_plain_pytest;
extern PyObject *const_str_plain_version;
extern PyObject *const_int_pos_2;
static PyObject *const_str_plain_nodename;
extern PyObject *const_str_plain_format;
static PyObject *const_str_plain_sysname;
extern PyObject *const_str_empty;
extern PyObject *const_str_plain_modname;
static PyObject *const_str_plain_help;
static PyObject *module_filename_obj;

static bool constants_created = false;

static void createModuleConstants( void )
{
    const_str_digest_a4c8752df691cc03f4e978376b91e5d9 = UNSTREAM_STRING( &constant_bin[ 3332996 ], 36, 0 );
    const_str_digest_de19a4a290566dd929fc4bf0489e3aad = UNSTREAM_STRING( &constant_bin[ 3333032 ], 9, 0 );
    const_str_plain_LANG = UNSTREAM_STRING( &constant_bin[ 3333041 ], 4, 1 );
    const_dict_38a1dad6291f8c2b00664c08bdb18de6 = _PyDict_NewPresized( 1 );
    PyDict_SetItem( const_dict_38a1dad6291f8c2b00664c08bdb18de6, const_str_plain_indent, const_int_pos_2 );
    assert( PyDict_Size( const_dict_38a1dad6291f8c2b00664c08bdb18de6 ) == 1 );
    const_tuple_str_digest_6cfbf864adddd0a72909234ea1acf66e_tuple = PyTuple_New( 1 );
    const_str_digest_6cfbf864adddd0a72909234ea1acf66e = UNSTREAM_STRING( &constant_bin[ 3288977 ], 4, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_6cfbf864adddd0a72909234ea1acf66e_tuple, 0, const_str_digest_6cfbf864adddd0a72909234ea1acf66e ); Py_INCREF( const_str_digest_6cfbf864adddd0a72909234ea1acf66e );
    const_str_digest_6f9e83b2eec8456796877ff337fd9e8e = UNSTREAM_STRING( &constant_bin[ 3333045 ], 6, 0 );
    const_str_plain_LOCALE = UNSTREAM_STRING( &constant_bin[ 3333051 ], 6, 1 );
    const_str_plain_etree = UNSTREAM_STRING( &constant_bin[ 3043522 ], 5, 1 );
    const_str_plain_deps_blob = UNSTREAM_STRING( &constant_bin[ 3333057 ], 9, 1 );
    const_tuple_str_plain_LC_ALL_str_plain_None_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_LC_ALL_str_plain_None_tuple, 0, const_str_plain_LC_ALL ); Py_INCREF( const_str_plain_LC_ALL );
    PyTuple_SET_ITEM( const_tuple_str_plain_LC_ALL_str_plain_None_tuple, 1, const_str_plain_None ); Py_INCREF( const_str_plain_None );
    const_tuple_str_plain_pandas_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_pandas_tuple, 0, const_str_plain_pandas ); Py_INCREF( const_str_plain_pandas );
    const_str_plain_add_option = UNSTREAM_STRING( &constant_bin[ 3333066 ], 10, 1 );
    const_str_plain_OptionParser = UNSTREAM_STRING( &constant_bin[ 3333076 ], 12, 1 );
    const_str_digest_e4caad594972457f9c6a9870b1cae64a = UNSTREAM_STRING( &constant_bin[ 3333088 ], 36, 0 );
    const_str_plain_stat = UNSTREAM_STRING( &constant_bin[ 428 ], 4, 1 );
    const_str_plain_serr = UNSTREAM_STRING( &constant_bin[ 2600044 ], 4, 1 );
    const_tuple_str_plain_mod_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_mod_tuple, 0, const_str_plain_mod ); Py_INCREF( const_str_plain_mod );
    const_str_plain_blob = UNSTREAM_STRING( &constant_bin[ 3202131 ], 4, 1 );
    const_str_plain_pandas_datareader = UNSTREAM_STRING( &constant_bin[ 3333124 ], 17, 1 );
    const_str_digest_086976f9ec3c181ebceaa1c4f60ec335 = UNSTREAM_STRING( &constant_bin[ 78316 ], 18, 0 );
    const_tuple_0852dff813ee080787a63e6ce09b134d_tuple = PyTuple_New( 2 );
    const_str_digest_f1c6c9be039474fe95893251fd2b4000 = UNSTREAM_STRING( &constant_bin[ 344646 ], 2, 0 );
    PyTuple_SET_ITEM( const_tuple_0852dff813ee080787a63e6ce09b134d_tuple, 0, const_str_digest_f1c6c9be039474fe95893251fd2b4000 ); Py_INCREF( const_str_digest_f1c6c9be039474fe95893251fd2b4000 );
    const_str_digest_26078b1905f833a6f46b0c7ec844c347 = UNSTREAM_STRING( &constant_bin[ 3333141 ], 6, 0 );
    PyTuple_SET_ITEM( const_tuple_0852dff813ee080787a63e6ce09b134d_tuple, 1, const_str_digest_26078b1905f833a6f46b0c7ec844c347 ); Py_INCREF( const_str_digest_26078b1905f833a6f46b0c7ec844c347 );
    const_dict_4c3bfbd1be3e51fcab24b1e0e0360329 = _PyDict_NewPresized( 3 );
    const_str_plain_metavar = UNSTREAM_STRING( &constant_bin[ 3333147 ], 7, 1 );
    const_str_plain_FILE = UNSTREAM_STRING( &constant_bin[ 66376 ], 4, 1 );
    PyDict_SetItem( const_dict_4c3bfbd1be3e51fcab24b1e0e0360329, const_str_plain_metavar, const_str_plain_FILE );
    PyDict_SetItem( const_dict_4c3bfbd1be3e51fcab24b1e0e0360329, const_str_plain_nargs, const_int_pos_1 );
    const_str_plain_help = UNSTREAM_STRING( &constant_bin[ 67010 ], 4, 1 );
    const_str_digest_406700c4508db69a5394f514d3c7f2b4 = UNSTREAM_STRING( &constant_bin[ 3333154 ], 62, 0 );
    PyDict_SetItem( const_dict_4c3bfbd1be3e51fcab24b1e0e0360329, const_str_plain_help, const_str_digest_406700c4508db69a5394f514d3c7f2b4 );
    assert( PyDict_Size( const_dict_4c3bfbd1be3e51fcab24b1e0e0360329 ) == 3 );
    const_tuple_d25ef1c8245bb93fb19f58c544c26edd_tuple = PyTuple_New( 11 );
    PyTuple_SET_ITEM( const_tuple_d25ef1c8245bb93fb19f58c544c26edd_tuple, 0, const_str_plain_blob ); Py_INCREF( const_str_plain_blob );
    PyTuple_SET_ITEM( const_tuple_d25ef1c8245bb93fb19f58c544c26edd_tuple, 1, const_str_plain_commit ); Py_INCREF( const_str_plain_commit );
    PyTuple_SET_ITEM( const_tuple_d25ef1c8245bb93fb19f58c544c26edd_tuple, 2, const_str_plain_pipe ); Py_INCREF( const_str_plain_pipe );
    const_str_plain_so = UNSTREAM_STRING( &constant_bin[ 6637 ], 2, 1 );
    PyTuple_SET_ITEM( const_tuple_d25ef1c8245bb93fb19f58c544c26edd_tuple, 3, const_str_plain_so ); Py_INCREF( const_str_plain_so );
    PyTuple_SET_ITEM( const_tuple_d25ef1c8245bb93fb19f58c544c26edd_tuple, 4, const_str_plain_serr ); Py_INCREF( const_str_plain_serr );
    const_str_plain_sysname = UNSTREAM_STRING( &constant_bin[ 3333216 ], 7, 1 );
    PyTuple_SET_ITEM( const_tuple_d25ef1c8245bb93fb19f58c544c26edd_tuple, 5, const_str_plain_sysname ); Py_INCREF( const_str_plain_sysname );
    const_str_plain_nodename = UNSTREAM_STRING( &constant_bin[ 3333223 ], 8, 1 );
    PyTuple_SET_ITEM( const_tuple_d25ef1c8245bb93fb19f58c544c26edd_tuple, 6, const_str_plain_nodename ); Py_INCREF( const_str_plain_nodename );
    PyTuple_SET_ITEM( const_tuple_d25ef1c8245bb93fb19f58c544c26edd_tuple, 7, const_str_plain_release ); Py_INCREF( const_str_plain_release );
    PyTuple_SET_ITEM( const_tuple_d25ef1c8245bb93fb19f58c544c26edd_tuple, 8, const_str_plain_version ); Py_INCREF( const_str_plain_version );
    const_str_plain_machine = UNSTREAM_STRING( &constant_bin[ 67044 ], 7, 1 );
    PyTuple_SET_ITEM( const_tuple_d25ef1c8245bb93fb19f58c544c26edd_tuple, 9, const_str_plain_machine ); Py_INCREF( const_str_plain_machine );
    PyTuple_SET_ITEM( const_tuple_d25ef1c8245bb93fb19f58c544c26edd_tuple, 10, const_str_plain_processor ); Py_INCREF( const_str_plain_processor );
    const_str_digest_9d97a020f78fe137abaa84e2f6e65538 = UNSTREAM_STRING( &constant_bin[ 3333231 ], 93, 0 );
    const_str_plain_pip = UNSTREAM_STRING( &constant_bin[ 1810744 ], 3, 1 );
    const_str_digest_cf9819ddcc5b499ccc0bb111341da091 = UNSTREAM_STRING( &constant_bin[ 3333324 ], 10, 0 );
    const_str_plain_deps = UNSTREAM_STRING( &constant_bin[ 3333057 ], 4, 1 );
    const_str_plain_pymysql = UNSTREAM_STRING( &constant_bin[ 3333334 ], 7, 1 );
    const_str_digest_3c287b5bec3e65b64e8bf11becff175b = UNSTREAM_STRING( &constant_bin[ 3333341 ], 11, 0 );
    const_tuple_str_plain_OptionParser_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_OptionParser_tuple, 0, const_str_plain_OptionParser ); Py_INCREF( const_str_plain_OptionParser );
    const_str_plain_setuptools = UNSTREAM_STRING( &constant_bin[ 3333352 ], 10, 1 );
    const_str_digest_3f2384385fffd2e75a0fa312a90c74f5 = UNSTREAM_STRING( &constant_bin[ 3333362 ], 9, 0 );
    const_str_plain_returncode = UNSTREAM_STRING( &constant_bin[ 3333371 ], 10, 1 );
    const_str_plain_lang = UNSTREAM_STRING( &constant_bin[ 9109 ], 4, 1 );
    const_str_plain_parse_args = UNSTREAM_STRING( &constant_bin[ 3333381 ], 10, 1 );
    const_str_digest_cadba95ec56cdfc596077286494fe5db = UNSTREAM_STRING( &constant_bin[ 3333391 ], 11, 0 );
    const_str_plain_patsy = UNSTREAM_STRING( &constant_bin[ 3333402 ], 5, 1 );
    const_str_digest_a1239f6d2e05f69fe2c2fbddab07d3c6 = UNSTREAM_STRING( &constant_bin[ 3333407 ], 19, 0 );
    const_tuple_str_plain_LANG_str_plain_None_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_LANG_str_plain_None_tuple, 0, const_str_plain_LANG ); Py_INCREF( const_str_plain_LANG );
    PyTuple_SET_ITEM( const_tuple_str_plain_LANG_str_plain_None_tuple, 1, const_str_plain_None ); Py_INCREF( const_str_plain_None );
    const_tuple_str_digest_a1239f6d2e05f69fe2c2fbddab07d3c6_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_a1239f6d2e05f69fe2c2fbddab07d3c6_tuple, 0, const_str_digest_a1239f6d2e05f69fe2c2fbddab07d3c6 ); Py_INCREF( const_str_digest_a1239f6d2e05f69fe2c2fbddab07d3c6 );
    const_str_plain_optparse = UNSTREAM_STRING( &constant_bin[ 3333426 ], 8, 1 );
    const_tuple_f93c82889e8a13d5acf61e2a7b22020b_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_f93c82889e8a13d5acf61e2a7b22020b_tuple, 0, const_str_plain_OptionParser ); Py_INCREF( const_str_plain_OptionParser );
    PyTuple_SET_ITEM( const_tuple_f93c82889e8a13d5acf61e2a7b22020b_tuple, 1, const_str_plain_parser ); Py_INCREF( const_str_plain_parser );
    PyTuple_SET_ITEM( const_tuple_f93c82889e8a13d5acf61e2a7b22020b_tuple, 2, const_str_plain_options ); Py_INCREF( const_str_plain_options );
    PyTuple_SET_ITEM( const_tuple_f93c82889e8a13d5acf61e2a7b22020b_tuple, 3, const_str_plain_args ); Py_INCREF( const_str_plain_args );
    const_str_plain_sys_info = UNSTREAM_STRING( &constant_bin[ 3333434 ], 8, 1 );
    const_str_plain_simplejson = UNSTREAM_STRING( &constant_bin[ 3333442 ], 10, 1 );
    const_str_digest_8dd3f7e9fc0eba20fb6b94e4b0414cee = UNSTREAM_STRING( &constant_bin[ 3333452 ], 11, 0 );
    const_dict_4b89d655f33250fb2670a6025092b3bf = _PyDict_NewPresized( 1 );
    PyDict_SetItem( const_dict_4b89d655f33250fb2670a6025092b3bf, const_str_plain_encoding, const_str_plain_utf8 );
    assert( PyDict_Size( const_dict_4b89d655f33250fb2670a6025092b3bf ) == 1 );
    const_str_digest_a9f40c8606d369b0b14755e06fd5f845 = UNSTREAM_STRING( &constant_bin[ 3333463 ], 26, 0 );
    const_tuple_48e62b1582a2052a15a7634dedd661a8_tuple = PyTuple_New( 13 );
    const_str_plain_as_json = UNSTREAM_STRING( &constant_bin[ 3070835 ], 7, 1 );
    PyTuple_SET_ITEM( const_tuple_48e62b1582a2052a15a7634dedd661a8_tuple, 0, const_str_plain_as_json ); Py_INCREF( const_str_plain_as_json );
    PyTuple_SET_ITEM( const_tuple_48e62b1582a2052a15a7634dedd661a8_tuple, 1, const_str_plain_sys_info ); Py_INCREF( const_str_plain_sys_info );
    PyTuple_SET_ITEM( const_tuple_48e62b1582a2052a15a7634dedd661a8_tuple, 2, const_str_plain_deps ); Py_INCREF( const_str_plain_deps );
    PyTuple_SET_ITEM( const_tuple_48e62b1582a2052a15a7634dedd661a8_tuple, 3, const_str_plain_deps_blob ); Py_INCREF( const_str_plain_deps_blob );
    PyTuple_SET_ITEM( const_tuple_48e62b1582a2052a15a7634dedd661a8_tuple, 4, const_str_plain_modname ); Py_INCREF( const_str_plain_modname );
    const_str_plain_ver_f = UNSTREAM_STRING( &constant_bin[ 3333489 ], 5, 1 );
    PyTuple_SET_ITEM( const_tuple_48e62b1582a2052a15a7634dedd661a8_tuple, 5, const_str_plain_ver_f ); Py_INCREF( const_str_plain_ver_f );
    PyTuple_SET_ITEM( const_tuple_48e62b1582a2052a15a7634dedd661a8_tuple, 6, const_str_plain_mod ); Py_INCREF( const_str_plain_mod );
    PyTuple_SET_ITEM( const_tuple_48e62b1582a2052a15a7634dedd661a8_tuple, 7, const_str_plain_ver ); Py_INCREF( const_str_plain_ver );
    PyTuple_SET_ITEM( const_tuple_48e62b1582a2052a15a7634dedd661a8_tuple, 8, const_str_plain_json ); Py_INCREF( const_str_plain_json );
    PyTuple_SET_ITEM( const_tuple_48e62b1582a2052a15a7634dedd661a8_tuple, 9, const_str_plain_j ); Py_INCREF( const_str_plain_j );
    PyTuple_SET_ITEM( const_tuple_48e62b1582a2052a15a7634dedd661a8_tuple, 10, const_str_plain_f ); Py_INCREF( const_str_plain_f );
    PyTuple_SET_ITEM( const_tuple_48e62b1582a2052a15a7634dedd661a8_tuple, 11, const_str_plain_k ); Py_INCREF( const_str_plain_k );
    PyTuple_SET_ITEM( const_tuple_48e62b1582a2052a15a7634dedd661a8_tuple, 12, const_str_plain_stat ); Py_INCREF( const_str_plain_stat );
    const_tuple_str_digest_086976f9ec3c181ebceaa1c4f60ec335_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_086976f9ec3c181ebceaa1c4f60ec335_tuple, 0, const_str_digest_086976f9ec3c181ebceaa1c4f60ec335 ); Py_INCREF( const_str_digest_086976f9ec3c181ebceaa1c4f60ec335 );
    const_str_plain_dependencies = UNSTREAM_STRING( &constant_bin[ 1696339 ], 12, 1 );
    const_str_plain_get_sys_info = UNSTREAM_STRING( &constant_bin[ 3333494 ], 12, 1 );
    const_str_plain_sphinx = UNSTREAM_STRING( &constant_bin[ 3333506 ], 6, 1 );
    const_str_digest_78df4448430dabd91e9f5622640b7c0f = UNSTREAM_STRING( &constant_bin[ 3333512 ], 4, 0 );
    const_str_digest_1fb41749d718cbdfb4ff4a6b9485ee1a = UNSTREAM_STRING( &constant_bin[ 3333516 ], 11, 0 );
    const_str_digest_0a536a34bf38117b24345f72cb8edf64 = UNSTREAM_STRING( &constant_bin[ 3333527 ], 31, 0 );
    const_str_plain_Cython = UNSTREAM_STRING( &constant_bin[ 1718826 ], 6, 1 );
    const_str_plain_psycopg2 = UNSTREAM_STRING( &constant_bin[ 3174633 ], 8, 1 );
    const_str_digest_90010f8d5811a26762e8f6283b3d16ba = UNSTREAM_STRING( &constant_bin[ 3333558 ], 9, 0 );

    constants_created = true;
}

#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_pandas$util$_print_versions( void )
{
    // The module may not have been used at all.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_926b2cd8a50bada964ab9ff7ea36c401;
static PyCodeObject *codeobj_8b947b74bca15c4b71a777120c297bfa;
static PyCodeObject *codeobj_e41b1c514ea1750764423a6ad3fa842b;
static PyCodeObject *codeobj_f51c2fec9c6a331cc5981904628c07e1;
static PyCodeObject *codeobj_f73f0991cf3aa1993b37c583fcf51a1c;
static PyCodeObject *codeobj_5a4ac99e386874c3c884ed40f9dbb322;
static PyCodeObject *codeobj_2ec3fcea70543e510ce3df0dbd94aa4b;
static PyCodeObject *codeobj_38f7b17fed2ed0edc796d820d8b473df;
static PyCodeObject *codeobj_f40a2453591b4baa581365fc75869546;
static PyCodeObject *codeobj_072403d28f5741828c95c4e73365d503;
static PyCodeObject *codeobj_14a520a50b3a023d1743172655f97257;
static PyCodeObject *codeobj_1e8d1315dafed74c06af0f9717e1ea23;
static PyCodeObject *codeobj_e41547ccdc4c4e73dc064032cd7327eb;
static PyCodeObject *codeobj_8ef132c20da65b7d3460e4a332b6f9be;
static PyCodeObject *codeobj_73c6b457b34bdd71645a37575f39ac4c;
static PyCodeObject *codeobj_c7dc781ac04c588d3526dde79b9b6ce7;
static PyCodeObject *codeobj_7af43919b9767ca1039c00d325ec4c76;
static PyCodeObject *codeobj_eca96f70c2e5af9d01c7ee02fd375cfc;
static PyCodeObject *codeobj_6cce22e69d0dff310e212335427e63ec;
static PyCodeObject *codeobj_c88733dda5f5eb0d43a9100a815f3351;
static PyCodeObject *codeobj_06e645d0c17d11ba46064414e495bd0f;
static PyCodeObject *codeobj_1cf2bb340d87ac08e24b5cc60455989a;
static PyCodeObject *codeobj_1bb8923b3f31e892b461b41546196ed0;
static PyCodeObject *codeobj_799cfdc0da788d48e7b5639fe577b6d6;
static PyCodeObject *codeobj_51f02fd3ac7af88394eb95b568e206ab;
static PyCodeObject *codeobj_c27441d7de8a3ede56c605427a69e303;
static PyCodeObject *codeobj_65ec58e74815899139a3f53dae1074bc;
static PyCodeObject *codeobj_9f69f0cf233ce6e9d9b4f05d43820748;
static PyCodeObject *codeobj_02480085d27574d71b036399ed7da475;
static PyCodeObject *codeobj_7f234a464f34ccfabdf4b2ecfb5012f7;
static PyCodeObject *codeobj_dd0eb83b79989b2fbd28ecd5d44684bb;
static PyCodeObject *codeobj_e4b3e828b2b499d8ee29f6d608de7f47;
static PyCodeObject *codeobj_0c0e6bf08ffd07c0bd8549a6679c6f34;
static PyCodeObject *codeobj_3454a3ed34b89e6112d9a51765b9419c;
static PyCodeObject *codeobj_3676c49011b4315fe8eb7b1cea5a236d;
static PyCodeObject *codeobj_2c5d159c9ddebe4a467bddaa1f16e4c8;
static PyCodeObject *codeobj_49aeb67ae4770645ba85a4ae4c82ce83;
static PyCodeObject *codeobj_0a6c0c831f401b1e5eb3bcd85df09880;
static PyCodeObject *codeobj_e9a5b67587c704036c3f68c226ebe7dc;

static void createModuleCodeObjects(void)
{
    module_filename_obj = const_str_digest_9d97a020f78fe137abaa84e2f6e65538;
    codeobj_926b2cd8a50bada964ab9ff7ea36c401 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 64, const_tuple_str_plain_mod_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_8b947b74bca15c4b71a777120c297bfa = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 65, const_tuple_str_plain_mod_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_e41b1c514ea1750764423a6ad3fa842b = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 66, const_tuple_str_plain_mod_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_f51c2fec9c6a331cc5981904628c07e1 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 67, const_tuple_str_plain_mod_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_f73f0991cf3aa1993b37c583fcf51a1c = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 68, const_tuple_str_plain_mod_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_5a4ac99e386874c3c884ed40f9dbb322 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 69, const_tuple_str_plain_mod_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_2ec3fcea70543e510ce3df0dbd94aa4b = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 70, const_tuple_str_plain_mod_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_38f7b17fed2ed0edc796d820d8b473df = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 71, const_tuple_str_plain_mod_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_f40a2453591b4baa581365fc75869546 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 72, const_tuple_str_plain_mod_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_072403d28f5741828c95c4e73365d503 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 73, const_tuple_str_plain_mod_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_14a520a50b3a023d1743172655f97257 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 74, const_tuple_str_plain_mod_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_1e8d1315dafed74c06af0f9717e1ea23 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 75, const_tuple_str_plain_mod_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_e41547ccdc4c4e73dc064032cd7327eb = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 76, const_tuple_str_plain_mod_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_8ef132c20da65b7d3460e4a332b6f9be = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 77, const_tuple_str_plain_mod_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_73c6b457b34bdd71645a37575f39ac4c = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 78, const_tuple_str_plain_mod_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_c7dc781ac04c588d3526dde79b9b6ce7 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 79, const_tuple_str_plain_mod_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_7af43919b9767ca1039c00d325ec4c76 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 80, const_tuple_str_plain_mod_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_eca96f70c2e5af9d01c7ee02fd375cfc = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 81, const_tuple_str_plain_mod_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_6cce22e69d0dff310e212335427e63ec = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 82, const_tuple_str_plain_mod_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_c88733dda5f5eb0d43a9100a815f3351 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 83, const_tuple_str_plain_mod_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_06e645d0c17d11ba46064414e495bd0f = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 84, const_tuple_str_plain_mod_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_1cf2bb340d87ac08e24b5cc60455989a = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 85, const_tuple_str_plain_mod_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_1bb8923b3f31e892b461b41546196ed0 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 86, const_tuple_str_plain_mod_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_799cfdc0da788d48e7b5639fe577b6d6 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 87, const_tuple_str_plain_mod_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_51f02fd3ac7af88394eb95b568e206ab = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 88, const_tuple_str_plain_mod_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_c27441d7de8a3ede56c605427a69e303 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 89, const_tuple_str_plain_mod_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_65ec58e74815899139a3f53dae1074bc = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 90, const_tuple_str_plain_mod_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_9f69f0cf233ce6e9d9b4f05d43820748 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 91, const_tuple_str_plain_mod_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_02480085d27574d71b036399ed7da475 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 92, const_tuple_str_plain_mod_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_7f234a464f34ccfabdf4b2ecfb5012f7 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 93, const_tuple_str_plain_mod_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_dd0eb83b79989b2fbd28ecd5d44684bb = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 94, const_tuple_str_plain_mod_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_e4b3e828b2b499d8ee29f6d608de7f47 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 95, const_tuple_str_plain_mod_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_0c0e6bf08ffd07c0bd8549a6679c6f34 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 96, const_tuple_str_plain_mod_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_3454a3ed34b89e6112d9a51765b9419c = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 97, const_tuple_str_plain_mod_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_3676c49011b4315fe8eb7b1cea5a236d = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 98, const_tuple_str_plain_mod_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_2c5d159c9ddebe4a467bddaa1f16e4c8 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_a4c8752df691cc03f4e978376b91e5d9, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_49aeb67ae4770645ba85a4ae4c82ce83 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_sys_info, 11, const_tuple_d25ef1c8245bb93fb19f58c544c26edd_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_0a6c0c831f401b1e5eb3bcd85df09880 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_main, 140, const_tuple_f93c82889e8a13d5acf61e2a7b22020b_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_e9a5b67587c704036c3f68c226ebe7dc = MAKE_CODEOBJ( module_filename_obj, const_str_plain_show_versions, 59, const_tuple_48e62b1582a2052a15a7634dedd661a8_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
}

// The module function declarations.
static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_1_get_sys_info(  );


static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions( PyObject *defaults );


static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_10_lambda(  );


static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_11_lambda(  );


static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_12_lambda(  );


static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_13_lambda(  );


static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_14_lambda(  );


static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_15_lambda(  );


static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_16_lambda(  );


static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_17_lambda(  );


static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_18_lambda(  );


static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_19_lambda(  );


static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_1_lambda(  );


static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_20_lambda(  );


static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_21_lambda(  );


static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_22_lambda(  );


static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_23_lambda(  );


static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_24_lambda(  );


static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_25_lambda(  );


static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_26_lambda(  );


static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_27_lambda(  );


static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_28_lambda(  );


static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_29_lambda(  );


static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_2_lambda(  );


static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_30_lambda(  );


static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_31_lambda(  );


static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_32_lambda(  );


static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_33_lambda(  );


static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_34_lambda(  );


static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_35_lambda(  );


static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_3_lambda(  );


static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_4_lambda(  );


static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_5_lambda(  );


static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_6_lambda(  );


static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_7_lambda(  );


static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_8_lambda(  );


static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_9_lambda(  );


static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_3_main(  );


// The module function definitions.
static PyObject *impl_pandas$util$_print_versions$$$function_1_get_sys_info( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *var_blob = NULL;
    PyObject *var_commit = NULL;
    PyObject *var_pipe = NULL;
    PyObject *var_so = NULL;
    PyObject *var_serr = NULL;
    PyObject *var_sysname = NULL;
    PyObject *var_nodename = NULL;
    PyObject *var_release = NULL;
    PyObject *var_version = NULL;
    PyObject *var_machine = NULL;
    PyObject *var_processor = NULL;
    PyObject *tmp_try_except_1__unhandled_indicator = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    PyObject *tmp_tuple_unpack_2__element_1 = NULL;
    PyObject *tmp_tuple_unpack_2__element_2 = NULL;
    PyObject *tmp_tuple_unpack_2__element_3 = NULL;
    PyObject *tmp_tuple_unpack_2__element_4 = NULL;
    PyObject *tmp_tuple_unpack_2__element_5 = NULL;
    PyObject *tmp_tuple_unpack_2__element_6 = NULL;
    PyObject *tmp_tuple_unpack_2__source_iter = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_9;
    PyObject *exception_keeper_type_10;
    PyObject *exception_keeper_value_10;
    PyTracebackObject *exception_keeper_tb_10;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_10;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_and_left_truth_1;
    PyObject *tmp_and_left_value_1;
    PyObject *tmp_and_right_value_1;
    PyObject *tmp_args_element_name_1;
    PyObject *tmp_args_element_name_2;
    PyObject *tmp_args_element_name_3;
    PyObject *tmp_args_element_name_4;
    PyObject *tmp_args_element_name_5;
    PyObject *tmp_args_element_name_6;
    PyObject *tmp_args_element_name_7;
    PyObject *tmp_args_element_name_8;
    PyObject *tmp_args_name_1;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_assign_source_5;
    PyObject *tmp_assign_source_6;
    PyObject *tmp_assign_source_7;
    PyObject *tmp_assign_source_8;
    PyObject *tmp_assign_source_9;
    PyObject *tmp_assign_source_10;
    PyObject *tmp_assign_source_11;
    PyObject *tmp_assign_source_12;
    PyObject *tmp_assign_source_13;
    PyObject *tmp_assign_source_14;
    PyObject *tmp_assign_source_15;
    PyObject *tmp_assign_source_16;
    PyObject *tmp_assign_source_17;
    PyObject *tmp_assign_source_18;
    PyObject *tmp_assign_source_19;
    PyObject *tmp_assign_source_20;
    PyObject *tmp_assign_source_21;
    PyObject *tmp_assign_source_22;
    PyObject *tmp_assign_source_23;
    PyObject *tmp_assign_source_24;
    PyObject *tmp_assign_source_25;
    PyObject *tmp_assign_source_26;
    PyObject *tmp_called_instance_1;
    PyObject *tmp_called_instance_2;
    PyObject *tmp_called_instance_3;
    PyObject *tmp_called_instance_4;
    PyObject *tmp_called_instance_5;
    PyObject *tmp_called_instance_6;
    PyObject *tmp_called_instance_7;
    PyObject *tmp_called_instance_8;
    PyObject *tmp_called_instance_9;
    PyObject *tmp_called_instance_10;
    PyObject *tmp_called_instance_11;
    PyObject *tmp_called_instance_12;
    PyObject *tmp_called_name_1;
    PyObject *tmp_called_name_2;
    PyObject *tmp_called_name_3;
    PyObject *tmp_called_name_4;
    PyObject *tmp_called_name_5;
    PyObject *tmp_called_name_6;
    PyObject *tmp_called_name_7;
    PyObject *tmp_called_name_8;
    PyObject *tmp_called_name_9;
    PyObject *tmp_called_name_10;
    PyObject *tmp_called_name_11;
    PyObject *tmp_called_name_12;
    PyObject *tmp_called_name_13;
    PyObject *tmp_called_name_14;
    int tmp_cmp_Eq_1;
    PyObject *tmp_compare_left_1;
    PyObject *tmp_compare_left_2;
    PyObject *tmp_compare_left_3;
    PyObject *tmp_compare_right_1;
    PyObject *tmp_compare_right_2;
    PyObject *tmp_compare_right_3;
    int tmp_cond_truth_1;
    PyObject *tmp_cond_value_1;
    PyObject *tmp_dict_key_1;
    PyObject *tmp_dict_key_2;
    PyObject *tmp_dict_key_3;
    PyObject *tmp_dict_key_4;
    PyObject *tmp_dict_key_5;
    PyObject *tmp_dict_key_6;
    PyObject *tmp_dict_key_7;
    PyObject *tmp_dict_key_8;
    PyObject *tmp_dict_key_9;
    PyObject *tmp_dict_value_1;
    PyObject *tmp_dict_value_2;
    PyObject *tmp_dict_value_3;
    PyObject *tmp_dict_value_4;
    PyObject *tmp_dict_value_5;
    PyObject *tmp_dict_value_6;
    PyObject *tmp_dict_value_7;
    PyObject *tmp_dict_value_8;
    PyObject *tmp_dict_value_9;
    int tmp_exc_match_exception_match_1;
    bool tmp_is_1;
    PyObject *tmp_iter_arg_1;
    PyObject *tmp_iter_arg_2;
    PyObject *tmp_iterator_attempt;
    PyObject *tmp_iterator_name_1;
    PyObject *tmp_iterator_name_2;
    PyObject *tmp_kw_name_1;
    PyObject *tmp_kw_name_2;
    PyObject *tmp_kw_name_3;
    PyObject *tmp_kw_name_4;
    PyObject *tmp_kw_name_5;
    PyObject *tmp_kw_name_6;
    PyObject *tmp_kw_name_7;
    PyObject *tmp_kw_name_8;
    PyObject *tmp_left_name_1;
    PyObject *tmp_list_element_1;
    int tmp_res;
    bool tmp_result;
    PyObject *tmp_return_value;
    PyObject *tmp_right_name_1;
    PyObject *tmp_source_name_1;
    PyObject *tmp_source_name_2;
    PyObject *tmp_source_name_3;
    PyObject *tmp_source_name_4;
    PyObject *tmp_source_name_5;
    PyObject *tmp_source_name_6;
    PyObject *tmp_source_name_7;
    PyObject *tmp_source_name_8;
    PyObject *tmp_source_name_9;
    PyObject *tmp_source_name_10;
    PyObject *tmp_source_name_11;
    PyObject *tmp_source_name_12;
    PyObject *tmp_source_name_13;
    PyObject *tmp_source_name_14;
    PyObject *tmp_source_name_15;
    PyObject *tmp_source_name_16;
    PyObject *tmp_source_name_17;
    PyObject *tmp_source_name_18;
    PyObject *tmp_source_name_19;
    PyObject *tmp_source_name_20;
    PyObject *tmp_source_name_21;
    PyObject *tmp_tuple_element_1;
    PyObject *tmp_tuple_element_2;
    PyObject *tmp_tuple_element_3;
    PyObject *tmp_tuple_element_4;
    PyObject *tmp_tuple_element_5;
    PyObject *tmp_tuple_element_6;
    PyObject *tmp_tuple_element_7;
    PyObject *tmp_tuple_element_8;
    PyObject *tmp_tuple_element_9;
    PyObject *tmp_tuple_element_10;
    PyObject *tmp_tuple_element_11;
    PyObject *tmp_tuple_element_12;
    PyObject *tmp_unpack_1;
    PyObject *tmp_unpack_2;
    PyObject *tmp_unpack_3;
    PyObject *tmp_unpack_4;
    PyObject *tmp_unpack_5;
    PyObject *tmp_unpack_6;
    PyObject *tmp_unpack_7;
    PyObject *tmp_unpack_8;
    NUITKA_MAY_BE_UNUSED PyObject *tmp_unused;
    static struct Nuitka_FrameObject *cache_frame_49aeb67ae4770645ba85a4ae4c82ce83 = NULL;

    struct Nuitka_FrameObject *frame_49aeb67ae4770645ba85a4ae4c82ce83;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    tmp_assign_source_1 = PyList_New( 0 );
    assert( var_blob == NULL );
    var_blob = tmp_assign_source_1;

    tmp_assign_source_2 = Py_None;
    assert( var_commit == NULL );
    Py_INCREF( tmp_assign_source_2 );
    var_commit = tmp_assign_source_2;

    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_49aeb67ae4770645ba85a4ae4c82ce83, codeobj_49aeb67ae4770645ba85a4ae4c82ce83, module_pandas$util$_print_versions, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_49aeb67ae4770645ba85a4ae4c82ce83 = cache_frame_49aeb67ae4770645ba85a4ae4c82ce83;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_49aeb67ae4770645ba85a4ae4c82ce83 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_49aeb67ae4770645ba85a4ae4c82ce83 ) == 2 ); // Frame stack

    // Framed code:
    tmp_source_name_1 = GET_STRING_DICT_VALUE( moduledict_pandas$util$_print_versions, (Nuitka_StringObject *)const_str_plain_os );

    if (unlikely( tmp_source_name_1 == NULL ))
    {
        tmp_source_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
    }

    if ( tmp_source_name_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 18;
        type_description_1 = "ooooooooooo";
        goto frame_exception_exit_1;
    }

    tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_path );
    if ( tmp_called_instance_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 18;
        type_description_1 = "ooooooooooo";
        goto frame_exception_exit_1;
    }
    frame_49aeb67ae4770645ba85a4ae4c82ce83->m_frame.f_lineno = 18;
    tmp_and_left_value_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_isdir, &PyTuple_GET_ITEM( const_tuple_str_digest_6cfbf864adddd0a72909234ea1acf66e_tuple, 0 ) );

    Py_DECREF( tmp_called_instance_1 );
    if ( tmp_and_left_value_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 18;
        type_description_1 = "ooooooooooo";
        goto frame_exception_exit_1;
    }
    tmp_and_left_truth_1 = CHECK_IF_TRUE( tmp_and_left_value_1 );
    if ( tmp_and_left_truth_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_and_left_value_1 );

        exception_lineno = 18;
        type_description_1 = "ooooooooooo";
        goto frame_exception_exit_1;
    }
    if ( tmp_and_left_truth_1 == 1 )
    {
        goto and_right_1;
    }
    else
    {
        goto and_left_1;
    }
    and_right_1:;
    Py_DECREF( tmp_and_left_value_1 );
    tmp_source_name_2 = GET_STRING_DICT_VALUE( moduledict_pandas$util$_print_versions, (Nuitka_StringObject *)const_str_plain_os );

    if (unlikely( tmp_source_name_2 == NULL ))
    {
        tmp_source_name_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
    }

    if ( tmp_source_name_2 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 18;
        type_description_1 = "ooooooooooo";
        goto frame_exception_exit_1;
    }

    tmp_called_instance_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_path );
    if ( tmp_called_instance_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 18;
        type_description_1 = "ooooooooooo";
        goto frame_exception_exit_1;
    }
    frame_49aeb67ae4770645ba85a4ae4c82ce83->m_frame.f_lineno = 18;
    tmp_and_right_value_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_isdir, &PyTuple_GET_ITEM( const_tuple_str_plain_pandas_tuple, 0 ) );

    Py_DECREF( tmp_called_instance_2 );
    if ( tmp_and_right_value_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 18;
        type_description_1 = "ooooooooooo";
        goto frame_exception_exit_1;
    }
    tmp_cond_value_1 = tmp_and_right_value_1;
    goto and_end_1;
    and_left_1:;
    tmp_cond_value_1 = tmp_and_left_value_1;
    and_end_1:;
    tmp_cond_truth_1 = CHECK_IF_TRUE( tmp_cond_value_1 );
    if ( tmp_cond_truth_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_cond_value_1 );

        exception_lineno = 18;
        type_description_1 = "ooooooooooo";
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_cond_value_1 );
    if ( tmp_cond_truth_1 == 1 )
    {
        goto branch_yes_1;
    }
    else
    {
        goto branch_no_1;
    }
    branch_yes_1:;
    tmp_assign_source_3 = Py_True;
    assert( tmp_try_except_1__unhandled_indicator == NULL );
    Py_INCREF( tmp_assign_source_3 );
    tmp_try_except_1__unhandled_indicator = tmp_assign_source_3;

    // Tried code:
    tmp_source_name_3 = GET_STRING_DICT_VALUE( moduledict_pandas$util$_print_versions, (Nuitka_StringObject *)const_str_plain_subprocess );

    if (unlikely( tmp_source_name_3 == NULL ))
    {
        tmp_source_name_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_subprocess );
    }

    if ( tmp_source_name_3 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "subprocess" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 20;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_2;
    }

    tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_Popen );
    if ( tmp_called_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 20;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_2;
    }
    tmp_called_instance_3 = const_str_digest_a9f40c8606d369b0b14755e06fd5f845;
    frame_49aeb67ae4770645ba85a4ae4c82ce83->m_frame.f_lineno = 20;
    tmp_tuple_element_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_split, &PyTuple_GET_ITEM( const_tuple_str_space_tuple, 0 ) );

    if ( tmp_tuple_element_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_1 );

        exception_lineno = 20;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_2;
    }
    tmp_args_name_1 = PyTuple_New( 1 );
    PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
    tmp_dict_key_1 = const_str_plain_stdout;
    tmp_source_name_4 = GET_STRING_DICT_VALUE( moduledict_pandas$util$_print_versions, (Nuitka_StringObject *)const_str_plain_subprocess );

    if (unlikely( tmp_source_name_4 == NULL ))
    {
        tmp_source_name_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_subprocess );
    }

    if ( tmp_source_name_4 == NULL )
    {
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_name_1 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "subprocess" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 21;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_2;
    }

    tmp_dict_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_PIPE );
    if ( tmp_dict_value_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_name_1 );

        exception_lineno = 21;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_2;
    }
    tmp_kw_name_1 = _PyDict_NewPresized( 2 );
    tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
    Py_DECREF( tmp_dict_value_1 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_2 = const_str_plain_stderr;
    tmp_source_name_5 = GET_STRING_DICT_VALUE( moduledict_pandas$util$_print_versions, (Nuitka_StringObject *)const_str_plain_subprocess );

    if (unlikely( tmp_source_name_5 == NULL ))
    {
        tmp_source_name_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_subprocess );
    }

    if ( tmp_source_name_5 == NULL )
    {
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "subprocess" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 22;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_2;
    }

    tmp_dict_value_2 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_PIPE );
    if ( tmp_dict_value_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );

        exception_lineno = 22;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_2;
    }
    tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
    Py_DECREF( tmp_dict_value_2 );
    assert( !(tmp_res != 0) );
    frame_49aeb67ae4770645ba85a4ae4c82ce83->m_frame.f_lineno = 20;
    tmp_assign_source_4 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
    Py_DECREF( tmp_called_name_1 );
    Py_DECREF( tmp_args_name_1 );
    Py_DECREF( tmp_kw_name_1 );
    if ( tmp_assign_source_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 20;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_2;
    }
    assert( var_pipe == NULL );
    var_pipe = tmp_assign_source_4;

    // Tried code:
    tmp_called_instance_4 = var_pipe;

    CHECK_OBJECT( tmp_called_instance_4 );
    frame_49aeb67ae4770645ba85a4ae4c82ce83->m_frame.f_lineno = 23;
    tmp_iter_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_4, const_str_plain_communicate );
    if ( tmp_iter_arg_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 23;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_3;
    }
    tmp_assign_source_5 = MAKE_ITERATOR( tmp_iter_arg_1 );
    Py_DECREF( tmp_iter_arg_1 );
    if ( tmp_assign_source_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 23;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_3;
    }
    assert( tmp_tuple_unpack_1__source_iter == NULL );
    tmp_tuple_unpack_1__source_iter = tmp_assign_source_5;

    // Tried code:
    tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;

    CHECK_OBJECT( tmp_unpack_1 );
    tmp_assign_source_6 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
    if ( tmp_assign_source_6 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        type_description_1 = "ooooooooooo";
        exception_lineno = 23;
        goto try_except_handler_4;
    }
    assert( tmp_tuple_unpack_1__element_1 == NULL );
    tmp_tuple_unpack_1__element_1 = tmp_assign_source_6;

    tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;

    CHECK_OBJECT( tmp_unpack_2 );
    tmp_assign_source_7 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
    if ( tmp_assign_source_7 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        type_description_1 = "ooooooooooo";
        exception_lineno = 23;
        goto try_except_handler_4;
    }
    assert( tmp_tuple_unpack_1__element_2 == NULL );
    tmp_tuple_unpack_1__element_2 = tmp_assign_source_7;

    tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;

    CHECK_OBJECT( tmp_iterator_name_1 );
    // Check if iterator has left-over elements.
    CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

    tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

    if (likely( tmp_iterator_attempt == NULL ))
    {
        PyObject *error = GET_ERROR_OCCURRED();

        if ( error != NULL )
        {
            if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
            {
                CLEAR_ERROR_OCCURRED();
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                type_description_1 = "ooooooooooo";
                exception_lineno = 23;
                goto try_except_handler_4;
            }
        }
    }
    else
    {
        Py_DECREF( tmp_iterator_attempt );

        // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
        PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
        PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

        type_description_1 = "ooooooooooo";
        exception_lineno = 23;
        goto try_except_handler_4;
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_3;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto try_except_handler_2;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    tmp_assign_source_8 = tmp_tuple_unpack_1__element_1;

    CHECK_OBJECT( tmp_assign_source_8 );
    assert( var_so == NULL );
    Py_INCREF( tmp_assign_source_8 );
    var_so = tmp_assign_source_8;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    tmp_assign_source_9 = tmp_tuple_unpack_1__element_2;

    CHECK_OBJECT( tmp_assign_source_9 );
    assert( var_serr == NULL );
    Py_INCREF( tmp_assign_source_9 );
    var_serr = tmp_assign_source_9;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    goto try_end_3;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    tmp_assign_source_10 = Py_False;
    {
        PyObject *old = tmp_try_except_1__unhandled_indicator;
        assert( old != NULL );
        tmp_try_except_1__unhandled_indicator = tmp_assign_source_10;
        Py_INCREF( tmp_try_except_1__unhandled_indicator );
        Py_DECREF( old );
    }

    Py_DECREF( exception_keeper_type_3 );
    Py_XDECREF( exception_keeper_value_3 );
    Py_XDECREF( exception_keeper_tb_3 );
    goto try_end_3;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_1_get_sys_info );
    return NULL;
    // End of try:
    try_end_3:;
    // Tried code:
    tmp_compare_left_1 = tmp_try_except_1__unhandled_indicator;

    CHECK_OBJECT( tmp_compare_left_1 );
    tmp_compare_right_1 = Py_True;
    tmp_is_1 = ( tmp_compare_left_1 == tmp_compare_right_1 );
    if ( tmp_is_1 )
    {
        goto branch_yes_2;
    }
    else
    {
        goto branch_no_2;
    }
    branch_yes_2:;
    tmp_source_name_6 = var_pipe;

    if ( tmp_source_name_6 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "pipe" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 27;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_5;
    }

    tmp_compare_left_2 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_returncode );
    if ( tmp_compare_left_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 27;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_5;
    }
    tmp_compare_right_2 = const_int_0;
    tmp_cmp_Eq_1 = RICH_COMPARE_BOOL_EQ( tmp_compare_left_2, tmp_compare_right_2 );
    Py_DECREF( tmp_compare_left_2 );
    if ( tmp_cmp_Eq_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 27;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_5;
    }
    if ( tmp_cmp_Eq_1 == 1 )
    {
        goto branch_yes_3;
    }
    else
    {
        goto branch_no_3;
    }
    branch_yes_3:;
    tmp_assign_source_11 = var_so;

    if ( tmp_assign_source_11 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "so" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 28;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_5;
    }

    {
        PyObject *old = var_commit;
        var_commit = tmp_assign_source_11;
        Py_INCREF( var_commit );
        Py_XDECREF( old );
    }

    // Tried code:
    tmp_called_instance_5 = var_so;

    if ( tmp_called_instance_5 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "so" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 30;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_6;
    }

    frame_49aeb67ae4770645ba85a4ae4c82ce83->m_frame.f_lineno = 30;
    tmp_assign_source_12 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_5, const_str_plain_decode, &PyTuple_GET_ITEM( const_tuple_str_digest_c075052d723d6707083e869a0e3659bb_tuple, 0 ) );

    if ( tmp_assign_source_12 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 30;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_6;
    }
    {
        PyObject *old = var_commit;
        var_commit = tmp_assign_source_12;
        Py_XDECREF( old );
    }

    goto try_end_4;
    // Exception handler code:
    try_except_handler_6:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_4 == NULL )
    {
        exception_keeper_tb_4 = MAKE_TRACEBACK( frame_49aeb67ae4770645ba85a4ae4c82ce83, exception_keeper_lineno_4 );
    }
    else if ( exception_keeper_lineno_4 != 0 )
    {
        exception_keeper_tb_4 = ADD_TRACEBACK( exception_keeper_tb_4, frame_49aeb67ae4770645ba85a4ae4c82ce83, exception_keeper_lineno_4 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_4, &exception_keeper_value_4, &exception_keeper_tb_4 );
    PyException_SetTraceback( exception_keeper_value_4, (PyObject *)exception_keeper_tb_4 );
    PUBLISH_EXCEPTION( &exception_keeper_type_4, &exception_keeper_value_4, &exception_keeper_tb_4 );
    // Tried code:
    tmp_compare_left_3 = EXC_TYPE(PyThreadState_GET());
    tmp_compare_right_3 = PyExc_ValueError;
    tmp_exc_match_exception_match_1 = EXCEPTION_MATCH_BOOL( tmp_compare_left_3, tmp_compare_right_3 );
    if ( tmp_exc_match_exception_match_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 31;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_7;
    }
    if ( tmp_exc_match_exception_match_1 == 1 )
    {
        goto branch_no_4;
    }
    else
    {
        goto branch_yes_4;
    }
    branch_yes_4:;
    tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (unlikely( tmp_result == false ))
    {
        exception_lineno = 29;
    }

    if (exception_tb && exception_tb->tb_frame == &frame_49aeb67ae4770645ba85a4ae4c82ce83->m_frame) frame_49aeb67ae4770645ba85a4ae4c82ce83->m_frame.f_lineno = exception_tb->tb_lineno;
    type_description_1 = "ooooooooooo";
    goto try_except_handler_7;
    branch_no_4:;
    goto try_end_5;
    // Exception handler code:
    try_except_handler_7:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto try_except_handler_5;
    // End of try:
    try_end_5:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto try_end_4;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_1_get_sys_info );
    return NULL;
    // End of try:
    try_end_4:;
    tmp_called_instance_7 = var_commit;

    if ( tmp_called_instance_7 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "commit" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 33;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_5;
    }

    frame_49aeb67ae4770645ba85a4ae4c82ce83->m_frame.f_lineno = 33;
    tmp_called_instance_6 = CALL_METHOD_NO_ARGS( tmp_called_instance_7, const_str_plain_strip );
    if ( tmp_called_instance_6 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 33;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_5;
    }
    frame_49aeb67ae4770645ba85a4ae4c82ce83->m_frame.f_lineno = 33;
    tmp_assign_source_13 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_6, const_str_plain_strip, &PyTuple_GET_ITEM( const_tuple_str_chr_34_tuple, 0 ) );

    Py_DECREF( tmp_called_instance_6 );
    if ( tmp_assign_source_13 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 33;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_5;
    }
    {
        PyObject *old = var_commit;
        var_commit = tmp_assign_source_13;
        Py_XDECREF( old );
    }

    branch_no_3:;
    branch_no_2:;
    goto try_end_6;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_keeper_lineno_6 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_try_except_1__unhandled_indicator );
    Py_DECREF( tmp_try_except_1__unhandled_indicator );
    tmp_try_except_1__unhandled_indicator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_6;
    exception_value = exception_keeper_value_6;
    exception_tb = exception_keeper_tb_6;
    exception_lineno = exception_keeper_lineno_6;

    goto frame_exception_exit_1;
    // End of try:
    try_end_6:;
    CHECK_OBJECT( (PyObject *)tmp_try_except_1__unhandled_indicator );
    Py_DECREF( tmp_try_except_1__unhandled_indicator );
    tmp_try_except_1__unhandled_indicator = NULL;

    branch_no_1:;
    tmp_source_name_7 = var_blob;

    if ( tmp_source_name_7 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "blob" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 35;
        type_description_1 = "ooooooooooo";
        goto frame_exception_exit_1;
    }

    tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_append );
    if ( tmp_called_name_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 35;
        type_description_1 = "ooooooooooo";
        goto frame_exception_exit_1;
    }
    tmp_tuple_element_2 = const_str_plain_commit;
    tmp_args_element_name_1 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_2 );
    PyTuple_SET_ITEM( tmp_args_element_name_1, 0, tmp_tuple_element_2 );
    tmp_tuple_element_2 = var_commit;

    if ( tmp_tuple_element_2 == NULL )
    {
        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_1 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "commit" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 35;
        type_description_1 = "ooooooooooo";
        goto frame_exception_exit_1;
    }

    Py_INCREF( tmp_tuple_element_2 );
    PyTuple_SET_ITEM( tmp_args_element_name_1, 1, tmp_tuple_element_2 );
    frame_49aeb67ae4770645ba85a4ae4c82ce83->m_frame.f_lineno = 35;
    {
        PyObject *call_args[] = { tmp_args_element_name_1 };
        tmp_unused = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
    }

    Py_DECREF( tmp_called_name_2 );
    Py_DECREF( tmp_args_element_name_1 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 35;
        type_description_1 = "ooooooooooo";
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_unused );
    // Tried code:
    // Tried code:
    tmp_called_instance_8 = GET_STRING_DICT_VALUE( moduledict_pandas$util$_print_versions, (Nuitka_StringObject *)const_str_plain_platform );

    if (unlikely( tmp_called_instance_8 == NULL ))
    {
        tmp_called_instance_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_platform );
    }

    if ( tmp_called_instance_8 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "platform" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 39;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_9;
    }

    frame_49aeb67ae4770645ba85a4ae4c82ce83->m_frame.f_lineno = 39;
    tmp_iter_arg_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_8, const_str_plain_uname );
    if ( tmp_iter_arg_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 39;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_9;
    }
    tmp_assign_source_14 = MAKE_ITERATOR( tmp_iter_arg_2 );
    Py_DECREF( tmp_iter_arg_2 );
    if ( tmp_assign_source_14 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 38;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_9;
    }
    assert( tmp_tuple_unpack_2__source_iter == NULL );
    tmp_tuple_unpack_2__source_iter = tmp_assign_source_14;

    // Tried code:
    tmp_unpack_3 = tmp_tuple_unpack_2__source_iter;

    CHECK_OBJECT( tmp_unpack_3 );
    tmp_assign_source_15 = UNPACK_NEXT( tmp_unpack_3, 0, 6 );
    if ( tmp_assign_source_15 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        type_description_1 = "ooooooooooo";
        exception_lineno = 38;
        goto try_except_handler_10;
    }
    assert( tmp_tuple_unpack_2__element_1 == NULL );
    tmp_tuple_unpack_2__element_1 = tmp_assign_source_15;

    tmp_unpack_4 = tmp_tuple_unpack_2__source_iter;

    CHECK_OBJECT( tmp_unpack_4 );
    tmp_assign_source_16 = UNPACK_NEXT( tmp_unpack_4, 1, 6 );
    if ( tmp_assign_source_16 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        type_description_1 = "ooooooooooo";
        exception_lineno = 38;
        goto try_except_handler_10;
    }
    assert( tmp_tuple_unpack_2__element_2 == NULL );
    tmp_tuple_unpack_2__element_2 = tmp_assign_source_16;

    tmp_unpack_5 = tmp_tuple_unpack_2__source_iter;

    CHECK_OBJECT( tmp_unpack_5 );
    tmp_assign_source_17 = UNPACK_NEXT( tmp_unpack_5, 2, 6 );
    if ( tmp_assign_source_17 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        type_description_1 = "ooooooooooo";
        exception_lineno = 38;
        goto try_except_handler_10;
    }
    assert( tmp_tuple_unpack_2__element_3 == NULL );
    tmp_tuple_unpack_2__element_3 = tmp_assign_source_17;

    tmp_unpack_6 = tmp_tuple_unpack_2__source_iter;

    CHECK_OBJECT( tmp_unpack_6 );
    tmp_assign_source_18 = UNPACK_NEXT( tmp_unpack_6, 3, 6 );
    if ( tmp_assign_source_18 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        type_description_1 = "ooooooooooo";
        exception_lineno = 38;
        goto try_except_handler_10;
    }
    assert( tmp_tuple_unpack_2__element_4 == NULL );
    tmp_tuple_unpack_2__element_4 = tmp_assign_source_18;

    tmp_unpack_7 = tmp_tuple_unpack_2__source_iter;

    CHECK_OBJECT( tmp_unpack_7 );
    tmp_assign_source_19 = UNPACK_NEXT( tmp_unpack_7, 4, 6 );
    if ( tmp_assign_source_19 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        type_description_1 = "ooooooooooo";
        exception_lineno = 38;
        goto try_except_handler_10;
    }
    assert( tmp_tuple_unpack_2__element_5 == NULL );
    tmp_tuple_unpack_2__element_5 = tmp_assign_source_19;

    tmp_unpack_8 = tmp_tuple_unpack_2__source_iter;

    CHECK_OBJECT( tmp_unpack_8 );
    tmp_assign_source_20 = UNPACK_NEXT( tmp_unpack_8, 5, 6 );
    if ( tmp_assign_source_20 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        type_description_1 = "ooooooooooo";
        exception_lineno = 38;
        goto try_except_handler_10;
    }
    assert( tmp_tuple_unpack_2__element_6 == NULL );
    tmp_tuple_unpack_2__element_6 = tmp_assign_source_20;

    tmp_iterator_name_2 = tmp_tuple_unpack_2__source_iter;

    CHECK_OBJECT( tmp_iterator_name_2 );
    // Check if iterator has left-over elements.
    CHECK_OBJECT( tmp_iterator_name_2 ); assert( HAS_ITERNEXT( tmp_iterator_name_2 ) );

    tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_2 )->tp_iternext)( tmp_iterator_name_2 );

    if (likely( tmp_iterator_attempt == NULL ))
    {
        PyObject *error = GET_ERROR_OCCURRED();

        if ( error != NULL )
        {
            if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
            {
                CLEAR_ERROR_OCCURRED();
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                type_description_1 = "ooooooooooo";
                exception_lineno = 38;
                goto try_except_handler_10;
            }
        }
    }
    else
    {
        Py_DECREF( tmp_iterator_attempt );

        // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
        PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
        PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 6)" );
#endif
        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

        type_description_1 = "ooooooooooo";
        exception_lineno = 38;
        goto try_except_handler_10;
    }
    goto try_end_7;
    // Exception handler code:
    try_except_handler_10:;
    exception_keeper_type_7 = exception_type;
    exception_keeper_value_7 = exception_value;
    exception_keeper_tb_7 = exception_tb;
    exception_keeper_lineno_7 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_2__source_iter );
    Py_DECREF( tmp_tuple_unpack_2__source_iter );
    tmp_tuple_unpack_2__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_7;
    exception_value = exception_keeper_value_7;
    exception_tb = exception_keeper_tb_7;
    exception_lineno = exception_keeper_lineno_7;

    goto try_except_handler_9;
    // End of try:
    try_end_7:;
    goto try_end_8;
    // Exception handler code:
    try_except_handler_9:;
    exception_keeper_type_8 = exception_type;
    exception_keeper_value_8 = exception_value;
    exception_keeper_tb_8 = exception_tb;
    exception_keeper_lineno_8 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_2__element_1 );
    tmp_tuple_unpack_2__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_2__element_2 );
    tmp_tuple_unpack_2__element_2 = NULL;

    Py_XDECREF( tmp_tuple_unpack_2__element_3 );
    tmp_tuple_unpack_2__element_3 = NULL;

    Py_XDECREF( tmp_tuple_unpack_2__element_4 );
    tmp_tuple_unpack_2__element_4 = NULL;

    Py_XDECREF( tmp_tuple_unpack_2__element_5 );
    tmp_tuple_unpack_2__element_5 = NULL;

    Py_XDECREF( tmp_tuple_unpack_2__element_6 );
    tmp_tuple_unpack_2__element_6 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_8;
    exception_value = exception_keeper_value_8;
    exception_tb = exception_keeper_tb_8;
    exception_lineno = exception_keeper_lineno_8;

    goto try_except_handler_8;
    // End of try:
    try_end_8:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_2__source_iter );
    Py_DECREF( tmp_tuple_unpack_2__source_iter );
    tmp_tuple_unpack_2__source_iter = NULL;

    tmp_assign_source_21 = tmp_tuple_unpack_2__element_1;

    CHECK_OBJECT( tmp_assign_source_21 );
    assert( var_sysname == NULL );
    Py_INCREF( tmp_assign_source_21 );
    var_sysname = tmp_assign_source_21;

    Py_XDECREF( tmp_tuple_unpack_2__element_1 );
    tmp_tuple_unpack_2__element_1 = NULL;

    tmp_assign_source_22 = tmp_tuple_unpack_2__element_2;

    CHECK_OBJECT( tmp_assign_source_22 );
    assert( var_nodename == NULL );
    Py_INCREF( tmp_assign_source_22 );
    var_nodename = tmp_assign_source_22;

    Py_XDECREF( tmp_tuple_unpack_2__element_2 );
    tmp_tuple_unpack_2__element_2 = NULL;

    tmp_assign_source_23 = tmp_tuple_unpack_2__element_3;

    CHECK_OBJECT( tmp_assign_source_23 );
    assert( var_release == NULL );
    Py_INCREF( tmp_assign_source_23 );
    var_release = tmp_assign_source_23;

    Py_XDECREF( tmp_tuple_unpack_2__element_3 );
    tmp_tuple_unpack_2__element_3 = NULL;

    tmp_assign_source_24 = tmp_tuple_unpack_2__element_4;

    CHECK_OBJECT( tmp_assign_source_24 );
    assert( var_version == NULL );
    Py_INCREF( tmp_assign_source_24 );
    var_version = tmp_assign_source_24;

    Py_XDECREF( tmp_tuple_unpack_2__element_4 );
    tmp_tuple_unpack_2__element_4 = NULL;

    tmp_assign_source_25 = tmp_tuple_unpack_2__element_5;

    CHECK_OBJECT( tmp_assign_source_25 );
    assert( var_machine == NULL );
    Py_INCREF( tmp_assign_source_25 );
    var_machine = tmp_assign_source_25;

    Py_XDECREF( tmp_tuple_unpack_2__element_5 );
    tmp_tuple_unpack_2__element_5 = NULL;

    tmp_assign_source_26 = tmp_tuple_unpack_2__element_6;

    CHECK_OBJECT( tmp_assign_source_26 );
    assert( var_processor == NULL );
    Py_INCREF( tmp_assign_source_26 );
    var_processor = tmp_assign_source_26;

    Py_XDECREF( tmp_tuple_unpack_2__element_6 );
    tmp_tuple_unpack_2__element_6 = NULL;

    tmp_source_name_8 = var_blob;

    if ( tmp_source_name_8 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "blob" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 40;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_8;
    }

    tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_extend );
    if ( tmp_called_name_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 40;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_8;
    }
    tmp_tuple_element_3 = const_str_plain_python;
    tmp_list_element_1 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_3 );
    PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_3 );
    tmp_source_name_9 = const_str_dot;
    tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_join );
    assert( !(tmp_called_name_4 == NULL) );
    tmp_called_name_5 = (PyObject *)&PyMap_Type;
    tmp_args_element_name_4 = (PyObject *)&PyUnicode_Type;
    tmp_source_name_10 = GET_STRING_DICT_VALUE( moduledict_pandas$util$_print_versions, (Nuitka_StringObject *)const_str_plain_sys );

    if (unlikely( tmp_source_name_10 == NULL ))
    {
        tmp_source_name_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
    }

    if ( tmp_source_name_10 == NULL )
    {
        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_list_element_1 );
        Py_DECREF( tmp_called_name_4 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 41;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_8;
    }

    tmp_args_element_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_version_info );
    if ( tmp_args_element_name_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_list_element_1 );
        Py_DECREF( tmp_called_name_4 );

        exception_lineno = 41;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_8;
    }
    frame_49aeb67ae4770645ba85a4ae4c82ce83->m_frame.f_lineno = 41;
    {
        PyObject *call_args[] = { tmp_args_element_name_4, tmp_args_element_name_5 };
        tmp_args_element_name_3 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_5, call_args );
    }

    Py_DECREF( tmp_args_element_name_5 );
    if ( tmp_args_element_name_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_list_element_1 );
        Py_DECREF( tmp_called_name_4 );

        exception_lineno = 41;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_8;
    }
    frame_49aeb67ae4770645ba85a4ae4c82ce83->m_frame.f_lineno = 41;
    {
        PyObject *call_args[] = { tmp_args_element_name_3 };
        tmp_tuple_element_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
    }

    Py_DECREF( tmp_called_name_4 );
    Py_DECREF( tmp_args_element_name_3 );
    if ( tmp_tuple_element_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_list_element_1 );

        exception_lineno = 41;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_8;
    }
    PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_3 );
    tmp_args_element_name_2 = PyList_New( 10 );
    PyList_SET_ITEM( tmp_args_element_name_2, 0, tmp_list_element_1 );
    tmp_tuple_element_4 = const_str_digest_cadba95ec56cdfc596077286494fe5db;
    tmp_list_element_1 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_4 );
    PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_4 );
    tmp_called_instance_9 = GET_STRING_DICT_VALUE( moduledict_pandas$util$_print_versions, (Nuitka_StringObject *)const_str_plain_struct );

    if (unlikely( tmp_called_instance_9 == NULL ))
    {
        tmp_called_instance_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_struct );
    }

    if ( tmp_called_instance_9 == NULL )
    {
        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_2 );
        Py_DECREF( tmp_list_element_1 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "struct" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 42;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_8;
    }

    frame_49aeb67ae4770645ba85a4ae4c82ce83->m_frame.f_lineno = 42;
    tmp_left_name_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_9, const_str_plain_calcsize, &PyTuple_GET_ITEM( const_tuple_str_plain_P_tuple, 0 ) );

    if ( tmp_left_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_2 );
        Py_DECREF( tmp_list_element_1 );

        exception_lineno = 42;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_8;
    }
    tmp_right_name_1 = const_int_pos_8;
    tmp_tuple_element_4 = BINARY_OPERATION_MUL( tmp_left_name_1, tmp_right_name_1 );
    Py_DECREF( tmp_left_name_1 );
    if ( tmp_tuple_element_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_2 );
        Py_DECREF( tmp_list_element_1 );

        exception_lineno = 42;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_8;
    }
    PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_4 );
    PyList_SET_ITEM( tmp_args_element_name_2, 1, tmp_list_element_1 );
    tmp_tuple_element_5 = const_str_plain_OS;
    tmp_list_element_1 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_5 );
    PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_5 );
    tmp_source_name_11 = const_str_digest_90010f8d5811a26762e8f6283b3d16ba;
    tmp_called_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_format );
    assert( !(tmp_called_name_6 == NULL) );
    tmp_dict_key_3 = const_str_plain_sysname;
    tmp_dict_value_3 = var_sysname;

    if ( tmp_dict_value_3 == NULL )
    {
        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_2 );
        Py_DECREF( tmp_list_element_1 );
        Py_DECREF( tmp_called_name_6 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "sysname" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 43;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_8;
    }

    tmp_kw_name_2 = _PyDict_NewPresized( 1 );
    tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_3, tmp_dict_value_3 );
    assert( !(tmp_res != 0) );
    frame_49aeb67ae4770645ba85a4ae4c82ce83->m_frame.f_lineno = 43;
    tmp_tuple_element_5 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_6, tmp_kw_name_2 );
    Py_DECREF( tmp_called_name_6 );
    Py_DECREF( tmp_kw_name_2 );
    if ( tmp_tuple_element_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_2 );
        Py_DECREF( tmp_list_element_1 );

        exception_lineno = 43;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_8;
    }
    PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_5 );
    PyList_SET_ITEM( tmp_args_element_name_2, 2, tmp_list_element_1 );
    tmp_tuple_element_6 = const_str_digest_cf9819ddcc5b499ccc0bb111341da091;
    tmp_list_element_1 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_6 );
    PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_6 );
    tmp_source_name_12 = const_str_digest_3f2384385fffd2e75a0fa312a90c74f5;
    tmp_called_name_7 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_format );
    assert( !(tmp_called_name_7 == NULL) );
    tmp_dict_key_4 = const_str_plain_release;
    tmp_dict_value_4 = var_release;

    if ( tmp_dict_value_4 == NULL )
    {
        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_2 );
        Py_DECREF( tmp_list_element_1 );
        Py_DECREF( tmp_called_name_7 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "release" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 44;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_8;
    }

    tmp_kw_name_3 = _PyDict_NewPresized( 1 );
    tmp_res = PyDict_SetItem( tmp_kw_name_3, tmp_dict_key_4, tmp_dict_value_4 );
    assert( !(tmp_res != 0) );
    frame_49aeb67ae4770645ba85a4ae4c82ce83->m_frame.f_lineno = 44;
    tmp_tuple_element_6 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_7, tmp_kw_name_3 );
    Py_DECREF( tmp_called_name_7 );
    Py_DECREF( tmp_kw_name_3 );
    if ( tmp_tuple_element_6 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_2 );
        Py_DECREF( tmp_list_element_1 );

        exception_lineno = 44;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_8;
    }
    PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_6 );
    PyList_SET_ITEM( tmp_args_element_name_2, 3, tmp_list_element_1 );
    tmp_tuple_element_7 = const_str_plain_machine;
    tmp_list_element_1 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_7 );
    PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_7 );
    tmp_source_name_13 = const_str_digest_de19a4a290566dd929fc4bf0489e3aad;
    tmp_called_name_8 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain_format );
    assert( !(tmp_called_name_8 == NULL) );
    tmp_dict_key_5 = const_str_plain_machine;
    tmp_dict_value_5 = var_machine;

    if ( tmp_dict_value_5 == NULL )
    {
        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_2 );
        Py_DECREF( tmp_list_element_1 );
        Py_DECREF( tmp_called_name_8 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "machine" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 46;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_8;
    }

    tmp_kw_name_4 = _PyDict_NewPresized( 1 );
    tmp_res = PyDict_SetItem( tmp_kw_name_4, tmp_dict_key_5, tmp_dict_value_5 );
    assert( !(tmp_res != 0) );
    frame_49aeb67ae4770645ba85a4ae4c82ce83->m_frame.f_lineno = 46;
    tmp_tuple_element_7 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_8, tmp_kw_name_4 );
    Py_DECREF( tmp_called_name_8 );
    Py_DECREF( tmp_kw_name_4 );
    if ( tmp_tuple_element_7 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_2 );
        Py_DECREF( tmp_list_element_1 );

        exception_lineno = 46;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_8;
    }
    PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_7 );
    PyList_SET_ITEM( tmp_args_element_name_2, 4, tmp_list_element_1 );
    tmp_tuple_element_8 = const_str_plain_processor;
    tmp_list_element_1 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_8 );
    PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_8 );
    tmp_source_name_14 = const_str_digest_1fb41749d718cbdfb4ff4a6b9485ee1a;
    tmp_called_name_9 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain_format );
    assert( !(tmp_called_name_9 == NULL) );
    tmp_dict_key_6 = const_str_plain_processor;
    tmp_dict_value_6 = var_processor;

    if ( tmp_dict_value_6 == NULL )
    {
        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_2 );
        Py_DECREF( tmp_list_element_1 );
        Py_DECREF( tmp_called_name_9 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "processor" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 47;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_8;
    }

    tmp_kw_name_5 = _PyDict_NewPresized( 1 );
    tmp_res = PyDict_SetItem( tmp_kw_name_5, tmp_dict_key_6, tmp_dict_value_6 );
    assert( !(tmp_res != 0) );
    frame_49aeb67ae4770645ba85a4ae4c82ce83->m_frame.f_lineno = 47;
    tmp_tuple_element_8 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_9, tmp_kw_name_5 );
    Py_DECREF( tmp_called_name_9 );
    Py_DECREF( tmp_kw_name_5 );
    if ( tmp_tuple_element_8 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_2 );
        Py_DECREF( tmp_list_element_1 );

        exception_lineno = 47;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_8;
    }
    PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_8 );
    PyList_SET_ITEM( tmp_args_element_name_2, 5, tmp_list_element_1 );
    tmp_tuple_element_9 = const_str_plain_byteorder;
    tmp_list_element_1 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_9 );
    PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_9 );
    tmp_source_name_15 = const_str_digest_8dd3f7e9fc0eba20fb6b94e4b0414cee;
    tmp_called_name_10 = LOOKUP_ATTRIBUTE( tmp_source_name_15, const_str_plain_format );
    assert( !(tmp_called_name_10 == NULL) );
    tmp_dict_key_7 = const_str_plain_byteorder;
    tmp_source_name_16 = GET_STRING_DICT_VALUE( moduledict_pandas$util$_print_versions, (Nuitka_StringObject *)const_str_plain_sys );

    if (unlikely( tmp_source_name_16 == NULL ))
    {
        tmp_source_name_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
    }

    if ( tmp_source_name_16 == NULL )
    {
        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_2 );
        Py_DECREF( tmp_list_element_1 );
        Py_DECREF( tmp_called_name_10 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 48;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_8;
    }

    tmp_dict_value_7 = LOOKUP_ATTRIBUTE( tmp_source_name_16, const_str_plain_byteorder );
    if ( tmp_dict_value_7 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_2 );
        Py_DECREF( tmp_list_element_1 );
        Py_DECREF( tmp_called_name_10 );

        exception_lineno = 48;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_8;
    }
    tmp_kw_name_6 = _PyDict_NewPresized( 1 );
    tmp_res = PyDict_SetItem( tmp_kw_name_6, tmp_dict_key_7, tmp_dict_value_7 );
    Py_DECREF( tmp_dict_value_7 );
    assert( !(tmp_res != 0) );
    frame_49aeb67ae4770645ba85a4ae4c82ce83->m_frame.f_lineno = 48;
    tmp_tuple_element_9 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_10, tmp_kw_name_6 );
    Py_DECREF( tmp_called_name_10 );
    Py_DECREF( tmp_kw_name_6 );
    if ( tmp_tuple_element_9 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_2 );
        Py_DECREF( tmp_list_element_1 );

        exception_lineno = 48;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_8;
    }
    PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_9 );
    PyList_SET_ITEM( tmp_args_element_name_2, 6, tmp_list_element_1 );
    tmp_tuple_element_10 = const_str_plain_LC_ALL;
    tmp_list_element_1 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_10 );
    PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_10 );
    tmp_source_name_17 = const_str_digest_78df4448430dabd91e9f5622640b7c0f;
    tmp_called_name_11 = LOOKUP_ATTRIBUTE( tmp_source_name_17, const_str_plain_format );
    assert( !(tmp_called_name_11 == NULL) );
    tmp_dict_key_8 = const_str_plain_lc;
    tmp_source_name_18 = GET_STRING_DICT_VALUE( moduledict_pandas$util$_print_versions, (Nuitka_StringObject *)const_str_plain_os );

    if (unlikely( tmp_source_name_18 == NULL ))
    {
        tmp_source_name_18 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
    }

    if ( tmp_source_name_18 == NULL )
    {
        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_2 );
        Py_DECREF( tmp_list_element_1 );
        Py_DECREF( tmp_called_name_11 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 49;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_8;
    }

    tmp_called_instance_10 = LOOKUP_ATTRIBUTE( tmp_source_name_18, const_str_plain_environ );
    if ( tmp_called_instance_10 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_2 );
        Py_DECREF( tmp_list_element_1 );
        Py_DECREF( tmp_called_name_11 );

        exception_lineno = 49;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_8;
    }
    frame_49aeb67ae4770645ba85a4ae4c82ce83->m_frame.f_lineno = 49;
    tmp_dict_value_8 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_10, const_str_plain_get, &PyTuple_GET_ITEM( const_tuple_str_plain_LC_ALL_str_plain_None_tuple, 0 ) );

    Py_DECREF( tmp_called_instance_10 );
    if ( tmp_dict_value_8 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_2 );
        Py_DECREF( tmp_list_element_1 );
        Py_DECREF( tmp_called_name_11 );

        exception_lineno = 49;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_8;
    }
    tmp_kw_name_7 = _PyDict_NewPresized( 1 );
    tmp_res = PyDict_SetItem( tmp_kw_name_7, tmp_dict_key_8, tmp_dict_value_8 );
    Py_DECREF( tmp_dict_value_8 );
    assert( !(tmp_res != 0) );
    frame_49aeb67ae4770645ba85a4ae4c82ce83->m_frame.f_lineno = 49;
    tmp_tuple_element_10 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_11, tmp_kw_name_7 );
    Py_DECREF( tmp_called_name_11 );
    Py_DECREF( tmp_kw_name_7 );
    if ( tmp_tuple_element_10 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_2 );
        Py_DECREF( tmp_list_element_1 );

        exception_lineno = 49;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_8;
    }
    PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_10 );
    PyList_SET_ITEM( tmp_args_element_name_2, 7, tmp_list_element_1 );
    tmp_tuple_element_11 = const_str_plain_LANG;
    tmp_list_element_1 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_11 );
    PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_11 );
    tmp_source_name_19 = const_str_digest_6f9e83b2eec8456796877ff337fd9e8e;
    tmp_called_name_12 = LOOKUP_ATTRIBUTE( tmp_source_name_19, const_str_plain_format );
    assert( !(tmp_called_name_12 == NULL) );
    tmp_dict_key_9 = const_str_plain_lang;
    tmp_source_name_20 = GET_STRING_DICT_VALUE( moduledict_pandas$util$_print_versions, (Nuitka_StringObject *)const_str_plain_os );

    if (unlikely( tmp_source_name_20 == NULL ))
    {
        tmp_source_name_20 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
    }

    if ( tmp_source_name_20 == NULL )
    {
        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_2 );
        Py_DECREF( tmp_list_element_1 );
        Py_DECREF( tmp_called_name_12 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 50;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_8;
    }

    tmp_called_instance_11 = LOOKUP_ATTRIBUTE( tmp_source_name_20, const_str_plain_environ );
    if ( tmp_called_instance_11 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_2 );
        Py_DECREF( tmp_list_element_1 );
        Py_DECREF( tmp_called_name_12 );

        exception_lineno = 50;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_8;
    }
    frame_49aeb67ae4770645ba85a4ae4c82ce83->m_frame.f_lineno = 50;
    tmp_dict_value_9 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_11, const_str_plain_get, &PyTuple_GET_ITEM( const_tuple_str_plain_LANG_str_plain_None_tuple, 0 ) );

    Py_DECREF( tmp_called_instance_11 );
    if ( tmp_dict_value_9 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_2 );
        Py_DECREF( tmp_list_element_1 );
        Py_DECREF( tmp_called_name_12 );

        exception_lineno = 50;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_8;
    }
    tmp_kw_name_8 = _PyDict_NewPresized( 1 );
    tmp_res = PyDict_SetItem( tmp_kw_name_8, tmp_dict_key_9, tmp_dict_value_9 );
    Py_DECREF( tmp_dict_value_9 );
    assert( !(tmp_res != 0) );
    frame_49aeb67ae4770645ba85a4ae4c82ce83->m_frame.f_lineno = 50;
    tmp_tuple_element_11 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_12, tmp_kw_name_8 );
    Py_DECREF( tmp_called_name_12 );
    Py_DECREF( tmp_kw_name_8 );
    if ( tmp_tuple_element_11 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_2 );
        Py_DECREF( tmp_list_element_1 );

        exception_lineno = 50;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_8;
    }
    PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_11 );
    PyList_SET_ITEM( tmp_args_element_name_2, 8, tmp_list_element_1 );
    tmp_tuple_element_12 = const_str_plain_LOCALE;
    tmp_list_element_1 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_12 );
    PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_12 );
    tmp_source_name_21 = const_str_dot;
    tmp_called_name_13 = LOOKUP_ATTRIBUTE( tmp_source_name_21, const_str_plain_join );
    assert( !(tmp_called_name_13 == NULL) );
    tmp_called_name_14 = (PyObject *)&PyMap_Type;
    tmp_args_element_name_7 = (PyObject *)&PyUnicode_Type;
    tmp_called_instance_12 = GET_STRING_DICT_VALUE( moduledict_pandas$util$_print_versions, (Nuitka_StringObject *)const_str_plain_locale );

    if (unlikely( tmp_called_instance_12 == NULL ))
    {
        tmp_called_instance_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_locale );
    }

    if ( tmp_called_instance_12 == NULL )
    {
        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_2 );
        Py_DECREF( tmp_list_element_1 );
        Py_DECREF( tmp_called_name_13 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "locale" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 51;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_8;
    }

    frame_49aeb67ae4770645ba85a4ae4c82ce83->m_frame.f_lineno = 51;
    tmp_args_element_name_8 = CALL_METHOD_NO_ARGS( tmp_called_instance_12, const_str_plain_getlocale );
    if ( tmp_args_element_name_8 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_2 );
        Py_DECREF( tmp_list_element_1 );
        Py_DECREF( tmp_called_name_13 );

        exception_lineno = 51;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_8;
    }
    frame_49aeb67ae4770645ba85a4ae4c82ce83->m_frame.f_lineno = 51;
    {
        PyObject *call_args[] = { tmp_args_element_name_7, tmp_args_element_name_8 };
        tmp_args_element_name_6 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_14, call_args );
    }

    Py_DECREF( tmp_args_element_name_8 );
    if ( tmp_args_element_name_6 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_2 );
        Py_DECREF( tmp_list_element_1 );
        Py_DECREF( tmp_called_name_13 );

        exception_lineno = 51;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_8;
    }
    frame_49aeb67ae4770645ba85a4ae4c82ce83->m_frame.f_lineno = 51;
    {
        PyObject *call_args[] = { tmp_args_element_name_6 };
        tmp_tuple_element_12 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_13, call_args );
    }

    Py_DECREF( tmp_called_name_13 );
    Py_DECREF( tmp_args_element_name_6 );
    if ( tmp_tuple_element_12 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_2 );
        Py_DECREF( tmp_list_element_1 );

        exception_lineno = 51;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_8;
    }
    PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_12 );
    PyList_SET_ITEM( tmp_args_element_name_2, 9, tmp_list_element_1 );
    frame_49aeb67ae4770645ba85a4ae4c82ce83->m_frame.f_lineno = 40;
    {
        PyObject *call_args[] = { tmp_args_element_name_2 };
        tmp_unused = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
    }

    Py_DECREF( tmp_called_name_3 );
    Py_DECREF( tmp_args_element_name_2 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 40;
        type_description_1 = "ooooooooooo";
        goto try_except_handler_8;
    }
    Py_DECREF( tmp_unused );
    goto try_end_9;
    // Exception handler code:
    try_except_handler_8:;
    exception_keeper_type_9 = exception_type;
    exception_keeper_value_9 = exception_value;
    exception_keeper_tb_9 = exception_tb;
    exception_keeper_lineno_9 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_DECREF( exception_keeper_type_9 );
    Py_XDECREF( exception_keeper_value_9 );
    Py_XDECREF( exception_keeper_tb_9 );
    goto try_end_9;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_1_get_sys_info );
    return NULL;
    // End of try:
    try_end_9:;
    tmp_return_value = var_blob;

    if ( tmp_return_value == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "blob" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 56;
        type_description_1 = "ooooooooooo";
        goto frame_exception_exit_1;
    }

    Py_INCREF( tmp_return_value );
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_49aeb67ae4770645ba85a4ae4c82ce83 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_49aeb67ae4770645ba85a4ae4c82ce83 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_49aeb67ae4770645ba85a4ae4c82ce83 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_49aeb67ae4770645ba85a4ae4c82ce83, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_49aeb67ae4770645ba85a4ae4c82ce83->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_49aeb67ae4770645ba85a4ae4c82ce83, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_49aeb67ae4770645ba85a4ae4c82ce83,
        type_description_1,
        var_blob,
        var_commit,
        var_pipe,
        var_so,
        var_serr,
        var_sysname,
        var_nodename,
        var_release,
        var_version,
        var_machine,
        var_processor
    );


    // Release cached frame.
    if ( frame_49aeb67ae4770645ba85a4ae4c82ce83 == cache_frame_49aeb67ae4770645ba85a4ae4c82ce83 )
    {
        Py_DECREF( frame_49aeb67ae4770645ba85a4ae4c82ce83 );
    }
    cache_frame_49aeb67ae4770645ba85a4ae4c82ce83 = NULL;

    assertFrameObject( frame_49aeb67ae4770645ba85a4ae4c82ce83 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_1_get_sys_info );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( var_blob );
    var_blob = NULL;

    Py_XDECREF( var_commit );
    var_commit = NULL;

    Py_XDECREF( var_pipe );
    var_pipe = NULL;

    Py_XDECREF( var_so );
    var_so = NULL;

    Py_XDECREF( var_serr );
    var_serr = NULL;

    Py_XDECREF( var_sysname );
    var_sysname = NULL;

    Py_XDECREF( var_nodename );
    var_nodename = NULL;

    Py_XDECREF( var_release );
    var_release = NULL;

    Py_XDECREF( var_version );
    var_version = NULL;

    Py_XDECREF( var_machine );
    var_machine = NULL;

    Py_XDECREF( var_processor );
    var_processor = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_10 = exception_type;
    exception_keeper_value_10 = exception_value;
    exception_keeper_tb_10 = exception_tb;
    exception_keeper_lineno_10 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( var_blob );
    var_blob = NULL;

    Py_XDECREF( var_commit );
    var_commit = NULL;

    Py_XDECREF( var_pipe );
    var_pipe = NULL;

    Py_XDECREF( var_so );
    var_so = NULL;

    Py_XDECREF( var_serr );
    var_serr = NULL;

    Py_XDECREF( var_sysname );
    var_sysname = NULL;

    Py_XDECREF( var_nodename );
    var_nodename = NULL;

    Py_XDECREF( var_release );
    var_release = NULL;

    Py_XDECREF( var_version );
    var_version = NULL;

    Py_XDECREF( var_machine );
    var_machine = NULL;

    Py_XDECREF( var_processor );
    var_processor = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_10;
    exception_value = exception_keeper_value_10;
    exception_tb = exception_keeper_tb_10;
    exception_lineno = exception_keeper_lineno_10;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_1_get_sys_info );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$util$_print_versions$$$function_2_show_versions( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_as_json = python_pars[ 0 ];
    PyObject *var_sys_info = NULL;
    PyObject *var_deps = NULL;
    PyObject *var_deps_blob = NULL;
    PyObject *var_modname = NULL;
    PyObject *var_ver_f = NULL;
    PyObject *var_mod = NULL;
    PyObject *var_ver = NULL;
    PyObject *var_json = NULL;
    PyObject *var_j = NULL;
    PyObject *var_f = NULL;
    PyObject *var_k = NULL;
    PyObject *var_stat = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_for_loop_2__for_iterator = NULL;
    PyObject *tmp_for_loop_2__iter_value = NULL;
    PyObject *tmp_for_loop_3__for_iterator = NULL;
    PyObject *tmp_for_loop_3__iter_value = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    PyObject *tmp_tuple_unpack_2__element_1 = NULL;
    PyObject *tmp_tuple_unpack_2__element_2 = NULL;
    PyObject *tmp_tuple_unpack_2__source_iter = NULL;
    PyObject *tmp_tuple_unpack_3__element_1 = NULL;
    PyObject *tmp_tuple_unpack_3__element_2 = NULL;
    PyObject *tmp_tuple_unpack_3__source_iter = NULL;
    PyObject *tmp_with_1__enter = NULL;
    PyObject *tmp_with_1__exit = NULL;
    PyObject *tmp_with_1__indicator = NULL;
    PyObject *tmp_with_1__source = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_9;
    PyObject *exception_keeper_type_10;
    PyObject *exception_keeper_value_10;
    PyTracebackObject *exception_keeper_tb_10;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_10;
    PyObject *exception_keeper_type_11;
    PyObject *exception_keeper_value_11;
    PyTracebackObject *exception_keeper_tb_11;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_11;
    PyObject *exception_keeper_type_12;
    PyObject *exception_keeper_value_12;
    PyTracebackObject *exception_keeper_tb_12;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_12;
    PyObject *exception_keeper_type_13;
    PyObject *exception_keeper_value_13;
    PyTracebackObject *exception_keeper_tb_13;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_13;
    PyObject *exception_keeper_type_14;
    PyObject *exception_keeper_value_14;
    PyTracebackObject *exception_keeper_tb_14;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_14;
    PyObject *exception_keeper_type_15;
    PyObject *exception_keeper_value_15;
    PyTracebackObject *exception_keeper_tb_15;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_15;
    PyObject *exception_keeper_type_16;
    PyObject *exception_keeper_value_16;
    PyTracebackObject *exception_keeper_tb_16;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_16;
    PyObject *exception_keeper_type_17;
    PyObject *exception_keeper_value_17;
    PyTracebackObject *exception_keeper_tb_17;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_17;
    PyObject *exception_keeper_type_18;
    PyObject *exception_keeper_value_18;
    PyTracebackObject *exception_keeper_tb_18;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_18;
    PyObject *exception_keeper_type_19;
    PyObject *exception_keeper_value_19;
    PyTracebackObject *exception_keeper_tb_19;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_19;
    PyObject *exception_keeper_type_20;
    PyObject *exception_keeper_value_20;
    PyTracebackObject *exception_keeper_tb_20;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_20;
    PyObject *exception_keeper_type_21;
    PyObject *exception_keeper_value_21;
    PyTracebackObject *exception_keeper_tb_21;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_21;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    PyObject *exception_preserved_type_2;
    PyObject *exception_preserved_value_2;
    PyTracebackObject *exception_preserved_tb_2;
    PyObject *exception_preserved_type_3;
    PyObject *exception_preserved_value_3;
    PyTracebackObject *exception_preserved_tb_3;
    PyObject *tmp_args_element_name_1;
    PyObject *tmp_args_element_name_2;
    PyObject *tmp_args_element_name_3;
    PyObject *tmp_args_element_name_4;
    PyObject *tmp_args_element_name_5;
    PyObject *tmp_args_element_name_6;
    PyObject *tmp_args_element_name_7;
    PyObject *tmp_args_element_name_8;
    PyObject *tmp_args_element_name_9;
    PyObject *tmp_args_element_name_10;
    PyObject *tmp_args_name_1;
    PyObject *tmp_args_name_2;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_assign_source_5;
    PyObject *tmp_assign_source_6;
    PyObject *tmp_assign_source_7;
    PyObject *tmp_assign_source_8;
    PyObject *tmp_assign_source_9;
    PyObject *tmp_assign_source_10;
    PyObject *tmp_assign_source_11;
    PyObject *tmp_assign_source_12;
    PyObject *tmp_assign_source_13;
    PyObject *tmp_assign_source_14;
    PyObject *tmp_assign_source_15;
    PyObject *tmp_assign_source_16;
    PyObject *tmp_assign_source_17;
    PyObject *tmp_assign_source_18;
    PyObject *tmp_assign_source_19;
    PyObject *tmp_assign_source_20;
    PyObject *tmp_assign_source_21;
    PyObject *tmp_assign_source_22;
    PyObject *tmp_assign_source_23;
    PyObject *tmp_assign_source_24;
    PyObject *tmp_assign_source_25;
    PyObject *tmp_assign_source_26;
    PyObject *tmp_assign_source_27;
    PyObject *tmp_assign_source_28;
    PyObject *tmp_assign_source_29;
    PyObject *tmp_assign_source_30;
    PyObject *tmp_assign_source_31;
    PyObject *tmp_assign_source_32;
    PyObject *tmp_assign_source_33;
    PyObject *tmp_assign_source_34;
    PyObject *tmp_assign_source_35;
    PyObject *tmp_assign_source_36;
    PyObject *tmp_called_name_1;
    PyObject *tmp_called_name_2;
    PyObject *tmp_called_name_3;
    PyObject *tmp_called_name_4;
    PyObject *tmp_called_name_5;
    PyObject *tmp_called_name_6;
    PyObject *tmp_called_name_7;
    PyObject *tmp_called_name_8;
    PyObject *tmp_called_name_9;
    PyObject *tmp_called_name_10;
    PyObject *tmp_called_name_11;
    PyObject *tmp_called_name_12;
    PyObject *tmp_called_name_13;
    PyObject *tmp_called_name_14;
    PyObject *tmp_called_name_15;
    PyObject *tmp_called_name_16;
    PyObject *tmp_called_name_17;
    PyObject *tmp_called_name_18;
    PyObject *tmp_called_name_19;
    int tmp_cmp_In_1;
    PyObject *tmp_compare_left_1;
    PyObject *tmp_compare_left_2;
    PyObject *tmp_compare_left_3;
    PyObject *tmp_compare_left_4;
    PyObject *tmp_compare_left_5;
    PyObject *tmp_compare_left_6;
    PyObject *tmp_compare_left_7;
    PyObject *tmp_compare_left_8;
    PyObject *tmp_compare_right_1;
    PyObject *tmp_compare_right_2;
    PyObject *tmp_compare_right_3;
    PyObject *tmp_compare_right_4;
    PyObject *tmp_compare_right_5;
    PyObject *tmp_compare_right_6;
    PyObject *tmp_compare_right_7;
    PyObject *tmp_compare_right_8;
    int tmp_cond_truth_1;
    int tmp_cond_truth_2;
    PyObject *tmp_cond_value_1;
    PyObject *tmp_cond_value_2;
    PyObject *tmp_dict_key_1;
    PyObject *tmp_dict_key_2;
    PyObject *tmp_dict_key_3;
    PyObject *tmp_dict_key_4;
    PyObject *tmp_dict_key_5;
    PyObject *tmp_dict_key_6;
    PyObject *tmp_dict_seq_1;
    PyObject *tmp_dict_seq_2;
    PyObject *tmp_dict_value_1;
    PyObject *tmp_dict_value_2;
    PyObject *tmp_dict_value_3;
    PyObject *tmp_dict_value_4;
    PyObject *tmp_dict_value_5;
    PyObject *tmp_dict_value_6;
    int tmp_exc_match_exception_match_1;
    int tmp_exc_match_exception_match_2;
    int tmp_exc_match_exception_match_3;
    int tmp_exc_match_exception_match_4;
    PyObject *tmp_fromlist_name_1;
    PyObject *tmp_fromlist_name_2;
    PyObject *tmp_globals_name_1;
    PyObject *tmp_globals_name_2;
    bool tmp_is_1;
    bool tmp_is_2;
    bool tmp_is_3;
    PyObject *tmp_iter_arg_1;
    PyObject *tmp_iter_arg_2;
    PyObject *tmp_iter_arg_3;
    PyObject *tmp_iter_arg_4;
    PyObject *tmp_iter_arg_5;
    PyObject *tmp_iter_arg_6;
    PyObject *tmp_iterator_attempt;
    PyObject *tmp_iterator_name_1;
    PyObject *tmp_iterator_name_2;
    PyObject *tmp_iterator_name_3;
    PyObject *tmp_kw_name_1;
    PyObject *tmp_kw_name_2;
    PyObject *tmp_kw_name_3;
    PyObject *tmp_kw_name_4;
    PyObject *tmp_level_name_1;
    PyObject *tmp_level_name_2;
    PyObject *tmp_list_element_1;
    PyObject *tmp_locals_name_1;
    PyObject *tmp_locals_name_2;
    PyObject *tmp_name_name_1;
    PyObject *tmp_name_name_2;
    int tmp_res;
    bool tmp_result;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    PyObject *tmp_source_name_2;
    PyObject *tmp_source_name_3;
    PyObject *tmp_source_name_4;
    PyObject *tmp_source_name_5;
    PyObject *tmp_source_name_6;
    PyObject *tmp_source_name_7;
    PyObject *tmp_source_name_8;
    PyObject *tmp_source_name_9;
    PyObject *tmp_source_name_10;
    PyObject *tmp_source_name_11;
    PyObject *tmp_subscribed_name_1;
    PyObject *tmp_subscript_name_1;
    PyObject *tmp_tuple_element_1;
    PyObject *tmp_tuple_element_2;
    PyObject *tmp_tuple_element_3;
    PyObject *tmp_tuple_element_4;
    PyObject *tmp_tuple_element_5;
    PyObject *tmp_tuple_element_6;
    PyObject *tmp_tuple_element_7;
    PyObject *tmp_tuple_element_8;
    PyObject *tmp_tuple_element_9;
    PyObject *tmp_tuple_element_10;
    PyObject *tmp_tuple_element_11;
    PyObject *tmp_tuple_element_12;
    PyObject *tmp_tuple_element_13;
    PyObject *tmp_tuple_element_14;
    PyObject *tmp_tuple_element_15;
    PyObject *tmp_tuple_element_16;
    PyObject *tmp_tuple_element_17;
    PyObject *tmp_tuple_element_18;
    PyObject *tmp_tuple_element_19;
    PyObject *tmp_tuple_element_20;
    PyObject *tmp_tuple_element_21;
    PyObject *tmp_tuple_element_22;
    PyObject *tmp_tuple_element_23;
    PyObject *tmp_tuple_element_24;
    PyObject *tmp_tuple_element_25;
    PyObject *tmp_tuple_element_26;
    PyObject *tmp_tuple_element_27;
    PyObject *tmp_tuple_element_28;
    PyObject *tmp_tuple_element_29;
    PyObject *tmp_tuple_element_30;
    PyObject *tmp_tuple_element_31;
    PyObject *tmp_tuple_element_32;
    PyObject *tmp_tuple_element_33;
    PyObject *tmp_tuple_element_34;
    PyObject *tmp_tuple_element_35;
    PyObject *tmp_tuple_element_36;
    PyObject *tmp_tuple_element_37;
    PyObject *tmp_tuple_element_38;
    PyObject *tmp_tuple_element_39;
    PyObject *tmp_unpack_1;
    PyObject *tmp_unpack_2;
    PyObject *tmp_unpack_3;
    PyObject *tmp_unpack_4;
    PyObject *tmp_unpack_5;
    PyObject *tmp_unpack_6;
    NUITKA_MAY_BE_UNUSED PyObject *tmp_unused;
    PyObject *tmp_value_name_1;
    PyObject *tmp_value_name_2;
    PyObject *tmp_value_name_3;
    static struct Nuitka_FrameObject *cache_frame_e9a5b67587c704036c3f68c226ebe7dc = NULL;

    struct Nuitka_FrameObject *frame_e9a5b67587c704036c3f68c226ebe7dc;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_e9a5b67587c704036c3f68c226ebe7dc, codeobj_e9a5b67587c704036c3f68c226ebe7dc, module_pandas$util$_print_versions, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_e9a5b67587c704036c3f68c226ebe7dc = cache_frame_e9a5b67587c704036c3f68c226ebe7dc;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_e9a5b67587c704036c3f68c226ebe7dc );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_e9a5b67587c704036c3f68c226ebe7dc ) == 2 ); // Frame stack

    // Framed code:
    tmp_called_name_1 = GET_STRING_DICT_VALUE( moduledict_pandas$util$_print_versions, (Nuitka_StringObject *)const_str_plain_get_sys_info );

    if (unlikely( tmp_called_name_1 == NULL ))
    {
        tmp_called_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_sys_info );
    }

    if ( tmp_called_name_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_sys_info" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 60;
        type_description_1 = "ooooooooooooo";
        goto frame_exception_exit_1;
    }

    frame_e9a5b67587c704036c3f68c226ebe7dc->m_frame.f_lineno = 60;
    tmp_assign_source_1 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
    if ( tmp_assign_source_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 60;
        type_description_1 = "ooooooooooooo";
        goto frame_exception_exit_1;
    }
    assert( var_sys_info == NULL );
    var_sys_info = tmp_assign_source_1;

    tmp_tuple_element_1 = const_str_plain_pandas;
    tmp_list_element_1 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_1 );
    tmp_tuple_element_1 = MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_1_lambda(  );
    PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_1 );
    tmp_assign_source_2 = PyList_New( 35 );
    PyList_SET_ITEM( tmp_assign_source_2, 0, tmp_list_element_1 );
    tmp_tuple_element_2 = const_str_plain_pytest;
    tmp_list_element_1 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_2 );
    PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_2 );
    tmp_tuple_element_2 = MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_2_lambda(  );
    PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_2 );
    PyList_SET_ITEM( tmp_assign_source_2, 1, tmp_list_element_1 );
    tmp_tuple_element_3 = const_str_plain_pip;
    tmp_list_element_1 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_3 );
    PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_3 );
    tmp_tuple_element_3 = MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_3_lambda(  );
    PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_3 );
    PyList_SET_ITEM( tmp_assign_source_2, 2, tmp_list_element_1 );
    tmp_tuple_element_4 = const_str_plain_setuptools;
    tmp_list_element_1 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_4 );
    PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_4 );
    tmp_tuple_element_4 = MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_4_lambda(  );
    PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_4 );
    PyList_SET_ITEM( tmp_assign_source_2, 3, tmp_list_element_1 );
    tmp_tuple_element_5 = const_str_plain_Cython;
    tmp_list_element_1 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_5 );
    PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_5 );
    tmp_tuple_element_5 = MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_5_lambda(  );
    PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_5 );
    PyList_SET_ITEM( tmp_assign_source_2, 4, tmp_list_element_1 );
    tmp_tuple_element_6 = const_str_plain_numpy;
    tmp_list_element_1 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_6 );
    PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_6 );
    tmp_tuple_element_6 = MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_6_lambda(  );
    PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_6 );
    PyList_SET_ITEM( tmp_assign_source_2, 5, tmp_list_element_1 );
    tmp_tuple_element_7 = const_str_plain_scipy;
    tmp_list_element_1 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_7 );
    PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_7 );
    tmp_tuple_element_7 = MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_7_lambda(  );
    PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_7 );
    PyList_SET_ITEM( tmp_assign_source_2, 6, tmp_list_element_1 );
    tmp_tuple_element_8 = const_str_plain_pyarrow;
    tmp_list_element_1 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_8 );
    PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_8 );
    tmp_tuple_element_8 = MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_8_lambda(  );
    PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_8 );
    PyList_SET_ITEM( tmp_assign_source_2, 7, tmp_list_element_1 );
    tmp_tuple_element_9 = const_str_plain_xarray;
    tmp_list_element_1 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_9 );
    PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_9 );
    tmp_tuple_element_9 = MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_9_lambda(  );
    PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_9 );
    PyList_SET_ITEM( tmp_assign_source_2, 8, tmp_list_element_1 );
    tmp_tuple_element_10 = const_str_plain_IPython;
    tmp_list_element_1 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_10 );
    PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_10 );
    tmp_tuple_element_10 = MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_10_lambda(  );
    PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_10 );
    PyList_SET_ITEM( tmp_assign_source_2, 9, tmp_list_element_1 );
    tmp_tuple_element_11 = const_str_plain_sphinx;
    tmp_list_element_1 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_11 );
    PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_11 );
    tmp_tuple_element_11 = MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_11_lambda(  );
    PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_11 );
    PyList_SET_ITEM( tmp_assign_source_2, 10, tmp_list_element_1 );
    tmp_tuple_element_12 = const_str_plain_patsy;
    tmp_list_element_1 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_12 );
    PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_12 );
    tmp_tuple_element_12 = MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_12_lambda(  );
    PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_12 );
    PyList_SET_ITEM( tmp_assign_source_2, 11, tmp_list_element_1 );
    tmp_tuple_element_13 = const_str_plain_dateutil;
    tmp_list_element_1 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_13 );
    PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_13 );
    tmp_tuple_element_13 = MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_13_lambda(  );
    PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_13 );
    PyList_SET_ITEM( tmp_assign_source_2, 12, tmp_list_element_1 );
    tmp_tuple_element_14 = const_str_plain_pytz;
    tmp_list_element_1 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_14 );
    PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_14 );
    tmp_tuple_element_14 = MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_14_lambda(  );
    PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_14 );
    PyList_SET_ITEM( tmp_assign_source_2, 13, tmp_list_element_1 );
    tmp_tuple_element_15 = const_str_plain_blosc;
    tmp_list_element_1 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_15 );
    PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_15 );
    tmp_tuple_element_15 = MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_15_lambda(  );
    PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_15 );
    PyList_SET_ITEM( tmp_assign_source_2, 14, tmp_list_element_1 );
    tmp_tuple_element_16 = const_str_plain_bottleneck;
    tmp_list_element_1 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_16 );
    PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_16 );
    tmp_tuple_element_16 = MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_16_lambda(  );
    PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_16 );
    PyList_SET_ITEM( tmp_assign_source_2, 15, tmp_list_element_1 );
    tmp_tuple_element_17 = const_str_plain_tables;
    tmp_list_element_1 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_17 );
    PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_17 );
    tmp_tuple_element_17 = MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_17_lambda(  );
    PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_17 );
    PyList_SET_ITEM( tmp_assign_source_2, 16, tmp_list_element_1 );
    tmp_tuple_element_18 = const_str_plain_numexpr;
    tmp_list_element_1 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_18 );
    PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_18 );
    tmp_tuple_element_18 = MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_18_lambda(  );
    PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_18 );
    PyList_SET_ITEM( tmp_assign_source_2, 17, tmp_list_element_1 );
    tmp_tuple_element_19 = const_str_plain_feather;
    tmp_list_element_1 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_19 );
    PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_19 );
    tmp_tuple_element_19 = MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_19_lambda(  );
    PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_19 );
    PyList_SET_ITEM( tmp_assign_source_2, 18, tmp_list_element_1 );
    tmp_tuple_element_20 = const_str_plain_matplotlib;
    tmp_list_element_1 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_20 );
    PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_20 );
    tmp_tuple_element_20 = MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_20_lambda(  );
    PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_20 );
    PyList_SET_ITEM( tmp_assign_source_2, 19, tmp_list_element_1 );
    tmp_tuple_element_21 = const_str_plain_openpyxl;
    tmp_list_element_1 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_21 );
    PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_21 );
    tmp_tuple_element_21 = MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_21_lambda(  );
    PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_21 );
    PyList_SET_ITEM( tmp_assign_source_2, 20, tmp_list_element_1 );
    tmp_tuple_element_22 = const_str_plain_xlrd;
    tmp_list_element_1 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_22 );
    PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_22 );
    tmp_tuple_element_22 = MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_22_lambda(  );
    PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_22 );
    PyList_SET_ITEM( tmp_assign_source_2, 21, tmp_list_element_1 );
    tmp_tuple_element_23 = const_str_plain_xlwt;
    tmp_list_element_1 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_23 );
    PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_23 );
    tmp_tuple_element_23 = MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_23_lambda(  );
    PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_23 );
    PyList_SET_ITEM( tmp_assign_source_2, 22, tmp_list_element_1 );
    tmp_tuple_element_24 = const_str_plain_xlsxwriter;
    tmp_list_element_1 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_24 );
    PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_24 );
    tmp_tuple_element_24 = MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_24_lambda(  );
    PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_24 );
    PyList_SET_ITEM( tmp_assign_source_2, 23, tmp_list_element_1 );
    tmp_tuple_element_25 = const_str_plain_lxml;
    tmp_list_element_1 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_25 );
    PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_25 );
    tmp_tuple_element_25 = MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_25_lambda(  );
    PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_25 );
    PyList_SET_ITEM( tmp_assign_source_2, 24, tmp_list_element_1 );
    tmp_tuple_element_26 = const_str_plain_bs4;
    tmp_list_element_1 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_26 );
    PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_26 );
    tmp_tuple_element_26 = MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_26_lambda(  );
    PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_26 );
    PyList_SET_ITEM( tmp_assign_source_2, 25, tmp_list_element_1 );
    tmp_tuple_element_27 = const_str_plain_html5lib;
    tmp_list_element_1 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_27 );
    PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_27 );
    tmp_tuple_element_27 = MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_27_lambda(  );
    PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_27 );
    PyList_SET_ITEM( tmp_assign_source_2, 26, tmp_list_element_1 );
    tmp_tuple_element_28 = const_str_plain_sqlalchemy;
    tmp_list_element_1 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_28 );
    PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_28 );
    tmp_tuple_element_28 = MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_28_lambda(  );
    PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_28 );
    PyList_SET_ITEM( tmp_assign_source_2, 27, tmp_list_element_1 );
    tmp_tuple_element_29 = const_str_plain_pymysql;
    tmp_list_element_1 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_29 );
    PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_29 );
    tmp_tuple_element_29 = MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_29_lambda(  );
    PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_29 );
    PyList_SET_ITEM( tmp_assign_source_2, 28, tmp_list_element_1 );
    tmp_tuple_element_30 = const_str_plain_psycopg2;
    tmp_list_element_1 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_30 );
    PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_30 );
    tmp_tuple_element_30 = MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_30_lambda(  );
    PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_30 );
    PyList_SET_ITEM( tmp_assign_source_2, 29, tmp_list_element_1 );
    tmp_tuple_element_31 = const_str_plain_jinja2;
    tmp_list_element_1 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_31 );
    PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_31 );
    tmp_tuple_element_31 = MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_31_lambda(  );
    PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_31 );
    PyList_SET_ITEM( tmp_assign_source_2, 30, tmp_list_element_1 );
    tmp_tuple_element_32 = const_str_plain_s3fs;
    tmp_list_element_1 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_32 );
    PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_32 );
    tmp_tuple_element_32 = MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_32_lambda(  );
    PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_32 );
    PyList_SET_ITEM( tmp_assign_source_2, 31, tmp_list_element_1 );
    tmp_tuple_element_33 = const_str_plain_fastparquet;
    tmp_list_element_1 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_33 );
    PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_33 );
    tmp_tuple_element_33 = MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_33_lambda(  );
    PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_33 );
    PyList_SET_ITEM( tmp_assign_source_2, 32, tmp_list_element_1 );
    tmp_tuple_element_34 = const_str_plain_pandas_gbq;
    tmp_list_element_1 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_34 );
    PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_34 );
    tmp_tuple_element_34 = MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_34_lambda(  );
    PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_34 );
    PyList_SET_ITEM( tmp_assign_source_2, 33, tmp_list_element_1 );
    tmp_tuple_element_35 = const_str_plain_pandas_datareader;
    tmp_list_element_1 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_35 );
    PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_35 );
    tmp_tuple_element_35 = MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_35_lambda(  );
    PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_35 );
    PyList_SET_ITEM( tmp_assign_source_2, 34, tmp_list_element_1 );
    assert( var_deps == NULL );
    var_deps = tmp_assign_source_2;

    tmp_assign_source_3 = PyList_New( 0 );
    assert( var_deps_blob == NULL );
    var_deps_blob = tmp_assign_source_3;

    tmp_iter_arg_1 = var_deps;

    CHECK_OBJECT( tmp_iter_arg_1 );
    tmp_assign_source_4 = MAKE_ITERATOR( tmp_iter_arg_1 );
    if ( tmp_assign_source_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 102;
        type_description_1 = "ooooooooooooo";
        goto frame_exception_exit_1;
    }
    assert( tmp_for_loop_1__for_iterator == NULL );
    tmp_for_loop_1__for_iterator = tmp_assign_source_4;

    // Tried code:
    loop_start_1:;
    // Tried code:
    tmp_value_name_1 = tmp_for_loop_1__for_iterator;

    CHECK_OBJECT( tmp_value_name_1 );
    tmp_assign_source_5 = ITERATOR_NEXT( tmp_value_name_1 );
    if ( tmp_assign_source_5 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        type_description_1 = "ooooooooooooo";
        exception_lineno = 102;
        goto try_except_handler_3;
    }
    {
        PyObject *old = tmp_for_loop_1__iter_value;
        tmp_for_loop_1__iter_value = tmp_assign_source_5;
        Py_XDECREF( old );
    }

    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    tmp_compare_left_1 = exception_keeper_type_1;
    tmp_compare_right_1 = PyExc_StopIteration;
    tmp_exc_match_exception_match_1 = EXCEPTION_MATCH_BOOL( tmp_compare_left_1, tmp_compare_right_1 );
    if ( tmp_exc_match_exception_match_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

        Py_DECREF( exception_keeper_type_1 );
        Py_XDECREF( exception_keeper_value_1 );
        Py_XDECREF( exception_keeper_tb_1 );

        exception_lineno = 102;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_2;
    }
    if ( tmp_exc_match_exception_match_1 == 1 )
    {
        goto branch_yes_1;
    }
    else
    {
        goto branch_no_1;
    }
    branch_yes_1:;
    Py_DECREF( exception_keeper_type_1 );
    Py_XDECREF( exception_keeper_value_1 );
    Py_XDECREF( exception_keeper_tb_1 );
    goto loop_end_1;
    goto branch_end_1;
    branch_no_1:;
    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_2;
    branch_end_1:;
    // End of try:
    try_end_1:;
    // Tried code:
    tmp_iter_arg_2 = tmp_for_loop_1__iter_value;

    CHECK_OBJECT( tmp_iter_arg_2 );
    tmp_assign_source_6 = MAKE_ITERATOR( tmp_iter_arg_2 );
    if ( tmp_assign_source_6 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 102;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_4;
    }
    {
        PyObject *old = tmp_tuple_unpack_1__source_iter;
        tmp_tuple_unpack_1__source_iter = tmp_assign_source_6;
        Py_XDECREF( old );
    }

    // Tried code:
    tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;

    CHECK_OBJECT( tmp_unpack_1 );
    tmp_assign_source_7 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
    if ( tmp_assign_source_7 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        type_description_1 = "ooooooooooooo";
        exception_lineno = 102;
        goto try_except_handler_5;
    }
    {
        PyObject *old = tmp_tuple_unpack_1__element_1;
        tmp_tuple_unpack_1__element_1 = tmp_assign_source_7;
        Py_XDECREF( old );
    }

    tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;

    CHECK_OBJECT( tmp_unpack_2 );
    tmp_assign_source_8 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
    if ( tmp_assign_source_8 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        type_description_1 = "ooooooooooooo";
        exception_lineno = 102;
        goto try_except_handler_5;
    }
    {
        PyObject *old = tmp_tuple_unpack_1__element_2;
        tmp_tuple_unpack_1__element_2 = tmp_assign_source_8;
        Py_XDECREF( old );
    }

    tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;

    CHECK_OBJECT( tmp_iterator_name_1 );
    // Check if iterator has left-over elements.
    CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

    tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

    if (likely( tmp_iterator_attempt == NULL ))
    {
        PyObject *error = GET_ERROR_OCCURRED();

        if ( error != NULL )
        {
            if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
            {
                CLEAR_ERROR_OCCURRED();
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                type_description_1 = "ooooooooooooo";
                exception_lineno = 102;
                goto try_except_handler_5;
            }
        }
    }
    else
    {
        Py_DECREF( tmp_iterator_attempt );

        // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
        PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
        PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

        type_description_1 = "ooooooooooooo";
        exception_lineno = 102;
        goto try_except_handler_5;
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto try_except_handler_4;
    // End of try:
    try_end_2:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto try_except_handler_2;
    // End of try:
    try_end_3:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    tmp_assign_source_9 = tmp_tuple_unpack_1__element_1;

    CHECK_OBJECT( tmp_assign_source_9 );
    {
        PyObject *old = var_modname;
        var_modname = tmp_assign_source_9;
        Py_INCREF( var_modname );
        Py_XDECREF( old );
    }

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    tmp_assign_source_10 = tmp_tuple_unpack_1__element_2;

    CHECK_OBJECT( tmp_assign_source_10 );
    {
        PyObject *old = var_ver_f;
        var_ver_f = tmp_assign_source_10;
        Py_INCREF( var_ver_f );
        Py_XDECREF( old );
    }

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Tried code:
    tmp_compare_left_2 = var_modname;

    if ( tmp_compare_left_2 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "modname" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 104;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_6;
    }

    tmp_source_name_1 = GET_STRING_DICT_VALUE( moduledict_pandas$util$_print_versions, (Nuitka_StringObject *)const_str_plain_sys );

    if (unlikely( tmp_source_name_1 == NULL ))
    {
        tmp_source_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
    }

    if ( tmp_source_name_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 104;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_6;
    }

    tmp_compare_right_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_modules );
    if ( tmp_compare_right_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 104;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_6;
    }
    tmp_cmp_In_1 = PySequence_Contains( tmp_compare_right_2, tmp_compare_left_2 );
    Py_DECREF( tmp_compare_right_2 );
    assert( !(tmp_cmp_In_1 == -1) );
    if ( tmp_cmp_In_1 == 1 )
    {
        goto branch_yes_2;
    }
    else
    {
        goto branch_no_2;
    }
    branch_yes_2:;
    tmp_source_name_2 = GET_STRING_DICT_VALUE( moduledict_pandas$util$_print_versions, (Nuitka_StringObject *)const_str_plain_sys );

    if (unlikely( tmp_source_name_2 == NULL ))
    {
        tmp_source_name_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
    }

    if ( tmp_source_name_2 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 105;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_6;
    }

    tmp_subscribed_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_modules );
    if ( tmp_subscribed_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 105;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_6;
    }
    tmp_subscript_name_1 = var_modname;

    if ( tmp_subscript_name_1 == NULL )
    {
        Py_DECREF( tmp_subscribed_name_1 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "modname" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 105;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_6;
    }

    tmp_assign_source_11 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
    Py_DECREF( tmp_subscribed_name_1 );
    if ( tmp_assign_source_11 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 105;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_6;
    }
    {
        PyObject *old = var_mod;
        var_mod = tmp_assign_source_11;
        Py_XDECREF( old );
    }

    goto branch_end_2;
    branch_no_2:;
    tmp_source_name_3 = GET_STRING_DICT_VALUE( moduledict_pandas$util$_print_versions, (Nuitka_StringObject *)const_str_plain_importlib );

    if (unlikely( tmp_source_name_3 == NULL ))
    {
        tmp_source_name_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_importlib );
    }

    if ( tmp_source_name_3 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "importlib" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 107;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_6;
    }

    tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_import_module );
    if ( tmp_called_name_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 107;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_6;
    }
    tmp_args_element_name_1 = var_modname;

    if ( tmp_args_element_name_1 == NULL )
    {
        Py_DECREF( tmp_called_name_2 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "modname" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 107;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_6;
    }

    frame_e9a5b67587c704036c3f68c226ebe7dc->m_frame.f_lineno = 107;
    {
        PyObject *call_args[] = { tmp_args_element_name_1 };
        tmp_assign_source_12 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
    }

    Py_DECREF( tmp_called_name_2 );
    if ( tmp_assign_source_12 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 107;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_6;
    }
    {
        PyObject *old = var_mod;
        var_mod = tmp_assign_source_12;
        Py_XDECREF( old );
    }

    branch_end_2:;
    tmp_called_name_3 = var_ver_f;

    if ( tmp_called_name_3 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "ver_f" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 108;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_6;
    }

    tmp_args_element_name_2 = var_mod;

    CHECK_OBJECT( tmp_args_element_name_2 );
    frame_e9a5b67587c704036c3f68c226ebe7dc->m_frame.f_lineno = 108;
    {
        PyObject *call_args[] = { tmp_args_element_name_2 };
        tmp_assign_source_13 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
    }

    if ( tmp_assign_source_13 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 108;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_6;
    }
    {
        PyObject *old = var_ver;
        var_ver = tmp_assign_source_13;
        Py_XDECREF( old );
    }

    tmp_source_name_4 = var_deps_blob;

    if ( tmp_source_name_4 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "deps_blob" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 109;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_6;
    }

    tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_append );
    if ( tmp_called_name_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 109;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_6;
    }
    tmp_tuple_element_36 = var_modname;

    if ( tmp_tuple_element_36 == NULL )
    {
        Py_DECREF( tmp_called_name_4 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "modname" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 109;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_6;
    }

    tmp_args_element_name_3 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_36 );
    PyTuple_SET_ITEM( tmp_args_element_name_3, 0, tmp_tuple_element_36 );
    tmp_tuple_element_36 = var_ver;

    if ( tmp_tuple_element_36 == NULL )
    {
        Py_DECREF( tmp_called_name_4 );
        Py_DECREF( tmp_args_element_name_3 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "ver" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 109;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_6;
    }

    Py_INCREF( tmp_tuple_element_36 );
    PyTuple_SET_ITEM( tmp_args_element_name_3, 1, tmp_tuple_element_36 );
    frame_e9a5b67587c704036c3f68c226ebe7dc->m_frame.f_lineno = 109;
    {
        PyObject *call_args[] = { tmp_args_element_name_3 };
        tmp_unused = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
    }

    Py_DECREF( tmp_called_name_4 );
    Py_DECREF( tmp_args_element_name_3 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 109;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_6;
    }
    Py_DECREF( tmp_unused );
    goto try_end_4;
    // Exception handler code:
    try_except_handler_6:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_4 == NULL )
    {
        exception_keeper_tb_4 = MAKE_TRACEBACK( frame_e9a5b67587c704036c3f68c226ebe7dc, exception_keeper_lineno_4 );
    }
    else if ( exception_keeper_lineno_4 != 0 )
    {
        exception_keeper_tb_4 = ADD_TRACEBACK( exception_keeper_tb_4, frame_e9a5b67587c704036c3f68c226ebe7dc, exception_keeper_lineno_4 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_4, &exception_keeper_value_4, &exception_keeper_tb_4 );
    PyException_SetTraceback( exception_keeper_value_4, (PyObject *)exception_keeper_tb_4 );
    PUBLISH_EXCEPTION( &exception_keeper_type_4, &exception_keeper_value_4, &exception_keeper_tb_4 );
    // Tried code:
    tmp_source_name_5 = var_deps_blob;

    if ( tmp_source_name_5 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "deps_blob" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 111;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_7;
    }

    tmp_called_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_append );
    if ( tmp_called_name_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 111;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_7;
    }
    tmp_tuple_element_37 = var_modname;

    if ( tmp_tuple_element_37 == NULL )
    {
        Py_DECREF( tmp_called_name_5 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "modname" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 111;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_7;
    }

    tmp_args_element_name_4 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_37 );
    PyTuple_SET_ITEM( tmp_args_element_name_4, 0, tmp_tuple_element_37 );
    tmp_tuple_element_37 = Py_None;
    Py_INCREF( tmp_tuple_element_37 );
    PyTuple_SET_ITEM( tmp_args_element_name_4, 1, tmp_tuple_element_37 );
    frame_e9a5b67587c704036c3f68c226ebe7dc->m_frame.f_lineno = 111;
    {
        PyObject *call_args[] = { tmp_args_element_name_4 };
        tmp_unused = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, call_args );
    }

    Py_DECREF( tmp_called_name_5 );
    Py_DECREF( tmp_args_element_name_4 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 111;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_7;
    }
    Py_DECREF( tmp_unused );
    goto try_end_5;
    // Exception handler code:
    try_except_handler_7:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto try_except_handler_2;
    // End of try:
    try_end_5:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto try_end_4;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions );
    return NULL;
    // End of try:
    try_end_4:;
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 102;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_6;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_keeper_lineno_6 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_6;
    exception_value = exception_keeper_value_6;
    exception_tb = exception_keeper_tb_6;
    exception_lineno = exception_keeper_lineno_6;

    goto frame_exception_exit_1;
    // End of try:
    try_end_6:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    tmp_cond_value_1 = par_as_json;

    if ( tmp_cond_value_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "as_json" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 113;
        type_description_1 = "ooooooooooooo";
        goto frame_exception_exit_1;
    }

    tmp_cond_truth_1 = CHECK_IF_TRUE( tmp_cond_value_1 );
    if ( tmp_cond_truth_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 113;
        type_description_1 = "ooooooooooooo";
        goto frame_exception_exit_1;
    }
    if ( tmp_cond_truth_1 == 1 )
    {
        goto branch_yes_3;
    }
    else
    {
        goto branch_no_3;
    }
    branch_yes_3:;
    // Tried code:
    tmp_name_name_1 = const_str_plain_json;
    tmp_globals_name_1 = (PyObject *)moduledict_pandas$util$_print_versions;
    tmp_locals_name_1 = Py_None;
    tmp_fromlist_name_1 = Py_None;
    tmp_level_name_1 = const_int_0;
    frame_e9a5b67587c704036c3f68c226ebe7dc->m_frame.f_lineno = 115;
    tmp_assign_source_14 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
    if ( tmp_assign_source_14 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 115;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_8;
    }
    assert( var_json == NULL );
    var_json = tmp_assign_source_14;

    goto try_end_7;
    // Exception handler code:
    try_except_handler_8:;
    exception_keeper_type_7 = exception_type;
    exception_keeper_value_7 = exception_value;
    exception_keeper_tb_7 = exception_tb;
    exception_keeper_lineno_7 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_2 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_2 );
    exception_preserved_value_2 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_2 );
    exception_preserved_tb_2 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_2 );

    if ( exception_keeper_tb_7 == NULL )
    {
        exception_keeper_tb_7 = MAKE_TRACEBACK( frame_e9a5b67587c704036c3f68c226ebe7dc, exception_keeper_lineno_7 );
    }
    else if ( exception_keeper_lineno_7 != 0 )
    {
        exception_keeper_tb_7 = ADD_TRACEBACK( exception_keeper_tb_7, frame_e9a5b67587c704036c3f68c226ebe7dc, exception_keeper_lineno_7 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_7, &exception_keeper_value_7, &exception_keeper_tb_7 );
    PyException_SetTraceback( exception_keeper_value_7, (PyObject *)exception_keeper_tb_7 );
    PUBLISH_EXCEPTION( &exception_keeper_type_7, &exception_keeper_value_7, &exception_keeper_tb_7 );
    // Tried code:
    tmp_name_name_2 = const_str_plain_simplejson;
    tmp_globals_name_2 = (PyObject *)moduledict_pandas$util$_print_versions;
    tmp_locals_name_2 = Py_None;
    tmp_fromlist_name_2 = Py_None;
    tmp_level_name_2 = const_int_0;
    frame_e9a5b67587c704036c3f68c226ebe7dc->m_frame.f_lineno = 117;
    tmp_assign_source_15 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
    if ( tmp_assign_source_15 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 117;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_9;
    }
    assert( var_json == NULL );
    var_json = tmp_assign_source_15;

    goto try_end_8;
    // Exception handler code:
    try_except_handler_9:;
    exception_keeper_type_8 = exception_type;
    exception_keeper_value_8 = exception_value;
    exception_keeper_tb_8 = exception_tb;
    exception_keeper_lineno_8 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_2, exception_preserved_value_2, exception_preserved_tb_2 );
    // Re-raise.
    exception_type = exception_keeper_type_8;
    exception_value = exception_keeper_value_8;
    exception_tb = exception_keeper_tb_8;
    exception_lineno = exception_keeper_lineno_8;

    goto frame_exception_exit_1;
    // End of try:
    try_end_8:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_2, exception_preserved_value_2, exception_preserved_tb_2 );
    goto try_end_7;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions );
    return NULL;
    // End of try:
    try_end_7:;
    tmp_dict_key_1 = const_str_plain_system;
    tmp_dict_seq_1 = var_sys_info;

    if ( tmp_dict_seq_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "sys_info" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 119;
        type_description_1 = "ooooooooooooo";
        goto frame_exception_exit_1;
    }

    tmp_dict_value_1 = TO_DICT( tmp_dict_seq_1, NULL );
    if ( tmp_dict_value_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 119;
        type_description_1 = "ooooooooooooo";
        goto frame_exception_exit_1;
    }
    tmp_assign_source_16 = _PyDict_NewPresized( 2 );
    tmp_res = PyDict_SetItem( tmp_assign_source_16, tmp_dict_key_1, tmp_dict_value_1 );
    Py_DECREF( tmp_dict_value_1 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_2 = const_str_plain_dependencies;
    tmp_dict_seq_2 = var_deps_blob;

    if ( tmp_dict_seq_2 == NULL )
    {
        Py_DECREF( tmp_assign_source_16 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "deps_blob" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 119;
        type_description_1 = "ooooooooooooo";
        goto frame_exception_exit_1;
    }

    tmp_dict_value_2 = TO_DICT( tmp_dict_seq_2, NULL );
    if ( tmp_dict_value_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assign_source_16 );

        exception_lineno = 119;
        type_description_1 = "ooooooooooooo";
        goto frame_exception_exit_1;
    }
    tmp_res = PyDict_SetItem( tmp_assign_source_16, tmp_dict_key_2, tmp_dict_value_2 );
    Py_DECREF( tmp_dict_value_2 );
    assert( !(tmp_res != 0) );
    assert( var_j == NULL );
    var_j = tmp_assign_source_16;

    tmp_compare_left_3 = par_as_json;

    if ( tmp_compare_left_3 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "as_json" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 121;
        type_description_1 = "ooooooooooooo";
        goto frame_exception_exit_1;
    }

    tmp_compare_right_3 = Py_True;
    tmp_is_1 = ( tmp_compare_left_3 == tmp_compare_right_3 );
    if ( tmp_is_1 )
    {
        goto branch_yes_4;
    }
    else
    {
        goto branch_no_4;
    }
    branch_yes_4:;
    tmp_called_name_6 = LOOKUP_BUILTIN( const_str_plain_print );
    assert( tmp_called_name_6 != NULL );
    tmp_args_element_name_5 = var_j;

    if ( tmp_args_element_name_5 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "j" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 122;
        type_description_1 = "ooooooooooooo";
        goto frame_exception_exit_1;
    }

    frame_e9a5b67587c704036c3f68c226ebe7dc->m_frame.f_lineno = 122;
    {
        PyObject *call_args[] = { tmp_args_element_name_5 };
        tmp_unused = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, call_args );
    }

    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 122;
        type_description_1 = "ooooooooooooo";
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_unused );
    goto branch_end_4;
    branch_no_4:;
    // Tried code:
    tmp_source_name_6 = GET_STRING_DICT_VALUE( moduledict_pandas$util$_print_versions, (Nuitka_StringObject *)const_str_plain_codecs );

    if (unlikely( tmp_source_name_6 == NULL ))
    {
        tmp_source_name_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_codecs );
    }

    if ( tmp_source_name_6 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "codecs" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 124;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_10;
    }

    tmp_called_name_7 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_open );
    if ( tmp_called_name_7 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 124;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_10;
    }
    tmp_tuple_element_38 = par_as_json;

    if ( tmp_tuple_element_38 == NULL )
    {
        Py_DECREF( tmp_called_name_7 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "as_json" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 124;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_10;
    }

    tmp_args_name_1 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_38 );
    PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_38 );
    tmp_tuple_element_38 = const_str_plain_wb;
    Py_INCREF( tmp_tuple_element_38 );
    PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_38 );
    tmp_kw_name_1 = PyDict_Copy( const_dict_4b89d655f33250fb2670a6025092b3bf );
    frame_e9a5b67587c704036c3f68c226ebe7dc->m_frame.f_lineno = 124;
    tmp_assign_source_17 = CALL_FUNCTION( tmp_called_name_7, tmp_args_name_1, tmp_kw_name_1 );
    Py_DECREF( tmp_called_name_7 );
    Py_DECREF( tmp_args_name_1 );
    Py_DECREF( tmp_kw_name_1 );
    if ( tmp_assign_source_17 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 124;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_10;
    }
    assert( tmp_with_1__source == NULL );
    tmp_with_1__source = tmp_assign_source_17;

    tmp_source_name_7 = tmp_with_1__source;

    CHECK_OBJECT( tmp_source_name_7 );
    tmp_called_name_8 = LOOKUP_SPECIAL( tmp_source_name_7, const_str_plain___enter__ );
    if ( tmp_called_name_8 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 124;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_10;
    }
    frame_e9a5b67587c704036c3f68c226ebe7dc->m_frame.f_lineno = 124;
    tmp_assign_source_18 = CALL_FUNCTION_NO_ARGS( tmp_called_name_8 );
    Py_DECREF( tmp_called_name_8 );
    if ( tmp_assign_source_18 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 124;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_10;
    }
    assert( tmp_with_1__enter == NULL );
    tmp_with_1__enter = tmp_assign_source_18;

    tmp_source_name_8 = tmp_with_1__source;

    CHECK_OBJECT( tmp_source_name_8 );
    tmp_assign_source_19 = LOOKUP_SPECIAL( tmp_source_name_8, const_str_plain___exit__ );
    if ( tmp_assign_source_19 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 124;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_10;
    }
    assert( tmp_with_1__exit == NULL );
    tmp_with_1__exit = tmp_assign_source_19;

    tmp_assign_source_20 = Py_True;
    assert( tmp_with_1__indicator == NULL );
    Py_INCREF( tmp_assign_source_20 );
    tmp_with_1__indicator = tmp_assign_source_20;

    tmp_assign_source_21 = tmp_with_1__enter;

    CHECK_OBJECT( tmp_assign_source_21 );
    assert( var_f == NULL );
    Py_INCREF( tmp_assign_source_21 );
    var_f = tmp_assign_source_21;

    // Tried code:
    // Tried code:
    tmp_source_name_9 = var_json;

    if ( tmp_source_name_9 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "json" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 125;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_12;
    }

    tmp_called_name_9 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_dump );
    if ( tmp_called_name_9 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 125;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_12;
    }
    tmp_tuple_element_39 = var_j;

    if ( tmp_tuple_element_39 == NULL )
    {
        Py_DECREF( tmp_called_name_9 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "j" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 125;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_12;
    }

    tmp_args_name_2 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_39 );
    PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_39 );
    tmp_tuple_element_39 = var_f;

    if ( tmp_tuple_element_39 == NULL )
    {
        Py_DECREF( tmp_called_name_9 );
        Py_DECREF( tmp_args_name_2 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "f" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 125;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_12;
    }

    Py_INCREF( tmp_tuple_element_39 );
    PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_39 );
    tmp_kw_name_2 = PyDict_Copy( const_dict_38a1dad6291f8c2b00664c08bdb18de6 );
    frame_e9a5b67587c704036c3f68c226ebe7dc->m_frame.f_lineno = 125;
    tmp_unused = CALL_FUNCTION( tmp_called_name_9, tmp_args_name_2, tmp_kw_name_2 );
    Py_DECREF( tmp_called_name_9 );
    Py_DECREF( tmp_args_name_2 );
    Py_DECREF( tmp_kw_name_2 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 125;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_12;
    }
    Py_DECREF( tmp_unused );
    goto try_end_9;
    // Exception handler code:
    try_except_handler_12:;
    exception_keeper_type_9 = exception_type;
    exception_keeper_value_9 = exception_value;
    exception_keeper_tb_9 = exception_tb;
    exception_keeper_lineno_9 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_3 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_3 );
    exception_preserved_value_3 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_3 );
    exception_preserved_tb_3 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_3 );

    if ( exception_keeper_tb_9 == NULL )
    {
        exception_keeper_tb_9 = MAKE_TRACEBACK( frame_e9a5b67587c704036c3f68c226ebe7dc, exception_keeper_lineno_9 );
    }
    else if ( exception_keeper_lineno_9 != 0 )
    {
        exception_keeper_tb_9 = ADD_TRACEBACK( exception_keeper_tb_9, frame_e9a5b67587c704036c3f68c226ebe7dc, exception_keeper_lineno_9 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_9, &exception_keeper_value_9, &exception_keeper_tb_9 );
    PyException_SetTraceback( exception_keeper_value_9, (PyObject *)exception_keeper_tb_9 );
    PUBLISH_EXCEPTION( &exception_keeper_type_9, &exception_keeper_value_9, &exception_keeper_tb_9 );
    // Tried code:
    tmp_compare_left_4 = EXC_TYPE(PyThreadState_GET());
    tmp_compare_right_4 = PyExc_BaseException;
    tmp_exc_match_exception_match_2 = EXCEPTION_MATCH_BOOL( tmp_compare_left_4, tmp_compare_right_4 );
    if ( tmp_exc_match_exception_match_2 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 124;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_13;
    }
    if ( tmp_exc_match_exception_match_2 == 1 )
    {
        goto branch_yes_5;
    }
    else
    {
        goto branch_no_5;
    }
    branch_yes_5:;
    tmp_assign_source_22 = Py_False;
    {
        PyObject *old = tmp_with_1__indicator;
        assert( old != NULL );
        tmp_with_1__indicator = tmp_assign_source_22;
        Py_INCREF( tmp_with_1__indicator );
        Py_DECREF( old );
    }

    tmp_called_name_10 = tmp_with_1__exit;

    CHECK_OBJECT( tmp_called_name_10 );
    tmp_args_element_name_6 = EXC_TYPE(PyThreadState_GET());
    tmp_args_element_name_7 = EXC_VALUE(PyThreadState_GET());
    tmp_args_element_name_8 = EXC_TRACEBACK(PyThreadState_GET());
    frame_e9a5b67587c704036c3f68c226ebe7dc->m_frame.f_lineno = 124;
    {
        PyObject *call_args[] = { tmp_args_element_name_6, tmp_args_element_name_7, tmp_args_element_name_8 };
        tmp_cond_value_2 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_10, call_args );
    }

    if ( tmp_cond_value_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 124;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_13;
    }
    tmp_cond_truth_2 = CHECK_IF_TRUE( tmp_cond_value_2 );
    if ( tmp_cond_truth_2 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_cond_value_2 );

        exception_lineno = 124;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_13;
    }
    Py_DECREF( tmp_cond_value_2 );
    if ( tmp_cond_truth_2 == 1 )
    {
        goto branch_no_6;
    }
    else
    {
        goto branch_yes_6;
    }
    branch_yes_6:;
    tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (unlikely( tmp_result == false ))
    {
        exception_lineno = 124;
    }

    if (exception_tb && exception_tb->tb_frame == &frame_e9a5b67587c704036c3f68c226ebe7dc->m_frame) frame_e9a5b67587c704036c3f68c226ebe7dc->m_frame.f_lineno = exception_tb->tb_lineno;
    type_description_1 = "ooooooooooooo";
    goto try_except_handler_13;
    branch_no_6:;
    goto branch_end_5;
    branch_no_5:;
    tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (unlikely( tmp_result == false ))
    {
        exception_lineno = 124;
    }

    if (exception_tb && exception_tb->tb_frame == &frame_e9a5b67587c704036c3f68c226ebe7dc->m_frame) frame_e9a5b67587c704036c3f68c226ebe7dc->m_frame.f_lineno = exception_tb->tb_lineno;
    type_description_1 = "ooooooooooooo";
    goto try_except_handler_13;
    branch_end_5:;
    goto try_end_10;
    // Exception handler code:
    try_except_handler_13:;
    exception_keeper_type_10 = exception_type;
    exception_keeper_value_10 = exception_value;
    exception_keeper_tb_10 = exception_tb;
    exception_keeper_lineno_10 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_3, exception_preserved_value_3, exception_preserved_tb_3 );
    // Re-raise.
    exception_type = exception_keeper_type_10;
    exception_value = exception_keeper_value_10;
    exception_tb = exception_keeper_tb_10;
    exception_lineno = exception_keeper_lineno_10;

    goto try_except_handler_11;
    // End of try:
    try_end_10:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_3, exception_preserved_value_3, exception_preserved_tb_3 );
    goto try_end_9;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions );
    return NULL;
    // End of try:
    try_end_9:;
    goto try_end_11;
    // Exception handler code:
    try_except_handler_11:;
    exception_keeper_type_11 = exception_type;
    exception_keeper_value_11 = exception_value;
    exception_keeper_tb_11 = exception_tb;
    exception_keeper_lineno_11 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    tmp_compare_left_5 = tmp_with_1__indicator;

    CHECK_OBJECT( tmp_compare_left_5 );
    tmp_compare_right_5 = Py_True;
    tmp_is_2 = ( tmp_compare_left_5 == tmp_compare_right_5 );
    if ( tmp_is_2 )
    {
        goto branch_yes_7;
    }
    else
    {
        goto branch_no_7;
    }
    branch_yes_7:;
    tmp_called_name_11 = tmp_with_1__exit;

    CHECK_OBJECT( tmp_called_name_11 );
    frame_e9a5b67587c704036c3f68c226ebe7dc->m_frame.f_lineno = 124;
    tmp_unused = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_11, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

        Py_DECREF( exception_keeper_type_11 );
        Py_XDECREF( exception_keeper_value_11 );
        Py_XDECREF( exception_keeper_tb_11 );

        exception_lineno = 124;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_10;
    }
    Py_DECREF( tmp_unused );
    branch_no_7:;
    // Re-raise.
    exception_type = exception_keeper_type_11;
    exception_value = exception_keeper_value_11;
    exception_tb = exception_keeper_tb_11;
    exception_lineno = exception_keeper_lineno_11;

    goto try_except_handler_10;
    // End of try:
    try_end_11:;
    tmp_compare_left_6 = tmp_with_1__indicator;

    CHECK_OBJECT( tmp_compare_left_6 );
    tmp_compare_right_6 = Py_True;
    tmp_is_3 = ( tmp_compare_left_6 == tmp_compare_right_6 );
    if ( tmp_is_3 )
    {
        goto branch_yes_8;
    }
    else
    {
        goto branch_no_8;
    }
    branch_yes_8:;
    tmp_called_name_12 = tmp_with_1__exit;

    CHECK_OBJECT( tmp_called_name_12 );
    frame_e9a5b67587c704036c3f68c226ebe7dc->m_frame.f_lineno = 124;
    tmp_unused = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_12, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 124;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_10;
    }
    Py_DECREF( tmp_unused );
    branch_no_8:;
    goto try_end_12;
    // Exception handler code:
    try_except_handler_10:;
    exception_keeper_type_12 = exception_type;
    exception_keeper_value_12 = exception_value;
    exception_keeper_tb_12 = exception_tb;
    exception_keeper_lineno_12 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_with_1__source );
    tmp_with_1__source = NULL;

    Py_XDECREF( tmp_with_1__enter );
    tmp_with_1__enter = NULL;

    Py_XDECREF( tmp_with_1__exit );
    tmp_with_1__exit = NULL;

    Py_XDECREF( tmp_with_1__indicator );
    tmp_with_1__indicator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_12;
    exception_value = exception_keeper_value_12;
    exception_tb = exception_keeper_tb_12;
    exception_lineno = exception_keeper_lineno_12;

    goto frame_exception_exit_1;
    // End of try:
    try_end_12:;
    CHECK_OBJECT( (PyObject *)tmp_with_1__source );
    Py_DECREF( tmp_with_1__source );
    tmp_with_1__source = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_1__enter );
    Py_DECREF( tmp_with_1__enter );
    tmp_with_1__enter = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_1__exit );
    Py_DECREF( tmp_with_1__exit );
    tmp_with_1__exit = NULL;

    Py_XDECREF( tmp_with_1__indicator );
    tmp_with_1__indicator = NULL;

    branch_end_4:;
    goto branch_end_3;
    branch_no_3:;
    tmp_called_name_13 = LOOKUP_BUILTIN( const_str_plain_print );
    assert( tmp_called_name_13 != NULL );
    frame_e9a5b67587c704036c3f68c226ebe7dc->m_frame.f_lineno = 129;
    tmp_unused = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_13, &PyTuple_GET_ITEM( const_tuple_str_digest_a1239f6d2e05f69fe2c2fbddab07d3c6_tuple, 0 ) );

    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 129;
        type_description_1 = "ooooooooooooo";
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_unused );
    tmp_called_name_14 = LOOKUP_BUILTIN( const_str_plain_print );
    assert( tmp_called_name_14 != NULL );
    frame_e9a5b67587c704036c3f68c226ebe7dc->m_frame.f_lineno = 130;
    tmp_unused = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_14, &PyTuple_GET_ITEM( const_tuple_str_digest_086976f9ec3c181ebceaa1c4f60ec335_tuple, 0 ) );

    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 130;
        type_description_1 = "ooooooooooooo";
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_unused );
    tmp_iter_arg_3 = var_sys_info;

    if ( tmp_iter_arg_3 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "sys_info" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 132;
        type_description_1 = "ooooooooooooo";
        goto frame_exception_exit_1;
    }

    tmp_assign_source_23 = MAKE_ITERATOR( tmp_iter_arg_3 );
    if ( tmp_assign_source_23 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 132;
        type_description_1 = "ooooooooooooo";
        goto frame_exception_exit_1;
    }
    assert( tmp_for_loop_2__for_iterator == NULL );
    tmp_for_loop_2__for_iterator = tmp_assign_source_23;

    // Tried code:
    loop_start_2:;
    // Tried code:
    tmp_value_name_2 = tmp_for_loop_2__for_iterator;

    CHECK_OBJECT( tmp_value_name_2 );
    tmp_assign_source_24 = ITERATOR_NEXT( tmp_value_name_2 );
    if ( tmp_assign_source_24 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        type_description_1 = "ooooooooooooo";
        exception_lineno = 132;
        goto try_except_handler_15;
    }
    {
        PyObject *old = tmp_for_loop_2__iter_value;
        tmp_for_loop_2__iter_value = tmp_assign_source_24;
        Py_XDECREF( old );
    }

    goto try_end_13;
    // Exception handler code:
    try_except_handler_15:;
    exception_keeper_type_13 = exception_type;
    exception_keeper_value_13 = exception_value;
    exception_keeper_tb_13 = exception_tb;
    exception_keeper_lineno_13 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    tmp_compare_left_7 = exception_keeper_type_13;
    tmp_compare_right_7 = PyExc_StopIteration;
    tmp_exc_match_exception_match_3 = EXCEPTION_MATCH_BOOL( tmp_compare_left_7, tmp_compare_right_7 );
    if ( tmp_exc_match_exception_match_3 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

        Py_DECREF( exception_keeper_type_13 );
        Py_XDECREF( exception_keeper_value_13 );
        Py_XDECREF( exception_keeper_tb_13 );

        exception_lineno = 132;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_14;
    }
    if ( tmp_exc_match_exception_match_3 == 1 )
    {
        goto branch_yes_9;
    }
    else
    {
        goto branch_no_9;
    }
    branch_yes_9:;
    Py_DECREF( exception_keeper_type_13 );
    Py_XDECREF( exception_keeper_value_13 );
    Py_XDECREF( exception_keeper_tb_13 );
    goto loop_end_2;
    goto branch_end_9;
    branch_no_9:;
    // Re-raise.
    exception_type = exception_keeper_type_13;
    exception_value = exception_keeper_value_13;
    exception_tb = exception_keeper_tb_13;
    exception_lineno = exception_keeper_lineno_13;

    goto try_except_handler_14;
    branch_end_9:;
    // End of try:
    try_end_13:;
    // Tried code:
    tmp_iter_arg_4 = tmp_for_loop_2__iter_value;

    CHECK_OBJECT( tmp_iter_arg_4 );
    tmp_assign_source_25 = MAKE_ITERATOR( tmp_iter_arg_4 );
    if ( tmp_assign_source_25 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 132;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_16;
    }
    {
        PyObject *old = tmp_tuple_unpack_2__source_iter;
        tmp_tuple_unpack_2__source_iter = tmp_assign_source_25;
        Py_XDECREF( old );
    }

    // Tried code:
    tmp_unpack_3 = tmp_tuple_unpack_2__source_iter;

    CHECK_OBJECT( tmp_unpack_3 );
    tmp_assign_source_26 = UNPACK_NEXT( tmp_unpack_3, 0, 2 );
    if ( tmp_assign_source_26 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        type_description_1 = "ooooooooooooo";
        exception_lineno = 132;
        goto try_except_handler_17;
    }
    {
        PyObject *old = tmp_tuple_unpack_2__element_1;
        tmp_tuple_unpack_2__element_1 = tmp_assign_source_26;
        Py_XDECREF( old );
    }

    tmp_unpack_4 = tmp_tuple_unpack_2__source_iter;

    CHECK_OBJECT( tmp_unpack_4 );
    tmp_assign_source_27 = UNPACK_NEXT( tmp_unpack_4, 1, 2 );
    if ( tmp_assign_source_27 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        type_description_1 = "ooooooooooooo";
        exception_lineno = 132;
        goto try_except_handler_17;
    }
    {
        PyObject *old = tmp_tuple_unpack_2__element_2;
        tmp_tuple_unpack_2__element_2 = tmp_assign_source_27;
        Py_XDECREF( old );
    }

    tmp_iterator_name_2 = tmp_tuple_unpack_2__source_iter;

    CHECK_OBJECT( tmp_iterator_name_2 );
    // Check if iterator has left-over elements.
    CHECK_OBJECT( tmp_iterator_name_2 ); assert( HAS_ITERNEXT( tmp_iterator_name_2 ) );

    tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_2 )->tp_iternext)( tmp_iterator_name_2 );

    if (likely( tmp_iterator_attempt == NULL ))
    {
        PyObject *error = GET_ERROR_OCCURRED();

        if ( error != NULL )
        {
            if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
            {
                CLEAR_ERROR_OCCURRED();
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                type_description_1 = "ooooooooooooo";
                exception_lineno = 132;
                goto try_except_handler_17;
            }
        }
    }
    else
    {
        Py_DECREF( tmp_iterator_attempt );

        // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
        PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
        PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

        type_description_1 = "ooooooooooooo";
        exception_lineno = 132;
        goto try_except_handler_17;
    }
    goto try_end_14;
    // Exception handler code:
    try_except_handler_17:;
    exception_keeper_type_14 = exception_type;
    exception_keeper_value_14 = exception_value;
    exception_keeper_tb_14 = exception_tb;
    exception_keeper_lineno_14 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_2__source_iter );
    Py_DECREF( tmp_tuple_unpack_2__source_iter );
    tmp_tuple_unpack_2__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_14;
    exception_value = exception_keeper_value_14;
    exception_tb = exception_keeper_tb_14;
    exception_lineno = exception_keeper_lineno_14;

    goto try_except_handler_16;
    // End of try:
    try_end_14:;
    goto try_end_15;
    // Exception handler code:
    try_except_handler_16:;
    exception_keeper_type_15 = exception_type;
    exception_keeper_value_15 = exception_value;
    exception_keeper_tb_15 = exception_tb;
    exception_keeper_lineno_15 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_2__element_1 );
    tmp_tuple_unpack_2__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_2__element_2 );
    tmp_tuple_unpack_2__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_15;
    exception_value = exception_keeper_value_15;
    exception_tb = exception_keeper_tb_15;
    exception_lineno = exception_keeper_lineno_15;

    goto try_except_handler_14;
    // End of try:
    try_end_15:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_2__source_iter );
    Py_DECREF( tmp_tuple_unpack_2__source_iter );
    tmp_tuple_unpack_2__source_iter = NULL;

    tmp_assign_source_28 = tmp_tuple_unpack_2__element_1;

    CHECK_OBJECT( tmp_assign_source_28 );
    {
        PyObject *old = var_k;
        var_k = tmp_assign_source_28;
        Py_INCREF( var_k );
        Py_XDECREF( old );
    }

    Py_XDECREF( tmp_tuple_unpack_2__element_1 );
    tmp_tuple_unpack_2__element_1 = NULL;

    tmp_assign_source_29 = tmp_tuple_unpack_2__element_2;

    CHECK_OBJECT( tmp_assign_source_29 );
    {
        PyObject *old = var_stat;
        var_stat = tmp_assign_source_29;
        Py_INCREF( var_stat );
        Py_XDECREF( old );
    }

    Py_XDECREF( tmp_tuple_unpack_2__element_2 );
    tmp_tuple_unpack_2__element_2 = NULL;

    tmp_called_name_15 = LOOKUP_BUILTIN( const_str_plain_print );
    assert( tmp_called_name_15 != NULL );
    tmp_source_name_10 = const_str_digest_3c287b5bec3e65b64e8bf11becff175b;
    tmp_called_name_16 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_format );
    assert( !(tmp_called_name_16 == NULL) );
    tmp_dict_key_3 = const_str_plain_k;
    tmp_dict_value_3 = var_k;

    if ( tmp_dict_value_3 == NULL )
    {
        Py_DECREF( tmp_called_name_16 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "k" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 133;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_14;
    }

    tmp_kw_name_3 = _PyDict_NewPresized( 2 );
    tmp_res = PyDict_SetItem( tmp_kw_name_3, tmp_dict_key_3, tmp_dict_value_3 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_4 = const_str_plain_stat;
    tmp_dict_value_4 = var_stat;

    if ( tmp_dict_value_4 == NULL )
    {
        Py_DECREF( tmp_called_name_16 );
        Py_DECREF( tmp_kw_name_3 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "stat" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 133;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_14;
    }

    tmp_res = PyDict_SetItem( tmp_kw_name_3, tmp_dict_key_4, tmp_dict_value_4 );
    assert( !(tmp_res != 0) );
    frame_e9a5b67587c704036c3f68c226ebe7dc->m_frame.f_lineno = 133;
    tmp_args_element_name_9 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_16, tmp_kw_name_3 );
    Py_DECREF( tmp_called_name_16 );
    Py_DECREF( tmp_kw_name_3 );
    if ( tmp_args_element_name_9 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 133;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_14;
    }
    frame_e9a5b67587c704036c3f68c226ebe7dc->m_frame.f_lineno = 133;
    {
        PyObject *call_args[] = { tmp_args_element_name_9 };
        tmp_unused = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_15, call_args );
    }

    Py_DECREF( tmp_args_element_name_9 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 133;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_14;
    }
    Py_DECREF( tmp_unused );
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 132;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_14;
    }
    goto loop_start_2;
    loop_end_2:;
    goto try_end_16;
    // Exception handler code:
    try_except_handler_14:;
    exception_keeper_type_16 = exception_type;
    exception_keeper_value_16 = exception_value;
    exception_keeper_tb_16 = exception_tb;
    exception_keeper_lineno_16 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_2__iter_value );
    tmp_for_loop_2__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
    Py_DECREF( tmp_for_loop_2__for_iterator );
    tmp_for_loop_2__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_16;
    exception_value = exception_keeper_value_16;
    exception_tb = exception_keeper_tb_16;
    exception_lineno = exception_keeper_lineno_16;

    goto frame_exception_exit_1;
    // End of try:
    try_end_16:;
    Py_XDECREF( tmp_for_loop_2__iter_value );
    tmp_for_loop_2__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
    Py_DECREF( tmp_for_loop_2__for_iterator );
    tmp_for_loop_2__for_iterator = NULL;

    tmp_called_name_17 = LOOKUP_BUILTIN( const_str_plain_print );
    assert( tmp_called_name_17 != NULL );
    frame_e9a5b67587c704036c3f68c226ebe7dc->m_frame.f_lineno = 135;
    tmp_unused = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_17, &PyTuple_GET_ITEM( const_tuple_str_empty_tuple, 0 ) );

    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 135;
        type_description_1 = "ooooooooooooo";
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_unused );
    tmp_iter_arg_5 = var_deps_blob;

    if ( tmp_iter_arg_5 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "deps_blob" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 136;
        type_description_1 = "ooooooooooooo";
        goto frame_exception_exit_1;
    }

    tmp_assign_source_30 = MAKE_ITERATOR( tmp_iter_arg_5 );
    if ( tmp_assign_source_30 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 136;
        type_description_1 = "ooooooooooooo";
        goto frame_exception_exit_1;
    }
    assert( tmp_for_loop_3__for_iterator == NULL );
    tmp_for_loop_3__for_iterator = tmp_assign_source_30;

    // Tried code:
    loop_start_3:;
    // Tried code:
    tmp_value_name_3 = tmp_for_loop_3__for_iterator;

    CHECK_OBJECT( tmp_value_name_3 );
    tmp_assign_source_31 = ITERATOR_NEXT( tmp_value_name_3 );
    if ( tmp_assign_source_31 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        type_description_1 = "ooooooooooooo";
        exception_lineno = 136;
        goto try_except_handler_19;
    }
    {
        PyObject *old = tmp_for_loop_3__iter_value;
        tmp_for_loop_3__iter_value = tmp_assign_source_31;
        Py_XDECREF( old );
    }

    goto try_end_17;
    // Exception handler code:
    try_except_handler_19:;
    exception_keeper_type_17 = exception_type;
    exception_keeper_value_17 = exception_value;
    exception_keeper_tb_17 = exception_tb;
    exception_keeper_lineno_17 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    tmp_compare_left_8 = exception_keeper_type_17;
    tmp_compare_right_8 = PyExc_StopIteration;
    tmp_exc_match_exception_match_4 = EXCEPTION_MATCH_BOOL( tmp_compare_left_8, tmp_compare_right_8 );
    if ( tmp_exc_match_exception_match_4 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

        Py_DECREF( exception_keeper_type_17 );
        Py_XDECREF( exception_keeper_value_17 );
        Py_XDECREF( exception_keeper_tb_17 );

        exception_lineno = 136;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_18;
    }
    if ( tmp_exc_match_exception_match_4 == 1 )
    {
        goto branch_yes_10;
    }
    else
    {
        goto branch_no_10;
    }
    branch_yes_10:;
    Py_DECREF( exception_keeper_type_17 );
    Py_XDECREF( exception_keeper_value_17 );
    Py_XDECREF( exception_keeper_tb_17 );
    goto loop_end_3;
    goto branch_end_10;
    branch_no_10:;
    // Re-raise.
    exception_type = exception_keeper_type_17;
    exception_value = exception_keeper_value_17;
    exception_tb = exception_keeper_tb_17;
    exception_lineno = exception_keeper_lineno_17;

    goto try_except_handler_18;
    branch_end_10:;
    // End of try:
    try_end_17:;
    // Tried code:
    tmp_iter_arg_6 = tmp_for_loop_3__iter_value;

    CHECK_OBJECT( tmp_iter_arg_6 );
    tmp_assign_source_32 = MAKE_ITERATOR( tmp_iter_arg_6 );
    if ( tmp_assign_source_32 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 136;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_20;
    }
    {
        PyObject *old = tmp_tuple_unpack_3__source_iter;
        tmp_tuple_unpack_3__source_iter = tmp_assign_source_32;
        Py_XDECREF( old );
    }

    // Tried code:
    tmp_unpack_5 = tmp_tuple_unpack_3__source_iter;

    CHECK_OBJECT( tmp_unpack_5 );
    tmp_assign_source_33 = UNPACK_NEXT( tmp_unpack_5, 0, 2 );
    if ( tmp_assign_source_33 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        type_description_1 = "ooooooooooooo";
        exception_lineno = 136;
        goto try_except_handler_21;
    }
    {
        PyObject *old = tmp_tuple_unpack_3__element_1;
        tmp_tuple_unpack_3__element_1 = tmp_assign_source_33;
        Py_XDECREF( old );
    }

    tmp_unpack_6 = tmp_tuple_unpack_3__source_iter;

    CHECK_OBJECT( tmp_unpack_6 );
    tmp_assign_source_34 = UNPACK_NEXT( tmp_unpack_6, 1, 2 );
    if ( tmp_assign_source_34 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        type_description_1 = "ooooooooooooo";
        exception_lineno = 136;
        goto try_except_handler_21;
    }
    {
        PyObject *old = tmp_tuple_unpack_3__element_2;
        tmp_tuple_unpack_3__element_2 = tmp_assign_source_34;
        Py_XDECREF( old );
    }

    tmp_iterator_name_3 = tmp_tuple_unpack_3__source_iter;

    CHECK_OBJECT( tmp_iterator_name_3 );
    // Check if iterator has left-over elements.
    CHECK_OBJECT( tmp_iterator_name_3 ); assert( HAS_ITERNEXT( tmp_iterator_name_3 ) );

    tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_3 )->tp_iternext)( tmp_iterator_name_3 );

    if (likely( tmp_iterator_attempt == NULL ))
    {
        PyObject *error = GET_ERROR_OCCURRED();

        if ( error != NULL )
        {
            if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
            {
                CLEAR_ERROR_OCCURRED();
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                type_description_1 = "ooooooooooooo";
                exception_lineno = 136;
                goto try_except_handler_21;
            }
        }
    }
    else
    {
        Py_DECREF( tmp_iterator_attempt );

        // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
        PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
        PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

        type_description_1 = "ooooooooooooo";
        exception_lineno = 136;
        goto try_except_handler_21;
    }
    goto try_end_18;
    // Exception handler code:
    try_except_handler_21:;
    exception_keeper_type_18 = exception_type;
    exception_keeper_value_18 = exception_value;
    exception_keeper_tb_18 = exception_tb;
    exception_keeper_lineno_18 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_3__source_iter );
    Py_DECREF( tmp_tuple_unpack_3__source_iter );
    tmp_tuple_unpack_3__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_18;
    exception_value = exception_keeper_value_18;
    exception_tb = exception_keeper_tb_18;
    exception_lineno = exception_keeper_lineno_18;

    goto try_except_handler_20;
    // End of try:
    try_end_18:;
    goto try_end_19;
    // Exception handler code:
    try_except_handler_20:;
    exception_keeper_type_19 = exception_type;
    exception_keeper_value_19 = exception_value;
    exception_keeper_tb_19 = exception_tb;
    exception_keeper_lineno_19 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_3__element_1 );
    tmp_tuple_unpack_3__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_3__element_2 );
    tmp_tuple_unpack_3__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_19;
    exception_value = exception_keeper_value_19;
    exception_tb = exception_keeper_tb_19;
    exception_lineno = exception_keeper_lineno_19;

    goto try_except_handler_18;
    // End of try:
    try_end_19:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_3__source_iter );
    Py_DECREF( tmp_tuple_unpack_3__source_iter );
    tmp_tuple_unpack_3__source_iter = NULL;

    tmp_assign_source_35 = tmp_tuple_unpack_3__element_1;

    CHECK_OBJECT( tmp_assign_source_35 );
    {
        PyObject *old = var_k;
        var_k = tmp_assign_source_35;
        Py_INCREF( var_k );
        Py_XDECREF( old );
    }

    Py_XDECREF( tmp_tuple_unpack_3__element_1 );
    tmp_tuple_unpack_3__element_1 = NULL;

    tmp_assign_source_36 = tmp_tuple_unpack_3__element_2;

    CHECK_OBJECT( tmp_assign_source_36 );
    {
        PyObject *old = var_stat;
        var_stat = tmp_assign_source_36;
        Py_INCREF( var_stat );
        Py_XDECREF( old );
    }

    Py_XDECREF( tmp_tuple_unpack_3__element_2 );
    tmp_tuple_unpack_3__element_2 = NULL;

    tmp_called_name_18 = LOOKUP_BUILTIN( const_str_plain_print );
    assert( tmp_called_name_18 != NULL );
    tmp_source_name_11 = const_str_digest_3c287b5bec3e65b64e8bf11becff175b;
    tmp_called_name_19 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_format );
    assert( !(tmp_called_name_19 == NULL) );
    tmp_dict_key_5 = const_str_plain_k;
    tmp_dict_value_5 = var_k;

    if ( tmp_dict_value_5 == NULL )
    {
        Py_DECREF( tmp_called_name_19 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "k" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 137;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_18;
    }

    tmp_kw_name_4 = _PyDict_NewPresized( 2 );
    tmp_res = PyDict_SetItem( tmp_kw_name_4, tmp_dict_key_5, tmp_dict_value_5 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_6 = const_str_plain_stat;
    tmp_dict_value_6 = var_stat;

    if ( tmp_dict_value_6 == NULL )
    {
        Py_DECREF( tmp_called_name_19 );
        Py_DECREF( tmp_kw_name_4 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "stat" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 137;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_18;
    }

    tmp_res = PyDict_SetItem( tmp_kw_name_4, tmp_dict_key_6, tmp_dict_value_6 );
    assert( !(tmp_res != 0) );
    frame_e9a5b67587c704036c3f68c226ebe7dc->m_frame.f_lineno = 137;
    tmp_args_element_name_10 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_19, tmp_kw_name_4 );
    Py_DECREF( tmp_called_name_19 );
    Py_DECREF( tmp_kw_name_4 );
    if ( tmp_args_element_name_10 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 137;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_18;
    }
    frame_e9a5b67587c704036c3f68c226ebe7dc->m_frame.f_lineno = 137;
    {
        PyObject *call_args[] = { tmp_args_element_name_10 };
        tmp_unused = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_18, call_args );
    }

    Py_DECREF( tmp_args_element_name_10 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 137;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_18;
    }
    Py_DECREF( tmp_unused );
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 136;
        type_description_1 = "ooooooooooooo";
        goto try_except_handler_18;
    }
    goto loop_start_3;
    loop_end_3:;
    goto try_end_20;
    // Exception handler code:
    try_except_handler_18:;
    exception_keeper_type_20 = exception_type;
    exception_keeper_value_20 = exception_value;
    exception_keeper_tb_20 = exception_tb;
    exception_keeper_lineno_20 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_3__iter_value );
    tmp_for_loop_3__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_3__for_iterator );
    Py_DECREF( tmp_for_loop_3__for_iterator );
    tmp_for_loop_3__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_20;
    exception_value = exception_keeper_value_20;
    exception_tb = exception_keeper_tb_20;
    exception_lineno = exception_keeper_lineno_20;

    goto frame_exception_exit_1;
    // End of try:
    try_end_20:;
    Py_XDECREF( tmp_for_loop_3__iter_value );
    tmp_for_loop_3__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_3__for_iterator );
    Py_DECREF( tmp_for_loop_3__for_iterator );
    tmp_for_loop_3__for_iterator = NULL;

    branch_end_3:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e9a5b67587c704036c3f68c226ebe7dc );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e9a5b67587c704036c3f68c226ebe7dc );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_e9a5b67587c704036c3f68c226ebe7dc, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_e9a5b67587c704036c3f68c226ebe7dc->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_e9a5b67587c704036c3f68c226ebe7dc, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_e9a5b67587c704036c3f68c226ebe7dc,
        type_description_1,
        par_as_json,
        var_sys_info,
        var_deps,
        var_deps_blob,
        var_modname,
        var_ver_f,
        var_mod,
        var_ver,
        var_json,
        var_j,
        var_f,
        var_k,
        var_stat
    );


    // Release cached frame.
    if ( frame_e9a5b67587c704036c3f68c226ebe7dc == cache_frame_e9a5b67587c704036c3f68c226ebe7dc )
    {
        Py_DECREF( frame_e9a5b67587c704036c3f68c226ebe7dc );
    }
    cache_frame_e9a5b67587c704036c3f68c226ebe7dc = NULL;

    assertFrameObject( frame_e9a5b67587c704036c3f68c226ebe7dc );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_as_json );
    par_as_json = NULL;

    Py_XDECREF( var_sys_info );
    var_sys_info = NULL;

    Py_XDECREF( var_deps );
    var_deps = NULL;

    Py_XDECREF( var_deps_blob );
    var_deps_blob = NULL;

    Py_XDECREF( var_modname );
    var_modname = NULL;

    Py_XDECREF( var_ver_f );
    var_ver_f = NULL;

    Py_XDECREF( var_mod );
    var_mod = NULL;

    Py_XDECREF( var_ver );
    var_ver = NULL;

    Py_XDECREF( var_json );
    var_json = NULL;

    Py_XDECREF( var_j );
    var_j = NULL;

    Py_XDECREF( var_f );
    var_f = NULL;

    Py_XDECREF( var_k );
    var_k = NULL;

    Py_XDECREF( var_stat );
    var_stat = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_21 = exception_type;
    exception_keeper_value_21 = exception_value;
    exception_keeper_tb_21 = exception_tb;
    exception_keeper_lineno_21 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_as_json );
    par_as_json = NULL;

    Py_XDECREF( var_sys_info );
    var_sys_info = NULL;

    Py_XDECREF( var_deps );
    var_deps = NULL;

    Py_XDECREF( var_deps_blob );
    var_deps_blob = NULL;

    Py_XDECREF( var_modname );
    var_modname = NULL;

    Py_XDECREF( var_ver_f );
    var_ver_f = NULL;

    Py_XDECREF( var_mod );
    var_mod = NULL;

    Py_XDECREF( var_ver );
    var_ver = NULL;

    Py_XDECREF( var_json );
    var_json = NULL;

    Py_XDECREF( var_j );
    var_j = NULL;

    Py_XDECREF( var_f );
    var_f = NULL;

    Py_XDECREF( var_k );
    var_k = NULL;

    Py_XDECREF( var_stat );
    var_stat = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_21;
    exception_value = exception_keeper_value_21;
    exception_tb = exception_keeper_tb_21;
    exception_lineno = exception_keeper_lineno_21;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_1_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_mod = python_pars[ 0 ];
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    static struct Nuitka_FrameObject *cache_frame_926b2cd8a50bada964ab9ff7ea36c401 = NULL;

    struct Nuitka_FrameObject *frame_926b2cd8a50bada964ab9ff7ea36c401;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_926b2cd8a50bada964ab9ff7ea36c401, codeobj_926b2cd8a50bada964ab9ff7ea36c401, module_pandas$util$_print_versions, sizeof(void *) );
    frame_926b2cd8a50bada964ab9ff7ea36c401 = cache_frame_926b2cd8a50bada964ab9ff7ea36c401;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_926b2cd8a50bada964ab9ff7ea36c401 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_926b2cd8a50bada964ab9ff7ea36c401 ) == 2 ); // Frame stack

    // Framed code:
    tmp_source_name_1 = par_mod;

    CHECK_OBJECT( tmp_source_name_1 );
    tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___version__ );
    if ( tmp_return_value == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 64;
        type_description_1 = "o";
        goto frame_exception_exit_1;
    }
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_926b2cd8a50bada964ab9ff7ea36c401 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_926b2cd8a50bada964ab9ff7ea36c401 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_926b2cd8a50bada964ab9ff7ea36c401 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_926b2cd8a50bada964ab9ff7ea36c401, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_926b2cd8a50bada964ab9ff7ea36c401->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_926b2cd8a50bada964ab9ff7ea36c401, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_926b2cd8a50bada964ab9ff7ea36c401,
        type_description_1,
        par_mod
    );


    // Release cached frame.
    if ( frame_926b2cd8a50bada964ab9ff7ea36c401 == cache_frame_926b2cd8a50bada964ab9ff7ea36c401 )
    {
        Py_DECREF( frame_926b2cd8a50bada964ab9ff7ea36c401 );
    }
    cache_frame_926b2cd8a50bada964ab9ff7ea36c401 = NULL;

    assertFrameObject( frame_926b2cd8a50bada964ab9ff7ea36c401 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_1_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_mod );
    par_mod = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_mod );
    par_mod = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_1_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_2_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_mod = python_pars[ 0 ];
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    static struct Nuitka_FrameObject *cache_frame_8b947b74bca15c4b71a777120c297bfa = NULL;

    struct Nuitka_FrameObject *frame_8b947b74bca15c4b71a777120c297bfa;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_8b947b74bca15c4b71a777120c297bfa, codeobj_8b947b74bca15c4b71a777120c297bfa, module_pandas$util$_print_versions, sizeof(void *) );
    frame_8b947b74bca15c4b71a777120c297bfa = cache_frame_8b947b74bca15c4b71a777120c297bfa;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_8b947b74bca15c4b71a777120c297bfa );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_8b947b74bca15c4b71a777120c297bfa ) == 2 ); // Frame stack

    // Framed code:
    tmp_source_name_1 = par_mod;

    CHECK_OBJECT( tmp_source_name_1 );
    tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___version__ );
    if ( tmp_return_value == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 65;
        type_description_1 = "o";
        goto frame_exception_exit_1;
    }
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8b947b74bca15c4b71a777120c297bfa );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_8b947b74bca15c4b71a777120c297bfa );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8b947b74bca15c4b71a777120c297bfa );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_8b947b74bca15c4b71a777120c297bfa, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_8b947b74bca15c4b71a777120c297bfa->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_8b947b74bca15c4b71a777120c297bfa, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_8b947b74bca15c4b71a777120c297bfa,
        type_description_1,
        par_mod
    );


    // Release cached frame.
    if ( frame_8b947b74bca15c4b71a777120c297bfa == cache_frame_8b947b74bca15c4b71a777120c297bfa )
    {
        Py_DECREF( frame_8b947b74bca15c4b71a777120c297bfa );
    }
    cache_frame_8b947b74bca15c4b71a777120c297bfa = NULL;

    assertFrameObject( frame_8b947b74bca15c4b71a777120c297bfa );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_2_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_mod );
    par_mod = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_mod );
    par_mod = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_2_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_3_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_mod = python_pars[ 0 ];
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    static struct Nuitka_FrameObject *cache_frame_e41b1c514ea1750764423a6ad3fa842b = NULL;

    struct Nuitka_FrameObject *frame_e41b1c514ea1750764423a6ad3fa842b;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_e41b1c514ea1750764423a6ad3fa842b, codeobj_e41b1c514ea1750764423a6ad3fa842b, module_pandas$util$_print_versions, sizeof(void *) );
    frame_e41b1c514ea1750764423a6ad3fa842b = cache_frame_e41b1c514ea1750764423a6ad3fa842b;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_e41b1c514ea1750764423a6ad3fa842b );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_e41b1c514ea1750764423a6ad3fa842b ) == 2 ); // Frame stack

    // Framed code:
    tmp_source_name_1 = par_mod;

    CHECK_OBJECT( tmp_source_name_1 );
    tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___version__ );
    if ( tmp_return_value == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 66;
        type_description_1 = "o";
        goto frame_exception_exit_1;
    }
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e41b1c514ea1750764423a6ad3fa842b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_e41b1c514ea1750764423a6ad3fa842b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e41b1c514ea1750764423a6ad3fa842b );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_e41b1c514ea1750764423a6ad3fa842b, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_e41b1c514ea1750764423a6ad3fa842b->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_e41b1c514ea1750764423a6ad3fa842b, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_e41b1c514ea1750764423a6ad3fa842b,
        type_description_1,
        par_mod
    );


    // Release cached frame.
    if ( frame_e41b1c514ea1750764423a6ad3fa842b == cache_frame_e41b1c514ea1750764423a6ad3fa842b )
    {
        Py_DECREF( frame_e41b1c514ea1750764423a6ad3fa842b );
    }
    cache_frame_e41b1c514ea1750764423a6ad3fa842b = NULL;

    assertFrameObject( frame_e41b1c514ea1750764423a6ad3fa842b );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_3_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_mod );
    par_mod = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_mod );
    par_mod = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_3_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_4_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_mod = python_pars[ 0 ];
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    static struct Nuitka_FrameObject *cache_frame_f51c2fec9c6a331cc5981904628c07e1 = NULL;

    struct Nuitka_FrameObject *frame_f51c2fec9c6a331cc5981904628c07e1;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_f51c2fec9c6a331cc5981904628c07e1, codeobj_f51c2fec9c6a331cc5981904628c07e1, module_pandas$util$_print_versions, sizeof(void *) );
    frame_f51c2fec9c6a331cc5981904628c07e1 = cache_frame_f51c2fec9c6a331cc5981904628c07e1;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_f51c2fec9c6a331cc5981904628c07e1 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_f51c2fec9c6a331cc5981904628c07e1 ) == 2 ); // Frame stack

    // Framed code:
    tmp_source_name_1 = par_mod;

    CHECK_OBJECT( tmp_source_name_1 );
    tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___version__ );
    if ( tmp_return_value == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 67;
        type_description_1 = "o";
        goto frame_exception_exit_1;
    }
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f51c2fec9c6a331cc5981904628c07e1 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_f51c2fec9c6a331cc5981904628c07e1 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f51c2fec9c6a331cc5981904628c07e1 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_f51c2fec9c6a331cc5981904628c07e1, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_f51c2fec9c6a331cc5981904628c07e1->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_f51c2fec9c6a331cc5981904628c07e1, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_f51c2fec9c6a331cc5981904628c07e1,
        type_description_1,
        par_mod
    );


    // Release cached frame.
    if ( frame_f51c2fec9c6a331cc5981904628c07e1 == cache_frame_f51c2fec9c6a331cc5981904628c07e1 )
    {
        Py_DECREF( frame_f51c2fec9c6a331cc5981904628c07e1 );
    }
    cache_frame_f51c2fec9c6a331cc5981904628c07e1 = NULL;

    assertFrameObject( frame_f51c2fec9c6a331cc5981904628c07e1 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_4_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_mod );
    par_mod = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_mod );
    par_mod = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_4_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_5_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_mod = python_pars[ 0 ];
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    static struct Nuitka_FrameObject *cache_frame_f73f0991cf3aa1993b37c583fcf51a1c = NULL;

    struct Nuitka_FrameObject *frame_f73f0991cf3aa1993b37c583fcf51a1c;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_f73f0991cf3aa1993b37c583fcf51a1c, codeobj_f73f0991cf3aa1993b37c583fcf51a1c, module_pandas$util$_print_versions, sizeof(void *) );
    frame_f73f0991cf3aa1993b37c583fcf51a1c = cache_frame_f73f0991cf3aa1993b37c583fcf51a1c;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_f73f0991cf3aa1993b37c583fcf51a1c );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_f73f0991cf3aa1993b37c583fcf51a1c ) == 2 ); // Frame stack

    // Framed code:
    tmp_source_name_1 = par_mod;

    CHECK_OBJECT( tmp_source_name_1 );
    tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___version__ );
    if ( tmp_return_value == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 68;
        type_description_1 = "o";
        goto frame_exception_exit_1;
    }
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f73f0991cf3aa1993b37c583fcf51a1c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_f73f0991cf3aa1993b37c583fcf51a1c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f73f0991cf3aa1993b37c583fcf51a1c );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_f73f0991cf3aa1993b37c583fcf51a1c, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_f73f0991cf3aa1993b37c583fcf51a1c->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_f73f0991cf3aa1993b37c583fcf51a1c, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_f73f0991cf3aa1993b37c583fcf51a1c,
        type_description_1,
        par_mod
    );


    // Release cached frame.
    if ( frame_f73f0991cf3aa1993b37c583fcf51a1c == cache_frame_f73f0991cf3aa1993b37c583fcf51a1c )
    {
        Py_DECREF( frame_f73f0991cf3aa1993b37c583fcf51a1c );
    }
    cache_frame_f73f0991cf3aa1993b37c583fcf51a1c = NULL;

    assertFrameObject( frame_f73f0991cf3aa1993b37c583fcf51a1c );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_5_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_mod );
    par_mod = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_mod );
    par_mod = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_5_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_6_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_mod = python_pars[ 0 ];
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    PyObject *tmp_source_name_2;
    static struct Nuitka_FrameObject *cache_frame_5a4ac99e386874c3c884ed40f9dbb322 = NULL;

    struct Nuitka_FrameObject *frame_5a4ac99e386874c3c884ed40f9dbb322;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_5a4ac99e386874c3c884ed40f9dbb322, codeobj_5a4ac99e386874c3c884ed40f9dbb322, module_pandas$util$_print_versions, sizeof(void *) );
    frame_5a4ac99e386874c3c884ed40f9dbb322 = cache_frame_5a4ac99e386874c3c884ed40f9dbb322;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_5a4ac99e386874c3c884ed40f9dbb322 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_5a4ac99e386874c3c884ed40f9dbb322 ) == 2 ); // Frame stack

    // Framed code:
    tmp_source_name_2 = par_mod;

    CHECK_OBJECT( tmp_source_name_2 );
    tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_version );
    if ( tmp_source_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 69;
        type_description_1 = "o";
        goto frame_exception_exit_1;
    }
    tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_version );
    Py_DECREF( tmp_source_name_1 );
    if ( tmp_return_value == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 69;
        type_description_1 = "o";
        goto frame_exception_exit_1;
    }
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5a4ac99e386874c3c884ed40f9dbb322 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_5a4ac99e386874c3c884ed40f9dbb322 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5a4ac99e386874c3c884ed40f9dbb322 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_5a4ac99e386874c3c884ed40f9dbb322, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_5a4ac99e386874c3c884ed40f9dbb322->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_5a4ac99e386874c3c884ed40f9dbb322, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_5a4ac99e386874c3c884ed40f9dbb322,
        type_description_1,
        par_mod
    );


    // Release cached frame.
    if ( frame_5a4ac99e386874c3c884ed40f9dbb322 == cache_frame_5a4ac99e386874c3c884ed40f9dbb322 )
    {
        Py_DECREF( frame_5a4ac99e386874c3c884ed40f9dbb322 );
    }
    cache_frame_5a4ac99e386874c3c884ed40f9dbb322 = NULL;

    assertFrameObject( frame_5a4ac99e386874c3c884ed40f9dbb322 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_6_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_mod );
    par_mod = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_mod );
    par_mod = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_6_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_7_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_mod = python_pars[ 0 ];
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    PyObject *tmp_source_name_2;
    static struct Nuitka_FrameObject *cache_frame_2ec3fcea70543e510ce3df0dbd94aa4b = NULL;

    struct Nuitka_FrameObject *frame_2ec3fcea70543e510ce3df0dbd94aa4b;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_2ec3fcea70543e510ce3df0dbd94aa4b, codeobj_2ec3fcea70543e510ce3df0dbd94aa4b, module_pandas$util$_print_versions, sizeof(void *) );
    frame_2ec3fcea70543e510ce3df0dbd94aa4b = cache_frame_2ec3fcea70543e510ce3df0dbd94aa4b;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_2ec3fcea70543e510ce3df0dbd94aa4b );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_2ec3fcea70543e510ce3df0dbd94aa4b ) == 2 ); // Frame stack

    // Framed code:
    tmp_source_name_2 = par_mod;

    CHECK_OBJECT( tmp_source_name_2 );
    tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_version );
    if ( tmp_source_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 70;
        type_description_1 = "o";
        goto frame_exception_exit_1;
    }
    tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_version );
    Py_DECREF( tmp_source_name_1 );
    if ( tmp_return_value == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 70;
        type_description_1 = "o";
        goto frame_exception_exit_1;
    }
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2ec3fcea70543e510ce3df0dbd94aa4b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_2ec3fcea70543e510ce3df0dbd94aa4b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2ec3fcea70543e510ce3df0dbd94aa4b );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_2ec3fcea70543e510ce3df0dbd94aa4b, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_2ec3fcea70543e510ce3df0dbd94aa4b->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_2ec3fcea70543e510ce3df0dbd94aa4b, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_2ec3fcea70543e510ce3df0dbd94aa4b,
        type_description_1,
        par_mod
    );


    // Release cached frame.
    if ( frame_2ec3fcea70543e510ce3df0dbd94aa4b == cache_frame_2ec3fcea70543e510ce3df0dbd94aa4b )
    {
        Py_DECREF( frame_2ec3fcea70543e510ce3df0dbd94aa4b );
    }
    cache_frame_2ec3fcea70543e510ce3df0dbd94aa4b = NULL;

    assertFrameObject( frame_2ec3fcea70543e510ce3df0dbd94aa4b );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_7_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_mod );
    par_mod = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_mod );
    par_mod = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_7_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_8_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_mod = python_pars[ 0 ];
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    static struct Nuitka_FrameObject *cache_frame_38f7b17fed2ed0edc796d820d8b473df = NULL;

    struct Nuitka_FrameObject *frame_38f7b17fed2ed0edc796d820d8b473df;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_38f7b17fed2ed0edc796d820d8b473df, codeobj_38f7b17fed2ed0edc796d820d8b473df, module_pandas$util$_print_versions, sizeof(void *) );
    frame_38f7b17fed2ed0edc796d820d8b473df = cache_frame_38f7b17fed2ed0edc796d820d8b473df;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_38f7b17fed2ed0edc796d820d8b473df );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_38f7b17fed2ed0edc796d820d8b473df ) == 2 ); // Frame stack

    // Framed code:
    tmp_source_name_1 = par_mod;

    CHECK_OBJECT( tmp_source_name_1 );
    tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___version__ );
    if ( tmp_return_value == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 71;
        type_description_1 = "o";
        goto frame_exception_exit_1;
    }
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_38f7b17fed2ed0edc796d820d8b473df );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_38f7b17fed2ed0edc796d820d8b473df );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_38f7b17fed2ed0edc796d820d8b473df );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_38f7b17fed2ed0edc796d820d8b473df, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_38f7b17fed2ed0edc796d820d8b473df->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_38f7b17fed2ed0edc796d820d8b473df, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_38f7b17fed2ed0edc796d820d8b473df,
        type_description_1,
        par_mod
    );


    // Release cached frame.
    if ( frame_38f7b17fed2ed0edc796d820d8b473df == cache_frame_38f7b17fed2ed0edc796d820d8b473df )
    {
        Py_DECREF( frame_38f7b17fed2ed0edc796d820d8b473df );
    }
    cache_frame_38f7b17fed2ed0edc796d820d8b473df = NULL;

    assertFrameObject( frame_38f7b17fed2ed0edc796d820d8b473df );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_8_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_mod );
    par_mod = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_mod );
    par_mod = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_8_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_9_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_mod = python_pars[ 0 ];
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    static struct Nuitka_FrameObject *cache_frame_f40a2453591b4baa581365fc75869546 = NULL;

    struct Nuitka_FrameObject *frame_f40a2453591b4baa581365fc75869546;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_f40a2453591b4baa581365fc75869546, codeobj_f40a2453591b4baa581365fc75869546, module_pandas$util$_print_versions, sizeof(void *) );
    frame_f40a2453591b4baa581365fc75869546 = cache_frame_f40a2453591b4baa581365fc75869546;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_f40a2453591b4baa581365fc75869546 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_f40a2453591b4baa581365fc75869546 ) == 2 ); // Frame stack

    // Framed code:
    tmp_source_name_1 = par_mod;

    CHECK_OBJECT( tmp_source_name_1 );
    tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___version__ );
    if ( tmp_return_value == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 72;
        type_description_1 = "o";
        goto frame_exception_exit_1;
    }
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f40a2453591b4baa581365fc75869546 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_f40a2453591b4baa581365fc75869546 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f40a2453591b4baa581365fc75869546 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_f40a2453591b4baa581365fc75869546, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_f40a2453591b4baa581365fc75869546->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_f40a2453591b4baa581365fc75869546, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_f40a2453591b4baa581365fc75869546,
        type_description_1,
        par_mod
    );


    // Release cached frame.
    if ( frame_f40a2453591b4baa581365fc75869546 == cache_frame_f40a2453591b4baa581365fc75869546 )
    {
        Py_DECREF( frame_f40a2453591b4baa581365fc75869546 );
    }
    cache_frame_f40a2453591b4baa581365fc75869546 = NULL;

    assertFrameObject( frame_f40a2453591b4baa581365fc75869546 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_9_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_mod );
    par_mod = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_mod );
    par_mod = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_9_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_10_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_mod = python_pars[ 0 ];
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    static struct Nuitka_FrameObject *cache_frame_072403d28f5741828c95c4e73365d503 = NULL;

    struct Nuitka_FrameObject *frame_072403d28f5741828c95c4e73365d503;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_072403d28f5741828c95c4e73365d503, codeobj_072403d28f5741828c95c4e73365d503, module_pandas$util$_print_versions, sizeof(void *) );
    frame_072403d28f5741828c95c4e73365d503 = cache_frame_072403d28f5741828c95c4e73365d503;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_072403d28f5741828c95c4e73365d503 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_072403d28f5741828c95c4e73365d503 ) == 2 ); // Frame stack

    // Framed code:
    tmp_source_name_1 = par_mod;

    CHECK_OBJECT( tmp_source_name_1 );
    tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___version__ );
    if ( tmp_return_value == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 73;
        type_description_1 = "o";
        goto frame_exception_exit_1;
    }
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_072403d28f5741828c95c4e73365d503 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_072403d28f5741828c95c4e73365d503 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_072403d28f5741828c95c4e73365d503 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_072403d28f5741828c95c4e73365d503, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_072403d28f5741828c95c4e73365d503->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_072403d28f5741828c95c4e73365d503, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_072403d28f5741828c95c4e73365d503,
        type_description_1,
        par_mod
    );


    // Release cached frame.
    if ( frame_072403d28f5741828c95c4e73365d503 == cache_frame_072403d28f5741828c95c4e73365d503 )
    {
        Py_DECREF( frame_072403d28f5741828c95c4e73365d503 );
    }
    cache_frame_072403d28f5741828c95c4e73365d503 = NULL;

    assertFrameObject( frame_072403d28f5741828c95c4e73365d503 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_10_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_mod );
    par_mod = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_mod );
    par_mod = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_10_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_11_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_mod = python_pars[ 0 ];
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    static struct Nuitka_FrameObject *cache_frame_14a520a50b3a023d1743172655f97257 = NULL;

    struct Nuitka_FrameObject *frame_14a520a50b3a023d1743172655f97257;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_14a520a50b3a023d1743172655f97257, codeobj_14a520a50b3a023d1743172655f97257, module_pandas$util$_print_versions, sizeof(void *) );
    frame_14a520a50b3a023d1743172655f97257 = cache_frame_14a520a50b3a023d1743172655f97257;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_14a520a50b3a023d1743172655f97257 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_14a520a50b3a023d1743172655f97257 ) == 2 ); // Frame stack

    // Framed code:
    tmp_source_name_1 = par_mod;

    CHECK_OBJECT( tmp_source_name_1 );
    tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___version__ );
    if ( tmp_return_value == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 74;
        type_description_1 = "o";
        goto frame_exception_exit_1;
    }
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_14a520a50b3a023d1743172655f97257 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_14a520a50b3a023d1743172655f97257 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_14a520a50b3a023d1743172655f97257 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_14a520a50b3a023d1743172655f97257, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_14a520a50b3a023d1743172655f97257->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_14a520a50b3a023d1743172655f97257, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_14a520a50b3a023d1743172655f97257,
        type_description_1,
        par_mod
    );


    // Release cached frame.
    if ( frame_14a520a50b3a023d1743172655f97257 == cache_frame_14a520a50b3a023d1743172655f97257 )
    {
        Py_DECREF( frame_14a520a50b3a023d1743172655f97257 );
    }
    cache_frame_14a520a50b3a023d1743172655f97257 = NULL;

    assertFrameObject( frame_14a520a50b3a023d1743172655f97257 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_11_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_mod );
    par_mod = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_mod );
    par_mod = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_11_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_12_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_mod = python_pars[ 0 ];
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    static struct Nuitka_FrameObject *cache_frame_1e8d1315dafed74c06af0f9717e1ea23 = NULL;

    struct Nuitka_FrameObject *frame_1e8d1315dafed74c06af0f9717e1ea23;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_1e8d1315dafed74c06af0f9717e1ea23, codeobj_1e8d1315dafed74c06af0f9717e1ea23, module_pandas$util$_print_versions, sizeof(void *) );
    frame_1e8d1315dafed74c06af0f9717e1ea23 = cache_frame_1e8d1315dafed74c06af0f9717e1ea23;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_1e8d1315dafed74c06af0f9717e1ea23 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_1e8d1315dafed74c06af0f9717e1ea23 ) == 2 ); // Frame stack

    // Framed code:
    tmp_source_name_1 = par_mod;

    CHECK_OBJECT( tmp_source_name_1 );
    tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___version__ );
    if ( tmp_return_value == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 75;
        type_description_1 = "o";
        goto frame_exception_exit_1;
    }
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1e8d1315dafed74c06af0f9717e1ea23 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_1e8d1315dafed74c06af0f9717e1ea23 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1e8d1315dafed74c06af0f9717e1ea23 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_1e8d1315dafed74c06af0f9717e1ea23, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_1e8d1315dafed74c06af0f9717e1ea23->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_1e8d1315dafed74c06af0f9717e1ea23, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_1e8d1315dafed74c06af0f9717e1ea23,
        type_description_1,
        par_mod
    );


    // Release cached frame.
    if ( frame_1e8d1315dafed74c06af0f9717e1ea23 == cache_frame_1e8d1315dafed74c06af0f9717e1ea23 )
    {
        Py_DECREF( frame_1e8d1315dafed74c06af0f9717e1ea23 );
    }
    cache_frame_1e8d1315dafed74c06af0f9717e1ea23 = NULL;

    assertFrameObject( frame_1e8d1315dafed74c06af0f9717e1ea23 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_12_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_mod );
    par_mod = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_mod );
    par_mod = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_12_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_13_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_mod = python_pars[ 0 ];
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    static struct Nuitka_FrameObject *cache_frame_e41547ccdc4c4e73dc064032cd7327eb = NULL;

    struct Nuitka_FrameObject *frame_e41547ccdc4c4e73dc064032cd7327eb;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_e41547ccdc4c4e73dc064032cd7327eb, codeobj_e41547ccdc4c4e73dc064032cd7327eb, module_pandas$util$_print_versions, sizeof(void *) );
    frame_e41547ccdc4c4e73dc064032cd7327eb = cache_frame_e41547ccdc4c4e73dc064032cd7327eb;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_e41547ccdc4c4e73dc064032cd7327eb );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_e41547ccdc4c4e73dc064032cd7327eb ) == 2 ); // Frame stack

    // Framed code:
    tmp_source_name_1 = par_mod;

    CHECK_OBJECT( tmp_source_name_1 );
    tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___version__ );
    if ( tmp_return_value == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 76;
        type_description_1 = "o";
        goto frame_exception_exit_1;
    }
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e41547ccdc4c4e73dc064032cd7327eb );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_e41547ccdc4c4e73dc064032cd7327eb );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e41547ccdc4c4e73dc064032cd7327eb );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_e41547ccdc4c4e73dc064032cd7327eb, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_e41547ccdc4c4e73dc064032cd7327eb->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_e41547ccdc4c4e73dc064032cd7327eb, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_e41547ccdc4c4e73dc064032cd7327eb,
        type_description_1,
        par_mod
    );


    // Release cached frame.
    if ( frame_e41547ccdc4c4e73dc064032cd7327eb == cache_frame_e41547ccdc4c4e73dc064032cd7327eb )
    {
        Py_DECREF( frame_e41547ccdc4c4e73dc064032cd7327eb );
    }
    cache_frame_e41547ccdc4c4e73dc064032cd7327eb = NULL;

    assertFrameObject( frame_e41547ccdc4c4e73dc064032cd7327eb );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_13_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_mod );
    par_mod = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_mod );
    par_mod = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_13_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_14_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_mod = python_pars[ 0 ];
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    static struct Nuitka_FrameObject *cache_frame_8ef132c20da65b7d3460e4a332b6f9be = NULL;

    struct Nuitka_FrameObject *frame_8ef132c20da65b7d3460e4a332b6f9be;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_8ef132c20da65b7d3460e4a332b6f9be, codeobj_8ef132c20da65b7d3460e4a332b6f9be, module_pandas$util$_print_versions, sizeof(void *) );
    frame_8ef132c20da65b7d3460e4a332b6f9be = cache_frame_8ef132c20da65b7d3460e4a332b6f9be;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_8ef132c20da65b7d3460e4a332b6f9be );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_8ef132c20da65b7d3460e4a332b6f9be ) == 2 ); // Frame stack

    // Framed code:
    tmp_source_name_1 = par_mod;

    CHECK_OBJECT( tmp_source_name_1 );
    tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_VERSION );
    if ( tmp_return_value == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 77;
        type_description_1 = "o";
        goto frame_exception_exit_1;
    }
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8ef132c20da65b7d3460e4a332b6f9be );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_8ef132c20da65b7d3460e4a332b6f9be );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8ef132c20da65b7d3460e4a332b6f9be );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_8ef132c20da65b7d3460e4a332b6f9be, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_8ef132c20da65b7d3460e4a332b6f9be->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_8ef132c20da65b7d3460e4a332b6f9be, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_8ef132c20da65b7d3460e4a332b6f9be,
        type_description_1,
        par_mod
    );


    // Release cached frame.
    if ( frame_8ef132c20da65b7d3460e4a332b6f9be == cache_frame_8ef132c20da65b7d3460e4a332b6f9be )
    {
        Py_DECREF( frame_8ef132c20da65b7d3460e4a332b6f9be );
    }
    cache_frame_8ef132c20da65b7d3460e4a332b6f9be = NULL;

    assertFrameObject( frame_8ef132c20da65b7d3460e4a332b6f9be );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_14_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_mod );
    par_mod = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_mod );
    par_mod = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_14_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_15_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_mod = python_pars[ 0 ];
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    static struct Nuitka_FrameObject *cache_frame_73c6b457b34bdd71645a37575f39ac4c = NULL;

    struct Nuitka_FrameObject *frame_73c6b457b34bdd71645a37575f39ac4c;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_73c6b457b34bdd71645a37575f39ac4c, codeobj_73c6b457b34bdd71645a37575f39ac4c, module_pandas$util$_print_versions, sizeof(void *) );
    frame_73c6b457b34bdd71645a37575f39ac4c = cache_frame_73c6b457b34bdd71645a37575f39ac4c;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_73c6b457b34bdd71645a37575f39ac4c );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_73c6b457b34bdd71645a37575f39ac4c ) == 2 ); // Frame stack

    // Framed code:
    tmp_source_name_1 = par_mod;

    CHECK_OBJECT( tmp_source_name_1 );
    tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___version__ );
    if ( tmp_return_value == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 78;
        type_description_1 = "o";
        goto frame_exception_exit_1;
    }
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_73c6b457b34bdd71645a37575f39ac4c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_73c6b457b34bdd71645a37575f39ac4c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_73c6b457b34bdd71645a37575f39ac4c );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_73c6b457b34bdd71645a37575f39ac4c, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_73c6b457b34bdd71645a37575f39ac4c->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_73c6b457b34bdd71645a37575f39ac4c, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_73c6b457b34bdd71645a37575f39ac4c,
        type_description_1,
        par_mod
    );


    // Release cached frame.
    if ( frame_73c6b457b34bdd71645a37575f39ac4c == cache_frame_73c6b457b34bdd71645a37575f39ac4c )
    {
        Py_DECREF( frame_73c6b457b34bdd71645a37575f39ac4c );
    }
    cache_frame_73c6b457b34bdd71645a37575f39ac4c = NULL;

    assertFrameObject( frame_73c6b457b34bdd71645a37575f39ac4c );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_15_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_mod );
    par_mod = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_mod );
    par_mod = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_15_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_16_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_mod = python_pars[ 0 ];
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    static struct Nuitka_FrameObject *cache_frame_c7dc781ac04c588d3526dde79b9b6ce7 = NULL;

    struct Nuitka_FrameObject *frame_c7dc781ac04c588d3526dde79b9b6ce7;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_c7dc781ac04c588d3526dde79b9b6ce7, codeobj_c7dc781ac04c588d3526dde79b9b6ce7, module_pandas$util$_print_versions, sizeof(void *) );
    frame_c7dc781ac04c588d3526dde79b9b6ce7 = cache_frame_c7dc781ac04c588d3526dde79b9b6ce7;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_c7dc781ac04c588d3526dde79b9b6ce7 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_c7dc781ac04c588d3526dde79b9b6ce7 ) == 2 ); // Frame stack

    // Framed code:
    tmp_source_name_1 = par_mod;

    CHECK_OBJECT( tmp_source_name_1 );
    tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___version__ );
    if ( tmp_return_value == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 79;
        type_description_1 = "o";
        goto frame_exception_exit_1;
    }
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c7dc781ac04c588d3526dde79b9b6ce7 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_c7dc781ac04c588d3526dde79b9b6ce7 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c7dc781ac04c588d3526dde79b9b6ce7 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_c7dc781ac04c588d3526dde79b9b6ce7, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_c7dc781ac04c588d3526dde79b9b6ce7->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_c7dc781ac04c588d3526dde79b9b6ce7, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_c7dc781ac04c588d3526dde79b9b6ce7,
        type_description_1,
        par_mod
    );


    // Release cached frame.
    if ( frame_c7dc781ac04c588d3526dde79b9b6ce7 == cache_frame_c7dc781ac04c588d3526dde79b9b6ce7 )
    {
        Py_DECREF( frame_c7dc781ac04c588d3526dde79b9b6ce7 );
    }
    cache_frame_c7dc781ac04c588d3526dde79b9b6ce7 = NULL;

    assertFrameObject( frame_c7dc781ac04c588d3526dde79b9b6ce7 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_16_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_mod );
    par_mod = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_mod );
    par_mod = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_16_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_17_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_mod = python_pars[ 0 ];
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    static struct Nuitka_FrameObject *cache_frame_7af43919b9767ca1039c00d325ec4c76 = NULL;

    struct Nuitka_FrameObject *frame_7af43919b9767ca1039c00d325ec4c76;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_7af43919b9767ca1039c00d325ec4c76, codeobj_7af43919b9767ca1039c00d325ec4c76, module_pandas$util$_print_versions, sizeof(void *) );
    frame_7af43919b9767ca1039c00d325ec4c76 = cache_frame_7af43919b9767ca1039c00d325ec4c76;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_7af43919b9767ca1039c00d325ec4c76 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_7af43919b9767ca1039c00d325ec4c76 ) == 2 ); // Frame stack

    // Framed code:
    tmp_source_name_1 = par_mod;

    CHECK_OBJECT( tmp_source_name_1 );
    tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___version__ );
    if ( tmp_return_value == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 80;
        type_description_1 = "o";
        goto frame_exception_exit_1;
    }
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7af43919b9767ca1039c00d325ec4c76 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_7af43919b9767ca1039c00d325ec4c76 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7af43919b9767ca1039c00d325ec4c76 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_7af43919b9767ca1039c00d325ec4c76, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_7af43919b9767ca1039c00d325ec4c76->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_7af43919b9767ca1039c00d325ec4c76, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_7af43919b9767ca1039c00d325ec4c76,
        type_description_1,
        par_mod
    );


    // Release cached frame.
    if ( frame_7af43919b9767ca1039c00d325ec4c76 == cache_frame_7af43919b9767ca1039c00d325ec4c76 )
    {
        Py_DECREF( frame_7af43919b9767ca1039c00d325ec4c76 );
    }
    cache_frame_7af43919b9767ca1039c00d325ec4c76 = NULL;

    assertFrameObject( frame_7af43919b9767ca1039c00d325ec4c76 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_17_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_mod );
    par_mod = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_mod );
    par_mod = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_17_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_18_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_mod = python_pars[ 0 ];
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    static struct Nuitka_FrameObject *cache_frame_eca96f70c2e5af9d01c7ee02fd375cfc = NULL;

    struct Nuitka_FrameObject *frame_eca96f70c2e5af9d01c7ee02fd375cfc;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_eca96f70c2e5af9d01c7ee02fd375cfc, codeobj_eca96f70c2e5af9d01c7ee02fd375cfc, module_pandas$util$_print_versions, sizeof(void *) );
    frame_eca96f70c2e5af9d01c7ee02fd375cfc = cache_frame_eca96f70c2e5af9d01c7ee02fd375cfc;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_eca96f70c2e5af9d01c7ee02fd375cfc );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_eca96f70c2e5af9d01c7ee02fd375cfc ) == 2 ); // Frame stack

    // Framed code:
    tmp_source_name_1 = par_mod;

    CHECK_OBJECT( tmp_source_name_1 );
    tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___version__ );
    if ( tmp_return_value == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 81;
        type_description_1 = "o";
        goto frame_exception_exit_1;
    }
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_eca96f70c2e5af9d01c7ee02fd375cfc );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_eca96f70c2e5af9d01c7ee02fd375cfc );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_eca96f70c2e5af9d01c7ee02fd375cfc );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_eca96f70c2e5af9d01c7ee02fd375cfc, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_eca96f70c2e5af9d01c7ee02fd375cfc->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_eca96f70c2e5af9d01c7ee02fd375cfc, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_eca96f70c2e5af9d01c7ee02fd375cfc,
        type_description_1,
        par_mod
    );


    // Release cached frame.
    if ( frame_eca96f70c2e5af9d01c7ee02fd375cfc == cache_frame_eca96f70c2e5af9d01c7ee02fd375cfc )
    {
        Py_DECREF( frame_eca96f70c2e5af9d01c7ee02fd375cfc );
    }
    cache_frame_eca96f70c2e5af9d01c7ee02fd375cfc = NULL;

    assertFrameObject( frame_eca96f70c2e5af9d01c7ee02fd375cfc );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_18_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_mod );
    par_mod = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_mod );
    par_mod = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_18_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_19_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_mod = python_pars[ 0 ];
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    static struct Nuitka_FrameObject *cache_frame_6cce22e69d0dff310e212335427e63ec = NULL;

    struct Nuitka_FrameObject *frame_6cce22e69d0dff310e212335427e63ec;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_6cce22e69d0dff310e212335427e63ec, codeobj_6cce22e69d0dff310e212335427e63ec, module_pandas$util$_print_versions, sizeof(void *) );
    frame_6cce22e69d0dff310e212335427e63ec = cache_frame_6cce22e69d0dff310e212335427e63ec;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_6cce22e69d0dff310e212335427e63ec );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_6cce22e69d0dff310e212335427e63ec ) == 2 ); // Frame stack

    // Framed code:
    tmp_source_name_1 = par_mod;

    CHECK_OBJECT( tmp_source_name_1 );
    tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___version__ );
    if ( tmp_return_value == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 82;
        type_description_1 = "o";
        goto frame_exception_exit_1;
    }
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6cce22e69d0dff310e212335427e63ec );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_6cce22e69d0dff310e212335427e63ec );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6cce22e69d0dff310e212335427e63ec );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_6cce22e69d0dff310e212335427e63ec, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_6cce22e69d0dff310e212335427e63ec->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_6cce22e69d0dff310e212335427e63ec, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_6cce22e69d0dff310e212335427e63ec,
        type_description_1,
        par_mod
    );


    // Release cached frame.
    if ( frame_6cce22e69d0dff310e212335427e63ec == cache_frame_6cce22e69d0dff310e212335427e63ec )
    {
        Py_DECREF( frame_6cce22e69d0dff310e212335427e63ec );
    }
    cache_frame_6cce22e69d0dff310e212335427e63ec = NULL;

    assertFrameObject( frame_6cce22e69d0dff310e212335427e63ec );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_19_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_mod );
    par_mod = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_mod );
    par_mod = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_19_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_20_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_mod = python_pars[ 0 ];
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    static struct Nuitka_FrameObject *cache_frame_c88733dda5f5eb0d43a9100a815f3351 = NULL;

    struct Nuitka_FrameObject *frame_c88733dda5f5eb0d43a9100a815f3351;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_c88733dda5f5eb0d43a9100a815f3351, codeobj_c88733dda5f5eb0d43a9100a815f3351, module_pandas$util$_print_versions, sizeof(void *) );
    frame_c88733dda5f5eb0d43a9100a815f3351 = cache_frame_c88733dda5f5eb0d43a9100a815f3351;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_c88733dda5f5eb0d43a9100a815f3351 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_c88733dda5f5eb0d43a9100a815f3351 ) == 2 ); // Frame stack

    // Framed code:
    tmp_source_name_1 = par_mod;

    CHECK_OBJECT( tmp_source_name_1 );
    tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___version__ );
    if ( tmp_return_value == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 83;
        type_description_1 = "o";
        goto frame_exception_exit_1;
    }
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c88733dda5f5eb0d43a9100a815f3351 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_c88733dda5f5eb0d43a9100a815f3351 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c88733dda5f5eb0d43a9100a815f3351 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_c88733dda5f5eb0d43a9100a815f3351, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_c88733dda5f5eb0d43a9100a815f3351->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_c88733dda5f5eb0d43a9100a815f3351, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_c88733dda5f5eb0d43a9100a815f3351,
        type_description_1,
        par_mod
    );


    // Release cached frame.
    if ( frame_c88733dda5f5eb0d43a9100a815f3351 == cache_frame_c88733dda5f5eb0d43a9100a815f3351 )
    {
        Py_DECREF( frame_c88733dda5f5eb0d43a9100a815f3351 );
    }
    cache_frame_c88733dda5f5eb0d43a9100a815f3351 = NULL;

    assertFrameObject( frame_c88733dda5f5eb0d43a9100a815f3351 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_20_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_mod );
    par_mod = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_mod );
    par_mod = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_20_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_21_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_mod = python_pars[ 0 ];
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    static struct Nuitka_FrameObject *cache_frame_06e645d0c17d11ba46064414e495bd0f = NULL;

    struct Nuitka_FrameObject *frame_06e645d0c17d11ba46064414e495bd0f;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_06e645d0c17d11ba46064414e495bd0f, codeobj_06e645d0c17d11ba46064414e495bd0f, module_pandas$util$_print_versions, sizeof(void *) );
    frame_06e645d0c17d11ba46064414e495bd0f = cache_frame_06e645d0c17d11ba46064414e495bd0f;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_06e645d0c17d11ba46064414e495bd0f );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_06e645d0c17d11ba46064414e495bd0f ) == 2 ); // Frame stack

    // Framed code:
    tmp_source_name_1 = par_mod;

    CHECK_OBJECT( tmp_source_name_1 );
    tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___version__ );
    if ( tmp_return_value == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 84;
        type_description_1 = "o";
        goto frame_exception_exit_1;
    }
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_06e645d0c17d11ba46064414e495bd0f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_06e645d0c17d11ba46064414e495bd0f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_06e645d0c17d11ba46064414e495bd0f );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_06e645d0c17d11ba46064414e495bd0f, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_06e645d0c17d11ba46064414e495bd0f->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_06e645d0c17d11ba46064414e495bd0f, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_06e645d0c17d11ba46064414e495bd0f,
        type_description_1,
        par_mod
    );


    // Release cached frame.
    if ( frame_06e645d0c17d11ba46064414e495bd0f == cache_frame_06e645d0c17d11ba46064414e495bd0f )
    {
        Py_DECREF( frame_06e645d0c17d11ba46064414e495bd0f );
    }
    cache_frame_06e645d0c17d11ba46064414e495bd0f = NULL;

    assertFrameObject( frame_06e645d0c17d11ba46064414e495bd0f );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_21_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_mod );
    par_mod = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_mod );
    par_mod = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_21_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_22_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_mod = python_pars[ 0 ];
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    static struct Nuitka_FrameObject *cache_frame_1cf2bb340d87ac08e24b5cc60455989a = NULL;

    struct Nuitka_FrameObject *frame_1cf2bb340d87ac08e24b5cc60455989a;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_1cf2bb340d87ac08e24b5cc60455989a, codeobj_1cf2bb340d87ac08e24b5cc60455989a, module_pandas$util$_print_versions, sizeof(void *) );
    frame_1cf2bb340d87ac08e24b5cc60455989a = cache_frame_1cf2bb340d87ac08e24b5cc60455989a;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_1cf2bb340d87ac08e24b5cc60455989a );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_1cf2bb340d87ac08e24b5cc60455989a ) == 2 ); // Frame stack

    // Framed code:
    tmp_source_name_1 = par_mod;

    CHECK_OBJECT( tmp_source_name_1 );
    tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___VERSION__ );
    if ( tmp_return_value == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 85;
        type_description_1 = "o";
        goto frame_exception_exit_1;
    }
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1cf2bb340d87ac08e24b5cc60455989a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_1cf2bb340d87ac08e24b5cc60455989a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1cf2bb340d87ac08e24b5cc60455989a );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_1cf2bb340d87ac08e24b5cc60455989a, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_1cf2bb340d87ac08e24b5cc60455989a->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_1cf2bb340d87ac08e24b5cc60455989a, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_1cf2bb340d87ac08e24b5cc60455989a,
        type_description_1,
        par_mod
    );


    // Release cached frame.
    if ( frame_1cf2bb340d87ac08e24b5cc60455989a == cache_frame_1cf2bb340d87ac08e24b5cc60455989a )
    {
        Py_DECREF( frame_1cf2bb340d87ac08e24b5cc60455989a );
    }
    cache_frame_1cf2bb340d87ac08e24b5cc60455989a = NULL;

    assertFrameObject( frame_1cf2bb340d87ac08e24b5cc60455989a );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_22_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_mod );
    par_mod = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_mod );
    par_mod = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_22_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_23_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_mod = python_pars[ 0 ];
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    static struct Nuitka_FrameObject *cache_frame_1bb8923b3f31e892b461b41546196ed0 = NULL;

    struct Nuitka_FrameObject *frame_1bb8923b3f31e892b461b41546196ed0;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_1bb8923b3f31e892b461b41546196ed0, codeobj_1bb8923b3f31e892b461b41546196ed0, module_pandas$util$_print_versions, sizeof(void *) );
    frame_1bb8923b3f31e892b461b41546196ed0 = cache_frame_1bb8923b3f31e892b461b41546196ed0;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_1bb8923b3f31e892b461b41546196ed0 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_1bb8923b3f31e892b461b41546196ed0 ) == 2 ); // Frame stack

    // Framed code:
    tmp_source_name_1 = par_mod;

    CHECK_OBJECT( tmp_source_name_1 );
    tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___VERSION__ );
    if ( tmp_return_value == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 86;
        type_description_1 = "o";
        goto frame_exception_exit_1;
    }
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1bb8923b3f31e892b461b41546196ed0 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_1bb8923b3f31e892b461b41546196ed0 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1bb8923b3f31e892b461b41546196ed0 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_1bb8923b3f31e892b461b41546196ed0, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_1bb8923b3f31e892b461b41546196ed0->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_1bb8923b3f31e892b461b41546196ed0, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_1bb8923b3f31e892b461b41546196ed0,
        type_description_1,
        par_mod
    );


    // Release cached frame.
    if ( frame_1bb8923b3f31e892b461b41546196ed0 == cache_frame_1bb8923b3f31e892b461b41546196ed0 )
    {
        Py_DECREF( frame_1bb8923b3f31e892b461b41546196ed0 );
    }
    cache_frame_1bb8923b3f31e892b461b41546196ed0 = NULL;

    assertFrameObject( frame_1bb8923b3f31e892b461b41546196ed0 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_23_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_mod );
    par_mod = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_mod );
    par_mod = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_23_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_24_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_mod = python_pars[ 0 ];
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    static struct Nuitka_FrameObject *cache_frame_799cfdc0da788d48e7b5639fe577b6d6 = NULL;

    struct Nuitka_FrameObject *frame_799cfdc0da788d48e7b5639fe577b6d6;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_799cfdc0da788d48e7b5639fe577b6d6, codeobj_799cfdc0da788d48e7b5639fe577b6d6, module_pandas$util$_print_versions, sizeof(void *) );
    frame_799cfdc0da788d48e7b5639fe577b6d6 = cache_frame_799cfdc0da788d48e7b5639fe577b6d6;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_799cfdc0da788d48e7b5639fe577b6d6 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_799cfdc0da788d48e7b5639fe577b6d6 ) == 2 ); // Frame stack

    // Framed code:
    tmp_source_name_1 = par_mod;

    CHECK_OBJECT( tmp_source_name_1 );
    tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___version__ );
    if ( tmp_return_value == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 87;
        type_description_1 = "o";
        goto frame_exception_exit_1;
    }
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_799cfdc0da788d48e7b5639fe577b6d6 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_799cfdc0da788d48e7b5639fe577b6d6 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_799cfdc0da788d48e7b5639fe577b6d6 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_799cfdc0da788d48e7b5639fe577b6d6, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_799cfdc0da788d48e7b5639fe577b6d6->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_799cfdc0da788d48e7b5639fe577b6d6, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_799cfdc0da788d48e7b5639fe577b6d6,
        type_description_1,
        par_mod
    );


    // Release cached frame.
    if ( frame_799cfdc0da788d48e7b5639fe577b6d6 == cache_frame_799cfdc0da788d48e7b5639fe577b6d6 )
    {
        Py_DECREF( frame_799cfdc0da788d48e7b5639fe577b6d6 );
    }
    cache_frame_799cfdc0da788d48e7b5639fe577b6d6 = NULL;

    assertFrameObject( frame_799cfdc0da788d48e7b5639fe577b6d6 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_24_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_mod );
    par_mod = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_mod );
    par_mod = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_24_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_25_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_mod = python_pars[ 0 ];
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    PyObject *tmp_source_name_2;
    static struct Nuitka_FrameObject *cache_frame_51f02fd3ac7af88394eb95b568e206ab = NULL;

    struct Nuitka_FrameObject *frame_51f02fd3ac7af88394eb95b568e206ab;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_51f02fd3ac7af88394eb95b568e206ab, codeobj_51f02fd3ac7af88394eb95b568e206ab, module_pandas$util$_print_versions, sizeof(void *) );
    frame_51f02fd3ac7af88394eb95b568e206ab = cache_frame_51f02fd3ac7af88394eb95b568e206ab;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_51f02fd3ac7af88394eb95b568e206ab );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_51f02fd3ac7af88394eb95b568e206ab ) == 2 ); // Frame stack

    // Framed code:
    tmp_source_name_2 = par_mod;

    CHECK_OBJECT( tmp_source_name_2 );
    tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_etree );
    if ( tmp_source_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 88;
        type_description_1 = "o";
        goto frame_exception_exit_1;
    }
    tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___version__ );
    Py_DECREF( tmp_source_name_1 );
    if ( tmp_return_value == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 88;
        type_description_1 = "o";
        goto frame_exception_exit_1;
    }
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_51f02fd3ac7af88394eb95b568e206ab );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_51f02fd3ac7af88394eb95b568e206ab );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_51f02fd3ac7af88394eb95b568e206ab );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_51f02fd3ac7af88394eb95b568e206ab, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_51f02fd3ac7af88394eb95b568e206ab->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_51f02fd3ac7af88394eb95b568e206ab, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_51f02fd3ac7af88394eb95b568e206ab,
        type_description_1,
        par_mod
    );


    // Release cached frame.
    if ( frame_51f02fd3ac7af88394eb95b568e206ab == cache_frame_51f02fd3ac7af88394eb95b568e206ab )
    {
        Py_DECREF( frame_51f02fd3ac7af88394eb95b568e206ab );
    }
    cache_frame_51f02fd3ac7af88394eb95b568e206ab = NULL;

    assertFrameObject( frame_51f02fd3ac7af88394eb95b568e206ab );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_25_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_mod );
    par_mod = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_mod );
    par_mod = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_25_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_26_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_mod = python_pars[ 0 ];
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    static struct Nuitka_FrameObject *cache_frame_c27441d7de8a3ede56c605427a69e303 = NULL;

    struct Nuitka_FrameObject *frame_c27441d7de8a3ede56c605427a69e303;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_c27441d7de8a3ede56c605427a69e303, codeobj_c27441d7de8a3ede56c605427a69e303, module_pandas$util$_print_versions, sizeof(void *) );
    frame_c27441d7de8a3ede56c605427a69e303 = cache_frame_c27441d7de8a3ede56c605427a69e303;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_c27441d7de8a3ede56c605427a69e303 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_c27441d7de8a3ede56c605427a69e303 ) == 2 ); // Frame stack

    // Framed code:
    tmp_source_name_1 = par_mod;

    CHECK_OBJECT( tmp_source_name_1 );
    tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___version__ );
    if ( tmp_return_value == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 89;
        type_description_1 = "o";
        goto frame_exception_exit_1;
    }
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c27441d7de8a3ede56c605427a69e303 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_c27441d7de8a3ede56c605427a69e303 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c27441d7de8a3ede56c605427a69e303 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_c27441d7de8a3ede56c605427a69e303, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_c27441d7de8a3ede56c605427a69e303->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_c27441d7de8a3ede56c605427a69e303, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_c27441d7de8a3ede56c605427a69e303,
        type_description_1,
        par_mod
    );


    // Release cached frame.
    if ( frame_c27441d7de8a3ede56c605427a69e303 == cache_frame_c27441d7de8a3ede56c605427a69e303 )
    {
        Py_DECREF( frame_c27441d7de8a3ede56c605427a69e303 );
    }
    cache_frame_c27441d7de8a3ede56c605427a69e303 = NULL;

    assertFrameObject( frame_c27441d7de8a3ede56c605427a69e303 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_26_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_mod );
    par_mod = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_mod );
    par_mod = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_26_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_27_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_mod = python_pars[ 0 ];
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    static struct Nuitka_FrameObject *cache_frame_65ec58e74815899139a3f53dae1074bc = NULL;

    struct Nuitka_FrameObject *frame_65ec58e74815899139a3f53dae1074bc;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_65ec58e74815899139a3f53dae1074bc, codeobj_65ec58e74815899139a3f53dae1074bc, module_pandas$util$_print_versions, sizeof(void *) );
    frame_65ec58e74815899139a3f53dae1074bc = cache_frame_65ec58e74815899139a3f53dae1074bc;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_65ec58e74815899139a3f53dae1074bc );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_65ec58e74815899139a3f53dae1074bc ) == 2 ); // Frame stack

    // Framed code:
    tmp_source_name_1 = par_mod;

    CHECK_OBJECT( tmp_source_name_1 );
    tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___version__ );
    if ( tmp_return_value == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 90;
        type_description_1 = "o";
        goto frame_exception_exit_1;
    }
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_65ec58e74815899139a3f53dae1074bc );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_65ec58e74815899139a3f53dae1074bc );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_65ec58e74815899139a3f53dae1074bc );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_65ec58e74815899139a3f53dae1074bc, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_65ec58e74815899139a3f53dae1074bc->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_65ec58e74815899139a3f53dae1074bc, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_65ec58e74815899139a3f53dae1074bc,
        type_description_1,
        par_mod
    );


    // Release cached frame.
    if ( frame_65ec58e74815899139a3f53dae1074bc == cache_frame_65ec58e74815899139a3f53dae1074bc )
    {
        Py_DECREF( frame_65ec58e74815899139a3f53dae1074bc );
    }
    cache_frame_65ec58e74815899139a3f53dae1074bc = NULL;

    assertFrameObject( frame_65ec58e74815899139a3f53dae1074bc );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_27_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_mod );
    par_mod = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_mod );
    par_mod = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_27_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_28_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_mod = python_pars[ 0 ];
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    static struct Nuitka_FrameObject *cache_frame_9f69f0cf233ce6e9d9b4f05d43820748 = NULL;

    struct Nuitka_FrameObject *frame_9f69f0cf233ce6e9d9b4f05d43820748;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_9f69f0cf233ce6e9d9b4f05d43820748, codeobj_9f69f0cf233ce6e9d9b4f05d43820748, module_pandas$util$_print_versions, sizeof(void *) );
    frame_9f69f0cf233ce6e9d9b4f05d43820748 = cache_frame_9f69f0cf233ce6e9d9b4f05d43820748;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_9f69f0cf233ce6e9d9b4f05d43820748 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_9f69f0cf233ce6e9d9b4f05d43820748 ) == 2 ); // Frame stack

    // Framed code:
    tmp_source_name_1 = par_mod;

    CHECK_OBJECT( tmp_source_name_1 );
    tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___version__ );
    if ( tmp_return_value == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 91;
        type_description_1 = "o";
        goto frame_exception_exit_1;
    }
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9f69f0cf233ce6e9d9b4f05d43820748 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_9f69f0cf233ce6e9d9b4f05d43820748 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9f69f0cf233ce6e9d9b4f05d43820748 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_9f69f0cf233ce6e9d9b4f05d43820748, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_9f69f0cf233ce6e9d9b4f05d43820748->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_9f69f0cf233ce6e9d9b4f05d43820748, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_9f69f0cf233ce6e9d9b4f05d43820748,
        type_description_1,
        par_mod
    );


    // Release cached frame.
    if ( frame_9f69f0cf233ce6e9d9b4f05d43820748 == cache_frame_9f69f0cf233ce6e9d9b4f05d43820748 )
    {
        Py_DECREF( frame_9f69f0cf233ce6e9d9b4f05d43820748 );
    }
    cache_frame_9f69f0cf233ce6e9d9b4f05d43820748 = NULL;

    assertFrameObject( frame_9f69f0cf233ce6e9d9b4f05d43820748 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_28_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_mod );
    par_mod = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_mod );
    par_mod = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_28_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_29_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_mod = python_pars[ 0 ];
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    static struct Nuitka_FrameObject *cache_frame_02480085d27574d71b036399ed7da475 = NULL;

    struct Nuitka_FrameObject *frame_02480085d27574d71b036399ed7da475;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_02480085d27574d71b036399ed7da475, codeobj_02480085d27574d71b036399ed7da475, module_pandas$util$_print_versions, sizeof(void *) );
    frame_02480085d27574d71b036399ed7da475 = cache_frame_02480085d27574d71b036399ed7da475;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_02480085d27574d71b036399ed7da475 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_02480085d27574d71b036399ed7da475 ) == 2 ); // Frame stack

    // Framed code:
    tmp_source_name_1 = par_mod;

    CHECK_OBJECT( tmp_source_name_1 );
    tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___version__ );
    if ( tmp_return_value == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 92;
        type_description_1 = "o";
        goto frame_exception_exit_1;
    }
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_02480085d27574d71b036399ed7da475 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_02480085d27574d71b036399ed7da475 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_02480085d27574d71b036399ed7da475 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_02480085d27574d71b036399ed7da475, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_02480085d27574d71b036399ed7da475->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_02480085d27574d71b036399ed7da475, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_02480085d27574d71b036399ed7da475,
        type_description_1,
        par_mod
    );


    // Release cached frame.
    if ( frame_02480085d27574d71b036399ed7da475 == cache_frame_02480085d27574d71b036399ed7da475 )
    {
        Py_DECREF( frame_02480085d27574d71b036399ed7da475 );
    }
    cache_frame_02480085d27574d71b036399ed7da475 = NULL;

    assertFrameObject( frame_02480085d27574d71b036399ed7da475 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_29_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_mod );
    par_mod = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_mod );
    par_mod = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_29_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_30_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_mod = python_pars[ 0 ];
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    static struct Nuitka_FrameObject *cache_frame_7f234a464f34ccfabdf4b2ecfb5012f7 = NULL;

    struct Nuitka_FrameObject *frame_7f234a464f34ccfabdf4b2ecfb5012f7;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_7f234a464f34ccfabdf4b2ecfb5012f7, codeobj_7f234a464f34ccfabdf4b2ecfb5012f7, module_pandas$util$_print_versions, sizeof(void *) );
    frame_7f234a464f34ccfabdf4b2ecfb5012f7 = cache_frame_7f234a464f34ccfabdf4b2ecfb5012f7;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_7f234a464f34ccfabdf4b2ecfb5012f7 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_7f234a464f34ccfabdf4b2ecfb5012f7 ) == 2 ); // Frame stack

    // Framed code:
    tmp_source_name_1 = par_mod;

    CHECK_OBJECT( tmp_source_name_1 );
    tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___version__ );
    if ( tmp_return_value == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 93;
        type_description_1 = "o";
        goto frame_exception_exit_1;
    }
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7f234a464f34ccfabdf4b2ecfb5012f7 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_7f234a464f34ccfabdf4b2ecfb5012f7 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7f234a464f34ccfabdf4b2ecfb5012f7 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_7f234a464f34ccfabdf4b2ecfb5012f7, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_7f234a464f34ccfabdf4b2ecfb5012f7->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_7f234a464f34ccfabdf4b2ecfb5012f7, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_7f234a464f34ccfabdf4b2ecfb5012f7,
        type_description_1,
        par_mod
    );


    // Release cached frame.
    if ( frame_7f234a464f34ccfabdf4b2ecfb5012f7 == cache_frame_7f234a464f34ccfabdf4b2ecfb5012f7 )
    {
        Py_DECREF( frame_7f234a464f34ccfabdf4b2ecfb5012f7 );
    }
    cache_frame_7f234a464f34ccfabdf4b2ecfb5012f7 = NULL;

    assertFrameObject( frame_7f234a464f34ccfabdf4b2ecfb5012f7 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_30_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_mod );
    par_mod = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_mod );
    par_mod = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_30_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_31_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_mod = python_pars[ 0 ];
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    static struct Nuitka_FrameObject *cache_frame_dd0eb83b79989b2fbd28ecd5d44684bb = NULL;

    struct Nuitka_FrameObject *frame_dd0eb83b79989b2fbd28ecd5d44684bb;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_dd0eb83b79989b2fbd28ecd5d44684bb, codeobj_dd0eb83b79989b2fbd28ecd5d44684bb, module_pandas$util$_print_versions, sizeof(void *) );
    frame_dd0eb83b79989b2fbd28ecd5d44684bb = cache_frame_dd0eb83b79989b2fbd28ecd5d44684bb;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_dd0eb83b79989b2fbd28ecd5d44684bb );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_dd0eb83b79989b2fbd28ecd5d44684bb ) == 2 ); // Frame stack

    // Framed code:
    tmp_source_name_1 = par_mod;

    CHECK_OBJECT( tmp_source_name_1 );
    tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___version__ );
    if ( tmp_return_value == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 94;
        type_description_1 = "o";
        goto frame_exception_exit_1;
    }
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_dd0eb83b79989b2fbd28ecd5d44684bb );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_dd0eb83b79989b2fbd28ecd5d44684bb );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_dd0eb83b79989b2fbd28ecd5d44684bb );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_dd0eb83b79989b2fbd28ecd5d44684bb, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_dd0eb83b79989b2fbd28ecd5d44684bb->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_dd0eb83b79989b2fbd28ecd5d44684bb, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_dd0eb83b79989b2fbd28ecd5d44684bb,
        type_description_1,
        par_mod
    );


    // Release cached frame.
    if ( frame_dd0eb83b79989b2fbd28ecd5d44684bb == cache_frame_dd0eb83b79989b2fbd28ecd5d44684bb )
    {
        Py_DECREF( frame_dd0eb83b79989b2fbd28ecd5d44684bb );
    }
    cache_frame_dd0eb83b79989b2fbd28ecd5d44684bb = NULL;

    assertFrameObject( frame_dd0eb83b79989b2fbd28ecd5d44684bb );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_31_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_mod );
    par_mod = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_mod );
    par_mod = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_31_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_32_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_mod = python_pars[ 0 ];
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    static struct Nuitka_FrameObject *cache_frame_e4b3e828b2b499d8ee29f6d608de7f47 = NULL;

    struct Nuitka_FrameObject *frame_e4b3e828b2b499d8ee29f6d608de7f47;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_e4b3e828b2b499d8ee29f6d608de7f47, codeobj_e4b3e828b2b499d8ee29f6d608de7f47, module_pandas$util$_print_versions, sizeof(void *) );
    frame_e4b3e828b2b499d8ee29f6d608de7f47 = cache_frame_e4b3e828b2b499d8ee29f6d608de7f47;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_e4b3e828b2b499d8ee29f6d608de7f47 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_e4b3e828b2b499d8ee29f6d608de7f47 ) == 2 ); // Frame stack

    // Framed code:
    tmp_source_name_1 = par_mod;

    CHECK_OBJECT( tmp_source_name_1 );
    tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___version__ );
    if ( tmp_return_value == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 95;
        type_description_1 = "o";
        goto frame_exception_exit_1;
    }
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e4b3e828b2b499d8ee29f6d608de7f47 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_e4b3e828b2b499d8ee29f6d608de7f47 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e4b3e828b2b499d8ee29f6d608de7f47 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_e4b3e828b2b499d8ee29f6d608de7f47, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_e4b3e828b2b499d8ee29f6d608de7f47->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_e4b3e828b2b499d8ee29f6d608de7f47, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_e4b3e828b2b499d8ee29f6d608de7f47,
        type_description_1,
        par_mod
    );


    // Release cached frame.
    if ( frame_e4b3e828b2b499d8ee29f6d608de7f47 == cache_frame_e4b3e828b2b499d8ee29f6d608de7f47 )
    {
        Py_DECREF( frame_e4b3e828b2b499d8ee29f6d608de7f47 );
    }
    cache_frame_e4b3e828b2b499d8ee29f6d608de7f47 = NULL;

    assertFrameObject( frame_e4b3e828b2b499d8ee29f6d608de7f47 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_32_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_mod );
    par_mod = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_mod );
    par_mod = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_32_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_33_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_mod = python_pars[ 0 ];
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    static struct Nuitka_FrameObject *cache_frame_0c0e6bf08ffd07c0bd8549a6679c6f34 = NULL;

    struct Nuitka_FrameObject *frame_0c0e6bf08ffd07c0bd8549a6679c6f34;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_0c0e6bf08ffd07c0bd8549a6679c6f34, codeobj_0c0e6bf08ffd07c0bd8549a6679c6f34, module_pandas$util$_print_versions, sizeof(void *) );
    frame_0c0e6bf08ffd07c0bd8549a6679c6f34 = cache_frame_0c0e6bf08ffd07c0bd8549a6679c6f34;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_0c0e6bf08ffd07c0bd8549a6679c6f34 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_0c0e6bf08ffd07c0bd8549a6679c6f34 ) == 2 ); // Frame stack

    // Framed code:
    tmp_source_name_1 = par_mod;

    CHECK_OBJECT( tmp_source_name_1 );
    tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___version__ );
    if ( tmp_return_value == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 96;
        type_description_1 = "o";
        goto frame_exception_exit_1;
    }
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0c0e6bf08ffd07c0bd8549a6679c6f34 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_0c0e6bf08ffd07c0bd8549a6679c6f34 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0c0e6bf08ffd07c0bd8549a6679c6f34 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_0c0e6bf08ffd07c0bd8549a6679c6f34, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_0c0e6bf08ffd07c0bd8549a6679c6f34->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_0c0e6bf08ffd07c0bd8549a6679c6f34, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_0c0e6bf08ffd07c0bd8549a6679c6f34,
        type_description_1,
        par_mod
    );


    // Release cached frame.
    if ( frame_0c0e6bf08ffd07c0bd8549a6679c6f34 == cache_frame_0c0e6bf08ffd07c0bd8549a6679c6f34 )
    {
        Py_DECREF( frame_0c0e6bf08ffd07c0bd8549a6679c6f34 );
    }
    cache_frame_0c0e6bf08ffd07c0bd8549a6679c6f34 = NULL;

    assertFrameObject( frame_0c0e6bf08ffd07c0bd8549a6679c6f34 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_33_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_mod );
    par_mod = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_mod );
    par_mod = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_33_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_34_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_mod = python_pars[ 0 ];
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    static struct Nuitka_FrameObject *cache_frame_3454a3ed34b89e6112d9a51765b9419c = NULL;

    struct Nuitka_FrameObject *frame_3454a3ed34b89e6112d9a51765b9419c;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_3454a3ed34b89e6112d9a51765b9419c, codeobj_3454a3ed34b89e6112d9a51765b9419c, module_pandas$util$_print_versions, sizeof(void *) );
    frame_3454a3ed34b89e6112d9a51765b9419c = cache_frame_3454a3ed34b89e6112d9a51765b9419c;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_3454a3ed34b89e6112d9a51765b9419c );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_3454a3ed34b89e6112d9a51765b9419c ) == 2 ); // Frame stack

    // Framed code:
    tmp_source_name_1 = par_mod;

    CHECK_OBJECT( tmp_source_name_1 );
    tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___version__ );
    if ( tmp_return_value == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 97;
        type_description_1 = "o";
        goto frame_exception_exit_1;
    }
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3454a3ed34b89e6112d9a51765b9419c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_3454a3ed34b89e6112d9a51765b9419c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3454a3ed34b89e6112d9a51765b9419c );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_3454a3ed34b89e6112d9a51765b9419c, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_3454a3ed34b89e6112d9a51765b9419c->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_3454a3ed34b89e6112d9a51765b9419c, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_3454a3ed34b89e6112d9a51765b9419c,
        type_description_1,
        par_mod
    );


    // Release cached frame.
    if ( frame_3454a3ed34b89e6112d9a51765b9419c == cache_frame_3454a3ed34b89e6112d9a51765b9419c )
    {
        Py_DECREF( frame_3454a3ed34b89e6112d9a51765b9419c );
    }
    cache_frame_3454a3ed34b89e6112d9a51765b9419c = NULL;

    assertFrameObject( frame_3454a3ed34b89e6112d9a51765b9419c );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_34_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_mod );
    par_mod = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_mod );
    par_mod = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_34_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_35_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_mod = python_pars[ 0 ];
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    static struct Nuitka_FrameObject *cache_frame_3676c49011b4315fe8eb7b1cea5a236d = NULL;

    struct Nuitka_FrameObject *frame_3676c49011b4315fe8eb7b1cea5a236d;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_3676c49011b4315fe8eb7b1cea5a236d, codeobj_3676c49011b4315fe8eb7b1cea5a236d, module_pandas$util$_print_versions, sizeof(void *) );
    frame_3676c49011b4315fe8eb7b1cea5a236d = cache_frame_3676c49011b4315fe8eb7b1cea5a236d;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_3676c49011b4315fe8eb7b1cea5a236d );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_3676c49011b4315fe8eb7b1cea5a236d ) == 2 ); // Frame stack

    // Framed code:
    tmp_source_name_1 = par_mod;

    CHECK_OBJECT( tmp_source_name_1 );
    tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___version__ );
    if ( tmp_return_value == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 98;
        type_description_1 = "o";
        goto frame_exception_exit_1;
    }
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3676c49011b4315fe8eb7b1cea5a236d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_3676c49011b4315fe8eb7b1cea5a236d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3676c49011b4315fe8eb7b1cea5a236d );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_3676c49011b4315fe8eb7b1cea5a236d, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_3676c49011b4315fe8eb7b1cea5a236d->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_3676c49011b4315fe8eb7b1cea5a236d, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_3676c49011b4315fe8eb7b1cea5a236d,
        type_description_1,
        par_mod
    );


    // Release cached frame.
    if ( frame_3676c49011b4315fe8eb7b1cea5a236d == cache_frame_3676c49011b4315fe8eb7b1cea5a236d )
    {
        Py_DECREF( frame_3676c49011b4315fe8eb7b1cea5a236d );
    }
    cache_frame_3676c49011b4315fe8eb7b1cea5a236d = NULL;

    assertFrameObject( frame_3676c49011b4315fe8eb7b1cea5a236d );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_35_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_mod );
    par_mod = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_mod );
    par_mod = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_2_show_versions$$$function_35_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$util$_print_versions$$$function_3_main( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *var_OptionParser = NULL;
    PyObject *var_parser = NULL;
    PyObject *var_options = NULL;
    PyObject *var_args = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *tmp_args_name_1;
    PyObject *tmp_assattr_name_1;
    PyObject *tmp_assattr_target_1;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_assign_source_5;
    PyObject *tmp_assign_source_6;
    PyObject *tmp_assign_source_7;
    PyObject *tmp_called_instance_1;
    PyObject *tmp_called_name_1;
    PyObject *tmp_called_name_2;
    PyObject *tmp_called_name_3;
    int tmp_cmp_Eq_1;
    PyObject *tmp_compare_left_1;
    PyObject *tmp_compare_right_1;
    PyObject *tmp_dict_key_1;
    PyObject *tmp_dict_value_1;
    PyObject *tmp_fromlist_name_1;
    PyObject *tmp_globals_name_1;
    PyObject *tmp_import_name_from_1;
    PyObject *tmp_iter_arg_1;
    PyObject *tmp_iterator_attempt;
    PyObject *tmp_iterator_name_1;
    PyObject *tmp_kw_name_1;
    PyObject *tmp_kw_name_2;
    PyObject *tmp_level_name_1;
    PyObject *tmp_locals_name_1;
    PyObject *tmp_name_name_1;
    int tmp_res;
    bool tmp_result;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    PyObject *tmp_source_name_2;
    PyObject *tmp_source_name_3;
    PyObject *tmp_unpack_1;
    PyObject *tmp_unpack_2;
    NUITKA_MAY_BE_UNUSED PyObject *tmp_unused;
    static struct Nuitka_FrameObject *cache_frame_0a6c0c831f401b1e5eb3bcd85df09880 = NULL;

    struct Nuitka_FrameObject *frame_0a6c0c831f401b1e5eb3bcd85df09880;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_0a6c0c831f401b1e5eb3bcd85df09880, codeobj_0a6c0c831f401b1e5eb3bcd85df09880, module_pandas$util$_print_versions, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_0a6c0c831f401b1e5eb3bcd85df09880 = cache_frame_0a6c0c831f401b1e5eb3bcd85df09880;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_0a6c0c831f401b1e5eb3bcd85df09880 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_0a6c0c831f401b1e5eb3bcd85df09880 ) == 2 ); // Frame stack

    // Framed code:
    tmp_name_name_1 = const_str_plain_optparse;
    tmp_globals_name_1 = (PyObject *)moduledict_pandas$util$_print_versions;
    tmp_locals_name_1 = Py_None;
    tmp_fromlist_name_1 = const_tuple_str_plain_OptionParser_tuple;
    tmp_level_name_1 = const_int_0;
    frame_0a6c0c831f401b1e5eb3bcd85df09880->m_frame.f_lineno = 141;
    tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
    if ( tmp_import_name_from_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 141;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    tmp_assign_source_1 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_OptionParser );
    Py_DECREF( tmp_import_name_from_1 );
    if ( tmp_assign_source_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 141;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    assert( var_OptionParser == NULL );
    var_OptionParser = tmp_assign_source_1;

    tmp_called_name_1 = var_OptionParser;

    CHECK_OBJECT( tmp_called_name_1 );
    frame_0a6c0c831f401b1e5eb3bcd85df09880->m_frame.f_lineno = 142;
    tmp_assign_source_2 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
    if ( tmp_assign_source_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 142;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    assert( var_parser == NULL );
    var_parser = tmp_assign_source_2;

    tmp_source_name_1 = var_parser;

    CHECK_OBJECT( tmp_source_name_1 );
    tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_add_option );
    if ( tmp_called_name_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 143;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    tmp_args_name_1 = const_tuple_0852dff813ee080787a63e6ce09b134d_tuple;
    tmp_kw_name_1 = PyDict_Copy( const_dict_4c3bfbd1be3e51fcab24b1e0e0360329 );
    frame_0a6c0c831f401b1e5eb3bcd85df09880->m_frame.f_lineno = 143;
    tmp_unused = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_1, tmp_kw_name_1 );
    Py_DECREF( tmp_called_name_2 );
    Py_DECREF( tmp_kw_name_1 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 143;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_unused );
    // Tried code:
    tmp_called_instance_1 = var_parser;

    if ( tmp_called_instance_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "parser" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 147;
        type_description_1 = "oooo";
        goto try_except_handler_2;
    }

    frame_0a6c0c831f401b1e5eb3bcd85df09880->m_frame.f_lineno = 147;
    tmp_iter_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_parse_args );
    if ( tmp_iter_arg_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 147;
        type_description_1 = "oooo";
        goto try_except_handler_2;
    }
    tmp_assign_source_3 = MAKE_ITERATOR( tmp_iter_arg_1 );
    Py_DECREF( tmp_iter_arg_1 );
    if ( tmp_assign_source_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 147;
        type_description_1 = "oooo";
        goto try_except_handler_2;
    }
    assert( tmp_tuple_unpack_1__source_iter == NULL );
    tmp_tuple_unpack_1__source_iter = tmp_assign_source_3;

    // Tried code:
    tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;

    CHECK_OBJECT( tmp_unpack_1 );
    tmp_assign_source_4 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
    if ( tmp_assign_source_4 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        type_description_1 = "oooo";
        exception_lineno = 147;
        goto try_except_handler_3;
    }
    assert( tmp_tuple_unpack_1__element_1 == NULL );
    tmp_tuple_unpack_1__element_1 = tmp_assign_source_4;

    tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;

    CHECK_OBJECT( tmp_unpack_2 );
    tmp_assign_source_5 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
    if ( tmp_assign_source_5 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        type_description_1 = "oooo";
        exception_lineno = 147;
        goto try_except_handler_3;
    }
    assert( tmp_tuple_unpack_1__element_2 == NULL );
    tmp_tuple_unpack_1__element_2 = tmp_assign_source_5;

    tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;

    CHECK_OBJECT( tmp_iterator_name_1 );
    // Check if iterator has left-over elements.
    CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

    tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

    if (likely( tmp_iterator_attempt == NULL ))
    {
        PyObject *error = GET_ERROR_OCCURRED();

        if ( error != NULL )
        {
            if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
            {
                CLEAR_ERROR_OCCURRED();
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                type_description_1 = "oooo";
                exception_lineno = 147;
                goto try_except_handler_3;
            }
        }
    }
    else
    {
        Py_DECREF( tmp_iterator_attempt );

        // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
        PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
        PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

        type_description_1 = "oooo";
        exception_lineno = 147;
        goto try_except_handler_3;
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_2;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    tmp_assign_source_6 = tmp_tuple_unpack_1__element_1;

    CHECK_OBJECT( tmp_assign_source_6 );
    assert( var_options == NULL );
    Py_INCREF( tmp_assign_source_6 );
    var_options = tmp_assign_source_6;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    tmp_assign_source_7 = tmp_tuple_unpack_1__element_2;

    CHECK_OBJECT( tmp_assign_source_7 );
    assert( var_args == NULL );
    Py_INCREF( tmp_assign_source_7 );
    var_args = tmp_assign_source_7;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    tmp_source_name_2 = var_options;

    if ( tmp_source_name_2 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "options" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 149;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }

    tmp_compare_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_json );
    if ( tmp_compare_left_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 149;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    tmp_compare_right_1 = const_str_chr_45;
    tmp_cmp_Eq_1 = RICH_COMPARE_BOOL_EQ( tmp_compare_left_1, tmp_compare_right_1 );
    Py_DECREF( tmp_compare_left_1 );
    if ( tmp_cmp_Eq_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 149;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    if ( tmp_cmp_Eq_1 == 1 )
    {
        goto branch_yes_1;
    }
    else
    {
        goto branch_no_1;
    }
    branch_yes_1:;
    tmp_assattr_name_1 = Py_True;
    tmp_assattr_target_1 = var_options;

    if ( tmp_assattr_target_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "options" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 150;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }

    tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_json, tmp_assattr_name_1 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 150;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    branch_no_1:;
    tmp_called_name_3 = GET_STRING_DICT_VALUE( moduledict_pandas$util$_print_versions, (Nuitka_StringObject *)const_str_plain_show_versions );

    if (unlikely( tmp_called_name_3 == NULL ))
    {
        tmp_called_name_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_show_versions );
    }

    if ( tmp_called_name_3 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "show_versions" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 152;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }

    tmp_dict_key_1 = const_str_plain_as_json;
    tmp_source_name_3 = var_options;

    if ( tmp_source_name_3 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "options" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 152;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }

    tmp_dict_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_json );
    if ( tmp_dict_value_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 152;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    tmp_kw_name_2 = _PyDict_NewPresized( 1 );
    tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_1, tmp_dict_value_1 );
    Py_DECREF( tmp_dict_value_1 );
    assert( !(tmp_res != 0) );
    frame_0a6c0c831f401b1e5eb3bcd85df09880->m_frame.f_lineno = 152;
    tmp_unused = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_3, tmp_kw_name_2 );
    Py_DECREF( tmp_kw_name_2 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 152;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_unused );

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0a6c0c831f401b1e5eb3bcd85df09880 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0a6c0c831f401b1e5eb3bcd85df09880 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_0a6c0c831f401b1e5eb3bcd85df09880, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_0a6c0c831f401b1e5eb3bcd85df09880->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_0a6c0c831f401b1e5eb3bcd85df09880, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_0a6c0c831f401b1e5eb3bcd85df09880,
        type_description_1,
        var_OptionParser,
        var_parser,
        var_options,
        var_args
    );


    // Release cached frame.
    if ( frame_0a6c0c831f401b1e5eb3bcd85df09880 == cache_frame_0a6c0c831f401b1e5eb3bcd85df09880 )
    {
        Py_DECREF( frame_0a6c0c831f401b1e5eb3bcd85df09880 );
    }
    cache_frame_0a6c0c831f401b1e5eb3bcd85df09880 = NULL;

    assertFrameObject( frame_0a6c0c831f401b1e5eb3bcd85df09880 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    tmp_return_value = const_int_0;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_3_main );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( var_OptionParser );
    var_OptionParser = NULL;

    Py_XDECREF( var_parser );
    var_parser = NULL;

    Py_XDECREF( var_options );
    var_options = NULL;

    Py_XDECREF( var_args );
    var_args = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( var_OptionParser );
    var_OptionParser = NULL;

    Py_XDECREF( var_parser );
    var_parser = NULL;

    Py_XDECREF( var_options );
    var_options = NULL;

    Py_XDECREF( var_args );
    var_args = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$util$_print_versions$$$function_3_main );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}



static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_1_get_sys_info(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$util$_print_versions$$$function_1_get_sys_info,
        const_str_plain_get_sys_info,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_49aeb67ae4770645ba85a4ae4c82ce83,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$util$_print_versions,
        const_str_digest_e4caad594972457f9c6a9870b1cae64a,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$util$_print_versions$$$function_2_show_versions,
        const_str_plain_show_versions,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_e9a5b67587c704036c3f68c226ebe7dc,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$util$_print_versions,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_10_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_10_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_0a536a34bf38117b24345f72cb8edf64,
#endif
        codeobj_072403d28f5741828c95c4e73365d503,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$util$_print_versions,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_11_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_11_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_0a536a34bf38117b24345f72cb8edf64,
#endif
        codeobj_14a520a50b3a023d1743172655f97257,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$util$_print_versions,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_12_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_12_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_0a536a34bf38117b24345f72cb8edf64,
#endif
        codeobj_1e8d1315dafed74c06af0f9717e1ea23,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$util$_print_versions,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_13_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_13_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_0a536a34bf38117b24345f72cb8edf64,
#endif
        codeobj_e41547ccdc4c4e73dc064032cd7327eb,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$util$_print_versions,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_14_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_14_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_0a536a34bf38117b24345f72cb8edf64,
#endif
        codeobj_8ef132c20da65b7d3460e4a332b6f9be,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$util$_print_versions,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_15_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_15_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_0a536a34bf38117b24345f72cb8edf64,
#endif
        codeobj_73c6b457b34bdd71645a37575f39ac4c,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$util$_print_versions,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_16_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_16_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_0a536a34bf38117b24345f72cb8edf64,
#endif
        codeobj_c7dc781ac04c588d3526dde79b9b6ce7,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$util$_print_versions,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_17_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_17_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_0a536a34bf38117b24345f72cb8edf64,
#endif
        codeobj_7af43919b9767ca1039c00d325ec4c76,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$util$_print_versions,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_18_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_18_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_0a536a34bf38117b24345f72cb8edf64,
#endif
        codeobj_eca96f70c2e5af9d01c7ee02fd375cfc,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$util$_print_versions,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_19_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_19_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_0a536a34bf38117b24345f72cb8edf64,
#endif
        codeobj_6cce22e69d0dff310e212335427e63ec,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$util$_print_versions,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_1_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_1_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_0a536a34bf38117b24345f72cb8edf64,
#endif
        codeobj_926b2cd8a50bada964ab9ff7ea36c401,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$util$_print_versions,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_20_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_20_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_0a536a34bf38117b24345f72cb8edf64,
#endif
        codeobj_c88733dda5f5eb0d43a9100a815f3351,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$util$_print_versions,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_21_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_21_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_0a536a34bf38117b24345f72cb8edf64,
#endif
        codeobj_06e645d0c17d11ba46064414e495bd0f,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$util$_print_versions,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_22_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_22_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_0a536a34bf38117b24345f72cb8edf64,
#endif
        codeobj_1cf2bb340d87ac08e24b5cc60455989a,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$util$_print_versions,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_23_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_23_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_0a536a34bf38117b24345f72cb8edf64,
#endif
        codeobj_1bb8923b3f31e892b461b41546196ed0,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$util$_print_versions,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_24_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_24_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_0a536a34bf38117b24345f72cb8edf64,
#endif
        codeobj_799cfdc0da788d48e7b5639fe577b6d6,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$util$_print_versions,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_25_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_25_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_0a536a34bf38117b24345f72cb8edf64,
#endif
        codeobj_51f02fd3ac7af88394eb95b568e206ab,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$util$_print_versions,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_26_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_26_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_0a536a34bf38117b24345f72cb8edf64,
#endif
        codeobj_c27441d7de8a3ede56c605427a69e303,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$util$_print_versions,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_27_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_27_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_0a536a34bf38117b24345f72cb8edf64,
#endif
        codeobj_65ec58e74815899139a3f53dae1074bc,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$util$_print_versions,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_28_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_28_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_0a536a34bf38117b24345f72cb8edf64,
#endif
        codeobj_9f69f0cf233ce6e9d9b4f05d43820748,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$util$_print_versions,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_29_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_29_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_0a536a34bf38117b24345f72cb8edf64,
#endif
        codeobj_02480085d27574d71b036399ed7da475,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$util$_print_versions,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_2_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_2_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_0a536a34bf38117b24345f72cb8edf64,
#endif
        codeobj_8b947b74bca15c4b71a777120c297bfa,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$util$_print_versions,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_30_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_30_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_0a536a34bf38117b24345f72cb8edf64,
#endif
        codeobj_7f234a464f34ccfabdf4b2ecfb5012f7,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$util$_print_versions,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_31_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_31_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_0a536a34bf38117b24345f72cb8edf64,
#endif
        codeobj_dd0eb83b79989b2fbd28ecd5d44684bb,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$util$_print_versions,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_32_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_32_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_0a536a34bf38117b24345f72cb8edf64,
#endif
        codeobj_e4b3e828b2b499d8ee29f6d608de7f47,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$util$_print_versions,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_33_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_33_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_0a536a34bf38117b24345f72cb8edf64,
#endif
        codeobj_0c0e6bf08ffd07c0bd8549a6679c6f34,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$util$_print_versions,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_34_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_34_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_0a536a34bf38117b24345f72cb8edf64,
#endif
        codeobj_3454a3ed34b89e6112d9a51765b9419c,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$util$_print_versions,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_35_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_35_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_0a536a34bf38117b24345f72cb8edf64,
#endif
        codeobj_3676c49011b4315fe8eb7b1cea5a236d,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$util$_print_versions,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_3_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_3_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_0a536a34bf38117b24345f72cb8edf64,
#endif
        codeobj_e41b1c514ea1750764423a6ad3fa842b,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$util$_print_versions,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_4_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_4_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_0a536a34bf38117b24345f72cb8edf64,
#endif
        codeobj_f51c2fec9c6a331cc5981904628c07e1,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$util$_print_versions,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_5_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_5_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_0a536a34bf38117b24345f72cb8edf64,
#endif
        codeobj_f73f0991cf3aa1993b37c583fcf51a1c,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$util$_print_versions,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_6_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_6_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_0a536a34bf38117b24345f72cb8edf64,
#endif
        codeobj_5a4ac99e386874c3c884ed40f9dbb322,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$util$_print_versions,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_7_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_7_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_0a536a34bf38117b24345f72cb8edf64,
#endif
        codeobj_2ec3fcea70543e510ce3df0dbd94aa4b,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$util$_print_versions,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_8_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_8_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_0a536a34bf38117b24345f72cb8edf64,
#endif
        codeobj_38f7b17fed2ed0edc796d820d8b473df,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$util$_print_versions,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions$$$function_9_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$util$_print_versions$$$function_2_show_versions$$$function_9_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_0a536a34bf38117b24345f72cb8edf64,
#endif
        codeobj_f40a2453591b4baa581365fc75869546,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$util$_print_versions,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$util$_print_versions$$$function_3_main(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$util$_print_versions$$$function_3_main,
        const_str_plain_main,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_0a6c0c831f401b1e5eb3bcd85df09880,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$util$_print_versions,
        Py_None,
        0
    );


    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_pandas$util$_print_versions =
{
    PyModuleDef_HEAD_INIT,
    "pandas.util._print_versions",   /* m_name */
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;

extern PyObject *const_str_plain___loader__;
extern PyObject *metapath_based_loader;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL.

MOD_INIT_DECL( pandas$util$_print_versions )
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_pandas$util$_print_versions );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("pandas.util._print_versions: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("pandas.util._print_versions: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("pandas.util._print_versions: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initpandas$util$_print_versions" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_pandas$util$_print_versions = Py_InitModule4(
        "pandas.util._print_versions",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_pandas$util$_print_versions = PyModule_Create( &mdef_pandas$util$_print_versions );
#endif

    moduledict_pandas$util$_print_versions = MODULE_DICT( module_pandas$util$_print_versions );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_pandas$util$_print_versions, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_pandas$util$_print_versions,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );

#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_pandas$util$_print_versions, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_pandas$util$_print_versions,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_pandas$util$_print_versions, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_pandas$util$_print_versions,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_pandas$util$_print_versions );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PySys_GetObject( (char *)"modules" ), const_str_digest_da95bebd2524b65728827a9e6d1504b8, module_pandas$util$_print_versions );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_pandas$util$_print_versions, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_pandas$util$_print_versions, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_pandas$util$_print_versions, (Nuitka_StringObject *)const_str_plain___loader__, metapath_based_loader );
#endif

#if PYTHON_VERSION >= 340
#if 0
    UPDATE_STRING_DICT0( moduledict_pandas$util$_print_versions, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_pandas$util$_print_versions, (Nuitka_StringObject *)const_str_plain___name__ ),
            metapath_based_loader
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );

        UPDATE_STRING_DICT1( moduledict_pandas$util$_print_versions, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );

        Py_DECREF( module_spec_class );
    }
#endif
#endif


    // Temp variables if any
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_assign_source_5;
    PyObject *tmp_assign_source_6;
    PyObject *tmp_assign_source_7;
    PyObject *tmp_assign_source_8;
    PyObject *tmp_assign_source_9;
    PyObject *tmp_assign_source_10;
    PyObject *tmp_assign_source_11;
    PyObject *tmp_assign_source_12;
    PyObject *tmp_assign_source_13;
    PyObject *tmp_assign_source_14;
    PyObject *tmp_defaults_1;
    PyObject *tmp_fromlist_name_1;
    PyObject *tmp_fromlist_name_2;
    PyObject *tmp_fromlist_name_3;
    PyObject *tmp_fromlist_name_4;
    PyObject *tmp_fromlist_name_5;
    PyObject *tmp_fromlist_name_6;
    PyObject *tmp_fromlist_name_7;
    PyObject *tmp_fromlist_name_8;
    PyObject *tmp_globals_name_1;
    PyObject *tmp_globals_name_2;
    PyObject *tmp_globals_name_3;
    PyObject *tmp_globals_name_4;
    PyObject *tmp_globals_name_5;
    PyObject *tmp_globals_name_6;
    PyObject *tmp_globals_name_7;
    PyObject *tmp_globals_name_8;
    PyObject *tmp_level_name_1;
    PyObject *tmp_level_name_2;
    PyObject *tmp_level_name_3;
    PyObject *tmp_level_name_4;
    PyObject *tmp_level_name_5;
    PyObject *tmp_level_name_6;
    PyObject *tmp_level_name_7;
    PyObject *tmp_level_name_8;
    PyObject *tmp_locals_name_1;
    PyObject *tmp_locals_name_2;
    PyObject *tmp_locals_name_3;
    PyObject *tmp_locals_name_4;
    PyObject *tmp_locals_name_5;
    PyObject *tmp_locals_name_6;
    PyObject *tmp_locals_name_7;
    PyObject *tmp_locals_name_8;
    PyObject *tmp_name_name_1;
    PyObject *tmp_name_name_2;
    PyObject *tmp_name_name_3;
    PyObject *tmp_name_name_4;
    PyObject *tmp_name_name_5;
    PyObject *tmp_name_name_6;
    PyObject *tmp_name_name_7;
    PyObject *tmp_name_name_8;
    struct Nuitka_FrameObject *frame_2c5d159c9ddebe4a467bddaa1f16e4c8;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;

    // Module code.
    tmp_assign_source_1 = Py_None;
    UPDATE_STRING_DICT0( moduledict_pandas$util$_print_versions, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    tmp_assign_source_2 = const_str_digest_9d97a020f78fe137abaa84e2f6e65538;
    UPDATE_STRING_DICT0( moduledict_pandas$util$_print_versions, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    tmp_assign_source_3 = Py_None;
    UPDATE_STRING_DICT0( moduledict_pandas$util$_print_versions, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    // Frame without reuse.
    frame_2c5d159c9ddebe4a467bddaa1f16e4c8 = MAKE_MODULE_FRAME( codeobj_2c5d159c9ddebe4a467bddaa1f16e4c8, module_pandas$util$_print_versions );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_2c5d159c9ddebe4a467bddaa1f16e4c8 );
    assert( Py_REFCNT( frame_2c5d159c9ddebe4a467bddaa1f16e4c8 ) == 2 );

    // Framed code:
    tmp_name_name_1 = const_str_plain_os;
    tmp_globals_name_1 = (PyObject *)moduledict_pandas$util$_print_versions;
    tmp_locals_name_1 = Py_None;
    tmp_fromlist_name_1 = Py_None;
    tmp_level_name_1 = const_int_0;
    frame_2c5d159c9ddebe4a467bddaa1f16e4c8->m_frame.f_lineno = 1;
    tmp_assign_source_4 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
    if ( tmp_assign_source_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 1;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$util$_print_versions, (Nuitka_StringObject *)const_str_plain_os, tmp_assign_source_4 );
    tmp_name_name_2 = const_str_plain_platform;
    tmp_globals_name_2 = (PyObject *)moduledict_pandas$util$_print_versions;
    tmp_locals_name_2 = Py_None;
    tmp_fromlist_name_2 = Py_None;
    tmp_level_name_2 = const_int_0;
    frame_2c5d159c9ddebe4a467bddaa1f16e4c8->m_frame.f_lineno = 2;
    tmp_assign_source_5 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
    if ( tmp_assign_source_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 2;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$util$_print_versions, (Nuitka_StringObject *)const_str_plain_platform, tmp_assign_source_5 );
    tmp_name_name_3 = const_str_plain_sys;
    tmp_globals_name_3 = (PyObject *)moduledict_pandas$util$_print_versions;
    tmp_locals_name_3 = Py_None;
    tmp_fromlist_name_3 = Py_None;
    tmp_level_name_3 = const_int_0;
    frame_2c5d159c9ddebe4a467bddaa1f16e4c8->m_frame.f_lineno = 3;
    tmp_assign_source_6 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
    assert( !(tmp_assign_source_6 == NULL) );
    UPDATE_STRING_DICT1( moduledict_pandas$util$_print_versions, (Nuitka_StringObject *)const_str_plain_sys, tmp_assign_source_6 );
    tmp_name_name_4 = const_str_plain_struct;
    tmp_globals_name_4 = (PyObject *)moduledict_pandas$util$_print_versions;
    tmp_locals_name_4 = Py_None;
    tmp_fromlist_name_4 = Py_None;
    tmp_level_name_4 = const_int_0;
    frame_2c5d159c9ddebe4a467bddaa1f16e4c8->m_frame.f_lineno = 4;
    tmp_assign_source_7 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
    if ( tmp_assign_source_7 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 4;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$util$_print_versions, (Nuitka_StringObject *)const_str_plain_struct, tmp_assign_source_7 );
    tmp_name_name_5 = const_str_plain_subprocess;
    tmp_globals_name_5 = (PyObject *)moduledict_pandas$util$_print_versions;
    tmp_locals_name_5 = Py_None;
    tmp_fromlist_name_5 = Py_None;
    tmp_level_name_5 = const_int_0;
    frame_2c5d159c9ddebe4a467bddaa1f16e4c8->m_frame.f_lineno = 5;
    tmp_assign_source_8 = IMPORT_MODULE5( tmp_name_name_5, tmp_globals_name_5, tmp_locals_name_5, tmp_fromlist_name_5, tmp_level_name_5 );
    if ( tmp_assign_source_8 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 5;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$util$_print_versions, (Nuitka_StringObject *)const_str_plain_subprocess, tmp_assign_source_8 );
    tmp_name_name_6 = const_str_plain_codecs;
    tmp_globals_name_6 = (PyObject *)moduledict_pandas$util$_print_versions;
    tmp_locals_name_6 = Py_None;
    tmp_fromlist_name_6 = Py_None;
    tmp_level_name_6 = const_int_0;
    frame_2c5d159c9ddebe4a467bddaa1f16e4c8->m_frame.f_lineno = 6;
    tmp_assign_source_9 = IMPORT_MODULE5( tmp_name_name_6, tmp_globals_name_6, tmp_locals_name_6, tmp_fromlist_name_6, tmp_level_name_6 );
    if ( tmp_assign_source_9 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 6;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$util$_print_versions, (Nuitka_StringObject *)const_str_plain_codecs, tmp_assign_source_9 );
    tmp_name_name_7 = const_str_plain_locale;
    tmp_globals_name_7 = (PyObject *)moduledict_pandas$util$_print_versions;
    tmp_locals_name_7 = Py_None;
    tmp_fromlist_name_7 = Py_None;
    tmp_level_name_7 = const_int_0;
    frame_2c5d159c9ddebe4a467bddaa1f16e4c8->m_frame.f_lineno = 7;
    tmp_assign_source_10 = IMPORT_MODULE5( tmp_name_name_7, tmp_globals_name_7, tmp_locals_name_7, tmp_fromlist_name_7, tmp_level_name_7 );
    if ( tmp_assign_source_10 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 7;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$util$_print_versions, (Nuitka_StringObject *)const_str_plain_locale, tmp_assign_source_10 );
    tmp_name_name_8 = const_str_plain_importlib;
    tmp_globals_name_8 = (PyObject *)moduledict_pandas$util$_print_versions;
    tmp_locals_name_8 = Py_None;
    tmp_fromlist_name_8 = Py_None;
    tmp_level_name_8 = const_int_0;
    frame_2c5d159c9ddebe4a467bddaa1f16e4c8->m_frame.f_lineno = 8;
    tmp_assign_source_11 = IMPORT_MODULE5( tmp_name_name_8, tmp_globals_name_8, tmp_locals_name_8, tmp_fromlist_name_8, tmp_level_name_8 );
    if ( tmp_assign_source_11 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 8;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$util$_print_versions, (Nuitka_StringObject *)const_str_plain_importlib, tmp_assign_source_11 );

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_2c5d159c9ddebe4a467bddaa1f16e4c8 );
#endif
    popFrameStack();

    assertFrameObject( frame_2c5d159c9ddebe4a467bddaa1f16e4c8 );

    goto frame_no_exception_1;
    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_2c5d159c9ddebe4a467bddaa1f16e4c8 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_2c5d159c9ddebe4a467bddaa1f16e4c8, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_2c5d159c9ddebe4a467bddaa1f16e4c8->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_2c5d159c9ddebe4a467bddaa1f16e4c8, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;
    frame_no_exception_1:;
    tmp_assign_source_12 = MAKE_FUNCTION_pandas$util$_print_versions$$$function_1_get_sys_info(  );
    UPDATE_STRING_DICT1( moduledict_pandas$util$_print_versions, (Nuitka_StringObject *)const_str_plain_get_sys_info, tmp_assign_source_12 );
    tmp_defaults_1 = const_tuple_false_tuple;
    Py_INCREF( tmp_defaults_1 );
    tmp_assign_source_13 = MAKE_FUNCTION_pandas$util$_print_versions$$$function_2_show_versions( tmp_defaults_1 );
    UPDATE_STRING_DICT1( moduledict_pandas$util$_print_versions, (Nuitka_StringObject *)const_str_plain_show_versions, tmp_assign_source_13 );
    tmp_assign_source_14 = MAKE_FUNCTION_pandas$util$_print_versions$$$function_3_main(  );
    UPDATE_STRING_DICT1( moduledict_pandas$util$_print_versions, (Nuitka_StringObject *)const_str_plain_main, tmp_assign_source_14 );

    return MOD_RETURN_VALUE( module_pandas$util$_print_versions );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
