/* Generated code for Python source for module 'pandas.io.api'
 * created by Nuitka version 0.5.32.7
 *
 * This code is in part copyright 2018 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The _module_pandas$io$api is a Python object pointer of module type. */

/* Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_pandas$io$api;
PyDictObject *moduledict_pandas$io$api;

/* The module constants used, if any. */
extern PyObject *const_str_plain_read_json;
extern PyObject *const_str_plain_read_sql_table;
extern PyObject *const_str_plain_warn;
extern PyObject *const_str_plain_stacklevel;
extern PyObject *const_str_digest_a40d4ade2992e495604f976c385896dc;
static PyObject *const_tuple_str_plain_read_parquet_tuple;
extern PyObject *const_str_plain_read_clipboard;
static PyObject *const_tuple_239c3ca5f939d96b38a434c5a290d647_tuple;
extern PyObject *const_str_digest_643687ca524b713c08bac0e0fb284af6;
extern PyObject *const_str_digest_1b409ad5171b43d7f68abd8a3582a15b;
extern PyObject *const_str_digest_bd21952672e0a4be57b7e4bb75aad0fc;
static PyObject *const_tuple_str_plain_read_csv_str_plain_read_table_str_plain_read_fwf_tuple;
extern PyObject *const_str_plain_warnings;
extern PyObject *const_str_plain_read_parquet;
extern PyObject *const_str_plain_to_pickle;
static PyObject *const_str_digest_2b0756bbaabafd81f71158e90e1a045a;
static PyObject *const_str_digest_86b450487c15a00df34de4b82ad46280;
static PyObject *const_tuple_str_plain_read_feather_tuple;
extern PyObject *const_str_plain_read_pickle;
static PyObject *const_tuple_str_plain_read_msgpack_str_plain_to_msgpack_tuple;
extern PyObject *const_str_digest_094d97527936a6e3d816e81d57f7f99a;
extern PyObject *const_str_plain___file__;
extern PyObject *const_str_digest_097981268501cd7c6d993b1c3ffe923b;
extern PyObject *const_str_digest_604007273228d149e49d492f15420598;
static PyObject *const_tuple_str_plain_Term_tuple;
static PyObject *const_tuple_fbfa11d372e92c3f30cdd65db79dfc1e_tuple;
extern PyObject *const_str_plain_args;
static PyObject *const_tuple_d0542d5a6fa90cc1d7467797c56b6e80_tuple;
extern PyObject *const_int_0;
extern PyObject *const_str_digest_75090e6ed0be6fa03614e573a9d579f0;
static PyObject *const_tuple_str_plain_read_gbq_tuple;
extern PyObject *const_str_digest_e69d58d9e2862e786df4b7a1d941b057;
extern PyObject *const_str_plain_read_hdf;
extern PyObject *const_str_digest_c598ce781452927f25bd06a8898d5075;
extern PyObject *const_str_plain_ExcelFile;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_digest_a3503bdf669fdda45ff28f9aaf94e14f;
static PyObject *const_tuple_str_plain_read_json_tuple;
extern PyObject *const_str_plain_read_sql;
extern PyObject *const_dict_f154c9a58c9419d7e391901d7b7fe49e;
static PyObject *const_tuple_str_plain_HDFStore_str_plain_get_store_str_plain_read_hdf_tuple;
static PyObject *const_tuple_str_plain_read_stata_tuple;
extern PyObject *const_str_plain_read_csv;
static PyObject *const_tuple_str_plain_read_clipboard_tuple;
static PyObject *const_tuple_str_plain_read_html_tuple;
extern PyObject *const_str_plain_HDFStore;
extern PyObject *const_str_plain_read_excel;
static PyObject *const_tuple_str_plain_read_pickle_str_plain_to_pickle_tuple;
extern PyObject *const_str_digest_8717a5f4bc167d7ea49925b7dac16f2a;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_plain_kwargs;
static PyObject *const_str_digest_2c2de31924a351392c0e38801a650d0e;
extern PyObject *const_str_plain_to_msgpack;
extern PyObject *const_str_plain_get_store;
extern PyObject *const_str_digest_064a8208bd1386240dc3f245d83d37cd;
extern PyObject *const_int_pos_2;
extern PyObject *const_str_plain_ExcelWriter;
extern PyObject *const_str_digest_7a1fc9bd9faaac9777b29fc3d69d3c1d;
extern PyObject *const_str_plain_read_feather;
extern PyObject *const_str_plain_read_msgpack;
extern PyObject *const_str_plain_read_stata;
extern PyObject *const_str_plain_FutureWarning;
extern PyObject *const_tuple_str_plain_read_sas_tuple;
extern PyObject *const_str_plain_read_sas;
extern PyObject *const_str_plain_read_gbq;
extern PyObject *const_str_digest_9bdf56c59c2f38d290ad60074cc357bc;
extern PyObject *const_str_plain_read_table;
static PyObject *const_str_digest_2e52a822134d98b69dc1d37750829d70;
extern PyObject *const_str_plain_Term;
extern PyObject *const_str_plain_read_sql_query;
extern PyObject *const_str_plain_read_fwf;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_plain_read_html;
static PyObject *module_filename_obj;

static bool constants_created = false;

static void createModuleConstants( void )
{
    const_tuple_str_plain_read_parquet_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_read_parquet_tuple, 0, const_str_plain_read_parquet ); Py_INCREF( const_str_plain_read_parquet );
    const_tuple_239c3ca5f939d96b38a434c5a290d647_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_239c3ca5f939d96b38a434c5a290d647_tuple, 0, const_str_plain_read_sql ); Py_INCREF( const_str_plain_read_sql );
    PyTuple_SET_ITEM( const_tuple_239c3ca5f939d96b38a434c5a290d647_tuple, 1, const_str_plain_read_sql_table ); Py_INCREF( const_str_plain_read_sql_table );
    PyTuple_SET_ITEM( const_tuple_239c3ca5f939d96b38a434c5a290d647_tuple, 2, const_str_plain_read_sql_query ); Py_INCREF( const_str_plain_read_sql_query );
    const_tuple_str_plain_read_csv_str_plain_read_table_str_plain_read_fwf_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_read_csv_str_plain_read_table_str_plain_read_fwf_tuple, 0, const_str_plain_read_csv ); Py_INCREF( const_str_plain_read_csv );
    PyTuple_SET_ITEM( const_tuple_str_plain_read_csv_str_plain_read_table_str_plain_read_fwf_tuple, 1, const_str_plain_read_table ); Py_INCREF( const_str_plain_read_table );
    PyTuple_SET_ITEM( const_tuple_str_plain_read_csv_str_plain_read_table_str_plain_read_fwf_tuple, 2, const_str_plain_read_fwf ); Py_INCREF( const_str_plain_read_fwf );
    const_str_digest_2b0756bbaabafd81f71158e90e1a045a = UNSTREAM_STRING( &constant_bin[ 2949437 ], 145, 0 );
    const_str_digest_86b450487c15a00df34de4b82ad46280 = UNSTREAM_STRING( &constant_bin[ 2949582 ], 13, 0 );
    const_tuple_str_plain_read_feather_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_read_feather_tuple, 0, const_str_plain_read_feather ); Py_INCREF( const_str_plain_read_feather );
    const_tuple_str_plain_read_msgpack_str_plain_to_msgpack_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_read_msgpack_str_plain_to_msgpack_tuple, 0, const_str_plain_read_msgpack ); Py_INCREF( const_str_plain_read_msgpack );
    PyTuple_SET_ITEM( const_tuple_str_plain_read_msgpack_str_plain_to_msgpack_tuple, 1, const_str_plain_to_msgpack ); Py_INCREF( const_str_plain_to_msgpack );
    const_tuple_str_plain_Term_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Term_tuple, 0, const_str_plain_Term ); Py_INCREF( const_str_plain_Term );
    const_tuple_fbfa11d372e92c3f30cdd65db79dfc1e_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_fbfa11d372e92c3f30cdd65db79dfc1e_tuple, 0, const_str_plain_args ); Py_INCREF( const_str_plain_args );
    PyTuple_SET_ITEM( const_tuple_fbfa11d372e92c3f30cdd65db79dfc1e_tuple, 1, const_str_plain_kwargs ); Py_INCREF( const_str_plain_kwargs );
    PyTuple_SET_ITEM( const_tuple_fbfa11d372e92c3f30cdd65db79dfc1e_tuple, 2, const_str_plain_warnings ); Py_INCREF( const_str_plain_warnings );
    PyTuple_SET_ITEM( const_tuple_fbfa11d372e92c3f30cdd65db79dfc1e_tuple, 3, const_str_plain_Term ); Py_INCREF( const_str_plain_Term );
    const_tuple_d0542d5a6fa90cc1d7467797c56b6e80_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_d0542d5a6fa90cc1d7467797c56b6e80_tuple, 0, const_str_plain_ExcelFile ); Py_INCREF( const_str_plain_ExcelFile );
    PyTuple_SET_ITEM( const_tuple_d0542d5a6fa90cc1d7467797c56b6e80_tuple, 1, const_str_plain_ExcelWriter ); Py_INCREF( const_str_plain_ExcelWriter );
    PyTuple_SET_ITEM( const_tuple_d0542d5a6fa90cc1d7467797c56b6e80_tuple, 2, const_str_plain_read_excel ); Py_INCREF( const_str_plain_read_excel );
    const_tuple_str_plain_read_gbq_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_read_gbq_tuple, 0, const_str_plain_read_gbq ); Py_INCREF( const_str_plain_read_gbq );
    const_tuple_str_plain_read_json_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_read_json_tuple, 0, const_str_plain_read_json ); Py_INCREF( const_str_plain_read_json );
    const_tuple_str_plain_HDFStore_str_plain_get_store_str_plain_read_hdf_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_HDFStore_str_plain_get_store_str_plain_read_hdf_tuple, 0, const_str_plain_HDFStore ); Py_INCREF( const_str_plain_HDFStore );
    PyTuple_SET_ITEM( const_tuple_str_plain_HDFStore_str_plain_get_store_str_plain_read_hdf_tuple, 1, const_str_plain_get_store ); Py_INCREF( const_str_plain_get_store );
    PyTuple_SET_ITEM( const_tuple_str_plain_HDFStore_str_plain_get_store_str_plain_read_hdf_tuple, 2, const_str_plain_read_hdf ); Py_INCREF( const_str_plain_read_hdf );
    const_tuple_str_plain_read_stata_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_read_stata_tuple, 0, const_str_plain_read_stata ); Py_INCREF( const_str_plain_read_stata );
    const_tuple_str_plain_read_clipboard_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_read_clipboard_tuple, 0, const_str_plain_read_clipboard ); Py_INCREF( const_str_plain_read_clipboard );
    const_tuple_str_plain_read_html_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_read_html_tuple, 0, const_str_plain_read_html ); Py_INCREF( const_str_plain_read_html );
    const_tuple_str_plain_read_pickle_str_plain_to_pickle_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_read_pickle_str_plain_to_pickle_tuple, 0, const_str_plain_read_pickle ); Py_INCREF( const_str_plain_read_pickle );
    PyTuple_SET_ITEM( const_tuple_str_plain_read_pickle_str_plain_to_pickle_tuple, 1, const_str_plain_to_pickle ); Py_INCREF( const_str_plain_to_pickle );
    const_str_digest_2c2de31924a351392c0e38801a650d0e = UNSTREAM_STRING( &constant_bin[ 2949595 ], 79, 0 );
    const_str_digest_2e52a822134d98b69dc1d37750829d70 = UNSTREAM_STRING( &constant_bin[ 2949674 ], 22, 0 );

    constants_created = true;
}

#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_pandas$io$api( void )
{
    // The module may not have been used at all.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_cf67d5d3a7658e7d99409eae54dfa575;
static PyCodeObject *codeobj_ad542ce37db5ba35b474700ec3f27ffb;

static void createModuleCodeObjects(void)
{
    module_filename_obj = const_str_digest_2c2de31924a351392c0e38801a650d0e;
    codeobj_cf67d5d3a7658e7d99409eae54dfa575 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_2e52a822134d98b69dc1d37750829d70, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_ad542ce37db5ba35b474700ec3f27ffb = MAKE_CODEOBJ( module_filename_obj, const_str_plain_Term, 23, const_tuple_fbfa11d372e92c3f30cdd65db79dfc1e_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARARGS | CO_VARKEYWORDS | CO_NOFREE );
}

// The module function declarations.
NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_3_complex_call_helper_star_list_star_dict( PyObject **python_pars );


static PyObject *MAKE_FUNCTION_pandas$io$api$$$function_1_Term(  );


// The module function definitions.
static PyObject *impl_pandas$io$api$$$function_1_Term( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_args = python_pars[ 0 ];
    PyObject *par_kwargs = python_pars[ 1 ];
    PyObject *var_warnings = NULL;
    PyObject *var_Term = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_args_name_1;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_called_name_1;
    PyObject *tmp_dircall_arg1_1;
    PyObject *tmp_dircall_arg2_1;
    PyObject *tmp_dircall_arg3_1;
    PyObject *tmp_fromlist_name_1;
    PyObject *tmp_fromlist_name_2;
    PyObject *tmp_globals_name_1;
    PyObject *tmp_globals_name_2;
    PyObject *tmp_import_name_from_1;
    PyObject *tmp_kw_name_1;
    PyObject *tmp_level_name_1;
    PyObject *tmp_level_name_2;
    PyObject *tmp_locals_name_1;
    PyObject *tmp_locals_name_2;
    PyObject *tmp_name_name_1;
    PyObject *tmp_name_name_2;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    PyObject *tmp_tuple_element_1;
    NUITKA_MAY_BE_UNUSED PyObject *tmp_unused;
    static struct Nuitka_FrameObject *cache_frame_ad542ce37db5ba35b474700ec3f27ffb = NULL;

    struct Nuitka_FrameObject *frame_ad542ce37db5ba35b474700ec3f27ffb;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_ad542ce37db5ba35b474700ec3f27ffb, codeobj_ad542ce37db5ba35b474700ec3f27ffb, module_pandas$io$api, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_ad542ce37db5ba35b474700ec3f27ffb = cache_frame_ad542ce37db5ba35b474700ec3f27ffb;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_ad542ce37db5ba35b474700ec3f27ffb );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_ad542ce37db5ba35b474700ec3f27ffb ) == 2 ); // Frame stack

    // Framed code:
    tmp_name_name_1 = const_str_plain_warnings;
    tmp_globals_name_1 = (PyObject *)moduledict_pandas$io$api;
    tmp_locals_name_1 = Py_None;
    tmp_fromlist_name_1 = Py_None;
    tmp_level_name_1 = const_int_0;
    frame_ad542ce37db5ba35b474700ec3f27ffb->m_frame.f_lineno = 24;
    tmp_assign_source_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
    if ( tmp_assign_source_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 24;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    assert( var_warnings == NULL );
    var_warnings = tmp_assign_source_1;

    tmp_source_name_1 = var_warnings;

    CHECK_OBJECT( tmp_source_name_1 );
    tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_warn );
    if ( tmp_called_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 26;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    tmp_tuple_element_1 = const_str_digest_2b0756bbaabafd81f71158e90e1a045a;
    tmp_args_name_1 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
    tmp_tuple_element_1 = GET_STRING_DICT_VALUE( moduledict_pandas$io$api, (Nuitka_StringObject *)const_str_plain_FutureWarning );

    if (unlikely( tmp_tuple_element_1 == NULL ))
    {
        tmp_tuple_element_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_FutureWarning );
    }

    if ( tmp_tuple_element_1 == NULL )
    {
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_name_1 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "FutureWarning" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 30;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }

    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_1 );
    tmp_kw_name_1 = PyDict_Copy( const_dict_f154c9a58c9419d7e391901d7b7fe49e );
    frame_ad542ce37db5ba35b474700ec3f27ffb->m_frame.f_lineno = 26;
    tmp_unused = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
    Py_DECREF( tmp_called_name_1 );
    Py_DECREF( tmp_args_name_1 );
    Py_DECREF( tmp_kw_name_1 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 26;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_unused );
    tmp_name_name_2 = const_str_digest_097981268501cd7c6d993b1c3ffe923b;
    tmp_globals_name_2 = (PyObject *)moduledict_pandas$io$api;
    tmp_locals_name_2 = Py_None;
    tmp_fromlist_name_2 = const_tuple_str_plain_Term_tuple;
    tmp_level_name_2 = const_int_0;
    frame_ad542ce37db5ba35b474700ec3f27ffb->m_frame.f_lineno = 31;
    tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
    if ( tmp_import_name_from_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 31;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    tmp_assign_source_2 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_Term );
    Py_DECREF( tmp_import_name_from_1 );
    if ( tmp_assign_source_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 31;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    assert( var_Term == NULL );
    var_Term = tmp_assign_source_2;

    tmp_dircall_arg1_1 = var_Term;

    CHECK_OBJECT( tmp_dircall_arg1_1 );
    tmp_dircall_arg2_1 = par_args;

    if ( tmp_dircall_arg2_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "args" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 32;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }

    tmp_dircall_arg3_1 = par_kwargs;

    if ( tmp_dircall_arg3_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "kwargs" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 32;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }

    Py_INCREF( tmp_dircall_arg1_1 );
    Py_INCREF( tmp_dircall_arg2_1 );
    Py_INCREF( tmp_dircall_arg3_1 );

    {
        PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1};
        tmp_return_value = impl___internal__$$$function_3_complex_call_helper_star_list_star_dict( dir_call_args );
    }
    if ( tmp_return_value == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 32;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ad542ce37db5ba35b474700ec3f27ffb );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_ad542ce37db5ba35b474700ec3f27ffb );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ad542ce37db5ba35b474700ec3f27ffb );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_ad542ce37db5ba35b474700ec3f27ffb, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_ad542ce37db5ba35b474700ec3f27ffb->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_ad542ce37db5ba35b474700ec3f27ffb, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_ad542ce37db5ba35b474700ec3f27ffb,
        type_description_1,
        par_args,
        par_kwargs,
        var_warnings,
        var_Term
    );


    // Release cached frame.
    if ( frame_ad542ce37db5ba35b474700ec3f27ffb == cache_frame_ad542ce37db5ba35b474700ec3f27ffb )
    {
        Py_DECREF( frame_ad542ce37db5ba35b474700ec3f27ffb );
    }
    cache_frame_ad542ce37db5ba35b474700ec3f27ffb = NULL;

    assertFrameObject( frame_ad542ce37db5ba35b474700ec3f27ffb );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$io$api$$$function_1_Term );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_args );
    par_args = NULL;

    Py_XDECREF( par_kwargs );
    par_kwargs = NULL;

    Py_XDECREF( var_warnings );
    var_warnings = NULL;

    Py_XDECREF( var_Term );
    var_Term = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_args );
    par_args = NULL;

    Py_XDECREF( par_kwargs );
    par_kwargs = NULL;

    Py_XDECREF( var_warnings );
    var_warnings = NULL;

    Py_XDECREF( var_Term );
    var_Term = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$io$api$$$function_1_Term );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}



static PyObject *MAKE_FUNCTION_pandas$io$api$$$function_1_Term(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$io$api$$$function_1_Term,
        const_str_plain_Term,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_ad542ce37db5ba35b474700ec3f27ffb,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$io$api,
        Py_None,
        0
    );


    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_pandas$io$api =
{
    PyModuleDef_HEAD_INIT,
    "pandas.io.api",   /* m_name */
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;

extern PyObject *const_str_plain___loader__;
extern PyObject *metapath_based_loader;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL.

MOD_INIT_DECL( pandas$io$api )
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_pandas$io$api );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("pandas.io.api: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("pandas.io.api: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("pandas.io.api: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initpandas$io$api" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_pandas$io$api = Py_InitModule4(
        "pandas.io.api",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_pandas$io$api = PyModule_Create( &mdef_pandas$io$api );
#endif

    moduledict_pandas$io$api = MODULE_DICT( module_pandas$io$api );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_pandas$io$api, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_pandas$io$api,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );

#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_pandas$io$api, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_pandas$io$api,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_pandas$io$api, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_pandas$io$api,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_pandas$io$api );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PySys_GetObject( (char *)"modules" ), const_str_digest_9bdf56c59c2f38d290ad60074cc357bc, module_pandas$io$api );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_pandas$io$api, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_pandas$io$api, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_pandas$io$api, (Nuitka_StringObject *)const_str_plain___loader__, metapath_based_loader );
#endif

#if PYTHON_VERSION >= 340
#if 0
    UPDATE_STRING_DICT0( moduledict_pandas$io$api, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_pandas$io$api, (Nuitka_StringObject *)const_str_plain___name__ ),
            metapath_based_loader
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );

        UPDATE_STRING_DICT1( moduledict_pandas$io$api, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );

        Py_DECREF( module_spec_class );
    }
#endif
#endif


    // Temp variables if any
    PyObject *tmp_import_from_1__module = NULL;
    PyObject *tmp_import_from_2__module = NULL;
    PyObject *tmp_import_from_3__module = NULL;
    PyObject *tmp_import_from_4__module = NULL;
    PyObject *tmp_import_from_5__module = NULL;
    PyObject *tmp_import_from_6__module = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_assign_source_5;
    PyObject *tmp_assign_source_6;
    PyObject *tmp_assign_source_7;
    PyObject *tmp_assign_source_8;
    PyObject *tmp_assign_source_9;
    PyObject *tmp_assign_source_10;
    PyObject *tmp_assign_source_11;
    PyObject *tmp_assign_source_12;
    PyObject *tmp_assign_source_13;
    PyObject *tmp_assign_source_14;
    PyObject *tmp_assign_source_15;
    PyObject *tmp_assign_source_16;
    PyObject *tmp_assign_source_17;
    PyObject *tmp_assign_source_18;
    PyObject *tmp_assign_source_19;
    PyObject *tmp_assign_source_20;
    PyObject *tmp_assign_source_21;
    PyObject *tmp_assign_source_22;
    PyObject *tmp_assign_source_23;
    PyObject *tmp_assign_source_24;
    PyObject *tmp_assign_source_25;
    PyObject *tmp_assign_source_26;
    PyObject *tmp_assign_source_27;
    PyObject *tmp_assign_source_28;
    PyObject *tmp_assign_source_29;
    PyObject *tmp_assign_source_30;
    PyObject *tmp_assign_source_31;
    PyObject *tmp_assign_source_32;
    PyObject *tmp_assign_source_33;
    PyObject *tmp_assign_source_34;
    PyObject *tmp_fromlist_name_1;
    PyObject *tmp_fromlist_name_2;
    PyObject *tmp_fromlist_name_3;
    PyObject *tmp_fromlist_name_4;
    PyObject *tmp_fromlist_name_5;
    PyObject *tmp_fromlist_name_6;
    PyObject *tmp_fromlist_name_7;
    PyObject *tmp_fromlist_name_8;
    PyObject *tmp_fromlist_name_9;
    PyObject *tmp_fromlist_name_10;
    PyObject *tmp_fromlist_name_11;
    PyObject *tmp_fromlist_name_12;
    PyObject *tmp_fromlist_name_13;
    PyObject *tmp_fromlist_name_14;
    PyObject *tmp_globals_name_1;
    PyObject *tmp_globals_name_2;
    PyObject *tmp_globals_name_3;
    PyObject *tmp_globals_name_4;
    PyObject *tmp_globals_name_5;
    PyObject *tmp_globals_name_6;
    PyObject *tmp_globals_name_7;
    PyObject *tmp_globals_name_8;
    PyObject *tmp_globals_name_9;
    PyObject *tmp_globals_name_10;
    PyObject *tmp_globals_name_11;
    PyObject *tmp_globals_name_12;
    PyObject *tmp_globals_name_13;
    PyObject *tmp_globals_name_14;
    PyObject *tmp_import_name_from_1;
    PyObject *tmp_import_name_from_2;
    PyObject *tmp_import_name_from_3;
    PyObject *tmp_import_name_from_4;
    PyObject *tmp_import_name_from_5;
    PyObject *tmp_import_name_from_6;
    PyObject *tmp_import_name_from_7;
    PyObject *tmp_import_name_from_8;
    PyObject *tmp_import_name_from_9;
    PyObject *tmp_import_name_from_10;
    PyObject *tmp_import_name_from_11;
    PyObject *tmp_import_name_from_12;
    PyObject *tmp_import_name_from_13;
    PyObject *tmp_import_name_from_14;
    PyObject *tmp_import_name_from_15;
    PyObject *tmp_import_name_from_16;
    PyObject *tmp_import_name_from_17;
    PyObject *tmp_import_name_from_18;
    PyObject *tmp_import_name_from_19;
    PyObject *tmp_import_name_from_20;
    PyObject *tmp_import_name_from_21;
    PyObject *tmp_import_name_from_22;
    PyObject *tmp_import_name_from_23;
    PyObject *tmp_import_name_from_24;
    PyObject *tmp_level_name_1;
    PyObject *tmp_level_name_2;
    PyObject *tmp_level_name_3;
    PyObject *tmp_level_name_4;
    PyObject *tmp_level_name_5;
    PyObject *tmp_level_name_6;
    PyObject *tmp_level_name_7;
    PyObject *tmp_level_name_8;
    PyObject *tmp_level_name_9;
    PyObject *tmp_level_name_10;
    PyObject *tmp_level_name_11;
    PyObject *tmp_level_name_12;
    PyObject *tmp_level_name_13;
    PyObject *tmp_level_name_14;
    PyObject *tmp_locals_name_1;
    PyObject *tmp_locals_name_2;
    PyObject *tmp_locals_name_3;
    PyObject *tmp_locals_name_4;
    PyObject *tmp_locals_name_5;
    PyObject *tmp_locals_name_6;
    PyObject *tmp_locals_name_7;
    PyObject *tmp_locals_name_8;
    PyObject *tmp_locals_name_9;
    PyObject *tmp_locals_name_10;
    PyObject *tmp_locals_name_11;
    PyObject *tmp_locals_name_12;
    PyObject *tmp_locals_name_13;
    PyObject *tmp_locals_name_14;
    PyObject *tmp_name_name_1;
    PyObject *tmp_name_name_2;
    PyObject *tmp_name_name_3;
    PyObject *tmp_name_name_4;
    PyObject *tmp_name_name_5;
    PyObject *tmp_name_name_6;
    PyObject *tmp_name_name_7;
    PyObject *tmp_name_name_8;
    PyObject *tmp_name_name_9;
    PyObject *tmp_name_name_10;
    PyObject *tmp_name_name_11;
    PyObject *tmp_name_name_12;
    PyObject *tmp_name_name_13;
    PyObject *tmp_name_name_14;
    struct Nuitka_FrameObject *frame_cf67d5d3a7658e7d99409eae54dfa575;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;

    // Module code.
    tmp_assign_source_1 = const_str_digest_86b450487c15a00df34de4b82ad46280;
    UPDATE_STRING_DICT0( moduledict_pandas$io$api, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    tmp_assign_source_2 = const_str_digest_2c2de31924a351392c0e38801a650d0e;
    UPDATE_STRING_DICT0( moduledict_pandas$io$api, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    tmp_assign_source_3 = Py_None;
    UPDATE_STRING_DICT0( moduledict_pandas$io$api, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    // Frame without reuse.
    frame_cf67d5d3a7658e7d99409eae54dfa575 = MAKE_MODULE_FRAME( codeobj_cf67d5d3a7658e7d99409eae54dfa575, module_pandas$io$api );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_cf67d5d3a7658e7d99409eae54dfa575 );
    assert( Py_REFCNT( frame_cf67d5d3a7658e7d99409eae54dfa575 ) == 2 );

    // Framed code:
    tmp_name_name_1 = const_str_digest_a3503bdf669fdda45ff28f9aaf94e14f;
    tmp_globals_name_1 = (PyObject *)moduledict_pandas$io$api;
    tmp_locals_name_1 = Py_None;
    tmp_fromlist_name_1 = const_tuple_str_plain_read_csv_str_plain_read_table_str_plain_read_fwf_tuple;
    tmp_level_name_1 = const_int_0;
    frame_cf67d5d3a7658e7d99409eae54dfa575->m_frame.f_lineno = 7;
    tmp_assign_source_4 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
    if ( tmp_assign_source_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 7;

        goto frame_exception_exit_1;
    }
    assert( tmp_import_from_1__module == NULL );
    tmp_import_from_1__module = tmp_assign_source_4;

    // Tried code:
    tmp_import_name_from_1 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_1 );
    tmp_assign_source_5 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_read_csv );
    if ( tmp_assign_source_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 7;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$io$api, (Nuitka_StringObject *)const_str_plain_read_csv, tmp_assign_source_5 );
    tmp_import_name_from_2 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_2 );
    tmp_assign_source_6 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_read_table );
    if ( tmp_assign_source_6 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 7;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$io$api, (Nuitka_StringObject *)const_str_plain_read_table, tmp_assign_source_6 );
    tmp_import_name_from_3 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_3 );
    tmp_assign_source_7 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_read_fwf );
    if ( tmp_assign_source_7 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 7;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$io$api, (Nuitka_StringObject *)const_str_plain_read_fwf, tmp_assign_source_7 );
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    tmp_name_name_2 = const_str_digest_643687ca524b713c08bac0e0fb284af6;
    tmp_globals_name_2 = (PyObject *)moduledict_pandas$io$api;
    tmp_locals_name_2 = Py_None;
    tmp_fromlist_name_2 = const_tuple_str_plain_read_clipboard_tuple;
    tmp_level_name_2 = const_int_0;
    frame_cf67d5d3a7658e7d99409eae54dfa575->m_frame.f_lineno = 8;
    tmp_import_name_from_4 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
    if ( tmp_import_name_from_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 8;

        goto frame_exception_exit_1;
    }
    tmp_assign_source_8 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_read_clipboard );
    Py_DECREF( tmp_import_name_from_4 );
    if ( tmp_assign_source_8 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 8;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$io$api, (Nuitka_StringObject *)const_str_plain_read_clipboard, tmp_assign_source_8 );
    tmp_name_name_3 = const_str_digest_e69d58d9e2862e786df4b7a1d941b057;
    tmp_globals_name_3 = (PyObject *)moduledict_pandas$io$api;
    tmp_locals_name_3 = Py_None;
    tmp_fromlist_name_3 = const_tuple_d0542d5a6fa90cc1d7467797c56b6e80_tuple;
    tmp_level_name_3 = const_int_0;
    frame_cf67d5d3a7658e7d99409eae54dfa575->m_frame.f_lineno = 9;
    tmp_assign_source_9 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
    if ( tmp_assign_source_9 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 9;

        goto frame_exception_exit_1;
    }
    assert( tmp_import_from_2__module == NULL );
    tmp_import_from_2__module = tmp_assign_source_9;

    // Tried code:
    tmp_import_name_from_5 = tmp_import_from_2__module;

    CHECK_OBJECT( tmp_import_name_from_5 );
    tmp_assign_source_10 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_ExcelFile );
    if ( tmp_assign_source_10 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 9;

        goto try_except_handler_2;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$io$api, (Nuitka_StringObject *)const_str_plain_ExcelFile, tmp_assign_source_10 );
    tmp_import_name_from_6 = tmp_import_from_2__module;

    CHECK_OBJECT( tmp_import_name_from_6 );
    tmp_assign_source_11 = IMPORT_NAME( tmp_import_name_from_6, const_str_plain_ExcelWriter );
    if ( tmp_assign_source_11 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 9;

        goto try_except_handler_2;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$io$api, (Nuitka_StringObject *)const_str_plain_ExcelWriter, tmp_assign_source_11 );
    tmp_import_name_from_7 = tmp_import_from_2__module;

    CHECK_OBJECT( tmp_import_name_from_7 );
    tmp_assign_source_12 = IMPORT_NAME( tmp_import_name_from_7, const_str_plain_read_excel );
    if ( tmp_assign_source_12 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 9;

        goto try_except_handler_2;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$io$api, (Nuitka_StringObject *)const_str_plain_read_excel, tmp_assign_source_12 );
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    tmp_name_name_4 = const_str_digest_097981268501cd7c6d993b1c3ffe923b;
    tmp_globals_name_4 = (PyObject *)moduledict_pandas$io$api;
    tmp_locals_name_4 = Py_None;
    tmp_fromlist_name_4 = const_tuple_str_plain_HDFStore_str_plain_get_store_str_plain_read_hdf_tuple;
    tmp_level_name_4 = const_int_0;
    frame_cf67d5d3a7658e7d99409eae54dfa575->m_frame.f_lineno = 10;
    tmp_assign_source_13 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
    if ( tmp_assign_source_13 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 10;

        goto frame_exception_exit_1;
    }
    assert( tmp_import_from_3__module == NULL );
    tmp_import_from_3__module = tmp_assign_source_13;

    // Tried code:
    tmp_import_name_from_8 = tmp_import_from_3__module;

    CHECK_OBJECT( tmp_import_name_from_8 );
    tmp_assign_source_14 = IMPORT_NAME( tmp_import_name_from_8, const_str_plain_HDFStore );
    if ( tmp_assign_source_14 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 10;

        goto try_except_handler_3;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$io$api, (Nuitka_StringObject *)const_str_plain_HDFStore, tmp_assign_source_14 );
    tmp_import_name_from_9 = tmp_import_from_3__module;

    CHECK_OBJECT( tmp_import_name_from_9 );
    tmp_assign_source_15 = IMPORT_NAME( tmp_import_name_from_9, const_str_plain_get_store );
    if ( tmp_assign_source_15 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 10;

        goto try_except_handler_3;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$io$api, (Nuitka_StringObject *)const_str_plain_get_store, tmp_assign_source_15 );
    tmp_import_name_from_10 = tmp_import_from_3__module;

    CHECK_OBJECT( tmp_import_name_from_10 );
    tmp_assign_source_16 = IMPORT_NAME( tmp_import_name_from_10, const_str_plain_read_hdf );
    if ( tmp_assign_source_16 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 10;

        goto try_except_handler_3;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$io$api, (Nuitka_StringObject *)const_str_plain_read_hdf, tmp_assign_source_16 );
    goto try_end_3;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_3__module );
    Py_DECREF( tmp_import_from_3__module );
    tmp_import_from_3__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_3__module );
    Py_DECREF( tmp_import_from_3__module );
    tmp_import_from_3__module = NULL;

    tmp_name_name_5 = const_str_digest_c598ce781452927f25bd06a8898d5075;
    tmp_globals_name_5 = (PyObject *)moduledict_pandas$io$api;
    tmp_locals_name_5 = Py_None;
    tmp_fromlist_name_5 = const_tuple_str_plain_read_json_tuple;
    tmp_level_name_5 = const_int_0;
    frame_cf67d5d3a7658e7d99409eae54dfa575->m_frame.f_lineno = 11;
    tmp_import_name_from_11 = IMPORT_MODULE5( tmp_name_name_5, tmp_globals_name_5, tmp_locals_name_5, tmp_fromlist_name_5, tmp_level_name_5 );
    if ( tmp_import_name_from_11 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 11;

        goto frame_exception_exit_1;
    }
    tmp_assign_source_17 = IMPORT_NAME( tmp_import_name_from_11, const_str_plain_read_json );
    Py_DECREF( tmp_import_name_from_11 );
    if ( tmp_assign_source_17 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 11;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$io$api, (Nuitka_StringObject *)const_str_plain_read_json, tmp_assign_source_17 );
    tmp_name_name_6 = const_str_digest_064a8208bd1386240dc3f245d83d37cd;
    tmp_globals_name_6 = (PyObject *)moduledict_pandas$io$api;
    tmp_locals_name_6 = Py_None;
    tmp_fromlist_name_6 = const_tuple_str_plain_read_html_tuple;
    tmp_level_name_6 = const_int_0;
    frame_cf67d5d3a7658e7d99409eae54dfa575->m_frame.f_lineno = 12;
    tmp_import_name_from_12 = IMPORT_MODULE5( tmp_name_name_6, tmp_globals_name_6, tmp_locals_name_6, tmp_fromlist_name_6, tmp_level_name_6 );
    if ( tmp_import_name_from_12 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 12;

        goto frame_exception_exit_1;
    }
    tmp_assign_source_18 = IMPORT_NAME( tmp_import_name_from_12, const_str_plain_read_html );
    Py_DECREF( tmp_import_name_from_12 );
    if ( tmp_assign_source_18 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 12;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$io$api, (Nuitka_StringObject *)const_str_plain_read_html, tmp_assign_source_18 );
    tmp_name_name_7 = const_str_digest_a40d4ade2992e495604f976c385896dc;
    tmp_globals_name_7 = (PyObject *)moduledict_pandas$io$api;
    tmp_locals_name_7 = Py_None;
    tmp_fromlist_name_7 = const_tuple_239c3ca5f939d96b38a434c5a290d647_tuple;
    tmp_level_name_7 = const_int_0;
    frame_cf67d5d3a7658e7d99409eae54dfa575->m_frame.f_lineno = 13;
    tmp_assign_source_19 = IMPORT_MODULE5( tmp_name_name_7, tmp_globals_name_7, tmp_locals_name_7, tmp_fromlist_name_7, tmp_level_name_7 );
    if ( tmp_assign_source_19 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 13;

        goto frame_exception_exit_1;
    }
    assert( tmp_import_from_4__module == NULL );
    tmp_import_from_4__module = tmp_assign_source_19;

    // Tried code:
    tmp_import_name_from_13 = tmp_import_from_4__module;

    CHECK_OBJECT( tmp_import_name_from_13 );
    tmp_assign_source_20 = IMPORT_NAME( tmp_import_name_from_13, const_str_plain_read_sql );
    if ( tmp_assign_source_20 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 13;

        goto try_except_handler_4;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$io$api, (Nuitka_StringObject *)const_str_plain_read_sql, tmp_assign_source_20 );
    tmp_import_name_from_14 = tmp_import_from_4__module;

    CHECK_OBJECT( tmp_import_name_from_14 );
    tmp_assign_source_21 = IMPORT_NAME( tmp_import_name_from_14, const_str_plain_read_sql_table );
    if ( tmp_assign_source_21 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 13;

        goto try_except_handler_4;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$io$api, (Nuitka_StringObject *)const_str_plain_read_sql_table, tmp_assign_source_21 );
    tmp_import_name_from_15 = tmp_import_from_4__module;

    CHECK_OBJECT( tmp_import_name_from_15 );
    tmp_assign_source_22 = IMPORT_NAME( tmp_import_name_from_15, const_str_plain_read_sql_query );
    if ( tmp_assign_source_22 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 13;

        goto try_except_handler_4;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$io$api, (Nuitka_StringObject *)const_str_plain_read_sql_query, tmp_assign_source_22 );
    goto try_end_4;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_4__module );
    Py_DECREF( tmp_import_from_4__module );
    tmp_import_from_4__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto frame_exception_exit_1;
    // End of try:
    try_end_4:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_4__module );
    Py_DECREF( tmp_import_from_4__module );
    tmp_import_from_4__module = NULL;

    tmp_name_name_8 = const_str_digest_604007273228d149e49d492f15420598;
    tmp_globals_name_8 = (PyObject *)moduledict_pandas$io$api;
    tmp_locals_name_8 = Py_None;
    tmp_fromlist_name_8 = const_tuple_str_plain_read_sas_tuple;
    tmp_level_name_8 = const_int_0;
    frame_cf67d5d3a7658e7d99409eae54dfa575->m_frame.f_lineno = 14;
    tmp_import_name_from_16 = IMPORT_MODULE5( tmp_name_name_8, tmp_globals_name_8, tmp_locals_name_8, tmp_fromlist_name_8, tmp_level_name_8 );
    if ( tmp_import_name_from_16 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 14;

        goto frame_exception_exit_1;
    }
    tmp_assign_source_23 = IMPORT_NAME( tmp_import_name_from_16, const_str_plain_read_sas );
    Py_DECREF( tmp_import_name_from_16 );
    if ( tmp_assign_source_23 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 14;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$io$api, (Nuitka_StringObject *)const_str_plain_read_sas, tmp_assign_source_23 );
    tmp_name_name_9 = const_str_digest_094d97527936a6e3d816e81d57f7f99a;
    tmp_globals_name_9 = (PyObject *)moduledict_pandas$io$api;
    tmp_locals_name_9 = Py_None;
    tmp_fromlist_name_9 = const_tuple_str_plain_read_feather_tuple;
    tmp_level_name_9 = const_int_0;
    frame_cf67d5d3a7658e7d99409eae54dfa575->m_frame.f_lineno = 15;
    tmp_import_name_from_17 = IMPORT_MODULE5( tmp_name_name_9, tmp_globals_name_9, tmp_locals_name_9, tmp_fromlist_name_9, tmp_level_name_9 );
    if ( tmp_import_name_from_17 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 15;

        goto frame_exception_exit_1;
    }
    tmp_assign_source_24 = IMPORT_NAME( tmp_import_name_from_17, const_str_plain_read_feather );
    Py_DECREF( tmp_import_name_from_17 );
    if ( tmp_assign_source_24 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 15;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$io$api, (Nuitka_StringObject *)const_str_plain_read_feather, tmp_assign_source_24 );
    tmp_name_name_10 = const_str_digest_1b409ad5171b43d7f68abd8a3582a15b;
    tmp_globals_name_10 = (PyObject *)moduledict_pandas$io$api;
    tmp_locals_name_10 = Py_None;
    tmp_fromlist_name_10 = const_tuple_str_plain_read_parquet_tuple;
    tmp_level_name_10 = const_int_0;
    frame_cf67d5d3a7658e7d99409eae54dfa575->m_frame.f_lineno = 16;
    tmp_import_name_from_18 = IMPORT_MODULE5( tmp_name_name_10, tmp_globals_name_10, tmp_locals_name_10, tmp_fromlist_name_10, tmp_level_name_10 );
    if ( tmp_import_name_from_18 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 16;

        goto frame_exception_exit_1;
    }
    tmp_assign_source_25 = IMPORT_NAME( tmp_import_name_from_18, const_str_plain_read_parquet );
    Py_DECREF( tmp_import_name_from_18 );
    if ( tmp_assign_source_25 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 16;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$io$api, (Nuitka_StringObject *)const_str_plain_read_parquet, tmp_assign_source_25 );
    tmp_name_name_11 = const_str_digest_bd21952672e0a4be57b7e4bb75aad0fc;
    tmp_globals_name_11 = (PyObject *)moduledict_pandas$io$api;
    tmp_locals_name_11 = Py_None;
    tmp_fromlist_name_11 = const_tuple_str_plain_read_stata_tuple;
    tmp_level_name_11 = const_int_0;
    frame_cf67d5d3a7658e7d99409eae54dfa575->m_frame.f_lineno = 17;
    tmp_import_name_from_19 = IMPORT_MODULE5( tmp_name_name_11, tmp_globals_name_11, tmp_locals_name_11, tmp_fromlist_name_11, tmp_level_name_11 );
    if ( tmp_import_name_from_19 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 17;

        goto frame_exception_exit_1;
    }
    tmp_assign_source_26 = IMPORT_NAME( tmp_import_name_from_19, const_str_plain_read_stata );
    Py_DECREF( tmp_import_name_from_19 );
    if ( tmp_assign_source_26 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 17;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$io$api, (Nuitka_StringObject *)const_str_plain_read_stata, tmp_assign_source_26 );
    tmp_name_name_12 = const_str_digest_7a1fc9bd9faaac9777b29fc3d69d3c1d;
    tmp_globals_name_12 = (PyObject *)moduledict_pandas$io$api;
    tmp_locals_name_12 = Py_None;
    tmp_fromlist_name_12 = const_tuple_str_plain_read_pickle_str_plain_to_pickle_tuple;
    tmp_level_name_12 = const_int_0;
    frame_cf67d5d3a7658e7d99409eae54dfa575->m_frame.f_lineno = 18;
    tmp_assign_source_27 = IMPORT_MODULE5( tmp_name_name_12, tmp_globals_name_12, tmp_locals_name_12, tmp_fromlist_name_12, tmp_level_name_12 );
    if ( tmp_assign_source_27 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 18;

        goto frame_exception_exit_1;
    }
    assert( tmp_import_from_5__module == NULL );
    tmp_import_from_5__module = tmp_assign_source_27;

    // Tried code:
    tmp_import_name_from_20 = tmp_import_from_5__module;

    CHECK_OBJECT( tmp_import_name_from_20 );
    tmp_assign_source_28 = IMPORT_NAME( tmp_import_name_from_20, const_str_plain_read_pickle );
    if ( tmp_assign_source_28 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 18;

        goto try_except_handler_5;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$io$api, (Nuitka_StringObject *)const_str_plain_read_pickle, tmp_assign_source_28 );
    tmp_import_name_from_21 = tmp_import_from_5__module;

    CHECK_OBJECT( tmp_import_name_from_21 );
    tmp_assign_source_29 = IMPORT_NAME( tmp_import_name_from_21, const_str_plain_to_pickle );
    if ( tmp_assign_source_29 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 18;

        goto try_except_handler_5;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$io$api, (Nuitka_StringObject *)const_str_plain_to_pickle, tmp_assign_source_29 );
    goto try_end_5;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_5__module );
    Py_DECREF( tmp_import_from_5__module );
    tmp_import_from_5__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto frame_exception_exit_1;
    // End of try:
    try_end_5:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_5__module );
    Py_DECREF( tmp_import_from_5__module );
    tmp_import_from_5__module = NULL;

    tmp_name_name_13 = const_str_digest_8717a5f4bc167d7ea49925b7dac16f2a;
    tmp_globals_name_13 = (PyObject *)moduledict_pandas$io$api;
    tmp_locals_name_13 = Py_None;
    tmp_fromlist_name_13 = const_tuple_str_plain_read_msgpack_str_plain_to_msgpack_tuple;
    tmp_level_name_13 = const_int_0;
    frame_cf67d5d3a7658e7d99409eae54dfa575->m_frame.f_lineno = 19;
    tmp_assign_source_30 = IMPORT_MODULE5( tmp_name_name_13, tmp_globals_name_13, tmp_locals_name_13, tmp_fromlist_name_13, tmp_level_name_13 );
    if ( tmp_assign_source_30 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 19;

        goto frame_exception_exit_1;
    }
    assert( tmp_import_from_6__module == NULL );
    tmp_import_from_6__module = tmp_assign_source_30;

    // Tried code:
    tmp_import_name_from_22 = tmp_import_from_6__module;

    CHECK_OBJECT( tmp_import_name_from_22 );
    tmp_assign_source_31 = IMPORT_NAME( tmp_import_name_from_22, const_str_plain_read_msgpack );
    if ( tmp_assign_source_31 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 19;

        goto try_except_handler_6;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$io$api, (Nuitka_StringObject *)const_str_plain_read_msgpack, tmp_assign_source_31 );
    tmp_import_name_from_23 = tmp_import_from_6__module;

    CHECK_OBJECT( tmp_import_name_from_23 );
    tmp_assign_source_32 = IMPORT_NAME( tmp_import_name_from_23, const_str_plain_to_msgpack );
    if ( tmp_assign_source_32 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 19;

        goto try_except_handler_6;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$io$api, (Nuitka_StringObject *)const_str_plain_to_msgpack, tmp_assign_source_32 );
    goto try_end_6;
    // Exception handler code:
    try_except_handler_6:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_keeper_lineno_6 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_6__module );
    Py_DECREF( tmp_import_from_6__module );
    tmp_import_from_6__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_6;
    exception_value = exception_keeper_value_6;
    exception_tb = exception_keeper_tb_6;
    exception_lineno = exception_keeper_lineno_6;

    goto frame_exception_exit_1;
    // End of try:
    try_end_6:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_6__module );
    Py_DECREF( tmp_import_from_6__module );
    tmp_import_from_6__module = NULL;

    tmp_name_name_14 = const_str_digest_75090e6ed0be6fa03614e573a9d579f0;
    tmp_globals_name_14 = (PyObject *)moduledict_pandas$io$api;
    tmp_locals_name_14 = Py_None;
    tmp_fromlist_name_14 = const_tuple_str_plain_read_gbq_tuple;
    tmp_level_name_14 = const_int_0;
    frame_cf67d5d3a7658e7d99409eae54dfa575->m_frame.f_lineno = 20;
    tmp_import_name_from_24 = IMPORT_MODULE5( tmp_name_name_14, tmp_globals_name_14, tmp_locals_name_14, tmp_fromlist_name_14, tmp_level_name_14 );
    if ( tmp_import_name_from_24 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 20;

        goto frame_exception_exit_1;
    }
    tmp_assign_source_33 = IMPORT_NAME( tmp_import_name_from_24, const_str_plain_read_gbq );
    Py_DECREF( tmp_import_name_from_24 );
    if ( tmp_assign_source_33 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 20;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$io$api, (Nuitka_StringObject *)const_str_plain_read_gbq, tmp_assign_source_33 );

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_cf67d5d3a7658e7d99409eae54dfa575 );
#endif
    popFrameStack();

    assertFrameObject( frame_cf67d5d3a7658e7d99409eae54dfa575 );

    goto frame_no_exception_1;
    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_cf67d5d3a7658e7d99409eae54dfa575 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_cf67d5d3a7658e7d99409eae54dfa575, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_cf67d5d3a7658e7d99409eae54dfa575->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_cf67d5d3a7658e7d99409eae54dfa575, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;
    frame_no_exception_1:;
    tmp_assign_source_34 = MAKE_FUNCTION_pandas$io$api$$$function_1_Term(  );
    UPDATE_STRING_DICT1( moduledict_pandas$io$api, (Nuitka_StringObject *)const_str_plain_Term, tmp_assign_source_34 );

    return MOD_RETURN_VALUE( module_pandas$io$api );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
