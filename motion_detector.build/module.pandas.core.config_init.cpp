/* Generated code for Python source for module 'pandas.core.config_init'
 * created by Nuitka version 0.5.32.7
 *
 * This code is in part copyright 2018 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The _module_pandas$core$config_init is a Python object pointer of module type. */

/* Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_pandas$core$config_init;
PyDictObject *moduledict_pandas$core$config_init;

/* The module constants used, if any. */
extern PyObject *const_str_plain_cb;
static PyObject *const_str_digest_9670544456fe8ec48aad3cc3f43bc6df;
static PyObject *const_str_plain_pc_latex_multirow;
extern PyObject *const_str_plain_warn;
static PyObject *const_str_digest_4497acf6863e0333fbe35765df4bdb52;
extern PyObject *const_str_plain_border;
extern PyObject *const_int_pos_12;
static PyObject *const_str_plain_multi_sparse;
static PyObject *const_str_digest_3b2c0193880aff67a5404a80d2c4fce1;
extern PyObject *const_str_plain_max_categories;
extern PyObject *const_int_pos_20;
static PyObject *const_str_digest_7b040172b3c9edf5c62359d0376f0338;
static PyObject *const_str_digest_0fe0d9af171687ff64bb6c40c034c136;
static PyObject *const_tuple_str_digest_dcbf3c6268caaf75da6d6de7bfa9dfac_tuple;
static PyObject *const_tuple_str_plain_nanops_tuple;
extern PyObject *const_str_plain_is_instance_factory;
extern PyObject *const_str_plain_use_numexpr;
static PyObject *const_tuple_str_plain_compute_tuple;
static PyObject *const_str_digest_3b01a421e528fddeffa2f6f36e25f610;
static PyObject *const_str_plain_pc_encoding_doc;
extern PyObject *const_str_plain_get_option;
extern PyObject *const_str_plain_precision;
static PyObject *const_list_str_plain_openpyxl_list;
static PyObject *const_str_digest_42083bc4d862dcb2ba3d5c3079c8b46e;
extern PyObject *const_str_plain_is_terminal;
static PyObject *const_str_digest_99cdadd45b47f7b22a2c379861d1710a;
extern PyObject *const_str_plain___file__;
static PyObject *const_str_digest_dfed39f8c622349086edb057745e7a7c;
static PyObject *const_str_digest_d81e5a583e51219530e4aa7bf1ec93ce;
static PyObject *const_list_str_plain_openpyxl_str_plain_xlsxwriter_list;
static PyObject *const_tuple_str_digest_3c0f1fdaa71bdfe9f292a45ca9e89145_tuple;
extern PyObject *const_str_plain_bool;
extern PyObject *const_str_plain___exit__;
static PyObject *const_str_plain_writer_engine_doc;
static PyObject *const_str_plain_register_converter_cb;
static PyObject *const_tuple_str_plain_display_tuple;
extern PyObject *const_str_plain_xlsm;
static PyObject *const_str_plain_use_bottleneck;
static PyObject *const_str_digest_b3457d17b246c509349a6a956ce44332;
static PyObject *const_str_plain_use_inf_as_na_cb;
static PyObject *const_str_digest_ff62b92b31818043edce46d1900d7de3;
static PyObject *const_tuple_str_plain__use_inf_as_na_tuple;
extern PyObject *const_str_plain_compute;
extern PyObject *const_str_plain_raise;
static PyObject *const_str_digest_692d141c202ce579d6e235169c8eaa7b;
static PyObject *const_str_plain_pc_max_seq_items;
static PyObject *const_str_plain_pc_width_doc;
extern PyObject *const_str_plain__use_inf_as_na;
extern PyObject *const_str_plain_mode;
extern PyObject *const_int_pos_80;
static PyObject *const_tuple_str_plain_deregister_matplotlib_converters_tuple;
extern PyObject *const_int_pos_6;
extern PyObject *const_str_plain_None;
static PyObject *const_str_plain_pc_ambiguous_as_wide_doc;
static PyObject *const_str_plain_pc_multi_sparse_doc;
static PyObject *const_tuple_str_plain_detect_console_encoding_tuple;
extern PyObject *const_str_plain___enter__;
static PyObject *const_str_plain_pc_latex_longtable;
static PyObject *const_str_plain_use_inf_as_null;
extern PyObject *const_str_plain_validator;
static PyObject *const_str_plain_pc_latex_multicolumn;
extern PyObject *const_str_plain_deregister_matplotlib_converters;
extern PyObject *const_str_plain_auto;
static PyObject *const_str_digest_dcbf3c6268caaf75da6d6de7bfa9dfac;
static PyObject *const_str_digest_284e68a8357c0c7572e494cf44721a5a;
extern PyObject *const_str_plain_join;
static PyObject *const_str_plain_parquet_engine_doc;
extern PyObject *const_tuple_str_plain_expressions_tuple;
extern PyObject *const_tuple_none_none_none_tuple;
static PyObject *const_str_digest_7a42d199b93f92d5aefb172b3cfa54dd;
extern PyObject *const_tuple_str_plain_mode_tuple;
static PyObject *const_str_plain_max_info_columns;
static PyObject *const_str_digest_a4c9172024d586d4113a4ddc2c7331e3;
static PyObject *const_str_plain_pc_html_use_mathjax_doc;
extern PyObject *const_str_plain___doc__;
static PyObject *const_str_plain_use_inf_as_na_doc;
static PyObject *const_str_plain_pc_nb_repr_h_doc;
extern PyObject *const_str_plain_is_bool;
static PyObject *const_tuple_str_plain_html_tuple;
static PyObject *const_str_plain_pprint_nest_depth;
static PyObject *const_str_digest_36781382e72dac19c5e46696c90720ae;
extern PyObject *const_str_digest_0b45bd1227a3ca701809b4b137e5400c;
static PyObject *const_list_str_plain_auto_str_plain_pyarrow_str_plain_fastparquet_list;
extern PyObject *const_str_digest_fd66022e234f90372ceae074a8020048;
extern PyObject *const_str_plain___debug__;
extern PyObject *const_str_plain_cf;
static PyObject *const_str_plain_large_repr;
static PyObject *const_str_plain_pc_show_dimensions_doc;
static PyObject *const_str_digest_06816bb44a974157f48e12c6a7055eaa;
extern PyObject *const_str_digest_53eb620cf819e7f2e2a90c9ff72c4e4b;
extern PyObject *const_str_plain_register_matplotlib_converters;
extern PyObject *const_str_plain_str;
static PyObject *const_str_plain_pc_max_info_rows_doc;
static PyObject *const_str_digest_b0e6b6e8f5f54372dfb38c17db742597;
static PyObject *const_str_plain_column_space;
extern PyObject *const_str_plain_ext;
static PyObject *const_str_digest_f2956f16d5c6b095746cfa96ebbd29c0;
static PyObject *const_str_digest_8af9049bb103d0ae35d21227a8a5a762;
extern PyObject *const_str_digest_ac9917bb8c64ab6890c5c907f082236f;
extern PyObject *const_str_plain_int;
extern PyObject *const_str_plain_detect_console_encoding;
static PyObject *const_str_plain__xlsm_options;
extern PyObject *const_str_plain_xlsx;
static PyObject *const_str_digest_162c04eaa4f9de7abebfff3d81422a8b;
extern PyObject *const_str_plain_deep;
static PyObject *const_str_digest_a9c4f8563d1ecedf60bda713bd0b1e2c;
static PyObject *const_str_digest_f9be2e7dbb52eeee952286f86a48c0d1;
static PyObject *const_int_pos_1690785;
static PyObject *const_tuple_tuple_type_int_anon_NoneType_tuple_tuple;
static PyObject *const_str_plain_pc_chop_threshold_doc;
static PyObject *const_str_plain_date_yearfirst;
static PyObject *const_str_plain_register_converter_doc;
static PyObject *const_str_digest_1c14e0e1f14a8db299372128951bac65;
static PyObject *const_tuple_str_plain_key_str_plain__use_inf_as_na_tuple;
static PyObject *const_str_digest_3c0f1fdaa71bdfe9f292a45ca9e89145;
extern PyObject *const_str_plain__enable_data_resource_formatter;
static PyObject *const_tuple_str_plain__enable_data_resource_formatter_tuple;
static PyObject *const_str_digest_3ecce95cea7e70b97124a63fdb6f472c;
extern PyObject *const_int_pos_8;
extern PyObject *const_str_plain_engine;
extern PyObject *const_str_plain_xlsxwriter;
extern PyObject *const_str_plain_display;
extern PyObject *const_str_plain_show_dimensions;
static PyObject *const_str_plain_max_info_rows;
extern PyObject *const_str_plain_l;
static PyObject *const_str_plain_pc_colspace_doc;
extern PyObject *const_tuple_empty;
static PyObject *const_str_digest_6de8d02e7df5afc37f0e3f0e946776a1;
static PyObject *const_str_digest_603db2006590fd8b8437f02ab548d669;
static PyObject *const_str_digest_5c883b3797590ee62e4ca0d8acbabdbf;
static PyObject *const_str_plain_pc_latex_repr_doc;
static PyObject *const_str_digest_a8ce4dc3e5952b64cf078ae95e08188a;
extern PyObject *const_str_plain_writer;
static PyObject *const_tuple_str_digest_d81e5a583e51219530e4aa7bf1ec93ce_tuple;
extern PyObject *const_str_digest_b3f0e8a09cdae8f83a14da645f450b0f;
static PyObject *const_str_plain_pc_precision_doc;
static PyObject *const_str_digest_12c6d9d73b32c369db53c25a405c94a9;
static PyObject *const_str_digest_12536abbc33fcdb5edeca9cb579ae639;
extern PyObject *const_str_plain_others;
static PyObject *const_tuple_str_plain_column_space_int_pos_12_tuple;
static PyObject *const_str_digest_c8ce5ba4cfc04c1c564d4d58eee1151a;
static PyObject *const_str_plain_pc_html_border_doc;
static PyObject *const_str_digest_3fed32ca54d55e7419f2cf87ece4440b;
static PyObject *const_list_none_true_false_str_plain_deep_list;
static PyObject *const_str_digest_79b75e74f53a10e8129732de4fc9ea36;
static PyObject *const_str_plain_use_bottleneck_cb;
extern PyObject *const_int_pos_60;
extern PyObject *const_str_plain_is_text;
extern PyObject *const_str_plain_memory_usage;
extern PyObject *const_str_plain_config;
static PyObject *const_str_plain_pc_latex_multicolumn_format;
static PyObject *const_str_digest_92d5fb4be587bc095681b0b7376435ec;
static PyObject *const_str_plain_float_format_doc;
static PyObject *const_str_plain_pc_east_asian_width_doc;
extern PyObject *const_str_plain_max_seq_items;
extern PyObject *const_str_plain_False;
static PyObject *const_str_digest_e6fe352ef36e0bf7027ddc4a9efaf76c;
static PyObject *const_str_plain_pc_date_dayfirst_doc;
extern PyObject *const_str_plain_is_callable;
extern PyObject *const_str_plain_float_format;
extern PyObject *const_str_plain_encoding;
static PyObject *const_str_digest_70761738ff7ce1bdfec83dad7b784939;
static PyObject *const_str_digest_8b6677d435ef234e5391d3dcc8cf1b40;
static PyObject *const_list_str_plain_truncate_str_plain_info_list;
static PyObject *const_tuple_str_plain_register_matplotlib_converters_tuple;
static PyObject *const_list_str_plain_xlwt_list;
static PyObject *const_list_true_false_str_plain_truncate_list;
static PyObject *const_str_digest_9828a77d868bc8c07cee23db35f2d9a5;
extern PyObject *const_str_digest_b0b825b0001352666a073831b2d2ecba;
extern PyObject *const_int_0;
static PyObject *const_str_digest_32d9ee95a950d61124e98e2a12fc0214;
static PyObject *const_tuple_str_digest_42083bc4d862dcb2ba3d5c3079c8b46e_tuple;
static PyObject *const_str_plain_use_numexpr_doc;
extern PyObject *const_str_plain_msg;
static PyObject *const_tuple_type_int_anon_NoneType_tuple;
static PyObject *const_str_digest_45068c861a2fe7b61a5c03d495ca5c0a;
extern PyObject *const_str_plain_deprecate_option;
extern PyObject *const_str_plain_is_int;
static PyObject *const_str_digest_36e3b1f94c51cffa19984b47d24b4246;
extern PyObject *const_str_plain_xls;
extern PyObject *const_str_digest_db35ab94a03c3cbeb13cbe2a1d728b77;
extern PyObject *const_str_plain_max_columns;
static PyObject *const_str_digest_3cce2b8ce49df0213b09bcbcf6ed7cda;
static PyObject *const_str_plain_use_numexpr_cb;
extern PyObject *const_str_plain_core;
extern PyObject *const_str_plain_nanops;
static PyObject *const_str_digest_baeb24505d73a9f85f64f36d492f470b;
static PyObject *const_str_plain_pc_max_cols_doc;
extern PyObject *const_str_plain_right;
extern PyObject *const_dict_6385baa8555cb0cf1caa11af10a4cfce;
static PyObject *const_str_plain_html;
extern PyObject *const_str_digest_7a907ce278e4875739e67044c12dd234;
extern PyObject *const_str_plain_rkey;
extern PyObject *const_str_digest_6083b29174101382b93cdaa563f79a64;
static PyObject *const_str_plain_colheader_justify;
extern PyObject *const_str_plain_config_prefix;
static PyObject *const_list_anon_NoneType_type_int_list;
static PyObject *const_str_plain_use_bottleneck_doc;
static PyObject *const_str_plain_pc_max_info_cols_doc;
extern PyObject *const_int_pos_50;
static PyObject *const_str_digest_8673b45174cf95301c8e18f39b235e8c;
static PyObject *const_str_plain__xlsx_options;
extern PyObject *const_str_digest_48ae02d7bc3fc61cbffb3f46dd54fdfa;
extern PyObject *const_str_plain_openpyxl;
static PyObject *const_str_plain_use_inf_as_null_doc;
static PyObject *const_str_digest_99e766b2632da277c8e75ec5e46abcd7;
static PyObject *const_str_plain_pc_memory_usage_doc;
static PyObject *const_str_digest_35b0c6e3e7281501288a4ab5b486f5d6;
static PyObject *const_str_plain_style_backup;
extern PyObject *const_str_plain_fastparquet;
static PyObject *const_str_plain_pc_table_schema_doc;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_plain_expressions;
static PyObject *const_tuple_str_digest_35b0c6e3e7281501288a4ab5b486f5d6_tuple;
static PyObject *const_str_plain_colheader_justify_doc;
static PyObject *const_str_digest_d640058ba6564339d64044d11762fd11;
extern PyObject *const_str_plain_set_use_bottleneck;
static PyObject *const_str_plain_pc_large_repr_doc;
static PyObject *const_str_digest_55909c86f77471fd84b96a2c560d530c;
static PyObject *const_str_plain_date_dayfirst;
static PyObject *const_str_plain_expand_frame_repr;
static PyObject *const_str_plain_max_colwidth_doc;
static PyObject *const_str_plain_tc_sim_interactive_doc;
static PyObject *const_tuple_d384e95b8089e28989357db5c2fae7f2_tuple;
static PyObject *const_str_digest_f5fa2e7b82b0176dddfc8ec97f30c139;
static PyObject *const_str_digest_d779ffca91b7e173eb78d35afad339b2;
static PyObject *const_list_none_str_plain_warn_str_plain_raise_list;
static PyObject *const_str_digest_86f291ce802a3d5c7c1dc1d9671e3230;
static PyObject *const_tuple_str_digest_12536abbc33fcdb5edeca9cb579ae639_tuple;
static PyObject *const_tuple_str_digest_45068c861a2fe7b61a5c03d495ca5c0a_tuple;
extern PyObject *const_str_plain_xlwt;
extern PyObject *const_int_pos_1;
static PyObject *const_str_digest_2c5f1d42f6beaab0f171385068265a49;
static PyObject *const_str_plain_notebook_repr_html;
static PyObject *const_tuple_str_plain_key_str_plain__enable_data_resource_formatter_tuple;
static PyObject *const_str_plain_pc_max_rows_doc;
extern PyObject *const_str_plain_width;
static PyObject *const_tuple_413ae156d7ae29f4e06579546b354151_tuple;
extern PyObject *const_str_plain_pyarrow;
extern PyObject *const_str_plain_truncate;
extern PyObject *const_str_plain_key;
extern PyObject *const_str_plain_set_use_numexpr;
static PyObject *const_str_digest_25e404ad3ac11a956aa85b1d77b401de;
extern PyObject *const_str_digest_c04039a7e34f9463fede19ff82de69b6;
static PyObject *const_str_digest_0f50b2f82ea6dafb9acb48b11438c59c;
static PyObject *const_str_plain_register_converters;
extern PyObject *const_int_pos_3;
static PyObject *const_tuple_str_plain_key_str_plain_expressions_tuple;
static PyObject *const_str_plain__xls_options;
static PyObject *const_str_plain_pc_expand_repr_doc;
static PyObject *const_str_digest_02315a6b6613311a88485cf6ec10d6ed;
static PyObject *const_str_digest_47930e395de0263309e238bd2d8cce2c;
static PyObject *const_str_plain_use_inf_as_na;
static PyObject *const_str_digest_b4fd9bef99574cf727e466922f2dc8a0;
static PyObject *const_str_plain_pc_latex_escape;
static PyObject *const_str_digest_67ab286b537608a8ed02613094baf20d;
static PyObject *const_str_plain_chop_threshold;
static PyObject *const_tuple_str_plain_key_str_plain_nanops_tuple;
static PyObject *const_str_plain_pc_max_categories_doc;
extern PyObject *const_str_plain_max_rows;
static PyObject *const_str_plain_pc_date_yearfirst_doc;
static PyObject *const_str_digest_efff17427f06ff36726c48901702ae1d;
static PyObject *const_str_plain_pc_html_border_deprecation_warning;
static PyObject *const_str_plain_pc_pprint_nest_depth;
static PyObject *const_tuple_str_plain_is_terminal_tuple;
static PyObject *const_str_plain_chained_assignment;
static PyObject *const_str_plain_sim_interactive;
extern PyObject *const_str_plain_format;
extern PyObject *const_str_plain_info;
static PyObject *const_str_digest_f3097cefcee812f2202499367dd8f891;
static PyObject *const_str_digest_de1533dce7c0c3057803e6914fcf5814;
static PyObject *const_str_plain_table_schema_cb;
extern PyObject *const_str_plain_max_cols;
extern PyObject *const_str_plain_register_option;
extern PyObject *const_str_plain_is_one_of_factory;
extern PyObject *const_int_pos_100;
static PyObject *const_str_plain_max_colwidth;
static PyObject *module_filename_obj;

static bool constants_created = false;

static void createModuleConstants( void )
{
    const_str_digest_9670544456fe8ec48aad3cc3f43bc6df = UNSTREAM_STRING( &constant_bin[ 1852314 ], 165, 0 );
    const_str_plain_pc_latex_multirow = UNSTREAM_STRING( &constant_bin[ 1852479 ], 17, 1 );
    const_str_digest_4497acf6863e0333fbe35765df4bdb52 = UNSTREAM_STRING( &constant_bin[ 1852496 ], 32, 0 );
    const_str_plain_multi_sparse = UNSTREAM_STRING( &constant_bin[ 1852528 ], 12, 1 );
    const_str_digest_3b2c0193880aff67a5404a80d2c4fce1 = UNSTREAM_STRING( &constant_bin[ 1852540 ], 212, 0 );
    const_str_digest_7b040172b3c9edf5c62359d0376f0338 = UNSTREAM_STRING( &constant_bin[ 1852752 ], 152, 0 );
    const_str_digest_0fe0d9af171687ff64bb6c40c034c136 = UNSTREAM_STRING( &constant_bin[ 1852904 ], 135, 0 );
    const_tuple_str_digest_dcbf3c6268caaf75da6d6de7bfa9dfac_tuple = PyTuple_New( 1 );
    const_str_digest_dcbf3c6268caaf75da6d6de7bfa9dfac = UNSTREAM_STRING( &constant_bin[ 1853039 ], 10, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_dcbf3c6268caaf75da6d6de7bfa9dfac_tuple, 0, const_str_digest_dcbf3c6268caaf75da6d6de7bfa9dfac ); Py_INCREF( const_str_digest_dcbf3c6268caaf75da6d6de7bfa9dfac );
    const_tuple_str_plain_nanops_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_nanops_tuple, 0, const_str_plain_nanops ); Py_INCREF( const_str_plain_nanops );
    const_tuple_str_plain_compute_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_compute_tuple, 0, const_str_plain_compute ); Py_INCREF( const_str_plain_compute );
    const_str_digest_3b01a421e528fddeffa2f6f36e25f610 = UNSTREAM_STRING( &constant_bin[ 1853049 ], 85, 0 );
    const_str_plain_pc_encoding_doc = UNSTREAM_STRING( &constant_bin[ 1853134 ], 15, 1 );
    const_list_str_plain_openpyxl_list = PyList_New( 1 );
    PyList_SET_ITEM( const_list_str_plain_openpyxl_list, 0, const_str_plain_openpyxl ); Py_INCREF( const_str_plain_openpyxl );
    const_str_digest_42083bc4d862dcb2ba3d5c3079c8b46e = UNSTREAM_STRING( &constant_bin[ 1853149 ], 11, 0 );
    const_str_digest_99cdadd45b47f7b22a2c379861d1710a = UNSTREAM_STRING( &constant_bin[ 1853160 ], 96, 0 );
    const_str_digest_dfed39f8c622349086edb057745e7a7c = UNSTREAM_STRING( &constant_bin[ 1853256 ], 236, 0 );
    const_str_digest_d81e5a583e51219530e4aa7bf1ec93ce = UNSTREAM_STRING( &constant_bin[ 1853492 ], 13, 0 );
    const_list_str_plain_openpyxl_str_plain_xlsxwriter_list = PyList_New( 2 );
    PyList_SET_ITEM( const_list_str_plain_openpyxl_str_plain_xlsxwriter_list, 0, const_str_plain_openpyxl ); Py_INCREF( const_str_plain_openpyxl );
    PyList_SET_ITEM( const_list_str_plain_openpyxl_str_plain_xlsxwriter_list, 1, const_str_plain_xlsxwriter ); Py_INCREF( const_str_plain_xlsxwriter );
    const_tuple_str_digest_3c0f1fdaa71bdfe9f292a45ca9e89145_tuple = PyTuple_New( 1 );
    const_str_digest_3c0f1fdaa71bdfe9f292a45ca9e89145 = UNSTREAM_STRING( &constant_bin[ 1853505 ], 19, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_3c0f1fdaa71bdfe9f292a45ca9e89145_tuple, 0, const_str_digest_3c0f1fdaa71bdfe9f292a45ca9e89145 ); Py_INCREF( const_str_digest_3c0f1fdaa71bdfe9f292a45ca9e89145 );
    const_str_plain_writer_engine_doc = UNSTREAM_STRING( &constant_bin[ 1853524 ], 17, 1 );
    const_str_plain_register_converter_cb = UNSTREAM_STRING( &constant_bin[ 1853541 ], 21, 1 );
    const_tuple_str_plain_display_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_display_tuple, 0, const_str_plain_display ); Py_INCREF( const_str_plain_display );
    const_str_plain_use_bottleneck = UNSTREAM_STRING( &constant_bin[ 1853562 ], 14, 1 );
    const_str_digest_b3457d17b246c509349a6a956ce44332 = UNSTREAM_STRING( &constant_bin[ 1853576 ], 238, 0 );
    const_str_plain_use_inf_as_na_cb = UNSTREAM_STRING( &constant_bin[ 1853814 ], 16, 1 );
    const_str_digest_ff62b92b31818043edce46d1900d7de3 = UNSTREAM_STRING( &constant_bin[ 1853830 ], 273, 0 );
    const_tuple_str_plain__use_inf_as_na_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain__use_inf_as_na_tuple, 0, const_str_plain__use_inf_as_na ); Py_INCREF( const_str_plain__use_inf_as_na );
    const_str_digest_692d141c202ce579d6e235169c8eaa7b = UNSTREAM_STRING( &constant_bin[ 1854103 ], 96, 0 );
    const_str_plain_pc_max_seq_items = UNSTREAM_STRING( &constant_bin[ 1854199 ], 16, 1 );
    const_str_plain_pc_width_doc = UNSTREAM_STRING( &constant_bin[ 1854215 ], 12, 1 );
    const_tuple_str_plain_deregister_matplotlib_converters_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_deregister_matplotlib_converters_tuple, 0, const_str_plain_deregister_matplotlib_converters ); Py_INCREF( const_str_plain_deregister_matplotlib_converters );
    const_str_plain_pc_ambiguous_as_wide_doc = UNSTREAM_STRING( &constant_bin[ 1854227 ], 24, 1 );
    const_str_plain_pc_multi_sparse_doc = UNSTREAM_STRING( &constant_bin[ 1854251 ], 19, 1 );
    const_tuple_str_plain_detect_console_encoding_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_detect_console_encoding_tuple, 0, const_str_plain_detect_console_encoding ); Py_INCREF( const_str_plain_detect_console_encoding );
    const_str_plain_pc_latex_longtable = UNSTREAM_STRING( &constant_bin[ 1854270 ], 18, 1 );
    const_str_plain_use_inf_as_null = UNSTREAM_STRING( &constant_bin[ 1854288 ], 15, 1 );
    const_str_plain_pc_latex_multicolumn = UNSTREAM_STRING( &constant_bin[ 1854303 ], 20, 1 );
    const_str_digest_284e68a8357c0c7572e494cf44721a5a = UNSTREAM_STRING( &constant_bin[ 1854323 ], 15, 0 );
    const_str_plain_parquet_engine_doc = UNSTREAM_STRING( &constant_bin[ 1854338 ], 18, 1 );
    const_str_digest_7a42d199b93f92d5aefb172b3cfa54dd = UNSTREAM_STRING( &constant_bin[ 1854356 ], 48, 0 );
    const_str_plain_max_info_columns = UNSTREAM_STRING( &constant_bin[ 1854404 ], 16, 1 );
    const_str_digest_a4c9172024d586d4113a4ddc2c7331e3 = UNSTREAM_STRING( &constant_bin[ 1854420 ], 10, 0 );
    const_str_plain_pc_html_use_mathjax_doc = UNSTREAM_STRING( &constant_bin[ 1854430 ], 23, 1 );
    const_str_plain_use_inf_as_na_doc = UNSTREAM_STRING( &constant_bin[ 1854453 ], 17, 1 );
    const_str_plain_pc_nb_repr_h_doc = UNSTREAM_STRING( &constant_bin[ 1854470 ], 16, 1 );
    const_tuple_str_plain_html_tuple = PyTuple_New( 1 );
    const_str_plain_html = UNSTREAM_STRING( &constant_bin[ 12994 ], 4, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_html_tuple, 0, const_str_plain_html ); Py_INCREF( const_str_plain_html );
    const_str_plain_pprint_nest_depth = UNSTREAM_STRING( &constant_bin[ 1854486 ], 17, 1 );
    const_str_digest_36781382e72dac19c5e46696c90720ae = UNSTREAM_STRING( &constant_bin[ 1854503 ], 76, 0 );
    const_list_str_plain_auto_str_plain_pyarrow_str_plain_fastparquet_list = PyList_New( 3 );
    PyList_SET_ITEM( const_list_str_plain_auto_str_plain_pyarrow_str_plain_fastparquet_list, 0, const_str_plain_auto ); Py_INCREF( const_str_plain_auto );
    PyList_SET_ITEM( const_list_str_plain_auto_str_plain_pyarrow_str_plain_fastparquet_list, 1, const_str_plain_pyarrow ); Py_INCREF( const_str_plain_pyarrow );
    PyList_SET_ITEM( const_list_str_plain_auto_str_plain_pyarrow_str_plain_fastparquet_list, 2, const_str_plain_fastparquet ); Py_INCREF( const_str_plain_fastparquet );
    const_str_plain_large_repr = UNSTREAM_STRING( &constant_bin[ 1854579 ], 10, 1 );
    const_str_plain_pc_show_dimensions_doc = UNSTREAM_STRING( &constant_bin[ 1854589 ], 22, 1 );
    const_str_digest_06816bb44a974157f48e12c6a7055eaa = UNSTREAM_STRING( &constant_bin[ 1854611 ], 24, 0 );
    const_str_plain_pc_max_info_rows_doc = UNSTREAM_STRING( &constant_bin[ 1854635 ], 20, 1 );
    const_str_digest_b0e6b6e8f5f54372dfb38c17db742597 = UNSTREAM_STRING( &constant_bin[ 1854655 ], 239, 0 );
    const_str_plain_column_space = UNSTREAM_STRING( &constant_bin[ 1854894 ], 12, 1 );
    const_str_digest_f2956f16d5c6b095746cfa96ebbd29c0 = UNSTREAM_STRING( &constant_bin[ 1854906 ], 89, 0 );
    const_str_digest_8af9049bb103d0ae35d21227a8a5a762 = UNSTREAM_STRING( &constant_bin[ 1854995 ], 125, 0 );
    const_str_plain__xlsm_options = UNSTREAM_STRING( &constant_bin[ 1855120 ], 13, 1 );
    const_str_digest_162c04eaa4f9de7abebfff3d81422a8b = UNSTREAM_STRING( &constant_bin[ 1855133 ], 105, 0 );
    const_str_digest_a9c4f8563d1ecedf60bda713bd0b1e2c = UNSTREAM_STRING( &constant_bin[ 1855238 ], 569, 0 );
    const_str_digest_f9be2e7dbb52eeee952286f86a48c0d1 = UNSTREAM_STRING( &constant_bin[ 1855807 ], 84, 0 );
    const_int_pos_1690785 = PyLong_FromUnsignedLong( 1690785ul );
    const_tuple_tuple_type_int_anon_NoneType_tuple_tuple = PyTuple_New( 1 );
    const_tuple_type_int_anon_NoneType_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_type_int_anon_NoneType_tuple, 0, (PyObject *)&PyLong_Type ); Py_INCREF( (PyObject *)&PyLong_Type );
    PyTuple_SET_ITEM( const_tuple_type_int_anon_NoneType_tuple, 1, (PyObject *)Py_TYPE( Py_None ) ); Py_INCREF( (PyObject *)Py_TYPE( Py_None ) );
    PyTuple_SET_ITEM( const_tuple_tuple_type_int_anon_NoneType_tuple_tuple, 0, const_tuple_type_int_anon_NoneType_tuple ); Py_INCREF( const_tuple_type_int_anon_NoneType_tuple );
    const_str_plain_pc_chop_threshold_doc = UNSTREAM_STRING( &constant_bin[ 1855891 ], 21, 1 );
    const_str_plain_date_yearfirst = UNSTREAM_STRING( &constant_bin[ 1855912 ], 14, 1 );
    const_str_plain_register_converter_doc = UNSTREAM_STRING( &constant_bin[ 1855926 ], 22, 1 );
    const_str_digest_1c14e0e1f14a8db299372128951bac65 = UNSTREAM_STRING( &constant_bin[ 1855948 ], 12, 0 );
    const_tuple_str_plain_key_str_plain__use_inf_as_na_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_key_str_plain__use_inf_as_na_tuple, 0, const_str_plain_key ); Py_INCREF( const_str_plain_key );
    PyTuple_SET_ITEM( const_tuple_str_plain_key_str_plain__use_inf_as_na_tuple, 1, const_str_plain__use_inf_as_na ); Py_INCREF( const_str_plain__use_inf_as_na );
    const_tuple_str_plain__enable_data_resource_formatter_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain__enable_data_resource_formatter_tuple, 0, const_str_plain__enable_data_resource_formatter ); Py_INCREF( const_str_plain__enable_data_resource_formatter );
    const_str_digest_3ecce95cea7e70b97124a63fdb6f472c = UNSTREAM_STRING( &constant_bin[ 1855960 ], 104, 0 );
    const_str_plain_max_info_rows = UNSTREAM_STRING( &constant_bin[ 1853697 ], 13, 1 );
    const_str_plain_pc_colspace_doc = UNSTREAM_STRING( &constant_bin[ 1856064 ], 15, 1 );
    const_str_digest_6de8d02e7df5afc37f0e3f0e946776a1 = UNSTREAM_STRING( &constant_bin[ 1856079 ], 118, 0 );
    const_str_digest_603db2006590fd8b8437f02ab548d669 = UNSTREAM_STRING( &constant_bin[ 1856197 ], 175, 0 );
    const_str_digest_5c883b3797590ee62e4ca0d8acbabdbf = UNSTREAM_STRING( &constant_bin[ 1856372 ], 159, 0 );
    const_str_plain_pc_latex_repr_doc = UNSTREAM_STRING( &constant_bin[ 1856531 ], 17, 1 );
    const_str_digest_a8ce4dc3e5952b64cf078ae95e08188a = UNSTREAM_STRING( &constant_bin[ 1856548 ], 24, 0 );
    const_tuple_str_digest_d81e5a583e51219530e4aa7bf1ec93ce_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_d81e5a583e51219530e4aa7bf1ec93ce_tuple, 0, const_str_digest_d81e5a583e51219530e4aa7bf1ec93ce ); Py_INCREF( const_str_digest_d81e5a583e51219530e4aa7bf1ec93ce );
    const_str_plain_pc_precision_doc = UNSTREAM_STRING( &constant_bin[ 1856572 ], 16, 1 );
    const_str_digest_12c6d9d73b32c369db53c25a405c94a9 = UNSTREAM_STRING( &constant_bin[ 1856588 ], 16, 0 );
    const_str_digest_12536abbc33fcdb5edeca9cb579ae639 = UNSTREAM_STRING( &constant_bin[ 1853492 ], 12, 0 );
    const_tuple_str_plain_column_space_int_pos_12_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_column_space_int_pos_12_tuple, 0, const_str_plain_column_space ); Py_INCREF( const_str_plain_column_space );
    PyTuple_SET_ITEM( const_tuple_str_plain_column_space_int_pos_12_tuple, 1, const_int_pos_12 ); Py_INCREF( const_int_pos_12 );
    const_str_digest_c8ce5ba4cfc04c1c564d4d58eee1151a = UNSTREAM_STRING( &constant_bin[ 1856604 ], 25, 0 );
    const_str_plain_pc_html_border_doc = UNSTREAM_STRING( &constant_bin[ 1856629 ], 18, 1 );
    const_str_digest_3fed32ca54d55e7419f2cf87ece4440b = UNSTREAM_STRING( &constant_bin[ 1856647 ], 149, 0 );
    const_list_none_true_false_str_plain_deep_list = PyList_New( 4 );
    PyList_SET_ITEM( const_list_none_true_false_str_plain_deep_list, 0, Py_None ); Py_INCREF( Py_None );
    PyList_SET_ITEM( const_list_none_true_false_str_plain_deep_list, 1, Py_True ); Py_INCREF( Py_True );
    PyList_SET_ITEM( const_list_none_true_false_str_plain_deep_list, 2, Py_False ); Py_INCREF( Py_False );
    PyList_SET_ITEM( const_list_none_true_false_str_plain_deep_list, 3, const_str_plain_deep ); Py_INCREF( const_str_plain_deep );
    const_str_digest_79b75e74f53a10e8129732de4fc9ea36 = UNSTREAM_STRING( &constant_bin[ 1856796 ], 108, 0 );
    const_str_plain_use_bottleneck_cb = UNSTREAM_STRING( &constant_bin[ 1856904 ], 17, 1 );
    const_str_plain_pc_latex_multicolumn_format = UNSTREAM_STRING( &constant_bin[ 1856921 ], 27, 1 );
    const_str_digest_92d5fb4be587bc095681b0b7376435ec = UNSTREAM_STRING( &constant_bin[ 1856948 ], 146, 0 );
    const_str_plain_float_format_doc = UNSTREAM_STRING( &constant_bin[ 1857094 ], 16, 1 );
    const_str_plain_pc_east_asian_width_doc = UNSTREAM_STRING( &constant_bin[ 1857110 ], 23, 1 );
    const_str_digest_e6fe352ef36e0bf7027ddc4a9efaf76c = UNSTREAM_STRING( &constant_bin[ 1857133 ], 120, 0 );
    const_str_plain_pc_date_dayfirst_doc = UNSTREAM_STRING( &constant_bin[ 1857253 ], 20, 1 );
    const_str_digest_70761738ff7ce1bdfec83dad7b784939 = UNSTREAM_STRING( &constant_bin[ 1857273 ], 17, 0 );
    const_str_digest_8b6677d435ef234e5391d3dcc8cf1b40 = UNSTREAM_STRING( &constant_bin[ 1857290 ], 18, 0 );
    const_list_str_plain_truncate_str_plain_info_list = PyList_New( 2 );
    PyList_SET_ITEM( const_list_str_plain_truncate_str_plain_info_list, 0, const_str_plain_truncate ); Py_INCREF( const_str_plain_truncate );
    PyList_SET_ITEM( const_list_str_plain_truncate_str_plain_info_list, 1, const_str_plain_info ); Py_INCREF( const_str_plain_info );
    const_tuple_str_plain_register_matplotlib_converters_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_register_matplotlib_converters_tuple, 0, const_str_plain_register_matplotlib_converters ); Py_INCREF( const_str_plain_register_matplotlib_converters );
    const_list_str_plain_xlwt_list = PyList_New( 1 );
    PyList_SET_ITEM( const_list_str_plain_xlwt_list, 0, const_str_plain_xlwt ); Py_INCREF( const_str_plain_xlwt );
    const_list_true_false_str_plain_truncate_list = PyList_New( 3 );
    PyList_SET_ITEM( const_list_true_false_str_plain_truncate_list, 0, Py_True ); Py_INCREF( Py_True );
    PyList_SET_ITEM( const_list_true_false_str_plain_truncate_list, 1, Py_False ); Py_INCREF( Py_False );
    PyList_SET_ITEM( const_list_true_false_str_plain_truncate_list, 2, const_str_plain_truncate ); Py_INCREF( const_str_plain_truncate );
    const_str_digest_9828a77d868bc8c07cee23db35f2d9a5 = UNSTREAM_STRING( &constant_bin[ 1857308 ], 328, 0 );
    const_str_digest_32d9ee95a950d61124e98e2a12fc0214 = UNSTREAM_STRING( &constant_bin[ 1857636 ], 117, 0 );
    const_tuple_str_digest_42083bc4d862dcb2ba3d5c3079c8b46e_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_42083bc4d862dcb2ba3d5c3079c8b46e_tuple, 0, const_str_digest_42083bc4d862dcb2ba3d5c3079c8b46e ); Py_INCREF( const_str_digest_42083bc4d862dcb2ba3d5c3079c8b46e );
    const_str_plain_use_numexpr_doc = UNSTREAM_STRING( &constant_bin[ 1857753 ], 15, 1 );
    const_str_digest_45068c861a2fe7b61a5c03d495ca5c0a = UNSTREAM_STRING( &constant_bin[ 1857768 ], 13, 0 );
    const_str_digest_36e3b1f94c51cffa19984b47d24b4246 = UNSTREAM_STRING( &constant_bin[ 1857781 ], 177, 0 );
    const_str_digest_3cce2b8ce49df0213b09bcbcf6ed7cda = UNSTREAM_STRING( &constant_bin[ 1857958 ], 146, 0 );
    const_str_plain_use_numexpr_cb = UNSTREAM_STRING( &constant_bin[ 1858104 ], 14, 1 );
    const_str_digest_baeb24505d73a9f85f64f36d492f470b = UNSTREAM_STRING( &constant_bin[ 1858118 ], 114, 0 );
    const_str_plain_pc_max_cols_doc = UNSTREAM_STRING( &constant_bin[ 1858232 ], 15, 1 );
    const_str_plain_colheader_justify = UNSTREAM_STRING( &constant_bin[ 1858247 ], 17, 1 );
    const_list_anon_NoneType_type_int_list = PyList_New( 2 );
    PyList_SET_ITEM( const_list_anon_NoneType_type_int_list, 0, (PyObject *)Py_TYPE( Py_None ) ); Py_INCREF( (PyObject *)Py_TYPE( Py_None ) );
    PyList_SET_ITEM( const_list_anon_NoneType_type_int_list, 1, (PyObject *)&PyLong_Type ); Py_INCREF( (PyObject *)&PyLong_Type );
    const_str_plain_use_bottleneck_doc = UNSTREAM_STRING( &constant_bin[ 1858264 ], 18, 1 );
    const_str_plain_pc_max_info_cols_doc = UNSTREAM_STRING( &constant_bin[ 1858282 ], 20, 1 );
    const_str_digest_8673b45174cf95301c8e18f39b235e8c = UNSTREAM_STRING( &constant_bin[ 1858302 ], 14, 0 );
    const_str_plain__xlsx_options = UNSTREAM_STRING( &constant_bin[ 1858316 ], 13, 1 );
    const_str_plain_use_inf_as_null_doc = UNSTREAM_STRING( &constant_bin[ 1858329 ], 19, 1 );
    const_str_digest_99e766b2632da277c8e75ec5e46abcd7 = UNSTREAM_STRING( &constant_bin[ 1858348 ], 113, 0 );
    const_str_plain_pc_memory_usage_doc = UNSTREAM_STRING( &constant_bin[ 1858461 ], 19, 1 );
    const_str_digest_35b0c6e3e7281501288a4ab5b486f5d6 = UNSTREAM_STRING( &constant_bin[ 1858480 ], 20, 0 );
    const_str_plain_style_backup = UNSTREAM_STRING( &constant_bin[ 1858500 ], 12, 1 );
    const_str_plain_pc_table_schema_doc = UNSTREAM_STRING( &constant_bin[ 1858512 ], 19, 1 );
    const_tuple_str_digest_35b0c6e3e7281501288a4ab5b486f5d6_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_35b0c6e3e7281501288a4ab5b486f5d6_tuple, 0, const_str_digest_35b0c6e3e7281501288a4ab5b486f5d6 ); Py_INCREF( const_str_digest_35b0c6e3e7281501288a4ab5b486f5d6 );
    const_str_plain_colheader_justify_doc = UNSTREAM_STRING( &constant_bin[ 1858531 ], 21, 1 );
    const_str_digest_d640058ba6564339d64044d11762fd11 = UNSTREAM_STRING( &constant_bin[ 1858552 ], 143, 0 );
    const_str_plain_pc_large_repr_doc = UNSTREAM_STRING( &constant_bin[ 1858695 ], 17, 1 );
    const_str_digest_55909c86f77471fd84b96a2c560d530c = UNSTREAM_STRING( &constant_bin[ 1858712 ], 473, 0 );
    const_str_plain_date_dayfirst = UNSTREAM_STRING( &constant_bin[ 1857256 ], 13, 1 );
    const_str_plain_expand_frame_repr = UNSTREAM_STRING( &constant_bin[ 1859185 ], 17, 1 );
    const_str_plain_max_colwidth_doc = UNSTREAM_STRING( &constant_bin[ 1859202 ], 16, 1 );
    const_str_plain_tc_sim_interactive_doc = UNSTREAM_STRING( &constant_bin[ 1859218 ], 22, 1 );
    const_tuple_d384e95b8089e28989357db5c2fae7f2_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_d384e95b8089e28989357db5c2fae7f2_tuple, 0, const_str_plain_is_int ); Py_INCREF( const_str_plain_is_int );
    PyTuple_SET_ITEM( const_tuple_d384e95b8089e28989357db5c2fae7f2_tuple, 1, const_str_plain_is_bool ); Py_INCREF( const_str_plain_is_bool );
    PyTuple_SET_ITEM( const_tuple_d384e95b8089e28989357db5c2fae7f2_tuple, 2, const_str_plain_is_text ); Py_INCREF( const_str_plain_is_text );
    PyTuple_SET_ITEM( const_tuple_d384e95b8089e28989357db5c2fae7f2_tuple, 3, const_str_plain_is_instance_factory ); Py_INCREF( const_str_plain_is_instance_factory );
    PyTuple_SET_ITEM( const_tuple_d384e95b8089e28989357db5c2fae7f2_tuple, 4, const_str_plain_is_one_of_factory ); Py_INCREF( const_str_plain_is_one_of_factory );
    PyTuple_SET_ITEM( const_tuple_d384e95b8089e28989357db5c2fae7f2_tuple, 5, const_str_plain_is_callable ); Py_INCREF( const_str_plain_is_callable );
    const_str_digest_f5fa2e7b82b0176dddfc8ec97f30c139 = UNSTREAM_STRING( &constant_bin[ 1859240 ], 135, 0 );
    const_str_digest_d779ffca91b7e173eb78d35afad339b2 = UNSTREAM_STRING( &constant_bin[ 1859375 ], 238, 0 );
    const_list_none_str_plain_warn_str_plain_raise_list = PyList_New( 3 );
    PyList_SET_ITEM( const_list_none_str_plain_warn_str_plain_raise_list, 0, Py_None ); Py_INCREF( Py_None );
    PyList_SET_ITEM( const_list_none_str_plain_warn_str_plain_raise_list, 1, const_str_plain_warn ); Py_INCREF( const_str_plain_warn );
    PyList_SET_ITEM( const_list_none_str_plain_warn_str_plain_raise_list, 2, const_str_plain_raise ); Py_INCREF( const_str_plain_raise );
    const_str_digest_86f291ce802a3d5c7c1dc1d9671e3230 = UNSTREAM_STRING( &constant_bin[ 1859613 ], 126, 0 );
    const_tuple_str_digest_12536abbc33fcdb5edeca9cb579ae639_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_12536abbc33fcdb5edeca9cb579ae639_tuple, 0, const_str_digest_12536abbc33fcdb5edeca9cb579ae639 ); Py_INCREF( const_str_digest_12536abbc33fcdb5edeca9cb579ae639 );
    const_tuple_str_digest_45068c861a2fe7b61a5c03d495ca5c0a_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_45068c861a2fe7b61a5c03d495ca5c0a_tuple, 0, const_str_digest_45068c861a2fe7b61a5c03d495ca5c0a ); Py_INCREF( const_str_digest_45068c861a2fe7b61a5c03d495ca5c0a );
    const_str_digest_2c5f1d42f6beaab0f171385068265a49 = UNSTREAM_STRING( &constant_bin[ 1859739 ], 226, 0 );
    const_str_plain_notebook_repr_html = UNSTREAM_STRING( &constant_bin[ 1859965 ], 18, 1 );
    const_tuple_str_plain_key_str_plain__enable_data_resource_formatter_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_key_str_plain__enable_data_resource_formatter_tuple, 0, const_str_plain_key ); Py_INCREF( const_str_plain_key );
    PyTuple_SET_ITEM( const_tuple_str_plain_key_str_plain__enable_data_resource_formatter_tuple, 1, const_str_plain__enable_data_resource_formatter ); Py_INCREF( const_str_plain__enable_data_resource_formatter );
    const_str_plain_pc_max_rows_doc = UNSTREAM_STRING( &constant_bin[ 1859983 ], 15, 1 );
    const_tuple_413ae156d7ae29f4e06579546b354151_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_413ae156d7ae29f4e06579546b354151_tuple, 0, const_str_plain_key ); Py_INCREF( const_str_plain_key );
    PyTuple_SET_ITEM( const_tuple_413ae156d7ae29f4e06579546b354151_tuple, 1, const_str_plain_register_matplotlib_converters ); Py_INCREF( const_str_plain_register_matplotlib_converters );
    PyTuple_SET_ITEM( const_tuple_413ae156d7ae29f4e06579546b354151_tuple, 2, const_str_plain_deregister_matplotlib_converters ); Py_INCREF( const_str_plain_deregister_matplotlib_converters );
    const_str_digest_25e404ad3ac11a956aa85b1d77b401de = UNSTREAM_STRING( &constant_bin[ 1859998 ], 111, 0 );
    const_str_digest_0f50b2f82ea6dafb9acb48b11438c59c = UNSTREAM_STRING( &constant_bin[ 1860109 ], 131, 0 );
    const_str_plain_register_converters = UNSTREAM_STRING( &constant_bin[ 1860240 ], 19, 1 );
    const_tuple_str_plain_key_str_plain_expressions_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_key_str_plain_expressions_tuple, 0, const_str_plain_key ); Py_INCREF( const_str_plain_key );
    PyTuple_SET_ITEM( const_tuple_str_plain_key_str_plain_expressions_tuple, 1, const_str_plain_expressions ); Py_INCREF( const_str_plain_expressions );
    const_str_plain__xls_options = UNSTREAM_STRING( &constant_bin[ 1860259 ], 12, 1 );
    const_str_plain_pc_expand_repr_doc = UNSTREAM_STRING( &constant_bin[ 1860271 ], 18, 1 );
    const_str_digest_02315a6b6613311a88485cf6ec10d6ed = UNSTREAM_STRING( &constant_bin[ 1860289 ], 80, 0 );
    const_str_digest_47930e395de0263309e238bd2d8cce2c = UNSTREAM_STRING( &constant_bin[ 1860369 ], 124, 0 );
    const_str_plain_use_inf_as_na = UNSTREAM_STRING( &constant_bin[ 1853814 ], 13, 1 );
    const_str_digest_b4fd9bef99574cf727e466922f2dc8a0 = UNSTREAM_STRING( &constant_bin[ 1860493 ], 220, 0 );
    const_str_plain_pc_latex_escape = UNSTREAM_STRING( &constant_bin[ 1860713 ], 15, 1 );
    const_str_digest_67ab286b537608a8ed02613094baf20d = UNSTREAM_STRING( &constant_bin[ 1860728 ], 152, 0 );
    const_str_plain_chop_threshold = UNSTREAM_STRING( &constant_bin[ 1855894 ], 14, 1 );
    const_tuple_str_plain_key_str_plain_nanops_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_key_str_plain_nanops_tuple, 0, const_str_plain_key ); Py_INCREF( const_str_plain_key );
    PyTuple_SET_ITEM( const_tuple_str_plain_key_str_plain_nanops_tuple, 1, const_str_plain_nanops ); Py_INCREF( const_str_plain_nanops );
    const_str_plain_pc_max_categories_doc = UNSTREAM_STRING( &constant_bin[ 1860880 ], 21, 1 );
    const_str_plain_pc_date_yearfirst_doc = UNSTREAM_STRING( &constant_bin[ 1860901 ], 21, 1 );
    const_str_digest_efff17427f06ff36726c48901702ae1d = UNSTREAM_STRING( &constant_bin[ 1854611 ], 17, 0 );
    const_str_plain_pc_html_border_deprecation_warning = UNSTREAM_STRING( &constant_bin[ 1860922 ], 34, 1 );
    const_str_plain_pc_pprint_nest_depth = UNSTREAM_STRING( &constant_bin[ 1860956 ], 20, 1 );
    const_tuple_str_plain_is_terminal_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_is_terminal_tuple, 0, const_str_plain_is_terminal ); Py_INCREF( const_str_plain_is_terminal );
    const_str_plain_chained_assignment = UNSTREAM_STRING( &constant_bin[ 1860976 ], 18, 1 );
    const_str_plain_sim_interactive = UNSTREAM_STRING( &constant_bin[ 1806935 ], 15, 1 );
    const_str_digest_f3097cefcee812f2202499367dd8f891 = UNSTREAM_STRING( &constant_bin[ 1860994 ], 571, 0 );
    const_str_digest_de1533dce7c0c3057803e6914fcf5814 = UNSTREAM_STRING( &constant_bin[ 1861565 ], 134, 0 );
    const_str_plain_table_schema_cb = UNSTREAM_STRING( &constant_bin[ 1861699 ], 15, 1 );
    const_str_plain_max_colwidth = UNSTREAM_STRING( &constant_bin[ 1859202 ], 12, 1 );

    constants_created = true;
}

#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_pandas$core$config_init( void )
{
    // The module may not have been used at all.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_3209efbe1fc5d2857a97a7e36ce015f8;
static PyCodeObject *codeobj_21c02fa2e15e4cdad81fff1b8f4db482;
static PyCodeObject *codeobj_2eecd91b75e20338b192c233ae1656d5;
static PyCodeObject *codeobj_bc52bd7f8419602940b525b7bdd8b535;
static PyCodeObject *codeobj_32ae009b8fd949246dee06ecbfc1c516;
static PyCodeObject *codeobj_8c240a030f351b8dbad36bc047195550;

static void createModuleCodeObjects(void)
{
    module_filename_obj = const_str_digest_f2956f16d5c6b095746cfa96ebbd29c0;
    codeobj_3209efbe1fc5d2857a97a7e36ce015f8 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_4497acf6863e0333fbe35765df4bdb52, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_21c02fa2e15e4cdad81fff1b8f4db482 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_register_converter_cb, 493, const_tuple_413ae156d7ae29f4e06579546b354151_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_2eecd91b75e20338b192c233ae1656d5 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_table_schema_cb, 301, const_tuple_str_plain_key_str_plain__enable_data_resource_formatter_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_bc52bd7f8419602940b525b7bdd8b535 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_use_bottleneck_cb, 28, const_tuple_str_plain_key_str_plain_nanops_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_32ae009b8fd949246dee06ecbfc1c516 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_use_inf_as_na_cb, 408, const_tuple_str_plain_key_str_plain__use_inf_as_na_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_8c240a030f351b8dbad36bc047195550 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_use_numexpr_cb, 41, const_tuple_str_plain_key_str_plain_expressions_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
}

// The module function declarations.
static PyObject *MAKE_FUNCTION_pandas$core$config_init$$$function_1_use_bottleneck_cb(  );


static PyObject *MAKE_FUNCTION_pandas$core$config_init$$$function_2_use_numexpr_cb(  );


static PyObject *MAKE_FUNCTION_pandas$core$config_init$$$function_3_table_schema_cb(  );


static PyObject *MAKE_FUNCTION_pandas$core$config_init$$$function_4_use_inf_as_na_cb(  );


static PyObject *MAKE_FUNCTION_pandas$core$config_init$$$function_5_register_converter_cb(  );


// The module function definitions.
static PyObject *impl_pandas$core$config_init$$$function_1_use_bottleneck_cb( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_key = python_pars[ 0 ];
    PyObject *var_nanops = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_args_element_name_1;
    PyObject *tmp_args_element_name_2;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_called_name_1;
    PyObject *tmp_called_name_2;
    PyObject *tmp_fromlist_name_1;
    PyObject *tmp_globals_name_1;
    PyObject *tmp_import_name_from_1;
    PyObject *tmp_level_name_1;
    PyObject *tmp_locals_name_1;
    PyObject *tmp_name_name_1;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    PyObject *tmp_source_name_2;
    NUITKA_MAY_BE_UNUSED PyObject *tmp_unused;
    static struct Nuitka_FrameObject *cache_frame_bc52bd7f8419602940b525b7bdd8b535 = NULL;

    struct Nuitka_FrameObject *frame_bc52bd7f8419602940b525b7bdd8b535;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_bc52bd7f8419602940b525b7bdd8b535, codeobj_bc52bd7f8419602940b525b7bdd8b535, module_pandas$core$config_init, sizeof(void *)+sizeof(void *) );
    frame_bc52bd7f8419602940b525b7bdd8b535 = cache_frame_bc52bd7f8419602940b525b7bdd8b535;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_bc52bd7f8419602940b525b7bdd8b535 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_bc52bd7f8419602940b525b7bdd8b535 ) == 2 ); // Frame stack

    // Framed code:
    tmp_name_name_1 = const_str_digest_b0b825b0001352666a073831b2d2ecba;
    tmp_globals_name_1 = (PyObject *)moduledict_pandas$core$config_init;
    tmp_locals_name_1 = Py_None;
    tmp_fromlist_name_1 = const_tuple_str_plain_nanops_tuple;
    tmp_level_name_1 = const_int_0;
    frame_bc52bd7f8419602940b525b7bdd8b535->m_frame.f_lineno = 29;
    tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
    if ( tmp_import_name_from_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 29;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }
    tmp_assign_source_1 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_nanops );
    Py_DECREF( tmp_import_name_from_1 );
    if ( tmp_assign_source_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 29;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }
    assert( var_nanops == NULL );
    var_nanops = tmp_assign_source_1;

    tmp_source_name_1 = var_nanops;

    CHECK_OBJECT( tmp_source_name_1 );
    tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_set_use_bottleneck );
    if ( tmp_called_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 30;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }
    tmp_source_name_2 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_source_name_2 == NULL ))
    {
        tmp_source_name_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_source_name_2 == NULL )
    {
        Py_DECREF( tmp_called_name_1 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 30;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }

    tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_get_option );
    if ( tmp_called_name_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_1 );

        exception_lineno = 30;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }
    tmp_args_element_name_2 = par_key;

    if ( tmp_args_element_name_2 == NULL )
    {
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_called_name_2 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "key" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 30;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }

    frame_bc52bd7f8419602940b525b7bdd8b535->m_frame.f_lineno = 30;
    {
        PyObject *call_args[] = { tmp_args_element_name_2 };
        tmp_args_element_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
    }

    Py_DECREF( tmp_called_name_2 );
    if ( tmp_args_element_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_1 );

        exception_lineno = 30;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }
    frame_bc52bd7f8419602940b525b7bdd8b535->m_frame.f_lineno = 30;
    {
        PyObject *call_args[] = { tmp_args_element_name_1 };
        tmp_unused = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
    }

    Py_DECREF( tmp_called_name_1 );
    Py_DECREF( tmp_args_element_name_1 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 30;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_unused );

#if 0
    RESTORE_FRAME_EXCEPTION( frame_bc52bd7f8419602940b525b7bdd8b535 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_bc52bd7f8419602940b525b7bdd8b535 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_bc52bd7f8419602940b525b7bdd8b535, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_bc52bd7f8419602940b525b7bdd8b535->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_bc52bd7f8419602940b525b7bdd8b535, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_bc52bd7f8419602940b525b7bdd8b535,
        type_description_1,
        par_key,
        var_nanops
    );


    // Release cached frame.
    if ( frame_bc52bd7f8419602940b525b7bdd8b535 == cache_frame_bc52bd7f8419602940b525b7bdd8b535 )
    {
        Py_DECREF( frame_bc52bd7f8419602940b525b7bdd8b535 );
    }
    cache_frame_bc52bd7f8419602940b525b7bdd8b535 = NULL;

    assertFrameObject( frame_bc52bd7f8419602940b525b7bdd8b535 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$core$config_init$$$function_1_use_bottleneck_cb );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_key );
    par_key = NULL;

    Py_XDECREF( var_nanops );
    var_nanops = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_key );
    par_key = NULL;

    Py_XDECREF( var_nanops );
    var_nanops = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$core$config_init$$$function_1_use_bottleneck_cb );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$core$config_init$$$function_2_use_numexpr_cb( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_key = python_pars[ 0 ];
    PyObject *var_expressions = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_args_element_name_1;
    PyObject *tmp_args_element_name_2;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_called_name_1;
    PyObject *tmp_called_name_2;
    PyObject *tmp_fromlist_name_1;
    PyObject *tmp_globals_name_1;
    PyObject *tmp_import_name_from_1;
    PyObject *tmp_level_name_1;
    PyObject *tmp_locals_name_1;
    PyObject *tmp_name_name_1;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    PyObject *tmp_source_name_2;
    NUITKA_MAY_BE_UNUSED PyObject *tmp_unused;
    static struct Nuitka_FrameObject *cache_frame_8c240a030f351b8dbad36bc047195550 = NULL;

    struct Nuitka_FrameObject *frame_8c240a030f351b8dbad36bc047195550;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_8c240a030f351b8dbad36bc047195550, codeobj_8c240a030f351b8dbad36bc047195550, module_pandas$core$config_init, sizeof(void *)+sizeof(void *) );
    frame_8c240a030f351b8dbad36bc047195550 = cache_frame_8c240a030f351b8dbad36bc047195550;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_8c240a030f351b8dbad36bc047195550 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_8c240a030f351b8dbad36bc047195550 ) == 2 ); // Frame stack

    // Framed code:
    tmp_name_name_1 = const_str_digest_fd66022e234f90372ceae074a8020048;
    tmp_globals_name_1 = (PyObject *)moduledict_pandas$core$config_init;
    tmp_locals_name_1 = Py_None;
    tmp_fromlist_name_1 = const_tuple_str_plain_expressions_tuple;
    tmp_level_name_1 = const_int_0;
    frame_8c240a030f351b8dbad36bc047195550->m_frame.f_lineno = 42;
    tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
    if ( tmp_import_name_from_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 42;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }
    tmp_assign_source_1 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_expressions );
    Py_DECREF( tmp_import_name_from_1 );
    if ( tmp_assign_source_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 42;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }
    assert( var_expressions == NULL );
    var_expressions = tmp_assign_source_1;

    tmp_source_name_1 = var_expressions;

    CHECK_OBJECT( tmp_source_name_1 );
    tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_set_use_numexpr );
    if ( tmp_called_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 43;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }
    tmp_source_name_2 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_source_name_2 == NULL ))
    {
        tmp_source_name_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_source_name_2 == NULL )
    {
        Py_DECREF( tmp_called_name_1 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 43;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }

    tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_get_option );
    if ( tmp_called_name_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_1 );

        exception_lineno = 43;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }
    tmp_args_element_name_2 = par_key;

    if ( tmp_args_element_name_2 == NULL )
    {
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_called_name_2 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "key" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 43;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }

    frame_8c240a030f351b8dbad36bc047195550->m_frame.f_lineno = 43;
    {
        PyObject *call_args[] = { tmp_args_element_name_2 };
        tmp_args_element_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
    }

    Py_DECREF( tmp_called_name_2 );
    if ( tmp_args_element_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_1 );

        exception_lineno = 43;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }
    frame_8c240a030f351b8dbad36bc047195550->m_frame.f_lineno = 43;
    {
        PyObject *call_args[] = { tmp_args_element_name_1 };
        tmp_unused = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
    }

    Py_DECREF( tmp_called_name_1 );
    Py_DECREF( tmp_args_element_name_1 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 43;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_unused );

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8c240a030f351b8dbad36bc047195550 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8c240a030f351b8dbad36bc047195550 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_8c240a030f351b8dbad36bc047195550, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_8c240a030f351b8dbad36bc047195550->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_8c240a030f351b8dbad36bc047195550, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_8c240a030f351b8dbad36bc047195550,
        type_description_1,
        par_key,
        var_expressions
    );


    // Release cached frame.
    if ( frame_8c240a030f351b8dbad36bc047195550 == cache_frame_8c240a030f351b8dbad36bc047195550 )
    {
        Py_DECREF( frame_8c240a030f351b8dbad36bc047195550 );
    }
    cache_frame_8c240a030f351b8dbad36bc047195550 = NULL;

    assertFrameObject( frame_8c240a030f351b8dbad36bc047195550 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$core$config_init$$$function_2_use_numexpr_cb );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_key );
    par_key = NULL;

    Py_XDECREF( var_expressions );
    var_expressions = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_key );
    par_key = NULL;

    Py_XDECREF( var_expressions );
    var_expressions = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$core$config_init$$$function_2_use_numexpr_cb );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$core$config_init$$$function_3_table_schema_cb( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_key = python_pars[ 0 ];
    PyObject *var__enable_data_resource_formatter = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_args_element_name_1;
    PyObject *tmp_args_element_name_2;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_called_name_1;
    PyObject *tmp_called_name_2;
    PyObject *tmp_fromlist_name_1;
    PyObject *tmp_globals_name_1;
    PyObject *tmp_import_name_from_1;
    PyObject *tmp_level_name_1;
    PyObject *tmp_locals_name_1;
    PyObject *tmp_name_name_1;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    NUITKA_MAY_BE_UNUSED PyObject *tmp_unused;
    static struct Nuitka_FrameObject *cache_frame_2eecd91b75e20338b192c233ae1656d5 = NULL;

    struct Nuitka_FrameObject *frame_2eecd91b75e20338b192c233ae1656d5;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_2eecd91b75e20338b192c233ae1656d5, codeobj_2eecd91b75e20338b192c233ae1656d5, module_pandas$core$config_init, sizeof(void *)+sizeof(void *) );
    frame_2eecd91b75e20338b192c233ae1656d5 = cache_frame_2eecd91b75e20338b192c233ae1656d5;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_2eecd91b75e20338b192c233ae1656d5 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_2eecd91b75e20338b192c233ae1656d5 ) == 2 ); // Frame stack

    // Framed code:
    tmp_name_name_1 = const_str_digest_ac9917bb8c64ab6890c5c907f082236f;
    tmp_globals_name_1 = (PyObject *)moduledict_pandas$core$config_init;
    tmp_locals_name_1 = Py_None;
    tmp_fromlist_name_1 = const_tuple_str_plain__enable_data_resource_formatter_tuple;
    tmp_level_name_1 = const_int_0;
    frame_2eecd91b75e20338b192c233ae1656d5->m_frame.f_lineno = 302;
    tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
    if ( tmp_import_name_from_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 302;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }
    tmp_assign_source_1 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain__enable_data_resource_formatter );
    Py_DECREF( tmp_import_name_from_1 );
    if ( tmp_assign_source_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 302;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }
    assert( var__enable_data_resource_formatter == NULL );
    var__enable_data_resource_formatter = tmp_assign_source_1;

    tmp_called_name_1 = var__enable_data_resource_formatter;

    CHECK_OBJECT( tmp_called_name_1 );
    tmp_source_name_1 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_source_name_1 == NULL ))
    {
        tmp_source_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_source_name_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 303;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }

    tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_get_option );
    if ( tmp_called_name_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 303;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }
    tmp_args_element_name_2 = par_key;

    if ( tmp_args_element_name_2 == NULL )
    {
        Py_DECREF( tmp_called_name_2 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "key" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 303;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }

    frame_2eecd91b75e20338b192c233ae1656d5->m_frame.f_lineno = 303;
    {
        PyObject *call_args[] = { tmp_args_element_name_2 };
        tmp_args_element_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
    }

    Py_DECREF( tmp_called_name_2 );
    if ( tmp_args_element_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 303;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }
    frame_2eecd91b75e20338b192c233ae1656d5->m_frame.f_lineno = 303;
    {
        PyObject *call_args[] = { tmp_args_element_name_1 };
        tmp_unused = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
    }

    Py_DECREF( tmp_args_element_name_1 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 303;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_unused );

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2eecd91b75e20338b192c233ae1656d5 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2eecd91b75e20338b192c233ae1656d5 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_2eecd91b75e20338b192c233ae1656d5, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_2eecd91b75e20338b192c233ae1656d5->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_2eecd91b75e20338b192c233ae1656d5, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_2eecd91b75e20338b192c233ae1656d5,
        type_description_1,
        par_key,
        var__enable_data_resource_formatter
    );


    // Release cached frame.
    if ( frame_2eecd91b75e20338b192c233ae1656d5 == cache_frame_2eecd91b75e20338b192c233ae1656d5 )
    {
        Py_DECREF( frame_2eecd91b75e20338b192c233ae1656d5 );
    }
    cache_frame_2eecd91b75e20338b192c233ae1656d5 = NULL;

    assertFrameObject( frame_2eecd91b75e20338b192c233ae1656d5 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$core$config_init$$$function_3_table_schema_cb );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_key );
    par_key = NULL;

    Py_XDECREF( var__enable_data_resource_formatter );
    var__enable_data_resource_formatter = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_key );
    par_key = NULL;

    Py_XDECREF( var__enable_data_resource_formatter );
    var__enable_data_resource_formatter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$core$config_init$$$function_3_table_schema_cb );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$core$config_init$$$function_4_use_inf_as_na_cb( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_key = python_pars[ 0 ];
    PyObject *var__use_inf_as_na = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_args_element_name_1;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_called_name_1;
    PyObject *tmp_fromlist_name_1;
    PyObject *tmp_globals_name_1;
    PyObject *tmp_import_name_from_1;
    PyObject *tmp_level_name_1;
    PyObject *tmp_locals_name_1;
    PyObject *tmp_name_name_1;
    PyObject *tmp_return_value;
    NUITKA_MAY_BE_UNUSED PyObject *tmp_unused;
    static struct Nuitka_FrameObject *cache_frame_32ae009b8fd949246dee06ecbfc1c516 = NULL;

    struct Nuitka_FrameObject *frame_32ae009b8fd949246dee06ecbfc1c516;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_32ae009b8fd949246dee06ecbfc1c516, codeobj_32ae009b8fd949246dee06ecbfc1c516, module_pandas$core$config_init, sizeof(void *)+sizeof(void *) );
    frame_32ae009b8fd949246dee06ecbfc1c516 = cache_frame_32ae009b8fd949246dee06ecbfc1c516;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_32ae009b8fd949246dee06ecbfc1c516 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_32ae009b8fd949246dee06ecbfc1c516 ) == 2 ); // Frame stack

    // Framed code:
    tmp_name_name_1 = const_str_digest_7a907ce278e4875739e67044c12dd234;
    tmp_globals_name_1 = (PyObject *)moduledict_pandas$core$config_init;
    tmp_locals_name_1 = Py_None;
    tmp_fromlist_name_1 = const_tuple_str_plain__use_inf_as_na_tuple;
    tmp_level_name_1 = const_int_0;
    frame_32ae009b8fd949246dee06ecbfc1c516->m_frame.f_lineno = 409;
    tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
    if ( tmp_import_name_from_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 409;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }
    tmp_assign_source_1 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain__use_inf_as_na );
    Py_DECREF( tmp_import_name_from_1 );
    if ( tmp_assign_source_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 409;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }
    assert( var__use_inf_as_na == NULL );
    var__use_inf_as_na = tmp_assign_source_1;

    tmp_called_name_1 = var__use_inf_as_na;

    CHECK_OBJECT( tmp_called_name_1 );
    tmp_args_element_name_1 = par_key;

    if ( tmp_args_element_name_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "key" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 410;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }

    frame_32ae009b8fd949246dee06ecbfc1c516->m_frame.f_lineno = 410;
    {
        PyObject *call_args[] = { tmp_args_element_name_1 };
        tmp_unused = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
    }

    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 410;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_unused );

#if 0
    RESTORE_FRAME_EXCEPTION( frame_32ae009b8fd949246dee06ecbfc1c516 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_32ae009b8fd949246dee06ecbfc1c516 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_32ae009b8fd949246dee06ecbfc1c516, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_32ae009b8fd949246dee06ecbfc1c516->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_32ae009b8fd949246dee06ecbfc1c516, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_32ae009b8fd949246dee06ecbfc1c516,
        type_description_1,
        par_key,
        var__use_inf_as_na
    );


    // Release cached frame.
    if ( frame_32ae009b8fd949246dee06ecbfc1c516 == cache_frame_32ae009b8fd949246dee06ecbfc1c516 )
    {
        Py_DECREF( frame_32ae009b8fd949246dee06ecbfc1c516 );
    }
    cache_frame_32ae009b8fd949246dee06ecbfc1c516 = NULL;

    assertFrameObject( frame_32ae009b8fd949246dee06ecbfc1c516 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$core$config_init$$$function_4_use_inf_as_na_cb );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_key );
    par_key = NULL;

    Py_XDECREF( var__use_inf_as_na );
    var__use_inf_as_na = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_key );
    par_key = NULL;

    Py_XDECREF( var__use_inf_as_na );
    var__use_inf_as_na = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$core$config_init$$$function_4_use_inf_as_na_cb );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$core$config_init$$$function_5_register_converter_cb( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_key = python_pars[ 0 ];
    PyObject *var_register_matplotlib_converters = NULL;
    PyObject *var_deregister_matplotlib_converters = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_args_element_name_1;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_called_name_1;
    PyObject *tmp_called_name_2;
    PyObject *tmp_called_name_3;
    int tmp_cond_truth_1;
    PyObject *tmp_cond_value_1;
    PyObject *tmp_fromlist_name_1;
    PyObject *tmp_fromlist_name_2;
    PyObject *tmp_globals_name_1;
    PyObject *tmp_globals_name_2;
    PyObject *tmp_import_name_from_1;
    PyObject *tmp_import_name_from_2;
    PyObject *tmp_level_name_1;
    PyObject *tmp_level_name_2;
    PyObject *tmp_locals_name_1;
    PyObject *tmp_locals_name_2;
    PyObject *tmp_name_name_1;
    PyObject *tmp_name_name_2;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    NUITKA_MAY_BE_UNUSED PyObject *tmp_unused;
    static struct Nuitka_FrameObject *cache_frame_21c02fa2e15e4cdad81fff1b8f4db482 = NULL;

    struct Nuitka_FrameObject *frame_21c02fa2e15e4cdad81fff1b8f4db482;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_21c02fa2e15e4cdad81fff1b8f4db482, codeobj_21c02fa2e15e4cdad81fff1b8f4db482, module_pandas$core$config_init, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_21c02fa2e15e4cdad81fff1b8f4db482 = cache_frame_21c02fa2e15e4cdad81fff1b8f4db482;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_21c02fa2e15e4cdad81fff1b8f4db482 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_21c02fa2e15e4cdad81fff1b8f4db482 ) == 2 ); // Frame stack

    // Framed code:
    tmp_name_name_1 = const_str_digest_c04039a7e34f9463fede19ff82de69b6;
    tmp_globals_name_1 = (PyObject *)moduledict_pandas$core$config_init;
    tmp_locals_name_1 = Py_None;
    tmp_fromlist_name_1 = const_tuple_str_plain_register_matplotlib_converters_tuple;
    tmp_level_name_1 = const_int_0;
    frame_21c02fa2e15e4cdad81fff1b8f4db482->m_frame.f_lineno = 494;
    tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
    if ( tmp_import_name_from_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 494;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    tmp_assign_source_1 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_register_matplotlib_converters );
    Py_DECREF( tmp_import_name_from_1 );
    if ( tmp_assign_source_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 494;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    assert( var_register_matplotlib_converters == NULL );
    var_register_matplotlib_converters = tmp_assign_source_1;

    tmp_name_name_2 = const_str_digest_c04039a7e34f9463fede19ff82de69b6;
    tmp_globals_name_2 = (PyObject *)moduledict_pandas$core$config_init;
    tmp_locals_name_2 = Py_None;
    tmp_fromlist_name_2 = const_tuple_str_plain_deregister_matplotlib_converters_tuple;
    tmp_level_name_2 = const_int_0;
    frame_21c02fa2e15e4cdad81fff1b8f4db482->m_frame.f_lineno = 495;
    tmp_import_name_from_2 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
    if ( tmp_import_name_from_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 495;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    tmp_assign_source_2 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_deregister_matplotlib_converters );
    Py_DECREF( tmp_import_name_from_2 );
    if ( tmp_assign_source_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 495;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    assert( var_deregister_matplotlib_converters == NULL );
    var_deregister_matplotlib_converters = tmp_assign_source_2;

    tmp_source_name_1 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_source_name_1 == NULL ))
    {
        tmp_source_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_source_name_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 497;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_get_option );
    if ( tmp_called_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 497;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    tmp_args_element_name_1 = par_key;

    if ( tmp_args_element_name_1 == NULL )
    {
        Py_DECREF( tmp_called_name_1 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "key" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 497;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    frame_21c02fa2e15e4cdad81fff1b8f4db482->m_frame.f_lineno = 497;
    {
        PyObject *call_args[] = { tmp_args_element_name_1 };
        tmp_cond_value_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
    }

    Py_DECREF( tmp_called_name_1 );
    if ( tmp_cond_value_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 497;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    tmp_cond_truth_1 = CHECK_IF_TRUE( tmp_cond_value_1 );
    if ( tmp_cond_truth_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_cond_value_1 );

        exception_lineno = 497;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_cond_value_1 );
    if ( tmp_cond_truth_1 == 1 )
    {
        goto branch_yes_1;
    }
    else
    {
        goto branch_no_1;
    }
    branch_yes_1:;
    tmp_called_name_2 = var_register_matplotlib_converters;

    if ( tmp_called_name_2 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "register_matplotlib_converters" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 498;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    frame_21c02fa2e15e4cdad81fff1b8f4db482->m_frame.f_lineno = 498;
    tmp_unused = CALL_FUNCTION_NO_ARGS( tmp_called_name_2 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 498;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_unused );
    goto branch_end_1;
    branch_no_1:;
    tmp_called_name_3 = var_deregister_matplotlib_converters;

    if ( tmp_called_name_3 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "deregister_matplotlib_converters" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 500;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    frame_21c02fa2e15e4cdad81fff1b8f4db482->m_frame.f_lineno = 500;
    tmp_unused = CALL_FUNCTION_NO_ARGS( tmp_called_name_3 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 500;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_unused );
    branch_end_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_21c02fa2e15e4cdad81fff1b8f4db482 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_21c02fa2e15e4cdad81fff1b8f4db482 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_21c02fa2e15e4cdad81fff1b8f4db482, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_21c02fa2e15e4cdad81fff1b8f4db482->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_21c02fa2e15e4cdad81fff1b8f4db482, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_21c02fa2e15e4cdad81fff1b8f4db482,
        type_description_1,
        par_key,
        var_register_matplotlib_converters,
        var_deregister_matplotlib_converters
    );


    // Release cached frame.
    if ( frame_21c02fa2e15e4cdad81fff1b8f4db482 == cache_frame_21c02fa2e15e4cdad81fff1b8f4db482 )
    {
        Py_DECREF( frame_21c02fa2e15e4cdad81fff1b8f4db482 );
    }
    cache_frame_21c02fa2e15e4cdad81fff1b8f4db482 = NULL;

    assertFrameObject( frame_21c02fa2e15e4cdad81fff1b8f4db482 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$core$config_init$$$function_5_register_converter_cb );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_key );
    par_key = NULL;

    Py_XDECREF( var_register_matplotlib_converters );
    var_register_matplotlib_converters = NULL;

    Py_XDECREF( var_deregister_matplotlib_converters );
    var_deregister_matplotlib_converters = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_key );
    par_key = NULL;

    Py_XDECREF( var_register_matplotlib_converters );
    var_register_matplotlib_converters = NULL;

    Py_XDECREF( var_deregister_matplotlib_converters );
    var_deregister_matplotlib_converters = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$core$config_init$$$function_5_register_converter_cb );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}



static PyObject *MAKE_FUNCTION_pandas$core$config_init$$$function_1_use_bottleneck_cb(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$core$config_init$$$function_1_use_bottleneck_cb,
        const_str_plain_use_bottleneck_cb,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_bc52bd7f8419602940b525b7bdd8b535,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$core$config_init,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$core$config_init$$$function_2_use_numexpr_cb(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$core$config_init$$$function_2_use_numexpr_cb,
        const_str_plain_use_numexpr_cb,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_8c240a030f351b8dbad36bc047195550,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$core$config_init,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$core$config_init$$$function_3_table_schema_cb(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$core$config_init$$$function_3_table_schema_cb,
        const_str_plain_table_schema_cb,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_2eecd91b75e20338b192c233ae1656d5,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$core$config_init,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$core$config_init$$$function_4_use_inf_as_na_cb(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$core$config_init$$$function_4_use_inf_as_na_cb,
        const_str_plain_use_inf_as_na_cb,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_32ae009b8fd949246dee06ecbfc1c516,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$core$config_init,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$core$config_init$$$function_5_register_converter_cb(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$core$config_init$$$function_5_register_converter_cb,
        const_str_plain_register_converter_cb,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_21c02fa2e15e4cdad81fff1b8f4db482,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$core$config_init,
        Py_None,
        0
    );


    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_pandas$core$config_init =
{
    PyModuleDef_HEAD_INIT,
    "pandas.core.config_init",   /* m_name */
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;

extern PyObject *const_str_plain___loader__;
extern PyObject *metapath_based_loader;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL.

MOD_INIT_DECL( pandas$core$config_init )
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_pandas$core$config_init );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("pandas.core.config_init: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("pandas.core.config_init: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("pandas.core.config_init: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initpandas$core$config_init" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_pandas$core$config_init = Py_InitModule4(
        "pandas.core.config_init",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_pandas$core$config_init = PyModule_Create( &mdef_pandas$core$config_init );
#endif

    moduledict_pandas$core$config_init = MODULE_DICT( module_pandas$core$config_init );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_pandas$core$config_init,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );

#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_pandas$core$config_init,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_pandas$core$config_init,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_pandas$core$config_init );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PySys_GetObject( (char *)"modules" ), const_str_digest_48ae02d7bc3fc61cbffb3f46dd54fdfa, module_pandas$core$config_init );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain___loader__, metapath_based_loader );
#endif

#if PYTHON_VERSION >= 340
#if 0
    UPDATE_STRING_DICT0( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain___name__ ),
            metapath_based_loader
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );

        UPDATE_STRING_DICT1( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );

        Py_DECREF( module_spec_class );
    }
#endif
#endif


    // Temp variables if any
    PyObject *tmp_import_from_1__module = NULL;
    PyObject *tmp_with_10__enter = NULL;
    PyObject *tmp_with_10__exit = NULL;
    PyObject *tmp_with_10__indicator = NULL;
    PyObject *tmp_with_10__source = NULL;
    PyObject *tmp_with_11__enter = NULL;
    PyObject *tmp_with_11__exit = NULL;
    PyObject *tmp_with_11__indicator = NULL;
    PyObject *tmp_with_11__source = NULL;
    PyObject *tmp_with_1__enter = NULL;
    PyObject *tmp_with_1__exit = NULL;
    PyObject *tmp_with_1__indicator = NULL;
    PyObject *tmp_with_1__source = NULL;
    PyObject *tmp_with_2__enter = NULL;
    PyObject *tmp_with_2__exit = NULL;
    PyObject *tmp_with_2__indicator = NULL;
    PyObject *tmp_with_2__source = NULL;
    PyObject *tmp_with_3__enter = NULL;
    PyObject *tmp_with_3__exit = NULL;
    PyObject *tmp_with_3__indicator = NULL;
    PyObject *tmp_with_3__source = NULL;
    PyObject *tmp_with_4__enter = NULL;
    PyObject *tmp_with_4__exit = NULL;
    PyObject *tmp_with_4__indicator = NULL;
    PyObject *tmp_with_4__source = NULL;
    PyObject *tmp_with_5__enter = NULL;
    PyObject *tmp_with_5__exit = NULL;
    PyObject *tmp_with_5__indicator = NULL;
    PyObject *tmp_with_5__source = NULL;
    PyObject *tmp_with_6__enter = NULL;
    PyObject *tmp_with_6__exit = NULL;
    PyObject *tmp_with_6__indicator = NULL;
    PyObject *tmp_with_6__source = NULL;
    PyObject *tmp_with_7__enter = NULL;
    PyObject *tmp_with_7__exit = NULL;
    PyObject *tmp_with_7__indicator = NULL;
    PyObject *tmp_with_7__source = NULL;
    PyObject *tmp_with_8__enter = NULL;
    PyObject *tmp_with_8__exit = NULL;
    PyObject *tmp_with_8__indicator = NULL;
    PyObject *tmp_with_8__source = NULL;
    PyObject *tmp_with_9__enter = NULL;
    PyObject *tmp_with_9__exit = NULL;
    PyObject *tmp_with_9__indicator = NULL;
    PyObject *tmp_with_9__source = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_9;
    PyObject *exception_keeper_type_10;
    PyObject *exception_keeper_value_10;
    PyTracebackObject *exception_keeper_tb_10;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_10;
    PyObject *exception_keeper_type_11;
    PyObject *exception_keeper_value_11;
    PyTracebackObject *exception_keeper_tb_11;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_11;
    PyObject *exception_keeper_type_12;
    PyObject *exception_keeper_value_12;
    PyTracebackObject *exception_keeper_tb_12;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_12;
    PyObject *exception_keeper_type_13;
    PyObject *exception_keeper_value_13;
    PyTracebackObject *exception_keeper_tb_13;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_13;
    PyObject *exception_keeper_type_14;
    PyObject *exception_keeper_value_14;
    PyTracebackObject *exception_keeper_tb_14;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_14;
    PyObject *exception_keeper_type_15;
    PyObject *exception_keeper_value_15;
    PyTracebackObject *exception_keeper_tb_15;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_15;
    PyObject *exception_keeper_type_16;
    PyObject *exception_keeper_value_16;
    PyTracebackObject *exception_keeper_tb_16;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_16;
    PyObject *exception_keeper_type_17;
    PyObject *exception_keeper_value_17;
    PyTracebackObject *exception_keeper_tb_17;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_17;
    PyObject *exception_keeper_type_18;
    PyObject *exception_keeper_value_18;
    PyTracebackObject *exception_keeper_tb_18;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_18;
    PyObject *exception_keeper_type_19;
    PyObject *exception_keeper_value_19;
    PyTracebackObject *exception_keeper_tb_19;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_19;
    PyObject *exception_keeper_type_20;
    PyObject *exception_keeper_value_20;
    PyTracebackObject *exception_keeper_tb_20;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_20;
    PyObject *exception_keeper_type_21;
    PyObject *exception_keeper_value_21;
    PyTracebackObject *exception_keeper_tb_21;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_21;
    PyObject *exception_keeper_type_22;
    PyObject *exception_keeper_value_22;
    PyTracebackObject *exception_keeper_tb_22;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_22;
    PyObject *exception_keeper_type_23;
    PyObject *exception_keeper_value_23;
    PyTracebackObject *exception_keeper_tb_23;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_23;
    PyObject *exception_keeper_type_24;
    PyObject *exception_keeper_value_24;
    PyTracebackObject *exception_keeper_tb_24;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_24;
    PyObject *exception_keeper_type_25;
    PyObject *exception_keeper_value_25;
    PyTracebackObject *exception_keeper_tb_25;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_25;
    PyObject *exception_keeper_type_26;
    PyObject *exception_keeper_value_26;
    PyTracebackObject *exception_keeper_tb_26;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_26;
    PyObject *exception_keeper_type_27;
    PyObject *exception_keeper_value_27;
    PyTracebackObject *exception_keeper_tb_27;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_27;
    PyObject *exception_keeper_type_28;
    PyObject *exception_keeper_value_28;
    PyTracebackObject *exception_keeper_tb_28;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_28;
    PyObject *exception_keeper_type_29;
    PyObject *exception_keeper_value_29;
    PyTracebackObject *exception_keeper_tb_29;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_29;
    PyObject *exception_keeper_type_30;
    PyObject *exception_keeper_value_30;
    PyTracebackObject *exception_keeper_tb_30;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_30;
    PyObject *exception_keeper_type_31;
    PyObject *exception_keeper_value_31;
    PyTracebackObject *exception_keeper_tb_31;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_31;
    PyObject *exception_keeper_type_32;
    PyObject *exception_keeper_value_32;
    PyTracebackObject *exception_keeper_tb_32;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_32;
    PyObject *exception_keeper_type_33;
    PyObject *exception_keeper_value_33;
    PyTracebackObject *exception_keeper_tb_33;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_33;
    PyObject *exception_keeper_type_34;
    PyObject *exception_keeper_value_34;
    PyTracebackObject *exception_keeper_tb_34;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_34;
    PyObject *exception_keeper_type_35;
    PyObject *exception_keeper_value_35;
    PyTracebackObject *exception_keeper_tb_35;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_35;
    PyObject *exception_keeper_type_36;
    PyObject *exception_keeper_value_36;
    PyTracebackObject *exception_keeper_tb_36;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_36;
    PyObject *exception_keeper_type_37;
    PyObject *exception_keeper_value_37;
    PyTracebackObject *exception_keeper_tb_37;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_37;
    PyObject *exception_keeper_type_38;
    PyObject *exception_keeper_value_38;
    PyTracebackObject *exception_keeper_tb_38;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_38;
    PyObject *exception_keeper_type_39;
    PyObject *exception_keeper_value_39;
    PyTracebackObject *exception_keeper_tb_39;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_39;
    PyObject *exception_keeper_type_40;
    PyObject *exception_keeper_value_40;
    PyTracebackObject *exception_keeper_tb_40;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_40;
    PyObject *exception_keeper_type_41;
    PyObject *exception_keeper_value_41;
    PyTracebackObject *exception_keeper_tb_41;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_41;
    PyObject *exception_keeper_type_42;
    PyObject *exception_keeper_value_42;
    PyTracebackObject *exception_keeper_tb_42;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_42;
    PyObject *exception_keeper_type_43;
    PyObject *exception_keeper_value_43;
    PyTracebackObject *exception_keeper_tb_43;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_43;
    PyObject *exception_keeper_type_44;
    PyObject *exception_keeper_value_44;
    PyTracebackObject *exception_keeper_tb_44;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_44;
    PyObject *exception_keeper_type_45;
    PyObject *exception_keeper_value_45;
    PyTracebackObject *exception_keeper_tb_45;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_45;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    PyObject *exception_preserved_type_2;
    PyObject *exception_preserved_value_2;
    PyTracebackObject *exception_preserved_tb_2;
    PyObject *exception_preserved_type_3;
    PyObject *exception_preserved_value_3;
    PyTracebackObject *exception_preserved_tb_3;
    PyObject *exception_preserved_type_4;
    PyObject *exception_preserved_value_4;
    PyTracebackObject *exception_preserved_tb_4;
    PyObject *exception_preserved_type_5;
    PyObject *exception_preserved_value_5;
    PyTracebackObject *exception_preserved_tb_5;
    PyObject *exception_preserved_type_6;
    PyObject *exception_preserved_value_6;
    PyTracebackObject *exception_preserved_tb_6;
    PyObject *exception_preserved_type_7;
    PyObject *exception_preserved_value_7;
    PyTracebackObject *exception_preserved_tb_7;
    PyObject *exception_preserved_type_8;
    PyObject *exception_preserved_value_8;
    PyTracebackObject *exception_preserved_tb_8;
    PyObject *exception_preserved_type_9;
    PyObject *exception_preserved_value_9;
    PyTracebackObject *exception_preserved_tb_9;
    PyObject *exception_preserved_type_10;
    PyObject *exception_preserved_value_10;
    PyTracebackObject *exception_preserved_tb_10;
    PyObject *exception_preserved_type_11;
    PyObject *exception_preserved_value_11;
    PyTracebackObject *exception_preserved_tb_11;
    PyObject *tmp_args_element_name_1;
    PyObject *tmp_args_element_name_2;
    PyObject *tmp_args_element_name_3;
    PyObject *tmp_args_element_name_4;
    PyObject *tmp_args_element_name_5;
    PyObject *tmp_args_element_name_6;
    PyObject *tmp_args_element_name_7;
    PyObject *tmp_args_element_name_8;
    PyObject *tmp_args_element_name_9;
    PyObject *tmp_args_element_name_10;
    PyObject *tmp_args_element_name_11;
    PyObject *tmp_args_element_name_12;
    PyObject *tmp_args_element_name_13;
    PyObject *tmp_args_element_name_14;
    PyObject *tmp_args_element_name_15;
    PyObject *tmp_args_element_name_16;
    PyObject *tmp_args_element_name_17;
    PyObject *tmp_args_element_name_18;
    PyObject *tmp_args_element_name_19;
    PyObject *tmp_args_element_name_20;
    PyObject *tmp_args_element_name_21;
    PyObject *tmp_args_element_name_22;
    PyObject *tmp_args_element_name_23;
    PyObject *tmp_args_element_name_24;
    PyObject *tmp_args_element_name_25;
    PyObject *tmp_args_element_name_26;
    PyObject *tmp_args_element_name_27;
    PyObject *tmp_args_element_name_28;
    PyObject *tmp_args_element_name_29;
    PyObject *tmp_args_element_name_30;
    PyObject *tmp_args_element_name_31;
    PyObject *tmp_args_element_name_32;
    PyObject *tmp_args_element_name_33;
    PyObject *tmp_args_element_name_34;
    PyObject *tmp_args_element_name_35;
    PyObject *tmp_args_element_name_36;
    PyObject *tmp_args_element_name_37;
    PyObject *tmp_args_element_name_38;
    PyObject *tmp_args_element_name_39;
    PyObject *tmp_args_element_name_40;
    PyObject *tmp_args_element_name_41;
    PyObject *tmp_args_element_name_42;
    PyObject *tmp_args_element_name_43;
    PyObject *tmp_args_element_name_44;
    PyObject *tmp_args_element_name_45;
    PyObject *tmp_args_element_name_46;
    PyObject *tmp_args_element_name_47;
    PyObject *tmp_args_element_name_48;
    PyObject *tmp_args_element_name_49;
    PyObject *tmp_args_name_1;
    PyObject *tmp_args_name_2;
    PyObject *tmp_args_name_3;
    PyObject *tmp_args_name_4;
    PyObject *tmp_args_name_5;
    PyObject *tmp_args_name_6;
    PyObject *tmp_args_name_7;
    PyObject *tmp_args_name_8;
    PyObject *tmp_args_name_9;
    PyObject *tmp_args_name_10;
    PyObject *tmp_args_name_11;
    PyObject *tmp_args_name_12;
    PyObject *tmp_args_name_13;
    PyObject *tmp_args_name_14;
    PyObject *tmp_args_name_15;
    PyObject *tmp_args_name_16;
    PyObject *tmp_args_name_17;
    PyObject *tmp_args_name_18;
    PyObject *tmp_args_name_19;
    PyObject *tmp_args_name_20;
    PyObject *tmp_args_name_21;
    PyObject *tmp_args_name_22;
    PyObject *tmp_args_name_23;
    PyObject *tmp_args_name_24;
    PyObject *tmp_args_name_25;
    PyObject *tmp_args_name_26;
    PyObject *tmp_args_name_27;
    PyObject *tmp_args_name_28;
    PyObject *tmp_args_name_29;
    PyObject *tmp_args_name_30;
    PyObject *tmp_args_name_31;
    PyObject *tmp_args_name_32;
    PyObject *tmp_args_name_33;
    PyObject *tmp_args_name_34;
    PyObject *tmp_args_name_35;
    PyObject *tmp_args_name_36;
    PyObject *tmp_args_name_37;
    PyObject *tmp_args_name_38;
    PyObject *tmp_args_name_39;
    PyObject *tmp_args_name_40;
    PyObject *tmp_args_name_41;
    PyObject *tmp_args_name_42;
    PyObject *tmp_args_name_43;
    PyObject *tmp_args_name_44;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_assign_source_5;
    PyObject *tmp_assign_source_6;
    PyObject *tmp_assign_source_7;
    PyObject *tmp_assign_source_8;
    PyObject *tmp_assign_source_9;
    PyObject *tmp_assign_source_10;
    PyObject *tmp_assign_source_11;
    PyObject *tmp_assign_source_12;
    PyObject *tmp_assign_source_13;
    PyObject *tmp_assign_source_14;
    PyObject *tmp_assign_source_15;
    PyObject *tmp_assign_source_16;
    PyObject *tmp_assign_source_17;
    PyObject *tmp_assign_source_18;
    PyObject *tmp_assign_source_19;
    PyObject *tmp_assign_source_20;
    PyObject *tmp_assign_source_21;
    PyObject *tmp_assign_source_22;
    PyObject *tmp_assign_source_23;
    PyObject *tmp_assign_source_24;
    PyObject *tmp_assign_source_25;
    PyObject *tmp_assign_source_26;
    PyObject *tmp_assign_source_27;
    PyObject *tmp_assign_source_28;
    PyObject *tmp_assign_source_29;
    PyObject *tmp_assign_source_30;
    PyObject *tmp_assign_source_31;
    PyObject *tmp_assign_source_32;
    PyObject *tmp_assign_source_33;
    PyObject *tmp_assign_source_34;
    PyObject *tmp_assign_source_35;
    PyObject *tmp_assign_source_36;
    PyObject *tmp_assign_source_37;
    PyObject *tmp_assign_source_38;
    PyObject *tmp_assign_source_39;
    PyObject *tmp_assign_source_40;
    PyObject *tmp_assign_source_41;
    PyObject *tmp_assign_source_42;
    PyObject *tmp_assign_source_43;
    PyObject *tmp_assign_source_44;
    PyObject *tmp_assign_source_45;
    PyObject *tmp_assign_source_46;
    PyObject *tmp_assign_source_47;
    PyObject *tmp_assign_source_48;
    PyObject *tmp_assign_source_49;
    PyObject *tmp_assign_source_50;
    PyObject *tmp_assign_source_51;
    PyObject *tmp_assign_source_52;
    PyObject *tmp_assign_source_53;
    PyObject *tmp_assign_source_54;
    PyObject *tmp_assign_source_55;
    PyObject *tmp_assign_source_56;
    PyObject *tmp_assign_source_57;
    PyObject *tmp_assign_source_58;
    PyObject *tmp_assign_source_59;
    PyObject *tmp_assign_source_60;
    PyObject *tmp_assign_source_61;
    PyObject *tmp_assign_source_62;
    PyObject *tmp_assign_source_63;
    PyObject *tmp_assign_source_64;
    PyObject *tmp_assign_source_65;
    PyObject *tmp_assign_source_66;
    PyObject *tmp_assign_source_67;
    PyObject *tmp_assign_source_68;
    PyObject *tmp_assign_source_69;
    PyObject *tmp_assign_source_70;
    PyObject *tmp_assign_source_71;
    PyObject *tmp_assign_source_72;
    PyObject *tmp_assign_source_73;
    PyObject *tmp_assign_source_74;
    PyObject *tmp_assign_source_75;
    PyObject *tmp_assign_source_76;
    PyObject *tmp_assign_source_77;
    PyObject *tmp_assign_source_78;
    PyObject *tmp_assign_source_79;
    PyObject *tmp_assign_source_80;
    PyObject *tmp_assign_source_81;
    PyObject *tmp_assign_source_82;
    PyObject *tmp_assign_source_83;
    PyObject *tmp_assign_source_84;
    PyObject *tmp_assign_source_85;
    PyObject *tmp_assign_source_86;
    PyObject *tmp_assign_source_87;
    PyObject *tmp_assign_source_88;
    PyObject *tmp_assign_source_89;
    PyObject *tmp_assign_source_90;
    PyObject *tmp_assign_source_91;
    PyObject *tmp_assign_source_92;
    PyObject *tmp_assign_source_93;
    PyObject *tmp_assign_source_94;
    PyObject *tmp_assign_source_95;
    PyObject *tmp_assign_source_96;
    PyObject *tmp_assign_source_97;
    PyObject *tmp_assign_source_98;
    PyObject *tmp_assign_source_99;
    PyObject *tmp_assign_source_100;
    PyObject *tmp_assign_source_101;
    PyObject *tmp_assign_source_102;
    PyObject *tmp_assign_source_103;
    PyObject *tmp_assign_source_104;
    PyObject *tmp_assign_source_105;
    PyObject *tmp_assign_source_106;
    PyObject *tmp_assign_source_107;
    PyObject *tmp_assign_source_108;
    PyObject *tmp_assign_source_109;
    PyObject *tmp_assign_source_110;
    PyObject *tmp_assign_source_111;
    PyObject *tmp_assign_source_112;
    PyObject *tmp_assign_source_113;
    PyObject *tmp_assign_source_114;
    PyObject *tmp_assign_source_115;
    PyObject *tmp_assign_source_116;
    PyObject *tmp_assign_source_117;
    PyObject *tmp_assign_source_118;
    PyObject *tmp_assign_source_119;
    PyObject *tmp_assign_source_120;
    PyObject *tmp_assign_source_121;
    PyObject *tmp_assign_source_122;
    PyObject *tmp_assign_source_123;
    PyObject *tmp_call_arg_element_1;
    PyObject *tmp_call_arg_element_2;
    PyObject *tmp_call_arg_element_3;
    PyObject *tmp_call_arg_element_4;
    PyObject *tmp_call_arg_element_5;
    PyObject *tmp_call_arg_element_6;
    PyObject *tmp_call_arg_element_7;
    PyObject *tmp_call_arg_element_8;
    PyObject *tmp_called_instance_1;
    PyObject *tmp_called_instance_2;
    PyObject *tmp_called_instance_3;
    PyObject *tmp_called_instance_4;
    PyObject *tmp_called_instance_5;
    PyObject *tmp_called_instance_6;
    PyObject *tmp_called_instance_7;
    PyObject *tmp_called_instance_8;
    PyObject *tmp_called_instance_9;
    PyObject *tmp_called_instance_10;
    PyObject *tmp_called_instance_11;
    PyObject *tmp_called_name_1;
    PyObject *tmp_called_name_2;
    PyObject *tmp_called_name_3;
    PyObject *tmp_called_name_4;
    PyObject *tmp_called_name_5;
    PyObject *tmp_called_name_6;
    PyObject *tmp_called_name_7;
    PyObject *tmp_called_name_8;
    PyObject *tmp_called_name_9;
    PyObject *tmp_called_name_10;
    PyObject *tmp_called_name_11;
    PyObject *tmp_called_name_12;
    PyObject *tmp_called_name_13;
    PyObject *tmp_called_name_14;
    PyObject *tmp_called_name_15;
    PyObject *tmp_called_name_16;
    PyObject *tmp_called_name_17;
    PyObject *tmp_called_name_18;
    PyObject *tmp_called_name_19;
    PyObject *tmp_called_name_20;
    PyObject *tmp_called_name_21;
    PyObject *tmp_called_name_22;
    PyObject *tmp_called_name_23;
    PyObject *tmp_called_name_24;
    PyObject *tmp_called_name_25;
    PyObject *tmp_called_name_26;
    PyObject *tmp_called_name_27;
    PyObject *tmp_called_name_28;
    PyObject *tmp_called_name_29;
    PyObject *tmp_called_name_30;
    PyObject *tmp_called_name_31;
    PyObject *tmp_called_name_32;
    PyObject *tmp_called_name_33;
    PyObject *tmp_called_name_34;
    PyObject *tmp_called_name_35;
    PyObject *tmp_called_name_36;
    PyObject *tmp_called_name_37;
    PyObject *tmp_called_name_38;
    PyObject *tmp_called_name_39;
    PyObject *tmp_called_name_40;
    PyObject *tmp_called_name_41;
    PyObject *tmp_called_name_42;
    PyObject *tmp_called_name_43;
    PyObject *tmp_called_name_44;
    PyObject *tmp_called_name_45;
    PyObject *tmp_called_name_46;
    PyObject *tmp_called_name_47;
    PyObject *tmp_called_name_48;
    PyObject *tmp_called_name_49;
    PyObject *tmp_called_name_50;
    PyObject *tmp_called_name_51;
    PyObject *tmp_called_name_52;
    PyObject *tmp_called_name_53;
    PyObject *tmp_called_name_54;
    PyObject *tmp_called_name_55;
    PyObject *tmp_called_name_56;
    PyObject *tmp_called_name_57;
    PyObject *tmp_called_name_58;
    PyObject *tmp_called_name_59;
    PyObject *tmp_called_name_60;
    PyObject *tmp_called_name_61;
    PyObject *tmp_called_name_62;
    PyObject *tmp_called_name_63;
    PyObject *tmp_called_name_64;
    PyObject *tmp_called_name_65;
    PyObject *tmp_called_name_66;
    PyObject *tmp_called_name_67;
    PyObject *tmp_called_name_68;
    PyObject *tmp_called_name_69;
    PyObject *tmp_called_name_70;
    PyObject *tmp_called_name_71;
    PyObject *tmp_called_name_72;
    PyObject *tmp_called_name_73;
    PyObject *tmp_called_name_74;
    PyObject *tmp_called_name_75;
    PyObject *tmp_called_name_76;
    PyObject *tmp_called_name_77;
    PyObject *tmp_called_name_78;
    PyObject *tmp_called_name_79;
    PyObject *tmp_called_name_80;
    PyObject *tmp_called_name_81;
    PyObject *tmp_called_name_82;
    PyObject *tmp_called_name_83;
    PyObject *tmp_called_name_84;
    PyObject *tmp_called_name_85;
    PyObject *tmp_called_name_86;
    PyObject *tmp_called_name_87;
    PyObject *tmp_called_name_88;
    PyObject *tmp_called_name_89;
    PyObject *tmp_called_name_90;
    PyObject *tmp_called_name_91;
    PyObject *tmp_called_name_92;
    PyObject *tmp_called_name_93;
    PyObject *tmp_called_name_94;
    PyObject *tmp_called_name_95;
    PyObject *tmp_called_name_96;
    PyObject *tmp_called_name_97;
    PyObject *tmp_called_name_98;
    PyObject *tmp_called_name_99;
    PyObject *tmp_called_name_100;
    PyObject *tmp_called_name_101;
    PyObject *tmp_called_name_102;
    PyObject *tmp_called_name_103;
    PyObject *tmp_called_name_104;
    PyObject *tmp_called_name_105;
    PyObject *tmp_called_name_106;
    PyObject *tmp_called_name_107;
    PyObject *tmp_called_name_108;
    PyObject *tmp_called_name_109;
    PyObject *tmp_called_name_110;
    PyObject *tmp_compare_left_1;
    PyObject *tmp_compare_left_2;
    PyObject *tmp_compare_left_3;
    PyObject *tmp_compare_left_4;
    PyObject *tmp_compare_left_5;
    PyObject *tmp_compare_left_6;
    PyObject *tmp_compare_left_7;
    PyObject *tmp_compare_left_8;
    PyObject *tmp_compare_left_9;
    PyObject *tmp_compare_left_10;
    PyObject *tmp_compare_left_11;
    PyObject *tmp_compare_left_12;
    PyObject *tmp_compare_left_13;
    PyObject *tmp_compare_left_14;
    PyObject *tmp_compare_left_15;
    PyObject *tmp_compare_left_16;
    PyObject *tmp_compare_left_17;
    PyObject *tmp_compare_left_18;
    PyObject *tmp_compare_left_19;
    PyObject *tmp_compare_left_20;
    PyObject *tmp_compare_left_21;
    PyObject *tmp_compare_left_22;
    PyObject *tmp_compare_left_23;
    PyObject *tmp_compare_left_24;
    PyObject *tmp_compare_left_25;
    PyObject *tmp_compare_left_26;
    PyObject *tmp_compare_left_27;
    PyObject *tmp_compare_left_28;
    PyObject *tmp_compare_left_29;
    PyObject *tmp_compare_left_30;
    PyObject *tmp_compare_left_31;
    PyObject *tmp_compare_left_32;
    PyObject *tmp_compare_left_33;
    PyObject *tmp_compare_right_1;
    PyObject *tmp_compare_right_2;
    PyObject *tmp_compare_right_3;
    PyObject *tmp_compare_right_4;
    PyObject *tmp_compare_right_5;
    PyObject *tmp_compare_right_6;
    PyObject *tmp_compare_right_7;
    PyObject *tmp_compare_right_8;
    PyObject *tmp_compare_right_9;
    PyObject *tmp_compare_right_10;
    PyObject *tmp_compare_right_11;
    PyObject *tmp_compare_right_12;
    PyObject *tmp_compare_right_13;
    PyObject *tmp_compare_right_14;
    PyObject *tmp_compare_right_15;
    PyObject *tmp_compare_right_16;
    PyObject *tmp_compare_right_17;
    PyObject *tmp_compare_right_18;
    PyObject *tmp_compare_right_19;
    PyObject *tmp_compare_right_20;
    PyObject *tmp_compare_right_21;
    PyObject *tmp_compare_right_22;
    PyObject *tmp_compare_right_23;
    PyObject *tmp_compare_right_24;
    PyObject *tmp_compare_right_25;
    PyObject *tmp_compare_right_26;
    PyObject *tmp_compare_right_27;
    PyObject *tmp_compare_right_28;
    PyObject *tmp_compare_right_29;
    PyObject *tmp_compare_right_30;
    PyObject *tmp_compare_right_31;
    PyObject *tmp_compare_right_32;
    PyObject *tmp_compare_right_33;
    int tmp_cond_truth_1;
    int tmp_cond_truth_2;
    int tmp_cond_truth_3;
    int tmp_cond_truth_4;
    int tmp_cond_truth_5;
    int tmp_cond_truth_6;
    int tmp_cond_truth_7;
    int tmp_cond_truth_8;
    int tmp_cond_truth_9;
    int tmp_cond_truth_10;
    int tmp_cond_truth_11;
    int tmp_cond_truth_12;
    PyObject *tmp_cond_value_1;
    PyObject *tmp_cond_value_2;
    PyObject *tmp_cond_value_3;
    PyObject *tmp_cond_value_4;
    PyObject *tmp_cond_value_5;
    PyObject *tmp_cond_value_6;
    PyObject *tmp_cond_value_7;
    PyObject *tmp_cond_value_8;
    PyObject *tmp_cond_value_9;
    PyObject *tmp_cond_value_10;
    PyObject *tmp_cond_value_11;
    PyObject *tmp_cond_value_12;
    PyObject *tmp_dict_key_1;
    PyObject *tmp_dict_key_2;
    PyObject *tmp_dict_key_3;
    PyObject *tmp_dict_key_4;
    PyObject *tmp_dict_key_5;
    PyObject *tmp_dict_key_6;
    PyObject *tmp_dict_key_7;
    PyObject *tmp_dict_key_8;
    PyObject *tmp_dict_key_9;
    PyObject *tmp_dict_key_10;
    PyObject *tmp_dict_key_11;
    PyObject *tmp_dict_key_12;
    PyObject *tmp_dict_key_13;
    PyObject *tmp_dict_key_14;
    PyObject *tmp_dict_key_15;
    PyObject *tmp_dict_key_16;
    PyObject *tmp_dict_key_17;
    PyObject *tmp_dict_key_18;
    PyObject *tmp_dict_key_19;
    PyObject *tmp_dict_key_20;
    PyObject *tmp_dict_key_21;
    PyObject *tmp_dict_key_22;
    PyObject *tmp_dict_key_23;
    PyObject *tmp_dict_key_24;
    PyObject *tmp_dict_key_25;
    PyObject *tmp_dict_key_26;
    PyObject *tmp_dict_key_27;
    PyObject *tmp_dict_key_28;
    PyObject *tmp_dict_key_29;
    PyObject *tmp_dict_key_30;
    PyObject *tmp_dict_key_31;
    PyObject *tmp_dict_key_32;
    PyObject *tmp_dict_key_33;
    PyObject *tmp_dict_key_34;
    PyObject *tmp_dict_key_35;
    PyObject *tmp_dict_key_36;
    PyObject *tmp_dict_key_37;
    PyObject *tmp_dict_key_38;
    PyObject *tmp_dict_key_39;
    PyObject *tmp_dict_key_40;
    PyObject *tmp_dict_key_41;
    PyObject *tmp_dict_key_42;
    PyObject *tmp_dict_key_43;
    PyObject *tmp_dict_key_44;
    PyObject *tmp_dict_key_45;
    PyObject *tmp_dict_key_46;
    PyObject *tmp_dict_key_47;
    PyObject *tmp_dict_key_48;
    PyObject *tmp_dict_key_49;
    PyObject *tmp_dict_key_50;
    PyObject *tmp_dict_key_51;
    PyObject *tmp_dict_key_52;
    PyObject *tmp_dict_key_53;
    PyObject *tmp_dict_value_1;
    PyObject *tmp_dict_value_2;
    PyObject *tmp_dict_value_3;
    PyObject *tmp_dict_value_4;
    PyObject *tmp_dict_value_5;
    PyObject *tmp_dict_value_6;
    PyObject *tmp_dict_value_7;
    PyObject *tmp_dict_value_8;
    PyObject *tmp_dict_value_9;
    PyObject *tmp_dict_value_10;
    PyObject *tmp_dict_value_11;
    PyObject *tmp_dict_value_12;
    PyObject *tmp_dict_value_13;
    PyObject *tmp_dict_value_14;
    PyObject *tmp_dict_value_15;
    PyObject *tmp_dict_value_16;
    PyObject *tmp_dict_value_17;
    PyObject *tmp_dict_value_18;
    PyObject *tmp_dict_value_19;
    PyObject *tmp_dict_value_20;
    PyObject *tmp_dict_value_21;
    PyObject *tmp_dict_value_22;
    PyObject *tmp_dict_value_23;
    PyObject *tmp_dict_value_24;
    PyObject *tmp_dict_value_25;
    PyObject *tmp_dict_value_26;
    PyObject *tmp_dict_value_27;
    PyObject *tmp_dict_value_28;
    PyObject *tmp_dict_value_29;
    PyObject *tmp_dict_value_30;
    PyObject *tmp_dict_value_31;
    PyObject *tmp_dict_value_32;
    PyObject *tmp_dict_value_33;
    PyObject *tmp_dict_value_34;
    PyObject *tmp_dict_value_35;
    PyObject *tmp_dict_value_36;
    PyObject *tmp_dict_value_37;
    PyObject *tmp_dict_value_38;
    PyObject *tmp_dict_value_39;
    PyObject *tmp_dict_value_40;
    PyObject *tmp_dict_value_41;
    PyObject *tmp_dict_value_42;
    PyObject *tmp_dict_value_43;
    PyObject *tmp_dict_value_44;
    PyObject *tmp_dict_value_45;
    PyObject *tmp_dict_value_46;
    PyObject *tmp_dict_value_47;
    PyObject *tmp_dict_value_48;
    PyObject *tmp_dict_value_49;
    PyObject *tmp_dict_value_50;
    PyObject *tmp_dict_value_51;
    PyObject *tmp_dict_value_52;
    PyObject *tmp_dict_value_53;
    int tmp_exc_match_exception_match_1;
    int tmp_exc_match_exception_match_2;
    int tmp_exc_match_exception_match_3;
    int tmp_exc_match_exception_match_4;
    int tmp_exc_match_exception_match_5;
    int tmp_exc_match_exception_match_6;
    int tmp_exc_match_exception_match_7;
    int tmp_exc_match_exception_match_8;
    int tmp_exc_match_exception_match_9;
    int tmp_exc_match_exception_match_10;
    int tmp_exc_match_exception_match_11;
    PyObject *tmp_fromlist_name_1;
    PyObject *tmp_fromlist_name_2;
    PyObject *tmp_fromlist_name_3;
    PyObject *tmp_fromlist_name_4;
    PyObject *tmp_globals_name_1;
    PyObject *tmp_globals_name_2;
    PyObject *tmp_globals_name_3;
    PyObject *tmp_globals_name_4;
    PyObject *tmp_import_name_from_1;
    PyObject *tmp_import_name_from_2;
    PyObject *tmp_import_name_from_3;
    PyObject *tmp_import_name_from_4;
    PyObject *tmp_import_name_from_5;
    PyObject *tmp_import_name_from_6;
    PyObject *tmp_import_name_from_7;
    PyObject *tmp_import_name_from_8;
    PyObject *tmp_import_name_from_9;
    PyObject *tmp_import_name_from_10;
    bool tmp_is_1;
    bool tmp_is_2;
    bool tmp_is_3;
    bool tmp_is_4;
    bool tmp_is_5;
    bool tmp_is_6;
    bool tmp_is_7;
    bool tmp_is_8;
    bool tmp_is_9;
    bool tmp_is_10;
    bool tmp_is_11;
    bool tmp_is_12;
    bool tmp_is_13;
    bool tmp_is_14;
    bool tmp_is_15;
    bool tmp_is_16;
    bool tmp_is_17;
    bool tmp_is_18;
    bool tmp_is_19;
    bool tmp_is_20;
    bool tmp_is_21;
    bool tmp_is_22;
    PyObject *tmp_kw_name_1;
    PyObject *tmp_kw_name_2;
    PyObject *tmp_kw_name_3;
    PyObject *tmp_kw_name_4;
    PyObject *tmp_kw_name_5;
    PyObject *tmp_kw_name_6;
    PyObject *tmp_kw_name_7;
    PyObject *tmp_kw_name_8;
    PyObject *tmp_kw_name_9;
    PyObject *tmp_kw_name_10;
    PyObject *tmp_kw_name_11;
    PyObject *tmp_kw_name_12;
    PyObject *tmp_kw_name_13;
    PyObject *tmp_kw_name_14;
    PyObject *tmp_kw_name_15;
    PyObject *tmp_kw_name_16;
    PyObject *tmp_kw_name_17;
    PyObject *tmp_kw_name_18;
    PyObject *tmp_kw_name_19;
    PyObject *tmp_kw_name_20;
    PyObject *tmp_kw_name_21;
    PyObject *tmp_kw_name_22;
    PyObject *tmp_kw_name_23;
    PyObject *tmp_kw_name_24;
    PyObject *tmp_kw_name_25;
    PyObject *tmp_kw_name_26;
    PyObject *tmp_kw_name_27;
    PyObject *tmp_kw_name_28;
    PyObject *tmp_kw_name_29;
    PyObject *tmp_kw_name_30;
    PyObject *tmp_kw_name_31;
    PyObject *tmp_kw_name_32;
    PyObject *tmp_kw_name_33;
    PyObject *tmp_kw_name_34;
    PyObject *tmp_kw_name_35;
    PyObject *tmp_kw_name_36;
    PyObject *tmp_kw_name_37;
    PyObject *tmp_kw_name_38;
    PyObject *tmp_kw_name_39;
    PyObject *tmp_kw_name_40;
    PyObject *tmp_kw_name_41;
    PyObject *tmp_kw_name_42;
    PyObject *tmp_kw_name_43;
    PyObject *tmp_kw_name_44;
    PyObject *tmp_kw_name_45;
    PyObject *tmp_kw_name_46;
    PyObject *tmp_kw_name_47;
    PyObject *tmp_level_name_1;
    PyObject *tmp_level_name_2;
    PyObject *tmp_level_name_3;
    PyObject *tmp_level_name_4;
    PyObject *tmp_list_element_1;
    PyObject *tmp_locals_name_1;
    PyObject *tmp_locals_name_2;
    PyObject *tmp_locals_name_3;
    PyObject *tmp_locals_name_4;
    PyObject *tmp_name_name_1;
    PyObject *tmp_name_name_2;
    PyObject *tmp_name_name_3;
    PyObject *tmp_name_name_4;
    int tmp_res;
    bool tmp_result;
    PyObject *tmp_source_name_1;
    PyObject *tmp_source_name_2;
    PyObject *tmp_source_name_3;
    PyObject *tmp_source_name_4;
    PyObject *tmp_source_name_5;
    PyObject *tmp_source_name_6;
    PyObject *tmp_source_name_7;
    PyObject *tmp_source_name_8;
    PyObject *tmp_source_name_9;
    PyObject *tmp_source_name_10;
    PyObject *tmp_source_name_11;
    PyObject *tmp_source_name_12;
    PyObject *tmp_source_name_13;
    PyObject *tmp_source_name_14;
    PyObject *tmp_source_name_15;
    PyObject *tmp_source_name_16;
    PyObject *tmp_source_name_17;
    PyObject *tmp_source_name_18;
    PyObject *tmp_source_name_19;
    PyObject *tmp_source_name_20;
    PyObject *tmp_source_name_21;
    PyObject *tmp_source_name_22;
    PyObject *tmp_source_name_23;
    PyObject *tmp_source_name_24;
    PyObject *tmp_source_name_25;
    PyObject *tmp_source_name_26;
    PyObject *tmp_source_name_27;
    PyObject *tmp_source_name_28;
    PyObject *tmp_source_name_29;
    PyObject *tmp_source_name_30;
    PyObject *tmp_source_name_31;
    PyObject *tmp_source_name_32;
    PyObject *tmp_source_name_33;
    PyObject *tmp_source_name_34;
    PyObject *tmp_source_name_35;
    PyObject *tmp_source_name_36;
    PyObject *tmp_source_name_37;
    PyObject *tmp_source_name_38;
    PyObject *tmp_source_name_39;
    PyObject *tmp_source_name_40;
    PyObject *tmp_source_name_41;
    PyObject *tmp_source_name_42;
    PyObject *tmp_source_name_43;
    PyObject *tmp_source_name_44;
    PyObject *tmp_source_name_45;
    PyObject *tmp_source_name_46;
    PyObject *tmp_source_name_47;
    PyObject *tmp_source_name_48;
    PyObject *tmp_source_name_49;
    PyObject *tmp_source_name_50;
    PyObject *tmp_source_name_51;
    PyObject *tmp_source_name_52;
    PyObject *tmp_source_name_53;
    PyObject *tmp_source_name_54;
    PyObject *tmp_source_name_55;
    PyObject *tmp_source_name_56;
    PyObject *tmp_source_name_57;
    PyObject *tmp_source_name_58;
    PyObject *tmp_source_name_59;
    PyObject *tmp_source_name_60;
    PyObject *tmp_source_name_61;
    PyObject *tmp_source_name_62;
    PyObject *tmp_source_name_63;
    PyObject *tmp_source_name_64;
    PyObject *tmp_source_name_65;
    PyObject *tmp_source_name_66;
    PyObject *tmp_source_name_67;
    PyObject *tmp_source_name_68;
    PyObject *tmp_source_name_69;
    PyObject *tmp_source_name_70;
    PyObject *tmp_source_name_71;
    PyObject *tmp_source_name_72;
    PyObject *tmp_source_name_73;
    PyObject *tmp_source_name_74;
    PyObject *tmp_source_name_75;
    PyObject *tmp_source_name_76;
    PyObject *tmp_tuple_element_1;
    PyObject *tmp_tuple_element_2;
    PyObject *tmp_tuple_element_3;
    PyObject *tmp_tuple_element_4;
    PyObject *tmp_tuple_element_5;
    PyObject *tmp_tuple_element_6;
    PyObject *tmp_tuple_element_7;
    PyObject *tmp_tuple_element_8;
    PyObject *tmp_tuple_element_9;
    PyObject *tmp_tuple_element_10;
    PyObject *tmp_tuple_element_11;
    PyObject *tmp_tuple_element_12;
    PyObject *tmp_tuple_element_13;
    PyObject *tmp_tuple_element_14;
    PyObject *tmp_tuple_element_15;
    PyObject *tmp_tuple_element_16;
    PyObject *tmp_tuple_element_17;
    PyObject *tmp_tuple_element_18;
    PyObject *tmp_tuple_element_19;
    PyObject *tmp_tuple_element_20;
    PyObject *tmp_tuple_element_21;
    PyObject *tmp_tuple_element_22;
    PyObject *tmp_tuple_element_23;
    PyObject *tmp_tuple_element_24;
    PyObject *tmp_tuple_element_25;
    PyObject *tmp_tuple_element_26;
    PyObject *tmp_tuple_element_27;
    PyObject *tmp_tuple_element_28;
    PyObject *tmp_tuple_element_29;
    PyObject *tmp_tuple_element_30;
    PyObject *tmp_tuple_element_31;
    PyObject *tmp_tuple_element_32;
    PyObject *tmp_tuple_element_33;
    PyObject *tmp_tuple_element_34;
    PyObject *tmp_tuple_element_35;
    PyObject *tmp_tuple_element_36;
    PyObject *tmp_tuple_element_37;
    PyObject *tmp_tuple_element_38;
    PyObject *tmp_tuple_element_39;
    PyObject *tmp_tuple_element_40;
    PyObject *tmp_tuple_element_41;
    NUITKA_MAY_BE_UNUSED PyObject *tmp_unused;
    struct Nuitka_FrameObject *frame_3209efbe1fc5d2857a97a7e36ce015f8;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;

    // Module code.
    tmp_assign_source_1 = const_str_digest_55909c86f77471fd84b96a2c560d530c;
    UPDATE_STRING_DICT0( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    tmp_assign_source_2 = const_str_digest_f2956f16d5c6b095746cfa96ebbd29c0;
    UPDATE_STRING_DICT0( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    tmp_assign_source_3 = Py_None;
    UPDATE_STRING_DICT0( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    // Frame without reuse.
    frame_3209efbe1fc5d2857a97a7e36ce015f8 = MAKE_MODULE_FRAME( codeobj_3209efbe1fc5d2857a97a7e36ce015f8, module_pandas$core$config_init );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_3209efbe1fc5d2857a97a7e36ce015f8 );
    assert( Py_REFCNT( frame_3209efbe1fc5d2857a97a7e36ce015f8 ) == 2 );

    // Framed code:
    tmp_name_name_1 = const_str_digest_53eb620cf819e7f2e2a90c9ff72c4e4b;
    tmp_globals_name_1 = (PyObject *)moduledict_pandas$core$config_init;
    tmp_locals_name_1 = Py_None;
    tmp_fromlist_name_1 = Py_None;
    tmp_level_name_1 = const_int_0;
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 12;
    tmp_import_name_from_2 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
    if ( tmp_import_name_from_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 12;

        goto frame_exception_exit_1;
    }
    tmp_import_name_from_1 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_core );
    Py_DECREF( tmp_import_name_from_2 );
    if ( tmp_import_name_from_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 12;

        goto frame_exception_exit_1;
    }
    tmp_assign_source_4 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_config );
    Py_DECREF( tmp_import_name_from_1 );
    if ( tmp_assign_source_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 12;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf, tmp_assign_source_4 );
    tmp_name_name_2 = const_str_digest_53eb620cf819e7f2e2a90c9ff72c4e4b;
    tmp_globals_name_2 = (PyObject *)moduledict_pandas$core$config_init;
    tmp_locals_name_2 = Py_None;
    tmp_fromlist_name_2 = const_tuple_d384e95b8089e28989357db5c2fae7f2_tuple;
    tmp_level_name_2 = const_int_0;
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 13;
    tmp_assign_source_5 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
    if ( tmp_assign_source_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 13;

        goto frame_exception_exit_1;
    }
    assert( tmp_import_from_1__module == NULL );
    tmp_import_from_1__module = tmp_assign_source_5;

    // Tried code:
    tmp_import_name_from_3 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_3 );
    tmp_assign_source_6 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_is_int );
    if ( tmp_assign_source_6 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 13;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_is_int, tmp_assign_source_6 );
    tmp_import_name_from_4 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_4 );
    tmp_assign_source_7 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_is_bool );
    if ( tmp_assign_source_7 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 13;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_is_bool, tmp_assign_source_7 );
    tmp_import_name_from_5 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_5 );
    tmp_assign_source_8 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_is_text );
    if ( tmp_assign_source_8 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 13;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_is_text, tmp_assign_source_8 );
    tmp_import_name_from_6 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_6 );
    tmp_assign_source_9 = IMPORT_NAME( tmp_import_name_from_6, const_str_plain_is_instance_factory );
    if ( tmp_assign_source_9 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 13;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_is_instance_factory, tmp_assign_source_9 );
    tmp_import_name_from_7 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_7 );
    tmp_assign_source_10 = IMPORT_NAME( tmp_import_name_from_7, const_str_plain_is_one_of_factory );
    if ( tmp_assign_source_10 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 13;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_is_one_of_factory, tmp_assign_source_10 );
    tmp_import_name_from_8 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_8 );
    tmp_assign_source_11 = IMPORT_NAME( tmp_import_name_from_8, const_str_plain_is_callable );
    if ( tmp_assign_source_11 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 13;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_is_callable, tmp_assign_source_11 );
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    tmp_name_name_3 = const_str_digest_0b45bd1227a3ca701809b4b137e5400c;
    tmp_globals_name_3 = (PyObject *)moduledict_pandas$core$config_init;
    tmp_locals_name_3 = Py_None;
    tmp_fromlist_name_3 = const_tuple_str_plain_detect_console_encoding_tuple;
    tmp_level_name_3 = const_int_0;
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 15;
    tmp_import_name_from_9 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
    if ( tmp_import_name_from_9 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 15;

        goto frame_exception_exit_1;
    }
    tmp_assign_source_12 = IMPORT_NAME( tmp_import_name_from_9, const_str_plain_detect_console_encoding );
    Py_DECREF( tmp_import_name_from_9 );
    if ( tmp_assign_source_12 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 15;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_detect_console_encoding, tmp_assign_source_12 );
    tmp_name_name_4 = const_str_digest_b3f0e8a09cdae8f83a14da645f450b0f;
    tmp_globals_name_4 = (PyObject *)moduledict_pandas$core$config_init;
    tmp_locals_name_4 = Py_None;
    tmp_fromlist_name_4 = const_tuple_str_plain_is_terminal_tuple;
    tmp_level_name_4 = const_int_0;
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 16;
    tmp_import_name_from_10 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
    if ( tmp_import_name_from_10 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 16;

        goto frame_exception_exit_1;
    }
    tmp_assign_source_13 = IMPORT_NAME( tmp_import_name_from_10, const_str_plain_is_terminal );
    Py_DECREF( tmp_import_name_from_10 );
    if ( tmp_assign_source_13 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 16;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_is_terminal, tmp_assign_source_13 );
    tmp_assign_source_14 = const_str_digest_86f291ce802a3d5c7c1dc1d9671e3230;
    UPDATE_STRING_DICT0( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_use_bottleneck_doc, tmp_assign_source_14 );
    tmp_assign_source_15 = MAKE_FUNCTION_pandas$core$config_init$$$function_1_use_bottleneck_cb(  );
    UPDATE_STRING_DICT1( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_use_bottleneck_cb, tmp_assign_source_15 );
    tmp_assign_source_16 = const_str_digest_f5fa2e7b82b0176dddfc8ec97f30c139;
    UPDATE_STRING_DICT0( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_use_numexpr_doc, tmp_assign_source_16 );
    tmp_assign_source_17 = MAKE_FUNCTION_pandas$core$config_init$$$function_2_use_numexpr_cb(  );
    UPDATE_STRING_DICT1( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_use_numexpr_cb, tmp_assign_source_17 );
    // Tried code:
    tmp_called_instance_1 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_called_instance_1 == NULL ))
    {
        tmp_called_instance_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_called_instance_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 46;

        goto try_except_handler_2;
    }

    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 46;
    tmp_assign_source_18 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_config_prefix, &PyTuple_GET_ITEM( const_tuple_str_plain_compute_tuple, 0 ) );

    if ( tmp_assign_source_18 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 46;

        goto try_except_handler_2;
    }
    assert( tmp_with_1__source == NULL );
    tmp_with_1__source = tmp_assign_source_18;

    tmp_source_name_1 = tmp_with_1__source;

    CHECK_OBJECT( tmp_source_name_1 );
    tmp_called_name_1 = LOOKUP_SPECIAL( tmp_source_name_1, const_str_plain___enter__ );
    if ( tmp_called_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 46;

        goto try_except_handler_2;
    }
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 46;
    tmp_assign_source_19 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
    Py_DECREF( tmp_called_name_1 );
    if ( tmp_assign_source_19 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 46;

        goto try_except_handler_2;
    }
    assert( tmp_with_1__enter == NULL );
    tmp_with_1__enter = tmp_assign_source_19;

    tmp_source_name_2 = tmp_with_1__source;

    CHECK_OBJECT( tmp_source_name_2 );
    tmp_assign_source_20 = LOOKUP_SPECIAL( tmp_source_name_2, const_str_plain___exit__ );
    if ( tmp_assign_source_20 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 46;

        goto try_except_handler_2;
    }
    assert( tmp_with_1__exit == NULL );
    tmp_with_1__exit = tmp_assign_source_20;

    tmp_assign_source_21 = Py_True;
    assert( tmp_with_1__indicator == NULL );
    Py_INCREF( tmp_assign_source_21 );
    tmp_with_1__indicator = tmp_assign_source_21;

    // Tried code:
    // Tried code:
    tmp_source_name_3 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_source_name_3 == NULL ))
    {
        tmp_source_name_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_source_name_3 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 47;

        goto try_except_handler_4;
    }

    tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_register_option );
    if ( tmp_called_name_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 47;

        goto try_except_handler_4;
    }
    tmp_tuple_element_1 = const_str_plain_use_bottleneck;
    tmp_args_name_1 = PyTuple_New( 3 );
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
    tmp_tuple_element_1 = Py_True;
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_1 );
    tmp_tuple_element_1 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_use_bottleneck_doc );

    if (unlikely( tmp_tuple_element_1 == NULL ))
    {
        tmp_tuple_element_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_use_bottleneck_doc );
    }

    if ( tmp_tuple_element_1 == NULL )
    {
        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_name_1 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "use_bottleneck_doc" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 47;

        goto try_except_handler_4;
    }

    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_args_name_1, 2, tmp_tuple_element_1 );
    tmp_dict_key_1 = const_str_plain_validator;
    tmp_dict_value_1 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_is_bool );

    if (unlikely( tmp_dict_value_1 == NULL ))
    {
        tmp_dict_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_bool );
    }

    if ( tmp_dict_value_1 == NULL )
    {
        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_name_1 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_bool" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 48;

        goto try_except_handler_4;
    }

    tmp_kw_name_1 = _PyDict_NewPresized( 2 );
    tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_2 = const_str_plain_cb;
    tmp_dict_value_2 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_use_bottleneck_cb );

    if (unlikely( tmp_dict_value_2 == NULL ))
    {
        tmp_dict_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_use_bottleneck_cb );
    }

    if ( tmp_dict_value_2 == NULL )
    {
        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "use_bottleneck_cb" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 48;

        goto try_except_handler_4;
    }

    tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
    assert( !(tmp_res != 0) );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 47;
    tmp_unused = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_1, tmp_kw_name_1 );
    Py_DECREF( tmp_called_name_2 );
    Py_DECREF( tmp_args_name_1 );
    Py_DECREF( tmp_kw_name_1 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 47;

        goto try_except_handler_4;
    }
    Py_DECREF( tmp_unused );
    tmp_source_name_4 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_source_name_4 == NULL ))
    {
        tmp_source_name_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_source_name_4 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 49;

        goto try_except_handler_4;
    }

    tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_register_option );
    if ( tmp_called_name_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 49;

        goto try_except_handler_4;
    }
    tmp_tuple_element_2 = const_str_plain_use_numexpr;
    tmp_args_name_2 = PyTuple_New( 3 );
    Py_INCREF( tmp_tuple_element_2 );
    PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_2 );
    tmp_tuple_element_2 = Py_True;
    Py_INCREF( tmp_tuple_element_2 );
    PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_2 );
    tmp_tuple_element_2 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_use_numexpr_doc );

    if (unlikely( tmp_tuple_element_2 == NULL ))
    {
        tmp_tuple_element_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_use_numexpr_doc );
    }

    if ( tmp_tuple_element_2 == NULL )
    {
        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_name_2 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "use_numexpr_doc" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 49;

        goto try_except_handler_4;
    }

    Py_INCREF( tmp_tuple_element_2 );
    PyTuple_SET_ITEM( tmp_args_name_2, 2, tmp_tuple_element_2 );
    tmp_dict_key_3 = const_str_plain_validator;
    tmp_dict_value_3 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_is_bool );

    if (unlikely( tmp_dict_value_3 == NULL ))
    {
        tmp_dict_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_bool );
    }

    if ( tmp_dict_value_3 == NULL )
    {
        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_name_2 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_bool" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 50;

        goto try_except_handler_4;
    }

    tmp_kw_name_2 = _PyDict_NewPresized( 2 );
    tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_3, tmp_dict_value_3 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_4 = const_str_plain_cb;
    tmp_dict_value_4 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_use_numexpr_cb );

    if (unlikely( tmp_dict_value_4 == NULL ))
    {
        tmp_dict_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_use_numexpr_cb );
    }

    if ( tmp_dict_value_4 == NULL )
    {
        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_name_2 );
        Py_DECREF( tmp_kw_name_2 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "use_numexpr_cb" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 50;

        goto try_except_handler_4;
    }

    tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_4, tmp_dict_value_4 );
    assert( !(tmp_res != 0) );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 49;
    tmp_unused = CALL_FUNCTION( tmp_called_name_3, tmp_args_name_2, tmp_kw_name_2 );
    Py_DECREF( tmp_called_name_3 );
    Py_DECREF( tmp_args_name_2 );
    Py_DECREF( tmp_kw_name_2 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 49;

        goto try_except_handler_4;
    }
    Py_DECREF( tmp_unused );
    goto try_end_2;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_2 == NULL )
    {
        exception_keeper_tb_2 = MAKE_TRACEBACK( frame_3209efbe1fc5d2857a97a7e36ce015f8, exception_keeper_lineno_2 );
    }
    else if ( exception_keeper_lineno_2 != 0 )
    {
        exception_keeper_tb_2 = ADD_TRACEBACK( exception_keeper_tb_2, frame_3209efbe1fc5d2857a97a7e36ce015f8, exception_keeper_lineno_2 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_2, &exception_keeper_value_2, &exception_keeper_tb_2 );
    PyException_SetTraceback( exception_keeper_value_2, (PyObject *)exception_keeper_tb_2 );
    PUBLISH_EXCEPTION( &exception_keeper_type_2, &exception_keeper_value_2, &exception_keeper_tb_2 );
    // Tried code:
    tmp_compare_left_1 = EXC_TYPE(PyThreadState_GET());
    tmp_compare_right_1 = PyExc_BaseException;
    tmp_exc_match_exception_match_1 = EXCEPTION_MATCH_BOOL( tmp_compare_left_1, tmp_compare_right_1 );
    if ( tmp_exc_match_exception_match_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 46;

        goto try_except_handler_5;
    }
    if ( tmp_exc_match_exception_match_1 == 1 )
    {
        goto branch_yes_1;
    }
    else
    {
        goto branch_no_1;
    }
    branch_yes_1:;
    tmp_assign_source_22 = Py_False;
    {
        PyObject *old = tmp_with_1__indicator;
        assert( old != NULL );
        tmp_with_1__indicator = tmp_assign_source_22;
        Py_INCREF( tmp_with_1__indicator );
        Py_DECREF( old );
    }

    tmp_called_name_4 = tmp_with_1__exit;

    CHECK_OBJECT( tmp_called_name_4 );
    tmp_args_element_name_1 = EXC_TYPE(PyThreadState_GET());
    tmp_args_element_name_2 = EXC_VALUE(PyThreadState_GET());
    tmp_args_element_name_3 = EXC_TRACEBACK(PyThreadState_GET());
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 46;
    {
        PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3 };
        tmp_cond_value_1 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_4, call_args );
    }

    if ( tmp_cond_value_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 46;

        goto try_except_handler_5;
    }
    tmp_cond_truth_1 = CHECK_IF_TRUE( tmp_cond_value_1 );
    if ( tmp_cond_truth_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_cond_value_1 );

        exception_lineno = 46;

        goto try_except_handler_5;
    }
    Py_DECREF( tmp_cond_value_1 );
    if ( tmp_cond_truth_1 == 1 )
    {
        goto branch_no_2;
    }
    else
    {
        goto branch_yes_2;
    }
    branch_yes_2:;
    tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (unlikely( tmp_result == false ))
    {
        exception_lineno = 46;
    }

    if (exception_tb && exception_tb->tb_frame == &frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame) frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = exception_tb->tb_lineno;

    goto try_except_handler_5;
    branch_no_2:;
    goto branch_end_1;
    branch_no_1:;
    tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (unlikely( tmp_result == false ))
    {
        exception_lineno = 46;
    }

    if (exception_tb && exception_tb->tb_frame == &frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame) frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = exception_tb->tb_lineno;

    goto try_except_handler_5;
    branch_end_1:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto try_except_handler_3;
    // End of try:
    try_end_3:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto try_end_2;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$core$config_init );
    return MOD_RETURN_VALUE( NULL );
    // End of try:
    try_end_2:;
    goto try_end_4;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    tmp_compare_left_2 = tmp_with_1__indicator;

    CHECK_OBJECT( tmp_compare_left_2 );
    tmp_compare_right_2 = Py_True;
    tmp_is_1 = ( tmp_compare_left_2 == tmp_compare_right_2 );
    if ( tmp_is_1 )
    {
        goto branch_yes_3;
    }
    else
    {
        goto branch_no_3;
    }
    branch_yes_3:;
    tmp_called_name_5 = tmp_with_1__exit;

    CHECK_OBJECT( tmp_called_name_5 );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 46;
    tmp_unused = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_5, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

        Py_DECREF( exception_keeper_type_4 );
        Py_XDECREF( exception_keeper_value_4 );
        Py_XDECREF( exception_keeper_tb_4 );

        exception_lineno = 46;

        goto try_except_handler_2;
    }
    Py_DECREF( tmp_unused );
    branch_no_3:;
    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto try_except_handler_2;
    // End of try:
    try_end_4:;
    tmp_compare_left_3 = tmp_with_1__indicator;

    CHECK_OBJECT( tmp_compare_left_3 );
    tmp_compare_right_3 = Py_True;
    tmp_is_2 = ( tmp_compare_left_3 == tmp_compare_right_3 );
    if ( tmp_is_2 )
    {
        goto branch_yes_4;
    }
    else
    {
        goto branch_no_4;
    }
    branch_yes_4:;
    tmp_called_name_6 = tmp_with_1__exit;

    CHECK_OBJECT( tmp_called_name_6 );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 46;
    tmp_unused = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_6, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 46;

        goto try_except_handler_2;
    }
    Py_DECREF( tmp_unused );
    branch_no_4:;
    goto try_end_5;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_with_1__source );
    tmp_with_1__source = NULL;

    Py_XDECREF( tmp_with_1__enter );
    tmp_with_1__enter = NULL;

    Py_XDECREF( tmp_with_1__exit );
    tmp_with_1__exit = NULL;

    Py_XDECREF( tmp_with_1__indicator );
    tmp_with_1__indicator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto frame_exception_exit_1;
    // End of try:
    try_end_5:;
    CHECK_OBJECT( (PyObject *)tmp_with_1__source );
    Py_DECREF( tmp_with_1__source );
    tmp_with_1__source = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_1__enter );
    Py_DECREF( tmp_with_1__enter );
    tmp_with_1__enter = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_1__exit );
    Py_DECREF( tmp_with_1__exit );
    tmp_with_1__exit = NULL;

    Py_XDECREF( tmp_with_1__indicator );
    tmp_with_1__indicator = NULL;

    tmp_assign_source_23 = const_str_digest_162c04eaa4f9de7abebfff3d81422a8b;
    UPDATE_STRING_DICT0( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_precision_doc, tmp_assign_source_23 );
    tmp_assign_source_24 = const_str_digest_7a42d199b93f92d5aefb172b3cfa54dd;
    UPDATE_STRING_DICT0( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_colspace_doc, tmp_assign_source_24 );
    tmp_assign_source_25 = const_str_digest_f3097cefcee812f2202499367dd8f891;
    UPDATE_STRING_DICT0( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_max_rows_doc, tmp_assign_source_25 );
    tmp_assign_source_26 = const_str_digest_a9c4f8563d1ecedf60bda713bd0b1e2c;
    UPDATE_STRING_DICT0( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_max_cols_doc, tmp_assign_source_26 );
    tmp_assign_source_27 = const_str_digest_92d5fb4be587bc095681b0b7376435ec;
    UPDATE_STRING_DICT0( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_max_categories_doc, tmp_assign_source_27 );
    tmp_assign_source_28 = const_str_digest_6de8d02e7df5afc37f0e3f0e946776a1;
    UPDATE_STRING_DICT0( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_max_info_cols_doc, tmp_assign_source_28 );
    tmp_assign_source_29 = const_str_digest_32d9ee95a950d61124e98e2a12fc0214;
    UPDATE_STRING_DICT0( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_nb_repr_h_doc, tmp_assign_source_29 );
    tmp_assign_source_30 = const_str_digest_f9be2e7dbb52eeee952286f86a48c0d1;
    UPDATE_STRING_DICT0( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_date_dayfirst_doc, tmp_assign_source_30 );
    tmp_assign_source_31 = const_str_digest_3b01a421e528fddeffa2f6f36e25f610;
    UPDATE_STRING_DICT0( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_date_yearfirst_doc, tmp_assign_source_31 );
    tmp_assign_source_32 = const_str_digest_02315a6b6613311a88485cf6ec10d6ed;
    UPDATE_STRING_DICT0( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_pprint_nest_depth, tmp_assign_source_32 );
    tmp_assign_source_33 = const_str_digest_99e766b2632da277c8e75ec5e46abcd7;
    UPDATE_STRING_DICT0( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_multi_sparse_doc, tmp_assign_source_33 );
    tmp_assign_source_34 = const_str_digest_3b2c0193880aff67a5404a80d2c4fce1;
    UPDATE_STRING_DICT0( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_encoding_doc, tmp_assign_source_34 );
    tmp_assign_source_35 = const_str_digest_dfed39f8c622349086edb057745e7a7c;
    UPDATE_STRING_DICT0( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_float_format_doc, tmp_assign_source_35 );
    tmp_assign_source_36 = const_str_digest_36e3b1f94c51cffa19984b47d24b4246;
    UPDATE_STRING_DICT0( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_max_colwidth_doc, tmp_assign_source_36 );
    tmp_assign_source_37 = const_str_digest_692d141c202ce579d6e235169c8eaa7b;
    UPDATE_STRING_DICT0( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_colheader_justify_doc, tmp_assign_source_37 );
    tmp_assign_source_38 = const_str_digest_b0e6b6e8f5f54372dfb38c17db742597;
    UPDATE_STRING_DICT0( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_expand_repr_doc, tmp_assign_source_38 );
    tmp_assign_source_39 = const_str_digest_2c5f1d42f6beaab0f171385068265a49;
    UPDATE_STRING_DICT0( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_show_dimensions_doc, tmp_assign_source_39 );
    tmp_assign_source_40 = const_str_digest_9670544456fe8ec48aad3cc3f43bc6df;
    UPDATE_STRING_DICT0( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_east_asian_width_doc, tmp_assign_source_40 );
    tmp_assign_source_41 = const_str_digest_25e404ad3ac11a956aa85b1d77b401de;
    UPDATE_STRING_DICT0( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_ambiguous_as_wide_doc, tmp_assign_source_41 );
    tmp_assign_source_42 = const_str_digest_de1533dce7c0c3057803e6914fcf5814;
    UPDATE_STRING_DICT0( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_latex_repr_doc, tmp_assign_source_42 );
    tmp_assign_source_43 = const_str_digest_e6fe352ef36e0bf7027ddc4a9efaf76c;
    UPDATE_STRING_DICT0( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_table_schema_doc, tmp_assign_source_43 );
    tmp_assign_source_44 = const_str_digest_79b75e74f53a10e8129732de4fc9ea36;
    UPDATE_STRING_DICT0( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_html_border_doc, tmp_assign_source_44 );
    tmp_assign_source_45 = const_str_digest_99cdadd45b47f7b22a2c379861d1710a;
    UPDATE_STRING_DICT0( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_html_border_deprecation_warning, tmp_assign_source_45 );
    tmp_assign_source_46 = const_str_digest_603db2006590fd8b8437f02ab548d669;
    UPDATE_STRING_DICT0( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_html_use_mathjax_doc, tmp_assign_source_46 );
    tmp_assign_source_47 = const_str_digest_9828a77d868bc8c07cee23db35f2d9a5;
    UPDATE_STRING_DICT0( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_width_doc, tmp_assign_source_47 );
    tmp_assign_source_48 = const_str_digest_67ab286b537608a8ed02613094baf20d;
    UPDATE_STRING_DICT0( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_chop_threshold_doc, tmp_assign_source_48 );
    tmp_assign_source_49 = const_str_digest_ff62b92b31818043edce46d1900d7de3;
    UPDATE_STRING_DICT0( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_max_seq_items, tmp_assign_source_49 );
    tmp_assign_source_50 = const_str_digest_b3457d17b246c509349a6a956ce44332;
    UPDATE_STRING_DICT0( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_max_info_rows_doc, tmp_assign_source_50 );
    tmp_assign_source_51 = const_str_digest_d779ffca91b7e173eb78d35afad339b2;
    UPDATE_STRING_DICT0( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_large_repr_doc, tmp_assign_source_51 );
    tmp_assign_source_52 = const_str_digest_5c883b3797590ee62e4ca0d8acbabdbf;
    UPDATE_STRING_DICT0( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_memory_usage_doc, tmp_assign_source_52 );
    tmp_assign_source_53 = const_str_digest_0f50b2f82ea6dafb9acb48b11438c59c;
    UPDATE_STRING_DICT0( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_latex_escape, tmp_assign_source_53 );
    tmp_assign_source_54 = const_str_digest_47930e395de0263309e238bd2d8cce2c;
    UPDATE_STRING_DICT0( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_latex_longtable, tmp_assign_source_54 );
    tmp_assign_source_55 = const_str_digest_7b040172b3c9edf5c62359d0376f0338;
    UPDATE_STRING_DICT0( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_latex_multicolumn, tmp_assign_source_55 );
    tmp_assign_source_56 = const_str_digest_d640058ba6564339d64044d11762fd11;
    UPDATE_STRING_DICT0( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_latex_multicolumn_format, tmp_assign_source_56 );
    tmp_assign_source_57 = const_str_digest_3cce2b8ce49df0213b09bcbcf6ed7cda;
    UPDATE_STRING_DICT0( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_latex_multirow, tmp_assign_source_57 );
    tmp_assign_source_58 = PyDict_New();
    UPDATE_STRING_DICT1( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_style_backup, tmp_assign_source_58 );
    tmp_assign_source_59 = MAKE_FUNCTION_pandas$core$config_init$$$function_3_table_schema_cb(  );
    UPDATE_STRING_DICT1( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_table_schema_cb, tmp_assign_source_59 );
    // Tried code:
    tmp_called_instance_2 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_called_instance_2 == NULL ))
    {
        tmp_called_instance_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_called_instance_2 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 306;

        goto try_except_handler_6;
    }

    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 306;
    tmp_assign_source_60 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_config_prefix, &PyTuple_GET_ITEM( const_tuple_str_plain_display_tuple, 0 ) );

    if ( tmp_assign_source_60 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 306;

        goto try_except_handler_6;
    }
    assert( tmp_with_2__source == NULL );
    tmp_with_2__source = tmp_assign_source_60;

    tmp_source_name_5 = tmp_with_2__source;

    CHECK_OBJECT( tmp_source_name_5 );
    tmp_called_name_7 = LOOKUP_SPECIAL( tmp_source_name_5, const_str_plain___enter__ );
    if ( tmp_called_name_7 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 306;

        goto try_except_handler_6;
    }
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 306;
    tmp_assign_source_61 = CALL_FUNCTION_NO_ARGS( tmp_called_name_7 );
    Py_DECREF( tmp_called_name_7 );
    if ( tmp_assign_source_61 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 306;

        goto try_except_handler_6;
    }
    assert( tmp_with_2__enter == NULL );
    tmp_with_2__enter = tmp_assign_source_61;

    tmp_source_name_6 = tmp_with_2__source;

    CHECK_OBJECT( tmp_source_name_6 );
    tmp_assign_source_62 = LOOKUP_SPECIAL( tmp_source_name_6, const_str_plain___exit__ );
    if ( tmp_assign_source_62 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 306;

        goto try_except_handler_6;
    }
    assert( tmp_with_2__exit == NULL );
    tmp_with_2__exit = tmp_assign_source_62;

    tmp_assign_source_63 = Py_True;
    assert( tmp_with_2__indicator == NULL );
    Py_INCREF( tmp_assign_source_63 );
    tmp_with_2__indicator = tmp_assign_source_63;

    // Tried code:
    // Tried code:
    tmp_source_name_7 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_source_name_7 == NULL ))
    {
        tmp_source_name_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_source_name_7 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 307;

        goto try_except_handler_8;
    }

    tmp_called_name_8 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_register_option );
    if ( tmp_called_name_8 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 307;

        goto try_except_handler_8;
    }
    tmp_tuple_element_3 = const_str_plain_precision;
    tmp_args_name_3 = PyTuple_New( 3 );
    Py_INCREF( tmp_tuple_element_3 );
    PyTuple_SET_ITEM( tmp_args_name_3, 0, tmp_tuple_element_3 );
    tmp_tuple_element_3 = const_int_pos_6;
    Py_INCREF( tmp_tuple_element_3 );
    PyTuple_SET_ITEM( tmp_args_name_3, 1, tmp_tuple_element_3 );
    tmp_tuple_element_3 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_precision_doc );

    if (unlikely( tmp_tuple_element_3 == NULL ))
    {
        tmp_tuple_element_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_pc_precision_doc );
    }

    if ( tmp_tuple_element_3 == NULL )
    {
        Py_DECREF( tmp_called_name_8 );
        Py_DECREF( tmp_args_name_3 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "pc_precision_doc" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 307;

        goto try_except_handler_8;
    }

    Py_INCREF( tmp_tuple_element_3 );
    PyTuple_SET_ITEM( tmp_args_name_3, 2, tmp_tuple_element_3 );
    tmp_dict_key_5 = const_str_plain_validator;
    tmp_dict_value_5 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_is_int );

    if (unlikely( tmp_dict_value_5 == NULL ))
    {
        tmp_dict_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_int );
    }

    if ( tmp_dict_value_5 == NULL )
    {
        Py_DECREF( tmp_called_name_8 );
        Py_DECREF( tmp_args_name_3 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_int" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 307;

        goto try_except_handler_8;
    }

    tmp_kw_name_3 = _PyDict_NewPresized( 1 );
    tmp_res = PyDict_SetItem( tmp_kw_name_3, tmp_dict_key_5, tmp_dict_value_5 );
    assert( !(tmp_res != 0) );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 307;
    tmp_unused = CALL_FUNCTION( tmp_called_name_8, tmp_args_name_3, tmp_kw_name_3 );
    Py_DECREF( tmp_called_name_8 );
    Py_DECREF( tmp_args_name_3 );
    Py_DECREF( tmp_kw_name_3 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 307;

        goto try_except_handler_8;
    }
    Py_DECREF( tmp_unused );
    tmp_source_name_8 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_source_name_8 == NULL ))
    {
        tmp_source_name_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_source_name_8 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 308;

        goto try_except_handler_8;
    }

    tmp_called_name_9 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_register_option );
    if ( tmp_called_name_9 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 308;

        goto try_except_handler_8;
    }
    tmp_tuple_element_4 = const_str_plain_float_format;
    tmp_args_name_4 = PyTuple_New( 3 );
    Py_INCREF( tmp_tuple_element_4 );
    PyTuple_SET_ITEM( tmp_args_name_4, 0, tmp_tuple_element_4 );
    tmp_tuple_element_4 = Py_None;
    Py_INCREF( tmp_tuple_element_4 );
    PyTuple_SET_ITEM( tmp_args_name_4, 1, tmp_tuple_element_4 );
    tmp_tuple_element_4 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_float_format_doc );

    if (unlikely( tmp_tuple_element_4 == NULL ))
    {
        tmp_tuple_element_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_float_format_doc );
    }

    if ( tmp_tuple_element_4 == NULL )
    {
        Py_DECREF( tmp_called_name_9 );
        Py_DECREF( tmp_args_name_4 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "float_format_doc" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 308;

        goto try_except_handler_8;
    }

    Py_INCREF( tmp_tuple_element_4 );
    PyTuple_SET_ITEM( tmp_args_name_4, 2, tmp_tuple_element_4 );
    tmp_dict_key_6 = const_str_plain_validator;
    tmp_called_name_10 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_is_one_of_factory );

    if (unlikely( tmp_called_name_10 == NULL ))
    {
        tmp_called_name_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_one_of_factory );
    }

    if ( tmp_called_name_10 == NULL )
    {
        Py_DECREF( tmp_called_name_9 );
        Py_DECREF( tmp_args_name_4 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_one_of_factory" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 309;

        goto try_except_handler_8;
    }

    tmp_list_element_1 = Py_None;
    tmp_args_element_name_4 = PyList_New( 2 );
    Py_INCREF( tmp_list_element_1 );
    PyList_SET_ITEM( tmp_args_element_name_4, 0, tmp_list_element_1 );
    tmp_list_element_1 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_is_callable );

    if (unlikely( tmp_list_element_1 == NULL ))
    {
        tmp_list_element_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_callable );
    }

    if ( tmp_list_element_1 == NULL )
    {
        Py_DECREF( tmp_called_name_9 );
        Py_DECREF( tmp_args_name_4 );
        Py_DECREF( tmp_args_element_name_4 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_callable" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 309;

        goto try_except_handler_8;
    }

    Py_INCREF( tmp_list_element_1 );
    PyList_SET_ITEM( tmp_args_element_name_4, 1, tmp_list_element_1 );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 309;
    {
        PyObject *call_args[] = { tmp_args_element_name_4 };
        tmp_dict_value_6 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_10, call_args );
    }

    Py_DECREF( tmp_args_element_name_4 );
    if ( tmp_dict_value_6 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_9 );
        Py_DECREF( tmp_args_name_4 );

        exception_lineno = 309;

        goto try_except_handler_8;
    }
    tmp_kw_name_4 = _PyDict_NewPresized( 1 );
    tmp_res = PyDict_SetItem( tmp_kw_name_4, tmp_dict_key_6, tmp_dict_value_6 );
    Py_DECREF( tmp_dict_value_6 );
    assert( !(tmp_res != 0) );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 308;
    tmp_unused = CALL_FUNCTION( tmp_called_name_9, tmp_args_name_4, tmp_kw_name_4 );
    Py_DECREF( tmp_called_name_9 );
    Py_DECREF( tmp_args_name_4 );
    Py_DECREF( tmp_kw_name_4 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 308;

        goto try_except_handler_8;
    }
    Py_DECREF( tmp_unused );
    tmp_source_name_9 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_source_name_9 == NULL ))
    {
        tmp_source_name_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_source_name_9 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 310;

        goto try_except_handler_8;
    }

    tmp_called_name_11 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_register_option );
    if ( tmp_called_name_11 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 310;

        goto try_except_handler_8;
    }
    tmp_args_name_5 = const_tuple_str_plain_column_space_int_pos_12_tuple;
    tmp_dict_key_7 = const_str_plain_validator;
    tmp_dict_value_7 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_is_int );

    if (unlikely( tmp_dict_value_7 == NULL ))
    {
        tmp_dict_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_int );
    }

    if ( tmp_dict_value_7 == NULL )
    {
        Py_DECREF( tmp_called_name_11 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_int" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 310;

        goto try_except_handler_8;
    }

    tmp_kw_name_5 = _PyDict_NewPresized( 1 );
    tmp_res = PyDict_SetItem( tmp_kw_name_5, tmp_dict_key_7, tmp_dict_value_7 );
    assert( !(tmp_res != 0) );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 310;
    tmp_unused = CALL_FUNCTION( tmp_called_name_11, tmp_args_name_5, tmp_kw_name_5 );
    Py_DECREF( tmp_called_name_11 );
    Py_DECREF( tmp_kw_name_5 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 310;

        goto try_except_handler_8;
    }
    Py_DECREF( tmp_unused );
    tmp_source_name_10 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_source_name_10 == NULL ))
    {
        tmp_source_name_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_source_name_10 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 311;

        goto try_except_handler_8;
    }

    tmp_called_name_12 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_register_option );
    if ( tmp_called_name_12 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 311;

        goto try_except_handler_8;
    }
    tmp_tuple_element_5 = const_str_plain_max_info_rows;
    tmp_args_name_6 = PyTuple_New( 3 );
    Py_INCREF( tmp_tuple_element_5 );
    PyTuple_SET_ITEM( tmp_args_name_6, 0, tmp_tuple_element_5 );
    tmp_tuple_element_5 = const_int_pos_1690785;
    Py_INCREF( tmp_tuple_element_5 );
    PyTuple_SET_ITEM( tmp_args_name_6, 1, tmp_tuple_element_5 );
    tmp_tuple_element_5 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_max_info_rows_doc );

    if (unlikely( tmp_tuple_element_5 == NULL ))
    {
        tmp_tuple_element_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_pc_max_info_rows_doc );
    }

    if ( tmp_tuple_element_5 == NULL )
    {
        Py_DECREF( tmp_called_name_12 );
        Py_DECREF( tmp_args_name_6 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "pc_max_info_rows_doc" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 311;

        goto try_except_handler_8;
    }

    Py_INCREF( tmp_tuple_element_5 );
    PyTuple_SET_ITEM( tmp_args_name_6, 2, tmp_tuple_element_5 );
    tmp_dict_key_8 = const_str_plain_validator;
    tmp_called_name_13 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_is_instance_factory );

    if (unlikely( tmp_called_name_13 == NULL ))
    {
        tmp_called_name_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_instance_factory );
    }

    if ( tmp_called_name_13 == NULL )
    {
        Py_DECREF( tmp_called_name_12 );
        Py_DECREF( tmp_args_name_6 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_instance_factory" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 312;

        goto try_except_handler_8;
    }

    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 312;
    tmp_dict_value_8 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_13, &PyTuple_GET_ITEM( const_tuple_tuple_type_int_anon_NoneType_tuple_tuple, 0 ) );

    if ( tmp_dict_value_8 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_12 );
        Py_DECREF( tmp_args_name_6 );

        exception_lineno = 312;

        goto try_except_handler_8;
    }
    tmp_kw_name_6 = _PyDict_NewPresized( 1 );
    tmp_res = PyDict_SetItem( tmp_kw_name_6, tmp_dict_key_8, tmp_dict_value_8 );
    Py_DECREF( tmp_dict_value_8 );
    assert( !(tmp_res != 0) );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 311;
    tmp_unused = CALL_FUNCTION( tmp_called_name_12, tmp_args_name_6, tmp_kw_name_6 );
    Py_DECREF( tmp_called_name_12 );
    Py_DECREF( tmp_args_name_6 );
    Py_DECREF( tmp_kw_name_6 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 311;

        goto try_except_handler_8;
    }
    Py_DECREF( tmp_unused );
    tmp_source_name_11 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_source_name_11 == NULL ))
    {
        tmp_source_name_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_source_name_11 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 313;

        goto try_except_handler_8;
    }

    tmp_called_name_14 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_register_option );
    if ( tmp_called_name_14 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 313;

        goto try_except_handler_8;
    }
    tmp_tuple_element_6 = const_str_plain_max_rows;
    tmp_args_name_7 = PyTuple_New( 3 );
    Py_INCREF( tmp_tuple_element_6 );
    PyTuple_SET_ITEM( tmp_args_name_7, 0, tmp_tuple_element_6 );
    tmp_tuple_element_6 = const_int_pos_60;
    Py_INCREF( tmp_tuple_element_6 );
    PyTuple_SET_ITEM( tmp_args_name_7, 1, tmp_tuple_element_6 );
    tmp_tuple_element_6 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_max_rows_doc );

    if (unlikely( tmp_tuple_element_6 == NULL ))
    {
        tmp_tuple_element_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_pc_max_rows_doc );
    }

    if ( tmp_tuple_element_6 == NULL )
    {
        Py_DECREF( tmp_called_name_14 );
        Py_DECREF( tmp_args_name_7 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "pc_max_rows_doc" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 313;

        goto try_except_handler_8;
    }

    Py_INCREF( tmp_tuple_element_6 );
    PyTuple_SET_ITEM( tmp_args_name_7, 2, tmp_tuple_element_6 );
    tmp_dict_key_9 = const_str_plain_validator;
    tmp_called_name_15 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_is_instance_factory );

    if (unlikely( tmp_called_name_15 == NULL ))
    {
        tmp_called_name_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_instance_factory );
    }

    if ( tmp_called_name_15 == NULL )
    {
        Py_DECREF( tmp_called_name_14 );
        Py_DECREF( tmp_args_name_7 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_instance_factory" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 314;

        goto try_except_handler_8;
    }

    tmp_call_arg_element_1 = LIST_COPY( const_list_anon_NoneType_type_int_list );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 314;
    {
        PyObject *call_args[] = { tmp_call_arg_element_1 };
        tmp_dict_value_9 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_15, call_args );
    }

    Py_DECREF( tmp_call_arg_element_1 );
    if ( tmp_dict_value_9 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_14 );
        Py_DECREF( tmp_args_name_7 );

        exception_lineno = 314;

        goto try_except_handler_8;
    }
    tmp_kw_name_7 = _PyDict_NewPresized( 1 );
    tmp_res = PyDict_SetItem( tmp_kw_name_7, tmp_dict_key_9, tmp_dict_value_9 );
    Py_DECREF( tmp_dict_value_9 );
    assert( !(tmp_res != 0) );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 313;
    tmp_unused = CALL_FUNCTION( tmp_called_name_14, tmp_args_name_7, tmp_kw_name_7 );
    Py_DECREF( tmp_called_name_14 );
    Py_DECREF( tmp_args_name_7 );
    Py_DECREF( tmp_kw_name_7 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 313;

        goto try_except_handler_8;
    }
    Py_DECREF( tmp_unused );
    tmp_source_name_12 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_source_name_12 == NULL ))
    {
        tmp_source_name_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_source_name_12 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 315;

        goto try_except_handler_8;
    }

    tmp_called_name_16 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_register_option );
    if ( tmp_called_name_16 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 315;

        goto try_except_handler_8;
    }
    tmp_tuple_element_7 = const_str_plain_max_categories;
    tmp_args_name_8 = PyTuple_New( 3 );
    Py_INCREF( tmp_tuple_element_7 );
    PyTuple_SET_ITEM( tmp_args_name_8, 0, tmp_tuple_element_7 );
    tmp_tuple_element_7 = const_int_pos_8;
    Py_INCREF( tmp_tuple_element_7 );
    PyTuple_SET_ITEM( tmp_args_name_8, 1, tmp_tuple_element_7 );
    tmp_tuple_element_7 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_max_categories_doc );

    if (unlikely( tmp_tuple_element_7 == NULL ))
    {
        tmp_tuple_element_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_pc_max_categories_doc );
    }

    if ( tmp_tuple_element_7 == NULL )
    {
        Py_DECREF( tmp_called_name_16 );
        Py_DECREF( tmp_args_name_8 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "pc_max_categories_doc" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 315;

        goto try_except_handler_8;
    }

    Py_INCREF( tmp_tuple_element_7 );
    PyTuple_SET_ITEM( tmp_args_name_8, 2, tmp_tuple_element_7 );
    tmp_dict_key_10 = const_str_plain_validator;
    tmp_dict_value_10 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_is_int );

    if (unlikely( tmp_dict_value_10 == NULL ))
    {
        tmp_dict_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_int );
    }

    if ( tmp_dict_value_10 == NULL )
    {
        Py_DECREF( tmp_called_name_16 );
        Py_DECREF( tmp_args_name_8 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_int" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 316;

        goto try_except_handler_8;
    }

    tmp_kw_name_8 = _PyDict_NewPresized( 1 );
    tmp_res = PyDict_SetItem( tmp_kw_name_8, tmp_dict_key_10, tmp_dict_value_10 );
    assert( !(tmp_res != 0) );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 315;
    tmp_unused = CALL_FUNCTION( tmp_called_name_16, tmp_args_name_8, tmp_kw_name_8 );
    Py_DECREF( tmp_called_name_16 );
    Py_DECREF( tmp_args_name_8 );
    Py_DECREF( tmp_kw_name_8 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 315;

        goto try_except_handler_8;
    }
    Py_DECREF( tmp_unused );
    tmp_source_name_13 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_source_name_13 == NULL ))
    {
        tmp_source_name_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_source_name_13 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 317;

        goto try_except_handler_8;
    }

    tmp_called_name_17 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain_register_option );
    if ( tmp_called_name_17 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 317;

        goto try_except_handler_8;
    }
    tmp_tuple_element_8 = const_str_plain_max_colwidth;
    tmp_args_name_9 = PyTuple_New( 3 );
    Py_INCREF( tmp_tuple_element_8 );
    PyTuple_SET_ITEM( tmp_args_name_9, 0, tmp_tuple_element_8 );
    tmp_tuple_element_8 = const_int_pos_50;
    Py_INCREF( tmp_tuple_element_8 );
    PyTuple_SET_ITEM( tmp_args_name_9, 1, tmp_tuple_element_8 );
    tmp_tuple_element_8 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_max_colwidth_doc );

    if (unlikely( tmp_tuple_element_8 == NULL ))
    {
        tmp_tuple_element_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_max_colwidth_doc );
    }

    if ( tmp_tuple_element_8 == NULL )
    {
        Py_DECREF( tmp_called_name_17 );
        Py_DECREF( tmp_args_name_9 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "max_colwidth_doc" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 317;

        goto try_except_handler_8;
    }

    Py_INCREF( tmp_tuple_element_8 );
    PyTuple_SET_ITEM( tmp_args_name_9, 2, tmp_tuple_element_8 );
    tmp_dict_key_11 = const_str_plain_validator;
    tmp_dict_value_11 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_is_int );

    if (unlikely( tmp_dict_value_11 == NULL ))
    {
        tmp_dict_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_int );
    }

    if ( tmp_dict_value_11 == NULL )
    {
        Py_DECREF( tmp_called_name_17 );
        Py_DECREF( tmp_args_name_9 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_int" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 317;

        goto try_except_handler_8;
    }

    tmp_kw_name_9 = _PyDict_NewPresized( 1 );
    tmp_res = PyDict_SetItem( tmp_kw_name_9, tmp_dict_key_11, tmp_dict_value_11 );
    assert( !(tmp_res != 0) );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 317;
    tmp_unused = CALL_FUNCTION( tmp_called_name_17, tmp_args_name_9, tmp_kw_name_9 );
    Py_DECREF( tmp_called_name_17 );
    Py_DECREF( tmp_args_name_9 );
    Py_DECREF( tmp_kw_name_9 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 317;

        goto try_except_handler_8;
    }
    Py_DECREF( tmp_unused );
    tmp_called_name_18 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_is_terminal );

    if (unlikely( tmp_called_name_18 == NULL ))
    {
        tmp_called_name_18 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_terminal );
    }

    if ( tmp_called_name_18 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_terminal" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 318;

        goto try_except_handler_8;
    }

    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 318;
    tmp_cond_value_2 = CALL_FUNCTION_NO_ARGS( tmp_called_name_18 );
    if ( tmp_cond_value_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 318;

        goto try_except_handler_8;
    }
    tmp_cond_truth_2 = CHECK_IF_TRUE( tmp_cond_value_2 );
    if ( tmp_cond_truth_2 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_cond_value_2 );

        exception_lineno = 318;

        goto try_except_handler_8;
    }
    Py_DECREF( tmp_cond_value_2 );
    if ( tmp_cond_truth_2 == 1 )
    {
        goto branch_yes_5;
    }
    else
    {
        goto branch_no_5;
    }
    branch_yes_5:;
    tmp_assign_source_64 = const_int_0;
    UPDATE_STRING_DICT0( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_max_cols, tmp_assign_source_64 );
    goto branch_end_5;
    branch_no_5:;
    tmp_assign_source_65 = const_int_pos_20;
    UPDATE_STRING_DICT0( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_max_cols, tmp_assign_source_65 );
    branch_end_5:;
    tmp_source_name_14 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_source_name_14 == NULL ))
    {
        tmp_source_name_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_source_name_14 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 322;

        goto try_except_handler_8;
    }

    tmp_called_name_19 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain_register_option );
    if ( tmp_called_name_19 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 322;

        goto try_except_handler_8;
    }
    tmp_tuple_element_9 = const_str_plain_max_columns;
    tmp_args_name_10 = PyTuple_New( 3 );
    Py_INCREF( tmp_tuple_element_9 );
    PyTuple_SET_ITEM( tmp_args_name_10, 0, tmp_tuple_element_9 );
    tmp_tuple_element_9 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_max_cols );

    if (unlikely( tmp_tuple_element_9 == NULL ))
    {
        tmp_tuple_element_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_max_cols );
    }

    if ( tmp_tuple_element_9 == NULL )
    {
        Py_DECREF( tmp_called_name_19 );
        Py_DECREF( tmp_args_name_10 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "max_cols" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 322;

        goto try_except_handler_8;
    }

    Py_INCREF( tmp_tuple_element_9 );
    PyTuple_SET_ITEM( tmp_args_name_10, 1, tmp_tuple_element_9 );
    tmp_tuple_element_9 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_max_cols_doc );

    if (unlikely( tmp_tuple_element_9 == NULL ))
    {
        tmp_tuple_element_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_pc_max_cols_doc );
    }

    if ( tmp_tuple_element_9 == NULL )
    {
        Py_DECREF( tmp_called_name_19 );
        Py_DECREF( tmp_args_name_10 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "pc_max_cols_doc" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 322;

        goto try_except_handler_8;
    }

    Py_INCREF( tmp_tuple_element_9 );
    PyTuple_SET_ITEM( tmp_args_name_10, 2, tmp_tuple_element_9 );
    tmp_dict_key_12 = const_str_plain_validator;
    tmp_called_name_20 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_is_instance_factory );

    if (unlikely( tmp_called_name_20 == NULL ))
    {
        tmp_called_name_20 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_instance_factory );
    }

    if ( tmp_called_name_20 == NULL )
    {
        Py_DECREF( tmp_called_name_19 );
        Py_DECREF( tmp_args_name_10 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_instance_factory" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 323;

        goto try_except_handler_8;
    }

    tmp_call_arg_element_2 = LIST_COPY( const_list_anon_NoneType_type_int_list );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 323;
    {
        PyObject *call_args[] = { tmp_call_arg_element_2 };
        tmp_dict_value_12 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_20, call_args );
    }

    Py_DECREF( tmp_call_arg_element_2 );
    if ( tmp_dict_value_12 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_19 );
        Py_DECREF( tmp_args_name_10 );

        exception_lineno = 323;

        goto try_except_handler_8;
    }
    tmp_kw_name_10 = _PyDict_NewPresized( 1 );
    tmp_res = PyDict_SetItem( tmp_kw_name_10, tmp_dict_key_12, tmp_dict_value_12 );
    Py_DECREF( tmp_dict_value_12 );
    assert( !(tmp_res != 0) );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 322;
    tmp_unused = CALL_FUNCTION( tmp_called_name_19, tmp_args_name_10, tmp_kw_name_10 );
    Py_DECREF( tmp_called_name_19 );
    Py_DECREF( tmp_args_name_10 );
    Py_DECREF( tmp_kw_name_10 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 322;

        goto try_except_handler_8;
    }
    Py_DECREF( tmp_unused );
    tmp_source_name_15 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_source_name_15 == NULL ))
    {
        tmp_source_name_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_source_name_15 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 324;

        goto try_except_handler_8;
    }

    tmp_called_name_21 = LOOKUP_ATTRIBUTE( tmp_source_name_15, const_str_plain_register_option );
    if ( tmp_called_name_21 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 324;

        goto try_except_handler_8;
    }
    tmp_tuple_element_10 = const_str_plain_large_repr;
    tmp_args_name_11 = PyTuple_New( 3 );
    Py_INCREF( tmp_tuple_element_10 );
    PyTuple_SET_ITEM( tmp_args_name_11, 0, tmp_tuple_element_10 );
    tmp_tuple_element_10 = const_str_plain_truncate;
    Py_INCREF( tmp_tuple_element_10 );
    PyTuple_SET_ITEM( tmp_args_name_11, 1, tmp_tuple_element_10 );
    tmp_tuple_element_10 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_large_repr_doc );

    if (unlikely( tmp_tuple_element_10 == NULL ))
    {
        tmp_tuple_element_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_pc_large_repr_doc );
    }

    if ( tmp_tuple_element_10 == NULL )
    {
        Py_DECREF( tmp_called_name_21 );
        Py_DECREF( tmp_args_name_11 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "pc_large_repr_doc" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 324;

        goto try_except_handler_8;
    }

    Py_INCREF( tmp_tuple_element_10 );
    PyTuple_SET_ITEM( tmp_args_name_11, 2, tmp_tuple_element_10 );
    tmp_dict_key_13 = const_str_plain_validator;
    tmp_called_name_22 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_is_one_of_factory );

    if (unlikely( tmp_called_name_22 == NULL ))
    {
        tmp_called_name_22 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_one_of_factory );
    }

    if ( tmp_called_name_22 == NULL )
    {
        Py_DECREF( tmp_called_name_21 );
        Py_DECREF( tmp_args_name_11 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_one_of_factory" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 325;

        goto try_except_handler_8;
    }

    tmp_call_arg_element_3 = LIST_COPY( const_list_str_plain_truncate_str_plain_info_list );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 325;
    {
        PyObject *call_args[] = { tmp_call_arg_element_3 };
        tmp_dict_value_13 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_22, call_args );
    }

    Py_DECREF( tmp_call_arg_element_3 );
    if ( tmp_dict_value_13 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_21 );
        Py_DECREF( tmp_args_name_11 );

        exception_lineno = 325;

        goto try_except_handler_8;
    }
    tmp_kw_name_11 = _PyDict_NewPresized( 1 );
    tmp_res = PyDict_SetItem( tmp_kw_name_11, tmp_dict_key_13, tmp_dict_value_13 );
    Py_DECREF( tmp_dict_value_13 );
    assert( !(tmp_res != 0) );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 324;
    tmp_unused = CALL_FUNCTION( tmp_called_name_21, tmp_args_name_11, tmp_kw_name_11 );
    Py_DECREF( tmp_called_name_21 );
    Py_DECREF( tmp_args_name_11 );
    Py_DECREF( tmp_kw_name_11 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 324;

        goto try_except_handler_8;
    }
    Py_DECREF( tmp_unused );
    tmp_source_name_16 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_source_name_16 == NULL ))
    {
        tmp_source_name_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_source_name_16 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 326;

        goto try_except_handler_8;
    }

    tmp_called_name_23 = LOOKUP_ATTRIBUTE( tmp_source_name_16, const_str_plain_register_option );
    if ( tmp_called_name_23 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 326;

        goto try_except_handler_8;
    }
    tmp_tuple_element_11 = const_str_plain_max_info_columns;
    tmp_args_name_12 = PyTuple_New( 3 );
    Py_INCREF( tmp_tuple_element_11 );
    PyTuple_SET_ITEM( tmp_args_name_12, 0, tmp_tuple_element_11 );
    tmp_tuple_element_11 = const_int_pos_100;
    Py_INCREF( tmp_tuple_element_11 );
    PyTuple_SET_ITEM( tmp_args_name_12, 1, tmp_tuple_element_11 );
    tmp_tuple_element_11 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_max_info_cols_doc );

    if (unlikely( tmp_tuple_element_11 == NULL ))
    {
        tmp_tuple_element_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_pc_max_info_cols_doc );
    }

    if ( tmp_tuple_element_11 == NULL )
    {
        Py_DECREF( tmp_called_name_23 );
        Py_DECREF( tmp_args_name_12 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "pc_max_info_cols_doc" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 326;

        goto try_except_handler_8;
    }

    Py_INCREF( tmp_tuple_element_11 );
    PyTuple_SET_ITEM( tmp_args_name_12, 2, tmp_tuple_element_11 );
    tmp_dict_key_14 = const_str_plain_validator;
    tmp_dict_value_14 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_is_int );

    if (unlikely( tmp_dict_value_14 == NULL ))
    {
        tmp_dict_value_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_int );
    }

    if ( tmp_dict_value_14 == NULL )
    {
        Py_DECREF( tmp_called_name_23 );
        Py_DECREF( tmp_args_name_12 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_int" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 327;

        goto try_except_handler_8;
    }

    tmp_kw_name_12 = _PyDict_NewPresized( 1 );
    tmp_res = PyDict_SetItem( tmp_kw_name_12, tmp_dict_key_14, tmp_dict_value_14 );
    assert( !(tmp_res != 0) );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 326;
    tmp_unused = CALL_FUNCTION( tmp_called_name_23, tmp_args_name_12, tmp_kw_name_12 );
    Py_DECREF( tmp_called_name_23 );
    Py_DECREF( tmp_args_name_12 );
    Py_DECREF( tmp_kw_name_12 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 326;

        goto try_except_handler_8;
    }
    Py_DECREF( tmp_unused );
    tmp_source_name_17 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_source_name_17 == NULL ))
    {
        tmp_source_name_17 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_source_name_17 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 328;

        goto try_except_handler_8;
    }

    tmp_called_name_24 = LOOKUP_ATTRIBUTE( tmp_source_name_17, const_str_plain_register_option );
    if ( tmp_called_name_24 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 328;

        goto try_except_handler_8;
    }
    tmp_tuple_element_12 = const_str_plain_colheader_justify;
    tmp_args_name_13 = PyTuple_New( 3 );
    Py_INCREF( tmp_tuple_element_12 );
    PyTuple_SET_ITEM( tmp_args_name_13, 0, tmp_tuple_element_12 );
    tmp_tuple_element_12 = const_str_plain_right;
    Py_INCREF( tmp_tuple_element_12 );
    PyTuple_SET_ITEM( tmp_args_name_13, 1, tmp_tuple_element_12 );
    tmp_tuple_element_12 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_colheader_justify_doc );

    if (unlikely( tmp_tuple_element_12 == NULL ))
    {
        tmp_tuple_element_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_colheader_justify_doc );
    }

    if ( tmp_tuple_element_12 == NULL )
    {
        Py_DECREF( tmp_called_name_24 );
        Py_DECREF( tmp_args_name_13 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "colheader_justify_doc" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 328;

        goto try_except_handler_8;
    }

    Py_INCREF( tmp_tuple_element_12 );
    PyTuple_SET_ITEM( tmp_args_name_13, 2, tmp_tuple_element_12 );
    tmp_dict_key_15 = const_str_plain_validator;
    tmp_dict_value_15 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_is_text );

    if (unlikely( tmp_dict_value_15 == NULL ))
    {
        tmp_dict_value_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_text );
    }

    if ( tmp_dict_value_15 == NULL )
    {
        Py_DECREF( tmp_called_name_24 );
        Py_DECREF( tmp_args_name_13 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_text" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 329;

        goto try_except_handler_8;
    }

    tmp_kw_name_13 = _PyDict_NewPresized( 1 );
    tmp_res = PyDict_SetItem( tmp_kw_name_13, tmp_dict_key_15, tmp_dict_value_15 );
    assert( !(tmp_res != 0) );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 328;
    tmp_unused = CALL_FUNCTION( tmp_called_name_24, tmp_args_name_13, tmp_kw_name_13 );
    Py_DECREF( tmp_called_name_24 );
    Py_DECREF( tmp_args_name_13 );
    Py_DECREF( tmp_kw_name_13 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 328;

        goto try_except_handler_8;
    }
    Py_DECREF( tmp_unused );
    tmp_source_name_18 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_source_name_18 == NULL ))
    {
        tmp_source_name_18 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_source_name_18 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 330;

        goto try_except_handler_8;
    }

    tmp_called_name_25 = LOOKUP_ATTRIBUTE( tmp_source_name_18, const_str_plain_register_option );
    if ( tmp_called_name_25 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 330;

        goto try_except_handler_8;
    }
    tmp_tuple_element_13 = const_str_plain_notebook_repr_html;
    tmp_args_name_14 = PyTuple_New( 3 );
    Py_INCREF( tmp_tuple_element_13 );
    PyTuple_SET_ITEM( tmp_args_name_14, 0, tmp_tuple_element_13 );
    tmp_tuple_element_13 = Py_True;
    Py_INCREF( tmp_tuple_element_13 );
    PyTuple_SET_ITEM( tmp_args_name_14, 1, tmp_tuple_element_13 );
    tmp_tuple_element_13 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_nb_repr_h_doc );

    if (unlikely( tmp_tuple_element_13 == NULL ))
    {
        tmp_tuple_element_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_pc_nb_repr_h_doc );
    }

    if ( tmp_tuple_element_13 == NULL )
    {
        Py_DECREF( tmp_called_name_25 );
        Py_DECREF( tmp_args_name_14 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "pc_nb_repr_h_doc" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 330;

        goto try_except_handler_8;
    }

    Py_INCREF( tmp_tuple_element_13 );
    PyTuple_SET_ITEM( tmp_args_name_14, 2, tmp_tuple_element_13 );
    tmp_dict_key_16 = const_str_plain_validator;
    tmp_dict_value_16 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_is_bool );

    if (unlikely( tmp_dict_value_16 == NULL ))
    {
        tmp_dict_value_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_bool );
    }

    if ( tmp_dict_value_16 == NULL )
    {
        Py_DECREF( tmp_called_name_25 );
        Py_DECREF( tmp_args_name_14 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_bool" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 331;

        goto try_except_handler_8;
    }

    tmp_kw_name_14 = _PyDict_NewPresized( 1 );
    tmp_res = PyDict_SetItem( tmp_kw_name_14, tmp_dict_key_16, tmp_dict_value_16 );
    assert( !(tmp_res != 0) );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 330;
    tmp_unused = CALL_FUNCTION( tmp_called_name_25, tmp_args_name_14, tmp_kw_name_14 );
    Py_DECREF( tmp_called_name_25 );
    Py_DECREF( tmp_args_name_14 );
    Py_DECREF( tmp_kw_name_14 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 330;

        goto try_except_handler_8;
    }
    Py_DECREF( tmp_unused );
    tmp_source_name_19 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_source_name_19 == NULL ))
    {
        tmp_source_name_19 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_source_name_19 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 332;

        goto try_except_handler_8;
    }

    tmp_called_name_26 = LOOKUP_ATTRIBUTE( tmp_source_name_19, const_str_plain_register_option );
    if ( tmp_called_name_26 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 332;

        goto try_except_handler_8;
    }
    tmp_tuple_element_14 = const_str_plain_date_dayfirst;
    tmp_args_name_15 = PyTuple_New( 3 );
    Py_INCREF( tmp_tuple_element_14 );
    PyTuple_SET_ITEM( tmp_args_name_15, 0, tmp_tuple_element_14 );
    tmp_tuple_element_14 = Py_False;
    Py_INCREF( tmp_tuple_element_14 );
    PyTuple_SET_ITEM( tmp_args_name_15, 1, tmp_tuple_element_14 );
    tmp_tuple_element_14 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_date_dayfirst_doc );

    if (unlikely( tmp_tuple_element_14 == NULL ))
    {
        tmp_tuple_element_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_pc_date_dayfirst_doc );
    }

    if ( tmp_tuple_element_14 == NULL )
    {
        Py_DECREF( tmp_called_name_26 );
        Py_DECREF( tmp_args_name_15 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "pc_date_dayfirst_doc" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 332;

        goto try_except_handler_8;
    }

    Py_INCREF( tmp_tuple_element_14 );
    PyTuple_SET_ITEM( tmp_args_name_15, 2, tmp_tuple_element_14 );
    tmp_dict_key_17 = const_str_plain_validator;
    tmp_dict_value_17 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_is_bool );

    if (unlikely( tmp_dict_value_17 == NULL ))
    {
        tmp_dict_value_17 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_bool );
    }

    if ( tmp_dict_value_17 == NULL )
    {
        Py_DECREF( tmp_called_name_26 );
        Py_DECREF( tmp_args_name_15 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_bool" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 333;

        goto try_except_handler_8;
    }

    tmp_kw_name_15 = _PyDict_NewPresized( 1 );
    tmp_res = PyDict_SetItem( tmp_kw_name_15, tmp_dict_key_17, tmp_dict_value_17 );
    assert( !(tmp_res != 0) );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 332;
    tmp_unused = CALL_FUNCTION( tmp_called_name_26, tmp_args_name_15, tmp_kw_name_15 );
    Py_DECREF( tmp_called_name_26 );
    Py_DECREF( tmp_args_name_15 );
    Py_DECREF( tmp_kw_name_15 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 332;

        goto try_except_handler_8;
    }
    Py_DECREF( tmp_unused );
    tmp_source_name_20 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_source_name_20 == NULL ))
    {
        tmp_source_name_20 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_source_name_20 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 334;

        goto try_except_handler_8;
    }

    tmp_called_name_27 = LOOKUP_ATTRIBUTE( tmp_source_name_20, const_str_plain_register_option );
    if ( tmp_called_name_27 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 334;

        goto try_except_handler_8;
    }
    tmp_tuple_element_15 = const_str_plain_date_yearfirst;
    tmp_args_name_16 = PyTuple_New( 3 );
    Py_INCREF( tmp_tuple_element_15 );
    PyTuple_SET_ITEM( tmp_args_name_16, 0, tmp_tuple_element_15 );
    tmp_tuple_element_15 = Py_False;
    Py_INCREF( tmp_tuple_element_15 );
    PyTuple_SET_ITEM( tmp_args_name_16, 1, tmp_tuple_element_15 );
    tmp_tuple_element_15 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_date_yearfirst_doc );

    if (unlikely( tmp_tuple_element_15 == NULL ))
    {
        tmp_tuple_element_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_pc_date_yearfirst_doc );
    }

    if ( tmp_tuple_element_15 == NULL )
    {
        Py_DECREF( tmp_called_name_27 );
        Py_DECREF( tmp_args_name_16 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "pc_date_yearfirst_doc" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 334;

        goto try_except_handler_8;
    }

    Py_INCREF( tmp_tuple_element_15 );
    PyTuple_SET_ITEM( tmp_args_name_16, 2, tmp_tuple_element_15 );
    tmp_dict_key_18 = const_str_plain_validator;
    tmp_dict_value_18 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_is_bool );

    if (unlikely( tmp_dict_value_18 == NULL ))
    {
        tmp_dict_value_18 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_bool );
    }

    if ( tmp_dict_value_18 == NULL )
    {
        Py_DECREF( tmp_called_name_27 );
        Py_DECREF( tmp_args_name_16 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_bool" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 335;

        goto try_except_handler_8;
    }

    tmp_kw_name_16 = _PyDict_NewPresized( 1 );
    tmp_res = PyDict_SetItem( tmp_kw_name_16, tmp_dict_key_18, tmp_dict_value_18 );
    assert( !(tmp_res != 0) );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 334;
    tmp_unused = CALL_FUNCTION( tmp_called_name_27, tmp_args_name_16, tmp_kw_name_16 );
    Py_DECREF( tmp_called_name_27 );
    Py_DECREF( tmp_args_name_16 );
    Py_DECREF( tmp_kw_name_16 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 334;

        goto try_except_handler_8;
    }
    Py_DECREF( tmp_unused );
    tmp_source_name_21 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_source_name_21 == NULL ))
    {
        tmp_source_name_21 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_source_name_21 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 336;

        goto try_except_handler_8;
    }

    tmp_called_name_28 = LOOKUP_ATTRIBUTE( tmp_source_name_21, const_str_plain_register_option );
    if ( tmp_called_name_28 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 336;

        goto try_except_handler_8;
    }
    tmp_tuple_element_16 = const_str_plain_pprint_nest_depth;
    tmp_args_name_17 = PyTuple_New( 3 );
    Py_INCREF( tmp_tuple_element_16 );
    PyTuple_SET_ITEM( tmp_args_name_17, 0, tmp_tuple_element_16 );
    tmp_tuple_element_16 = const_int_pos_3;
    Py_INCREF( tmp_tuple_element_16 );
    PyTuple_SET_ITEM( tmp_args_name_17, 1, tmp_tuple_element_16 );
    tmp_tuple_element_16 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_pprint_nest_depth );

    if (unlikely( tmp_tuple_element_16 == NULL ))
    {
        tmp_tuple_element_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_pc_pprint_nest_depth );
    }

    if ( tmp_tuple_element_16 == NULL )
    {
        Py_DECREF( tmp_called_name_28 );
        Py_DECREF( tmp_args_name_17 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "pc_pprint_nest_depth" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 336;

        goto try_except_handler_8;
    }

    Py_INCREF( tmp_tuple_element_16 );
    PyTuple_SET_ITEM( tmp_args_name_17, 2, tmp_tuple_element_16 );
    tmp_dict_key_19 = const_str_plain_validator;
    tmp_dict_value_19 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_is_int );

    if (unlikely( tmp_dict_value_19 == NULL ))
    {
        tmp_dict_value_19 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_int );
    }

    if ( tmp_dict_value_19 == NULL )
    {
        Py_DECREF( tmp_called_name_28 );
        Py_DECREF( tmp_args_name_17 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_int" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 337;

        goto try_except_handler_8;
    }

    tmp_kw_name_17 = _PyDict_NewPresized( 1 );
    tmp_res = PyDict_SetItem( tmp_kw_name_17, tmp_dict_key_19, tmp_dict_value_19 );
    assert( !(tmp_res != 0) );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 336;
    tmp_unused = CALL_FUNCTION( tmp_called_name_28, tmp_args_name_17, tmp_kw_name_17 );
    Py_DECREF( tmp_called_name_28 );
    Py_DECREF( tmp_args_name_17 );
    Py_DECREF( tmp_kw_name_17 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 336;

        goto try_except_handler_8;
    }
    Py_DECREF( tmp_unused );
    tmp_source_name_22 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_source_name_22 == NULL ))
    {
        tmp_source_name_22 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_source_name_22 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 338;

        goto try_except_handler_8;
    }

    tmp_called_name_29 = LOOKUP_ATTRIBUTE( tmp_source_name_22, const_str_plain_register_option );
    if ( tmp_called_name_29 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 338;

        goto try_except_handler_8;
    }
    tmp_tuple_element_17 = const_str_plain_multi_sparse;
    tmp_args_name_18 = PyTuple_New( 3 );
    Py_INCREF( tmp_tuple_element_17 );
    PyTuple_SET_ITEM( tmp_args_name_18, 0, tmp_tuple_element_17 );
    tmp_tuple_element_17 = Py_True;
    Py_INCREF( tmp_tuple_element_17 );
    PyTuple_SET_ITEM( tmp_args_name_18, 1, tmp_tuple_element_17 );
    tmp_tuple_element_17 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_multi_sparse_doc );

    if (unlikely( tmp_tuple_element_17 == NULL ))
    {
        tmp_tuple_element_17 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_pc_multi_sparse_doc );
    }

    if ( tmp_tuple_element_17 == NULL )
    {
        Py_DECREF( tmp_called_name_29 );
        Py_DECREF( tmp_args_name_18 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "pc_multi_sparse_doc" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 338;

        goto try_except_handler_8;
    }

    Py_INCREF( tmp_tuple_element_17 );
    PyTuple_SET_ITEM( tmp_args_name_18, 2, tmp_tuple_element_17 );
    tmp_dict_key_20 = const_str_plain_validator;
    tmp_dict_value_20 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_is_bool );

    if (unlikely( tmp_dict_value_20 == NULL ))
    {
        tmp_dict_value_20 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_bool );
    }

    if ( tmp_dict_value_20 == NULL )
    {
        Py_DECREF( tmp_called_name_29 );
        Py_DECREF( tmp_args_name_18 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_bool" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 339;

        goto try_except_handler_8;
    }

    tmp_kw_name_18 = _PyDict_NewPresized( 1 );
    tmp_res = PyDict_SetItem( tmp_kw_name_18, tmp_dict_key_20, tmp_dict_value_20 );
    assert( !(tmp_res != 0) );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 338;
    tmp_unused = CALL_FUNCTION( tmp_called_name_29, tmp_args_name_18, tmp_kw_name_18 );
    Py_DECREF( tmp_called_name_29 );
    Py_DECREF( tmp_args_name_18 );
    Py_DECREF( tmp_kw_name_18 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 338;

        goto try_except_handler_8;
    }
    Py_DECREF( tmp_unused );
    tmp_source_name_23 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_source_name_23 == NULL ))
    {
        tmp_source_name_23 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_source_name_23 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 340;

        goto try_except_handler_8;
    }

    tmp_called_name_30 = LOOKUP_ATTRIBUTE( tmp_source_name_23, const_str_plain_register_option );
    if ( tmp_called_name_30 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 340;

        goto try_except_handler_8;
    }
    tmp_tuple_element_18 = const_str_plain_encoding;
    tmp_args_name_19 = PyTuple_New( 3 );
    Py_INCREF( tmp_tuple_element_18 );
    PyTuple_SET_ITEM( tmp_args_name_19, 0, tmp_tuple_element_18 );
    tmp_called_name_31 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_detect_console_encoding );

    if (unlikely( tmp_called_name_31 == NULL ))
    {
        tmp_called_name_31 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_detect_console_encoding );
    }

    if ( tmp_called_name_31 == NULL )
    {
        Py_DECREF( tmp_called_name_30 );
        Py_DECREF( tmp_args_name_19 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "detect_console_encoding" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 340;

        goto try_except_handler_8;
    }

    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 340;
    tmp_tuple_element_18 = CALL_FUNCTION_NO_ARGS( tmp_called_name_31 );
    if ( tmp_tuple_element_18 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_30 );
        Py_DECREF( tmp_args_name_19 );

        exception_lineno = 340;

        goto try_except_handler_8;
    }
    PyTuple_SET_ITEM( tmp_args_name_19, 1, tmp_tuple_element_18 );
    tmp_tuple_element_18 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_encoding_doc );

    if (unlikely( tmp_tuple_element_18 == NULL ))
    {
        tmp_tuple_element_18 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_pc_encoding_doc );
    }

    if ( tmp_tuple_element_18 == NULL )
    {
        Py_DECREF( tmp_called_name_30 );
        Py_DECREF( tmp_args_name_19 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "pc_encoding_doc" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 340;

        goto try_except_handler_8;
    }

    Py_INCREF( tmp_tuple_element_18 );
    PyTuple_SET_ITEM( tmp_args_name_19, 2, tmp_tuple_element_18 );
    tmp_dict_key_21 = const_str_plain_validator;
    tmp_dict_value_21 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_is_text );

    if (unlikely( tmp_dict_value_21 == NULL ))
    {
        tmp_dict_value_21 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_text );
    }

    if ( tmp_dict_value_21 == NULL )
    {
        Py_DECREF( tmp_called_name_30 );
        Py_DECREF( tmp_args_name_19 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_text" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 341;

        goto try_except_handler_8;
    }

    tmp_kw_name_19 = _PyDict_NewPresized( 1 );
    tmp_res = PyDict_SetItem( tmp_kw_name_19, tmp_dict_key_21, tmp_dict_value_21 );
    assert( !(tmp_res != 0) );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 340;
    tmp_unused = CALL_FUNCTION( tmp_called_name_30, tmp_args_name_19, tmp_kw_name_19 );
    Py_DECREF( tmp_called_name_30 );
    Py_DECREF( tmp_args_name_19 );
    Py_DECREF( tmp_kw_name_19 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 340;

        goto try_except_handler_8;
    }
    Py_DECREF( tmp_unused );
    tmp_source_name_24 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_source_name_24 == NULL ))
    {
        tmp_source_name_24 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_source_name_24 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 342;

        goto try_except_handler_8;
    }

    tmp_called_name_32 = LOOKUP_ATTRIBUTE( tmp_source_name_24, const_str_plain_register_option );
    if ( tmp_called_name_32 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 342;

        goto try_except_handler_8;
    }
    tmp_args_element_name_5 = const_str_plain_expand_frame_repr;
    tmp_args_element_name_6 = Py_True;
    tmp_args_element_name_7 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_expand_repr_doc );

    if (unlikely( tmp_args_element_name_7 == NULL ))
    {
        tmp_args_element_name_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_pc_expand_repr_doc );
    }

    if ( tmp_args_element_name_7 == NULL )
    {
        Py_DECREF( tmp_called_name_32 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "pc_expand_repr_doc" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 342;

        goto try_except_handler_8;
    }

    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 342;
    {
        PyObject *call_args[] = { tmp_args_element_name_5, tmp_args_element_name_6, tmp_args_element_name_7 };
        tmp_unused = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_32, call_args );
    }

    Py_DECREF( tmp_called_name_32 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 342;

        goto try_except_handler_8;
    }
    Py_DECREF( tmp_unused );
    tmp_source_name_25 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_source_name_25 == NULL ))
    {
        tmp_source_name_25 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_source_name_25 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 343;

        goto try_except_handler_8;
    }

    tmp_called_name_33 = LOOKUP_ATTRIBUTE( tmp_source_name_25, const_str_plain_register_option );
    if ( tmp_called_name_33 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 343;

        goto try_except_handler_8;
    }
    tmp_tuple_element_19 = const_str_plain_show_dimensions;
    tmp_args_name_20 = PyTuple_New( 3 );
    Py_INCREF( tmp_tuple_element_19 );
    PyTuple_SET_ITEM( tmp_args_name_20, 0, tmp_tuple_element_19 );
    tmp_tuple_element_19 = const_str_plain_truncate;
    Py_INCREF( tmp_tuple_element_19 );
    PyTuple_SET_ITEM( tmp_args_name_20, 1, tmp_tuple_element_19 );
    tmp_tuple_element_19 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_show_dimensions_doc );

    if (unlikely( tmp_tuple_element_19 == NULL ))
    {
        tmp_tuple_element_19 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_pc_show_dimensions_doc );
    }

    if ( tmp_tuple_element_19 == NULL )
    {
        Py_DECREF( tmp_called_name_33 );
        Py_DECREF( tmp_args_name_20 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "pc_show_dimensions_doc" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 343;

        goto try_except_handler_8;
    }

    Py_INCREF( tmp_tuple_element_19 );
    PyTuple_SET_ITEM( tmp_args_name_20, 2, tmp_tuple_element_19 );
    tmp_dict_key_22 = const_str_plain_validator;
    tmp_called_name_34 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_is_one_of_factory );

    if (unlikely( tmp_called_name_34 == NULL ))
    {
        tmp_called_name_34 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_one_of_factory );
    }

    if ( tmp_called_name_34 == NULL )
    {
        Py_DECREF( tmp_called_name_33 );
        Py_DECREF( tmp_args_name_20 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_one_of_factory" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 344;

        goto try_except_handler_8;
    }

    tmp_call_arg_element_4 = LIST_COPY( const_list_true_false_str_plain_truncate_list );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 344;
    {
        PyObject *call_args[] = { tmp_call_arg_element_4 };
        tmp_dict_value_22 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_34, call_args );
    }

    Py_DECREF( tmp_call_arg_element_4 );
    if ( tmp_dict_value_22 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_33 );
        Py_DECREF( tmp_args_name_20 );

        exception_lineno = 344;

        goto try_except_handler_8;
    }
    tmp_kw_name_20 = _PyDict_NewPresized( 1 );
    tmp_res = PyDict_SetItem( tmp_kw_name_20, tmp_dict_key_22, tmp_dict_value_22 );
    Py_DECREF( tmp_dict_value_22 );
    assert( !(tmp_res != 0) );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 343;
    tmp_unused = CALL_FUNCTION( tmp_called_name_33, tmp_args_name_20, tmp_kw_name_20 );
    Py_DECREF( tmp_called_name_33 );
    Py_DECREF( tmp_args_name_20 );
    Py_DECREF( tmp_kw_name_20 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 343;

        goto try_except_handler_8;
    }
    Py_DECREF( tmp_unused );
    tmp_source_name_26 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_source_name_26 == NULL ))
    {
        tmp_source_name_26 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_source_name_26 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 345;

        goto try_except_handler_8;
    }

    tmp_called_name_35 = LOOKUP_ATTRIBUTE( tmp_source_name_26, const_str_plain_register_option );
    if ( tmp_called_name_35 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 345;

        goto try_except_handler_8;
    }
    tmp_args_element_name_8 = const_str_plain_chop_threshold;
    tmp_args_element_name_9 = Py_None;
    tmp_args_element_name_10 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_chop_threshold_doc );

    if (unlikely( tmp_args_element_name_10 == NULL ))
    {
        tmp_args_element_name_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_pc_chop_threshold_doc );
    }

    if ( tmp_args_element_name_10 == NULL )
    {
        Py_DECREF( tmp_called_name_35 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "pc_chop_threshold_doc" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 345;

        goto try_except_handler_8;
    }

    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 345;
    {
        PyObject *call_args[] = { tmp_args_element_name_8, tmp_args_element_name_9, tmp_args_element_name_10 };
        tmp_unused = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_35, call_args );
    }

    Py_DECREF( tmp_called_name_35 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 345;

        goto try_except_handler_8;
    }
    Py_DECREF( tmp_unused );
    tmp_source_name_27 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_source_name_27 == NULL ))
    {
        tmp_source_name_27 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_source_name_27 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 346;

        goto try_except_handler_8;
    }

    tmp_called_name_36 = LOOKUP_ATTRIBUTE( tmp_source_name_27, const_str_plain_register_option );
    if ( tmp_called_name_36 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 346;

        goto try_except_handler_8;
    }
    tmp_args_element_name_11 = const_str_plain_max_seq_items;
    tmp_args_element_name_12 = const_int_pos_100;
    tmp_args_element_name_13 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_max_seq_items );

    if (unlikely( tmp_args_element_name_13 == NULL ))
    {
        tmp_args_element_name_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_pc_max_seq_items );
    }

    if ( tmp_args_element_name_13 == NULL )
    {
        Py_DECREF( tmp_called_name_36 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "pc_max_seq_items" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 346;

        goto try_except_handler_8;
    }

    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 346;
    {
        PyObject *call_args[] = { tmp_args_element_name_11, tmp_args_element_name_12, tmp_args_element_name_13 };
        tmp_unused = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_36, call_args );
    }

    Py_DECREF( tmp_called_name_36 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 346;

        goto try_except_handler_8;
    }
    Py_DECREF( tmp_unused );
    tmp_source_name_28 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_source_name_28 == NULL ))
    {
        tmp_source_name_28 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_source_name_28 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 347;

        goto try_except_handler_8;
    }

    tmp_called_name_37 = LOOKUP_ATTRIBUTE( tmp_source_name_28, const_str_plain_register_option );
    if ( tmp_called_name_37 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 347;

        goto try_except_handler_8;
    }
    tmp_tuple_element_20 = const_str_plain_width;
    tmp_args_name_21 = PyTuple_New( 3 );
    Py_INCREF( tmp_tuple_element_20 );
    PyTuple_SET_ITEM( tmp_args_name_21, 0, tmp_tuple_element_20 );
    tmp_tuple_element_20 = const_int_pos_80;
    Py_INCREF( tmp_tuple_element_20 );
    PyTuple_SET_ITEM( tmp_args_name_21, 1, tmp_tuple_element_20 );
    tmp_tuple_element_20 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_width_doc );

    if (unlikely( tmp_tuple_element_20 == NULL ))
    {
        tmp_tuple_element_20 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_pc_width_doc );
    }

    if ( tmp_tuple_element_20 == NULL )
    {
        Py_DECREF( tmp_called_name_37 );
        Py_DECREF( tmp_args_name_21 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "pc_width_doc" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 347;

        goto try_except_handler_8;
    }

    Py_INCREF( tmp_tuple_element_20 );
    PyTuple_SET_ITEM( tmp_args_name_21, 2, tmp_tuple_element_20 );
    tmp_dict_key_23 = const_str_plain_validator;
    tmp_called_name_38 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_is_instance_factory );

    if (unlikely( tmp_called_name_38 == NULL ))
    {
        tmp_called_name_38 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_instance_factory );
    }

    if ( tmp_called_name_38 == NULL )
    {
        Py_DECREF( tmp_called_name_37 );
        Py_DECREF( tmp_args_name_21 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_instance_factory" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 348;

        goto try_except_handler_8;
    }

    tmp_call_arg_element_5 = LIST_COPY( const_list_anon_NoneType_type_int_list );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 348;
    {
        PyObject *call_args[] = { tmp_call_arg_element_5 };
        tmp_dict_value_23 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_38, call_args );
    }

    Py_DECREF( tmp_call_arg_element_5 );
    if ( tmp_dict_value_23 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_37 );
        Py_DECREF( tmp_args_name_21 );

        exception_lineno = 348;

        goto try_except_handler_8;
    }
    tmp_kw_name_21 = _PyDict_NewPresized( 1 );
    tmp_res = PyDict_SetItem( tmp_kw_name_21, tmp_dict_key_23, tmp_dict_value_23 );
    Py_DECREF( tmp_dict_value_23 );
    assert( !(tmp_res != 0) );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 347;
    tmp_unused = CALL_FUNCTION( tmp_called_name_37, tmp_args_name_21, tmp_kw_name_21 );
    Py_DECREF( tmp_called_name_37 );
    Py_DECREF( tmp_args_name_21 );
    Py_DECREF( tmp_kw_name_21 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 347;

        goto try_except_handler_8;
    }
    Py_DECREF( tmp_unused );
    tmp_source_name_29 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_source_name_29 == NULL ))
    {
        tmp_source_name_29 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_source_name_29 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 349;

        goto try_except_handler_8;
    }

    tmp_called_name_39 = LOOKUP_ATTRIBUTE( tmp_source_name_29, const_str_plain_register_option );
    if ( tmp_called_name_39 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 349;

        goto try_except_handler_8;
    }
    tmp_tuple_element_21 = const_str_plain_memory_usage;
    tmp_args_name_22 = PyTuple_New( 3 );
    Py_INCREF( tmp_tuple_element_21 );
    PyTuple_SET_ITEM( tmp_args_name_22, 0, tmp_tuple_element_21 );
    tmp_tuple_element_21 = Py_True;
    Py_INCREF( tmp_tuple_element_21 );
    PyTuple_SET_ITEM( tmp_args_name_22, 1, tmp_tuple_element_21 );
    tmp_tuple_element_21 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_memory_usage_doc );

    if (unlikely( tmp_tuple_element_21 == NULL ))
    {
        tmp_tuple_element_21 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_pc_memory_usage_doc );
    }

    if ( tmp_tuple_element_21 == NULL )
    {
        Py_DECREF( tmp_called_name_39 );
        Py_DECREF( tmp_args_name_22 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "pc_memory_usage_doc" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 349;

        goto try_except_handler_8;
    }

    Py_INCREF( tmp_tuple_element_21 );
    PyTuple_SET_ITEM( tmp_args_name_22, 2, tmp_tuple_element_21 );
    tmp_dict_key_24 = const_str_plain_validator;
    tmp_called_name_40 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_is_one_of_factory );

    if (unlikely( tmp_called_name_40 == NULL ))
    {
        tmp_called_name_40 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_one_of_factory );
    }

    if ( tmp_called_name_40 == NULL )
    {
        Py_DECREF( tmp_called_name_39 );
        Py_DECREF( tmp_args_name_22 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_one_of_factory" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 350;

        goto try_except_handler_8;
    }

    tmp_call_arg_element_6 = LIST_COPY( const_list_none_true_false_str_plain_deep_list );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 350;
    {
        PyObject *call_args[] = { tmp_call_arg_element_6 };
        tmp_dict_value_24 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_40, call_args );
    }

    Py_DECREF( tmp_call_arg_element_6 );
    if ( tmp_dict_value_24 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_39 );
        Py_DECREF( tmp_args_name_22 );

        exception_lineno = 350;

        goto try_except_handler_8;
    }
    tmp_kw_name_22 = _PyDict_NewPresized( 1 );
    tmp_res = PyDict_SetItem( tmp_kw_name_22, tmp_dict_key_24, tmp_dict_value_24 );
    Py_DECREF( tmp_dict_value_24 );
    assert( !(tmp_res != 0) );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 349;
    tmp_unused = CALL_FUNCTION( tmp_called_name_39, tmp_args_name_22, tmp_kw_name_22 );
    Py_DECREF( tmp_called_name_39 );
    Py_DECREF( tmp_args_name_22 );
    Py_DECREF( tmp_kw_name_22 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 349;

        goto try_except_handler_8;
    }
    Py_DECREF( tmp_unused );
    tmp_source_name_30 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_source_name_30 == NULL ))
    {
        tmp_source_name_30 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_source_name_30 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 352;

        goto try_except_handler_8;
    }

    tmp_called_name_41 = LOOKUP_ATTRIBUTE( tmp_source_name_30, const_str_plain_register_option );
    if ( tmp_called_name_41 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 352;

        goto try_except_handler_8;
    }
    tmp_tuple_element_22 = const_str_digest_a8ce4dc3e5952b64cf078ae95e08188a;
    tmp_args_name_23 = PyTuple_New( 3 );
    Py_INCREF( tmp_tuple_element_22 );
    PyTuple_SET_ITEM( tmp_args_name_23, 0, tmp_tuple_element_22 );
    tmp_tuple_element_22 = Py_False;
    Py_INCREF( tmp_tuple_element_22 );
    PyTuple_SET_ITEM( tmp_args_name_23, 1, tmp_tuple_element_22 );
    tmp_tuple_element_22 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_east_asian_width_doc );

    if (unlikely( tmp_tuple_element_22 == NULL ))
    {
        tmp_tuple_element_22 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_pc_east_asian_width_doc );
    }

    if ( tmp_tuple_element_22 == NULL )
    {
        Py_DECREF( tmp_called_name_41 );
        Py_DECREF( tmp_args_name_23 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "pc_east_asian_width_doc" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 353;

        goto try_except_handler_8;
    }

    Py_INCREF( tmp_tuple_element_22 );
    PyTuple_SET_ITEM( tmp_args_name_23, 2, tmp_tuple_element_22 );
    tmp_dict_key_25 = const_str_plain_validator;
    tmp_dict_value_25 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_is_bool );

    if (unlikely( tmp_dict_value_25 == NULL ))
    {
        tmp_dict_value_25 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_bool );
    }

    if ( tmp_dict_value_25 == NULL )
    {
        Py_DECREF( tmp_called_name_41 );
        Py_DECREF( tmp_args_name_23 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_bool" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 353;

        goto try_except_handler_8;
    }

    tmp_kw_name_23 = _PyDict_NewPresized( 1 );
    tmp_res = PyDict_SetItem( tmp_kw_name_23, tmp_dict_key_25, tmp_dict_value_25 );
    assert( !(tmp_res != 0) );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 352;
    tmp_unused = CALL_FUNCTION( tmp_called_name_41, tmp_args_name_23, tmp_kw_name_23 );
    Py_DECREF( tmp_called_name_41 );
    Py_DECREF( tmp_args_name_23 );
    Py_DECREF( tmp_kw_name_23 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 352;

        goto try_except_handler_8;
    }
    Py_DECREF( tmp_unused );
    tmp_source_name_31 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_source_name_31 == NULL ))
    {
        tmp_source_name_31 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_source_name_31 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 354;

        goto try_except_handler_8;
    }

    tmp_called_name_42 = LOOKUP_ATTRIBUTE( tmp_source_name_31, const_str_plain_register_option );
    if ( tmp_called_name_42 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 354;

        goto try_except_handler_8;
    }
    tmp_tuple_element_23 = const_str_digest_c8ce5ba4cfc04c1c564d4d58eee1151a;
    tmp_args_name_24 = PyTuple_New( 3 );
    Py_INCREF( tmp_tuple_element_23 );
    PyTuple_SET_ITEM( tmp_args_name_24, 0, tmp_tuple_element_23 );
    tmp_tuple_element_23 = Py_False;
    Py_INCREF( tmp_tuple_element_23 );
    PyTuple_SET_ITEM( tmp_args_name_24, 1, tmp_tuple_element_23 );
    tmp_tuple_element_23 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_east_asian_width_doc );

    if (unlikely( tmp_tuple_element_23 == NULL ))
    {
        tmp_tuple_element_23 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_pc_east_asian_width_doc );
    }

    if ( tmp_tuple_element_23 == NULL )
    {
        Py_DECREF( tmp_called_name_42 );
        Py_DECREF( tmp_args_name_24 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "pc_east_asian_width_doc" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 355;

        goto try_except_handler_8;
    }

    Py_INCREF( tmp_tuple_element_23 );
    PyTuple_SET_ITEM( tmp_args_name_24, 2, tmp_tuple_element_23 );
    tmp_dict_key_26 = const_str_plain_validator;
    tmp_dict_value_26 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_is_bool );

    if (unlikely( tmp_dict_value_26 == NULL ))
    {
        tmp_dict_value_26 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_bool );
    }

    if ( tmp_dict_value_26 == NULL )
    {
        Py_DECREF( tmp_called_name_42 );
        Py_DECREF( tmp_args_name_24 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_bool" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 355;

        goto try_except_handler_8;
    }

    tmp_kw_name_24 = _PyDict_NewPresized( 1 );
    tmp_res = PyDict_SetItem( tmp_kw_name_24, tmp_dict_key_26, tmp_dict_value_26 );
    assert( !(tmp_res != 0) );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 354;
    tmp_unused = CALL_FUNCTION( tmp_called_name_42, tmp_args_name_24, tmp_kw_name_24 );
    Py_DECREF( tmp_called_name_42 );
    Py_DECREF( tmp_args_name_24 );
    Py_DECREF( tmp_kw_name_24 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 354;

        goto try_except_handler_8;
    }
    Py_DECREF( tmp_unused );
    tmp_source_name_32 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_source_name_32 == NULL ))
    {
        tmp_source_name_32 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_source_name_32 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 356;

        goto try_except_handler_8;
    }

    tmp_called_name_43 = LOOKUP_ATTRIBUTE( tmp_source_name_32, const_str_plain_register_option );
    if ( tmp_called_name_43 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 356;

        goto try_except_handler_8;
    }
    tmp_tuple_element_24 = const_str_digest_a4c9172024d586d4113a4ddc2c7331e3;
    tmp_args_name_25 = PyTuple_New( 3 );
    Py_INCREF( tmp_tuple_element_24 );
    PyTuple_SET_ITEM( tmp_args_name_25, 0, tmp_tuple_element_24 );
    tmp_tuple_element_24 = Py_False;
    Py_INCREF( tmp_tuple_element_24 );
    PyTuple_SET_ITEM( tmp_args_name_25, 1, tmp_tuple_element_24 );
    tmp_tuple_element_24 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_latex_repr_doc );

    if (unlikely( tmp_tuple_element_24 == NULL ))
    {
        tmp_tuple_element_24 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_pc_latex_repr_doc );
    }

    if ( tmp_tuple_element_24 == NULL )
    {
        Py_DECREF( tmp_called_name_43 );
        Py_DECREF( tmp_args_name_25 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "pc_latex_repr_doc" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 357;

        goto try_except_handler_8;
    }

    Py_INCREF( tmp_tuple_element_24 );
    PyTuple_SET_ITEM( tmp_args_name_25, 2, tmp_tuple_element_24 );
    tmp_dict_key_27 = const_str_plain_validator;
    tmp_dict_value_27 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_is_bool );

    if (unlikely( tmp_dict_value_27 == NULL ))
    {
        tmp_dict_value_27 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_bool );
    }

    if ( tmp_dict_value_27 == NULL )
    {
        Py_DECREF( tmp_called_name_43 );
        Py_DECREF( tmp_args_name_25 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_bool" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 357;

        goto try_except_handler_8;
    }

    tmp_kw_name_25 = _PyDict_NewPresized( 1 );
    tmp_res = PyDict_SetItem( tmp_kw_name_25, tmp_dict_key_27, tmp_dict_value_27 );
    assert( !(tmp_res != 0) );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 356;
    tmp_unused = CALL_FUNCTION( tmp_called_name_43, tmp_args_name_25, tmp_kw_name_25 );
    Py_DECREF( tmp_called_name_43 );
    Py_DECREF( tmp_args_name_25 );
    Py_DECREF( tmp_kw_name_25 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 356;

        goto try_except_handler_8;
    }
    Py_DECREF( tmp_unused );
    tmp_source_name_33 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_source_name_33 == NULL ))
    {
        tmp_source_name_33 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_source_name_33 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 358;

        goto try_except_handler_8;
    }

    tmp_called_name_44 = LOOKUP_ATTRIBUTE( tmp_source_name_33, const_str_plain_register_option );
    if ( tmp_called_name_44 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 358;

        goto try_except_handler_8;
    }
    tmp_tuple_element_25 = const_str_digest_1c14e0e1f14a8db299372128951bac65;
    tmp_args_name_26 = PyTuple_New( 3 );
    Py_INCREF( tmp_tuple_element_25 );
    PyTuple_SET_ITEM( tmp_args_name_26, 0, tmp_tuple_element_25 );
    tmp_tuple_element_25 = Py_True;
    Py_INCREF( tmp_tuple_element_25 );
    PyTuple_SET_ITEM( tmp_args_name_26, 1, tmp_tuple_element_25 );
    tmp_tuple_element_25 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_latex_escape );

    if (unlikely( tmp_tuple_element_25 == NULL ))
    {
        tmp_tuple_element_25 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_pc_latex_escape );
    }

    if ( tmp_tuple_element_25 == NULL )
    {
        Py_DECREF( tmp_called_name_44 );
        Py_DECREF( tmp_args_name_26 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "pc_latex_escape" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 358;

        goto try_except_handler_8;
    }

    Py_INCREF( tmp_tuple_element_25 );
    PyTuple_SET_ITEM( tmp_args_name_26, 2, tmp_tuple_element_25 );
    tmp_dict_key_28 = const_str_plain_validator;
    tmp_dict_value_28 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_is_bool );

    if (unlikely( tmp_dict_value_28 == NULL ))
    {
        tmp_dict_value_28 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_bool );
    }

    if ( tmp_dict_value_28 == NULL )
    {
        Py_DECREF( tmp_called_name_44 );
        Py_DECREF( tmp_args_name_26 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_bool" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 359;

        goto try_except_handler_8;
    }

    tmp_kw_name_26 = _PyDict_NewPresized( 1 );
    tmp_res = PyDict_SetItem( tmp_kw_name_26, tmp_dict_key_28, tmp_dict_value_28 );
    assert( !(tmp_res != 0) );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 358;
    tmp_unused = CALL_FUNCTION( tmp_called_name_44, tmp_args_name_26, tmp_kw_name_26 );
    Py_DECREF( tmp_called_name_44 );
    Py_DECREF( tmp_args_name_26 );
    Py_DECREF( tmp_kw_name_26 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 358;

        goto try_except_handler_8;
    }
    Py_DECREF( tmp_unused );
    tmp_source_name_34 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_source_name_34 == NULL ))
    {
        tmp_source_name_34 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_source_name_34 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 360;

        goto try_except_handler_8;
    }

    tmp_called_name_45 = LOOKUP_ATTRIBUTE( tmp_source_name_34, const_str_plain_register_option );
    if ( tmp_called_name_45 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 360;

        goto try_except_handler_8;
    }
    tmp_tuple_element_26 = const_str_digest_284e68a8357c0c7572e494cf44721a5a;
    tmp_args_name_27 = PyTuple_New( 3 );
    Py_INCREF( tmp_tuple_element_26 );
    PyTuple_SET_ITEM( tmp_args_name_27, 0, tmp_tuple_element_26 );
    tmp_tuple_element_26 = Py_False;
    Py_INCREF( tmp_tuple_element_26 );
    PyTuple_SET_ITEM( tmp_args_name_27, 1, tmp_tuple_element_26 );
    tmp_tuple_element_26 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_latex_longtable );

    if (unlikely( tmp_tuple_element_26 == NULL ))
    {
        tmp_tuple_element_26 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_pc_latex_longtable );
    }

    if ( tmp_tuple_element_26 == NULL )
    {
        Py_DECREF( tmp_called_name_45 );
        Py_DECREF( tmp_args_name_27 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "pc_latex_longtable" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 360;

        goto try_except_handler_8;
    }

    Py_INCREF( tmp_tuple_element_26 );
    PyTuple_SET_ITEM( tmp_args_name_27, 2, tmp_tuple_element_26 );
    tmp_dict_key_29 = const_str_plain_validator;
    tmp_dict_value_29 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_is_bool );

    if (unlikely( tmp_dict_value_29 == NULL ))
    {
        tmp_dict_value_29 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_bool );
    }

    if ( tmp_dict_value_29 == NULL )
    {
        Py_DECREF( tmp_called_name_45 );
        Py_DECREF( tmp_args_name_27 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_bool" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 361;

        goto try_except_handler_8;
    }

    tmp_kw_name_27 = _PyDict_NewPresized( 1 );
    tmp_res = PyDict_SetItem( tmp_kw_name_27, tmp_dict_key_29, tmp_dict_value_29 );
    assert( !(tmp_res != 0) );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 360;
    tmp_unused = CALL_FUNCTION( tmp_called_name_45, tmp_args_name_27, tmp_kw_name_27 );
    Py_DECREF( tmp_called_name_45 );
    Py_DECREF( tmp_args_name_27 );
    Py_DECREF( tmp_kw_name_27 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 360;

        goto try_except_handler_8;
    }
    Py_DECREF( tmp_unused );
    tmp_source_name_35 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_source_name_35 == NULL ))
    {
        tmp_source_name_35 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_source_name_35 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 362;

        goto try_except_handler_8;
    }

    tmp_called_name_46 = LOOKUP_ATTRIBUTE( tmp_source_name_35, const_str_plain_register_option );
    if ( tmp_called_name_46 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 362;

        goto try_except_handler_8;
    }
    tmp_tuple_element_27 = const_str_digest_efff17427f06ff36726c48901702ae1d;
    tmp_args_name_28 = PyTuple_New( 3 );
    Py_INCREF( tmp_tuple_element_27 );
    PyTuple_SET_ITEM( tmp_args_name_28, 0, tmp_tuple_element_27 );
    tmp_tuple_element_27 = Py_True;
    Py_INCREF( tmp_tuple_element_27 );
    PyTuple_SET_ITEM( tmp_args_name_28, 1, tmp_tuple_element_27 );
    tmp_tuple_element_27 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_latex_multicolumn );

    if (unlikely( tmp_tuple_element_27 == NULL ))
    {
        tmp_tuple_element_27 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_pc_latex_multicolumn );
    }

    if ( tmp_tuple_element_27 == NULL )
    {
        Py_DECREF( tmp_called_name_46 );
        Py_DECREF( tmp_args_name_28 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "pc_latex_multicolumn" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 362;

        goto try_except_handler_8;
    }

    Py_INCREF( tmp_tuple_element_27 );
    PyTuple_SET_ITEM( tmp_args_name_28, 2, tmp_tuple_element_27 );
    tmp_dict_key_30 = const_str_plain_validator;
    tmp_dict_value_30 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_is_bool );

    if (unlikely( tmp_dict_value_30 == NULL ))
    {
        tmp_dict_value_30 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_bool );
    }

    if ( tmp_dict_value_30 == NULL )
    {
        Py_DECREF( tmp_called_name_46 );
        Py_DECREF( tmp_args_name_28 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_bool" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 363;

        goto try_except_handler_8;
    }

    tmp_kw_name_28 = _PyDict_NewPresized( 1 );
    tmp_res = PyDict_SetItem( tmp_kw_name_28, tmp_dict_key_30, tmp_dict_value_30 );
    assert( !(tmp_res != 0) );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 362;
    tmp_unused = CALL_FUNCTION( tmp_called_name_46, tmp_args_name_28, tmp_kw_name_28 );
    Py_DECREF( tmp_called_name_46 );
    Py_DECREF( tmp_args_name_28 );
    Py_DECREF( tmp_kw_name_28 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 362;

        goto try_except_handler_8;
    }
    Py_DECREF( tmp_unused );
    tmp_source_name_36 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_source_name_36 == NULL ))
    {
        tmp_source_name_36 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_source_name_36 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 364;

        goto try_except_handler_8;
    }

    tmp_called_name_47 = LOOKUP_ATTRIBUTE( tmp_source_name_36, const_str_plain_register_option );
    if ( tmp_called_name_47 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 364;

        goto try_except_handler_8;
    }
    tmp_tuple_element_28 = const_str_digest_06816bb44a974157f48e12c6a7055eaa;
    tmp_args_name_29 = PyTuple_New( 3 );
    Py_INCREF( tmp_tuple_element_28 );
    PyTuple_SET_ITEM( tmp_args_name_29, 0, tmp_tuple_element_28 );
    tmp_tuple_element_28 = const_str_plain_l;
    Py_INCREF( tmp_tuple_element_28 );
    PyTuple_SET_ITEM( tmp_args_name_29, 1, tmp_tuple_element_28 );
    tmp_tuple_element_28 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_latex_multicolumn );

    if (unlikely( tmp_tuple_element_28 == NULL ))
    {
        tmp_tuple_element_28 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_pc_latex_multicolumn );
    }

    if ( tmp_tuple_element_28 == NULL )
    {
        Py_DECREF( tmp_called_name_47 );
        Py_DECREF( tmp_args_name_29 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "pc_latex_multicolumn" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 364;

        goto try_except_handler_8;
    }

    Py_INCREF( tmp_tuple_element_28 );
    PyTuple_SET_ITEM( tmp_args_name_29, 2, tmp_tuple_element_28 );
    tmp_dict_key_31 = const_str_plain_validator;
    tmp_dict_value_31 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_is_text );

    if (unlikely( tmp_dict_value_31 == NULL ))
    {
        tmp_dict_value_31 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_text );
    }

    if ( tmp_dict_value_31 == NULL )
    {
        Py_DECREF( tmp_called_name_47 );
        Py_DECREF( tmp_args_name_29 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_text" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 365;

        goto try_except_handler_8;
    }

    tmp_kw_name_29 = _PyDict_NewPresized( 1 );
    tmp_res = PyDict_SetItem( tmp_kw_name_29, tmp_dict_key_31, tmp_dict_value_31 );
    assert( !(tmp_res != 0) );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 364;
    tmp_unused = CALL_FUNCTION( tmp_called_name_47, tmp_args_name_29, tmp_kw_name_29 );
    Py_DECREF( tmp_called_name_47 );
    Py_DECREF( tmp_args_name_29 );
    Py_DECREF( tmp_kw_name_29 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 364;

        goto try_except_handler_8;
    }
    Py_DECREF( tmp_unused );
    tmp_source_name_37 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_source_name_37 == NULL ))
    {
        tmp_source_name_37 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_source_name_37 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 366;

        goto try_except_handler_8;
    }

    tmp_called_name_48 = LOOKUP_ATTRIBUTE( tmp_source_name_37, const_str_plain_register_option );
    if ( tmp_called_name_48 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 366;

        goto try_except_handler_8;
    }
    tmp_tuple_element_29 = const_str_digest_8673b45174cf95301c8e18f39b235e8c;
    tmp_args_name_30 = PyTuple_New( 3 );
    Py_INCREF( tmp_tuple_element_29 );
    PyTuple_SET_ITEM( tmp_args_name_30, 0, tmp_tuple_element_29 );
    tmp_tuple_element_29 = Py_False;
    Py_INCREF( tmp_tuple_element_29 );
    PyTuple_SET_ITEM( tmp_args_name_30, 1, tmp_tuple_element_29 );
    tmp_tuple_element_29 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_latex_multirow );

    if (unlikely( tmp_tuple_element_29 == NULL ))
    {
        tmp_tuple_element_29 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_pc_latex_multirow );
    }

    if ( tmp_tuple_element_29 == NULL )
    {
        Py_DECREF( tmp_called_name_48 );
        Py_DECREF( tmp_args_name_30 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "pc_latex_multirow" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 366;

        goto try_except_handler_8;
    }

    Py_INCREF( tmp_tuple_element_29 );
    PyTuple_SET_ITEM( tmp_args_name_30, 2, tmp_tuple_element_29 );
    tmp_dict_key_32 = const_str_plain_validator;
    tmp_dict_value_32 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_is_bool );

    if (unlikely( tmp_dict_value_32 == NULL ))
    {
        tmp_dict_value_32 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_bool );
    }

    if ( tmp_dict_value_32 == NULL )
    {
        Py_DECREF( tmp_called_name_48 );
        Py_DECREF( tmp_args_name_30 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_bool" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 367;

        goto try_except_handler_8;
    }

    tmp_kw_name_30 = _PyDict_NewPresized( 1 );
    tmp_res = PyDict_SetItem( tmp_kw_name_30, tmp_dict_key_32, tmp_dict_value_32 );
    assert( !(tmp_res != 0) );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 366;
    tmp_unused = CALL_FUNCTION( tmp_called_name_48, tmp_args_name_30, tmp_kw_name_30 );
    Py_DECREF( tmp_called_name_48 );
    Py_DECREF( tmp_args_name_30 );
    Py_DECREF( tmp_kw_name_30 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 366;

        goto try_except_handler_8;
    }
    Py_DECREF( tmp_unused );
    tmp_source_name_38 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_source_name_38 == NULL ))
    {
        tmp_source_name_38 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_source_name_38 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 368;

        goto try_except_handler_8;
    }

    tmp_called_name_49 = LOOKUP_ATTRIBUTE( tmp_source_name_38, const_str_plain_register_option );
    if ( tmp_called_name_49 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 368;

        goto try_except_handler_8;
    }
    tmp_tuple_element_30 = const_str_digest_70761738ff7ce1bdfec83dad7b784939;
    tmp_args_name_31 = PyTuple_New( 3 );
    Py_INCREF( tmp_tuple_element_30 );
    PyTuple_SET_ITEM( tmp_args_name_31, 0, tmp_tuple_element_30 );
    tmp_tuple_element_30 = Py_False;
    Py_INCREF( tmp_tuple_element_30 );
    PyTuple_SET_ITEM( tmp_args_name_31, 1, tmp_tuple_element_30 );
    tmp_tuple_element_30 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_table_schema_doc );

    if (unlikely( tmp_tuple_element_30 == NULL ))
    {
        tmp_tuple_element_30 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_pc_table_schema_doc );
    }

    if ( tmp_tuple_element_30 == NULL )
    {
        Py_DECREF( tmp_called_name_49 );
        Py_DECREF( tmp_args_name_31 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "pc_table_schema_doc" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 368;

        goto try_except_handler_8;
    }

    Py_INCREF( tmp_tuple_element_30 );
    PyTuple_SET_ITEM( tmp_args_name_31, 2, tmp_tuple_element_30 );
    tmp_dict_key_33 = const_str_plain_validator;
    tmp_dict_value_33 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_is_bool );

    if (unlikely( tmp_dict_value_33 == NULL ))
    {
        tmp_dict_value_33 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_bool );
    }

    if ( tmp_dict_value_33 == NULL )
    {
        Py_DECREF( tmp_called_name_49 );
        Py_DECREF( tmp_args_name_31 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_bool" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 369;

        goto try_except_handler_8;
    }

    tmp_kw_name_31 = _PyDict_NewPresized( 2 );
    tmp_res = PyDict_SetItem( tmp_kw_name_31, tmp_dict_key_33, tmp_dict_value_33 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_34 = const_str_plain_cb;
    tmp_dict_value_34 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_table_schema_cb );

    if (unlikely( tmp_dict_value_34 == NULL ))
    {
        tmp_dict_value_34 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_table_schema_cb );
    }

    if ( tmp_dict_value_34 == NULL )
    {
        Py_DECREF( tmp_called_name_49 );
        Py_DECREF( tmp_args_name_31 );
        Py_DECREF( tmp_kw_name_31 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "table_schema_cb" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 369;

        goto try_except_handler_8;
    }

    tmp_res = PyDict_SetItem( tmp_kw_name_31, tmp_dict_key_34, tmp_dict_value_34 );
    assert( !(tmp_res != 0) );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 368;
    tmp_unused = CALL_FUNCTION( tmp_called_name_49, tmp_args_name_31, tmp_kw_name_31 );
    Py_DECREF( tmp_called_name_49 );
    Py_DECREF( tmp_args_name_31 );
    Py_DECREF( tmp_kw_name_31 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 368;

        goto try_except_handler_8;
    }
    Py_DECREF( tmp_unused );
    tmp_source_name_39 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_source_name_39 == NULL ))
    {
        tmp_source_name_39 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_source_name_39 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 370;

        goto try_except_handler_8;
    }

    tmp_called_name_50 = LOOKUP_ATTRIBUTE( tmp_source_name_39, const_str_plain_register_option );
    if ( tmp_called_name_50 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 370;

        goto try_except_handler_8;
    }
    tmp_tuple_element_31 = const_str_digest_42083bc4d862dcb2ba3d5c3079c8b46e;
    tmp_args_name_32 = PyTuple_New( 3 );
    Py_INCREF( tmp_tuple_element_31 );
    PyTuple_SET_ITEM( tmp_args_name_32, 0, tmp_tuple_element_31 );
    tmp_tuple_element_31 = const_int_pos_1;
    Py_INCREF( tmp_tuple_element_31 );
    PyTuple_SET_ITEM( tmp_args_name_32, 1, tmp_tuple_element_31 );
    tmp_tuple_element_31 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_html_border_doc );

    if (unlikely( tmp_tuple_element_31 == NULL ))
    {
        tmp_tuple_element_31 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_pc_html_border_doc );
    }

    if ( tmp_tuple_element_31 == NULL )
    {
        Py_DECREF( tmp_called_name_50 );
        Py_DECREF( tmp_args_name_32 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "pc_html_border_doc" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 370;

        goto try_except_handler_8;
    }

    Py_INCREF( tmp_tuple_element_31 );
    PyTuple_SET_ITEM( tmp_args_name_32, 2, tmp_tuple_element_31 );
    tmp_dict_key_35 = const_str_plain_validator;
    tmp_dict_value_35 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_is_int );

    if (unlikely( tmp_dict_value_35 == NULL ))
    {
        tmp_dict_value_35 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_int );
    }

    if ( tmp_dict_value_35 == NULL )
    {
        Py_DECREF( tmp_called_name_50 );
        Py_DECREF( tmp_args_name_32 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_int" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 371;

        goto try_except_handler_8;
    }

    tmp_kw_name_32 = _PyDict_NewPresized( 1 );
    tmp_res = PyDict_SetItem( tmp_kw_name_32, tmp_dict_key_35, tmp_dict_value_35 );
    assert( !(tmp_res != 0) );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 370;
    tmp_unused = CALL_FUNCTION( tmp_called_name_50, tmp_args_name_32, tmp_kw_name_32 );
    Py_DECREF( tmp_called_name_50 );
    Py_DECREF( tmp_args_name_32 );
    Py_DECREF( tmp_kw_name_32 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 370;

        goto try_except_handler_8;
    }
    Py_DECREF( tmp_unused );
    tmp_source_name_40 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_source_name_40 == NULL ))
    {
        tmp_source_name_40 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_source_name_40 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 372;

        goto try_except_handler_8;
    }

    tmp_called_name_51 = LOOKUP_ATTRIBUTE( tmp_source_name_40, const_str_plain_register_option );
    if ( tmp_called_name_51 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 372;

        goto try_except_handler_8;
    }
    tmp_tuple_element_32 = const_str_digest_12c6d9d73b32c369db53c25a405c94a9;
    tmp_args_name_33 = PyTuple_New( 3 );
    Py_INCREF( tmp_tuple_element_32 );
    PyTuple_SET_ITEM( tmp_args_name_33, 0, tmp_tuple_element_32 );
    tmp_tuple_element_32 = Py_True;
    Py_INCREF( tmp_tuple_element_32 );
    PyTuple_SET_ITEM( tmp_args_name_33, 1, tmp_tuple_element_32 );
    tmp_tuple_element_32 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_html_use_mathjax_doc );

    if (unlikely( tmp_tuple_element_32 == NULL ))
    {
        tmp_tuple_element_32 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_pc_html_use_mathjax_doc );
    }

    if ( tmp_tuple_element_32 == NULL )
    {
        Py_DECREF( tmp_called_name_51 );
        Py_DECREF( tmp_args_name_33 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "pc_html_use_mathjax_doc" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 372;

        goto try_except_handler_8;
    }

    Py_INCREF( tmp_tuple_element_32 );
    PyTuple_SET_ITEM( tmp_args_name_33, 2, tmp_tuple_element_32 );
    tmp_dict_key_36 = const_str_plain_validator;
    tmp_dict_value_36 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_is_bool );

    if (unlikely( tmp_dict_value_36 == NULL ))
    {
        tmp_dict_value_36 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_bool );
    }

    if ( tmp_dict_value_36 == NULL )
    {
        Py_DECREF( tmp_called_name_51 );
        Py_DECREF( tmp_args_name_33 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_bool" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 373;

        goto try_except_handler_8;
    }

    tmp_kw_name_33 = _PyDict_NewPresized( 1 );
    tmp_res = PyDict_SetItem( tmp_kw_name_33, tmp_dict_key_36, tmp_dict_value_36 );
    assert( !(tmp_res != 0) );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 372;
    tmp_unused = CALL_FUNCTION( tmp_called_name_51, tmp_args_name_33, tmp_kw_name_33 );
    Py_DECREF( tmp_called_name_51 );
    Py_DECREF( tmp_args_name_33 );
    Py_DECREF( tmp_kw_name_33 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 372;

        goto try_except_handler_8;
    }
    Py_DECREF( tmp_unused );
    goto try_end_6;
    // Exception handler code:
    try_except_handler_8:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_keeper_lineno_6 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_2 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_2 );
    exception_preserved_value_2 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_2 );
    exception_preserved_tb_2 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_2 );

    if ( exception_keeper_tb_6 == NULL )
    {
        exception_keeper_tb_6 = MAKE_TRACEBACK( frame_3209efbe1fc5d2857a97a7e36ce015f8, exception_keeper_lineno_6 );
    }
    else if ( exception_keeper_lineno_6 != 0 )
    {
        exception_keeper_tb_6 = ADD_TRACEBACK( exception_keeper_tb_6, frame_3209efbe1fc5d2857a97a7e36ce015f8, exception_keeper_lineno_6 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_6, &exception_keeper_value_6, &exception_keeper_tb_6 );
    PyException_SetTraceback( exception_keeper_value_6, (PyObject *)exception_keeper_tb_6 );
    PUBLISH_EXCEPTION( &exception_keeper_type_6, &exception_keeper_value_6, &exception_keeper_tb_6 );
    // Tried code:
    tmp_compare_left_4 = EXC_TYPE(PyThreadState_GET());
    tmp_compare_right_4 = PyExc_BaseException;
    tmp_exc_match_exception_match_2 = EXCEPTION_MATCH_BOOL( tmp_compare_left_4, tmp_compare_right_4 );
    if ( tmp_exc_match_exception_match_2 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 306;

        goto try_except_handler_9;
    }
    if ( tmp_exc_match_exception_match_2 == 1 )
    {
        goto branch_yes_6;
    }
    else
    {
        goto branch_no_6;
    }
    branch_yes_6:;
    tmp_assign_source_66 = Py_False;
    {
        PyObject *old = tmp_with_2__indicator;
        assert( old != NULL );
        tmp_with_2__indicator = tmp_assign_source_66;
        Py_INCREF( tmp_with_2__indicator );
        Py_DECREF( old );
    }

    tmp_called_name_52 = tmp_with_2__exit;

    CHECK_OBJECT( tmp_called_name_52 );
    tmp_args_element_name_14 = EXC_TYPE(PyThreadState_GET());
    tmp_args_element_name_15 = EXC_VALUE(PyThreadState_GET());
    tmp_args_element_name_16 = EXC_TRACEBACK(PyThreadState_GET());
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 306;
    {
        PyObject *call_args[] = { tmp_args_element_name_14, tmp_args_element_name_15, tmp_args_element_name_16 };
        tmp_cond_value_3 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_52, call_args );
    }

    if ( tmp_cond_value_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 306;

        goto try_except_handler_9;
    }
    tmp_cond_truth_3 = CHECK_IF_TRUE( tmp_cond_value_3 );
    if ( tmp_cond_truth_3 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_cond_value_3 );

        exception_lineno = 306;

        goto try_except_handler_9;
    }
    Py_DECREF( tmp_cond_value_3 );
    if ( tmp_cond_truth_3 == 1 )
    {
        goto branch_no_7;
    }
    else
    {
        goto branch_yes_7;
    }
    branch_yes_7:;
    tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (unlikely( tmp_result == false ))
    {
        exception_lineno = 306;
    }

    if (exception_tb && exception_tb->tb_frame == &frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame) frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = exception_tb->tb_lineno;

    goto try_except_handler_9;
    branch_no_7:;
    goto branch_end_6;
    branch_no_6:;
    tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (unlikely( tmp_result == false ))
    {
        exception_lineno = 306;
    }

    if (exception_tb && exception_tb->tb_frame == &frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame) frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = exception_tb->tb_lineno;

    goto try_except_handler_9;
    branch_end_6:;
    goto try_end_7;
    // Exception handler code:
    try_except_handler_9:;
    exception_keeper_type_7 = exception_type;
    exception_keeper_value_7 = exception_value;
    exception_keeper_tb_7 = exception_tb;
    exception_keeper_lineno_7 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_2, exception_preserved_value_2, exception_preserved_tb_2 );
    // Re-raise.
    exception_type = exception_keeper_type_7;
    exception_value = exception_keeper_value_7;
    exception_tb = exception_keeper_tb_7;
    exception_lineno = exception_keeper_lineno_7;

    goto try_except_handler_7;
    // End of try:
    try_end_7:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_2, exception_preserved_value_2, exception_preserved_tb_2 );
    goto try_end_6;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$core$config_init );
    return MOD_RETURN_VALUE( NULL );
    // End of try:
    try_end_6:;
    goto try_end_8;
    // Exception handler code:
    try_except_handler_7:;
    exception_keeper_type_8 = exception_type;
    exception_keeper_value_8 = exception_value;
    exception_keeper_tb_8 = exception_tb;
    exception_keeper_lineno_8 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    tmp_compare_left_5 = tmp_with_2__indicator;

    CHECK_OBJECT( tmp_compare_left_5 );
    tmp_compare_right_5 = Py_True;
    tmp_is_3 = ( tmp_compare_left_5 == tmp_compare_right_5 );
    if ( tmp_is_3 )
    {
        goto branch_yes_8;
    }
    else
    {
        goto branch_no_8;
    }
    branch_yes_8:;
    tmp_called_name_53 = tmp_with_2__exit;

    CHECK_OBJECT( tmp_called_name_53 );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 306;
    tmp_unused = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_53, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

        Py_DECREF( exception_keeper_type_8 );
        Py_XDECREF( exception_keeper_value_8 );
        Py_XDECREF( exception_keeper_tb_8 );

        exception_lineno = 306;

        goto try_except_handler_6;
    }
    Py_DECREF( tmp_unused );
    branch_no_8:;
    // Re-raise.
    exception_type = exception_keeper_type_8;
    exception_value = exception_keeper_value_8;
    exception_tb = exception_keeper_tb_8;
    exception_lineno = exception_keeper_lineno_8;

    goto try_except_handler_6;
    // End of try:
    try_end_8:;
    tmp_compare_left_6 = tmp_with_2__indicator;

    CHECK_OBJECT( tmp_compare_left_6 );
    tmp_compare_right_6 = Py_True;
    tmp_is_4 = ( tmp_compare_left_6 == tmp_compare_right_6 );
    if ( tmp_is_4 )
    {
        goto branch_yes_9;
    }
    else
    {
        goto branch_no_9;
    }
    branch_yes_9:;
    tmp_called_name_54 = tmp_with_2__exit;

    CHECK_OBJECT( tmp_called_name_54 );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 306;
    tmp_unused = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_54, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 306;

        goto try_except_handler_6;
    }
    Py_DECREF( tmp_unused );
    branch_no_9:;
    goto try_end_9;
    // Exception handler code:
    try_except_handler_6:;
    exception_keeper_type_9 = exception_type;
    exception_keeper_value_9 = exception_value;
    exception_keeper_tb_9 = exception_tb;
    exception_keeper_lineno_9 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_with_2__source );
    tmp_with_2__source = NULL;

    Py_XDECREF( tmp_with_2__enter );
    tmp_with_2__enter = NULL;

    Py_XDECREF( tmp_with_2__exit );
    tmp_with_2__exit = NULL;

    Py_XDECREF( tmp_with_2__indicator );
    tmp_with_2__indicator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_9;
    exception_value = exception_keeper_value_9;
    exception_tb = exception_keeper_tb_9;
    exception_lineno = exception_keeper_lineno_9;

    goto frame_exception_exit_1;
    // End of try:
    try_end_9:;
    CHECK_OBJECT( (PyObject *)tmp_with_2__source );
    Py_DECREF( tmp_with_2__source );
    tmp_with_2__source = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_2__enter );
    Py_DECREF( tmp_with_2__enter );
    tmp_with_2__enter = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_2__exit );
    Py_DECREF( tmp_with_2__exit );
    tmp_with_2__exit = NULL;

    Py_XDECREF( tmp_with_2__indicator );
    tmp_with_2__indicator = NULL;

    // Tried code:
    tmp_called_instance_3 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_called_instance_3 == NULL ))
    {
        tmp_called_instance_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_called_instance_3 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 375;

        goto try_except_handler_10;
    }

    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 375;
    tmp_assign_source_67 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_config_prefix, &PyTuple_GET_ITEM( const_tuple_str_plain_html_tuple, 0 ) );

    if ( tmp_assign_source_67 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 375;

        goto try_except_handler_10;
    }
    assert( tmp_with_3__source == NULL );
    tmp_with_3__source = tmp_assign_source_67;

    tmp_source_name_41 = tmp_with_3__source;

    CHECK_OBJECT( tmp_source_name_41 );
    tmp_called_name_55 = LOOKUP_SPECIAL( tmp_source_name_41, const_str_plain___enter__ );
    if ( tmp_called_name_55 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 375;

        goto try_except_handler_10;
    }
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 375;
    tmp_assign_source_68 = CALL_FUNCTION_NO_ARGS( tmp_called_name_55 );
    Py_DECREF( tmp_called_name_55 );
    if ( tmp_assign_source_68 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 375;

        goto try_except_handler_10;
    }
    assert( tmp_with_3__enter == NULL );
    tmp_with_3__enter = tmp_assign_source_68;

    tmp_source_name_42 = tmp_with_3__source;

    CHECK_OBJECT( tmp_source_name_42 );
    tmp_assign_source_69 = LOOKUP_SPECIAL( tmp_source_name_42, const_str_plain___exit__ );
    if ( tmp_assign_source_69 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 375;

        goto try_except_handler_10;
    }
    assert( tmp_with_3__exit == NULL );
    tmp_with_3__exit = tmp_assign_source_69;

    tmp_assign_source_70 = Py_True;
    assert( tmp_with_3__indicator == NULL );
    Py_INCREF( tmp_assign_source_70 );
    tmp_with_3__indicator = tmp_assign_source_70;

    // Tried code:
    // Tried code:
    tmp_source_name_43 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_source_name_43 == NULL ))
    {
        tmp_source_name_43 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_source_name_43 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 376;

        goto try_except_handler_12;
    }

    tmp_called_name_56 = LOOKUP_ATTRIBUTE( tmp_source_name_43, const_str_plain_register_option );
    if ( tmp_called_name_56 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 376;

        goto try_except_handler_12;
    }
    tmp_tuple_element_33 = const_str_plain_border;
    tmp_args_name_34 = PyTuple_New( 3 );
    Py_INCREF( tmp_tuple_element_33 );
    PyTuple_SET_ITEM( tmp_args_name_34, 0, tmp_tuple_element_33 );
    tmp_tuple_element_33 = const_int_pos_1;
    Py_INCREF( tmp_tuple_element_33 );
    PyTuple_SET_ITEM( tmp_args_name_34, 1, tmp_tuple_element_33 );
    tmp_tuple_element_33 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_html_border_doc );

    if (unlikely( tmp_tuple_element_33 == NULL ))
    {
        tmp_tuple_element_33 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_pc_html_border_doc );
    }

    if ( tmp_tuple_element_33 == NULL )
    {
        Py_DECREF( tmp_called_name_56 );
        Py_DECREF( tmp_args_name_34 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "pc_html_border_doc" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 376;

        goto try_except_handler_12;
    }

    Py_INCREF( tmp_tuple_element_33 );
    PyTuple_SET_ITEM( tmp_args_name_34, 2, tmp_tuple_element_33 );
    tmp_dict_key_37 = const_str_plain_validator;
    tmp_dict_value_37 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_is_int );

    if (unlikely( tmp_dict_value_37 == NULL ))
    {
        tmp_dict_value_37 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_int );
    }

    if ( tmp_dict_value_37 == NULL )
    {
        Py_DECREF( tmp_called_name_56 );
        Py_DECREF( tmp_args_name_34 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_int" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 377;

        goto try_except_handler_12;
    }

    tmp_kw_name_34 = _PyDict_NewPresized( 1 );
    tmp_res = PyDict_SetItem( tmp_kw_name_34, tmp_dict_key_37, tmp_dict_value_37 );
    assert( !(tmp_res != 0) );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 376;
    tmp_unused = CALL_FUNCTION( tmp_called_name_56, tmp_args_name_34, tmp_kw_name_34 );
    Py_DECREF( tmp_called_name_56 );
    Py_DECREF( tmp_args_name_34 );
    Py_DECREF( tmp_kw_name_34 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 376;

        goto try_except_handler_12;
    }
    Py_DECREF( tmp_unused );
    goto try_end_10;
    // Exception handler code:
    try_except_handler_12:;
    exception_keeper_type_10 = exception_type;
    exception_keeper_value_10 = exception_value;
    exception_keeper_tb_10 = exception_tb;
    exception_keeper_lineno_10 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_3 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_3 );
    exception_preserved_value_3 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_3 );
    exception_preserved_tb_3 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_3 );

    if ( exception_keeper_tb_10 == NULL )
    {
        exception_keeper_tb_10 = MAKE_TRACEBACK( frame_3209efbe1fc5d2857a97a7e36ce015f8, exception_keeper_lineno_10 );
    }
    else if ( exception_keeper_lineno_10 != 0 )
    {
        exception_keeper_tb_10 = ADD_TRACEBACK( exception_keeper_tb_10, frame_3209efbe1fc5d2857a97a7e36ce015f8, exception_keeper_lineno_10 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_10, &exception_keeper_value_10, &exception_keeper_tb_10 );
    PyException_SetTraceback( exception_keeper_value_10, (PyObject *)exception_keeper_tb_10 );
    PUBLISH_EXCEPTION( &exception_keeper_type_10, &exception_keeper_value_10, &exception_keeper_tb_10 );
    // Tried code:
    tmp_compare_left_7 = EXC_TYPE(PyThreadState_GET());
    tmp_compare_right_7 = PyExc_BaseException;
    tmp_exc_match_exception_match_3 = EXCEPTION_MATCH_BOOL( tmp_compare_left_7, tmp_compare_right_7 );
    if ( tmp_exc_match_exception_match_3 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 375;

        goto try_except_handler_13;
    }
    if ( tmp_exc_match_exception_match_3 == 1 )
    {
        goto branch_yes_10;
    }
    else
    {
        goto branch_no_10;
    }
    branch_yes_10:;
    tmp_assign_source_71 = Py_False;
    {
        PyObject *old = tmp_with_3__indicator;
        assert( old != NULL );
        tmp_with_3__indicator = tmp_assign_source_71;
        Py_INCREF( tmp_with_3__indicator );
        Py_DECREF( old );
    }

    tmp_called_name_57 = tmp_with_3__exit;

    CHECK_OBJECT( tmp_called_name_57 );
    tmp_args_element_name_17 = EXC_TYPE(PyThreadState_GET());
    tmp_args_element_name_18 = EXC_VALUE(PyThreadState_GET());
    tmp_args_element_name_19 = EXC_TRACEBACK(PyThreadState_GET());
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 375;
    {
        PyObject *call_args[] = { tmp_args_element_name_17, tmp_args_element_name_18, tmp_args_element_name_19 };
        tmp_cond_value_4 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_57, call_args );
    }

    if ( tmp_cond_value_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 375;

        goto try_except_handler_13;
    }
    tmp_cond_truth_4 = CHECK_IF_TRUE( tmp_cond_value_4 );
    if ( tmp_cond_truth_4 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_cond_value_4 );

        exception_lineno = 375;

        goto try_except_handler_13;
    }
    Py_DECREF( tmp_cond_value_4 );
    if ( tmp_cond_truth_4 == 1 )
    {
        goto branch_no_11;
    }
    else
    {
        goto branch_yes_11;
    }
    branch_yes_11:;
    tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (unlikely( tmp_result == false ))
    {
        exception_lineno = 375;
    }

    if (exception_tb && exception_tb->tb_frame == &frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame) frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = exception_tb->tb_lineno;

    goto try_except_handler_13;
    branch_no_11:;
    goto branch_end_10;
    branch_no_10:;
    tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (unlikely( tmp_result == false ))
    {
        exception_lineno = 375;
    }

    if (exception_tb && exception_tb->tb_frame == &frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame) frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = exception_tb->tb_lineno;

    goto try_except_handler_13;
    branch_end_10:;
    goto try_end_11;
    // Exception handler code:
    try_except_handler_13:;
    exception_keeper_type_11 = exception_type;
    exception_keeper_value_11 = exception_value;
    exception_keeper_tb_11 = exception_tb;
    exception_keeper_lineno_11 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_3, exception_preserved_value_3, exception_preserved_tb_3 );
    // Re-raise.
    exception_type = exception_keeper_type_11;
    exception_value = exception_keeper_value_11;
    exception_tb = exception_keeper_tb_11;
    exception_lineno = exception_keeper_lineno_11;

    goto try_except_handler_11;
    // End of try:
    try_end_11:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_3, exception_preserved_value_3, exception_preserved_tb_3 );
    goto try_end_10;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$core$config_init );
    return MOD_RETURN_VALUE( NULL );
    // End of try:
    try_end_10:;
    goto try_end_12;
    // Exception handler code:
    try_except_handler_11:;
    exception_keeper_type_12 = exception_type;
    exception_keeper_value_12 = exception_value;
    exception_keeper_tb_12 = exception_tb;
    exception_keeper_lineno_12 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    tmp_compare_left_8 = tmp_with_3__indicator;

    CHECK_OBJECT( tmp_compare_left_8 );
    tmp_compare_right_8 = Py_True;
    tmp_is_5 = ( tmp_compare_left_8 == tmp_compare_right_8 );
    if ( tmp_is_5 )
    {
        goto branch_yes_12;
    }
    else
    {
        goto branch_no_12;
    }
    branch_yes_12:;
    tmp_called_name_58 = tmp_with_3__exit;

    CHECK_OBJECT( tmp_called_name_58 );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 375;
    tmp_unused = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_58, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

        Py_DECREF( exception_keeper_type_12 );
        Py_XDECREF( exception_keeper_value_12 );
        Py_XDECREF( exception_keeper_tb_12 );

        exception_lineno = 375;

        goto try_except_handler_10;
    }
    Py_DECREF( tmp_unused );
    branch_no_12:;
    // Re-raise.
    exception_type = exception_keeper_type_12;
    exception_value = exception_keeper_value_12;
    exception_tb = exception_keeper_tb_12;
    exception_lineno = exception_keeper_lineno_12;

    goto try_except_handler_10;
    // End of try:
    try_end_12:;
    tmp_compare_left_9 = tmp_with_3__indicator;

    CHECK_OBJECT( tmp_compare_left_9 );
    tmp_compare_right_9 = Py_True;
    tmp_is_6 = ( tmp_compare_left_9 == tmp_compare_right_9 );
    if ( tmp_is_6 )
    {
        goto branch_yes_13;
    }
    else
    {
        goto branch_no_13;
    }
    branch_yes_13:;
    tmp_called_name_59 = tmp_with_3__exit;

    CHECK_OBJECT( tmp_called_name_59 );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 375;
    tmp_unused = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_59, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 375;

        goto try_except_handler_10;
    }
    Py_DECREF( tmp_unused );
    branch_no_13:;
    goto try_end_13;
    // Exception handler code:
    try_except_handler_10:;
    exception_keeper_type_13 = exception_type;
    exception_keeper_value_13 = exception_value;
    exception_keeper_tb_13 = exception_tb;
    exception_keeper_lineno_13 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_with_3__source );
    tmp_with_3__source = NULL;

    Py_XDECREF( tmp_with_3__enter );
    tmp_with_3__enter = NULL;

    Py_XDECREF( tmp_with_3__exit );
    tmp_with_3__exit = NULL;

    Py_XDECREF( tmp_with_3__indicator );
    tmp_with_3__indicator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_13;
    exception_value = exception_keeper_value_13;
    exception_tb = exception_keeper_tb_13;
    exception_lineno = exception_keeper_lineno_13;

    goto frame_exception_exit_1;
    // End of try:
    try_end_13:;
    CHECK_OBJECT( (PyObject *)tmp_with_3__source );
    Py_DECREF( tmp_with_3__source );
    tmp_with_3__source = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_3__enter );
    Py_DECREF( tmp_with_3__enter );
    tmp_with_3__enter = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_3__exit );
    Py_DECREF( tmp_with_3__exit );
    tmp_with_3__exit = NULL;

    Py_XDECREF( tmp_with_3__indicator );
    tmp_with_3__indicator = NULL;

    tmp_source_name_44 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_source_name_44 == NULL ))
    {
        tmp_source_name_44 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_source_name_44 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 379;

        goto frame_exception_exit_1;
    }

    tmp_called_name_60 = LOOKUP_ATTRIBUTE( tmp_source_name_44, const_str_plain_deprecate_option );
    if ( tmp_called_name_60 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 379;

        goto frame_exception_exit_1;
    }
    tmp_args_name_35 = const_tuple_str_digest_42083bc4d862dcb2ba3d5c3079c8b46e_tuple;
    tmp_dict_key_38 = const_str_plain_msg;
    tmp_dict_value_38 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_pc_html_border_deprecation_warning );

    if (unlikely( tmp_dict_value_38 == NULL ))
    {
        tmp_dict_value_38 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_pc_html_border_deprecation_warning );
    }

    if ( tmp_dict_value_38 == NULL )
    {
        Py_DECREF( tmp_called_name_60 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "pc_html_border_deprecation_warning" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 379;

        goto frame_exception_exit_1;
    }

    tmp_kw_name_35 = _PyDict_NewPresized( 2 );
    tmp_res = PyDict_SetItem( tmp_kw_name_35, tmp_dict_key_38, tmp_dict_value_38 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_39 = const_str_plain_rkey;
    tmp_dict_value_39 = const_str_digest_6083b29174101382b93cdaa563f79a64;
    tmp_res = PyDict_SetItem( tmp_kw_name_35, tmp_dict_key_39, tmp_dict_value_39 );
    assert( !(tmp_res != 0) );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 379;
    tmp_unused = CALL_FUNCTION( tmp_called_name_60, tmp_args_name_35, tmp_kw_name_35 );
    Py_DECREF( tmp_called_name_60 );
    Py_DECREF( tmp_kw_name_35 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 379;

        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_unused );
    tmp_assign_source_72 = const_str_digest_36781382e72dac19c5e46696c90720ae;
    UPDATE_STRING_DICT0( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_tc_sim_interactive_doc, tmp_assign_source_72 );
    // Tried code:
    tmp_called_instance_4 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_called_instance_4 == NULL ))
    {
        tmp_called_instance_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_called_instance_4 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 388;

        goto try_except_handler_14;
    }

    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 388;
    tmp_assign_source_73 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_4, const_str_plain_config_prefix, &PyTuple_GET_ITEM( const_tuple_str_plain_mode_tuple, 0 ) );

    if ( tmp_assign_source_73 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 388;

        goto try_except_handler_14;
    }
    assert( tmp_with_4__source == NULL );
    tmp_with_4__source = tmp_assign_source_73;

    tmp_source_name_45 = tmp_with_4__source;

    CHECK_OBJECT( tmp_source_name_45 );
    tmp_called_name_61 = LOOKUP_SPECIAL( tmp_source_name_45, const_str_plain___enter__ );
    if ( tmp_called_name_61 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 388;

        goto try_except_handler_14;
    }
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 388;
    tmp_assign_source_74 = CALL_FUNCTION_NO_ARGS( tmp_called_name_61 );
    Py_DECREF( tmp_called_name_61 );
    if ( tmp_assign_source_74 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 388;

        goto try_except_handler_14;
    }
    assert( tmp_with_4__enter == NULL );
    tmp_with_4__enter = tmp_assign_source_74;

    tmp_source_name_46 = tmp_with_4__source;

    CHECK_OBJECT( tmp_source_name_46 );
    tmp_assign_source_75 = LOOKUP_SPECIAL( tmp_source_name_46, const_str_plain___exit__ );
    if ( tmp_assign_source_75 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 388;

        goto try_except_handler_14;
    }
    assert( tmp_with_4__exit == NULL );
    tmp_with_4__exit = tmp_assign_source_75;

    tmp_assign_source_76 = Py_True;
    assert( tmp_with_4__indicator == NULL );
    Py_INCREF( tmp_assign_source_76 );
    tmp_with_4__indicator = tmp_assign_source_76;

    // Tried code:
    // Tried code:
    tmp_source_name_47 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_source_name_47 == NULL ))
    {
        tmp_source_name_47 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_source_name_47 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 389;

        goto try_except_handler_16;
    }

    tmp_called_name_62 = LOOKUP_ATTRIBUTE( tmp_source_name_47, const_str_plain_register_option );
    if ( tmp_called_name_62 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 389;

        goto try_except_handler_16;
    }
    tmp_args_element_name_20 = const_str_plain_sim_interactive;
    tmp_args_element_name_21 = Py_False;
    tmp_args_element_name_22 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_tc_sim_interactive_doc );

    if (unlikely( tmp_args_element_name_22 == NULL ))
    {
        tmp_args_element_name_22 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_tc_sim_interactive_doc );
    }

    if ( tmp_args_element_name_22 == NULL )
    {
        Py_DECREF( tmp_called_name_62 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "tc_sim_interactive_doc" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 389;

        goto try_except_handler_16;
    }

    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 389;
    {
        PyObject *call_args[] = { tmp_args_element_name_20, tmp_args_element_name_21, tmp_args_element_name_22 };
        tmp_unused = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_62, call_args );
    }

    Py_DECREF( tmp_called_name_62 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 389;

        goto try_except_handler_16;
    }
    Py_DECREF( tmp_unused );
    goto try_end_14;
    // Exception handler code:
    try_except_handler_16:;
    exception_keeper_type_14 = exception_type;
    exception_keeper_value_14 = exception_value;
    exception_keeper_tb_14 = exception_tb;
    exception_keeper_lineno_14 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_4 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_4 );
    exception_preserved_value_4 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_4 );
    exception_preserved_tb_4 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_4 );

    if ( exception_keeper_tb_14 == NULL )
    {
        exception_keeper_tb_14 = MAKE_TRACEBACK( frame_3209efbe1fc5d2857a97a7e36ce015f8, exception_keeper_lineno_14 );
    }
    else if ( exception_keeper_lineno_14 != 0 )
    {
        exception_keeper_tb_14 = ADD_TRACEBACK( exception_keeper_tb_14, frame_3209efbe1fc5d2857a97a7e36ce015f8, exception_keeper_lineno_14 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_14, &exception_keeper_value_14, &exception_keeper_tb_14 );
    PyException_SetTraceback( exception_keeper_value_14, (PyObject *)exception_keeper_tb_14 );
    PUBLISH_EXCEPTION( &exception_keeper_type_14, &exception_keeper_value_14, &exception_keeper_tb_14 );
    // Tried code:
    tmp_compare_left_10 = EXC_TYPE(PyThreadState_GET());
    tmp_compare_right_10 = PyExc_BaseException;
    tmp_exc_match_exception_match_4 = EXCEPTION_MATCH_BOOL( tmp_compare_left_10, tmp_compare_right_10 );
    if ( tmp_exc_match_exception_match_4 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 388;

        goto try_except_handler_17;
    }
    if ( tmp_exc_match_exception_match_4 == 1 )
    {
        goto branch_yes_14;
    }
    else
    {
        goto branch_no_14;
    }
    branch_yes_14:;
    tmp_assign_source_77 = Py_False;
    {
        PyObject *old = tmp_with_4__indicator;
        assert( old != NULL );
        tmp_with_4__indicator = tmp_assign_source_77;
        Py_INCREF( tmp_with_4__indicator );
        Py_DECREF( old );
    }

    tmp_called_name_63 = tmp_with_4__exit;

    CHECK_OBJECT( tmp_called_name_63 );
    tmp_args_element_name_23 = EXC_TYPE(PyThreadState_GET());
    tmp_args_element_name_24 = EXC_VALUE(PyThreadState_GET());
    tmp_args_element_name_25 = EXC_TRACEBACK(PyThreadState_GET());
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 388;
    {
        PyObject *call_args[] = { tmp_args_element_name_23, tmp_args_element_name_24, tmp_args_element_name_25 };
        tmp_cond_value_5 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_63, call_args );
    }

    if ( tmp_cond_value_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 388;

        goto try_except_handler_17;
    }
    tmp_cond_truth_5 = CHECK_IF_TRUE( tmp_cond_value_5 );
    if ( tmp_cond_truth_5 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_cond_value_5 );

        exception_lineno = 388;

        goto try_except_handler_17;
    }
    Py_DECREF( tmp_cond_value_5 );
    if ( tmp_cond_truth_5 == 1 )
    {
        goto branch_no_15;
    }
    else
    {
        goto branch_yes_15;
    }
    branch_yes_15:;
    tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (unlikely( tmp_result == false ))
    {
        exception_lineno = 388;
    }

    if (exception_tb && exception_tb->tb_frame == &frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame) frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = exception_tb->tb_lineno;

    goto try_except_handler_17;
    branch_no_15:;
    goto branch_end_14;
    branch_no_14:;
    tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (unlikely( tmp_result == false ))
    {
        exception_lineno = 388;
    }

    if (exception_tb && exception_tb->tb_frame == &frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame) frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = exception_tb->tb_lineno;

    goto try_except_handler_17;
    branch_end_14:;
    goto try_end_15;
    // Exception handler code:
    try_except_handler_17:;
    exception_keeper_type_15 = exception_type;
    exception_keeper_value_15 = exception_value;
    exception_keeper_tb_15 = exception_tb;
    exception_keeper_lineno_15 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_4, exception_preserved_value_4, exception_preserved_tb_4 );
    // Re-raise.
    exception_type = exception_keeper_type_15;
    exception_value = exception_keeper_value_15;
    exception_tb = exception_keeper_tb_15;
    exception_lineno = exception_keeper_lineno_15;

    goto try_except_handler_15;
    // End of try:
    try_end_15:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_4, exception_preserved_value_4, exception_preserved_tb_4 );
    goto try_end_14;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$core$config_init );
    return MOD_RETURN_VALUE( NULL );
    // End of try:
    try_end_14:;
    goto try_end_16;
    // Exception handler code:
    try_except_handler_15:;
    exception_keeper_type_16 = exception_type;
    exception_keeper_value_16 = exception_value;
    exception_keeper_tb_16 = exception_tb;
    exception_keeper_lineno_16 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    tmp_compare_left_11 = tmp_with_4__indicator;

    CHECK_OBJECT( tmp_compare_left_11 );
    tmp_compare_right_11 = Py_True;
    tmp_is_7 = ( tmp_compare_left_11 == tmp_compare_right_11 );
    if ( tmp_is_7 )
    {
        goto branch_yes_16;
    }
    else
    {
        goto branch_no_16;
    }
    branch_yes_16:;
    tmp_called_name_64 = tmp_with_4__exit;

    CHECK_OBJECT( tmp_called_name_64 );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 388;
    tmp_unused = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_64, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

        Py_DECREF( exception_keeper_type_16 );
        Py_XDECREF( exception_keeper_value_16 );
        Py_XDECREF( exception_keeper_tb_16 );

        exception_lineno = 388;

        goto try_except_handler_14;
    }
    Py_DECREF( tmp_unused );
    branch_no_16:;
    // Re-raise.
    exception_type = exception_keeper_type_16;
    exception_value = exception_keeper_value_16;
    exception_tb = exception_keeper_tb_16;
    exception_lineno = exception_keeper_lineno_16;

    goto try_except_handler_14;
    // End of try:
    try_end_16:;
    tmp_compare_left_12 = tmp_with_4__indicator;

    CHECK_OBJECT( tmp_compare_left_12 );
    tmp_compare_right_12 = Py_True;
    tmp_is_8 = ( tmp_compare_left_12 == tmp_compare_right_12 );
    if ( tmp_is_8 )
    {
        goto branch_yes_17;
    }
    else
    {
        goto branch_no_17;
    }
    branch_yes_17:;
    tmp_called_name_65 = tmp_with_4__exit;

    CHECK_OBJECT( tmp_called_name_65 );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 388;
    tmp_unused = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_65, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 388;

        goto try_except_handler_14;
    }
    Py_DECREF( tmp_unused );
    branch_no_17:;
    goto try_end_17;
    // Exception handler code:
    try_except_handler_14:;
    exception_keeper_type_17 = exception_type;
    exception_keeper_value_17 = exception_value;
    exception_keeper_tb_17 = exception_tb;
    exception_keeper_lineno_17 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_with_4__source );
    tmp_with_4__source = NULL;

    Py_XDECREF( tmp_with_4__enter );
    tmp_with_4__enter = NULL;

    Py_XDECREF( tmp_with_4__exit );
    tmp_with_4__exit = NULL;

    Py_XDECREF( tmp_with_4__indicator );
    tmp_with_4__indicator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_17;
    exception_value = exception_keeper_value_17;
    exception_tb = exception_keeper_tb_17;
    exception_lineno = exception_keeper_lineno_17;

    goto frame_exception_exit_1;
    // End of try:
    try_end_17:;
    CHECK_OBJECT( (PyObject *)tmp_with_4__source );
    Py_DECREF( tmp_with_4__source );
    tmp_with_4__source = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_4__enter );
    Py_DECREF( tmp_with_4__enter );
    tmp_with_4__enter = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_4__exit );
    Py_DECREF( tmp_with_4__exit );
    tmp_with_4__exit = NULL;

    Py_XDECREF( tmp_with_4__indicator );
    tmp_with_4__indicator = NULL;

    tmp_assign_source_78 = const_str_digest_8af9049bb103d0ae35d21227a8a5a762;
    UPDATE_STRING_DICT0( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_use_inf_as_null_doc, tmp_assign_source_78 );
    tmp_assign_source_79 = const_str_digest_3fed32ca54d55e7419f2cf87ece4440b;
    UPDATE_STRING_DICT0( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_use_inf_as_na_doc, tmp_assign_source_79 );
    tmp_assign_source_80 = MAKE_FUNCTION_pandas$core$config_init$$$function_4_use_inf_as_na_cb(  );
    UPDATE_STRING_DICT1( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_use_inf_as_na_cb, tmp_assign_source_80 );
    // Tried code:
    tmp_called_instance_5 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_called_instance_5 == NULL ))
    {
        tmp_called_instance_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_called_instance_5 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 413;

        goto try_except_handler_18;
    }

    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 413;
    tmp_assign_source_81 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_5, const_str_plain_config_prefix, &PyTuple_GET_ITEM( const_tuple_str_plain_mode_tuple, 0 ) );

    if ( tmp_assign_source_81 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 413;

        goto try_except_handler_18;
    }
    assert( tmp_with_5__source == NULL );
    tmp_with_5__source = tmp_assign_source_81;

    tmp_source_name_48 = tmp_with_5__source;

    CHECK_OBJECT( tmp_source_name_48 );
    tmp_called_name_66 = LOOKUP_SPECIAL( tmp_source_name_48, const_str_plain___enter__ );
    if ( tmp_called_name_66 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 413;

        goto try_except_handler_18;
    }
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 413;
    tmp_assign_source_82 = CALL_FUNCTION_NO_ARGS( tmp_called_name_66 );
    Py_DECREF( tmp_called_name_66 );
    if ( tmp_assign_source_82 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 413;

        goto try_except_handler_18;
    }
    assert( tmp_with_5__enter == NULL );
    tmp_with_5__enter = tmp_assign_source_82;

    tmp_source_name_49 = tmp_with_5__source;

    CHECK_OBJECT( tmp_source_name_49 );
    tmp_assign_source_83 = LOOKUP_SPECIAL( tmp_source_name_49, const_str_plain___exit__ );
    if ( tmp_assign_source_83 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 413;

        goto try_except_handler_18;
    }
    assert( tmp_with_5__exit == NULL );
    tmp_with_5__exit = tmp_assign_source_83;

    tmp_assign_source_84 = Py_True;
    assert( tmp_with_5__indicator == NULL );
    Py_INCREF( tmp_assign_source_84 );
    tmp_with_5__indicator = tmp_assign_source_84;

    // Tried code:
    // Tried code:
    tmp_source_name_50 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_source_name_50 == NULL ))
    {
        tmp_source_name_50 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_source_name_50 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 414;

        goto try_except_handler_20;
    }

    tmp_called_name_67 = LOOKUP_ATTRIBUTE( tmp_source_name_50, const_str_plain_register_option );
    if ( tmp_called_name_67 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 414;

        goto try_except_handler_20;
    }
    tmp_tuple_element_34 = const_str_plain_use_inf_as_na;
    tmp_args_name_36 = PyTuple_New( 3 );
    Py_INCREF( tmp_tuple_element_34 );
    PyTuple_SET_ITEM( tmp_args_name_36, 0, tmp_tuple_element_34 );
    tmp_tuple_element_34 = Py_False;
    Py_INCREF( tmp_tuple_element_34 );
    PyTuple_SET_ITEM( tmp_args_name_36, 1, tmp_tuple_element_34 );
    tmp_tuple_element_34 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_use_inf_as_na_doc );

    if (unlikely( tmp_tuple_element_34 == NULL ))
    {
        tmp_tuple_element_34 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_use_inf_as_na_doc );
    }

    if ( tmp_tuple_element_34 == NULL )
    {
        Py_DECREF( tmp_called_name_67 );
        Py_DECREF( tmp_args_name_36 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "use_inf_as_na_doc" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 414;

        goto try_except_handler_20;
    }

    Py_INCREF( tmp_tuple_element_34 );
    PyTuple_SET_ITEM( tmp_args_name_36, 2, tmp_tuple_element_34 );
    tmp_dict_key_40 = const_str_plain_cb;
    tmp_dict_value_40 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_use_inf_as_na_cb );

    if (unlikely( tmp_dict_value_40 == NULL ))
    {
        tmp_dict_value_40 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_use_inf_as_na_cb );
    }

    if ( tmp_dict_value_40 == NULL )
    {
        Py_DECREF( tmp_called_name_67 );
        Py_DECREF( tmp_args_name_36 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "use_inf_as_na_cb" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 415;

        goto try_except_handler_20;
    }

    tmp_kw_name_36 = _PyDict_NewPresized( 1 );
    tmp_res = PyDict_SetItem( tmp_kw_name_36, tmp_dict_key_40, tmp_dict_value_40 );
    assert( !(tmp_res != 0) );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 414;
    tmp_unused = CALL_FUNCTION( tmp_called_name_67, tmp_args_name_36, tmp_kw_name_36 );
    Py_DECREF( tmp_called_name_67 );
    Py_DECREF( tmp_args_name_36 );
    Py_DECREF( tmp_kw_name_36 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 414;

        goto try_except_handler_20;
    }
    Py_DECREF( tmp_unused );
    tmp_source_name_51 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_source_name_51 == NULL ))
    {
        tmp_source_name_51 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_source_name_51 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 416;

        goto try_except_handler_20;
    }

    tmp_called_name_68 = LOOKUP_ATTRIBUTE( tmp_source_name_51, const_str_plain_register_option );
    if ( tmp_called_name_68 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 416;

        goto try_except_handler_20;
    }
    tmp_tuple_element_35 = const_str_plain_use_inf_as_null;
    tmp_args_name_37 = PyTuple_New( 3 );
    Py_INCREF( tmp_tuple_element_35 );
    PyTuple_SET_ITEM( tmp_args_name_37, 0, tmp_tuple_element_35 );
    tmp_tuple_element_35 = Py_False;
    Py_INCREF( tmp_tuple_element_35 );
    PyTuple_SET_ITEM( tmp_args_name_37, 1, tmp_tuple_element_35 );
    tmp_tuple_element_35 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_use_inf_as_null_doc );

    if (unlikely( tmp_tuple_element_35 == NULL ))
    {
        tmp_tuple_element_35 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_use_inf_as_null_doc );
    }

    if ( tmp_tuple_element_35 == NULL )
    {
        Py_DECREF( tmp_called_name_68 );
        Py_DECREF( tmp_args_name_37 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "use_inf_as_null_doc" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 416;

        goto try_except_handler_20;
    }

    Py_INCREF( tmp_tuple_element_35 );
    PyTuple_SET_ITEM( tmp_args_name_37, 2, tmp_tuple_element_35 );
    tmp_dict_key_41 = const_str_plain_cb;
    tmp_dict_value_41 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_use_inf_as_na_cb );

    if (unlikely( tmp_dict_value_41 == NULL ))
    {
        tmp_dict_value_41 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_use_inf_as_na_cb );
    }

    if ( tmp_dict_value_41 == NULL )
    {
        Py_DECREF( tmp_called_name_68 );
        Py_DECREF( tmp_args_name_37 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "use_inf_as_na_cb" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 417;

        goto try_except_handler_20;
    }

    tmp_kw_name_37 = _PyDict_NewPresized( 1 );
    tmp_res = PyDict_SetItem( tmp_kw_name_37, tmp_dict_key_41, tmp_dict_value_41 );
    assert( !(tmp_res != 0) );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 416;
    tmp_unused = CALL_FUNCTION( tmp_called_name_68, tmp_args_name_37, tmp_kw_name_37 );
    Py_DECREF( tmp_called_name_68 );
    Py_DECREF( tmp_args_name_37 );
    Py_DECREF( tmp_kw_name_37 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 416;

        goto try_except_handler_20;
    }
    Py_DECREF( tmp_unused );
    goto try_end_18;
    // Exception handler code:
    try_except_handler_20:;
    exception_keeper_type_18 = exception_type;
    exception_keeper_value_18 = exception_value;
    exception_keeper_tb_18 = exception_tb;
    exception_keeper_lineno_18 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_5 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_5 );
    exception_preserved_value_5 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_5 );
    exception_preserved_tb_5 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_5 );

    if ( exception_keeper_tb_18 == NULL )
    {
        exception_keeper_tb_18 = MAKE_TRACEBACK( frame_3209efbe1fc5d2857a97a7e36ce015f8, exception_keeper_lineno_18 );
    }
    else if ( exception_keeper_lineno_18 != 0 )
    {
        exception_keeper_tb_18 = ADD_TRACEBACK( exception_keeper_tb_18, frame_3209efbe1fc5d2857a97a7e36ce015f8, exception_keeper_lineno_18 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_18, &exception_keeper_value_18, &exception_keeper_tb_18 );
    PyException_SetTraceback( exception_keeper_value_18, (PyObject *)exception_keeper_tb_18 );
    PUBLISH_EXCEPTION( &exception_keeper_type_18, &exception_keeper_value_18, &exception_keeper_tb_18 );
    // Tried code:
    tmp_compare_left_13 = EXC_TYPE(PyThreadState_GET());
    tmp_compare_right_13 = PyExc_BaseException;
    tmp_exc_match_exception_match_5 = EXCEPTION_MATCH_BOOL( tmp_compare_left_13, tmp_compare_right_13 );
    if ( tmp_exc_match_exception_match_5 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 413;

        goto try_except_handler_21;
    }
    if ( tmp_exc_match_exception_match_5 == 1 )
    {
        goto branch_yes_18;
    }
    else
    {
        goto branch_no_18;
    }
    branch_yes_18:;
    tmp_assign_source_85 = Py_False;
    {
        PyObject *old = tmp_with_5__indicator;
        assert( old != NULL );
        tmp_with_5__indicator = tmp_assign_source_85;
        Py_INCREF( tmp_with_5__indicator );
        Py_DECREF( old );
    }

    tmp_called_name_69 = tmp_with_5__exit;

    CHECK_OBJECT( tmp_called_name_69 );
    tmp_args_element_name_26 = EXC_TYPE(PyThreadState_GET());
    tmp_args_element_name_27 = EXC_VALUE(PyThreadState_GET());
    tmp_args_element_name_28 = EXC_TRACEBACK(PyThreadState_GET());
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 413;
    {
        PyObject *call_args[] = { tmp_args_element_name_26, tmp_args_element_name_27, tmp_args_element_name_28 };
        tmp_cond_value_6 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_69, call_args );
    }

    if ( tmp_cond_value_6 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 413;

        goto try_except_handler_21;
    }
    tmp_cond_truth_6 = CHECK_IF_TRUE( tmp_cond_value_6 );
    if ( tmp_cond_truth_6 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_cond_value_6 );

        exception_lineno = 413;

        goto try_except_handler_21;
    }
    Py_DECREF( tmp_cond_value_6 );
    if ( tmp_cond_truth_6 == 1 )
    {
        goto branch_no_19;
    }
    else
    {
        goto branch_yes_19;
    }
    branch_yes_19:;
    tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (unlikely( tmp_result == false ))
    {
        exception_lineno = 413;
    }

    if (exception_tb && exception_tb->tb_frame == &frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame) frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = exception_tb->tb_lineno;

    goto try_except_handler_21;
    branch_no_19:;
    goto branch_end_18;
    branch_no_18:;
    tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (unlikely( tmp_result == false ))
    {
        exception_lineno = 413;
    }

    if (exception_tb && exception_tb->tb_frame == &frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame) frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = exception_tb->tb_lineno;

    goto try_except_handler_21;
    branch_end_18:;
    goto try_end_19;
    // Exception handler code:
    try_except_handler_21:;
    exception_keeper_type_19 = exception_type;
    exception_keeper_value_19 = exception_value;
    exception_keeper_tb_19 = exception_tb;
    exception_keeper_lineno_19 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_5, exception_preserved_value_5, exception_preserved_tb_5 );
    // Re-raise.
    exception_type = exception_keeper_type_19;
    exception_value = exception_keeper_value_19;
    exception_tb = exception_keeper_tb_19;
    exception_lineno = exception_keeper_lineno_19;

    goto try_except_handler_19;
    // End of try:
    try_end_19:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_5, exception_preserved_value_5, exception_preserved_tb_5 );
    goto try_end_18;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$core$config_init );
    return MOD_RETURN_VALUE( NULL );
    // End of try:
    try_end_18:;
    goto try_end_20;
    // Exception handler code:
    try_except_handler_19:;
    exception_keeper_type_20 = exception_type;
    exception_keeper_value_20 = exception_value;
    exception_keeper_tb_20 = exception_tb;
    exception_keeper_lineno_20 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    tmp_compare_left_14 = tmp_with_5__indicator;

    CHECK_OBJECT( tmp_compare_left_14 );
    tmp_compare_right_14 = Py_True;
    tmp_is_9 = ( tmp_compare_left_14 == tmp_compare_right_14 );
    if ( tmp_is_9 )
    {
        goto branch_yes_20;
    }
    else
    {
        goto branch_no_20;
    }
    branch_yes_20:;
    tmp_called_name_70 = tmp_with_5__exit;

    CHECK_OBJECT( tmp_called_name_70 );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 413;
    tmp_unused = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_70, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

        Py_DECREF( exception_keeper_type_20 );
        Py_XDECREF( exception_keeper_value_20 );
        Py_XDECREF( exception_keeper_tb_20 );

        exception_lineno = 413;

        goto try_except_handler_18;
    }
    Py_DECREF( tmp_unused );
    branch_no_20:;
    // Re-raise.
    exception_type = exception_keeper_type_20;
    exception_value = exception_keeper_value_20;
    exception_tb = exception_keeper_tb_20;
    exception_lineno = exception_keeper_lineno_20;

    goto try_except_handler_18;
    // End of try:
    try_end_20:;
    tmp_compare_left_15 = tmp_with_5__indicator;

    CHECK_OBJECT( tmp_compare_left_15 );
    tmp_compare_right_15 = Py_True;
    tmp_is_10 = ( tmp_compare_left_15 == tmp_compare_right_15 );
    if ( tmp_is_10 )
    {
        goto branch_yes_21;
    }
    else
    {
        goto branch_no_21;
    }
    branch_yes_21:;
    tmp_called_name_71 = tmp_with_5__exit;

    CHECK_OBJECT( tmp_called_name_71 );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 413;
    tmp_unused = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_71, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 413;

        goto try_except_handler_18;
    }
    Py_DECREF( tmp_unused );
    branch_no_21:;
    goto try_end_21;
    // Exception handler code:
    try_except_handler_18:;
    exception_keeper_type_21 = exception_type;
    exception_keeper_value_21 = exception_value;
    exception_keeper_tb_21 = exception_tb;
    exception_keeper_lineno_21 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_with_5__source );
    tmp_with_5__source = NULL;

    Py_XDECREF( tmp_with_5__enter );
    tmp_with_5__enter = NULL;

    Py_XDECREF( tmp_with_5__exit );
    tmp_with_5__exit = NULL;

    Py_XDECREF( tmp_with_5__indicator );
    tmp_with_5__indicator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_21;
    exception_value = exception_keeper_value_21;
    exception_tb = exception_keeper_tb_21;
    exception_lineno = exception_keeper_lineno_21;

    goto frame_exception_exit_1;
    // End of try:
    try_end_21:;
    CHECK_OBJECT( (PyObject *)tmp_with_5__source );
    Py_DECREF( tmp_with_5__source );
    tmp_with_5__source = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_5__enter );
    Py_DECREF( tmp_with_5__enter );
    tmp_with_5__enter = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_5__exit );
    Py_DECREF( tmp_with_5__exit );
    tmp_with_5__exit = NULL;

    Py_XDECREF( tmp_with_5__indicator );
    tmp_with_5__indicator = NULL;

    tmp_source_name_52 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_source_name_52 == NULL ))
    {
        tmp_source_name_52 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_source_name_52 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 419;

        goto frame_exception_exit_1;
    }

    tmp_called_name_72 = LOOKUP_ATTRIBUTE( tmp_source_name_52, const_str_plain_deprecate_option );
    if ( tmp_called_name_72 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 419;

        goto frame_exception_exit_1;
    }
    tmp_args_name_38 = const_tuple_str_digest_35b0c6e3e7281501288a4ab5b486f5d6_tuple;
    tmp_dict_key_42 = const_str_plain_msg;
    tmp_dict_value_42 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_use_inf_as_null_doc );

    if (unlikely( tmp_dict_value_42 == NULL ))
    {
        tmp_dict_value_42 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_use_inf_as_null_doc );
    }

    if ( tmp_dict_value_42 == NULL )
    {
        Py_DECREF( tmp_called_name_72 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "use_inf_as_null_doc" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 419;

        goto frame_exception_exit_1;
    }

    tmp_kw_name_38 = _PyDict_NewPresized( 2 );
    tmp_res = PyDict_SetItem( tmp_kw_name_38, tmp_dict_key_42, tmp_dict_value_42 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_43 = const_str_plain_rkey;
    tmp_dict_value_43 = const_str_digest_8b6677d435ef234e5391d3dcc8cf1b40;
    tmp_res = PyDict_SetItem( tmp_kw_name_38, tmp_dict_key_43, tmp_dict_value_43 );
    assert( !(tmp_res != 0) );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 419;
    tmp_unused = CALL_FUNCTION( tmp_called_name_72, tmp_args_name_38, tmp_kw_name_38 );
    Py_DECREF( tmp_called_name_72 );
    Py_DECREF( tmp_kw_name_38 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 419;

        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_unused );
    tmp_assign_source_86 = const_str_digest_baeb24505d73a9f85f64f36d492f470b;
    UPDATE_STRING_DICT0( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_chained_assignment, tmp_assign_source_86 );
    // Tried code:
    tmp_called_instance_6 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_called_instance_6 == NULL ))
    {
        tmp_called_instance_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_called_instance_6 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 430;

        goto try_except_handler_22;
    }

    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 430;
    tmp_assign_source_87 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_6, const_str_plain_config_prefix, &PyTuple_GET_ITEM( const_tuple_str_plain_mode_tuple, 0 ) );

    if ( tmp_assign_source_87 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 430;

        goto try_except_handler_22;
    }
    assert( tmp_with_6__source == NULL );
    tmp_with_6__source = tmp_assign_source_87;

    tmp_source_name_53 = tmp_with_6__source;

    CHECK_OBJECT( tmp_source_name_53 );
    tmp_called_name_73 = LOOKUP_SPECIAL( tmp_source_name_53, const_str_plain___enter__ );
    if ( tmp_called_name_73 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 430;

        goto try_except_handler_22;
    }
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 430;
    tmp_assign_source_88 = CALL_FUNCTION_NO_ARGS( tmp_called_name_73 );
    Py_DECREF( tmp_called_name_73 );
    if ( tmp_assign_source_88 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 430;

        goto try_except_handler_22;
    }
    assert( tmp_with_6__enter == NULL );
    tmp_with_6__enter = tmp_assign_source_88;

    tmp_source_name_54 = tmp_with_6__source;

    CHECK_OBJECT( tmp_source_name_54 );
    tmp_assign_source_89 = LOOKUP_SPECIAL( tmp_source_name_54, const_str_plain___exit__ );
    if ( tmp_assign_source_89 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 430;

        goto try_except_handler_22;
    }
    assert( tmp_with_6__exit == NULL );
    tmp_with_6__exit = tmp_assign_source_89;

    tmp_assign_source_90 = Py_True;
    assert( tmp_with_6__indicator == NULL );
    Py_INCREF( tmp_assign_source_90 );
    tmp_with_6__indicator = tmp_assign_source_90;

    // Tried code:
    // Tried code:
    tmp_source_name_55 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_source_name_55 == NULL ))
    {
        tmp_source_name_55 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_source_name_55 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 431;

        goto try_except_handler_24;
    }

    tmp_called_name_74 = LOOKUP_ATTRIBUTE( tmp_source_name_55, const_str_plain_register_option );
    if ( tmp_called_name_74 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 431;

        goto try_except_handler_24;
    }
    tmp_tuple_element_36 = const_str_plain_chained_assignment;
    tmp_args_name_39 = PyTuple_New( 3 );
    Py_INCREF( tmp_tuple_element_36 );
    PyTuple_SET_ITEM( tmp_args_name_39, 0, tmp_tuple_element_36 );
    tmp_tuple_element_36 = const_str_plain_warn;
    Py_INCREF( tmp_tuple_element_36 );
    PyTuple_SET_ITEM( tmp_args_name_39, 1, tmp_tuple_element_36 );
    tmp_tuple_element_36 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_chained_assignment );

    if (unlikely( tmp_tuple_element_36 == NULL ))
    {
        tmp_tuple_element_36 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_chained_assignment );
    }

    if ( tmp_tuple_element_36 == NULL )
    {
        Py_DECREF( tmp_called_name_74 );
        Py_DECREF( tmp_args_name_39 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "chained_assignment" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 431;

        goto try_except_handler_24;
    }

    Py_INCREF( tmp_tuple_element_36 );
    PyTuple_SET_ITEM( tmp_args_name_39, 2, tmp_tuple_element_36 );
    tmp_dict_key_44 = const_str_plain_validator;
    tmp_called_name_75 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_is_one_of_factory );

    if (unlikely( tmp_called_name_75 == NULL ))
    {
        tmp_called_name_75 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_one_of_factory );
    }

    if ( tmp_called_name_75 == NULL )
    {
        Py_DECREF( tmp_called_name_74 );
        Py_DECREF( tmp_args_name_39 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_one_of_factory" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 432;

        goto try_except_handler_24;
    }

    tmp_call_arg_element_7 = LIST_COPY( const_list_none_str_plain_warn_str_plain_raise_list );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 432;
    {
        PyObject *call_args[] = { tmp_call_arg_element_7 };
        tmp_dict_value_44 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_75, call_args );
    }

    Py_DECREF( tmp_call_arg_element_7 );
    if ( tmp_dict_value_44 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_74 );
        Py_DECREF( tmp_args_name_39 );

        exception_lineno = 432;

        goto try_except_handler_24;
    }
    tmp_kw_name_39 = _PyDict_NewPresized( 1 );
    tmp_res = PyDict_SetItem( tmp_kw_name_39, tmp_dict_key_44, tmp_dict_value_44 );
    Py_DECREF( tmp_dict_value_44 );
    assert( !(tmp_res != 0) );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 431;
    tmp_unused = CALL_FUNCTION( tmp_called_name_74, tmp_args_name_39, tmp_kw_name_39 );
    Py_DECREF( tmp_called_name_74 );
    Py_DECREF( tmp_args_name_39 );
    Py_DECREF( tmp_kw_name_39 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 431;

        goto try_except_handler_24;
    }
    Py_DECREF( tmp_unused );
    goto try_end_22;
    // Exception handler code:
    try_except_handler_24:;
    exception_keeper_type_22 = exception_type;
    exception_keeper_value_22 = exception_value;
    exception_keeper_tb_22 = exception_tb;
    exception_keeper_lineno_22 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_6 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_6 );
    exception_preserved_value_6 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_6 );
    exception_preserved_tb_6 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_6 );

    if ( exception_keeper_tb_22 == NULL )
    {
        exception_keeper_tb_22 = MAKE_TRACEBACK( frame_3209efbe1fc5d2857a97a7e36ce015f8, exception_keeper_lineno_22 );
    }
    else if ( exception_keeper_lineno_22 != 0 )
    {
        exception_keeper_tb_22 = ADD_TRACEBACK( exception_keeper_tb_22, frame_3209efbe1fc5d2857a97a7e36ce015f8, exception_keeper_lineno_22 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_22, &exception_keeper_value_22, &exception_keeper_tb_22 );
    PyException_SetTraceback( exception_keeper_value_22, (PyObject *)exception_keeper_tb_22 );
    PUBLISH_EXCEPTION( &exception_keeper_type_22, &exception_keeper_value_22, &exception_keeper_tb_22 );
    // Tried code:
    tmp_compare_left_16 = EXC_TYPE(PyThreadState_GET());
    tmp_compare_right_16 = PyExc_BaseException;
    tmp_exc_match_exception_match_6 = EXCEPTION_MATCH_BOOL( tmp_compare_left_16, tmp_compare_right_16 );
    if ( tmp_exc_match_exception_match_6 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 430;

        goto try_except_handler_25;
    }
    if ( tmp_exc_match_exception_match_6 == 1 )
    {
        goto branch_yes_22;
    }
    else
    {
        goto branch_no_22;
    }
    branch_yes_22:;
    tmp_assign_source_91 = Py_False;
    {
        PyObject *old = tmp_with_6__indicator;
        assert( old != NULL );
        tmp_with_6__indicator = tmp_assign_source_91;
        Py_INCREF( tmp_with_6__indicator );
        Py_DECREF( old );
    }

    tmp_called_name_76 = tmp_with_6__exit;

    CHECK_OBJECT( tmp_called_name_76 );
    tmp_args_element_name_29 = EXC_TYPE(PyThreadState_GET());
    tmp_args_element_name_30 = EXC_VALUE(PyThreadState_GET());
    tmp_args_element_name_31 = EXC_TRACEBACK(PyThreadState_GET());
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 430;
    {
        PyObject *call_args[] = { tmp_args_element_name_29, tmp_args_element_name_30, tmp_args_element_name_31 };
        tmp_cond_value_7 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_76, call_args );
    }

    if ( tmp_cond_value_7 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 430;

        goto try_except_handler_25;
    }
    tmp_cond_truth_7 = CHECK_IF_TRUE( tmp_cond_value_7 );
    if ( tmp_cond_truth_7 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_cond_value_7 );

        exception_lineno = 430;

        goto try_except_handler_25;
    }
    Py_DECREF( tmp_cond_value_7 );
    if ( tmp_cond_truth_7 == 1 )
    {
        goto branch_no_23;
    }
    else
    {
        goto branch_yes_23;
    }
    branch_yes_23:;
    tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (unlikely( tmp_result == false ))
    {
        exception_lineno = 430;
    }

    if (exception_tb && exception_tb->tb_frame == &frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame) frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = exception_tb->tb_lineno;

    goto try_except_handler_25;
    branch_no_23:;
    goto branch_end_22;
    branch_no_22:;
    tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (unlikely( tmp_result == false ))
    {
        exception_lineno = 430;
    }

    if (exception_tb && exception_tb->tb_frame == &frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame) frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = exception_tb->tb_lineno;

    goto try_except_handler_25;
    branch_end_22:;
    goto try_end_23;
    // Exception handler code:
    try_except_handler_25:;
    exception_keeper_type_23 = exception_type;
    exception_keeper_value_23 = exception_value;
    exception_keeper_tb_23 = exception_tb;
    exception_keeper_lineno_23 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_6, exception_preserved_value_6, exception_preserved_tb_6 );
    // Re-raise.
    exception_type = exception_keeper_type_23;
    exception_value = exception_keeper_value_23;
    exception_tb = exception_keeper_tb_23;
    exception_lineno = exception_keeper_lineno_23;

    goto try_except_handler_23;
    // End of try:
    try_end_23:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_6, exception_preserved_value_6, exception_preserved_tb_6 );
    goto try_end_22;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$core$config_init );
    return MOD_RETURN_VALUE( NULL );
    // End of try:
    try_end_22:;
    goto try_end_24;
    // Exception handler code:
    try_except_handler_23:;
    exception_keeper_type_24 = exception_type;
    exception_keeper_value_24 = exception_value;
    exception_keeper_tb_24 = exception_tb;
    exception_keeper_lineno_24 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    tmp_compare_left_17 = tmp_with_6__indicator;

    CHECK_OBJECT( tmp_compare_left_17 );
    tmp_compare_right_17 = Py_True;
    tmp_is_11 = ( tmp_compare_left_17 == tmp_compare_right_17 );
    if ( tmp_is_11 )
    {
        goto branch_yes_24;
    }
    else
    {
        goto branch_no_24;
    }
    branch_yes_24:;
    tmp_called_name_77 = tmp_with_6__exit;

    CHECK_OBJECT( tmp_called_name_77 );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 430;
    tmp_unused = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_77, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

        Py_DECREF( exception_keeper_type_24 );
        Py_XDECREF( exception_keeper_value_24 );
        Py_XDECREF( exception_keeper_tb_24 );

        exception_lineno = 430;

        goto try_except_handler_22;
    }
    Py_DECREF( tmp_unused );
    branch_no_24:;
    // Re-raise.
    exception_type = exception_keeper_type_24;
    exception_value = exception_keeper_value_24;
    exception_tb = exception_keeper_tb_24;
    exception_lineno = exception_keeper_lineno_24;

    goto try_except_handler_22;
    // End of try:
    try_end_24:;
    tmp_compare_left_18 = tmp_with_6__indicator;

    CHECK_OBJECT( tmp_compare_left_18 );
    tmp_compare_right_18 = Py_True;
    tmp_is_12 = ( tmp_compare_left_18 == tmp_compare_right_18 );
    if ( tmp_is_12 )
    {
        goto branch_yes_25;
    }
    else
    {
        goto branch_no_25;
    }
    branch_yes_25:;
    tmp_called_name_78 = tmp_with_6__exit;

    CHECK_OBJECT( tmp_called_name_78 );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 430;
    tmp_unused = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_78, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 430;

        goto try_except_handler_22;
    }
    Py_DECREF( tmp_unused );
    branch_no_25:;
    goto try_end_25;
    // Exception handler code:
    try_except_handler_22:;
    exception_keeper_type_25 = exception_type;
    exception_keeper_value_25 = exception_value;
    exception_keeper_tb_25 = exception_tb;
    exception_keeper_lineno_25 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_with_6__source );
    tmp_with_6__source = NULL;

    Py_XDECREF( tmp_with_6__enter );
    tmp_with_6__enter = NULL;

    Py_XDECREF( tmp_with_6__exit );
    tmp_with_6__exit = NULL;

    Py_XDECREF( tmp_with_6__indicator );
    tmp_with_6__indicator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_25;
    exception_value = exception_keeper_value_25;
    exception_tb = exception_keeper_tb_25;
    exception_lineno = exception_keeper_lineno_25;

    goto frame_exception_exit_1;
    // End of try:
    try_end_25:;
    CHECK_OBJECT( (PyObject *)tmp_with_6__source );
    Py_DECREF( tmp_with_6__source );
    tmp_with_6__source = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_6__enter );
    Py_DECREF( tmp_with_6__enter );
    tmp_with_6__enter = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_6__exit );
    Py_DECREF( tmp_with_6__exit );
    tmp_with_6__exit = NULL;

    Py_XDECREF( tmp_with_6__indicator );
    tmp_with_6__indicator = NULL;

    tmp_assign_source_92 = const_str_digest_3ecce95cea7e70b97124a63fdb6f472c;
    UPDATE_STRING_DICT0( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_writer_engine_doc, tmp_assign_source_92 );
    tmp_assign_source_93 = LIST_COPY( const_list_str_plain_xlwt_list );
    UPDATE_STRING_DICT1( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain__xls_options, tmp_assign_source_93 );
    tmp_assign_source_94 = LIST_COPY( const_list_str_plain_openpyxl_list );
    UPDATE_STRING_DICT1( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain__xlsm_options, tmp_assign_source_94 );
    tmp_assign_source_95 = LIST_COPY( const_list_str_plain_openpyxl_str_plain_xlsxwriter_list );
    UPDATE_STRING_DICT1( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain__xlsx_options, tmp_assign_source_95 );
    // Tried code:
    tmp_called_instance_7 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_called_instance_7 == NULL ))
    {
        tmp_called_instance_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_called_instance_7 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 446;

        goto try_except_handler_26;
    }

    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 446;
    tmp_assign_source_96 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_7, const_str_plain_config_prefix, &PyTuple_GET_ITEM( const_tuple_str_digest_12536abbc33fcdb5edeca9cb579ae639_tuple, 0 ) );

    if ( tmp_assign_source_96 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 446;

        goto try_except_handler_26;
    }
    assert( tmp_with_7__source == NULL );
    tmp_with_7__source = tmp_assign_source_96;

    tmp_source_name_56 = tmp_with_7__source;

    CHECK_OBJECT( tmp_source_name_56 );
    tmp_called_name_79 = LOOKUP_SPECIAL( tmp_source_name_56, const_str_plain___enter__ );
    if ( tmp_called_name_79 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 446;

        goto try_except_handler_26;
    }
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 446;
    tmp_assign_source_97 = CALL_FUNCTION_NO_ARGS( tmp_called_name_79 );
    Py_DECREF( tmp_called_name_79 );
    if ( tmp_assign_source_97 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 446;

        goto try_except_handler_26;
    }
    assert( tmp_with_7__enter == NULL );
    tmp_with_7__enter = tmp_assign_source_97;

    tmp_source_name_57 = tmp_with_7__source;

    CHECK_OBJECT( tmp_source_name_57 );
    tmp_assign_source_98 = LOOKUP_SPECIAL( tmp_source_name_57, const_str_plain___exit__ );
    if ( tmp_assign_source_98 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 446;

        goto try_except_handler_26;
    }
    assert( tmp_with_7__exit == NULL );
    tmp_with_7__exit = tmp_assign_source_98;

    tmp_assign_source_99 = Py_True;
    assert( tmp_with_7__indicator == NULL );
    Py_INCREF( tmp_assign_source_99 );
    tmp_with_7__indicator = tmp_assign_source_99;

    // Tried code:
    // Tried code:
    tmp_source_name_58 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_source_name_58 == NULL ))
    {
        tmp_source_name_58 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_source_name_58 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 447;

        goto try_except_handler_28;
    }

    tmp_called_name_80 = LOOKUP_ATTRIBUTE( tmp_source_name_58, const_str_plain_register_option );
    if ( tmp_called_name_80 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 447;

        goto try_except_handler_28;
    }
    tmp_tuple_element_37 = const_str_plain_writer;
    tmp_args_name_40 = PyTuple_New( 3 );
    Py_INCREF( tmp_tuple_element_37 );
    PyTuple_SET_ITEM( tmp_args_name_40, 0, tmp_tuple_element_37 );
    tmp_tuple_element_37 = const_str_plain_auto;
    Py_INCREF( tmp_tuple_element_37 );
    PyTuple_SET_ITEM( tmp_args_name_40, 1, tmp_tuple_element_37 );
    tmp_source_name_59 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_writer_engine_doc );

    if (unlikely( tmp_source_name_59 == NULL ))
    {
        tmp_source_name_59 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_writer_engine_doc );
    }

    if ( tmp_source_name_59 == NULL )
    {
        Py_DECREF( tmp_called_name_80 );
        Py_DECREF( tmp_args_name_40 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "writer_engine_doc" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 448;

        goto try_except_handler_28;
    }

    tmp_called_name_81 = LOOKUP_ATTRIBUTE( tmp_source_name_59, const_str_plain_format );
    if ( tmp_called_name_81 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_80 );
        Py_DECREF( tmp_args_name_40 );

        exception_lineno = 448;

        goto try_except_handler_28;
    }
    tmp_dict_key_45 = const_str_plain_ext;
    tmp_dict_value_45 = const_str_plain_xls;
    tmp_kw_name_40 = _PyDict_NewPresized( 2 );
    tmp_res = PyDict_SetItem( tmp_kw_name_40, tmp_dict_key_45, tmp_dict_value_45 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_46 = const_str_plain_others;
    tmp_source_name_60 = const_str_digest_db35ab94a03c3cbeb13cbe2a1d728b77;
    tmp_called_name_82 = LOOKUP_ATTRIBUTE( tmp_source_name_60, const_str_plain_join );
    assert( !(tmp_called_name_82 == NULL) );
    tmp_args_element_name_32 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain__xls_options );

    if (unlikely( tmp_args_element_name_32 == NULL ))
    {
        tmp_args_element_name_32 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__xls_options );
    }

    if ( tmp_args_element_name_32 == NULL )
    {
        Py_DECREF( tmp_called_name_80 );
        Py_DECREF( tmp_args_name_40 );
        Py_DECREF( tmp_called_name_81 );
        Py_DECREF( tmp_kw_name_40 );
        Py_DECREF( tmp_called_name_82 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_xls_options" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 450;

        goto try_except_handler_28;
    }

    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 450;
    {
        PyObject *call_args[] = { tmp_args_element_name_32 };
        tmp_dict_value_46 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_82, call_args );
    }

    Py_DECREF( tmp_called_name_82 );
    if ( tmp_dict_value_46 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_80 );
        Py_DECREF( tmp_args_name_40 );
        Py_DECREF( tmp_called_name_81 );
        Py_DECREF( tmp_kw_name_40 );

        exception_lineno = 450;

        goto try_except_handler_28;
    }
    tmp_res = PyDict_SetItem( tmp_kw_name_40, tmp_dict_key_46, tmp_dict_value_46 );
    Py_DECREF( tmp_dict_value_46 );
    assert( !(tmp_res != 0) );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 448;
    tmp_tuple_element_37 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_81, tmp_kw_name_40 );
    Py_DECREF( tmp_called_name_81 );
    Py_DECREF( tmp_kw_name_40 );
    if ( tmp_tuple_element_37 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_80 );
        Py_DECREF( tmp_args_name_40 );

        exception_lineno = 448;

        goto try_except_handler_28;
    }
    PyTuple_SET_ITEM( tmp_args_name_40, 2, tmp_tuple_element_37 );
    tmp_kw_name_41 = PyDict_Copy( const_dict_6385baa8555cb0cf1caa11af10a4cfce );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 447;
    tmp_unused = CALL_FUNCTION( tmp_called_name_80, tmp_args_name_40, tmp_kw_name_41 );
    Py_DECREF( tmp_called_name_80 );
    Py_DECREF( tmp_args_name_40 );
    Py_DECREF( tmp_kw_name_41 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 447;

        goto try_except_handler_28;
    }
    Py_DECREF( tmp_unused );
    goto try_end_26;
    // Exception handler code:
    try_except_handler_28:;
    exception_keeper_type_26 = exception_type;
    exception_keeper_value_26 = exception_value;
    exception_keeper_tb_26 = exception_tb;
    exception_keeper_lineno_26 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_7 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_7 );
    exception_preserved_value_7 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_7 );
    exception_preserved_tb_7 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_7 );

    if ( exception_keeper_tb_26 == NULL )
    {
        exception_keeper_tb_26 = MAKE_TRACEBACK( frame_3209efbe1fc5d2857a97a7e36ce015f8, exception_keeper_lineno_26 );
    }
    else if ( exception_keeper_lineno_26 != 0 )
    {
        exception_keeper_tb_26 = ADD_TRACEBACK( exception_keeper_tb_26, frame_3209efbe1fc5d2857a97a7e36ce015f8, exception_keeper_lineno_26 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_26, &exception_keeper_value_26, &exception_keeper_tb_26 );
    PyException_SetTraceback( exception_keeper_value_26, (PyObject *)exception_keeper_tb_26 );
    PUBLISH_EXCEPTION( &exception_keeper_type_26, &exception_keeper_value_26, &exception_keeper_tb_26 );
    // Tried code:
    tmp_compare_left_19 = EXC_TYPE(PyThreadState_GET());
    tmp_compare_right_19 = PyExc_BaseException;
    tmp_exc_match_exception_match_7 = EXCEPTION_MATCH_BOOL( tmp_compare_left_19, tmp_compare_right_19 );
    if ( tmp_exc_match_exception_match_7 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 446;

        goto try_except_handler_29;
    }
    if ( tmp_exc_match_exception_match_7 == 1 )
    {
        goto branch_yes_26;
    }
    else
    {
        goto branch_no_26;
    }
    branch_yes_26:;
    tmp_assign_source_100 = Py_False;
    {
        PyObject *old = tmp_with_7__indicator;
        assert( old != NULL );
        tmp_with_7__indicator = tmp_assign_source_100;
        Py_INCREF( tmp_with_7__indicator );
        Py_DECREF( old );
    }

    tmp_called_name_83 = tmp_with_7__exit;

    CHECK_OBJECT( tmp_called_name_83 );
    tmp_args_element_name_33 = EXC_TYPE(PyThreadState_GET());
    tmp_args_element_name_34 = EXC_VALUE(PyThreadState_GET());
    tmp_args_element_name_35 = EXC_TRACEBACK(PyThreadState_GET());
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 446;
    {
        PyObject *call_args[] = { tmp_args_element_name_33, tmp_args_element_name_34, tmp_args_element_name_35 };
        tmp_cond_value_8 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_83, call_args );
    }

    if ( tmp_cond_value_8 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 446;

        goto try_except_handler_29;
    }
    tmp_cond_truth_8 = CHECK_IF_TRUE( tmp_cond_value_8 );
    if ( tmp_cond_truth_8 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_cond_value_8 );

        exception_lineno = 446;

        goto try_except_handler_29;
    }
    Py_DECREF( tmp_cond_value_8 );
    if ( tmp_cond_truth_8 == 1 )
    {
        goto branch_no_27;
    }
    else
    {
        goto branch_yes_27;
    }
    branch_yes_27:;
    tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (unlikely( tmp_result == false ))
    {
        exception_lineno = 446;
    }

    if (exception_tb && exception_tb->tb_frame == &frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame) frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = exception_tb->tb_lineno;

    goto try_except_handler_29;
    branch_no_27:;
    goto branch_end_26;
    branch_no_26:;
    tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (unlikely( tmp_result == false ))
    {
        exception_lineno = 446;
    }

    if (exception_tb && exception_tb->tb_frame == &frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame) frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = exception_tb->tb_lineno;

    goto try_except_handler_29;
    branch_end_26:;
    goto try_end_27;
    // Exception handler code:
    try_except_handler_29:;
    exception_keeper_type_27 = exception_type;
    exception_keeper_value_27 = exception_value;
    exception_keeper_tb_27 = exception_tb;
    exception_keeper_lineno_27 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_7, exception_preserved_value_7, exception_preserved_tb_7 );
    // Re-raise.
    exception_type = exception_keeper_type_27;
    exception_value = exception_keeper_value_27;
    exception_tb = exception_keeper_tb_27;
    exception_lineno = exception_keeper_lineno_27;

    goto try_except_handler_27;
    // End of try:
    try_end_27:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_7, exception_preserved_value_7, exception_preserved_tb_7 );
    goto try_end_26;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$core$config_init );
    return MOD_RETURN_VALUE( NULL );
    // End of try:
    try_end_26:;
    goto try_end_28;
    // Exception handler code:
    try_except_handler_27:;
    exception_keeper_type_28 = exception_type;
    exception_keeper_value_28 = exception_value;
    exception_keeper_tb_28 = exception_tb;
    exception_keeper_lineno_28 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    tmp_compare_left_20 = tmp_with_7__indicator;

    CHECK_OBJECT( tmp_compare_left_20 );
    tmp_compare_right_20 = Py_True;
    tmp_is_13 = ( tmp_compare_left_20 == tmp_compare_right_20 );
    if ( tmp_is_13 )
    {
        goto branch_yes_28;
    }
    else
    {
        goto branch_no_28;
    }
    branch_yes_28:;
    tmp_called_name_84 = tmp_with_7__exit;

    CHECK_OBJECT( tmp_called_name_84 );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 446;
    tmp_unused = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_84, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

        Py_DECREF( exception_keeper_type_28 );
        Py_XDECREF( exception_keeper_value_28 );
        Py_XDECREF( exception_keeper_tb_28 );

        exception_lineno = 446;

        goto try_except_handler_26;
    }
    Py_DECREF( tmp_unused );
    branch_no_28:;
    // Re-raise.
    exception_type = exception_keeper_type_28;
    exception_value = exception_keeper_value_28;
    exception_tb = exception_keeper_tb_28;
    exception_lineno = exception_keeper_lineno_28;

    goto try_except_handler_26;
    // End of try:
    try_end_28:;
    tmp_compare_left_21 = tmp_with_7__indicator;

    CHECK_OBJECT( tmp_compare_left_21 );
    tmp_compare_right_21 = Py_True;
    tmp_is_14 = ( tmp_compare_left_21 == tmp_compare_right_21 );
    if ( tmp_is_14 )
    {
        goto branch_yes_29;
    }
    else
    {
        goto branch_no_29;
    }
    branch_yes_29:;
    tmp_called_name_85 = tmp_with_7__exit;

    CHECK_OBJECT( tmp_called_name_85 );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 446;
    tmp_unused = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_85, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 446;

        goto try_except_handler_26;
    }
    Py_DECREF( tmp_unused );
    branch_no_29:;
    goto try_end_29;
    // Exception handler code:
    try_except_handler_26:;
    exception_keeper_type_29 = exception_type;
    exception_keeper_value_29 = exception_value;
    exception_keeper_tb_29 = exception_tb;
    exception_keeper_lineno_29 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_with_7__source );
    tmp_with_7__source = NULL;

    Py_XDECREF( tmp_with_7__enter );
    tmp_with_7__enter = NULL;

    Py_XDECREF( tmp_with_7__exit );
    tmp_with_7__exit = NULL;

    Py_XDECREF( tmp_with_7__indicator );
    tmp_with_7__indicator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_29;
    exception_value = exception_keeper_value_29;
    exception_tb = exception_keeper_tb_29;
    exception_lineno = exception_keeper_lineno_29;

    goto frame_exception_exit_1;
    // End of try:
    try_end_29:;
    CHECK_OBJECT( (PyObject *)tmp_with_7__source );
    Py_DECREF( tmp_with_7__source );
    tmp_with_7__source = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_7__enter );
    Py_DECREF( tmp_with_7__enter );
    tmp_with_7__enter = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_7__exit );
    Py_DECREF( tmp_with_7__exit );
    tmp_with_7__exit = NULL;

    Py_XDECREF( tmp_with_7__indicator );
    tmp_with_7__indicator = NULL;

    // Tried code:
    tmp_called_instance_8 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_called_instance_8 == NULL ))
    {
        tmp_called_instance_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_called_instance_8 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 453;

        goto try_except_handler_30;
    }

    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 453;
    tmp_assign_source_101 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_8, const_str_plain_config_prefix, &PyTuple_GET_ITEM( const_tuple_str_digest_45068c861a2fe7b61a5c03d495ca5c0a_tuple, 0 ) );

    if ( tmp_assign_source_101 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 453;

        goto try_except_handler_30;
    }
    assert( tmp_with_8__source == NULL );
    tmp_with_8__source = tmp_assign_source_101;

    tmp_source_name_61 = tmp_with_8__source;

    CHECK_OBJECT( tmp_source_name_61 );
    tmp_called_name_86 = LOOKUP_SPECIAL( tmp_source_name_61, const_str_plain___enter__ );
    if ( tmp_called_name_86 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 453;

        goto try_except_handler_30;
    }
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 453;
    tmp_assign_source_102 = CALL_FUNCTION_NO_ARGS( tmp_called_name_86 );
    Py_DECREF( tmp_called_name_86 );
    if ( tmp_assign_source_102 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 453;

        goto try_except_handler_30;
    }
    assert( tmp_with_8__enter == NULL );
    tmp_with_8__enter = tmp_assign_source_102;

    tmp_source_name_62 = tmp_with_8__source;

    CHECK_OBJECT( tmp_source_name_62 );
    tmp_assign_source_103 = LOOKUP_SPECIAL( tmp_source_name_62, const_str_plain___exit__ );
    if ( tmp_assign_source_103 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 453;

        goto try_except_handler_30;
    }
    assert( tmp_with_8__exit == NULL );
    tmp_with_8__exit = tmp_assign_source_103;

    tmp_assign_source_104 = Py_True;
    assert( tmp_with_8__indicator == NULL );
    Py_INCREF( tmp_assign_source_104 );
    tmp_with_8__indicator = tmp_assign_source_104;

    // Tried code:
    // Tried code:
    tmp_source_name_63 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_source_name_63 == NULL ))
    {
        tmp_source_name_63 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_source_name_63 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 454;

        goto try_except_handler_32;
    }

    tmp_called_name_87 = LOOKUP_ATTRIBUTE( tmp_source_name_63, const_str_plain_register_option );
    if ( tmp_called_name_87 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 454;

        goto try_except_handler_32;
    }
    tmp_tuple_element_38 = const_str_plain_writer;
    tmp_args_name_41 = PyTuple_New( 3 );
    Py_INCREF( tmp_tuple_element_38 );
    PyTuple_SET_ITEM( tmp_args_name_41, 0, tmp_tuple_element_38 );
    tmp_tuple_element_38 = const_str_plain_auto;
    Py_INCREF( tmp_tuple_element_38 );
    PyTuple_SET_ITEM( tmp_args_name_41, 1, tmp_tuple_element_38 );
    tmp_source_name_64 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_writer_engine_doc );

    if (unlikely( tmp_source_name_64 == NULL ))
    {
        tmp_source_name_64 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_writer_engine_doc );
    }

    if ( tmp_source_name_64 == NULL )
    {
        Py_DECREF( tmp_called_name_87 );
        Py_DECREF( tmp_args_name_41 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "writer_engine_doc" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 455;

        goto try_except_handler_32;
    }

    tmp_called_name_88 = LOOKUP_ATTRIBUTE( tmp_source_name_64, const_str_plain_format );
    if ( tmp_called_name_88 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_87 );
        Py_DECREF( tmp_args_name_41 );

        exception_lineno = 455;

        goto try_except_handler_32;
    }
    tmp_dict_key_47 = const_str_plain_ext;
    tmp_dict_value_47 = const_str_plain_xlsm;
    tmp_kw_name_42 = _PyDict_NewPresized( 2 );
    tmp_res = PyDict_SetItem( tmp_kw_name_42, tmp_dict_key_47, tmp_dict_value_47 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_48 = const_str_plain_others;
    tmp_source_name_65 = const_str_digest_db35ab94a03c3cbeb13cbe2a1d728b77;
    tmp_called_name_89 = LOOKUP_ATTRIBUTE( tmp_source_name_65, const_str_plain_join );
    assert( !(tmp_called_name_89 == NULL) );
    tmp_args_element_name_36 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain__xlsm_options );

    if (unlikely( tmp_args_element_name_36 == NULL ))
    {
        tmp_args_element_name_36 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__xlsm_options );
    }

    if ( tmp_args_element_name_36 == NULL )
    {
        Py_DECREF( tmp_called_name_87 );
        Py_DECREF( tmp_args_name_41 );
        Py_DECREF( tmp_called_name_88 );
        Py_DECREF( tmp_kw_name_42 );
        Py_DECREF( tmp_called_name_89 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_xlsm_options" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 457;

        goto try_except_handler_32;
    }

    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 457;
    {
        PyObject *call_args[] = { tmp_args_element_name_36 };
        tmp_dict_value_48 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_89, call_args );
    }

    Py_DECREF( tmp_called_name_89 );
    if ( tmp_dict_value_48 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_87 );
        Py_DECREF( tmp_args_name_41 );
        Py_DECREF( tmp_called_name_88 );
        Py_DECREF( tmp_kw_name_42 );

        exception_lineno = 457;

        goto try_except_handler_32;
    }
    tmp_res = PyDict_SetItem( tmp_kw_name_42, tmp_dict_key_48, tmp_dict_value_48 );
    Py_DECREF( tmp_dict_value_48 );
    assert( !(tmp_res != 0) );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 455;
    tmp_tuple_element_38 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_88, tmp_kw_name_42 );
    Py_DECREF( tmp_called_name_88 );
    Py_DECREF( tmp_kw_name_42 );
    if ( tmp_tuple_element_38 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_87 );
        Py_DECREF( tmp_args_name_41 );

        exception_lineno = 455;

        goto try_except_handler_32;
    }
    PyTuple_SET_ITEM( tmp_args_name_41, 2, tmp_tuple_element_38 );
    tmp_kw_name_43 = PyDict_Copy( const_dict_6385baa8555cb0cf1caa11af10a4cfce );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 454;
    tmp_unused = CALL_FUNCTION( tmp_called_name_87, tmp_args_name_41, tmp_kw_name_43 );
    Py_DECREF( tmp_called_name_87 );
    Py_DECREF( tmp_args_name_41 );
    Py_DECREF( tmp_kw_name_43 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 454;

        goto try_except_handler_32;
    }
    Py_DECREF( tmp_unused );
    goto try_end_30;
    // Exception handler code:
    try_except_handler_32:;
    exception_keeper_type_30 = exception_type;
    exception_keeper_value_30 = exception_value;
    exception_keeper_tb_30 = exception_tb;
    exception_keeper_lineno_30 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_8 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_8 );
    exception_preserved_value_8 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_8 );
    exception_preserved_tb_8 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_8 );

    if ( exception_keeper_tb_30 == NULL )
    {
        exception_keeper_tb_30 = MAKE_TRACEBACK( frame_3209efbe1fc5d2857a97a7e36ce015f8, exception_keeper_lineno_30 );
    }
    else if ( exception_keeper_lineno_30 != 0 )
    {
        exception_keeper_tb_30 = ADD_TRACEBACK( exception_keeper_tb_30, frame_3209efbe1fc5d2857a97a7e36ce015f8, exception_keeper_lineno_30 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_30, &exception_keeper_value_30, &exception_keeper_tb_30 );
    PyException_SetTraceback( exception_keeper_value_30, (PyObject *)exception_keeper_tb_30 );
    PUBLISH_EXCEPTION( &exception_keeper_type_30, &exception_keeper_value_30, &exception_keeper_tb_30 );
    // Tried code:
    tmp_compare_left_22 = EXC_TYPE(PyThreadState_GET());
    tmp_compare_right_22 = PyExc_BaseException;
    tmp_exc_match_exception_match_8 = EXCEPTION_MATCH_BOOL( tmp_compare_left_22, tmp_compare_right_22 );
    if ( tmp_exc_match_exception_match_8 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 453;

        goto try_except_handler_33;
    }
    if ( tmp_exc_match_exception_match_8 == 1 )
    {
        goto branch_yes_30;
    }
    else
    {
        goto branch_no_30;
    }
    branch_yes_30:;
    tmp_assign_source_105 = Py_False;
    {
        PyObject *old = tmp_with_8__indicator;
        assert( old != NULL );
        tmp_with_8__indicator = tmp_assign_source_105;
        Py_INCREF( tmp_with_8__indicator );
        Py_DECREF( old );
    }

    tmp_called_name_90 = tmp_with_8__exit;

    CHECK_OBJECT( tmp_called_name_90 );
    tmp_args_element_name_37 = EXC_TYPE(PyThreadState_GET());
    tmp_args_element_name_38 = EXC_VALUE(PyThreadState_GET());
    tmp_args_element_name_39 = EXC_TRACEBACK(PyThreadState_GET());
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 453;
    {
        PyObject *call_args[] = { tmp_args_element_name_37, tmp_args_element_name_38, tmp_args_element_name_39 };
        tmp_cond_value_9 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_90, call_args );
    }

    if ( tmp_cond_value_9 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 453;

        goto try_except_handler_33;
    }
    tmp_cond_truth_9 = CHECK_IF_TRUE( tmp_cond_value_9 );
    if ( tmp_cond_truth_9 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_cond_value_9 );

        exception_lineno = 453;

        goto try_except_handler_33;
    }
    Py_DECREF( tmp_cond_value_9 );
    if ( tmp_cond_truth_9 == 1 )
    {
        goto branch_no_31;
    }
    else
    {
        goto branch_yes_31;
    }
    branch_yes_31:;
    tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (unlikely( tmp_result == false ))
    {
        exception_lineno = 453;
    }

    if (exception_tb && exception_tb->tb_frame == &frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame) frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = exception_tb->tb_lineno;

    goto try_except_handler_33;
    branch_no_31:;
    goto branch_end_30;
    branch_no_30:;
    tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (unlikely( tmp_result == false ))
    {
        exception_lineno = 453;
    }

    if (exception_tb && exception_tb->tb_frame == &frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame) frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = exception_tb->tb_lineno;

    goto try_except_handler_33;
    branch_end_30:;
    goto try_end_31;
    // Exception handler code:
    try_except_handler_33:;
    exception_keeper_type_31 = exception_type;
    exception_keeper_value_31 = exception_value;
    exception_keeper_tb_31 = exception_tb;
    exception_keeper_lineno_31 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_8, exception_preserved_value_8, exception_preserved_tb_8 );
    // Re-raise.
    exception_type = exception_keeper_type_31;
    exception_value = exception_keeper_value_31;
    exception_tb = exception_keeper_tb_31;
    exception_lineno = exception_keeper_lineno_31;

    goto try_except_handler_31;
    // End of try:
    try_end_31:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_8, exception_preserved_value_8, exception_preserved_tb_8 );
    goto try_end_30;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$core$config_init );
    return MOD_RETURN_VALUE( NULL );
    // End of try:
    try_end_30:;
    goto try_end_32;
    // Exception handler code:
    try_except_handler_31:;
    exception_keeper_type_32 = exception_type;
    exception_keeper_value_32 = exception_value;
    exception_keeper_tb_32 = exception_tb;
    exception_keeper_lineno_32 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    tmp_compare_left_23 = tmp_with_8__indicator;

    CHECK_OBJECT( tmp_compare_left_23 );
    tmp_compare_right_23 = Py_True;
    tmp_is_15 = ( tmp_compare_left_23 == tmp_compare_right_23 );
    if ( tmp_is_15 )
    {
        goto branch_yes_32;
    }
    else
    {
        goto branch_no_32;
    }
    branch_yes_32:;
    tmp_called_name_91 = tmp_with_8__exit;

    CHECK_OBJECT( tmp_called_name_91 );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 453;
    tmp_unused = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_91, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

        Py_DECREF( exception_keeper_type_32 );
        Py_XDECREF( exception_keeper_value_32 );
        Py_XDECREF( exception_keeper_tb_32 );

        exception_lineno = 453;

        goto try_except_handler_30;
    }
    Py_DECREF( tmp_unused );
    branch_no_32:;
    // Re-raise.
    exception_type = exception_keeper_type_32;
    exception_value = exception_keeper_value_32;
    exception_tb = exception_keeper_tb_32;
    exception_lineno = exception_keeper_lineno_32;

    goto try_except_handler_30;
    // End of try:
    try_end_32:;
    tmp_compare_left_24 = tmp_with_8__indicator;

    CHECK_OBJECT( tmp_compare_left_24 );
    tmp_compare_right_24 = Py_True;
    tmp_is_16 = ( tmp_compare_left_24 == tmp_compare_right_24 );
    if ( tmp_is_16 )
    {
        goto branch_yes_33;
    }
    else
    {
        goto branch_no_33;
    }
    branch_yes_33:;
    tmp_called_name_92 = tmp_with_8__exit;

    CHECK_OBJECT( tmp_called_name_92 );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 453;
    tmp_unused = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_92, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 453;

        goto try_except_handler_30;
    }
    Py_DECREF( tmp_unused );
    branch_no_33:;
    goto try_end_33;
    // Exception handler code:
    try_except_handler_30:;
    exception_keeper_type_33 = exception_type;
    exception_keeper_value_33 = exception_value;
    exception_keeper_tb_33 = exception_tb;
    exception_keeper_lineno_33 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_with_8__source );
    tmp_with_8__source = NULL;

    Py_XDECREF( tmp_with_8__enter );
    tmp_with_8__enter = NULL;

    Py_XDECREF( tmp_with_8__exit );
    tmp_with_8__exit = NULL;

    Py_XDECREF( tmp_with_8__indicator );
    tmp_with_8__indicator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_33;
    exception_value = exception_keeper_value_33;
    exception_tb = exception_keeper_tb_33;
    exception_lineno = exception_keeper_lineno_33;

    goto frame_exception_exit_1;
    // End of try:
    try_end_33:;
    CHECK_OBJECT( (PyObject *)tmp_with_8__source );
    Py_DECREF( tmp_with_8__source );
    tmp_with_8__source = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_8__enter );
    Py_DECREF( tmp_with_8__enter );
    tmp_with_8__enter = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_8__exit );
    Py_DECREF( tmp_with_8__exit );
    tmp_with_8__exit = NULL;

    Py_XDECREF( tmp_with_8__indicator );
    tmp_with_8__indicator = NULL;

    // Tried code:
    tmp_called_instance_9 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_called_instance_9 == NULL ))
    {
        tmp_called_instance_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_called_instance_9 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 461;

        goto try_except_handler_34;
    }

    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 461;
    tmp_assign_source_106 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_9, const_str_plain_config_prefix, &PyTuple_GET_ITEM( const_tuple_str_digest_d81e5a583e51219530e4aa7bf1ec93ce_tuple, 0 ) );

    if ( tmp_assign_source_106 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 461;

        goto try_except_handler_34;
    }
    assert( tmp_with_9__source == NULL );
    tmp_with_9__source = tmp_assign_source_106;

    tmp_source_name_66 = tmp_with_9__source;

    CHECK_OBJECT( tmp_source_name_66 );
    tmp_called_name_93 = LOOKUP_SPECIAL( tmp_source_name_66, const_str_plain___enter__ );
    if ( tmp_called_name_93 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 461;

        goto try_except_handler_34;
    }
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 461;
    tmp_assign_source_107 = CALL_FUNCTION_NO_ARGS( tmp_called_name_93 );
    Py_DECREF( tmp_called_name_93 );
    if ( tmp_assign_source_107 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 461;

        goto try_except_handler_34;
    }
    assert( tmp_with_9__enter == NULL );
    tmp_with_9__enter = tmp_assign_source_107;

    tmp_source_name_67 = tmp_with_9__source;

    CHECK_OBJECT( tmp_source_name_67 );
    tmp_assign_source_108 = LOOKUP_SPECIAL( tmp_source_name_67, const_str_plain___exit__ );
    if ( tmp_assign_source_108 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 461;

        goto try_except_handler_34;
    }
    assert( tmp_with_9__exit == NULL );
    tmp_with_9__exit = tmp_assign_source_108;

    tmp_assign_source_109 = Py_True;
    assert( tmp_with_9__indicator == NULL );
    Py_INCREF( tmp_assign_source_109 );
    tmp_with_9__indicator = tmp_assign_source_109;

    // Tried code:
    // Tried code:
    tmp_source_name_68 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_source_name_68 == NULL ))
    {
        tmp_source_name_68 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_source_name_68 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 462;

        goto try_except_handler_36;
    }

    tmp_called_name_94 = LOOKUP_ATTRIBUTE( tmp_source_name_68, const_str_plain_register_option );
    if ( tmp_called_name_94 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 462;

        goto try_except_handler_36;
    }
    tmp_tuple_element_39 = const_str_plain_writer;
    tmp_args_name_42 = PyTuple_New( 3 );
    Py_INCREF( tmp_tuple_element_39 );
    PyTuple_SET_ITEM( tmp_args_name_42, 0, tmp_tuple_element_39 );
    tmp_tuple_element_39 = const_str_plain_auto;
    Py_INCREF( tmp_tuple_element_39 );
    PyTuple_SET_ITEM( tmp_args_name_42, 1, tmp_tuple_element_39 );
    tmp_source_name_69 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_writer_engine_doc );

    if (unlikely( tmp_source_name_69 == NULL ))
    {
        tmp_source_name_69 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_writer_engine_doc );
    }

    if ( tmp_source_name_69 == NULL )
    {
        Py_DECREF( tmp_called_name_94 );
        Py_DECREF( tmp_args_name_42 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "writer_engine_doc" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 463;

        goto try_except_handler_36;
    }

    tmp_called_name_95 = LOOKUP_ATTRIBUTE( tmp_source_name_69, const_str_plain_format );
    if ( tmp_called_name_95 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_94 );
        Py_DECREF( tmp_args_name_42 );

        exception_lineno = 463;

        goto try_except_handler_36;
    }
    tmp_dict_key_49 = const_str_plain_ext;
    tmp_dict_value_49 = const_str_plain_xlsx;
    tmp_kw_name_44 = _PyDict_NewPresized( 2 );
    tmp_res = PyDict_SetItem( tmp_kw_name_44, tmp_dict_key_49, tmp_dict_value_49 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_50 = const_str_plain_others;
    tmp_source_name_70 = const_str_digest_db35ab94a03c3cbeb13cbe2a1d728b77;
    tmp_called_name_96 = LOOKUP_ATTRIBUTE( tmp_source_name_70, const_str_plain_join );
    assert( !(tmp_called_name_96 == NULL) );
    tmp_args_element_name_40 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain__xlsx_options );

    if (unlikely( tmp_args_element_name_40 == NULL ))
    {
        tmp_args_element_name_40 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__xlsx_options );
    }

    if ( tmp_args_element_name_40 == NULL )
    {
        Py_DECREF( tmp_called_name_94 );
        Py_DECREF( tmp_args_name_42 );
        Py_DECREF( tmp_called_name_95 );
        Py_DECREF( tmp_kw_name_44 );
        Py_DECREF( tmp_called_name_96 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_xlsx_options" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 465;

        goto try_except_handler_36;
    }

    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 465;
    {
        PyObject *call_args[] = { tmp_args_element_name_40 };
        tmp_dict_value_50 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_96, call_args );
    }

    Py_DECREF( tmp_called_name_96 );
    if ( tmp_dict_value_50 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_94 );
        Py_DECREF( tmp_args_name_42 );
        Py_DECREF( tmp_called_name_95 );
        Py_DECREF( tmp_kw_name_44 );

        exception_lineno = 465;

        goto try_except_handler_36;
    }
    tmp_res = PyDict_SetItem( tmp_kw_name_44, tmp_dict_key_50, tmp_dict_value_50 );
    Py_DECREF( tmp_dict_value_50 );
    assert( !(tmp_res != 0) );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 463;
    tmp_tuple_element_39 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_95, tmp_kw_name_44 );
    Py_DECREF( tmp_called_name_95 );
    Py_DECREF( tmp_kw_name_44 );
    if ( tmp_tuple_element_39 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_94 );
        Py_DECREF( tmp_args_name_42 );

        exception_lineno = 463;

        goto try_except_handler_36;
    }
    PyTuple_SET_ITEM( tmp_args_name_42, 2, tmp_tuple_element_39 );
    tmp_kw_name_45 = PyDict_Copy( const_dict_6385baa8555cb0cf1caa11af10a4cfce );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 462;
    tmp_unused = CALL_FUNCTION( tmp_called_name_94, tmp_args_name_42, tmp_kw_name_45 );
    Py_DECREF( tmp_called_name_94 );
    Py_DECREF( tmp_args_name_42 );
    Py_DECREF( tmp_kw_name_45 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 462;

        goto try_except_handler_36;
    }
    Py_DECREF( tmp_unused );
    goto try_end_34;
    // Exception handler code:
    try_except_handler_36:;
    exception_keeper_type_34 = exception_type;
    exception_keeper_value_34 = exception_value;
    exception_keeper_tb_34 = exception_tb;
    exception_keeper_lineno_34 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_9 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_9 );
    exception_preserved_value_9 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_9 );
    exception_preserved_tb_9 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_9 );

    if ( exception_keeper_tb_34 == NULL )
    {
        exception_keeper_tb_34 = MAKE_TRACEBACK( frame_3209efbe1fc5d2857a97a7e36ce015f8, exception_keeper_lineno_34 );
    }
    else if ( exception_keeper_lineno_34 != 0 )
    {
        exception_keeper_tb_34 = ADD_TRACEBACK( exception_keeper_tb_34, frame_3209efbe1fc5d2857a97a7e36ce015f8, exception_keeper_lineno_34 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_34, &exception_keeper_value_34, &exception_keeper_tb_34 );
    PyException_SetTraceback( exception_keeper_value_34, (PyObject *)exception_keeper_tb_34 );
    PUBLISH_EXCEPTION( &exception_keeper_type_34, &exception_keeper_value_34, &exception_keeper_tb_34 );
    // Tried code:
    tmp_compare_left_25 = EXC_TYPE(PyThreadState_GET());
    tmp_compare_right_25 = PyExc_BaseException;
    tmp_exc_match_exception_match_9 = EXCEPTION_MATCH_BOOL( tmp_compare_left_25, tmp_compare_right_25 );
    if ( tmp_exc_match_exception_match_9 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 461;

        goto try_except_handler_37;
    }
    if ( tmp_exc_match_exception_match_9 == 1 )
    {
        goto branch_yes_34;
    }
    else
    {
        goto branch_no_34;
    }
    branch_yes_34:;
    tmp_assign_source_110 = Py_False;
    {
        PyObject *old = tmp_with_9__indicator;
        assert( old != NULL );
        tmp_with_9__indicator = tmp_assign_source_110;
        Py_INCREF( tmp_with_9__indicator );
        Py_DECREF( old );
    }

    tmp_called_name_97 = tmp_with_9__exit;

    CHECK_OBJECT( tmp_called_name_97 );
    tmp_args_element_name_41 = EXC_TYPE(PyThreadState_GET());
    tmp_args_element_name_42 = EXC_VALUE(PyThreadState_GET());
    tmp_args_element_name_43 = EXC_TRACEBACK(PyThreadState_GET());
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 461;
    {
        PyObject *call_args[] = { tmp_args_element_name_41, tmp_args_element_name_42, tmp_args_element_name_43 };
        tmp_cond_value_10 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_97, call_args );
    }

    if ( tmp_cond_value_10 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 461;

        goto try_except_handler_37;
    }
    tmp_cond_truth_10 = CHECK_IF_TRUE( tmp_cond_value_10 );
    if ( tmp_cond_truth_10 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_cond_value_10 );

        exception_lineno = 461;

        goto try_except_handler_37;
    }
    Py_DECREF( tmp_cond_value_10 );
    if ( tmp_cond_truth_10 == 1 )
    {
        goto branch_no_35;
    }
    else
    {
        goto branch_yes_35;
    }
    branch_yes_35:;
    tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (unlikely( tmp_result == false ))
    {
        exception_lineno = 461;
    }

    if (exception_tb && exception_tb->tb_frame == &frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame) frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = exception_tb->tb_lineno;

    goto try_except_handler_37;
    branch_no_35:;
    goto branch_end_34;
    branch_no_34:;
    tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (unlikely( tmp_result == false ))
    {
        exception_lineno = 461;
    }

    if (exception_tb && exception_tb->tb_frame == &frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame) frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = exception_tb->tb_lineno;

    goto try_except_handler_37;
    branch_end_34:;
    goto try_end_35;
    // Exception handler code:
    try_except_handler_37:;
    exception_keeper_type_35 = exception_type;
    exception_keeper_value_35 = exception_value;
    exception_keeper_tb_35 = exception_tb;
    exception_keeper_lineno_35 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_9, exception_preserved_value_9, exception_preserved_tb_9 );
    // Re-raise.
    exception_type = exception_keeper_type_35;
    exception_value = exception_keeper_value_35;
    exception_tb = exception_keeper_tb_35;
    exception_lineno = exception_keeper_lineno_35;

    goto try_except_handler_35;
    // End of try:
    try_end_35:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_9, exception_preserved_value_9, exception_preserved_tb_9 );
    goto try_end_34;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$core$config_init );
    return MOD_RETURN_VALUE( NULL );
    // End of try:
    try_end_34:;
    goto try_end_36;
    // Exception handler code:
    try_except_handler_35:;
    exception_keeper_type_36 = exception_type;
    exception_keeper_value_36 = exception_value;
    exception_keeper_tb_36 = exception_tb;
    exception_keeper_lineno_36 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    tmp_compare_left_26 = tmp_with_9__indicator;

    CHECK_OBJECT( tmp_compare_left_26 );
    tmp_compare_right_26 = Py_True;
    tmp_is_17 = ( tmp_compare_left_26 == tmp_compare_right_26 );
    if ( tmp_is_17 )
    {
        goto branch_yes_36;
    }
    else
    {
        goto branch_no_36;
    }
    branch_yes_36:;
    tmp_called_name_98 = tmp_with_9__exit;

    CHECK_OBJECT( tmp_called_name_98 );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 461;
    tmp_unused = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_98, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

        Py_DECREF( exception_keeper_type_36 );
        Py_XDECREF( exception_keeper_value_36 );
        Py_XDECREF( exception_keeper_tb_36 );

        exception_lineno = 461;

        goto try_except_handler_34;
    }
    Py_DECREF( tmp_unused );
    branch_no_36:;
    // Re-raise.
    exception_type = exception_keeper_type_36;
    exception_value = exception_keeper_value_36;
    exception_tb = exception_keeper_tb_36;
    exception_lineno = exception_keeper_lineno_36;

    goto try_except_handler_34;
    // End of try:
    try_end_36:;
    tmp_compare_left_27 = tmp_with_9__indicator;

    CHECK_OBJECT( tmp_compare_left_27 );
    tmp_compare_right_27 = Py_True;
    tmp_is_18 = ( tmp_compare_left_27 == tmp_compare_right_27 );
    if ( tmp_is_18 )
    {
        goto branch_yes_37;
    }
    else
    {
        goto branch_no_37;
    }
    branch_yes_37:;
    tmp_called_name_99 = tmp_with_9__exit;

    CHECK_OBJECT( tmp_called_name_99 );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 461;
    tmp_unused = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_99, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 461;

        goto try_except_handler_34;
    }
    Py_DECREF( tmp_unused );
    branch_no_37:;
    goto try_end_37;
    // Exception handler code:
    try_except_handler_34:;
    exception_keeper_type_37 = exception_type;
    exception_keeper_value_37 = exception_value;
    exception_keeper_tb_37 = exception_tb;
    exception_keeper_lineno_37 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_with_9__source );
    tmp_with_9__source = NULL;

    Py_XDECREF( tmp_with_9__enter );
    tmp_with_9__enter = NULL;

    Py_XDECREF( tmp_with_9__exit );
    tmp_with_9__exit = NULL;

    Py_XDECREF( tmp_with_9__indicator );
    tmp_with_9__indicator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_37;
    exception_value = exception_keeper_value_37;
    exception_tb = exception_keeper_tb_37;
    exception_lineno = exception_keeper_lineno_37;

    goto frame_exception_exit_1;
    // End of try:
    try_end_37:;
    CHECK_OBJECT( (PyObject *)tmp_with_9__source );
    Py_DECREF( tmp_with_9__source );
    tmp_with_9__source = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_9__enter );
    Py_DECREF( tmp_with_9__enter );
    tmp_with_9__enter = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_9__exit );
    Py_DECREF( tmp_with_9__exit );
    tmp_with_9__exit = NULL;

    Py_XDECREF( tmp_with_9__indicator );
    tmp_with_9__indicator = NULL;

    tmp_assign_source_111 = const_str_digest_0fe0d9af171687ff64bb6c40c034c136;
    UPDATE_STRING_DICT0( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_parquet_engine_doc, tmp_assign_source_111 );
    // Tried code:
    tmp_called_instance_10 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_called_instance_10 == NULL ))
    {
        tmp_called_instance_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_called_instance_10 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 476;

        goto try_except_handler_38;
    }

    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 476;
    tmp_assign_source_112 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_10, const_str_plain_config_prefix, &PyTuple_GET_ITEM( const_tuple_str_digest_dcbf3c6268caaf75da6d6de7bfa9dfac_tuple, 0 ) );

    if ( tmp_assign_source_112 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 476;

        goto try_except_handler_38;
    }
    assert( tmp_with_10__source == NULL );
    tmp_with_10__source = tmp_assign_source_112;

    tmp_source_name_71 = tmp_with_10__source;

    CHECK_OBJECT( tmp_source_name_71 );
    tmp_called_name_100 = LOOKUP_SPECIAL( tmp_source_name_71, const_str_plain___enter__ );
    if ( tmp_called_name_100 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 476;

        goto try_except_handler_38;
    }
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 476;
    tmp_assign_source_113 = CALL_FUNCTION_NO_ARGS( tmp_called_name_100 );
    Py_DECREF( tmp_called_name_100 );
    if ( tmp_assign_source_113 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 476;

        goto try_except_handler_38;
    }
    assert( tmp_with_10__enter == NULL );
    tmp_with_10__enter = tmp_assign_source_113;

    tmp_source_name_72 = tmp_with_10__source;

    CHECK_OBJECT( tmp_source_name_72 );
    tmp_assign_source_114 = LOOKUP_SPECIAL( tmp_source_name_72, const_str_plain___exit__ );
    if ( tmp_assign_source_114 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 476;

        goto try_except_handler_38;
    }
    assert( tmp_with_10__exit == NULL );
    tmp_with_10__exit = tmp_assign_source_114;

    tmp_assign_source_115 = Py_True;
    assert( tmp_with_10__indicator == NULL );
    Py_INCREF( tmp_assign_source_115 );
    tmp_with_10__indicator = tmp_assign_source_115;

    // Tried code:
    // Tried code:
    tmp_source_name_73 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_source_name_73 == NULL ))
    {
        tmp_source_name_73 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_source_name_73 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 477;

        goto try_except_handler_40;
    }

    tmp_called_name_101 = LOOKUP_ATTRIBUTE( tmp_source_name_73, const_str_plain_register_option );
    if ( tmp_called_name_101 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 477;

        goto try_except_handler_40;
    }
    tmp_tuple_element_40 = const_str_plain_engine;
    tmp_args_name_43 = PyTuple_New( 3 );
    Py_INCREF( tmp_tuple_element_40 );
    PyTuple_SET_ITEM( tmp_args_name_43, 0, tmp_tuple_element_40 );
    tmp_tuple_element_40 = const_str_plain_auto;
    Py_INCREF( tmp_tuple_element_40 );
    PyTuple_SET_ITEM( tmp_args_name_43, 1, tmp_tuple_element_40 );
    tmp_tuple_element_40 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_parquet_engine_doc );

    if (unlikely( tmp_tuple_element_40 == NULL ))
    {
        tmp_tuple_element_40 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_parquet_engine_doc );
    }

    if ( tmp_tuple_element_40 == NULL )
    {
        Py_DECREF( tmp_called_name_101 );
        Py_DECREF( tmp_args_name_43 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "parquet_engine_doc" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 478;

        goto try_except_handler_40;
    }

    Py_INCREF( tmp_tuple_element_40 );
    PyTuple_SET_ITEM( tmp_args_name_43, 2, tmp_tuple_element_40 );
    tmp_dict_key_51 = const_str_plain_validator;
    tmp_called_name_102 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_is_one_of_factory );

    if (unlikely( tmp_called_name_102 == NULL ))
    {
        tmp_called_name_102 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_one_of_factory );
    }

    if ( tmp_called_name_102 == NULL )
    {
        Py_DECREF( tmp_called_name_101 );
        Py_DECREF( tmp_args_name_43 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_one_of_factory" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 479;

        goto try_except_handler_40;
    }

    tmp_call_arg_element_8 = LIST_COPY( const_list_str_plain_auto_str_plain_pyarrow_str_plain_fastparquet_list );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 479;
    {
        PyObject *call_args[] = { tmp_call_arg_element_8 };
        tmp_dict_value_51 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_102, call_args );
    }

    Py_DECREF( tmp_call_arg_element_8 );
    if ( tmp_dict_value_51 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_101 );
        Py_DECREF( tmp_args_name_43 );

        exception_lineno = 479;

        goto try_except_handler_40;
    }
    tmp_kw_name_46 = _PyDict_NewPresized( 1 );
    tmp_res = PyDict_SetItem( tmp_kw_name_46, tmp_dict_key_51, tmp_dict_value_51 );
    Py_DECREF( tmp_dict_value_51 );
    assert( !(tmp_res != 0) );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 477;
    tmp_unused = CALL_FUNCTION( tmp_called_name_101, tmp_args_name_43, tmp_kw_name_46 );
    Py_DECREF( tmp_called_name_101 );
    Py_DECREF( tmp_args_name_43 );
    Py_DECREF( tmp_kw_name_46 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 477;

        goto try_except_handler_40;
    }
    Py_DECREF( tmp_unused );
    goto try_end_38;
    // Exception handler code:
    try_except_handler_40:;
    exception_keeper_type_38 = exception_type;
    exception_keeper_value_38 = exception_value;
    exception_keeper_tb_38 = exception_tb;
    exception_keeper_lineno_38 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_10 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_10 );
    exception_preserved_value_10 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_10 );
    exception_preserved_tb_10 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_10 );

    if ( exception_keeper_tb_38 == NULL )
    {
        exception_keeper_tb_38 = MAKE_TRACEBACK( frame_3209efbe1fc5d2857a97a7e36ce015f8, exception_keeper_lineno_38 );
    }
    else if ( exception_keeper_lineno_38 != 0 )
    {
        exception_keeper_tb_38 = ADD_TRACEBACK( exception_keeper_tb_38, frame_3209efbe1fc5d2857a97a7e36ce015f8, exception_keeper_lineno_38 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_38, &exception_keeper_value_38, &exception_keeper_tb_38 );
    PyException_SetTraceback( exception_keeper_value_38, (PyObject *)exception_keeper_tb_38 );
    PUBLISH_EXCEPTION( &exception_keeper_type_38, &exception_keeper_value_38, &exception_keeper_tb_38 );
    // Tried code:
    tmp_compare_left_28 = EXC_TYPE(PyThreadState_GET());
    tmp_compare_right_28 = PyExc_BaseException;
    tmp_exc_match_exception_match_10 = EXCEPTION_MATCH_BOOL( tmp_compare_left_28, tmp_compare_right_28 );
    if ( tmp_exc_match_exception_match_10 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 476;

        goto try_except_handler_41;
    }
    if ( tmp_exc_match_exception_match_10 == 1 )
    {
        goto branch_yes_38;
    }
    else
    {
        goto branch_no_38;
    }
    branch_yes_38:;
    tmp_assign_source_116 = Py_False;
    {
        PyObject *old = tmp_with_10__indicator;
        assert( old != NULL );
        tmp_with_10__indicator = tmp_assign_source_116;
        Py_INCREF( tmp_with_10__indicator );
        Py_DECREF( old );
    }

    tmp_called_name_103 = tmp_with_10__exit;

    CHECK_OBJECT( tmp_called_name_103 );
    tmp_args_element_name_44 = EXC_TYPE(PyThreadState_GET());
    tmp_args_element_name_45 = EXC_VALUE(PyThreadState_GET());
    tmp_args_element_name_46 = EXC_TRACEBACK(PyThreadState_GET());
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 476;
    {
        PyObject *call_args[] = { tmp_args_element_name_44, tmp_args_element_name_45, tmp_args_element_name_46 };
        tmp_cond_value_11 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_103, call_args );
    }

    if ( tmp_cond_value_11 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 476;

        goto try_except_handler_41;
    }
    tmp_cond_truth_11 = CHECK_IF_TRUE( tmp_cond_value_11 );
    if ( tmp_cond_truth_11 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_cond_value_11 );

        exception_lineno = 476;

        goto try_except_handler_41;
    }
    Py_DECREF( tmp_cond_value_11 );
    if ( tmp_cond_truth_11 == 1 )
    {
        goto branch_no_39;
    }
    else
    {
        goto branch_yes_39;
    }
    branch_yes_39:;
    tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (unlikely( tmp_result == false ))
    {
        exception_lineno = 476;
    }

    if (exception_tb && exception_tb->tb_frame == &frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame) frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = exception_tb->tb_lineno;

    goto try_except_handler_41;
    branch_no_39:;
    goto branch_end_38;
    branch_no_38:;
    tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (unlikely( tmp_result == false ))
    {
        exception_lineno = 476;
    }

    if (exception_tb && exception_tb->tb_frame == &frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame) frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = exception_tb->tb_lineno;

    goto try_except_handler_41;
    branch_end_38:;
    goto try_end_39;
    // Exception handler code:
    try_except_handler_41:;
    exception_keeper_type_39 = exception_type;
    exception_keeper_value_39 = exception_value;
    exception_keeper_tb_39 = exception_tb;
    exception_keeper_lineno_39 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_10, exception_preserved_value_10, exception_preserved_tb_10 );
    // Re-raise.
    exception_type = exception_keeper_type_39;
    exception_value = exception_keeper_value_39;
    exception_tb = exception_keeper_tb_39;
    exception_lineno = exception_keeper_lineno_39;

    goto try_except_handler_39;
    // End of try:
    try_end_39:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_10, exception_preserved_value_10, exception_preserved_tb_10 );
    goto try_end_38;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$core$config_init );
    return MOD_RETURN_VALUE( NULL );
    // End of try:
    try_end_38:;
    goto try_end_40;
    // Exception handler code:
    try_except_handler_39:;
    exception_keeper_type_40 = exception_type;
    exception_keeper_value_40 = exception_value;
    exception_keeper_tb_40 = exception_tb;
    exception_keeper_lineno_40 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    tmp_compare_left_29 = tmp_with_10__indicator;

    CHECK_OBJECT( tmp_compare_left_29 );
    tmp_compare_right_29 = Py_True;
    tmp_is_19 = ( tmp_compare_left_29 == tmp_compare_right_29 );
    if ( tmp_is_19 )
    {
        goto branch_yes_40;
    }
    else
    {
        goto branch_no_40;
    }
    branch_yes_40:;
    tmp_called_name_104 = tmp_with_10__exit;

    CHECK_OBJECT( tmp_called_name_104 );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 476;
    tmp_unused = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_104, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

        Py_DECREF( exception_keeper_type_40 );
        Py_XDECREF( exception_keeper_value_40 );
        Py_XDECREF( exception_keeper_tb_40 );

        exception_lineno = 476;

        goto try_except_handler_38;
    }
    Py_DECREF( tmp_unused );
    branch_no_40:;
    // Re-raise.
    exception_type = exception_keeper_type_40;
    exception_value = exception_keeper_value_40;
    exception_tb = exception_keeper_tb_40;
    exception_lineno = exception_keeper_lineno_40;

    goto try_except_handler_38;
    // End of try:
    try_end_40:;
    tmp_compare_left_30 = tmp_with_10__indicator;

    CHECK_OBJECT( tmp_compare_left_30 );
    tmp_compare_right_30 = Py_True;
    tmp_is_20 = ( tmp_compare_left_30 == tmp_compare_right_30 );
    if ( tmp_is_20 )
    {
        goto branch_yes_41;
    }
    else
    {
        goto branch_no_41;
    }
    branch_yes_41:;
    tmp_called_name_105 = tmp_with_10__exit;

    CHECK_OBJECT( tmp_called_name_105 );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 476;
    tmp_unused = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_105, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 476;

        goto try_except_handler_38;
    }
    Py_DECREF( tmp_unused );
    branch_no_41:;
    goto try_end_41;
    // Exception handler code:
    try_except_handler_38:;
    exception_keeper_type_41 = exception_type;
    exception_keeper_value_41 = exception_value;
    exception_keeper_tb_41 = exception_tb;
    exception_keeper_lineno_41 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_with_10__source );
    tmp_with_10__source = NULL;

    Py_XDECREF( tmp_with_10__enter );
    tmp_with_10__enter = NULL;

    Py_XDECREF( tmp_with_10__exit );
    tmp_with_10__exit = NULL;

    Py_XDECREF( tmp_with_10__indicator );
    tmp_with_10__indicator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_41;
    exception_value = exception_keeper_value_41;
    exception_tb = exception_keeper_tb_41;
    exception_lineno = exception_keeper_lineno_41;

    goto frame_exception_exit_1;
    // End of try:
    try_end_41:;
    CHECK_OBJECT( (PyObject *)tmp_with_10__source );
    Py_DECREF( tmp_with_10__source );
    tmp_with_10__source = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_10__enter );
    Py_DECREF( tmp_with_10__enter );
    tmp_with_10__enter = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_10__exit );
    Py_DECREF( tmp_with_10__exit );
    tmp_with_10__exit = NULL;

    Py_XDECREF( tmp_with_10__indicator );
    tmp_with_10__indicator = NULL;

    tmp_assign_source_117 = const_str_digest_b4fd9bef99574cf727e466922f2dc8a0;
    UPDATE_STRING_DICT0( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_register_converter_doc, tmp_assign_source_117 );
    tmp_assign_source_118 = MAKE_FUNCTION_pandas$core$config_init$$$function_5_register_converter_cb(  );
    UPDATE_STRING_DICT1( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_register_converter_cb, tmp_assign_source_118 );
    // Tried code:
    tmp_called_instance_11 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_called_instance_11 == NULL ))
    {
        tmp_called_instance_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_called_instance_11 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 503;

        goto try_except_handler_42;
    }

    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 503;
    tmp_assign_source_119 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_11, const_str_plain_config_prefix, &PyTuple_GET_ITEM( const_tuple_str_digest_3c0f1fdaa71bdfe9f292a45ca9e89145_tuple, 0 ) );

    if ( tmp_assign_source_119 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 503;

        goto try_except_handler_42;
    }
    assert( tmp_with_11__source == NULL );
    tmp_with_11__source = tmp_assign_source_119;

    tmp_source_name_74 = tmp_with_11__source;

    CHECK_OBJECT( tmp_source_name_74 );
    tmp_called_name_106 = LOOKUP_SPECIAL( tmp_source_name_74, const_str_plain___enter__ );
    if ( tmp_called_name_106 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 503;

        goto try_except_handler_42;
    }
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 503;
    tmp_assign_source_120 = CALL_FUNCTION_NO_ARGS( tmp_called_name_106 );
    Py_DECREF( tmp_called_name_106 );
    if ( tmp_assign_source_120 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 503;

        goto try_except_handler_42;
    }
    assert( tmp_with_11__enter == NULL );
    tmp_with_11__enter = tmp_assign_source_120;

    tmp_source_name_75 = tmp_with_11__source;

    CHECK_OBJECT( tmp_source_name_75 );
    tmp_assign_source_121 = LOOKUP_SPECIAL( tmp_source_name_75, const_str_plain___exit__ );
    if ( tmp_assign_source_121 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 503;

        goto try_except_handler_42;
    }
    assert( tmp_with_11__exit == NULL );
    tmp_with_11__exit = tmp_assign_source_121;

    tmp_assign_source_122 = Py_True;
    assert( tmp_with_11__indicator == NULL );
    Py_INCREF( tmp_assign_source_122 );
    tmp_with_11__indicator = tmp_assign_source_122;

    // Tried code:
    // Tried code:
    tmp_source_name_76 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_cf );

    if (unlikely( tmp_source_name_76 == NULL ))
    {
        tmp_source_name_76 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cf );
    }

    if ( tmp_source_name_76 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cf" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 504;

        goto try_except_handler_44;
    }

    tmp_called_name_107 = LOOKUP_ATTRIBUTE( tmp_source_name_76, const_str_plain_register_option );
    if ( tmp_called_name_107 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 504;

        goto try_except_handler_44;
    }
    tmp_tuple_element_41 = const_str_plain_register_converters;
    tmp_args_name_44 = PyTuple_New( 3 );
    Py_INCREF( tmp_tuple_element_41 );
    PyTuple_SET_ITEM( tmp_args_name_44, 0, tmp_tuple_element_41 );
    tmp_tuple_element_41 = Py_True;
    Py_INCREF( tmp_tuple_element_41 );
    PyTuple_SET_ITEM( tmp_args_name_44, 1, tmp_tuple_element_41 );
    tmp_tuple_element_41 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_register_converter_doc );

    if (unlikely( tmp_tuple_element_41 == NULL ))
    {
        tmp_tuple_element_41 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register_converter_doc );
    }

    if ( tmp_tuple_element_41 == NULL )
    {
        Py_DECREF( tmp_called_name_107 );
        Py_DECREF( tmp_args_name_44 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register_converter_doc" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 504;

        goto try_except_handler_44;
    }

    Py_INCREF( tmp_tuple_element_41 );
    PyTuple_SET_ITEM( tmp_args_name_44, 2, tmp_tuple_element_41 );
    tmp_dict_key_52 = const_str_plain_validator;
    tmp_dict_value_52 = (PyObject *)&PyBool_Type;
    tmp_kw_name_47 = _PyDict_NewPresized( 2 );
    tmp_res = PyDict_SetItem( tmp_kw_name_47, tmp_dict_key_52, tmp_dict_value_52 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_53 = const_str_plain_cb;
    tmp_dict_value_53 = GET_STRING_DICT_VALUE( moduledict_pandas$core$config_init, (Nuitka_StringObject *)const_str_plain_register_converter_cb );

    if (unlikely( tmp_dict_value_53 == NULL ))
    {
        tmp_dict_value_53 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register_converter_cb );
    }

    if ( tmp_dict_value_53 == NULL )
    {
        Py_DECREF( tmp_called_name_107 );
        Py_DECREF( tmp_args_name_44 );
        Py_DECREF( tmp_kw_name_47 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register_converter_cb" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 505;

        goto try_except_handler_44;
    }

    tmp_res = PyDict_SetItem( tmp_kw_name_47, tmp_dict_key_53, tmp_dict_value_53 );
    assert( !(tmp_res != 0) );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 504;
    tmp_unused = CALL_FUNCTION( tmp_called_name_107, tmp_args_name_44, tmp_kw_name_47 );
    Py_DECREF( tmp_called_name_107 );
    Py_DECREF( tmp_args_name_44 );
    Py_DECREF( tmp_kw_name_47 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 504;

        goto try_except_handler_44;
    }
    Py_DECREF( tmp_unused );
    goto try_end_42;
    // Exception handler code:
    try_except_handler_44:;
    exception_keeper_type_42 = exception_type;
    exception_keeper_value_42 = exception_value;
    exception_keeper_tb_42 = exception_tb;
    exception_keeper_lineno_42 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_11 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_11 );
    exception_preserved_value_11 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_11 );
    exception_preserved_tb_11 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_11 );

    if ( exception_keeper_tb_42 == NULL )
    {
        exception_keeper_tb_42 = MAKE_TRACEBACK( frame_3209efbe1fc5d2857a97a7e36ce015f8, exception_keeper_lineno_42 );
    }
    else if ( exception_keeper_lineno_42 != 0 )
    {
        exception_keeper_tb_42 = ADD_TRACEBACK( exception_keeper_tb_42, frame_3209efbe1fc5d2857a97a7e36ce015f8, exception_keeper_lineno_42 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_42, &exception_keeper_value_42, &exception_keeper_tb_42 );
    PyException_SetTraceback( exception_keeper_value_42, (PyObject *)exception_keeper_tb_42 );
    PUBLISH_EXCEPTION( &exception_keeper_type_42, &exception_keeper_value_42, &exception_keeper_tb_42 );
    // Tried code:
    tmp_compare_left_31 = EXC_TYPE(PyThreadState_GET());
    tmp_compare_right_31 = PyExc_BaseException;
    tmp_exc_match_exception_match_11 = EXCEPTION_MATCH_BOOL( tmp_compare_left_31, tmp_compare_right_31 );
    if ( tmp_exc_match_exception_match_11 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 503;

        goto try_except_handler_45;
    }
    if ( tmp_exc_match_exception_match_11 == 1 )
    {
        goto branch_yes_42;
    }
    else
    {
        goto branch_no_42;
    }
    branch_yes_42:;
    tmp_assign_source_123 = Py_False;
    {
        PyObject *old = tmp_with_11__indicator;
        assert( old != NULL );
        tmp_with_11__indicator = tmp_assign_source_123;
        Py_INCREF( tmp_with_11__indicator );
        Py_DECREF( old );
    }

    tmp_called_name_108 = tmp_with_11__exit;

    CHECK_OBJECT( tmp_called_name_108 );
    tmp_args_element_name_47 = EXC_TYPE(PyThreadState_GET());
    tmp_args_element_name_48 = EXC_VALUE(PyThreadState_GET());
    tmp_args_element_name_49 = EXC_TRACEBACK(PyThreadState_GET());
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 503;
    {
        PyObject *call_args[] = { tmp_args_element_name_47, tmp_args_element_name_48, tmp_args_element_name_49 };
        tmp_cond_value_12 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_108, call_args );
    }

    if ( tmp_cond_value_12 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 503;

        goto try_except_handler_45;
    }
    tmp_cond_truth_12 = CHECK_IF_TRUE( tmp_cond_value_12 );
    if ( tmp_cond_truth_12 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_cond_value_12 );

        exception_lineno = 503;

        goto try_except_handler_45;
    }
    Py_DECREF( tmp_cond_value_12 );
    if ( tmp_cond_truth_12 == 1 )
    {
        goto branch_no_43;
    }
    else
    {
        goto branch_yes_43;
    }
    branch_yes_43:;
    tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (unlikely( tmp_result == false ))
    {
        exception_lineno = 503;
    }

    if (exception_tb && exception_tb->tb_frame == &frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame) frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = exception_tb->tb_lineno;

    goto try_except_handler_45;
    branch_no_43:;
    goto branch_end_42;
    branch_no_42:;
    tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (unlikely( tmp_result == false ))
    {
        exception_lineno = 503;
    }

    if (exception_tb && exception_tb->tb_frame == &frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame) frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = exception_tb->tb_lineno;

    goto try_except_handler_45;
    branch_end_42:;
    goto try_end_43;
    // Exception handler code:
    try_except_handler_45:;
    exception_keeper_type_43 = exception_type;
    exception_keeper_value_43 = exception_value;
    exception_keeper_tb_43 = exception_tb;
    exception_keeper_lineno_43 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_11, exception_preserved_value_11, exception_preserved_tb_11 );
    // Re-raise.
    exception_type = exception_keeper_type_43;
    exception_value = exception_keeper_value_43;
    exception_tb = exception_keeper_tb_43;
    exception_lineno = exception_keeper_lineno_43;

    goto try_except_handler_43;
    // End of try:
    try_end_43:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_11, exception_preserved_value_11, exception_preserved_tb_11 );
    goto try_end_42;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$core$config_init );
    return MOD_RETURN_VALUE( NULL );
    // End of try:
    try_end_42:;
    goto try_end_44;
    // Exception handler code:
    try_except_handler_43:;
    exception_keeper_type_44 = exception_type;
    exception_keeper_value_44 = exception_value;
    exception_keeper_tb_44 = exception_tb;
    exception_keeper_lineno_44 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    tmp_compare_left_32 = tmp_with_11__indicator;

    CHECK_OBJECT( tmp_compare_left_32 );
    tmp_compare_right_32 = Py_True;
    tmp_is_21 = ( tmp_compare_left_32 == tmp_compare_right_32 );
    if ( tmp_is_21 )
    {
        goto branch_yes_44;
    }
    else
    {
        goto branch_no_44;
    }
    branch_yes_44:;
    tmp_called_name_109 = tmp_with_11__exit;

    CHECK_OBJECT( tmp_called_name_109 );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 503;
    tmp_unused = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_109, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

        Py_DECREF( exception_keeper_type_44 );
        Py_XDECREF( exception_keeper_value_44 );
        Py_XDECREF( exception_keeper_tb_44 );

        exception_lineno = 503;

        goto try_except_handler_42;
    }
    Py_DECREF( tmp_unused );
    branch_no_44:;
    // Re-raise.
    exception_type = exception_keeper_type_44;
    exception_value = exception_keeper_value_44;
    exception_tb = exception_keeper_tb_44;
    exception_lineno = exception_keeper_lineno_44;

    goto try_except_handler_42;
    // End of try:
    try_end_44:;
    tmp_compare_left_33 = tmp_with_11__indicator;

    CHECK_OBJECT( tmp_compare_left_33 );
    tmp_compare_right_33 = Py_True;
    tmp_is_22 = ( tmp_compare_left_33 == tmp_compare_right_33 );
    if ( tmp_is_22 )
    {
        goto branch_yes_45;
    }
    else
    {
        goto branch_no_45;
    }
    branch_yes_45:;
    tmp_called_name_110 = tmp_with_11__exit;

    CHECK_OBJECT( tmp_called_name_110 );
    frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame.f_lineno = 503;
    tmp_unused = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_110, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 503;

        goto try_except_handler_42;
    }
    Py_DECREF( tmp_unused );
    branch_no_45:;
    goto try_end_45;
    // Exception handler code:
    try_except_handler_42:;
    exception_keeper_type_45 = exception_type;
    exception_keeper_value_45 = exception_value;
    exception_keeper_tb_45 = exception_tb;
    exception_keeper_lineno_45 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_with_11__source );
    tmp_with_11__source = NULL;

    Py_XDECREF( tmp_with_11__enter );
    tmp_with_11__enter = NULL;

    Py_XDECREF( tmp_with_11__exit );
    tmp_with_11__exit = NULL;

    Py_XDECREF( tmp_with_11__indicator );
    tmp_with_11__indicator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_45;
    exception_value = exception_keeper_value_45;
    exception_tb = exception_keeper_tb_45;
    exception_lineno = exception_keeper_lineno_45;

    goto frame_exception_exit_1;
    // End of try:
    try_end_45:;

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_3209efbe1fc5d2857a97a7e36ce015f8 );
#endif
    popFrameStack();

    assertFrameObject( frame_3209efbe1fc5d2857a97a7e36ce015f8 );

    goto frame_no_exception_1;
    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_3209efbe1fc5d2857a97a7e36ce015f8 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_3209efbe1fc5d2857a97a7e36ce015f8, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_3209efbe1fc5d2857a97a7e36ce015f8->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_3209efbe1fc5d2857a97a7e36ce015f8, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;
    frame_no_exception_1:;
    CHECK_OBJECT( (PyObject *)tmp_with_11__source );
    Py_DECREF( tmp_with_11__source );
    tmp_with_11__source = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_11__enter );
    Py_DECREF( tmp_with_11__enter );
    tmp_with_11__enter = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_11__exit );
    Py_DECREF( tmp_with_11__exit );
    tmp_with_11__exit = NULL;

    Py_XDECREF( tmp_with_11__indicator );
    tmp_with_11__indicator = NULL;


    return MOD_RETURN_VALUE( module_pandas$core$config_init );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
