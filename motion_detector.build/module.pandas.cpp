/* Generated code for Python source for module 'pandas'
 * created by Nuitka version 0.5.32.7
 *
 * This code is in part copyright 2018 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The _module_pandas is a Python object pointer of module type. */

/* Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_pandas;
PyDictObject *moduledict_pandas;

/* The module constants used, if any. */
static PyObject *const_tuple_str_digest_f28ac297014eae6038f099e88653df5d_str_empty_tuple;
extern PyObject *const_str_plain_test;
extern PyObject *const_str_plain_OutOfBoundsDatetime;
static PyObject *const_str_digest_4434d1336485b8c1f6633b23d2f67268;
static PyObject *const_tuple_str_plain_get_versions_tuple;
static PyObject *const_str_plain__style;
extern PyObject *const_str_plain___file__;
extern PyObject *const_str_plain_show_versions;
extern PyObject *const_str_digest_ea38e05af30aa1be8abd26f39d496acd;
static PyObject *const_str_digest_3d70eba0bba99ecdbd73a639512d8c59;
static PyObject *const_dict_83e7e19db27cfb349e8c76119d1bd0c6;
extern PyObject *const_tuple_str_chr_42_tuple;
extern PyObject *const_str_digest_6c6930528997e29cd62f219edf616ec7;
static PyObject *const_list_str_plain_na_values_list;
extern PyObject *const_str_digest_71757cb1d3ba810942677ab709005d4f;
extern PyObject *const_str_plain_json;
extern PyObject *const_str_plain_CParserError;
extern PyObject *const_str_chr_42;
extern PyObject *const_str_digest_85d0cfed91b588c9de3b494ad496b19c;
extern PyObject *const_str_plain_dateutil;
static PyObject *const_str_plain__tslib;
extern PyObject *const_str_plain___docformat__;
static PyObject *const_tuple_str_plain_numpy_str_plain_pytz_str_plain_dateutil_tuple;
extern PyObject *const_str_plain_plotting;
extern PyObject *const_str_digest_9bdf56c59c2f38d290ad60074cc357bc;
static PyObject *const_str_digest_0c8307d26eda1e1e7b270540cfbf10c4;
static PyObject *const_dict_c3779dfd0af6b2c8a7b47594aab3d3fc;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_plain___debug__;
static PyObject *const_str_digest_8a761d4910c7a914e8b9921226d204bd;
static PyObject *const_str_digest_40af7bdd1c7ab3ebd9f8e541abe1952c;
extern PyObject *const_str_digest_be2be1c115fc8af588b089f14adace08;
extern PyObject *const_str_digest_6ea18b76b9b72ed68e11aa82f9d4700e;
extern PyObject *const_str_plain_numpy;
static PyObject *const_str_plain_NaTType;
extern PyObject *const_str_plain_removals;
extern PyObject *const_str_plain_get_versions;
extern PyObject *const_str_plain__version;
extern PyObject *const_str_plain_infer_dtype;
static PyObject *const_str_digest_f924e77d0414d8bd9040c39451a10cbb;
extern PyObject *const_str_plain_na_values;
extern PyObject *const_str_digest_de1e82e029cb529be78cb83e0ad934c6;
static PyObject *const_str_digest_8d7d0cc0a53fbbf03b000db28fcbba76;
extern PyObject *const_str_plain_e;
extern PyObject *const_str_digest_7c9fbc1534b9d3e77d7ca1a84c272912;
static PyObject *const_tuple_str_plain_test_tuple;
extern PyObject *const_str_plain___path__;
extern PyObject *const_tuple_empty;
extern PyObject *const_tuple_str_plain__DeprecatedModule_tuple;
static PyObject *const_str_plain_dependency;
extern PyObject *const_str_plain_append;
extern PyObject *const_str_plain_datetime;
static PyObject *const_str_digest_9bbde6adbac89f60dff074367150551b;
extern PyObject *const_str_plain_pandas;
extern PyObject *const_str_plain_deprmodto;
extern PyObject *const_str_plain_parser;
extern PyObject *const_str_digest_a28def792293bb1bd13fa79bcde016ba;
static PyObject *const_str_plain_util;
static PyObject *const_str_digest_37861f83a3d31c67db7185e2a9bff43c;
extern PyObject *const_str_plain_restructuredtext;
static PyObject *const_str_digest_f37e0c624e82ce7ff72eee147cf2498c;
extern PyObject *const_str_plain_Timestamp;
static PyObject *const_str_digest_33c7028da0e5c1aff488a0beed24177c;
static PyObject *const_str_digest_4971be992dcede15255d25f725b4edbf;
extern PyObject *const_str_plain_False;
extern PyObject *const_str_plain_NaT;
static PyObject *const_str_digest_bd6e17405698c04366d70c9517b40844;
extern PyObject *const_str_plain_hashtable;
extern PyObject *const_str_digest_da95bebd2524b65728827a9e6d1504b8;
static PyObject *const_dict_1b934c4fa06710fba00933c689ea2ea9;
extern PyObject *const_str_plain_lib;
extern PyObject *const_str_plain___version__;
static PyObject *const_str_digest_834965eea13d260b22602595ea1d800e;
extern PyObject *const_int_0;
extern PyObject *const_str_plain_dumps;
static PyObject *const_list_str_digest_e395eb7dec6e5461fe8fada8d882e8ef_list;
extern PyObject *const_str_plain_Timedelta;
extern PyObject *const_str_plain_plot_params;
extern PyObject *const_str_plain_moved;
extern PyObject *const_str_digest_3d595a6e00e4e48b6d61c247ac7fc89f;
static PyObject *const_str_digest_aef36402f3427b74d0131f6acf113f39;
extern PyObject *const_str_plain__Options;
static PyObject *const_dict_0b0fd9fa9d2285f121210f2e2fa054c8;
static PyObject *const_str_digest_e395eb7dec6e5461fe8fada8d882e8ef;
extern PyObject *const_str_digest_48ae02d7bc3fc61cbffb3f46dd54fdfa;
static PyObject *const_str_digest_3de6af4c8e90b337081e62910d7cba33;
extern PyObject *const_str_digest_3a0b927a1532ded681dd49b7ca17edfe;
extern PyObject *const_str_plain_deprecated;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_plain_v;
extern PyObject *const_str_plain_deprmod;
static PyObject *const_str_plain__decorators;
static PyObject *const_str_plain__lib;
extern PyObject *const_str_plain_deprecate;
static PyObject *const_str_digest_f28ac297014eae6038f099e88653df5d;
static PyObject *const_dict_02267e27c2a1e87a85089b124a532f7d;
static PyObject *const_str_digest_e3e2c8aec631b1501b2ec33fc76e8c92;
static PyObject *const_str_digest_66558fed4e5cc9dff7d7eed9b7d90a33;
extern PyObject *const_str_plain_pytz;
extern PyObject *const_int_pos_1;
extern PyObject *const_str_plain_replace;
extern PyObject *const_str_plain__DeprecatedModule;
static PyObject *const_dict_7e4518cc1d7a198ed98c1605c8646c8a;
static PyObject *const_str_plain_missing_dependencies;
static PyObject *const_dict_d1888f56254f4c4bee106f491bc4a800;
extern PyObject *const_str_plain_tslib;
static PyObject *const_str_digest_aeef4a412ee9ab3321442889853d64ce;
static PyObject *const_dict_20025cb31fdfb3b6919a061eca418fd9;
static PyObject *const_dict_140920105d15f4c6f953ce2fe8a1a576;
extern PyObject *const_str_plain_module;
extern PyObject *const_str_digest_3fa11e71aa92f39879464952ab264ea1;
static PyObject *const_tuple_str_plain_show_versions_tuple;
extern PyObject *const_str_plain_loads;
extern PyObject *const_str_plain_get;
static PyObject *const_str_digest_83b4c9d8cc96123ca57e5949b041d9d9;
extern PyObject *const_str_plain_version;
static PyObject *const_tuple_str_plain_hashtable_str_plain_lib_str_plain_tslib_tuple;
extern PyObject *const_str_plain_format;
extern PyObject *const_str_empty;
extern PyObject *const_tuple_str_plain_datetime_tuple;
static PyObject *const_str_digest_059bcf492d979b203cf5a6691794a0b7;
static PyObject *const_str_plain__hashtable;
static PyObject *const_str_plain_hard_dependencies;
extern PyObject *const_str_plain_scatter_matrix;
static PyObject *module_filename_obj;

static bool constants_created = false;

static void createModuleConstants( void )
{
    const_tuple_str_digest_f28ac297014eae6038f099e88653df5d_str_empty_tuple = PyTuple_New( 2 );
    const_str_digest_f28ac297014eae6038f099e88653df5d = UNSTREAM_STRING( &constant_bin[ 1693707 ], 19, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_f28ac297014eae6038f099e88653df5d_str_empty_tuple, 0, const_str_digest_f28ac297014eae6038f099e88653df5d ); Py_INCREF( const_str_digest_f28ac297014eae6038f099e88653df5d );
    PyTuple_SET_ITEM( const_tuple_str_digest_f28ac297014eae6038f099e88653df5d_str_empty_tuple, 1, const_str_empty ); Py_INCREF( const_str_empty );
    const_str_digest_4434d1336485b8c1f6633b23d2f67268 = UNSTREAM_STRING( &constant_bin[ 1693726 ], 10, 0 );
    const_tuple_str_plain_get_versions_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_get_versions_tuple, 0, const_str_plain_get_versions ); Py_INCREF( const_str_plain_get_versions );
    const_str_plain__style = UNSTREAM_STRING( &constant_bin[ 1693736 ], 6, 1 );
    const_str_digest_3d70eba0bba99ecdbd73a639512d8c59 = UNSTREAM_STRING( &constant_bin[ 1693742 ], 13, 0 );
    const_dict_83e7e19db27cfb349e8c76119d1bd0c6 = _PyDict_NewPresized( 2 );
    const_str_digest_33c7028da0e5c1aff488a0beed24177c = UNSTREAM_STRING( &constant_bin[ 1693755 ], 11, 0 );
    PyDict_SetItem( const_dict_83e7e19db27cfb349e8c76119d1bd0c6, const_str_plain_deprmod, const_str_digest_33c7028da0e5c1aff488a0beed24177c );
    const_dict_c3779dfd0af6b2c8a7b47594aab3d3fc = _PyDict_NewPresized( 2 );
    const_str_digest_83b4c9d8cc96123ca57e5949b041d9d9 = UNSTREAM_STRING( &constant_bin[ 1693766 ], 20, 0 );
    PyDict_SetItem( const_dict_c3779dfd0af6b2c8a7b47594aab3d3fc, const_str_plain_dumps, const_str_digest_83b4c9d8cc96123ca57e5949b041d9d9 );
    const_str_digest_8d7d0cc0a53fbbf03b000db28fcbba76 = UNSTREAM_STRING( &constant_bin[ 1693786 ], 20, 0 );
    PyDict_SetItem( const_dict_c3779dfd0af6b2c8a7b47594aab3d3fc, const_str_plain_loads, const_str_digest_8d7d0cc0a53fbbf03b000db28fcbba76 );
    assert( PyDict_Size( const_dict_c3779dfd0af6b2c8a7b47594aab3d3fc ) == 2 );
    PyDict_SetItem( const_dict_83e7e19db27cfb349e8c76119d1bd0c6, const_str_plain_moved, const_dict_c3779dfd0af6b2c8a7b47594aab3d3fc );
    assert( PyDict_Size( const_dict_83e7e19db27cfb349e8c76119d1bd0c6 ) == 2 );
    const_list_str_plain_na_values_list = PyList_New( 1 );
    PyList_SET_ITEM( const_list_str_plain_na_values_list, 0, const_str_plain_na_values ); Py_INCREF( const_str_plain_na_values );
    const_str_plain__tslib = UNSTREAM_STRING( &constant_bin[ 1693806 ], 6, 1 );
    const_tuple_str_plain_numpy_str_plain_pytz_str_plain_dateutil_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_numpy_str_plain_pytz_str_plain_dateutil_tuple, 0, const_str_plain_numpy ); Py_INCREF( const_str_plain_numpy );
    PyTuple_SET_ITEM( const_tuple_str_plain_numpy_str_plain_pytz_str_plain_dateutil_tuple, 1, const_str_plain_pytz ); Py_INCREF( const_str_plain_pytz );
    PyTuple_SET_ITEM( const_tuple_str_plain_numpy_str_plain_pytz_str_plain_dateutil_tuple, 2, const_str_plain_dateutil ); Py_INCREF( const_str_plain_dateutil );
    const_str_digest_0c8307d26eda1e1e7b270540cfbf10c4 = UNSTREAM_STRING( &constant_bin[ 1693812 ], 12, 0 );
    const_str_digest_8a761d4910c7a914e8b9921226d204bd = UNSTREAM_STRING( &constant_bin[ 1693824 ], 25, 0 );
    const_str_digest_40af7bdd1c7ab3ebd9f8e541abe1952c = UNSTREAM_STRING( &constant_bin[ 1693849 ], 182, 0 );
    const_str_plain_NaTType = UNSTREAM_STRING( &constant_bin[ 1694031 ], 7, 1 );
    const_str_digest_f924e77d0414d8bd9040c39451a10cbb = UNSTREAM_STRING( &constant_bin[ 1694038 ], 28, 0 );
    const_tuple_str_plain_test_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_test_tuple, 0, const_str_plain_test ); Py_INCREF( const_str_plain_test );
    const_str_plain_dependency = UNSTREAM_STRING( &constant_bin[ 1694066 ], 10, 1 );
    const_str_digest_9bbde6adbac89f60dff074367150551b = UNSTREAM_STRING( &constant_bin[ 1694076 ], 16, 0 );
    const_str_plain_util = UNSTREAM_STRING( &constant_bin[ 726 ], 4, 1 );
    const_str_digest_37861f83a3d31c67db7185e2a9bff43c = UNSTREAM_STRING( &constant_bin[ 1694092 ], 33, 0 );
    const_str_digest_f37e0c624e82ce7ff72eee147cf2498c = UNSTREAM_STRING( &constant_bin[ 1694081 ], 10, 0 );
    const_str_digest_4971be992dcede15255d25f725b4edbf = UNSTREAM_STRING( &constant_bin[ 1694125 ], 11, 0 );
    const_str_digest_bd6e17405698c04366d70c9517b40844 = UNSTREAM_STRING( &constant_bin[ 1694136 ], 81, 0 );
    const_dict_1b934c4fa06710fba00933c689ea2ea9 = _PyDict_NewPresized( 2 );
    PyDict_SetItem( const_dict_1b934c4fa06710fba00933c689ea2ea9, const_str_plain_deprmod, const_str_digest_0c8307d26eda1e1e7b270540cfbf10c4 );
    const_dict_0b0fd9fa9d2285f121210f2e2fa054c8 = _PyDict_NewPresized( 5 );
    const_str_digest_3de6af4c8e90b337081e62910d7cba33 = UNSTREAM_STRING( &constant_bin[ 1694217 ], 16, 0 );
    PyDict_SetItem( const_dict_0b0fd9fa9d2285f121210f2e2fa054c8, const_str_plain_Timestamp, const_str_digest_3de6af4c8e90b337081e62910d7cba33 );
    const_str_digest_834965eea13d260b22602595ea1d800e = UNSTREAM_STRING( &constant_bin[ 1694233 ], 16, 0 );
    PyDict_SetItem( const_dict_0b0fd9fa9d2285f121210f2e2fa054c8, const_str_plain_Timedelta, const_str_digest_834965eea13d260b22602595ea1d800e );
    PyDict_SetItem( const_dict_0b0fd9fa9d2285f121210f2e2fa054c8, const_str_plain_NaT, const_str_digest_f37e0c624e82ce7ff72eee147cf2498c );
    PyDict_SetItem( const_dict_0b0fd9fa9d2285f121210f2e2fa054c8, const_str_plain_NaTType, const_str_digest_9bbde6adbac89f60dff074367150551b );
    PyDict_SetItem( const_dict_0b0fd9fa9d2285f121210f2e2fa054c8, const_str_plain_OutOfBoundsDatetime, const_str_digest_37861f83a3d31c67db7185e2a9bff43c );
    assert( PyDict_Size( const_dict_0b0fd9fa9d2285f121210f2e2fa054c8 ) == 5 );
    PyDict_SetItem( const_dict_1b934c4fa06710fba00933c689ea2ea9, const_str_plain_moved, const_dict_0b0fd9fa9d2285f121210f2e2fa054c8 );
    assert( PyDict_Size( const_dict_1b934c4fa06710fba00933c689ea2ea9 ) == 2 );
    const_list_str_digest_e395eb7dec6e5461fe8fada8d882e8ef_list = PyList_New( 1 );
    const_str_digest_e395eb7dec6e5461fe8fada8d882e8ef = UNSTREAM_STRING( &constant_bin[ 1694136 ], 69, 0 );
    PyList_SET_ITEM( const_list_str_digest_e395eb7dec6e5461fe8fada8d882e8ef_list, 0, const_str_digest_e395eb7dec6e5461fe8fada8d882e8ef ); Py_INCREF( const_str_digest_e395eb7dec6e5461fe8fada8d882e8ef );
    const_str_digest_aef36402f3427b74d0131f6acf113f39 = UNSTREAM_STRING( &constant_bin[ 1694249 ], 2062, 0 );
    const_str_plain__decorators = UNSTREAM_STRING( &constant_bin[ 1696311 ], 11, 1 );
    const_str_plain__lib = UNSTREAM_STRING( &constant_bin[ 630081 ], 4, 1 );
    const_dict_02267e27c2a1e87a85089b124a532f7d = _PyDict_NewPresized( 1 );
    PyDict_SetItem( const_dict_02267e27c2a1e87a85089b124a532f7d, const_str_plain_CParserError, const_str_digest_8a761d4910c7a914e8b9921226d204bd );
    assert( PyDict_Size( const_dict_02267e27c2a1e87a85089b124a532f7d ) == 1 );
    const_str_digest_e3e2c8aec631b1501b2ec33fc76e8c92 = UNSTREAM_STRING( &constant_bin[ 1696322 ], 33, 0 );
    const_str_digest_66558fed4e5cc9dff7d7eed9b7d90a33 = UNSTREAM_STRING( &constant_bin[ 1696355 ], 15, 0 );
    const_dict_7e4518cc1d7a198ed98c1605c8646c8a = _PyDict_NewPresized( 3 );
    PyDict_SetItem( const_dict_7e4518cc1d7a198ed98c1605c8646c8a, const_str_plain_deprmod, const_str_digest_4434d1336485b8c1f6633b23d2f67268 );
    PyDict_SetItem( const_dict_7e4518cc1d7a198ed98c1605c8646c8a, const_str_plain_deprmodto, Py_False );
    const_dict_d1888f56254f4c4bee106f491bc4a800 = _PyDict_NewPresized( 4 );
    PyDict_SetItem( const_dict_d1888f56254f4c4bee106f491bc4a800, const_str_plain_Timestamp, const_str_digest_3de6af4c8e90b337081e62910d7cba33 );
    PyDict_SetItem( const_dict_d1888f56254f4c4bee106f491bc4a800, const_str_plain_Timedelta, const_str_digest_834965eea13d260b22602595ea1d800e );
    PyDict_SetItem( const_dict_d1888f56254f4c4bee106f491bc4a800, const_str_plain_NaT, const_str_digest_f37e0c624e82ce7ff72eee147cf2498c );
    PyDict_SetItem( const_dict_d1888f56254f4c4bee106f491bc4a800, const_str_plain_infer_dtype, const_str_digest_f924e77d0414d8bd9040c39451a10cbb );
    assert( PyDict_Size( const_dict_d1888f56254f4c4bee106f491bc4a800 ) == 4 );
    PyDict_SetItem( const_dict_7e4518cc1d7a198ed98c1605c8646c8a, const_str_plain_moved, const_dict_d1888f56254f4c4bee106f491bc4a800 );
    assert( PyDict_Size( const_dict_7e4518cc1d7a198ed98c1605c8646c8a ) == 3 );
    const_str_plain_missing_dependencies = UNSTREAM_STRING( &constant_bin[ 1696370 ], 20, 1 );
    const_str_digest_aeef4a412ee9ab3321442889853d64ce = UNSTREAM_STRING( &constant_bin[ 1696390 ], 30, 0 );
    const_dict_20025cb31fdfb3b6919a061eca418fd9 = _PyDict_NewPresized( 3 );
    PyDict_SetItem( const_dict_20025cb31fdfb3b6919a061eca418fd9, const_str_plain_deprmod, const_str_digest_3d70eba0bba99ecdbd73a639512d8c59 );
    PyDict_SetItem( const_dict_20025cb31fdfb3b6919a061eca418fd9, const_str_plain_removals, const_list_str_plain_na_values_list );
    PyDict_SetItem( const_dict_20025cb31fdfb3b6919a061eca418fd9, const_str_plain_moved, const_dict_02267e27c2a1e87a85089b124a532f7d );
    assert( PyDict_Size( const_dict_20025cb31fdfb3b6919a061eca418fd9 ) == 3 );
    const_dict_140920105d15f4c6f953ce2fe8a1a576 = _PyDict_NewPresized( 1 );
    PyDict_SetItem( const_dict_140920105d15f4c6f953ce2fe8a1a576, const_str_plain_deprecated, Py_True );
    assert( PyDict_Size( const_dict_140920105d15f4c6f953ce2fe8a1a576 ) == 1 );
    const_tuple_str_plain_show_versions_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_show_versions_tuple, 0, const_str_plain_show_versions ); Py_INCREF( const_str_plain_show_versions );
    const_tuple_str_plain_hashtable_str_plain_lib_str_plain_tslib_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_hashtable_str_plain_lib_str_plain_tslib_tuple, 0, const_str_plain_hashtable ); Py_INCREF( const_str_plain_hashtable );
    PyTuple_SET_ITEM( const_tuple_str_plain_hashtable_str_plain_lib_str_plain_tslib_tuple, 1, const_str_plain_lib ); Py_INCREF( const_str_plain_lib );
    PyTuple_SET_ITEM( const_tuple_str_plain_hashtable_str_plain_lib_str_plain_tslib_tuple, 2, const_str_plain_tslib ); Py_INCREF( const_str_plain_tslib );
    const_str_digest_059bcf492d979b203cf5a6691794a0b7 = UNSTREAM_STRING( &constant_bin[ 1696420 ], 21, 0 );
    const_str_plain__hashtable = UNSTREAM_STRING( &constant_bin[ 1696441 ], 10, 1 );
    const_str_plain_hard_dependencies = UNSTREAM_STRING( &constant_bin[ 1696451 ], 17, 1 );

    constants_created = true;
}

#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_pandas( void )
{
    // The module may not have been used at all.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_ed0438480de34297226c0f6f8fcf3ee7;

static void createModuleCodeObjects(void)
{
    module_filename_obj = const_str_digest_bd6e17405698c04366d70c9517b40844;
    codeobj_ed0438480de34297226c0f6f8fcf3ee7 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_66558fed4e5cc9dff7d7eed9b7d90a33, 1, const_tuple_empty, 0, 0, CO_NOFREE );
}

// The module function declarations.


// The module function definitions.



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_pandas =
{
    PyModuleDef_HEAD_INIT,
    "pandas",   /* m_name */
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;

extern PyObject *const_str_plain___loader__;
extern PyObject *metapath_based_loader;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL.

MOD_INIT_DECL( pandas )
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_pandas );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("pandas: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("pandas: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("pandas: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initpandas" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_pandas = Py_InitModule4(
        "pandas",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_pandas = PyModule_Create( &mdef_pandas );
#endif

    moduledict_pandas = MODULE_DICT( module_pandas );

    // Update "__package__" value to what it ought to be.
    {
#if 1
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_pandas, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_pandas,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );

#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_pandas, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_pandas,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_pandas, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_pandas,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_pandas );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PySys_GetObject( (char *)"modules" ), const_str_plain_pandas, module_pandas );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_pandas, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_pandas, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_pandas, (Nuitka_StringObject *)const_str_plain___loader__, metapath_based_loader );
#endif

#if PYTHON_VERSION >= 340
#if 0
    UPDATE_STRING_DICT0( moduledict_pandas, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_pandas, (Nuitka_StringObject *)const_str_plain___name__ ),
            metapath_based_loader
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );

        UPDATE_STRING_DICT1( moduledict_pandas, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );

        Py_DECREF( module_spec_class );
    }
#endif
#endif


    // Temp variables if any
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_import_from_1__module = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_9;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    PyObject *exception_preserved_type_2;
    PyObject *exception_preserved_value_2;
    PyTracebackObject *exception_preserved_tb_2;
    PyObject *tmp_args_element_name_1;
    PyObject *tmp_args_element_name_2;
    PyObject *tmp_args_element_name_3;
    PyObject *tmp_args_element_name_4;
    PyObject *tmp_args_element_name_5;
    PyObject *tmp_args_element_name_6;
    PyObject *tmp_args_element_name_7;
    PyObject *tmp_args_element_name_8;
    PyObject *tmp_args_element_name_9;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_assign_source_5;
    PyObject *tmp_assign_source_6;
    PyObject *tmp_assign_source_7;
    PyObject *tmp_assign_source_8;
    PyObject *tmp_assign_source_9;
    PyObject *tmp_assign_source_10;
    PyObject *tmp_assign_source_11;
    PyObject *tmp_assign_source_12;
    PyObject *tmp_assign_source_13;
    PyObject *tmp_assign_source_14;
    PyObject *tmp_assign_source_15;
    PyObject *tmp_assign_source_16;
    PyObject *tmp_assign_source_17;
    PyObject *tmp_assign_source_18;
    PyObject *tmp_assign_source_19;
    PyObject *tmp_assign_source_20;
    PyObject *tmp_assign_source_21;
    PyObject *tmp_assign_source_22;
    PyObject *tmp_assign_source_23;
    PyObject *tmp_assign_source_24;
    PyObject *tmp_assign_source_25;
    PyObject *tmp_assign_source_26;
    PyObject *tmp_assign_source_27;
    PyObject *tmp_assign_source_28;
    PyObject *tmp_assign_source_29;
    PyObject *tmp_assign_source_30;
    PyObject *tmp_assign_source_31;
    PyObject *tmp_assign_source_32;
    PyObject *tmp_assign_source_33;
    PyObject *tmp_assign_source_34;
    PyObject *tmp_called_instance_1;
    PyObject *tmp_called_instance_2;
    PyObject *tmp_called_name_1;
    PyObject *tmp_called_name_2;
    PyObject *tmp_called_name_3;
    PyObject *tmp_called_name_4;
    PyObject *tmp_called_name_5;
    PyObject *tmp_called_name_6;
    PyObject *tmp_called_name_7;
    PyObject *tmp_called_name_8;
    PyObject *tmp_called_name_9;
    PyObject *tmp_called_name_10;
    PyObject *tmp_compare_left_1;
    PyObject *tmp_compare_left_2;
    PyObject *tmp_compare_left_3;
    PyObject *tmp_compare_right_1;
    PyObject *tmp_compare_right_2;
    PyObject *tmp_compare_right_3;
    int tmp_cond_truth_1;
    PyObject *tmp_cond_value_1;
    int tmp_exc_match_exception_match_1;
    int tmp_exc_match_exception_match_2;
    int tmp_exc_match_exception_match_3;
    PyObject *tmp_fromlist_name_2;
    PyObject *tmp_fromlist_name_3;
    PyObject *tmp_fromlist_name_4;
    PyObject *tmp_fromlist_name_5;
    PyObject *tmp_fromlist_name_6;
    PyObject *tmp_fromlist_name_7;
    PyObject *tmp_fromlist_name_8;
    PyObject *tmp_fromlist_name_9;
    PyObject *tmp_fromlist_name_10;
    PyObject *tmp_fromlist_name_11;
    PyObject *tmp_fromlist_name_12;
    PyObject *tmp_fromlist_name_13;
    PyObject *tmp_fromlist_name_14;
    PyObject *tmp_fromlist_name_15;
    PyObject *tmp_fromlist_name_16;
    PyObject *tmp_fromlist_name_17;
    PyObject *tmp_globals_name_2;
    PyObject *tmp_globals_name_3;
    PyObject *tmp_globals_name_4;
    PyObject *tmp_globals_name_5;
    PyObject *tmp_globals_name_6;
    PyObject *tmp_globals_name_7;
    PyObject *tmp_globals_name_8;
    PyObject *tmp_globals_name_9;
    PyObject *tmp_globals_name_10;
    PyObject *tmp_globals_name_11;
    PyObject *tmp_globals_name_12;
    PyObject *tmp_globals_name_13;
    PyObject *tmp_globals_name_14;
    PyObject *tmp_globals_name_15;
    PyObject *tmp_globals_name_16;
    PyObject *tmp_globals_name_17;
    PyObject *tmp_import_name_from_1;
    PyObject *tmp_import_name_from_2;
    PyObject *tmp_import_name_from_3;
    PyObject *tmp_import_name_from_4;
    PyObject *tmp_import_name_from_5;
    PyObject *tmp_import_name_from_6;
    PyObject *tmp_import_name_from_7;
    PyObject *tmp_import_name_from_8;
    PyObject *tmp_iter_arg_1;
    PyObject *tmp_kw_name_1;
    PyObject *tmp_kw_name_2;
    PyObject *tmp_kw_name_3;
    PyObject *tmp_kw_name_4;
    PyObject *tmp_kw_name_5;
    PyObject *tmp_level_name_2;
    PyObject *tmp_level_name_3;
    PyObject *tmp_level_name_4;
    PyObject *tmp_level_name_5;
    PyObject *tmp_level_name_6;
    PyObject *tmp_level_name_7;
    PyObject *tmp_level_name_8;
    PyObject *tmp_level_name_9;
    PyObject *tmp_level_name_10;
    PyObject *tmp_level_name_11;
    PyObject *tmp_level_name_12;
    PyObject *tmp_level_name_13;
    PyObject *tmp_level_name_14;
    PyObject *tmp_level_name_15;
    PyObject *tmp_level_name_16;
    PyObject *tmp_level_name_17;
    PyObject *tmp_locals_name_2;
    PyObject *tmp_locals_name_3;
    PyObject *tmp_locals_name_4;
    PyObject *tmp_locals_name_5;
    PyObject *tmp_locals_name_6;
    PyObject *tmp_locals_name_7;
    PyObject *tmp_locals_name_8;
    PyObject *tmp_locals_name_9;
    PyObject *tmp_locals_name_10;
    PyObject *tmp_locals_name_11;
    PyObject *tmp_locals_name_12;
    PyObject *tmp_locals_name_13;
    PyObject *tmp_locals_name_14;
    PyObject *tmp_locals_name_15;
    PyObject *tmp_locals_name_16;
    PyObject *tmp_locals_name_17;
    PyObject *tmp_make_exception_arg_1;
    PyObject *tmp_make_exception_arg_2;
    PyObject *tmp_name_name_1;
    PyObject *tmp_name_name_2;
    PyObject *tmp_name_name_3;
    PyObject *tmp_name_name_4;
    PyObject *tmp_name_name_5;
    PyObject *tmp_name_name_6;
    PyObject *tmp_name_name_7;
    PyObject *tmp_name_name_8;
    PyObject *tmp_name_name_9;
    PyObject *tmp_name_name_10;
    PyObject *tmp_name_name_11;
    PyObject *tmp_name_name_12;
    PyObject *tmp_name_name_13;
    PyObject *tmp_name_name_14;
    PyObject *tmp_name_name_15;
    PyObject *tmp_name_name_16;
    PyObject *tmp_name_name_17;
    PyObject *tmp_raise_type_1;
    PyObject *tmp_raise_type_2;
    int tmp_res;
    bool tmp_result;
    PyObject *tmp_source_name_1;
    PyObject *tmp_source_name_2;
    PyObject *tmp_source_name_3;
    PyObject *tmp_source_name_4;
    PyObject *tmp_source_name_5;
    PyObject *tmp_source_name_6;
    PyObject *tmp_source_name_7;
    PyObject *tmp_source_name_8;
    PyObject *tmp_source_name_9;
    PyObject *tmp_source_name_10;
    PyObject *tmp_source_name_11;
    PyObject *tmp_star_imported_1;
    PyObject *tmp_star_imported_2;
    PyObject *tmp_star_imported_3;
    PyObject *tmp_star_imported_4;
    PyObject *tmp_star_imported_5;
    PyObject *tmp_star_imported_6;
    PyObject *tmp_star_imported_7;
    PyObject *tmp_subscribed_name_1;
    PyObject *tmp_subscript_name_1;
    PyObject *tmp_unicode_arg_1;
    NUITKA_MAY_BE_UNUSED PyObject *tmp_unused;
    PyObject *tmp_value_name_1;
    struct Nuitka_FrameObject *frame_ed0438480de34297226c0f6f8fcf3ee7;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;

    // Module code.
    tmp_assign_source_1 = Py_None;
    UPDATE_STRING_DICT0( moduledict_pandas, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    tmp_assign_source_2 = const_str_digest_bd6e17405698c04366d70c9517b40844;
    UPDATE_STRING_DICT0( moduledict_pandas, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    tmp_assign_source_3 = LIST_COPY( const_list_str_digest_e395eb7dec6e5461fe8fada8d882e8ef_list );
    UPDATE_STRING_DICT1( moduledict_pandas, (Nuitka_StringObject *)const_str_plain___path__, tmp_assign_source_3 );
    tmp_assign_source_4 = Py_None;
    UPDATE_STRING_DICT0( moduledict_pandas, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_4 );
    tmp_assign_source_5 = const_str_plain_restructuredtext;
    UPDATE_STRING_DICT0( moduledict_pandas, (Nuitka_StringObject *)const_str_plain___docformat__, tmp_assign_source_5 );
    tmp_assign_source_6 = const_tuple_str_plain_numpy_str_plain_pytz_str_plain_dateutil_tuple;
    UPDATE_STRING_DICT0( moduledict_pandas, (Nuitka_StringObject *)const_str_plain_hard_dependencies, tmp_assign_source_6 );
    tmp_assign_source_7 = PyList_New( 0 );
    UPDATE_STRING_DICT1( moduledict_pandas, (Nuitka_StringObject *)const_str_plain_missing_dependencies, tmp_assign_source_7 );
    // Frame without reuse.
    frame_ed0438480de34297226c0f6f8fcf3ee7 = MAKE_MODULE_FRAME( codeobj_ed0438480de34297226c0f6f8fcf3ee7, module_pandas );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_ed0438480de34297226c0f6f8fcf3ee7 );
    assert( Py_REFCNT( frame_ed0438480de34297226c0f6f8fcf3ee7 ) == 2 );

    // Framed code:
    tmp_iter_arg_1 = GET_STRING_DICT_VALUE( moduledict_pandas, (Nuitka_StringObject *)const_str_plain_hard_dependencies );

    if (unlikely( tmp_iter_arg_1 == NULL ))
    {
        tmp_iter_arg_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_hard_dependencies );
    }

    CHECK_OBJECT( tmp_iter_arg_1 );
    tmp_assign_source_8 = MAKE_ITERATOR( tmp_iter_arg_1 );
    if ( tmp_assign_source_8 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 11;

        goto frame_exception_exit_1;
    }
    assert( tmp_for_loop_1__for_iterator == NULL );
    tmp_for_loop_1__for_iterator = tmp_assign_source_8;

    // Tried code:
    loop_start_1:;
    // Tried code:
    tmp_value_name_1 = tmp_for_loop_1__for_iterator;

    CHECK_OBJECT( tmp_value_name_1 );
    tmp_assign_source_9 = ITERATOR_NEXT( tmp_value_name_1 );
    if ( tmp_assign_source_9 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }



        exception_lineno = 11;
        goto try_except_handler_2;
    }
    {
        PyObject *old = tmp_for_loop_1__iter_value;
        tmp_for_loop_1__iter_value = tmp_assign_source_9;
        Py_XDECREF( old );
    }

    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    tmp_compare_left_1 = exception_keeper_type_1;
    tmp_compare_right_1 = PyExc_StopIteration;
    tmp_exc_match_exception_match_1 = EXCEPTION_MATCH_BOOL( tmp_compare_left_1, tmp_compare_right_1 );
    if ( tmp_exc_match_exception_match_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

        Py_DECREF( exception_keeper_type_1 );
        Py_XDECREF( exception_keeper_value_1 );
        Py_XDECREF( exception_keeper_tb_1 );

        exception_lineno = 11;

        goto try_except_handler_1;
    }
    if ( tmp_exc_match_exception_match_1 == 1 )
    {
        goto branch_yes_1;
    }
    else
    {
        goto branch_no_1;
    }
    branch_yes_1:;
    Py_DECREF( exception_keeper_type_1 );
    Py_XDECREF( exception_keeper_value_1 );
    Py_XDECREF( exception_keeper_tb_1 );
    goto loop_end_1;
    goto branch_end_1;
    branch_no_1:;
    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_1;
    branch_end_1:;
    // End of try:
    try_end_1:;
    tmp_assign_source_10 = tmp_for_loop_1__iter_value;

    CHECK_OBJECT( tmp_assign_source_10 );
    UPDATE_STRING_DICT0( moduledict_pandas, (Nuitka_StringObject *)const_str_plain_dependency, tmp_assign_source_10 );
    // Tried code:
    tmp_name_name_1 = GET_STRING_DICT_VALUE( moduledict_pandas, (Nuitka_StringObject *)const_str_plain_dependency );

    if (unlikely( tmp_name_name_1 == NULL ))
    {
        tmp_name_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_dependency );
    }

    CHECK_OBJECT( tmp_name_name_1 );
    frame_ed0438480de34297226c0f6f8fcf3ee7->m_frame.f_lineno = 13;
    tmp_unused = IMPORT_MODULE1( tmp_name_name_1 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 13;

        goto try_except_handler_3;
    }
    Py_DECREF( tmp_unused );
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_2 == NULL )
    {
        exception_keeper_tb_2 = MAKE_TRACEBACK( frame_ed0438480de34297226c0f6f8fcf3ee7, exception_keeper_lineno_2 );
    }
    else if ( exception_keeper_lineno_2 != 0 )
    {
        exception_keeper_tb_2 = ADD_TRACEBACK( exception_keeper_tb_2, frame_ed0438480de34297226c0f6f8fcf3ee7, exception_keeper_lineno_2 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_2, &exception_keeper_value_2, &exception_keeper_tb_2 );
    PyException_SetTraceback( exception_keeper_value_2, (PyObject *)exception_keeper_tb_2 );
    PUBLISH_EXCEPTION( &exception_keeper_type_2, &exception_keeper_value_2, &exception_keeper_tb_2 );
    // Tried code:
    tmp_compare_left_2 = EXC_TYPE(PyThreadState_GET());
    tmp_compare_right_2 = PyExc_ImportError;
    tmp_exc_match_exception_match_2 = EXCEPTION_MATCH_BOOL( tmp_compare_left_2, tmp_compare_right_2 );
    if ( tmp_exc_match_exception_match_2 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 14;

        goto try_except_handler_4;
    }
    if ( tmp_exc_match_exception_match_2 == 1 )
    {
        goto branch_yes_2;
    }
    else
    {
        goto branch_no_2;
    }
    branch_yes_2:;
    tmp_assign_source_11 = EXC_VALUE(PyThreadState_GET());
    UPDATE_STRING_DICT0( moduledict_pandas, (Nuitka_StringObject *)const_str_plain_e, tmp_assign_source_11 );
    // Tried code:
    tmp_source_name_1 = GET_STRING_DICT_VALUE( moduledict_pandas, (Nuitka_StringObject *)const_str_plain_missing_dependencies );

    if (unlikely( tmp_source_name_1 == NULL ))
    {
        tmp_source_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_missing_dependencies );
    }

    if ( tmp_source_name_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "missing_dependencies" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 15;

        goto try_except_handler_5;
    }

    tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_append );
    if ( tmp_called_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 15;

        goto try_except_handler_5;
    }
    tmp_args_element_name_1 = GET_STRING_DICT_VALUE( moduledict_pandas, (Nuitka_StringObject *)const_str_plain_dependency );

    if (unlikely( tmp_args_element_name_1 == NULL ))
    {
        tmp_args_element_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_dependency );
    }

    if ( tmp_args_element_name_1 == NULL )
    {
        Py_DECREF( tmp_called_name_1 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "dependency" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 15;

        goto try_except_handler_5;
    }

    frame_ed0438480de34297226c0f6f8fcf3ee7->m_frame.f_lineno = 15;
    {
        PyObject *call_args[] = { tmp_args_element_name_1 };
        tmp_unused = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
    }

    Py_DECREF( tmp_called_name_1 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 15;

        goto try_except_handler_5;
    }
    Py_DECREF( tmp_unused );
    goto try_end_3;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    tmp_res = PyDict_DelItem( (PyObject *)moduledict_pandas, const_str_plain_e );
    if ( tmp_res == -1 ) CLEAR_ERROR_OCCURRED();

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto try_except_handler_4;
    // End of try:
    try_end_3:;
    tmp_res = PyDict_DelItem( (PyObject *)moduledict_pandas, const_str_plain_e );
    if ( tmp_res == -1 ) CLEAR_ERROR_OCCURRED();

    goto branch_end_2;
    branch_no_2:;
    tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (unlikely( tmp_result == false ))
    {
        exception_lineno = 12;
    }

    if (exception_tb && exception_tb->tb_frame == &frame_ed0438480de34297226c0f6f8fcf3ee7->m_frame) frame_ed0438480de34297226c0f6f8fcf3ee7->m_frame.f_lineno = exception_tb->tb_lineno;

    goto try_except_handler_4;
    branch_end_2:;
    goto try_end_4;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto try_except_handler_1;
    // End of try:
    try_end_4:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto try_end_2;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas );
    return MOD_RETURN_VALUE( NULL );
    // End of try:
    try_end_2:;
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 11;

        goto try_except_handler_1;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_5;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto frame_exception_exit_1;
    // End of try:
    try_end_5:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    tmp_cond_value_1 = GET_STRING_DICT_VALUE( moduledict_pandas, (Nuitka_StringObject *)const_str_plain_missing_dependencies );

    if (unlikely( tmp_cond_value_1 == NULL ))
    {
        tmp_cond_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_missing_dependencies );
    }

    if ( tmp_cond_value_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "missing_dependencies" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 17;

        goto frame_exception_exit_1;
    }

    tmp_cond_truth_1 = CHECK_IF_TRUE( tmp_cond_value_1 );
    if ( tmp_cond_truth_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 17;

        goto frame_exception_exit_1;
    }
    if ( tmp_cond_truth_1 == 1 )
    {
        goto branch_yes_3;
    }
    else
    {
        goto branch_no_3;
    }
    branch_yes_3:;
    tmp_source_name_2 = const_str_digest_e3e2c8aec631b1501b2ec33fc76e8c92;
    tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_format );
    assert( !(tmp_called_name_2 == NULL) );
    tmp_args_element_name_2 = GET_STRING_DICT_VALUE( moduledict_pandas, (Nuitka_StringObject *)const_str_plain_missing_dependencies );

    if (unlikely( tmp_args_element_name_2 == NULL ))
    {
        tmp_args_element_name_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_missing_dependencies );
    }

    if ( tmp_args_element_name_2 == NULL )
    {
        Py_DECREF( tmp_called_name_2 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "missing_dependencies" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 19;

        goto frame_exception_exit_1;
    }

    frame_ed0438480de34297226c0f6f8fcf3ee7->m_frame.f_lineno = 19;
    {
        PyObject *call_args[] = { tmp_args_element_name_2 };
        tmp_make_exception_arg_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
    }

    Py_DECREF( tmp_called_name_2 );
    if ( tmp_make_exception_arg_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 19;

        goto frame_exception_exit_1;
    }
    frame_ed0438480de34297226c0f6f8fcf3ee7->m_frame.f_lineno = 18;
    {
        PyObject *call_args[] = { tmp_make_exception_arg_1 };
        tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_ImportError, call_args );
    }

    Py_DECREF( tmp_make_exception_arg_1 );
    assert( !(tmp_raise_type_1 == NULL) );
    exception_type = tmp_raise_type_1;
    exception_lineno = 18;
    RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );

    goto frame_exception_exit_1;
    branch_no_3:;
    tmp_res = PyDict_DelItem( (PyObject *)moduledict_pandas, const_str_plain_hard_dependencies );
    if ( tmp_res == -1 ) CLEAR_ERROR_OCCURRED();

    if ( tmp_res == -1 )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "hard_dependencies" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 20;

        goto frame_exception_exit_1;
    }

    tmp_res = PyDict_DelItem( (PyObject *)moduledict_pandas, const_str_plain_dependency );
    if ( tmp_res == -1 ) CLEAR_ERROR_OCCURRED();

    if ( tmp_res == -1 )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "dependency" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 20;

        goto frame_exception_exit_1;
    }

    tmp_res = PyDict_DelItem( (PyObject *)moduledict_pandas, const_str_plain_missing_dependencies );
    if ( tmp_res == -1 ) CLEAR_ERROR_OCCURRED();

    if ( tmp_res == -1 )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "missing_dependencies" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 20;

        goto frame_exception_exit_1;
    }

    tmp_name_name_2 = const_str_digest_ea38e05af30aa1be8abd26f39d496acd;
    tmp_globals_name_2 = (PyObject *)moduledict_pandas;
    tmp_locals_name_2 = (PyObject *)moduledict_pandas;
    tmp_fromlist_name_2 = const_tuple_str_chr_42_tuple;
    tmp_level_name_2 = const_int_0;
    frame_ed0438480de34297226c0f6f8fcf3ee7->m_frame.f_lineno = 23;
    tmp_star_imported_1 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
    if ( tmp_star_imported_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 23;

        goto frame_exception_exit_1;
    }
    tmp_result = IMPORT_MODULE_STAR( module_pandas, true, tmp_star_imported_1 );
    Py_DECREF( tmp_star_imported_1 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 23;

        goto frame_exception_exit_1;
    }
    // Tried code:
    tmp_name_name_3 = const_str_digest_3d595a6e00e4e48b6d61c247ac7fc89f;
    tmp_globals_name_3 = (PyObject *)moduledict_pandas;
    tmp_locals_name_3 = Py_None;
    tmp_fromlist_name_3 = const_tuple_str_plain_hashtable_str_plain_lib_str_plain_tslib_tuple;
    tmp_level_name_3 = const_int_0;
    frame_ed0438480de34297226c0f6f8fcf3ee7->m_frame.f_lineno = 26;
    tmp_assign_source_12 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
    if ( tmp_assign_source_12 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 26;

        goto try_except_handler_6;
    }
    assert( tmp_import_from_1__module == NULL );
    tmp_import_from_1__module = tmp_assign_source_12;

    // Tried code:
    tmp_import_name_from_1 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_1 );
    tmp_assign_source_13 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_hashtable );
    if ( tmp_assign_source_13 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 26;

        goto try_except_handler_7;
    }
    UPDATE_STRING_DICT1( moduledict_pandas, (Nuitka_StringObject *)const_str_plain__hashtable, tmp_assign_source_13 );
    tmp_import_name_from_2 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_2 );
    tmp_assign_source_14 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_lib );
    if ( tmp_assign_source_14 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 26;

        goto try_except_handler_7;
    }
    UPDATE_STRING_DICT1( moduledict_pandas, (Nuitka_StringObject *)const_str_plain__lib, tmp_assign_source_14 );
    tmp_import_name_from_3 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_3 );
    tmp_assign_source_15 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_tslib );
    if ( tmp_assign_source_15 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 26;

        goto try_except_handler_7;
    }
    UPDATE_STRING_DICT1( moduledict_pandas, (Nuitka_StringObject *)const_str_plain__tslib, tmp_assign_source_15 );
    goto try_end_6;
    // Exception handler code:
    try_except_handler_7:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_keeper_lineno_6 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_6;
    exception_value = exception_keeper_value_6;
    exception_tb = exception_keeper_tb_6;
    exception_lineno = exception_keeper_lineno_6;

    goto try_except_handler_6;
    // End of try:
    try_end_6:;
    goto try_end_7;
    // Exception handler code:
    try_except_handler_6:;
    exception_keeper_type_7 = exception_type;
    exception_keeper_value_7 = exception_value;
    exception_keeper_tb_7 = exception_tb;
    exception_keeper_lineno_7 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_2 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_2 );
    exception_preserved_value_2 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_2 );
    exception_preserved_tb_2 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_2 );

    if ( exception_keeper_tb_7 == NULL )
    {
        exception_keeper_tb_7 = MAKE_TRACEBACK( frame_ed0438480de34297226c0f6f8fcf3ee7, exception_keeper_lineno_7 );
    }
    else if ( exception_keeper_lineno_7 != 0 )
    {
        exception_keeper_tb_7 = ADD_TRACEBACK( exception_keeper_tb_7, frame_ed0438480de34297226c0f6f8fcf3ee7, exception_keeper_lineno_7 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_7, &exception_keeper_value_7, &exception_keeper_tb_7 );
    PyException_SetTraceback( exception_keeper_value_7, (PyObject *)exception_keeper_tb_7 );
    PUBLISH_EXCEPTION( &exception_keeper_type_7, &exception_keeper_value_7, &exception_keeper_tb_7 );
    // Tried code:
    tmp_compare_left_3 = EXC_TYPE(PyThreadState_GET());
    tmp_compare_right_3 = PyExc_ImportError;
    tmp_exc_match_exception_match_3 = EXCEPTION_MATCH_BOOL( tmp_compare_left_3, tmp_compare_right_3 );
    if ( tmp_exc_match_exception_match_3 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 29;

        goto try_except_handler_8;
    }
    if ( tmp_exc_match_exception_match_3 == 1 )
    {
        goto branch_yes_4;
    }
    else
    {
        goto branch_no_4;
    }
    branch_yes_4:;
    tmp_assign_source_16 = EXC_VALUE(PyThreadState_GET());
    UPDATE_STRING_DICT0( moduledict_pandas, (Nuitka_StringObject *)const_str_plain_e, tmp_assign_source_16 );
    // Tried code:
    tmp_unicode_arg_1 = GET_STRING_DICT_VALUE( moduledict_pandas, (Nuitka_StringObject *)const_str_plain_e );

    if (unlikely( tmp_unicode_arg_1 == NULL ))
    {
        tmp_unicode_arg_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_e );
    }

    CHECK_OBJECT( tmp_unicode_arg_1 );
    tmp_called_instance_1 = PyObject_Unicode( tmp_unicode_arg_1 );
    if ( tmp_called_instance_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 31;

        goto try_except_handler_9;
    }
    frame_ed0438480de34297226c0f6f8fcf3ee7->m_frame.f_lineno = 31;
    tmp_assign_source_17 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_replace, &PyTuple_GET_ITEM( const_tuple_str_digest_f28ac297014eae6038f099e88653df5d_str_empty_tuple, 0 ) );

    Py_DECREF( tmp_called_instance_1 );
    if ( tmp_assign_source_17 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 31;

        goto try_except_handler_9;
    }
    UPDATE_STRING_DICT1( moduledict_pandas, (Nuitka_StringObject *)const_str_plain_module, tmp_assign_source_17 );
    tmp_called_instance_2 = const_str_digest_40af7bdd1c7ab3ebd9f8e541abe1952c;
    tmp_args_element_name_3 = GET_STRING_DICT_VALUE( moduledict_pandas, (Nuitka_StringObject *)const_str_plain_module );

    if (unlikely( tmp_args_element_name_3 == NULL ))
    {
        tmp_args_element_name_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_module );
    }

    CHECK_OBJECT( tmp_args_element_name_3 );
    frame_ed0438480de34297226c0f6f8fcf3ee7->m_frame.f_lineno = 32;
    {
        PyObject *call_args[] = { tmp_args_element_name_3 };
        tmp_make_exception_arg_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_format, call_args );
    }

    if ( tmp_make_exception_arg_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 32;

        goto try_except_handler_9;
    }
    frame_ed0438480de34297226c0f6f8fcf3ee7->m_frame.f_lineno = 32;
    {
        PyObject *call_args[] = { tmp_make_exception_arg_2 };
        tmp_raise_type_2 = CALL_FUNCTION_WITH_ARGS1( PyExc_ImportError, call_args );
    }

    Py_DECREF( tmp_make_exception_arg_2 );
    assert( !(tmp_raise_type_2 == NULL) );
    exception_type = tmp_raise_type_2;
    exception_lineno = 32;
    RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );

    goto try_except_handler_9;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas );
    return MOD_RETURN_VALUE( NULL );
    // Exception handler code:
    try_except_handler_9:;
    exception_keeper_type_8 = exception_type;
    exception_keeper_value_8 = exception_value;
    exception_keeper_tb_8 = exception_tb;
    exception_keeper_lineno_8 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    tmp_res = PyDict_DelItem( (PyObject *)moduledict_pandas, const_str_plain_e );
    if ( tmp_res == -1 ) CLEAR_ERROR_OCCURRED();

    // Re-raise.
    exception_type = exception_keeper_type_8;
    exception_value = exception_keeper_value_8;
    exception_tb = exception_keeper_tb_8;
    exception_lineno = exception_keeper_lineno_8;

    goto try_except_handler_8;
    // End of try:
    goto branch_end_4;
    branch_no_4:;
    tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (unlikely( tmp_result == false ))
    {
        exception_lineno = 25;
    }

    if (exception_tb && exception_tb->tb_frame == &frame_ed0438480de34297226c0f6f8fcf3ee7->m_frame) frame_ed0438480de34297226c0f6f8fcf3ee7->m_frame.f_lineno = exception_tb->tb_lineno;

    goto try_except_handler_8;
    branch_end_4:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas );
    return MOD_RETURN_VALUE( NULL );
    // Exception handler code:
    try_except_handler_8:;
    exception_keeper_type_9 = exception_type;
    exception_keeper_value_9 = exception_value;
    exception_keeper_tb_9 = exception_tb;
    exception_keeper_lineno_9 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_2, exception_preserved_value_2, exception_preserved_tb_2 );
    // Re-raise.
    exception_type = exception_keeper_type_9;
    exception_value = exception_keeper_value_9;
    exception_tb = exception_keeper_tb_9;
    exception_lineno = exception_keeper_lineno_9;

    goto frame_exception_exit_1;
    // End of try:
    // End of try:
    try_end_7:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    tmp_name_name_4 = const_str_plain_datetime;
    tmp_globals_name_4 = (PyObject *)moduledict_pandas;
    tmp_locals_name_4 = Py_None;
    tmp_fromlist_name_4 = const_tuple_str_plain_datetime_tuple;
    tmp_level_name_4 = const_int_0;
    frame_ed0438480de34297226c0f6f8fcf3ee7->m_frame.f_lineno = 37;
    tmp_import_name_from_4 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
    if ( tmp_import_name_from_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 37;

        goto frame_exception_exit_1;
    }
    tmp_assign_source_18 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_datetime );
    Py_DECREF( tmp_import_name_from_4 );
    if ( tmp_assign_source_18 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 37;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas, (Nuitka_StringObject *)const_str_plain_datetime, tmp_assign_source_18 );
    tmp_name_name_5 = const_str_digest_48ae02d7bc3fc61cbffb3f46dd54fdfa;
    tmp_globals_name_5 = (PyObject *)moduledict_pandas;
    tmp_locals_name_5 = Py_None;
    tmp_fromlist_name_5 = Py_None;
    tmp_level_name_5 = const_int_0;
    frame_ed0438480de34297226c0f6f8fcf3ee7->m_frame.f_lineno = 40;
    tmp_assign_source_19 = IMPORT_MODULE5( tmp_name_name_5, tmp_globals_name_5, tmp_locals_name_5, tmp_fromlist_name_5, tmp_level_name_5 );
    if ( tmp_assign_source_19 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 40;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas, (Nuitka_StringObject *)const_str_plain_pandas, tmp_assign_source_19 );
    tmp_name_name_6 = const_str_digest_3fa11e71aa92f39879464952ab264ea1;
    tmp_globals_name_6 = (PyObject *)moduledict_pandas;
    tmp_locals_name_6 = (PyObject *)moduledict_pandas;
    tmp_fromlist_name_6 = const_tuple_str_chr_42_tuple;
    tmp_level_name_6 = const_int_0;
    frame_ed0438480de34297226c0f6f8fcf3ee7->m_frame.f_lineno = 42;
    tmp_star_imported_2 = IMPORT_MODULE5( tmp_name_name_6, tmp_globals_name_6, tmp_locals_name_6, tmp_fromlist_name_6, tmp_level_name_6 );
    if ( tmp_star_imported_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 42;

        goto frame_exception_exit_1;
    }
    tmp_result = IMPORT_MODULE_STAR( module_pandas, true, tmp_star_imported_2 );
    Py_DECREF( tmp_star_imported_2 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 42;

        goto frame_exception_exit_1;
    }
    tmp_name_name_7 = const_str_digest_7c9fbc1534b9d3e77d7ca1a84c272912;
    tmp_globals_name_7 = (PyObject *)moduledict_pandas;
    tmp_locals_name_7 = (PyObject *)moduledict_pandas;
    tmp_fromlist_name_7 = const_tuple_str_chr_42_tuple;
    tmp_level_name_7 = const_int_0;
    frame_ed0438480de34297226c0f6f8fcf3ee7->m_frame.f_lineno = 43;
    tmp_star_imported_3 = IMPORT_MODULE5( tmp_name_name_7, tmp_globals_name_7, tmp_locals_name_7, tmp_fromlist_name_7, tmp_level_name_7 );
    if ( tmp_star_imported_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 43;

        goto frame_exception_exit_1;
    }
    tmp_result = IMPORT_MODULE_STAR( module_pandas, true, tmp_star_imported_3 );
    Py_DECREF( tmp_star_imported_3 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 43;

        goto frame_exception_exit_1;
    }
    tmp_name_name_8 = const_str_digest_6c6930528997e29cd62f219edf616ec7;
    tmp_globals_name_8 = (PyObject *)moduledict_pandas;
    tmp_locals_name_8 = (PyObject *)moduledict_pandas;
    tmp_fromlist_name_8 = const_tuple_str_chr_42_tuple;
    tmp_level_name_8 = const_int_0;
    frame_ed0438480de34297226c0f6f8fcf3ee7->m_frame.f_lineno = 44;
    tmp_star_imported_4 = IMPORT_MODULE5( tmp_name_name_8, tmp_globals_name_8, tmp_locals_name_8, tmp_fromlist_name_8, tmp_level_name_8 );
    if ( tmp_star_imported_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 44;

        goto frame_exception_exit_1;
    }
    tmp_result = IMPORT_MODULE_STAR( module_pandas, true, tmp_star_imported_4 );
    Py_DECREF( tmp_star_imported_4 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 44;

        goto frame_exception_exit_1;
    }
    tmp_name_name_9 = const_str_digest_85d0cfed91b588c9de3b494ad496b19c;
    tmp_globals_name_9 = (PyObject *)moduledict_pandas;
    tmp_locals_name_9 = (PyObject *)moduledict_pandas;
    tmp_fromlist_name_9 = const_tuple_str_chr_42_tuple;
    tmp_level_name_9 = const_int_0;
    frame_ed0438480de34297226c0f6f8fcf3ee7->m_frame.f_lineno = 45;
    tmp_star_imported_5 = IMPORT_MODULE5( tmp_name_name_9, tmp_globals_name_9, tmp_locals_name_9, tmp_fromlist_name_9, tmp_level_name_9 );
    if ( tmp_star_imported_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 45;

        goto frame_exception_exit_1;
    }
    tmp_result = IMPORT_MODULE_STAR( module_pandas, true, tmp_star_imported_5 );
    Py_DECREF( tmp_star_imported_5 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 45;

        goto frame_exception_exit_1;
    }
    tmp_name_name_10 = const_str_digest_71757cb1d3ba810942677ab709005d4f;
    tmp_globals_name_10 = (PyObject *)moduledict_pandas;
    tmp_locals_name_10 = (PyObject *)moduledict_pandas;
    tmp_fromlist_name_10 = const_tuple_str_chr_42_tuple;
    tmp_level_name_10 = const_int_0;
    frame_ed0438480de34297226c0f6f8fcf3ee7->m_frame.f_lineno = 46;
    tmp_star_imported_6 = IMPORT_MODULE5( tmp_name_name_10, tmp_globals_name_10, tmp_locals_name_10, tmp_fromlist_name_10, tmp_level_name_10 );
    if ( tmp_star_imported_6 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 46;

        goto frame_exception_exit_1;
    }
    tmp_result = IMPORT_MODULE_STAR( module_pandas, true, tmp_star_imported_6 );
    Py_DECREF( tmp_star_imported_6 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 46;

        goto frame_exception_exit_1;
    }
    tmp_name_name_11 = const_str_digest_de1e82e029cb529be78cb83e0ad934c6;
    tmp_globals_name_11 = (PyObject *)moduledict_pandas;
    tmp_locals_name_11 = Py_None;
    tmp_fromlist_name_11 = Py_None;
    tmp_level_name_11 = const_int_0;
    frame_ed0438480de34297226c0f6f8fcf3ee7->m_frame.f_lineno = 49;
    tmp_assign_source_20 = IMPORT_MODULE5( tmp_name_name_11, tmp_globals_name_11, tmp_locals_name_11, tmp_fromlist_name_11, tmp_level_name_11 );
    if ( tmp_assign_source_20 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 49;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas, (Nuitka_StringObject *)const_str_plain_pandas, tmp_assign_source_20 );
    tmp_source_name_5 = GET_STRING_DICT_VALUE( moduledict_pandas, (Nuitka_StringObject *)const_str_plain_pandas );

    if (unlikely( tmp_source_name_5 == NULL ))
    {
        tmp_source_name_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_pandas );
    }

    CHECK_OBJECT( tmp_source_name_5 );
    tmp_source_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_plotting );
    if ( tmp_source_name_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 50;

        goto frame_exception_exit_1;
    }
    tmp_source_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain__style );
    Py_DECREF( tmp_source_name_4 );
    if ( tmp_source_name_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 50;

        goto frame_exception_exit_1;
    }
    tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain__Options );
    Py_DECREF( tmp_source_name_3 );
    if ( tmp_called_name_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 50;

        goto frame_exception_exit_1;
    }
    tmp_kw_name_1 = PyDict_Copy( const_dict_140920105d15f4c6f953ce2fe8a1a576 );
    frame_ed0438480de34297226c0f6f8fcf3ee7->m_frame.f_lineno = 50;
    tmp_assign_source_21 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_3, tmp_kw_name_1 );
    Py_DECREF( tmp_called_name_3 );
    Py_DECREF( tmp_kw_name_1 );
    if ( tmp_assign_source_21 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 50;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas, (Nuitka_StringObject *)const_str_plain_plot_params, tmp_assign_source_21 );
    tmp_source_name_8 = GET_STRING_DICT_VALUE( moduledict_pandas, (Nuitka_StringObject *)const_str_plain_pandas );

    if (unlikely( tmp_source_name_8 == NULL ))
    {
        tmp_source_name_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_pandas );
    }

    if ( tmp_source_name_8 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "pandas" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 52;

        goto frame_exception_exit_1;
    }

    tmp_source_name_7 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_util );
    if ( tmp_source_name_7 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 52;

        goto frame_exception_exit_1;
    }
    tmp_source_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain__decorators );
    Py_DECREF( tmp_source_name_7 );
    if ( tmp_source_name_6 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 52;

        goto frame_exception_exit_1;
    }
    tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_deprecate );
    Py_DECREF( tmp_source_name_6 );
    if ( tmp_called_name_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 52;

        goto frame_exception_exit_1;
    }
    tmp_args_element_name_4 = const_str_digest_059bcf492d979b203cf5a6691794a0b7;
    tmp_source_name_10 = GET_STRING_DICT_VALUE( moduledict_pandas, (Nuitka_StringObject *)const_str_plain_pandas );

    if (unlikely( tmp_source_name_10 == NULL ))
    {
        tmp_source_name_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_pandas );
    }

    if ( tmp_source_name_10 == NULL )
    {
        Py_DECREF( tmp_called_name_4 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "pandas" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 53;

        goto frame_exception_exit_1;
    }

    tmp_source_name_9 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_plotting );
    if ( tmp_source_name_9 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_4 );

        exception_lineno = 53;

        goto frame_exception_exit_1;
    }
    tmp_args_element_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_scatter_matrix );
    Py_DECREF( tmp_source_name_9 );
    if ( tmp_args_element_name_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_4 );

        exception_lineno = 53;

        goto frame_exception_exit_1;
    }
    tmp_args_element_name_6 = const_str_digest_6ea18b76b9b72ed68e11aa82f9d4700e;
    tmp_args_element_name_7 = const_str_digest_aeef4a412ee9ab3321442889853d64ce;
    frame_ed0438480de34297226c0f6f8fcf3ee7->m_frame.f_lineno = 52;
    {
        PyObject *call_args[] = { tmp_args_element_name_4, tmp_args_element_name_5, tmp_args_element_name_6, tmp_args_element_name_7 };
        tmp_assign_source_22 = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_4, call_args );
    }

    Py_DECREF( tmp_called_name_4 );
    Py_DECREF( tmp_args_element_name_5 );
    if ( tmp_assign_source_22 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 52;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas, (Nuitka_StringObject *)const_str_plain_scatter_matrix, tmp_assign_source_22 );
    tmp_name_name_12 = const_str_digest_da95bebd2524b65728827a9e6d1504b8;
    tmp_globals_name_12 = (PyObject *)moduledict_pandas;
    tmp_locals_name_12 = Py_None;
    tmp_fromlist_name_12 = const_tuple_str_plain_show_versions_tuple;
    tmp_level_name_12 = const_int_0;
    frame_ed0438480de34297226c0f6f8fcf3ee7->m_frame.f_lineno = 56;
    tmp_import_name_from_5 = IMPORT_MODULE5( tmp_name_name_12, tmp_globals_name_12, tmp_locals_name_12, tmp_fromlist_name_12, tmp_level_name_12 );
    if ( tmp_import_name_from_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 56;

        goto frame_exception_exit_1;
    }
    tmp_assign_source_23 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_show_versions );
    Py_DECREF( tmp_import_name_from_5 );
    if ( tmp_assign_source_23 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 56;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas, (Nuitka_StringObject *)const_str_plain_show_versions, tmp_assign_source_23 );
    tmp_name_name_13 = const_str_digest_9bdf56c59c2f38d290ad60074cc357bc;
    tmp_globals_name_13 = (PyObject *)moduledict_pandas;
    tmp_locals_name_13 = (PyObject *)moduledict_pandas;
    tmp_fromlist_name_13 = const_tuple_str_chr_42_tuple;
    tmp_level_name_13 = const_int_0;
    frame_ed0438480de34297226c0f6f8fcf3ee7->m_frame.f_lineno = 57;
    tmp_star_imported_7 = IMPORT_MODULE5( tmp_name_name_13, tmp_globals_name_13, tmp_locals_name_13, tmp_fromlist_name_13, tmp_level_name_13 );
    if ( tmp_star_imported_7 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 57;

        goto frame_exception_exit_1;
    }
    tmp_result = IMPORT_MODULE_STAR( module_pandas, true, tmp_star_imported_7 );
    Py_DECREF( tmp_star_imported_7 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 57;

        goto frame_exception_exit_1;
    }
    tmp_name_name_14 = const_str_digest_be2be1c115fc8af588b089f14adace08;
    tmp_globals_name_14 = (PyObject *)moduledict_pandas;
    tmp_locals_name_14 = Py_None;
    tmp_fromlist_name_14 = const_tuple_str_plain_test_tuple;
    tmp_level_name_14 = const_int_0;
    frame_ed0438480de34297226c0f6f8fcf3ee7->m_frame.f_lineno = 58;
    tmp_import_name_from_6 = IMPORT_MODULE5( tmp_name_name_14, tmp_globals_name_14, tmp_locals_name_14, tmp_fromlist_name_14, tmp_level_name_14 );
    if ( tmp_import_name_from_6 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 58;

        goto frame_exception_exit_1;
    }
    tmp_assign_source_24 = IMPORT_NAME( tmp_import_name_from_6, const_str_plain_test );
    Py_DECREF( tmp_import_name_from_6 );
    if ( tmp_assign_source_24 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 58;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas, (Nuitka_StringObject *)const_str_plain_test, tmp_assign_source_24 );
    tmp_name_name_15 = const_str_digest_a28def792293bb1bd13fa79bcde016ba;
    tmp_globals_name_15 = (PyObject *)moduledict_pandas;
    tmp_locals_name_15 = Py_None;
    tmp_fromlist_name_15 = Py_None;
    tmp_level_name_15 = const_int_0;
    frame_ed0438480de34297226c0f6f8fcf3ee7->m_frame.f_lineno = 59;
    tmp_assign_source_25 = IMPORT_MODULE5( tmp_name_name_15, tmp_globals_name_15, tmp_locals_name_15, tmp_fromlist_name_15, tmp_level_name_15 );
    if ( tmp_assign_source_25 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 59;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas, (Nuitka_StringObject *)const_str_plain_pandas, tmp_assign_source_25 );
    tmp_name_name_16 = const_str_digest_3a0b927a1532ded681dd49b7ca17edfe;
    tmp_globals_name_16 = (PyObject *)moduledict_pandas;
    tmp_locals_name_16 = Py_None;
    tmp_fromlist_name_16 = const_tuple_str_plain__DeprecatedModule_tuple;
    tmp_level_name_16 = const_int_0;
    frame_ed0438480de34297226c0f6f8fcf3ee7->m_frame.f_lineno = 62;
    tmp_import_name_from_7 = IMPORT_MODULE5( tmp_name_name_16, tmp_globals_name_16, tmp_locals_name_16, tmp_fromlist_name_16, tmp_level_name_16 );
    if ( tmp_import_name_from_7 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 62;

        goto frame_exception_exit_1;
    }
    tmp_assign_source_26 = IMPORT_NAME( tmp_import_name_from_7, const_str_plain__DeprecatedModule );
    Py_DECREF( tmp_import_name_from_7 );
    if ( tmp_assign_source_26 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 62;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas, (Nuitka_StringObject *)const_str_plain__DeprecatedModule, tmp_assign_source_26 );
    tmp_called_name_5 = GET_STRING_DICT_VALUE( moduledict_pandas, (Nuitka_StringObject *)const_str_plain__DeprecatedModule );

    if (unlikely( tmp_called_name_5 == NULL ))
    {
        tmp_called_name_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__DeprecatedModule );
    }

    CHECK_OBJECT( tmp_called_name_5 );
    tmp_kw_name_2 = DEEP_COPY( const_dict_83e7e19db27cfb349e8c76119d1bd0c6 );
    frame_ed0438480de34297226c0f6f8fcf3ee7->m_frame.f_lineno = 64;
    tmp_assign_source_27 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_5, tmp_kw_name_2 );
    Py_DECREF( tmp_kw_name_2 );
    if ( tmp_assign_source_27 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 64;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas, (Nuitka_StringObject *)const_str_plain_json, tmp_assign_source_27 );
    tmp_called_name_6 = GET_STRING_DICT_VALUE( moduledict_pandas, (Nuitka_StringObject *)const_str_plain__DeprecatedModule );

    if (unlikely( tmp_called_name_6 == NULL ))
    {
        tmp_called_name_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__DeprecatedModule );
    }

    if ( tmp_called_name_6 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_DeprecatedModule" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 67;

        goto frame_exception_exit_1;
    }

    tmp_kw_name_3 = DEEP_COPY( const_dict_20025cb31fdfb3b6919a061eca418fd9 );
    frame_ed0438480de34297226c0f6f8fcf3ee7->m_frame.f_lineno = 67;
    tmp_assign_source_28 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_6, tmp_kw_name_3 );
    Py_DECREF( tmp_kw_name_3 );
    if ( tmp_assign_source_28 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 67;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas, (Nuitka_StringObject *)const_str_plain_parser, tmp_assign_source_28 );
    tmp_called_name_7 = GET_STRING_DICT_VALUE( moduledict_pandas, (Nuitka_StringObject *)const_str_plain__DeprecatedModule );

    if (unlikely( tmp_called_name_7 == NULL ))
    {
        tmp_called_name_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__DeprecatedModule );
    }

    if ( tmp_called_name_7 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_DeprecatedModule" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 70;

        goto frame_exception_exit_1;
    }

    tmp_kw_name_4 = DEEP_COPY( const_dict_7e4518cc1d7a198ed98c1605c8646c8a );
    frame_ed0438480de34297226c0f6f8fcf3ee7->m_frame.f_lineno = 70;
    tmp_assign_source_29 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_7, tmp_kw_name_4 );
    Py_DECREF( tmp_kw_name_4 );
    if ( tmp_assign_source_29 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 70;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas, (Nuitka_StringObject *)const_str_plain_lib, tmp_assign_source_29 );
    tmp_called_name_8 = GET_STRING_DICT_VALUE( moduledict_pandas, (Nuitka_StringObject *)const_str_plain__DeprecatedModule );

    if (unlikely( tmp_called_name_8 == NULL ))
    {
        tmp_called_name_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__DeprecatedModule );
    }

    if ( tmp_called_name_8 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_DeprecatedModule" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 75;

        goto frame_exception_exit_1;
    }

    tmp_kw_name_5 = DEEP_COPY( const_dict_1b934c4fa06710fba00933c689ea2ea9 );
    frame_ed0438480de34297226c0f6f8fcf3ee7->m_frame.f_lineno = 75;
    tmp_assign_source_30 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_8, tmp_kw_name_5 );
    Py_DECREF( tmp_kw_name_5 );
    if ( tmp_assign_source_30 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 75;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas, (Nuitka_StringObject *)const_str_plain_tslib, tmp_assign_source_30 );
    tmp_name_name_17 = const_str_plain__version;
    tmp_globals_name_17 = (PyObject *)moduledict_pandas;
    tmp_locals_name_17 = Py_None;
    tmp_fromlist_name_17 = const_tuple_str_plain_get_versions_tuple;
    tmp_level_name_17 = const_int_pos_1;
    frame_ed0438480de34297226c0f6f8fcf3ee7->m_frame.f_lineno = 83;
    tmp_import_name_from_8 = IMPORT_MODULE5( tmp_name_name_17, tmp_globals_name_17, tmp_locals_name_17, tmp_fromlist_name_17, tmp_level_name_17 );
    if ( tmp_import_name_from_8 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 83;

        goto frame_exception_exit_1;
    }
    if ( PyModule_Check( tmp_import_name_from_8 ) )
    {
       tmp_assign_source_31 = IMPORT_NAME_OR_MODULE(
            tmp_import_name_from_8,
            (PyObject *)MODULE_DICT(tmp_import_name_from_8),
            const_str_plain_get_versions,
            const_int_pos_1
        );
    }
    else
    {
       tmp_assign_source_31 = IMPORT_NAME( tmp_import_name_from_8, const_str_plain_get_versions );
    }

    Py_DECREF( tmp_import_name_from_8 );
    if ( tmp_assign_source_31 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 83;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas, (Nuitka_StringObject *)const_str_plain_get_versions, tmp_assign_source_31 );
    tmp_called_name_9 = GET_STRING_DICT_VALUE( moduledict_pandas, (Nuitka_StringObject *)const_str_plain_get_versions );

    if (unlikely( tmp_called_name_9 == NULL ))
    {
        tmp_called_name_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_versions );
    }

    CHECK_OBJECT( tmp_called_name_9 );
    frame_ed0438480de34297226c0f6f8fcf3ee7->m_frame.f_lineno = 84;
    tmp_assign_source_32 = CALL_FUNCTION_NO_ARGS( tmp_called_name_9 );
    if ( tmp_assign_source_32 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 84;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas, (Nuitka_StringObject *)const_str_plain_v, tmp_assign_source_32 );
    tmp_source_name_11 = GET_STRING_DICT_VALUE( moduledict_pandas, (Nuitka_StringObject *)const_str_plain_v );

    if (unlikely( tmp_source_name_11 == NULL ))
    {
        tmp_source_name_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_v );
    }

    CHECK_OBJECT( tmp_source_name_11 );
    tmp_called_name_10 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_get );
    if ( tmp_called_name_10 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 85;

        goto frame_exception_exit_1;
    }
    tmp_args_element_name_8 = const_str_digest_4971be992dcede15255d25f725b4edbf;
    tmp_subscribed_name_1 = GET_STRING_DICT_VALUE( moduledict_pandas, (Nuitka_StringObject *)const_str_plain_v );

    if (unlikely( tmp_subscribed_name_1 == NULL ))
    {
        tmp_subscribed_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_v );
    }

    if ( tmp_subscribed_name_1 == NULL )
    {
        Py_DECREF( tmp_called_name_10 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "v" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 85;

        goto frame_exception_exit_1;
    }

    tmp_subscript_name_1 = const_str_plain_version;
    tmp_args_element_name_9 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
    if ( tmp_args_element_name_9 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_10 );

        exception_lineno = 85;

        goto frame_exception_exit_1;
    }
    frame_ed0438480de34297226c0f6f8fcf3ee7->m_frame.f_lineno = 85;
    {
        PyObject *call_args[] = { tmp_args_element_name_8, tmp_args_element_name_9 };
        tmp_assign_source_33 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_10, call_args );
    }

    Py_DECREF( tmp_called_name_10 );
    Py_DECREF( tmp_args_element_name_9 );
    if ( tmp_assign_source_33 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 85;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas, (Nuitka_StringObject *)const_str_plain___version__, tmp_assign_source_33 );
    tmp_res = PyDict_DelItem( (PyObject *)moduledict_pandas, const_str_plain_get_versions );
    if ( tmp_res == -1 ) CLEAR_ERROR_OCCURRED();

    if ( tmp_res == -1 )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_versions" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 86;

        goto frame_exception_exit_1;
    }

    tmp_res = PyDict_DelItem( (PyObject *)moduledict_pandas, const_str_plain_v );
    if ( tmp_res == -1 ) CLEAR_ERROR_OCCURRED();

    if ( tmp_res == -1 )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "v" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 86;

        goto frame_exception_exit_1;
    }


    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_ed0438480de34297226c0f6f8fcf3ee7 );
#endif
    popFrameStack();

    assertFrameObject( frame_ed0438480de34297226c0f6f8fcf3ee7 );

    goto frame_no_exception_1;
    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_ed0438480de34297226c0f6f8fcf3ee7 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_ed0438480de34297226c0f6f8fcf3ee7, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_ed0438480de34297226c0f6f8fcf3ee7->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_ed0438480de34297226c0f6f8fcf3ee7, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;
    frame_no_exception_1:;
    tmp_assign_source_34 = const_str_digest_aef36402f3427b74d0131f6acf113f39;
    UPDATE_STRING_DICT0( moduledict_pandas, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_34 );

    return MOD_RETURN_VALUE( module_pandas );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
