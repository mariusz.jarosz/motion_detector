/* Generated code for Python source for module 'pandas.compat.numpy.function'
 * created by Nuitka version 0.5.32.7
 *
 * This code is in part copyright 2018 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The _module_pandas$compat$numpy$function is a Python object pointer of module type. */

/* Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_pandas$compat$numpy$function;
PyDictObject *moduledict_pandas$compat$numpy$function;

/* The module constants used, if any. */
extern PyObject *const_str_plain_var;
extern PyObject *const_str_plain_ndarray;
static PyObject *const_str_plain_RESAMPLER_NUMPY_OPS;
extern PyObject *const_str_digest_78a24cb289d0bfe07c0851ef80e47803;
extern PyObject *const_str_plain_validate_clip_with_axis;
extern PyObject *const_str_plain_metaclass;
static PyObject *const_str_digest_1887ffcaa2a1b3781d3028198580af20;
static PyObject *const_tuple_c3ff0c7af9f35b3577bb79acdfd56d8c_tuple;
static PyObject *const_str_plain_process_skipna;
extern PyObject *const_str_plain_validate_argmin_with_skipna;
extern PyObject *const_str_plain_clip;
static PyObject *const_dict_9f4d59885d17d308693fb57f400e28ec;
extern PyObject *const_str_plain___name__;
extern PyObject *const_str_plain_validate_round;
extern PyObject *const_str_plain_validate_stat_ddof_func;
extern PyObject *const_str_plain_validate_sum;
extern PyObject *const_tuple_str_plain___class___tuple;
static PyObject *const_dict_0d15a901a4a59be49734f297aafa41ce;
extern PyObject *const_str_plain_object;
extern PyObject *const_str_plain___file__;
static PyObject *const_str_plain_COMPRESS_DEFAULTS;
extern PyObject *const_str_plain_arg;
static PyObject *const_dict_a7eec54e725b025c6b8cad7d8970188d;
extern PyObject *const_str_plain_args;
extern PyObject *const_str_plain_max;
extern PyObject *const_str_plain_quicksort;
extern PyObject *const_str_plain_validate_rolling_func;
extern PyObject *const_str_plain_sum;
extern PyObject *const_str_plain_validate_transpose;
static PyObject *const_tuple_str_plain_ascending_str_plain_args_str_plain_kwargs_tuple;
extern PyObject *const_str_plain_any;
extern PyObject *const_str_plain_raise;
static PyObject *const_dict_6341d11a4efcdeaa0034dab29c4edf26;
extern PyObject *const_int_neg_1;
static PyObject *const_str_plain_validate_clip;
static PyObject *const_str_plain_CLIP_DEFAULTS;
static PyObject *const_str_digest_931d3394095777aefbb643ab3ba3e281;
static PyObject *const_dict_1645b279e5395dfd36dcdbf0b6ad9508;
static PyObject *const_tuple_str_plain_skipna_str_plain_args_tuple;
extern PyObject *const_str_plain_mode;
extern PyObject *const_str_digest_77d8fb15a813ae44a7b8d0fe53dbee10;
extern PyObject *const_str_plain_None;
static PyObject *const_str_plain_RESHAPE_DEFAULTS;
extern PyObject *const_str_plain_validate_take_with_convert;
extern PyObject *const_str_plain_validate_resampler_func;
static PyObject *const_str_plain_TAKE_DEFAULTS;
extern PyObject *const_dict_733f34ee36736ef87a60e5feacd2f321;
extern PyObject *const_str_plain_func;
static PyObject *const_str_plain_ARGMINMAX_DEFAULTS;
extern PyObject *const_str_plain_validate_window_func;
static PyObject *const_str_digest_11da2e0cf4ab653c48cb31bda00c225f;
extern PyObject *const_tuple_none_none_none_tuple;
extern PyObject *const_str_plain_reshape;
static PyObject *const_tuple_f0146e445a649ac1a4a0d8e5762303c0_tuple;
static PyObject *const_tuple_str_plain_UnsupportedFunctionCall_tuple;
static PyObject *const_str_digest_ce9fc9ccbad74a97cac165256191b91e;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_plain_is_bool;
extern PyObject *const_str_plain___debug__;
static PyObject *const_tuple_88ada967604d3cd7e84806da94cffead_tuple;
extern PyObject *const_str_plain_transpose;
static PyObject *const_tuple_str_plain_is_integer_str_plain_is_bool_tuple;
static PyObject *const_str_plain_STAT_DDOF_FUNC_DEFAULTS;
extern PyObject *const_str_plain_UnsupportedFunctionCall;
extern PyObject *const_str_plain_validate_mean;
static PyObject *const_str_plain_REPEAT_DEFAULTS;
static PyObject *const_str_digest_656d217b03b241a110b4d9aafbf941cf;
static PyObject *const_str_digest_067df85a277f9fc27f09c3c3db457fd4;
extern PyObject *const_str_plain_numpy;
static PyObject *const_str_digest_010fbc4310049244af01346485335c47;
static PyObject *const_dict_b101e96d8e3a17c6babc9c875bf5726a;
extern PyObject *const_str_plain___qualname__;
static PyObject *const_str_digest_95206160780955a825d9d6c41730597d;
static PyObject *const_str_plain_LOGICAL_FUNC_DEFAULTS;
static PyObject *const_str_digest_321b3b2796ea540087d1662294f37838;
extern PyObject *const_str_plain_min;
static PyObject *const_str_digest_c8e3eac5104b86668a7b33f7d7a11742;
extern PyObject *const_str_plain_validate_take;
static PyObject *const_str_digest_dad771f51f9b65c9900377c5cf931800;
extern PyObject *const_str_digest_af281489cd70cb7b8ea2f52bbb2dbaa6;
static PyObject *const_str_digest_3d350ed25da3f646ef1c56f94de8231e;
static PyObject *const_tuple_str_plain_skipna_str_plain_args_str_plain_kwargs_tuple;
static PyObject *const_str_digest_36060ead743ae11388eade50f3791d13;
extern PyObject *const_str_plain_validate_kwargs;
static PyObject *const_str_plain_numpy_args;
static PyObject *const_str_plain_CUM_FUNC_DEFAULTS;
static PyObject *const_dict_57cff8749cb120e28dd3527c89e75318;
static PyObject *const_str_plain_MINMAX_DEFAULTS;
static PyObject *const_str_plain_validate_reshape;
static PyObject *const_dict_b338527898af006c31118ee99f14b9e7;
static PyObject *const_str_digest_4767b4d940301b42fbdf45ad230ccb88;
extern PyObject *const_str_plain_argsort;
static PyObject *const_dict_ed905960e68befe81857735769b68bb1;
extern PyObject *const_str_plain_e;
extern PyObject *const_str_plain_all;
extern PyObject *const_str_plain_validate_expanding_func;
static PyObject *const_str_plain_validate_cum_func;
static PyObject *const_dict_0bab94f1acda136e36e4f3a0d199ad0c;
static PyObject *const_tuple_str_plain_axis_str_plain_dtype_str_plain_out_tuple;
extern PyObject *const_str_plain_convert;
extern PyObject *const_str_plain_both;
extern PyObject *const_str_plain_validate_argmax;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_plain_repeat;
extern PyObject *const_str_plain_compress;
extern PyObject *const_str_plain_validate_argmin;
extern PyObject *const_str_plain_out;
static PyObject *const_str_plain_CompatValidator;
extern PyObject *const_str_plain_prod;
extern PyObject *const_str_plain_validate_min;
extern PyObject *const_str_plain_name;
static PyObject *const_dict_beaf922b95e152e9819e9297e7b00df0;
static PyObject *const_str_digest_5b73885ba63bd4fb387bd87a8e6c9ca9;
static PyObject *const_str_plain_ALLANY_DEFAULTS;
extern PyObject *const_str_plain_validate_stat_func;
extern PyObject *const_str_plain_validate_args_and_kwargs;
static PyObject *const_str_digest_a5bcc26525e3496b3ca041e208c285cd;
static PyObject *const_tuple_str_plain_method_str_plain_args_str_plain_kwargs_tuple;
static PyObject *const_str_plain_ARGSORT_DEFAULTS_KIND;
extern PyObject *const_str_plain_False;
static PyObject *const_str_plain_SORT_DEFAULTS;
extern PyObject *const_str_plain_argmax;
extern PyObject *const_str_plain_argmin;
static PyObject *const_dict_e83b47b89071cfcbed3a8af2f6814b7a;
static PyObject *const_tuple_str_plain_ndarray_tuple;
static PyObject *const_str_plain_ROUND_DEFAULTS;
static PyObject *const_str_digest_0d9582d19734cfbfc922a1db935b51b9;
extern PyObject *const_str_plain_validate_argsort_with_ascending;
extern PyObject *const_int_0;
extern PyObject *const_str_plain_validate_cumsum;
static PyObject *const_str_digest_e08b81464df8dbfa4d80e287ee8a432d;
extern PyObject *const_str_plain_std;
static PyObject *const_tuple_68279ea4df765abed1d2c84015756f92_tuple;
extern PyObject *const_str_plain_msg;
static PyObject *const_dict_2b811f9b7f6bd0285cfb478e33ee30a5;
extern PyObject *const_str_plain_validate_transpose_for_generic;
static PyObject *const_str_digest_3ad2fbf7f8aba849ac3a7ee1a5a2b338;
static PyObject *const_tuple_str_plain_CompatValidator_tuple_type_object_tuple_tuple;
extern PyObject *const_str_plain_validate_argmax_with_skipna;
static PyObject *const_str_digest_e4aa0c2297935b61953390e5fec0e752;
extern PyObject *const_str_plain_validate_logical_func;
static PyObject *const_dict_eea3f2a677f40c199539a2474c474882;
extern PyObject *const_str_plain_OrderedDict;
extern PyObject *const_str_plain_defaults;
static PyObject *const_dict_63aad700189767b23a7036368ba50802;
static PyObject *const_tuple_03dcd36c754e5293cc073c3987b09edc_tuple;
extern PyObject *const_str_plain_C;
extern PyObject *const_str_plain_validate_all;
static PyObject *const_tuple_ee9b57dd6dbfb86760e56cdfe38900e9_tuple;
extern PyObject *const_str_plain_validate_max;
static PyObject *const_str_digest_a56b470b9b98d99ed72588f4e40af1cd;
static PyObject *const_str_plain_TRANSPOSE_DEFAULTS;
extern PyObject *const_str_plain_ascending;
static PyObject *const_str_plain_STAT_FUNC_DEFAULTS;
extern PyObject *const_str_plain_type;
static PyObject *const_dict_c67bff7881a2f9460f832b506d621d61;
static PyObject *const_str_plain_allowed;
static PyObject *const_dict_4b538bf0e1c9772dfb35c0832cf13801;
extern PyObject *const_str_plain_validate_groupby_func;
static PyObject *const_str_digest_cd109c7512b0f864597bcb41344e500c;
static PyObject *const_dict_ac3c2736753dd3af9122456af46b74ec;
extern PyObject *const_str_plain___cached__;
static PyObject *const_tuple_str_plain_axis_str_plain_args_str_plain_kwargs_tuple;
extern PyObject *const_str_plain___class__;
static PyObject *const_tuple_670d66b460103c129188211b2bd1c4bf_tuple;
extern PyObject *const_tuple_none_tuple;
extern PyObject *const_str_plain_cumsum;
extern PyObject *const_tuple_type_object_tuple;
extern PyObject *const_str_plain___module__;
extern PyObject *const_str_plain_validate_cum_func_with_skipna;
static PyObject *const_dict_2a6badbca8fc560e0db9b9bc67f9f960;
static PyObject *const_tuple_str_plain_convert_str_plain_args_str_plain_kwargs_tuple;
extern PyObject *const_str_plain_is_integer;
extern PyObject *const_str_plain_kind;
extern PyObject *const_str_plain_max_fname_arg_count;
static PyObject *const_dict_eebf4d9cf128668e41d5ad4073195780;
extern PyObject *const_str_plain_skipna;
extern PyObject *const_str_plain_take;
static PyObject *const_dict_a437617822e508113d1685038edffd63;
extern PyObject *const_int_pos_1;
static PyObject *const_str_plain_validate_sort;
extern PyObject *const_str_plain_axis;
extern PyObject *const_dict_21b0a41bf986b828d73b3708697d69bd;
extern PyObject *const_str_plain___prepare__;
extern PyObject *const_str_plain___init__;
extern PyObject *const_int_pos_3;
extern PyObject *const_str_plain_validate_any;
extern PyObject *const_str_plain_dtype;
extern PyObject *const_str_plain_self;
extern PyObject *const_str_plain_inst;
extern PyObject *const_str_plain_validate_args;
extern PyObject *const_str_plain_mean;
extern PyObject *const_str_plain_method;
static PyObject *const_str_digest_9314a832e59ceb71d8a4ce93c01512b9;
extern PyObject *const_str_plain___call__;
extern PyObject *const_str_plain_axes;
extern PyObject *const_str_plain_validate_argsort;
extern PyObject *const_str_plain_round;
static PyObject *const_dict_46eb2478d8de6a3582cd6c8c657dd518;
extern PyObject *const_str_plain_fname;
extern PyObject *const_str_plain_kwargs;
extern PyObject *const_str_plain_sort;
static PyObject *const_str_plain_ARGSORT_DEFAULTS;
static PyObject *const_dict_59c9d706c48d2b98911957257a41eb33;
static PyObject *const_tuple_f07bd4ec6bd6a9403a12e46ed9eeeaaa_tuple;
extern PyObject *const_str_plain_validate_repeat;
static PyObject *const_str_plain_validate_argsort_kind;
extern PyObject *const_str_plain_validate_compress;
extern PyObject *const_tuple_str_plain_OrderedDict_tuple;
extern PyObject *const_str_plain_format;
extern PyObject *const_str_plain_klass;
extern PyObject *const_str_digest_69a010023c9fca5d0ce87758e225ccf2;
static PyObject *const_dict_bcd3f8c147fbe55491de0e3a2c6c8775;
extern PyObject *const_str_plain_order;
static PyObject *module_filename_obj;

static bool constants_created = false;

static void createModuleConstants( void )
{
    const_str_plain_RESAMPLER_NUMPY_OPS = UNSTREAM_STRING( &constant_bin[ 1702914 ], 19, 1 );
    const_str_digest_1887ffcaa2a1b3781d3028198580af20 = UNSTREAM_STRING( &constant_bin[ 1702933 ], 37, 0 );
    const_tuple_c3ff0c7af9f35b3577bb79acdfd56d8c_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_c3ff0c7af9f35b3577bb79acdfd56d8c_tuple, 0, const_str_plain_skipna ); Py_INCREF( const_str_plain_skipna );
    PyTuple_SET_ITEM( const_tuple_c3ff0c7af9f35b3577bb79acdfd56d8c_tuple, 1, const_str_plain_args ); Py_INCREF( const_str_plain_args );
    PyTuple_SET_ITEM( const_tuple_c3ff0c7af9f35b3577bb79acdfd56d8c_tuple, 2, const_str_plain_kwargs ); Py_INCREF( const_str_plain_kwargs );
    PyTuple_SET_ITEM( const_tuple_c3ff0c7af9f35b3577bb79acdfd56d8c_tuple, 3, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    const_str_plain_process_skipna = UNSTREAM_STRING( &constant_bin[ 1702970 ], 14, 1 );
    const_dict_9f4d59885d17d308693fb57f400e28ec = _PyDict_NewPresized( 3 );
    PyDict_SetItem( const_dict_9f4d59885d17d308693fb57f400e28ec, const_str_plain_fname, const_str_plain_argmax );
    PyDict_SetItem( const_dict_9f4d59885d17d308693fb57f400e28ec, const_str_plain_method, const_str_plain_both );
    PyDict_SetItem( const_dict_9f4d59885d17d308693fb57f400e28ec, const_str_plain_max_fname_arg_count, const_int_pos_1 );
    assert( PyDict_Size( const_dict_9f4d59885d17d308693fb57f400e28ec ) == 3 );
    const_dict_0d15a901a4a59be49734f297aafa41ce = _PyDict_NewPresized( 3 );
    PyDict_SetItem( const_dict_0d15a901a4a59be49734f297aafa41ce, const_str_plain_fname, const_str_plain_argsort );
    PyDict_SetItem( const_dict_0d15a901a4a59be49734f297aafa41ce, const_str_plain_max_fname_arg_count, const_int_0 );
    PyDict_SetItem( const_dict_0d15a901a4a59be49734f297aafa41ce, const_str_plain_method, const_str_plain_both );
    assert( PyDict_Size( const_dict_0d15a901a4a59be49734f297aafa41ce ) == 3 );
    const_str_plain_COMPRESS_DEFAULTS = UNSTREAM_STRING( &constant_bin[ 1702984 ], 17, 1 );
    const_dict_a7eec54e725b025c6b8cad7d8970188d = _PyDict_NewPresized( 3 );
    PyDict_SetItem( const_dict_a7eec54e725b025c6b8cad7d8970188d, const_str_plain_fname, const_str_plain_sort );
    PyDict_SetItem( const_dict_a7eec54e725b025c6b8cad7d8970188d, const_str_plain_method, const_str_plain_both );
    PyDict_SetItem( const_dict_a7eec54e725b025c6b8cad7d8970188d, const_str_plain_max_fname_arg_count, const_int_pos_1 );
    assert( PyDict_Size( const_dict_a7eec54e725b025c6b8cad7d8970188d ) == 3 );
    const_tuple_str_plain_ascending_str_plain_args_str_plain_kwargs_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_ascending_str_plain_args_str_plain_kwargs_tuple, 0, const_str_plain_ascending ); Py_INCREF( const_str_plain_ascending );
    PyTuple_SET_ITEM( const_tuple_str_plain_ascending_str_plain_args_str_plain_kwargs_tuple, 1, const_str_plain_args ); Py_INCREF( const_str_plain_args );
    PyTuple_SET_ITEM( const_tuple_str_plain_ascending_str_plain_args_str_plain_kwargs_tuple, 2, const_str_plain_kwargs ); Py_INCREF( const_str_plain_kwargs );
    const_dict_6341d11a4efcdeaa0034dab29c4edf26 = _PyDict_NewPresized( 2 );
    PyDict_SetItem( const_dict_6341d11a4efcdeaa0034dab29c4edf26, const_str_plain_fname, const_str_plain_sort );
    PyDict_SetItem( const_dict_6341d11a4efcdeaa0034dab29c4edf26, const_str_plain_method, const_str_plain_kwargs );
    assert( PyDict_Size( const_dict_6341d11a4efcdeaa0034dab29c4edf26 ) == 2 );
    const_str_plain_validate_clip = UNSTREAM_STRING( &constant_bin[ 1703001 ], 13, 1 );
    const_str_plain_CLIP_DEFAULTS = UNSTREAM_STRING( &constant_bin[ 1703014 ], 13, 1 );
    const_str_digest_931d3394095777aefbb643ab3ba3e281 = UNSTREAM_STRING( &constant_bin[ 1703027 ], 223, 0 );
    const_dict_1645b279e5395dfd36dcdbf0b6ad9508 = _PyDict_NewPresized( 3 );
    PyDict_SetItem( const_dict_1645b279e5395dfd36dcdbf0b6ad9508, const_str_plain_fname, const_str_plain_max );
    PyDict_SetItem( const_dict_1645b279e5395dfd36dcdbf0b6ad9508, const_str_plain_method, const_str_plain_both );
    PyDict_SetItem( const_dict_1645b279e5395dfd36dcdbf0b6ad9508, const_str_plain_max_fname_arg_count, const_int_pos_1 );
    assert( PyDict_Size( const_dict_1645b279e5395dfd36dcdbf0b6ad9508 ) == 3 );
    const_tuple_str_plain_skipna_str_plain_args_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_skipna_str_plain_args_tuple, 0, const_str_plain_skipna ); Py_INCREF( const_str_plain_skipna );
    PyTuple_SET_ITEM( const_tuple_str_plain_skipna_str_plain_args_tuple, 1, const_str_plain_args ); Py_INCREF( const_str_plain_args );
    const_str_plain_RESHAPE_DEFAULTS = UNSTREAM_STRING( &constant_bin[ 1703250 ], 16, 1 );
    const_str_plain_TAKE_DEFAULTS = UNSTREAM_STRING( &constant_bin[ 1703266 ], 13, 1 );
    const_str_plain_ARGMINMAX_DEFAULTS = UNSTREAM_STRING( &constant_bin[ 1703279 ], 18, 1 );
    const_str_digest_11da2e0cf4ab653c48cb31bda00c225f = UNSTREAM_STRING( &constant_bin[ 1703297 ], 36, 0 );
    const_tuple_f0146e445a649ac1a4a0d8e5762303c0_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_f0146e445a649ac1a4a0d8e5762303c0_tuple, 0, const_str_plain_inst ); Py_INCREF( const_str_plain_inst );
    PyTuple_SET_ITEM( const_tuple_f0146e445a649ac1a4a0d8e5762303c0_tuple, 1, const_str_plain_kwargs ); Py_INCREF( const_str_plain_kwargs );
    PyTuple_SET_ITEM( const_tuple_f0146e445a649ac1a4a0d8e5762303c0_tuple, 2, const_str_plain_e ); Py_INCREF( const_str_plain_e );
    PyTuple_SET_ITEM( const_tuple_f0146e445a649ac1a4a0d8e5762303c0_tuple, 3, const_str_plain_klass ); Py_INCREF( const_str_plain_klass );
    PyTuple_SET_ITEM( const_tuple_f0146e445a649ac1a4a0d8e5762303c0_tuple, 4, const_str_plain_msg ); Py_INCREF( const_str_plain_msg );
    const_tuple_str_plain_UnsupportedFunctionCall_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_UnsupportedFunctionCall_tuple, 0, const_str_plain_UnsupportedFunctionCall ); Py_INCREF( const_str_plain_UnsupportedFunctionCall );
    const_str_digest_ce9fc9ccbad74a97cac165256191b91e = UNSTREAM_STRING( &constant_bin[ 1703333 ], 89, 0 );
    const_tuple_88ada967604d3cd7e84806da94cffead_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_88ada967604d3cd7e84806da94cffead_tuple, 0, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    PyTuple_SET_ITEM( const_tuple_88ada967604d3cd7e84806da94cffead_tuple, 1, const_str_plain_args ); Py_INCREF( const_str_plain_args );
    PyTuple_SET_ITEM( const_tuple_88ada967604d3cd7e84806da94cffead_tuple, 2, const_str_plain_kwargs ); Py_INCREF( const_str_plain_kwargs );
    const_str_plain_allowed = UNSTREAM_STRING( &constant_bin[ 39875 ], 7, 1 );
    PyTuple_SET_ITEM( const_tuple_88ada967604d3cd7e84806da94cffead_tuple, 3, const_str_plain_allowed ); Py_INCREF( const_str_plain_allowed );
    const_tuple_str_plain_is_integer_str_plain_is_bool_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_is_integer_str_plain_is_bool_tuple, 0, const_str_plain_is_integer ); Py_INCREF( const_str_plain_is_integer );
    PyTuple_SET_ITEM( const_tuple_str_plain_is_integer_str_plain_is_bool_tuple, 1, const_str_plain_is_bool ); Py_INCREF( const_str_plain_is_bool );
    const_str_plain_STAT_DDOF_FUNC_DEFAULTS = UNSTREAM_STRING( &constant_bin[ 1703422 ], 23, 1 );
    const_str_plain_REPEAT_DEFAULTS = UNSTREAM_STRING( &constant_bin[ 1703445 ], 15, 1 );
    const_str_digest_656d217b03b241a110b4d9aafbf941cf = UNSTREAM_STRING( &constant_bin[ 1703460 ], 948, 0 );
    const_str_digest_067df85a277f9fc27f09c3c3db457fd4 = UNSTREAM_STRING( &constant_bin[ 1704408 ], 81, 0 );
    const_str_digest_010fbc4310049244af01346485335c47 = UNSTREAM_STRING( &constant_bin[ 1704489 ], 37, 0 );
    const_dict_b101e96d8e3a17c6babc9c875bf5726a = _PyDict_NewPresized( 3 );
    PyDict_SetItem( const_dict_b101e96d8e3a17c6babc9c875bf5726a, const_str_plain_fname, const_str_plain_repeat );
    PyDict_SetItem( const_dict_b101e96d8e3a17c6babc9c875bf5726a, const_str_plain_method, const_str_plain_both );
    PyDict_SetItem( const_dict_b101e96d8e3a17c6babc9c875bf5726a, const_str_plain_max_fname_arg_count, const_int_pos_1 );
    assert( PyDict_Size( const_dict_b101e96d8e3a17c6babc9c875bf5726a ) == 3 );
    const_str_digest_95206160780955a825d9d6c41730597d = UNSTREAM_STRING( &constant_bin[ 1704526 ], 94, 0 );
    const_str_plain_LOGICAL_FUNC_DEFAULTS = UNSTREAM_STRING( &constant_bin[ 1704620 ], 21, 1 );
    const_str_digest_321b3b2796ea540087d1662294f37838 = UNSTREAM_STRING( &constant_bin[ 1704641 ], 293, 0 );
    const_str_digest_c8e3eac5104b86668a7b33f7d7a11742 = UNSTREAM_STRING( &constant_bin[ 1704934 ], 289, 0 );
    const_str_digest_dad771f51f9b65c9900377c5cf931800 = UNSTREAM_STRING( &constant_bin[ 1705223 ], 174, 0 );
    const_str_digest_3d350ed25da3f646ef1c56f94de8231e = UNSTREAM_STRING( &constant_bin[ 1705397 ], 24, 0 );
    const_tuple_str_plain_skipna_str_plain_args_str_plain_kwargs_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_skipna_str_plain_args_str_plain_kwargs_tuple, 0, const_str_plain_skipna ); Py_INCREF( const_str_plain_skipna );
    PyTuple_SET_ITEM( const_tuple_str_plain_skipna_str_plain_args_str_plain_kwargs_tuple, 1, const_str_plain_args ); Py_INCREF( const_str_plain_args );
    PyTuple_SET_ITEM( const_tuple_str_plain_skipna_str_plain_args_str_plain_kwargs_tuple, 2, const_str_plain_kwargs ); Py_INCREF( const_str_plain_kwargs );
    const_str_digest_36060ead743ae11388eade50f3791d13 = UNSTREAM_STRING( &constant_bin[ 1705421 ], 289, 0 );
    const_str_plain_numpy_args = UNSTREAM_STRING( &constant_bin[ 1705710 ], 10, 1 );
    const_str_plain_CUM_FUNC_DEFAULTS = UNSTREAM_STRING( &constant_bin[ 1705720 ], 17, 1 );
    const_dict_57cff8749cb120e28dd3527c89e75318 = _PyDict_NewPresized( 3 );
    PyDict_SetItem( const_dict_57cff8749cb120e28dd3527c89e75318, const_str_plain_fname, const_str_plain_argmin );
    PyDict_SetItem( const_dict_57cff8749cb120e28dd3527c89e75318, const_str_plain_method, const_str_plain_both );
    PyDict_SetItem( const_dict_57cff8749cb120e28dd3527c89e75318, const_str_plain_max_fname_arg_count, const_int_pos_1 );
    assert( PyDict_Size( const_dict_57cff8749cb120e28dd3527c89e75318 ) == 3 );
    const_str_plain_MINMAX_DEFAULTS = UNSTREAM_STRING( &constant_bin[ 1703282 ], 15, 1 );
    const_str_plain_validate_reshape = UNSTREAM_STRING( &constant_bin[ 1705737 ], 16, 1 );
    const_dict_b338527898af006c31118ee99f14b9e7 = _PyDict_NewPresized( 3 );
    PyDict_SetItem( const_dict_b338527898af006c31118ee99f14b9e7, const_str_plain_fname, const_str_plain_round );
    PyDict_SetItem( const_dict_b338527898af006c31118ee99f14b9e7, const_str_plain_method, const_str_plain_both );
    PyDict_SetItem( const_dict_b338527898af006c31118ee99f14b9e7, const_str_plain_max_fname_arg_count, const_int_pos_1 );
    assert( PyDict_Size( const_dict_b338527898af006c31118ee99f14b9e7 ) == 3 );
    const_str_digest_4767b4d940301b42fbdf45ad230ccb88 = UNSTREAM_STRING( &constant_bin[ 1705753 ], 143, 0 );
    const_dict_ed905960e68befe81857735769b68bb1 = _PyDict_NewPresized( 3 );
    PyDict_SetItem( const_dict_ed905960e68befe81857735769b68bb1, const_str_plain_fname, const_str_plain_all );
    PyDict_SetItem( const_dict_ed905960e68befe81857735769b68bb1, const_str_plain_method, const_str_plain_both );
    PyDict_SetItem( const_dict_ed905960e68befe81857735769b68bb1, const_str_plain_max_fname_arg_count, const_int_pos_1 );
    assert( PyDict_Size( const_dict_ed905960e68befe81857735769b68bb1 ) == 3 );
    const_str_plain_validate_cum_func = UNSTREAM_STRING( &constant_bin[ 1705896 ], 17, 1 );
    const_dict_0bab94f1acda136e36e4f3a0d199ad0c = _PyDict_NewPresized( 3 );
    PyDict_SetItem( const_dict_0bab94f1acda136e36e4f3a0d199ad0c, const_str_plain_fname, const_str_plain_transpose );
    PyDict_SetItem( const_dict_0bab94f1acda136e36e4f3a0d199ad0c, const_str_plain_method, const_str_plain_both );
    PyDict_SetItem( const_dict_0bab94f1acda136e36e4f3a0d199ad0c, const_str_plain_max_fname_arg_count, const_int_0 );
    assert( PyDict_Size( const_dict_0bab94f1acda136e36e4f3a0d199ad0c ) == 3 );
    const_tuple_str_plain_axis_str_plain_dtype_str_plain_out_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_axis_str_plain_dtype_str_plain_out_tuple, 0, const_str_plain_axis ); Py_INCREF( const_str_plain_axis );
    PyTuple_SET_ITEM( const_tuple_str_plain_axis_str_plain_dtype_str_plain_out_tuple, 1, const_str_plain_dtype ); Py_INCREF( const_str_plain_dtype );
    PyTuple_SET_ITEM( const_tuple_str_plain_axis_str_plain_dtype_str_plain_out_tuple, 2, const_str_plain_out ); Py_INCREF( const_str_plain_out );
    const_str_plain_CompatValidator = UNSTREAM_STRING( &constant_bin[ 1705397 ], 15, 1 );
    const_dict_beaf922b95e152e9819e9297e7b00df0 = _PyDict_NewPresized( 1 );
    PyDict_SetItem( const_dict_beaf922b95e152e9819e9297e7b00df0, const_str_plain_axes, Py_None );
    assert( PyDict_Size( const_dict_beaf922b95e152e9819e9297e7b00df0 ) == 1 );
    const_str_digest_5b73885ba63bd4fb387bd87a8e6c9ca9 = UNSTREAM_STRING( &constant_bin[ 1705913 ], 83, 0 );
    const_str_plain_ALLANY_DEFAULTS = UNSTREAM_STRING( &constant_bin[ 1705996 ], 15, 1 );
    const_str_digest_a5bcc26525e3496b3ca041e208c285cd = UNSTREAM_STRING( &constant_bin[ 1706011 ], 79, 0 );
    const_tuple_str_plain_method_str_plain_args_str_plain_kwargs_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_method_str_plain_args_str_plain_kwargs_tuple, 0, const_str_plain_method ); Py_INCREF( const_str_plain_method );
    PyTuple_SET_ITEM( const_tuple_str_plain_method_str_plain_args_str_plain_kwargs_tuple, 1, const_str_plain_args ); Py_INCREF( const_str_plain_args );
    PyTuple_SET_ITEM( const_tuple_str_plain_method_str_plain_args_str_plain_kwargs_tuple, 2, const_str_plain_kwargs ); Py_INCREF( const_str_plain_kwargs );
    const_str_plain_ARGSORT_DEFAULTS_KIND = UNSTREAM_STRING( &constant_bin[ 1706090 ], 21, 1 );
    const_str_plain_SORT_DEFAULTS = UNSTREAM_STRING( &constant_bin[ 1706093 ], 13, 1 );
    const_dict_e83b47b89071cfcbed3a8af2f6814b7a = _PyDict_NewPresized( 2 );
    PyDict_SetItem( const_dict_e83b47b89071cfcbed3a8af2f6814b7a, const_str_plain_max_fname_arg_count, const_int_pos_3 );
    PyDict_SetItem( const_dict_e83b47b89071cfcbed3a8af2f6814b7a, const_str_plain_method, const_str_plain_both );
    assert( PyDict_Size( const_dict_e83b47b89071cfcbed3a8af2f6814b7a ) == 2 );
    const_tuple_str_plain_ndarray_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_ndarray_tuple, 0, const_str_plain_ndarray ); Py_INCREF( const_str_plain_ndarray );
    const_str_plain_ROUND_DEFAULTS = UNSTREAM_STRING( &constant_bin[ 1706111 ], 14, 1 );
    const_str_digest_0d9582d19734cfbfc922a1db935b51b9 = UNSTREAM_STRING( &constant_bin[ 1706125 ], 87, 0 );
    const_str_digest_e08b81464df8dbfa4d80e287ee8a432d = UNSTREAM_STRING( &constant_bin[ 1706212 ], 242, 0 );
    const_tuple_68279ea4df765abed1d2c84015756f92_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_68279ea4df765abed1d2c84015756f92_tuple, 0, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    PyTuple_SET_ITEM( const_tuple_68279ea4df765abed1d2c84015756f92_tuple, 1, const_str_plain_args ); Py_INCREF( const_str_plain_args );
    PyTuple_SET_ITEM( const_tuple_68279ea4df765abed1d2c84015756f92_tuple, 2, const_str_plain_kwargs ); Py_INCREF( const_str_plain_kwargs );
    PyTuple_SET_ITEM( const_tuple_68279ea4df765abed1d2c84015756f92_tuple, 3, const_str_plain_numpy_args ); Py_INCREF( const_str_plain_numpy_args );
    PyTuple_SET_ITEM( const_tuple_68279ea4df765abed1d2c84015756f92_tuple, 4, const_str_plain_msg ); Py_INCREF( const_str_plain_msg );
    PyTuple_SET_ITEM( const_tuple_68279ea4df765abed1d2c84015756f92_tuple, 5, const_str_plain_arg ); Py_INCREF( const_str_plain_arg );
    const_dict_2b811f9b7f6bd0285cfb478e33ee30a5 = _PyDict_NewPresized( 1 );
    PyDict_SetItem( const_dict_2b811f9b7f6bd0285cfb478e33ee30a5, const_str_plain_out, Py_None );
    assert( PyDict_Size( const_dict_2b811f9b7f6bd0285cfb478e33ee30a5 ) == 1 );
    const_str_digest_3ad2fbf7f8aba849ac3a7ee1a5a2b338 = UNSTREAM_STRING( &constant_bin[ 1704497 ], 28, 0 );
    const_tuple_str_plain_CompatValidator_tuple_type_object_tuple_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_CompatValidator_tuple_type_object_tuple_tuple, 0, const_str_plain_CompatValidator ); Py_INCREF( const_str_plain_CompatValidator );
    PyTuple_SET_ITEM( const_tuple_str_plain_CompatValidator_tuple_type_object_tuple_tuple, 1, const_tuple_type_object_tuple ); Py_INCREF( const_tuple_type_object_tuple );
    const_str_digest_e4aa0c2297935b61953390e5fec0e752 = UNSTREAM_STRING( &constant_bin[ 1706454 ], 22, 0 );
    const_dict_eea3f2a677f40c199539a2474c474882 = _PyDict_NewPresized( 2 );
    PyDict_SetItem( const_dict_eea3f2a677f40c199539a2474c474882, const_str_plain_fname, const_str_plain_take );
    PyDict_SetItem( const_dict_eea3f2a677f40c199539a2474c474882, const_str_plain_method, const_str_plain_kwargs );
    assert( PyDict_Size( const_dict_eea3f2a677f40c199539a2474c474882 ) == 2 );
    const_dict_63aad700189767b23a7036368ba50802 = _PyDict_NewPresized( 3 );
    PyDict_SetItem( const_dict_63aad700189767b23a7036368ba50802, const_str_plain_fname, const_str_plain_cumsum );
    PyDict_SetItem( const_dict_63aad700189767b23a7036368ba50802, const_str_plain_method, const_str_plain_both );
    PyDict_SetItem( const_dict_63aad700189767b23a7036368ba50802, const_str_plain_max_fname_arg_count, const_int_pos_1 );
    assert( PyDict_Size( const_dict_63aad700189767b23a7036368ba50802 ) == 3 );
    const_tuple_03dcd36c754e5293cc073c3987b09edc_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_03dcd36c754e5293cc073c3987b09edc_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_03dcd36c754e5293cc073c3987b09edc_tuple, 1, const_str_plain_defaults ); Py_INCREF( const_str_plain_defaults );
    PyTuple_SET_ITEM( const_tuple_03dcd36c754e5293cc073c3987b09edc_tuple, 2, const_str_plain_fname ); Py_INCREF( const_str_plain_fname );
    PyTuple_SET_ITEM( const_tuple_03dcd36c754e5293cc073c3987b09edc_tuple, 3, const_str_plain_method ); Py_INCREF( const_str_plain_method );
    PyTuple_SET_ITEM( const_tuple_03dcd36c754e5293cc073c3987b09edc_tuple, 4, const_str_plain_max_fname_arg_count ); Py_INCREF( const_str_plain_max_fname_arg_count );
    const_tuple_ee9b57dd6dbfb86760e56cdfe38900e9_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_ee9b57dd6dbfb86760e56cdfe38900e9_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_ee9b57dd6dbfb86760e56cdfe38900e9_tuple, 1, const_str_plain_args ); Py_INCREF( const_str_plain_args );
    PyTuple_SET_ITEM( const_tuple_ee9b57dd6dbfb86760e56cdfe38900e9_tuple, 2, const_str_plain_kwargs ); Py_INCREF( const_str_plain_kwargs );
    PyTuple_SET_ITEM( const_tuple_ee9b57dd6dbfb86760e56cdfe38900e9_tuple, 3, const_str_plain_fname ); Py_INCREF( const_str_plain_fname );
    PyTuple_SET_ITEM( const_tuple_ee9b57dd6dbfb86760e56cdfe38900e9_tuple, 4, const_str_plain_max_fname_arg_count ); Py_INCREF( const_str_plain_max_fname_arg_count );
    PyTuple_SET_ITEM( const_tuple_ee9b57dd6dbfb86760e56cdfe38900e9_tuple, 5, const_str_plain_method ); Py_INCREF( const_str_plain_method );
    const_str_digest_a56b470b9b98d99ed72588f4e40af1cd = UNSTREAM_STRING( &constant_bin[ 1706476 ], 28, 0 );
    const_str_plain_TRANSPOSE_DEFAULTS = UNSTREAM_STRING( &constant_bin[ 1706504 ], 18, 1 );
    const_str_plain_STAT_FUNC_DEFAULTS = UNSTREAM_STRING( &constant_bin[ 1706522 ], 18, 1 );
    const_dict_c67bff7881a2f9460f832b506d621d61 = _PyDict_NewPresized( 2 );
    PyDict_SetItem( const_dict_c67bff7881a2f9460f832b506d621d61, const_str_plain_method, const_str_plain_both );
    PyDict_SetItem( const_dict_c67bff7881a2f9460f832b506d621d61, const_str_plain_max_fname_arg_count, const_int_pos_1 );
    assert( PyDict_Size( const_dict_c67bff7881a2f9460f832b506d621d61 ) == 2 );
    const_dict_4b538bf0e1c9772dfb35c0832cf13801 = _PyDict_NewPresized( 3 );
    PyDict_SetItem( const_dict_4b538bf0e1c9772dfb35c0832cf13801, const_str_plain_fname, const_str_plain_any );
    PyDict_SetItem( const_dict_4b538bf0e1c9772dfb35c0832cf13801, const_str_plain_method, const_str_plain_both );
    PyDict_SetItem( const_dict_4b538bf0e1c9772dfb35c0832cf13801, const_str_plain_max_fname_arg_count, const_int_pos_1 );
    assert( PyDict_Size( const_dict_4b538bf0e1c9772dfb35c0832cf13801 ) == 3 );
    const_str_digest_cd109c7512b0f864597bcb41344e500c = UNSTREAM_STRING( &constant_bin[ 1706540 ], 24, 0 );
    const_dict_ac3c2736753dd3af9122456af46b74ec = _PyDict_NewPresized( 1 );
    PyDict_SetItem( const_dict_ac3c2736753dd3af9122456af46b74ec, const_str_plain_max_fname_arg_count, const_int_pos_3 );
    assert( PyDict_Size( const_dict_ac3c2736753dd3af9122456af46b74ec ) == 1 );
    const_tuple_str_plain_axis_str_plain_args_str_plain_kwargs_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_axis_str_plain_args_str_plain_kwargs_tuple, 0, const_str_plain_axis ); Py_INCREF( const_str_plain_axis );
    PyTuple_SET_ITEM( const_tuple_str_plain_axis_str_plain_args_str_plain_kwargs_tuple, 1, const_str_plain_args ); Py_INCREF( const_str_plain_args );
    PyTuple_SET_ITEM( const_tuple_str_plain_axis_str_plain_args_str_plain_kwargs_tuple, 2, const_str_plain_kwargs ); Py_INCREF( const_str_plain_kwargs );
    const_tuple_670d66b460103c129188211b2bd1c4bf_tuple = PyTuple_New( 7 );
    PyTuple_SET_ITEM( const_tuple_670d66b460103c129188211b2bd1c4bf_tuple, 0, const_str_plain_min ); Py_INCREF( const_str_plain_min );
    PyTuple_SET_ITEM( const_tuple_670d66b460103c129188211b2bd1c4bf_tuple, 1, const_str_plain_max ); Py_INCREF( const_str_plain_max );
    PyTuple_SET_ITEM( const_tuple_670d66b460103c129188211b2bd1c4bf_tuple, 2, const_str_plain_sum ); Py_INCREF( const_str_plain_sum );
    PyTuple_SET_ITEM( const_tuple_670d66b460103c129188211b2bd1c4bf_tuple, 3, const_str_plain_prod ); Py_INCREF( const_str_plain_prod );
    PyTuple_SET_ITEM( const_tuple_670d66b460103c129188211b2bd1c4bf_tuple, 4, const_str_plain_mean ); Py_INCREF( const_str_plain_mean );
    PyTuple_SET_ITEM( const_tuple_670d66b460103c129188211b2bd1c4bf_tuple, 5, const_str_plain_std ); Py_INCREF( const_str_plain_std );
    PyTuple_SET_ITEM( const_tuple_670d66b460103c129188211b2bd1c4bf_tuple, 6, const_str_plain_var ); Py_INCREF( const_str_plain_var );
    const_dict_2a6badbca8fc560e0db9b9bc67f9f960 = _PyDict_NewPresized( 3 );
    PyDict_SetItem( const_dict_2a6badbca8fc560e0db9b9bc67f9f960, const_str_plain_fname, const_str_plain_min );
    PyDict_SetItem( const_dict_2a6badbca8fc560e0db9b9bc67f9f960, const_str_plain_method, const_str_plain_both );
    PyDict_SetItem( const_dict_2a6badbca8fc560e0db9b9bc67f9f960, const_str_plain_max_fname_arg_count, const_int_pos_1 );
    assert( PyDict_Size( const_dict_2a6badbca8fc560e0db9b9bc67f9f960 ) == 3 );
    const_tuple_str_plain_convert_str_plain_args_str_plain_kwargs_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_convert_str_plain_args_str_plain_kwargs_tuple, 0, const_str_plain_convert ); Py_INCREF( const_str_plain_convert );
    PyTuple_SET_ITEM( const_tuple_str_plain_convert_str_plain_args_str_plain_kwargs_tuple, 1, const_str_plain_args ); Py_INCREF( const_str_plain_args );
    PyTuple_SET_ITEM( const_tuple_str_plain_convert_str_plain_args_str_plain_kwargs_tuple, 2, const_str_plain_kwargs ); Py_INCREF( const_str_plain_kwargs );
    const_dict_eebf4d9cf128668e41d5ad4073195780 = _PyDict_NewPresized( 3 );
    PyDict_SetItem( const_dict_eebf4d9cf128668e41d5ad4073195780, const_str_plain_fname, const_str_plain_reshape );
    PyDict_SetItem( const_dict_eebf4d9cf128668e41d5ad4073195780, const_str_plain_method, const_str_plain_both );
    PyDict_SetItem( const_dict_eebf4d9cf128668e41d5ad4073195780, const_str_plain_max_fname_arg_count, const_int_pos_1 );
    assert( PyDict_Size( const_dict_eebf4d9cf128668e41d5ad4073195780 ) == 3 );
    const_dict_a437617822e508113d1685038edffd63 = _PyDict_NewPresized( 1 );
    PyDict_SetItem( const_dict_a437617822e508113d1685038edffd63, const_str_plain_method, const_str_plain_kwargs );
    assert( PyDict_Size( const_dict_a437617822e508113d1685038edffd63 ) == 1 );
    const_str_plain_validate_sort = UNSTREAM_STRING( &constant_bin[ 1706564 ], 13, 1 );
    const_str_digest_9314a832e59ceb71d8a4ce93c01512b9 = UNSTREAM_STRING( &constant_bin[ 1706577 ], 263, 0 );
    const_dict_46eb2478d8de6a3582cd6c8c657dd518 = _PyDict_NewPresized( 3 );
    PyDict_SetItem( const_dict_46eb2478d8de6a3582cd6c8c657dd518, const_str_plain_fname, const_str_plain_compress );
    PyDict_SetItem( const_dict_46eb2478d8de6a3582cd6c8c657dd518, const_str_plain_method, const_str_plain_both );
    PyDict_SetItem( const_dict_46eb2478d8de6a3582cd6c8c657dd518, const_str_plain_max_fname_arg_count, const_int_pos_1 );
    assert( PyDict_Size( const_dict_46eb2478d8de6a3582cd6c8c657dd518 ) == 3 );
    const_str_plain_ARGSORT_DEFAULTS = UNSTREAM_STRING( &constant_bin[ 1706090 ], 16, 1 );
    const_dict_59c9d706c48d2b98911957257a41eb33 = _PyDict_NewPresized( 3 );
    PyDict_SetItem( const_dict_59c9d706c48d2b98911957257a41eb33, const_str_plain_fname, const_str_plain_mean );
    PyDict_SetItem( const_dict_59c9d706c48d2b98911957257a41eb33, const_str_plain_method, const_str_plain_both );
    PyDict_SetItem( const_dict_59c9d706c48d2b98911957257a41eb33, const_str_plain_max_fname_arg_count, const_int_pos_1 );
    assert( PyDict_Size( const_dict_59c9d706c48d2b98911957257a41eb33 ) == 3 );
    const_tuple_f07bd4ec6bd6a9403a12e46ed9eeeaaa_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_f07bd4ec6bd6a9403a12e46ed9eeeaaa_tuple, 0, const_str_plain_validate_args ); Py_INCREF( const_str_plain_validate_args );
    PyTuple_SET_ITEM( const_tuple_f07bd4ec6bd6a9403a12e46ed9eeeaaa_tuple, 1, const_str_plain_validate_kwargs ); Py_INCREF( const_str_plain_validate_kwargs );
    PyTuple_SET_ITEM( const_tuple_f07bd4ec6bd6a9403a12e46ed9eeeaaa_tuple, 2, const_str_plain_validate_args_and_kwargs ); Py_INCREF( const_str_plain_validate_args_and_kwargs );
    const_str_plain_validate_argsort_kind = UNSTREAM_STRING( &constant_bin[ 1706840 ], 21, 1 );
    const_dict_bcd3f8c147fbe55491de0e3a2c6c8775 = _PyDict_NewPresized( 3 );
    PyDict_SetItem( const_dict_bcd3f8c147fbe55491de0e3a2c6c8775, const_str_plain_fname, const_str_plain_clip );
    PyDict_SetItem( const_dict_bcd3f8c147fbe55491de0e3a2c6c8775, const_str_plain_method, const_str_plain_both );
    PyDict_SetItem( const_dict_bcd3f8c147fbe55491de0e3a2c6c8775, const_str_plain_max_fname_arg_count, const_int_pos_3 );
    assert( PyDict_Size( const_dict_bcd3f8c147fbe55491de0e3a2c6c8775 ) == 3 );

    constants_created = true;
}

#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_pandas$compat$numpy$function( void )
{
    // The module may not have been used at all.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_8cc68a7a34d1d402f731b2d00596f1d3;
static PyCodeObject *codeobj_832ad82c1fd8630f5016f41edc471736;
static PyCodeObject *codeobj_1e3f0419c049747f6785cc3c7311ed4c;
static PyCodeObject *codeobj_4a9bd1a3a08fe4f911ad739be813d1a0;
static PyCodeObject *codeobj_43e94ecae646e3d8f1f3ca9a745e993b;
static PyCodeObject *codeobj_713f7fe3df7bd6648893df8dc083acaa;
static PyCodeObject *codeobj_cb583cf8b472c057f6e631c41433adb2;
static PyCodeObject *codeobj_148a360f13c6662190bfd9d000600ba7;
static PyCodeObject *codeobj_a66b33f8faf355add77e1dae7c2a077e;
static PyCodeObject *codeobj_2f5bab5df7791de2e0bdef305e996143;
static PyCodeObject *codeobj_220060a11a280eed1c6dad14776295d7;
static PyCodeObject *codeobj_2ca6a45202a14a8e4efcd82e44936641;
static PyCodeObject *codeobj_decc215cc11da815a9438d30e9a73c92;
static PyCodeObject *codeobj_8dc1d850a1d11f56ce7e376c4bba1267;
static PyCodeObject *codeobj_a00567b750d5f828b6f606f204c47375;
static PyCodeObject *codeobj_664a2c3bc29db525ef3481f05ac9f980;
static PyCodeObject *codeobj_e5daa672e9453708a21f3999fd18e914;

static void createModuleCodeObjects(void)
{
    module_filename_obj = const_str_digest_95206160780955a825d9d6c41730597d;
    codeobj_8cc68a7a34d1d402f731b2d00596f1d3 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_010fbc4310049244af01346485335c47, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_832ad82c1fd8630f5016f41edc471736 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_CompatValidator, 29, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_1e3f0419c049747f6785cc3c7311ed4c = MAKE_CODEOBJ( module_filename_obj, const_str_plain___call__, 38, const_tuple_ee9b57dd6dbfb86760e56cdfe38900e9_tuple, 6, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_4a9bd1a3a08fe4f911ad739be813d1a0 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 31, const_tuple_03dcd36c754e5293cc073c3987b09edc_tuple, 5, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_43e94ecae646e3d8f1f3ca9a745e993b = MAKE_CODEOBJ( module_filename_obj, const_str_plain_process_skipna, 67, const_tuple_str_plain_skipna_str_plain_args_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_713f7fe3df7bd6648893df8dc083acaa = MAKE_CODEOBJ( module_filename_obj, const_str_plain_validate_argmax_with_skipna, 89, const_tuple_str_plain_skipna_str_plain_args_str_plain_kwargs_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_cb583cf8b472c057f6e631c41433adb2 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_validate_argmin_with_skipna, 75, const_tuple_str_plain_skipna_str_plain_args_str_plain_kwargs_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_148a360f13c6662190bfd9d000600ba7 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_validate_argsort_with_ascending, 119, const_tuple_str_plain_ascending_str_plain_args_str_plain_kwargs_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_a66b33f8faf355add77e1dae7c2a077e = MAKE_CODEOBJ( module_filename_obj, const_str_plain_validate_clip_with_axis, 141, const_tuple_str_plain_axis_str_plain_args_str_plain_kwargs_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_2f5bab5df7791de2e0bdef305e996143 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_validate_cum_func_with_skipna, 172, const_tuple_c3ff0c7af9f35b3577bb79acdfd56d8c_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_220060a11a280eed1c6dad14776295d7 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_validate_expanding_func, 312, const_tuple_68279ea4df765abed1d2c84015756f92_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_2ca6a45202a14a8e4efcd82e44936641 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_validate_groupby_func, 326, const_tuple_88ada967604d3cd7e84806da94cffead_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_decc215cc11da815a9438d30e9a73c92 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_validate_resampler_func, 349, const_tuple_str_plain_method_str_plain_args_str_plain_kwargs_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_8dc1d850a1d11f56ce7e376c4bba1267 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_validate_rolling_func, 298, const_tuple_68279ea4df765abed1d2c84015756f92_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_a00567b750d5f828b6f606f204c47375 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_validate_take_with_convert, 246, const_tuple_str_plain_convert_str_plain_args_str_plain_kwargs_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_664a2c3bc29db525ef3481f05ac9f980 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_validate_transpose_for_generic, 267, const_tuple_f0146e445a649ac1a4a0d8e5762303c0_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_e5daa672e9453708a21f3999fd18e914 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_validate_window_func, 284, const_tuple_68279ea4df765abed1d2c84015756f92_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
}

// The module function declarations.
static PyObject *MAKE_FUNCTION_pandas$compat$numpy$function$$$function_10_validate_transpose_for_generic(  );


static PyObject *MAKE_FUNCTION_pandas$compat$numpy$function$$$function_11_validate_window_func(  );


static PyObject *MAKE_FUNCTION_pandas$compat$numpy$function$$$function_12_validate_rolling_func(  );


static PyObject *MAKE_FUNCTION_pandas$compat$numpy$function$$$function_13_validate_expanding_func(  );


static PyObject *MAKE_FUNCTION_pandas$compat$numpy$function$$$function_14_validate_groupby_func( PyObject *defaults );


static PyObject *MAKE_FUNCTION_pandas$compat$numpy$function$$$function_15_validate_resampler_func(  );


static PyObject *MAKE_FUNCTION_pandas$compat$numpy$function$$$function_1___init__( PyObject *defaults );


static PyObject *MAKE_FUNCTION_pandas$compat$numpy$function$$$function_2___call__( PyObject *defaults );


static PyObject *MAKE_FUNCTION_pandas$compat$numpy$function$$$function_3_process_skipna(  );


static PyObject *MAKE_FUNCTION_pandas$compat$numpy$function$$$function_4_validate_argmin_with_skipna(  );


static PyObject *MAKE_FUNCTION_pandas$compat$numpy$function$$$function_5_validate_argmax_with_skipna(  );


static PyObject *MAKE_FUNCTION_pandas$compat$numpy$function$$$function_6_validate_argsort_with_ascending(  );


static PyObject *MAKE_FUNCTION_pandas$compat$numpy$function$$$function_7_validate_clip_with_axis(  );


static PyObject *MAKE_FUNCTION_pandas$compat$numpy$function$$$function_8_validate_cum_func_with_skipna(  );


static PyObject *MAKE_FUNCTION_pandas$compat$numpy$function$$$function_9_validate_take_with_convert(  );


// The module function definitions.
static PyObject *impl_pandas$compat$numpy$function$$$function_1___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_defaults = python_pars[ 1 ];
    PyObject *par_fname = python_pars[ 2 ];
    PyObject *par_method = python_pars[ 3 ];
    PyObject *par_max_fname_arg_count = python_pars[ 4 ];
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_assattr_name_1;
    PyObject *tmp_assattr_name_2;
    PyObject *tmp_assattr_name_3;
    PyObject *tmp_assattr_name_4;
    PyObject *tmp_assattr_target_1;
    PyObject *tmp_assattr_target_2;
    PyObject *tmp_assattr_target_3;
    PyObject *tmp_assattr_target_4;
    bool tmp_result;
    PyObject *tmp_return_value;
    static struct Nuitka_FrameObject *cache_frame_4a9bd1a3a08fe4f911ad739be813d1a0 = NULL;

    struct Nuitka_FrameObject *frame_4a9bd1a3a08fe4f911ad739be813d1a0;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_4a9bd1a3a08fe4f911ad739be813d1a0, codeobj_4a9bd1a3a08fe4f911ad739be813d1a0, module_pandas$compat$numpy$function, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_4a9bd1a3a08fe4f911ad739be813d1a0 = cache_frame_4a9bd1a3a08fe4f911ad739be813d1a0;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_4a9bd1a3a08fe4f911ad739be813d1a0 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_4a9bd1a3a08fe4f911ad739be813d1a0 ) == 2 ); // Frame stack

    // Framed code:
    tmp_assattr_name_1 = par_fname;

    CHECK_OBJECT( tmp_assattr_name_1 );
    tmp_assattr_target_1 = par_self;

    CHECK_OBJECT( tmp_assattr_target_1 );
    tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_fname, tmp_assattr_name_1 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 33;
        type_description_1 = "ooooo";
        goto frame_exception_exit_1;
    }
    tmp_assattr_name_2 = par_method;

    if ( tmp_assattr_name_2 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "method" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 34;
        type_description_1 = "ooooo";
        goto frame_exception_exit_1;
    }

    tmp_assattr_target_2 = par_self;

    if ( tmp_assattr_target_2 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "self" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 34;
        type_description_1 = "ooooo";
        goto frame_exception_exit_1;
    }

    tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_method, tmp_assattr_name_2 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 34;
        type_description_1 = "ooooo";
        goto frame_exception_exit_1;
    }
    tmp_assattr_name_3 = par_defaults;

    if ( tmp_assattr_name_3 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "defaults" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 35;
        type_description_1 = "ooooo";
        goto frame_exception_exit_1;
    }

    tmp_assattr_target_3 = par_self;

    if ( tmp_assattr_target_3 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "self" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 35;
        type_description_1 = "ooooo";
        goto frame_exception_exit_1;
    }

    tmp_result = SET_ATTRIBUTE( tmp_assattr_target_3, const_str_plain_defaults, tmp_assattr_name_3 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 35;
        type_description_1 = "ooooo";
        goto frame_exception_exit_1;
    }
    tmp_assattr_name_4 = par_max_fname_arg_count;

    if ( tmp_assattr_name_4 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "max_fname_arg_count" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 36;
        type_description_1 = "ooooo";
        goto frame_exception_exit_1;
    }

    tmp_assattr_target_4 = par_self;

    if ( tmp_assattr_target_4 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "self" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 36;
        type_description_1 = "ooooo";
        goto frame_exception_exit_1;
    }

    tmp_result = SET_ATTRIBUTE( tmp_assattr_target_4, const_str_plain_max_fname_arg_count, tmp_assattr_name_4 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 36;
        type_description_1 = "ooooo";
        goto frame_exception_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_4a9bd1a3a08fe4f911ad739be813d1a0 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_4a9bd1a3a08fe4f911ad739be813d1a0 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_4a9bd1a3a08fe4f911ad739be813d1a0, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_4a9bd1a3a08fe4f911ad739be813d1a0->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_4a9bd1a3a08fe4f911ad739be813d1a0, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_4a9bd1a3a08fe4f911ad739be813d1a0,
        type_description_1,
        par_self,
        par_defaults,
        par_fname,
        par_method,
        par_max_fname_arg_count
    );


    // Release cached frame.
    if ( frame_4a9bd1a3a08fe4f911ad739be813d1a0 == cache_frame_4a9bd1a3a08fe4f911ad739be813d1a0 )
    {
        Py_DECREF( frame_4a9bd1a3a08fe4f911ad739be813d1a0 );
    }
    cache_frame_4a9bd1a3a08fe4f911ad739be813d1a0 = NULL;

    assertFrameObject( frame_4a9bd1a3a08fe4f911ad739be813d1a0 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$compat$numpy$function$$$function_1___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_self );
    par_self = NULL;

    Py_XDECREF( par_defaults );
    par_defaults = NULL;

    Py_XDECREF( par_fname );
    par_fname = NULL;

    Py_XDECREF( par_method );
    par_method = NULL;

    Py_XDECREF( par_max_fname_arg_count );
    par_max_fname_arg_count = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_self );
    par_self = NULL;

    Py_XDECREF( par_defaults );
    par_defaults = NULL;

    Py_XDECREF( par_fname );
    par_fname = NULL;

    Py_XDECREF( par_method );
    par_method = NULL;

    Py_XDECREF( par_max_fname_arg_count );
    par_max_fname_arg_count = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$compat$numpy$function$$$function_1___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$compat$numpy$function$$$function_2___call__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_args = python_pars[ 1 ];
    PyObject *par_kwargs = python_pars[ 2 ];
    PyObject *par_fname = python_pars[ 3 ];
    PyObject *par_max_fname_arg_count = python_pars[ 4 ];
    PyObject *par_method = python_pars[ 5 ];
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_args_element_name_1;
    PyObject *tmp_args_element_name_2;
    PyObject *tmp_args_element_name_3;
    PyObject *tmp_args_element_name_4;
    PyObject *tmp_args_element_name_5;
    PyObject *tmp_args_element_name_6;
    PyObject *tmp_args_element_name_7;
    PyObject *tmp_args_element_name_8;
    PyObject *tmp_args_element_name_9;
    PyObject *tmp_args_element_name_10;
    PyObject *tmp_args_element_name_11;
    PyObject *tmp_args_element_name_12;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_called_name_1;
    PyObject *tmp_called_name_2;
    PyObject *tmp_called_name_3;
    PyObject *tmp_called_name_4;
    int tmp_cmp_Eq_1;
    int tmp_cmp_Eq_2;
    int tmp_cmp_Eq_3;
    PyObject *tmp_compare_left_1;
    PyObject *tmp_compare_left_2;
    PyObject *tmp_compare_left_3;
    PyObject *tmp_compare_left_4;
    PyObject *tmp_compare_left_5;
    PyObject *tmp_compare_left_6;
    PyObject *tmp_compare_right_1;
    PyObject *tmp_compare_right_2;
    PyObject *tmp_compare_right_3;
    PyObject *tmp_compare_right_4;
    PyObject *tmp_compare_right_5;
    PyObject *tmp_compare_right_6;
    int tmp_cond_truth_1;
    PyObject *tmp_cond_value_1;
    PyObject *tmp_dict_key_1;
    PyObject *tmp_dict_value_1;
    bool tmp_is_1;
    bool tmp_is_2;
    bool tmp_is_3;
    PyObject *tmp_kw_name_1;
    PyObject *tmp_make_exception_arg_1;
    int tmp_or_left_truth_1;
    PyObject *tmp_or_left_value_1;
    PyObject *tmp_or_right_value_1;
    PyObject *tmp_raise_type_1;
    int tmp_res;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    PyObject *tmp_source_name_2;
    PyObject *tmp_source_name_3;
    PyObject *tmp_source_name_4;
    PyObject *tmp_source_name_5;
    PyObject *tmp_source_name_6;
    PyObject *tmp_source_name_7;
    NUITKA_MAY_BE_UNUSED PyObject *tmp_unused;
    static struct Nuitka_FrameObject *cache_frame_1e3f0419c049747f6785cc3c7311ed4c = NULL;

    struct Nuitka_FrameObject *frame_1e3f0419c049747f6785cc3c7311ed4c;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_1e3f0419c049747f6785cc3c7311ed4c, codeobj_1e3f0419c049747f6785cc3c7311ed4c, module_pandas$compat$numpy$function, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_1e3f0419c049747f6785cc3c7311ed4c = cache_frame_1e3f0419c049747f6785cc3c7311ed4c;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_1e3f0419c049747f6785cc3c7311ed4c );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_1e3f0419c049747f6785cc3c7311ed4c ) == 2 ); // Frame stack

    // Framed code:
    tmp_or_left_value_1 = par_args;

    CHECK_OBJECT( tmp_or_left_value_1 );
    tmp_or_left_truth_1 = CHECK_IF_TRUE( tmp_or_left_value_1 );
    if ( tmp_or_left_truth_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 40;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }
    if ( tmp_or_left_truth_1 == 1 )
    {
        goto or_left_1;
    }
    else
    {
        goto or_right_1;
    }
    or_right_1:;
    tmp_or_right_value_1 = par_kwargs;

    CHECK_OBJECT( tmp_or_right_value_1 );
    tmp_cond_value_1 = tmp_or_right_value_1;
    goto or_end_1;
    or_left_1:;
    tmp_cond_value_1 = tmp_or_left_value_1;
    or_end_1:;
    tmp_cond_truth_1 = CHECK_IF_TRUE( tmp_cond_value_1 );
    if ( tmp_cond_truth_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 40;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }
    if ( tmp_cond_truth_1 == 1 )
    {
        goto branch_yes_1;
    }
    else
    {
        goto branch_no_1;
    }
    branch_yes_1:;
    tmp_compare_left_1 = par_fname;

    CHECK_OBJECT( tmp_compare_left_1 );
    tmp_compare_right_1 = Py_None;
    tmp_is_1 = ( tmp_compare_left_1 == tmp_compare_right_1 );
    if ( tmp_is_1 )
    {
        goto condexpr_true_1;
    }
    else
    {
        goto condexpr_false_1;
    }
    condexpr_true_1:;
    tmp_source_name_1 = par_self;

    if ( tmp_source_name_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "self" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 41;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }

    tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_fname );
    if ( tmp_assign_source_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 41;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }
    goto condexpr_end_1;
    condexpr_false_1:;
    tmp_assign_source_1 = par_fname;

    if ( tmp_assign_source_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "fname" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 41;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }

    Py_INCREF( tmp_assign_source_1 );
    condexpr_end_1:;
    {
        PyObject *old = par_fname;
        par_fname = tmp_assign_source_1;
        Py_XDECREF( old );
    }

    tmp_compare_left_2 = par_max_fname_arg_count;

    if ( tmp_compare_left_2 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "max_fname_arg_count" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 43;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }

    tmp_compare_right_2 = Py_None;
    tmp_is_2 = ( tmp_compare_left_2 == tmp_compare_right_2 );
    if ( tmp_is_2 )
    {
        goto condexpr_true_2;
    }
    else
    {
        goto condexpr_false_2;
    }
    condexpr_true_2:;
    tmp_source_name_2 = par_self;

    if ( tmp_source_name_2 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "self" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 42;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }

    tmp_assign_source_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_max_fname_arg_count );
    if ( tmp_assign_source_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 42;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }
    goto condexpr_end_2;
    condexpr_false_2:;
    tmp_assign_source_2 = par_max_fname_arg_count;

    if ( tmp_assign_source_2 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "max_fname_arg_count" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 44;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }

    Py_INCREF( tmp_assign_source_2 );
    condexpr_end_2:;
    {
        PyObject *old = par_max_fname_arg_count;
        par_max_fname_arg_count = tmp_assign_source_2;
        Py_XDECREF( old );
    }

    tmp_compare_left_3 = par_method;

    if ( tmp_compare_left_3 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "method" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 45;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }

    tmp_compare_right_3 = Py_None;
    tmp_is_3 = ( tmp_compare_left_3 == tmp_compare_right_3 );
    if ( tmp_is_3 )
    {
        goto condexpr_true_3;
    }
    else
    {
        goto condexpr_false_3;
    }
    condexpr_true_3:;
    tmp_source_name_3 = par_self;

    if ( tmp_source_name_3 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "self" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 45;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }

    tmp_assign_source_3 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_method );
    if ( tmp_assign_source_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 45;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }
    goto condexpr_end_3;
    condexpr_false_3:;
    tmp_assign_source_3 = par_method;

    if ( tmp_assign_source_3 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "method" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 45;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }

    Py_INCREF( tmp_assign_source_3 );
    condexpr_end_3:;
    {
        PyObject *old = par_method;
        par_method = tmp_assign_source_3;
        Py_XDECREF( old );
    }

    tmp_compare_left_4 = par_method;

    CHECK_OBJECT( tmp_compare_left_4 );
    tmp_compare_right_4 = const_str_plain_args;
    tmp_cmp_Eq_1 = RICH_COMPARE_BOOL_EQ( tmp_compare_left_4, tmp_compare_right_4 );
    if ( tmp_cmp_Eq_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 47;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }
    if ( tmp_cmp_Eq_1 == 1 )
    {
        goto branch_yes_2;
    }
    else
    {
        goto branch_no_2;
    }
    branch_yes_2:;
    tmp_called_name_1 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_validate_args );

    if (unlikely( tmp_called_name_1 == NULL ))
    {
        tmp_called_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_validate_args );
    }

    if ( tmp_called_name_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "validate_args" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 48;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_1 = par_fname;

    if ( tmp_args_element_name_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "fname" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 48;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_2 = par_args;

    if ( tmp_args_element_name_2 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "args" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 48;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_3 = par_max_fname_arg_count;

    if ( tmp_args_element_name_3 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "max_fname_arg_count" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 48;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }

    tmp_source_name_4 = par_self;

    if ( tmp_source_name_4 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "self" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 48;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_defaults );
    if ( tmp_args_element_name_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 48;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }
    frame_1e3f0419c049747f6785cc3c7311ed4c->m_frame.f_lineno = 48;
    {
        PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4 };
        tmp_unused = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_1, call_args );
    }

    Py_DECREF( tmp_args_element_name_4 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 48;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_unused );
    goto branch_end_2;
    branch_no_2:;
    tmp_compare_left_5 = par_method;

    if ( tmp_compare_left_5 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "method" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 49;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }

    tmp_compare_right_5 = const_str_plain_kwargs;
    tmp_cmp_Eq_2 = RICH_COMPARE_BOOL_EQ( tmp_compare_left_5, tmp_compare_right_5 );
    if ( tmp_cmp_Eq_2 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 49;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }
    if ( tmp_cmp_Eq_2 == 1 )
    {
        goto branch_yes_3;
    }
    else
    {
        goto branch_no_3;
    }
    branch_yes_3:;
    tmp_called_name_2 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_validate_kwargs );

    if (unlikely( tmp_called_name_2 == NULL ))
    {
        tmp_called_name_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_validate_kwargs );
    }

    if ( tmp_called_name_2 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "validate_kwargs" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 50;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_5 = par_fname;

    if ( tmp_args_element_name_5 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "fname" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 50;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_6 = par_kwargs;

    if ( tmp_args_element_name_6 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "kwargs" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 50;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }

    tmp_source_name_5 = par_self;

    if ( tmp_source_name_5 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "self" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 50;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_7 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_defaults );
    if ( tmp_args_element_name_7 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 50;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }
    frame_1e3f0419c049747f6785cc3c7311ed4c->m_frame.f_lineno = 50;
    {
        PyObject *call_args[] = { tmp_args_element_name_5, tmp_args_element_name_6, tmp_args_element_name_7 };
        tmp_unused = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_2, call_args );
    }

    Py_DECREF( tmp_args_element_name_7 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 50;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_unused );
    goto branch_end_3;
    branch_no_3:;
    tmp_compare_left_6 = par_method;

    if ( tmp_compare_left_6 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "method" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 51;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }

    tmp_compare_right_6 = const_str_plain_both;
    tmp_cmp_Eq_3 = RICH_COMPARE_BOOL_EQ( tmp_compare_left_6, tmp_compare_right_6 );
    if ( tmp_cmp_Eq_3 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 51;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }
    if ( tmp_cmp_Eq_3 == 1 )
    {
        goto branch_yes_4;
    }
    else
    {
        goto branch_no_4;
    }
    branch_yes_4:;
    tmp_called_name_3 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_validate_args_and_kwargs );

    if (unlikely( tmp_called_name_3 == NULL ))
    {
        tmp_called_name_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_validate_args_and_kwargs );
    }

    if ( tmp_called_name_3 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "validate_args_and_kwargs" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 52;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_8 = par_fname;

    if ( tmp_args_element_name_8 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "fname" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 52;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_9 = par_args;

    if ( tmp_args_element_name_9 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "args" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 52;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_10 = par_kwargs;

    if ( tmp_args_element_name_10 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "kwargs" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 52;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_11 = par_max_fname_arg_count;

    if ( tmp_args_element_name_11 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "max_fname_arg_count" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 53;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }

    tmp_source_name_6 = par_self;

    if ( tmp_source_name_6 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "self" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 54;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_12 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_defaults );
    if ( tmp_args_element_name_12 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 54;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }
    frame_1e3f0419c049747f6785cc3c7311ed4c->m_frame.f_lineno = 52;
    {
        PyObject *call_args[] = { tmp_args_element_name_8, tmp_args_element_name_9, tmp_args_element_name_10, tmp_args_element_name_11, tmp_args_element_name_12 };
        tmp_unused = CALL_FUNCTION_WITH_ARGS5( tmp_called_name_3, call_args );
    }

    Py_DECREF( tmp_args_element_name_12 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 52;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_unused );
    goto branch_end_4;
    branch_no_4:;
    tmp_source_name_7 = const_str_digest_11da2e0cf4ab653c48cb31bda00c225f;
    tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_format );
    assert( !(tmp_called_name_4 == NULL) );
    tmp_dict_key_1 = const_str_plain_method;
    tmp_dict_value_1 = par_method;

    if ( tmp_dict_value_1 == NULL )
    {
        Py_DECREF( tmp_called_name_4 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "method" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 57;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }

    tmp_kw_name_1 = _PyDict_NewPresized( 1 );
    tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
    assert( !(tmp_res != 0) );
    frame_1e3f0419c049747f6785cc3c7311ed4c->m_frame.f_lineno = 56;
    tmp_make_exception_arg_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_4, tmp_kw_name_1 );
    Py_DECREF( tmp_called_name_4 );
    Py_DECREF( tmp_kw_name_1 );
    if ( tmp_make_exception_arg_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 56;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }
    frame_1e3f0419c049747f6785cc3c7311ed4c->m_frame.f_lineno = 56;
    {
        PyObject *call_args[] = { tmp_make_exception_arg_1 };
        tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_ValueError, call_args );
    }

    Py_DECREF( tmp_make_exception_arg_1 );
    assert( !(tmp_raise_type_1 == NULL) );
    exception_type = tmp_raise_type_1;
    exception_lineno = 56;
    RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
    type_description_1 = "oooooo";
    goto frame_exception_exit_1;
    branch_end_4:;
    branch_end_3:;
    branch_end_2:;
    branch_no_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1e3f0419c049747f6785cc3c7311ed4c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1e3f0419c049747f6785cc3c7311ed4c );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_1e3f0419c049747f6785cc3c7311ed4c, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_1e3f0419c049747f6785cc3c7311ed4c->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_1e3f0419c049747f6785cc3c7311ed4c, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_1e3f0419c049747f6785cc3c7311ed4c,
        type_description_1,
        par_self,
        par_args,
        par_kwargs,
        par_fname,
        par_max_fname_arg_count,
        par_method
    );


    // Release cached frame.
    if ( frame_1e3f0419c049747f6785cc3c7311ed4c == cache_frame_1e3f0419c049747f6785cc3c7311ed4c )
    {
        Py_DECREF( frame_1e3f0419c049747f6785cc3c7311ed4c );
    }
    cache_frame_1e3f0419c049747f6785cc3c7311ed4c = NULL;

    assertFrameObject( frame_1e3f0419c049747f6785cc3c7311ed4c );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$compat$numpy$function$$$function_2___call__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_self );
    par_self = NULL;

    Py_XDECREF( par_args );
    par_args = NULL;

    Py_XDECREF( par_kwargs );
    par_kwargs = NULL;

    Py_XDECREF( par_fname );
    par_fname = NULL;

    Py_XDECREF( par_max_fname_arg_count );
    par_max_fname_arg_count = NULL;

    Py_XDECREF( par_method );
    par_method = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_self );
    par_self = NULL;

    Py_XDECREF( par_args );
    par_args = NULL;

    Py_XDECREF( par_kwargs );
    par_kwargs = NULL;

    Py_XDECREF( par_fname );
    par_fname = NULL;

    Py_XDECREF( par_max_fname_arg_count );
    par_max_fname_arg_count = NULL;

    Py_XDECREF( par_method );
    par_method = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$compat$numpy$function$$$function_2___call__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$compat$numpy$function$$$function_3_process_skipna( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_skipna = python_pars[ 0 ];
    PyObject *par_args = python_pars[ 1 ];
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_compexpr_left_1;
    PyObject *tmp_compexpr_right_1;
    int tmp_cond_truth_1;
    PyObject *tmp_cond_value_1;
    PyObject *tmp_isinstance_cls_1;
    PyObject *tmp_isinstance_inst_1;
    PyObject *tmp_left_name_1;
    int tmp_or_left_truth_1;
    PyObject *tmp_or_left_value_1;
    PyObject *tmp_or_right_value_1;
    PyObject *tmp_return_value;
    PyObject *tmp_right_name_1;
    PyObject *tmp_tuple_element_1;
    PyObject *tmp_tuple_element_2;
    static struct Nuitka_FrameObject *cache_frame_43e94ecae646e3d8f1f3ca9a745e993b = NULL;

    struct Nuitka_FrameObject *frame_43e94ecae646e3d8f1f3ca9a745e993b;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_43e94ecae646e3d8f1f3ca9a745e993b, codeobj_43e94ecae646e3d8f1f3ca9a745e993b, module_pandas$compat$numpy$function, sizeof(void *)+sizeof(void *) );
    frame_43e94ecae646e3d8f1f3ca9a745e993b = cache_frame_43e94ecae646e3d8f1f3ca9a745e993b;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_43e94ecae646e3d8f1f3ca9a745e993b );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_43e94ecae646e3d8f1f3ca9a745e993b ) == 2 ); // Frame stack

    // Framed code:
    tmp_isinstance_inst_1 = par_skipna;

    CHECK_OBJECT( tmp_isinstance_inst_1 );
    tmp_isinstance_cls_1 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_ndarray );

    if (unlikely( tmp_isinstance_cls_1 == NULL ))
    {
        tmp_isinstance_cls_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ndarray );
    }

    if ( tmp_isinstance_cls_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ndarray" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 68;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }

    tmp_or_left_value_1 = BUILTIN_ISINSTANCE( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
    if ( tmp_or_left_value_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 68;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }
    tmp_or_left_truth_1 = CHECK_IF_TRUE( tmp_or_left_value_1 );
    if ( tmp_or_left_truth_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 68;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }
    if ( tmp_or_left_truth_1 == 1 )
    {
        goto or_left_1;
    }
    else
    {
        goto or_right_1;
    }
    or_right_1:;
    tmp_compexpr_left_1 = par_skipna;

    CHECK_OBJECT( tmp_compexpr_left_1 );
    tmp_compexpr_right_1 = Py_None;
    tmp_or_right_value_1 = BOOL_FROM( tmp_compexpr_left_1 == tmp_compexpr_right_1 );
    tmp_cond_value_1 = tmp_or_right_value_1;
    goto or_end_1;
    or_left_1:;
    tmp_cond_value_1 = tmp_or_left_value_1;
    or_end_1:;
    tmp_cond_truth_1 = CHECK_IF_TRUE( tmp_cond_value_1 );
    if ( tmp_cond_truth_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 68;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }
    if ( tmp_cond_truth_1 == 1 )
    {
        goto branch_yes_1;
    }
    else
    {
        goto branch_no_1;
    }
    branch_yes_1:;
    tmp_tuple_element_1 = par_skipna;

    if ( tmp_tuple_element_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "skipna" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 69;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }

    tmp_left_name_1 = PyTuple_New( 1 );
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_left_name_1, 0, tmp_tuple_element_1 );
    tmp_right_name_1 = par_args;

    if ( tmp_right_name_1 == NULL )
    {
        Py_DECREF( tmp_left_name_1 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "args" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 69;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }

    tmp_assign_source_1 = BINARY_OPERATION_ADD( tmp_left_name_1, tmp_right_name_1 );
    Py_DECREF( tmp_left_name_1 );
    if ( tmp_assign_source_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 69;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }
    {
        PyObject *old = par_args;
        par_args = tmp_assign_source_1;
        Py_XDECREF( old );
    }

    tmp_assign_source_2 = Py_True;
    {
        PyObject *old = par_skipna;
        par_skipna = tmp_assign_source_2;
        Py_INCREF( par_skipna );
        Py_XDECREF( old );
    }

    branch_no_1:;
    tmp_tuple_element_2 = par_skipna;

    if ( tmp_tuple_element_2 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "skipna" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 72;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }

    tmp_return_value = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_2 );
    PyTuple_SET_ITEM( tmp_return_value, 0, tmp_tuple_element_2 );
    tmp_tuple_element_2 = par_args;

    if ( tmp_tuple_element_2 == NULL )
    {
        Py_DECREF( tmp_return_value );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "args" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 72;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }

    Py_INCREF( tmp_tuple_element_2 );
    PyTuple_SET_ITEM( tmp_return_value, 1, tmp_tuple_element_2 );
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_43e94ecae646e3d8f1f3ca9a745e993b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_43e94ecae646e3d8f1f3ca9a745e993b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_43e94ecae646e3d8f1f3ca9a745e993b );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_43e94ecae646e3d8f1f3ca9a745e993b, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_43e94ecae646e3d8f1f3ca9a745e993b->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_43e94ecae646e3d8f1f3ca9a745e993b, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_43e94ecae646e3d8f1f3ca9a745e993b,
        type_description_1,
        par_skipna,
        par_args
    );


    // Release cached frame.
    if ( frame_43e94ecae646e3d8f1f3ca9a745e993b == cache_frame_43e94ecae646e3d8f1f3ca9a745e993b )
    {
        Py_DECREF( frame_43e94ecae646e3d8f1f3ca9a745e993b );
    }
    cache_frame_43e94ecae646e3d8f1f3ca9a745e993b = NULL;

    assertFrameObject( frame_43e94ecae646e3d8f1f3ca9a745e993b );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$compat$numpy$function$$$function_3_process_skipna );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_skipna );
    par_skipna = NULL;

    Py_XDECREF( par_args );
    par_args = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_skipna );
    par_skipna = NULL;

    Py_XDECREF( par_args );
    par_args = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$compat$numpy$function$$$function_3_process_skipna );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$compat$numpy$function$$$function_4_validate_argmin_with_skipna( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_skipna = python_pars[ 0 ];
    PyObject *par_args = python_pars[ 1 ];
    PyObject *par_kwargs = python_pars[ 2 ];
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *tmp_args_element_name_1;
    PyObject *tmp_args_element_name_2;
    PyObject *tmp_args_element_name_3;
    PyObject *tmp_args_element_name_4;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_assign_source_5;
    PyObject *tmp_called_name_1;
    PyObject *tmp_called_name_2;
    PyObject *tmp_iter_arg_1;
    PyObject *tmp_iterator_attempt;
    PyObject *tmp_iterator_name_1;
    PyObject *tmp_return_value;
    PyObject *tmp_unpack_1;
    PyObject *tmp_unpack_2;
    NUITKA_MAY_BE_UNUSED PyObject *tmp_unused;
    static struct Nuitka_FrameObject *cache_frame_cb583cf8b472c057f6e631c41433adb2 = NULL;

    struct Nuitka_FrameObject *frame_cb583cf8b472c057f6e631c41433adb2;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_cb583cf8b472c057f6e631c41433adb2, codeobj_cb583cf8b472c057f6e631c41433adb2, module_pandas$compat$numpy$function, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_cb583cf8b472c057f6e631c41433adb2 = cache_frame_cb583cf8b472c057f6e631c41433adb2;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_cb583cf8b472c057f6e631c41433adb2 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_cb583cf8b472c057f6e631c41433adb2 ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    tmp_called_name_1 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_process_skipna );

    if (unlikely( tmp_called_name_1 == NULL ))
    {
        tmp_called_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_process_skipna );
    }

    if ( tmp_called_name_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "process_skipna" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 84;
        type_description_1 = "ooo";
        goto try_except_handler_2;
    }

    tmp_args_element_name_1 = par_skipna;

    CHECK_OBJECT( tmp_args_element_name_1 );
    tmp_args_element_name_2 = par_args;

    CHECK_OBJECT( tmp_args_element_name_2 );
    frame_cb583cf8b472c057f6e631c41433adb2->m_frame.f_lineno = 84;
    {
        PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
        tmp_iter_arg_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
    }

    if ( tmp_iter_arg_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 84;
        type_description_1 = "ooo";
        goto try_except_handler_2;
    }
    tmp_assign_source_1 = MAKE_ITERATOR( tmp_iter_arg_1 );
    Py_DECREF( tmp_iter_arg_1 );
    if ( tmp_assign_source_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 84;
        type_description_1 = "ooo";
        goto try_except_handler_2;
    }
    assert( tmp_tuple_unpack_1__source_iter == NULL );
    tmp_tuple_unpack_1__source_iter = tmp_assign_source_1;

    // Tried code:
    tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;

    CHECK_OBJECT( tmp_unpack_1 );
    tmp_assign_source_2 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
    if ( tmp_assign_source_2 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        type_description_1 = "ooo";
        exception_lineno = 84;
        goto try_except_handler_3;
    }
    assert( tmp_tuple_unpack_1__element_1 == NULL );
    tmp_tuple_unpack_1__element_1 = tmp_assign_source_2;

    tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;

    CHECK_OBJECT( tmp_unpack_2 );
    tmp_assign_source_3 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
    if ( tmp_assign_source_3 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        type_description_1 = "ooo";
        exception_lineno = 84;
        goto try_except_handler_3;
    }
    assert( tmp_tuple_unpack_1__element_2 == NULL );
    tmp_tuple_unpack_1__element_2 = tmp_assign_source_3;

    tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;

    CHECK_OBJECT( tmp_iterator_name_1 );
    // Check if iterator has left-over elements.
    CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

    tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

    if (likely( tmp_iterator_attempt == NULL ))
    {
        PyObject *error = GET_ERROR_OCCURRED();

        if ( error != NULL )
        {
            if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
            {
                CLEAR_ERROR_OCCURRED();
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                type_description_1 = "ooo";
                exception_lineno = 84;
                goto try_except_handler_3;
            }
        }
    }
    else
    {
        Py_DECREF( tmp_iterator_attempt );

        // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
        PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
        PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

        type_description_1 = "ooo";
        exception_lineno = 84;
        goto try_except_handler_3;
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_2;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    tmp_assign_source_4 = tmp_tuple_unpack_1__element_1;

    CHECK_OBJECT( tmp_assign_source_4 );
    {
        PyObject *old = par_skipna;
        par_skipna = tmp_assign_source_4;
        Py_INCREF( par_skipna );
        Py_XDECREF( old );
    }

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    tmp_assign_source_5 = tmp_tuple_unpack_1__element_2;

    CHECK_OBJECT( tmp_assign_source_5 );
    {
        PyObject *old = par_args;
        par_args = tmp_assign_source_5;
        Py_INCREF( par_args );
        Py_XDECREF( old );
    }

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    tmp_called_name_2 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_validate_argmin );

    if (unlikely( tmp_called_name_2 == NULL ))
    {
        tmp_called_name_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_validate_argmin );
    }

    if ( tmp_called_name_2 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "validate_argmin" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 85;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_3 = par_args;

    if ( tmp_args_element_name_3 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "args" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 85;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_4 = par_kwargs;

    if ( tmp_args_element_name_4 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "kwargs" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 85;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    frame_cb583cf8b472c057f6e631c41433adb2->m_frame.f_lineno = 85;
    {
        PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4 };
        tmp_unused = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
    }

    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 85;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_unused );
    tmp_return_value = par_skipna;

    if ( tmp_return_value == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "skipna" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 86;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    Py_INCREF( tmp_return_value );
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_cb583cf8b472c057f6e631c41433adb2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_cb583cf8b472c057f6e631c41433adb2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_cb583cf8b472c057f6e631c41433adb2 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_cb583cf8b472c057f6e631c41433adb2, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_cb583cf8b472c057f6e631c41433adb2->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_cb583cf8b472c057f6e631c41433adb2, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_cb583cf8b472c057f6e631c41433adb2,
        type_description_1,
        par_skipna,
        par_args,
        par_kwargs
    );


    // Release cached frame.
    if ( frame_cb583cf8b472c057f6e631c41433adb2 == cache_frame_cb583cf8b472c057f6e631c41433adb2 )
    {
        Py_DECREF( frame_cb583cf8b472c057f6e631c41433adb2 );
    }
    cache_frame_cb583cf8b472c057f6e631c41433adb2 = NULL;

    assertFrameObject( frame_cb583cf8b472c057f6e631c41433adb2 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$compat$numpy$function$$$function_4_validate_argmin_with_skipna );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_skipna );
    par_skipna = NULL;

    Py_XDECREF( par_args );
    par_args = NULL;

    Py_XDECREF( par_kwargs );
    par_kwargs = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_skipna );
    par_skipna = NULL;

    Py_XDECREF( par_args );
    par_args = NULL;

    Py_XDECREF( par_kwargs );
    par_kwargs = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$compat$numpy$function$$$function_4_validate_argmin_with_skipna );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$compat$numpy$function$$$function_5_validate_argmax_with_skipna( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_skipna = python_pars[ 0 ];
    PyObject *par_args = python_pars[ 1 ];
    PyObject *par_kwargs = python_pars[ 2 ];
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *tmp_args_element_name_1;
    PyObject *tmp_args_element_name_2;
    PyObject *tmp_args_element_name_3;
    PyObject *tmp_args_element_name_4;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_assign_source_5;
    PyObject *tmp_called_name_1;
    PyObject *tmp_called_name_2;
    PyObject *tmp_iter_arg_1;
    PyObject *tmp_iterator_attempt;
    PyObject *tmp_iterator_name_1;
    PyObject *tmp_return_value;
    PyObject *tmp_unpack_1;
    PyObject *tmp_unpack_2;
    NUITKA_MAY_BE_UNUSED PyObject *tmp_unused;
    static struct Nuitka_FrameObject *cache_frame_713f7fe3df7bd6648893df8dc083acaa = NULL;

    struct Nuitka_FrameObject *frame_713f7fe3df7bd6648893df8dc083acaa;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_713f7fe3df7bd6648893df8dc083acaa, codeobj_713f7fe3df7bd6648893df8dc083acaa, module_pandas$compat$numpy$function, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_713f7fe3df7bd6648893df8dc083acaa = cache_frame_713f7fe3df7bd6648893df8dc083acaa;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_713f7fe3df7bd6648893df8dc083acaa );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_713f7fe3df7bd6648893df8dc083acaa ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    tmp_called_name_1 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_process_skipna );

    if (unlikely( tmp_called_name_1 == NULL ))
    {
        tmp_called_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_process_skipna );
    }

    if ( tmp_called_name_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "process_skipna" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 98;
        type_description_1 = "ooo";
        goto try_except_handler_2;
    }

    tmp_args_element_name_1 = par_skipna;

    CHECK_OBJECT( tmp_args_element_name_1 );
    tmp_args_element_name_2 = par_args;

    CHECK_OBJECT( tmp_args_element_name_2 );
    frame_713f7fe3df7bd6648893df8dc083acaa->m_frame.f_lineno = 98;
    {
        PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
        tmp_iter_arg_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
    }

    if ( tmp_iter_arg_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 98;
        type_description_1 = "ooo";
        goto try_except_handler_2;
    }
    tmp_assign_source_1 = MAKE_ITERATOR( tmp_iter_arg_1 );
    Py_DECREF( tmp_iter_arg_1 );
    if ( tmp_assign_source_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 98;
        type_description_1 = "ooo";
        goto try_except_handler_2;
    }
    assert( tmp_tuple_unpack_1__source_iter == NULL );
    tmp_tuple_unpack_1__source_iter = tmp_assign_source_1;

    // Tried code:
    tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;

    CHECK_OBJECT( tmp_unpack_1 );
    tmp_assign_source_2 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
    if ( tmp_assign_source_2 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        type_description_1 = "ooo";
        exception_lineno = 98;
        goto try_except_handler_3;
    }
    assert( tmp_tuple_unpack_1__element_1 == NULL );
    tmp_tuple_unpack_1__element_1 = tmp_assign_source_2;

    tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;

    CHECK_OBJECT( tmp_unpack_2 );
    tmp_assign_source_3 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
    if ( tmp_assign_source_3 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        type_description_1 = "ooo";
        exception_lineno = 98;
        goto try_except_handler_3;
    }
    assert( tmp_tuple_unpack_1__element_2 == NULL );
    tmp_tuple_unpack_1__element_2 = tmp_assign_source_3;

    tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;

    CHECK_OBJECT( tmp_iterator_name_1 );
    // Check if iterator has left-over elements.
    CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

    tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

    if (likely( tmp_iterator_attempt == NULL ))
    {
        PyObject *error = GET_ERROR_OCCURRED();

        if ( error != NULL )
        {
            if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
            {
                CLEAR_ERROR_OCCURRED();
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                type_description_1 = "ooo";
                exception_lineno = 98;
                goto try_except_handler_3;
            }
        }
    }
    else
    {
        Py_DECREF( tmp_iterator_attempt );

        // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
        PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
        PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

        type_description_1 = "ooo";
        exception_lineno = 98;
        goto try_except_handler_3;
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_2;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    tmp_assign_source_4 = tmp_tuple_unpack_1__element_1;

    CHECK_OBJECT( tmp_assign_source_4 );
    {
        PyObject *old = par_skipna;
        par_skipna = tmp_assign_source_4;
        Py_INCREF( par_skipna );
        Py_XDECREF( old );
    }

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    tmp_assign_source_5 = tmp_tuple_unpack_1__element_2;

    CHECK_OBJECT( tmp_assign_source_5 );
    {
        PyObject *old = par_args;
        par_args = tmp_assign_source_5;
        Py_INCREF( par_args );
        Py_XDECREF( old );
    }

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    tmp_called_name_2 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_validate_argmax );

    if (unlikely( tmp_called_name_2 == NULL ))
    {
        tmp_called_name_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_validate_argmax );
    }

    if ( tmp_called_name_2 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "validate_argmax" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 99;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_3 = par_args;

    if ( tmp_args_element_name_3 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "args" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 99;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_4 = par_kwargs;

    if ( tmp_args_element_name_4 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "kwargs" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 99;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    frame_713f7fe3df7bd6648893df8dc083acaa->m_frame.f_lineno = 99;
    {
        PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4 };
        tmp_unused = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
    }

    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 99;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_unused );
    tmp_return_value = par_skipna;

    if ( tmp_return_value == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "skipna" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 100;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    Py_INCREF( tmp_return_value );
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_713f7fe3df7bd6648893df8dc083acaa );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_713f7fe3df7bd6648893df8dc083acaa );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_713f7fe3df7bd6648893df8dc083acaa );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_713f7fe3df7bd6648893df8dc083acaa, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_713f7fe3df7bd6648893df8dc083acaa->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_713f7fe3df7bd6648893df8dc083acaa, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_713f7fe3df7bd6648893df8dc083acaa,
        type_description_1,
        par_skipna,
        par_args,
        par_kwargs
    );


    // Release cached frame.
    if ( frame_713f7fe3df7bd6648893df8dc083acaa == cache_frame_713f7fe3df7bd6648893df8dc083acaa )
    {
        Py_DECREF( frame_713f7fe3df7bd6648893df8dc083acaa );
    }
    cache_frame_713f7fe3df7bd6648893df8dc083acaa = NULL;

    assertFrameObject( frame_713f7fe3df7bd6648893df8dc083acaa );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$compat$numpy$function$$$function_5_validate_argmax_with_skipna );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_skipna );
    par_skipna = NULL;

    Py_XDECREF( par_args );
    par_args = NULL;

    Py_XDECREF( par_kwargs );
    par_kwargs = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_skipna );
    par_skipna = NULL;

    Py_XDECREF( par_args );
    par_args = NULL;

    Py_XDECREF( par_kwargs );
    par_kwargs = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$compat$numpy$function$$$function_5_validate_argmax_with_skipna );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$compat$numpy$function$$$function_6_validate_argsort_with_ascending( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_ascending = python_pars[ 0 ];
    PyObject *par_args = python_pars[ 1 ];
    PyObject *par_kwargs = python_pars[ 2 ];
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_args_element_name_1;
    PyObject *tmp_args_name_1;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_called_name_1;
    PyObject *tmp_called_name_2;
    PyObject *tmp_compexpr_left_1;
    PyObject *tmp_compexpr_right_1;
    int tmp_cond_truth_1;
    PyObject *tmp_cond_value_1;
    PyObject *tmp_kw_name_1;
    PyObject *tmp_left_name_1;
    int tmp_or_left_truth_1;
    PyObject *tmp_or_left_value_1;
    PyObject *tmp_or_right_value_1;
    PyObject *tmp_return_value;
    PyObject *tmp_right_name_1;
    PyObject *tmp_tuple_element_1;
    PyObject *tmp_tuple_element_2;
    NUITKA_MAY_BE_UNUSED PyObject *tmp_unused;
    static struct Nuitka_FrameObject *cache_frame_148a360f13c6662190bfd9d000600ba7 = NULL;

    struct Nuitka_FrameObject *frame_148a360f13c6662190bfd9d000600ba7;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_148a360f13c6662190bfd9d000600ba7, codeobj_148a360f13c6662190bfd9d000600ba7, module_pandas$compat$numpy$function, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_148a360f13c6662190bfd9d000600ba7 = cache_frame_148a360f13c6662190bfd9d000600ba7;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_148a360f13c6662190bfd9d000600ba7 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_148a360f13c6662190bfd9d000600ba7 ) == 2 ); // Frame stack

    // Framed code:
    tmp_called_name_1 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_is_integer );

    if (unlikely( tmp_called_name_1 == NULL ))
    {
        tmp_called_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_integer );
    }

    if ( tmp_called_name_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_integer" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 128;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_1 = par_ascending;

    CHECK_OBJECT( tmp_args_element_name_1 );
    frame_148a360f13c6662190bfd9d000600ba7->m_frame.f_lineno = 128;
    {
        PyObject *call_args[] = { tmp_args_element_name_1 };
        tmp_or_left_value_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
    }

    if ( tmp_or_left_value_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 128;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    tmp_or_left_truth_1 = CHECK_IF_TRUE( tmp_or_left_value_1 );
    if ( tmp_or_left_truth_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_or_left_value_1 );

        exception_lineno = 128;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    if ( tmp_or_left_truth_1 == 1 )
    {
        goto or_left_1;
    }
    else
    {
        goto or_right_1;
    }
    or_right_1:;
    Py_DECREF( tmp_or_left_value_1 );
    tmp_compexpr_left_1 = par_ascending;

    if ( tmp_compexpr_left_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "ascending" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 128;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    tmp_compexpr_right_1 = Py_None;
    tmp_or_right_value_1 = BOOL_FROM( tmp_compexpr_left_1 == tmp_compexpr_right_1 );
    Py_INCREF( tmp_or_right_value_1 );
    tmp_cond_value_1 = tmp_or_right_value_1;
    goto or_end_1;
    or_left_1:;
    tmp_cond_value_1 = tmp_or_left_value_1;
    or_end_1:;
    tmp_cond_truth_1 = CHECK_IF_TRUE( tmp_cond_value_1 );
    if ( tmp_cond_truth_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_cond_value_1 );

        exception_lineno = 128;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_cond_value_1 );
    if ( tmp_cond_truth_1 == 1 )
    {
        goto branch_yes_1;
    }
    else
    {
        goto branch_no_1;
    }
    branch_yes_1:;
    tmp_tuple_element_1 = par_ascending;

    if ( tmp_tuple_element_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "ascending" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 129;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    tmp_left_name_1 = PyTuple_New( 1 );
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_left_name_1, 0, tmp_tuple_element_1 );
    tmp_right_name_1 = par_args;

    if ( tmp_right_name_1 == NULL )
    {
        Py_DECREF( tmp_left_name_1 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "args" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 129;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    tmp_assign_source_1 = BINARY_OPERATION_ADD( tmp_left_name_1, tmp_right_name_1 );
    Py_DECREF( tmp_left_name_1 );
    if ( tmp_assign_source_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 129;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    {
        PyObject *old = par_args;
        par_args = tmp_assign_source_1;
        Py_XDECREF( old );
    }

    tmp_assign_source_2 = Py_True;
    {
        PyObject *old = par_ascending;
        par_ascending = tmp_assign_source_2;
        Py_INCREF( par_ascending );
        Py_XDECREF( old );
    }

    branch_no_1:;
    tmp_called_name_2 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_validate_argsort_kind );

    if (unlikely( tmp_called_name_2 == NULL ))
    {
        tmp_called_name_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_validate_argsort_kind );
    }

    if ( tmp_called_name_2 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "validate_argsort_kind" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 132;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    tmp_tuple_element_2 = par_args;

    if ( tmp_tuple_element_2 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "args" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 132;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    tmp_args_name_1 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_2 );
    PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_2 );
    tmp_tuple_element_2 = par_kwargs;

    if ( tmp_tuple_element_2 == NULL )
    {
        Py_DECREF( tmp_args_name_1 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "kwargs" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 132;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    Py_INCREF( tmp_tuple_element_2 );
    PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_2 );
    tmp_kw_name_1 = PyDict_Copy( const_dict_ac3c2736753dd3af9122456af46b74ec );
    frame_148a360f13c6662190bfd9d000600ba7->m_frame.f_lineno = 132;
    tmp_unused = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_1, tmp_kw_name_1 );
    Py_DECREF( tmp_args_name_1 );
    Py_DECREF( tmp_kw_name_1 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 132;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_unused );
    tmp_return_value = par_ascending;

    if ( tmp_return_value == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "ascending" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 133;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    Py_INCREF( tmp_return_value );
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_148a360f13c6662190bfd9d000600ba7 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_148a360f13c6662190bfd9d000600ba7 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_148a360f13c6662190bfd9d000600ba7 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_148a360f13c6662190bfd9d000600ba7, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_148a360f13c6662190bfd9d000600ba7->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_148a360f13c6662190bfd9d000600ba7, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_148a360f13c6662190bfd9d000600ba7,
        type_description_1,
        par_ascending,
        par_args,
        par_kwargs
    );


    // Release cached frame.
    if ( frame_148a360f13c6662190bfd9d000600ba7 == cache_frame_148a360f13c6662190bfd9d000600ba7 )
    {
        Py_DECREF( frame_148a360f13c6662190bfd9d000600ba7 );
    }
    cache_frame_148a360f13c6662190bfd9d000600ba7 = NULL;

    assertFrameObject( frame_148a360f13c6662190bfd9d000600ba7 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$compat$numpy$function$$$function_6_validate_argsort_with_ascending );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_ascending );
    par_ascending = NULL;

    Py_XDECREF( par_args );
    par_args = NULL;

    Py_XDECREF( par_kwargs );
    par_kwargs = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_ascending );
    par_ascending = NULL;

    Py_XDECREF( par_args );
    par_args = NULL;

    Py_XDECREF( par_kwargs );
    par_kwargs = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$compat$numpy$function$$$function_6_validate_argsort_with_ascending );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$compat$numpy$function$$$function_7_validate_clip_with_axis( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_axis = python_pars[ 0 ];
    PyObject *par_args = python_pars[ 1 ];
    PyObject *par_kwargs = python_pars[ 2 ];
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_args_element_name_1;
    PyObject *tmp_args_element_name_2;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_called_name_1;
    PyObject *tmp_isinstance_cls_1;
    PyObject *tmp_isinstance_inst_1;
    PyObject *tmp_left_name_1;
    int tmp_res;
    PyObject *tmp_return_value;
    PyObject *tmp_right_name_1;
    PyObject *tmp_tuple_element_1;
    NUITKA_MAY_BE_UNUSED PyObject *tmp_unused;
    static struct Nuitka_FrameObject *cache_frame_a66b33f8faf355add77e1dae7c2a077e = NULL;

    struct Nuitka_FrameObject *frame_a66b33f8faf355add77e1dae7c2a077e;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_a66b33f8faf355add77e1dae7c2a077e, codeobj_a66b33f8faf355add77e1dae7c2a077e, module_pandas$compat$numpy$function, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_a66b33f8faf355add77e1dae7c2a077e = cache_frame_a66b33f8faf355add77e1dae7c2a077e;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_a66b33f8faf355add77e1dae7c2a077e );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_a66b33f8faf355add77e1dae7c2a077e ) == 2 ); // Frame stack

    // Framed code:
    tmp_isinstance_inst_1 = par_axis;

    CHECK_OBJECT( tmp_isinstance_inst_1 );
    tmp_isinstance_cls_1 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_ndarray );

    if (unlikely( tmp_isinstance_cls_1 == NULL ))
    {
        tmp_isinstance_cls_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ndarray );
    }

    if ( tmp_isinstance_cls_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ndarray" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 149;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
    if ( tmp_res == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 149;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    if ( tmp_res == 1 )
    {
        goto branch_yes_1;
    }
    else
    {
        goto branch_no_1;
    }
    branch_yes_1:;
    tmp_tuple_element_1 = par_axis;

    CHECK_OBJECT( tmp_tuple_element_1 );
    tmp_left_name_1 = PyTuple_New( 1 );
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_left_name_1, 0, tmp_tuple_element_1 );
    tmp_right_name_1 = par_args;

    CHECK_OBJECT( tmp_right_name_1 );
    tmp_assign_source_1 = BINARY_OPERATION_ADD( tmp_left_name_1, tmp_right_name_1 );
    Py_DECREF( tmp_left_name_1 );
    if ( tmp_assign_source_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 150;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    {
        PyObject *old = par_args;
        par_args = tmp_assign_source_1;
        Py_XDECREF( old );
    }

    tmp_assign_source_2 = Py_None;
    {
        PyObject *old = par_axis;
        par_axis = tmp_assign_source_2;
        Py_INCREF( par_axis );
        Py_XDECREF( old );
    }

    branch_no_1:;
    tmp_called_name_1 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_validate_clip );

    if (unlikely( tmp_called_name_1 == NULL ))
    {
        tmp_called_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_validate_clip );
    }

    if ( tmp_called_name_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "validate_clip" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 153;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_1 = par_args;

    CHECK_OBJECT( tmp_args_element_name_1 );
    tmp_args_element_name_2 = par_kwargs;

    if ( tmp_args_element_name_2 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "kwargs" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 153;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    frame_a66b33f8faf355add77e1dae7c2a077e->m_frame.f_lineno = 153;
    {
        PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
        tmp_unused = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
    }

    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 153;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_unused );
    tmp_return_value = par_axis;

    if ( tmp_return_value == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "axis" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 154;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    Py_INCREF( tmp_return_value );
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a66b33f8faf355add77e1dae7c2a077e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_a66b33f8faf355add77e1dae7c2a077e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a66b33f8faf355add77e1dae7c2a077e );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_a66b33f8faf355add77e1dae7c2a077e, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_a66b33f8faf355add77e1dae7c2a077e->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_a66b33f8faf355add77e1dae7c2a077e, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_a66b33f8faf355add77e1dae7c2a077e,
        type_description_1,
        par_axis,
        par_args,
        par_kwargs
    );


    // Release cached frame.
    if ( frame_a66b33f8faf355add77e1dae7c2a077e == cache_frame_a66b33f8faf355add77e1dae7c2a077e )
    {
        Py_DECREF( frame_a66b33f8faf355add77e1dae7c2a077e );
    }
    cache_frame_a66b33f8faf355add77e1dae7c2a077e = NULL;

    assertFrameObject( frame_a66b33f8faf355add77e1dae7c2a077e );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$compat$numpy$function$$$function_7_validate_clip_with_axis );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_axis );
    par_axis = NULL;

    Py_XDECREF( par_args );
    par_args = NULL;

    Py_XDECREF( par_kwargs );
    par_kwargs = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_axis );
    par_axis = NULL;

    Py_XDECREF( par_args );
    par_args = NULL;

    Py_XDECREF( par_kwargs );
    par_kwargs = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$compat$numpy$function$$$function_7_validate_clip_with_axis );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$compat$numpy$function$$$function_8_validate_cum_func_with_skipna( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_skipna = python_pars[ 0 ];
    PyObject *par_args = python_pars[ 1 ];
    PyObject *par_kwargs = python_pars[ 2 ];
    PyObject *par_name = python_pars[ 3 ];
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_args_element_name_1;
    PyObject *tmp_args_name_1;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_called_name_1;
    PyObject *tmp_called_name_2;
    int tmp_cond_truth_1;
    PyObject *tmp_cond_value_1;
    PyObject *tmp_dict_key_1;
    PyObject *tmp_dict_value_1;
    PyObject *tmp_kw_name_1;
    PyObject *tmp_left_name_1;
    int tmp_res;
    PyObject *tmp_return_value;
    PyObject *tmp_right_name_1;
    PyObject *tmp_tuple_element_1;
    PyObject *tmp_tuple_element_2;
    NUITKA_MAY_BE_UNUSED PyObject *tmp_unused;
    static struct Nuitka_FrameObject *cache_frame_2f5bab5df7791de2e0bdef305e996143 = NULL;

    struct Nuitka_FrameObject *frame_2f5bab5df7791de2e0bdef305e996143;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_2f5bab5df7791de2e0bdef305e996143, codeobj_2f5bab5df7791de2e0bdef305e996143, module_pandas$compat$numpy$function, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_2f5bab5df7791de2e0bdef305e996143 = cache_frame_2f5bab5df7791de2e0bdef305e996143;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_2f5bab5df7791de2e0bdef305e996143 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_2f5bab5df7791de2e0bdef305e996143 ) == 2 ); // Frame stack

    // Framed code:
    tmp_called_name_1 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_is_bool );

    if (unlikely( tmp_called_name_1 == NULL ))
    {
        tmp_called_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_bool );
    }

    if ( tmp_called_name_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_bool" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 179;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_1 = par_skipna;

    CHECK_OBJECT( tmp_args_element_name_1 );
    frame_2f5bab5df7791de2e0bdef305e996143->m_frame.f_lineno = 179;
    {
        PyObject *call_args[] = { tmp_args_element_name_1 };
        tmp_cond_value_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
    }

    if ( tmp_cond_value_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 179;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    tmp_cond_truth_1 = CHECK_IF_TRUE( tmp_cond_value_1 );
    if ( tmp_cond_truth_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_cond_value_1 );

        exception_lineno = 179;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_cond_value_1 );
    if ( tmp_cond_truth_1 == 1 )
    {
        goto branch_no_1;
    }
    else
    {
        goto branch_yes_1;
    }
    branch_yes_1:;
    tmp_tuple_element_1 = par_skipna;

    if ( tmp_tuple_element_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "skipna" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 180;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }

    tmp_left_name_1 = PyTuple_New( 1 );
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_left_name_1, 0, tmp_tuple_element_1 );
    tmp_right_name_1 = par_args;

    if ( tmp_right_name_1 == NULL )
    {
        Py_DECREF( tmp_left_name_1 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "args" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 180;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }

    tmp_assign_source_1 = BINARY_OPERATION_ADD( tmp_left_name_1, tmp_right_name_1 );
    Py_DECREF( tmp_left_name_1 );
    if ( tmp_assign_source_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 180;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    {
        PyObject *old = par_args;
        par_args = tmp_assign_source_1;
        Py_XDECREF( old );
    }

    tmp_assign_source_2 = Py_True;
    {
        PyObject *old = par_skipna;
        par_skipna = tmp_assign_source_2;
        Py_INCREF( par_skipna );
        Py_XDECREF( old );
    }

    branch_no_1:;
    tmp_called_name_2 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_validate_cum_func );

    if (unlikely( tmp_called_name_2 == NULL ))
    {
        tmp_called_name_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_validate_cum_func );
    }

    if ( tmp_called_name_2 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "validate_cum_func" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 183;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }

    tmp_tuple_element_2 = par_args;

    if ( tmp_tuple_element_2 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "args" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 183;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }

    tmp_args_name_1 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_2 );
    PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_2 );
    tmp_tuple_element_2 = par_kwargs;

    if ( tmp_tuple_element_2 == NULL )
    {
        Py_DECREF( tmp_args_name_1 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "kwargs" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 183;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }

    Py_INCREF( tmp_tuple_element_2 );
    PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_2 );
    tmp_dict_key_1 = const_str_plain_fname;
    tmp_dict_value_1 = par_name;

    if ( tmp_dict_value_1 == NULL )
    {
        Py_DECREF( tmp_args_name_1 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "name" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 183;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }

    tmp_kw_name_1 = _PyDict_NewPresized( 1 );
    tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
    assert( !(tmp_res != 0) );
    frame_2f5bab5df7791de2e0bdef305e996143->m_frame.f_lineno = 183;
    tmp_unused = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_1, tmp_kw_name_1 );
    Py_DECREF( tmp_args_name_1 );
    Py_DECREF( tmp_kw_name_1 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 183;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_unused );
    tmp_return_value = par_skipna;

    if ( tmp_return_value == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "skipna" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 184;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }

    Py_INCREF( tmp_return_value );
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2f5bab5df7791de2e0bdef305e996143 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_2f5bab5df7791de2e0bdef305e996143 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2f5bab5df7791de2e0bdef305e996143 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_2f5bab5df7791de2e0bdef305e996143, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_2f5bab5df7791de2e0bdef305e996143->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_2f5bab5df7791de2e0bdef305e996143, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_2f5bab5df7791de2e0bdef305e996143,
        type_description_1,
        par_skipna,
        par_args,
        par_kwargs,
        par_name
    );


    // Release cached frame.
    if ( frame_2f5bab5df7791de2e0bdef305e996143 == cache_frame_2f5bab5df7791de2e0bdef305e996143 )
    {
        Py_DECREF( frame_2f5bab5df7791de2e0bdef305e996143 );
    }
    cache_frame_2f5bab5df7791de2e0bdef305e996143 = NULL;

    assertFrameObject( frame_2f5bab5df7791de2e0bdef305e996143 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$compat$numpy$function$$$function_8_validate_cum_func_with_skipna );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_skipna );
    par_skipna = NULL;

    Py_XDECREF( par_args );
    par_args = NULL;

    Py_XDECREF( par_kwargs );
    par_kwargs = NULL;

    Py_XDECREF( par_name );
    par_name = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_skipna );
    par_skipna = NULL;

    Py_XDECREF( par_args );
    par_args = NULL;

    Py_XDECREF( par_kwargs );
    par_kwargs = NULL;

    Py_XDECREF( par_name );
    par_name = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$compat$numpy$function$$$function_8_validate_cum_func_with_skipna );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$compat$numpy$function$$$function_9_validate_take_with_convert( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_convert = python_pars[ 0 ];
    PyObject *par_args = python_pars[ 1 ];
    PyObject *par_kwargs = python_pars[ 2 ];
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_args_name_1;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_called_name_1;
    PyObject *tmp_compexpr_left_1;
    PyObject *tmp_compexpr_right_1;
    int tmp_cond_truth_1;
    PyObject *tmp_cond_value_1;
    PyObject *tmp_isinstance_cls_1;
    PyObject *tmp_isinstance_inst_1;
    PyObject *tmp_kw_name_1;
    PyObject *tmp_left_name_1;
    int tmp_or_left_truth_1;
    PyObject *tmp_or_left_value_1;
    PyObject *tmp_or_right_value_1;
    PyObject *tmp_return_value;
    PyObject *tmp_right_name_1;
    PyObject *tmp_tuple_element_1;
    PyObject *tmp_tuple_element_2;
    NUITKA_MAY_BE_UNUSED PyObject *tmp_unused;
    static struct Nuitka_FrameObject *cache_frame_a00567b750d5f828b6f606f204c47375 = NULL;

    struct Nuitka_FrameObject *frame_a00567b750d5f828b6f606f204c47375;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_a00567b750d5f828b6f606f204c47375, codeobj_a00567b750d5f828b6f606f204c47375, module_pandas$compat$numpy$function, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_a00567b750d5f828b6f606f204c47375 = cache_frame_a00567b750d5f828b6f606f204c47375;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_a00567b750d5f828b6f606f204c47375 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_a00567b750d5f828b6f606f204c47375 ) == 2 ); // Frame stack

    // Framed code:
    tmp_isinstance_inst_1 = par_convert;

    CHECK_OBJECT( tmp_isinstance_inst_1 );
    tmp_isinstance_cls_1 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_ndarray );

    if (unlikely( tmp_isinstance_cls_1 == NULL ))
    {
        tmp_isinstance_cls_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ndarray );
    }

    if ( tmp_isinstance_cls_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ndarray" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 254;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    tmp_or_left_value_1 = BUILTIN_ISINSTANCE( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
    if ( tmp_or_left_value_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 254;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    tmp_or_left_truth_1 = CHECK_IF_TRUE( tmp_or_left_value_1 );
    if ( tmp_or_left_truth_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 254;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    if ( tmp_or_left_truth_1 == 1 )
    {
        goto or_left_1;
    }
    else
    {
        goto or_right_1;
    }
    or_right_1:;
    tmp_compexpr_left_1 = par_convert;

    CHECK_OBJECT( tmp_compexpr_left_1 );
    tmp_compexpr_right_1 = Py_None;
    tmp_or_right_value_1 = BOOL_FROM( tmp_compexpr_left_1 == tmp_compexpr_right_1 );
    tmp_cond_value_1 = tmp_or_right_value_1;
    goto or_end_1;
    or_left_1:;
    tmp_cond_value_1 = tmp_or_left_value_1;
    or_end_1:;
    tmp_cond_truth_1 = CHECK_IF_TRUE( tmp_cond_value_1 );
    if ( tmp_cond_truth_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 254;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    if ( tmp_cond_truth_1 == 1 )
    {
        goto branch_yes_1;
    }
    else
    {
        goto branch_no_1;
    }
    branch_yes_1:;
    tmp_tuple_element_1 = par_convert;

    if ( tmp_tuple_element_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "convert" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 255;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    tmp_left_name_1 = PyTuple_New( 1 );
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_left_name_1, 0, tmp_tuple_element_1 );
    tmp_right_name_1 = par_args;

    if ( tmp_right_name_1 == NULL )
    {
        Py_DECREF( tmp_left_name_1 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "args" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 255;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    tmp_assign_source_1 = BINARY_OPERATION_ADD( tmp_left_name_1, tmp_right_name_1 );
    Py_DECREF( tmp_left_name_1 );
    if ( tmp_assign_source_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 255;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    {
        PyObject *old = par_args;
        par_args = tmp_assign_source_1;
        Py_XDECREF( old );
    }

    tmp_assign_source_2 = Py_True;
    {
        PyObject *old = par_convert;
        par_convert = tmp_assign_source_2;
        Py_INCREF( par_convert );
        Py_XDECREF( old );
    }

    branch_no_1:;
    tmp_called_name_1 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_validate_take );

    if (unlikely( tmp_called_name_1 == NULL ))
    {
        tmp_called_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_validate_take );
    }

    if ( tmp_called_name_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "validate_take" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 258;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    tmp_tuple_element_2 = par_args;

    if ( tmp_tuple_element_2 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "args" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 258;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    tmp_args_name_1 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_2 );
    PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_2 );
    tmp_tuple_element_2 = par_kwargs;

    if ( tmp_tuple_element_2 == NULL )
    {
        Py_DECREF( tmp_args_name_1 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "kwargs" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 258;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    Py_INCREF( tmp_tuple_element_2 );
    PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_2 );
    tmp_kw_name_1 = PyDict_Copy( const_dict_e83b47b89071cfcbed3a8af2f6814b7a );
    frame_a00567b750d5f828b6f606f204c47375->m_frame.f_lineno = 258;
    tmp_unused = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
    Py_DECREF( tmp_args_name_1 );
    Py_DECREF( tmp_kw_name_1 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 258;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_unused );
    tmp_return_value = par_convert;

    if ( tmp_return_value == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "convert" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 259;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    Py_INCREF( tmp_return_value );
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a00567b750d5f828b6f606f204c47375 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_a00567b750d5f828b6f606f204c47375 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a00567b750d5f828b6f606f204c47375 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_a00567b750d5f828b6f606f204c47375, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_a00567b750d5f828b6f606f204c47375->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_a00567b750d5f828b6f606f204c47375, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_a00567b750d5f828b6f606f204c47375,
        type_description_1,
        par_convert,
        par_args,
        par_kwargs
    );


    // Release cached frame.
    if ( frame_a00567b750d5f828b6f606f204c47375 == cache_frame_a00567b750d5f828b6f606f204c47375 )
    {
        Py_DECREF( frame_a00567b750d5f828b6f606f204c47375 );
    }
    cache_frame_a00567b750d5f828b6f606f204c47375 = NULL;

    assertFrameObject( frame_a00567b750d5f828b6f606f204c47375 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$compat$numpy$function$$$function_9_validate_take_with_convert );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_convert );
    par_convert = NULL;

    Py_XDECREF( par_args );
    par_args = NULL;

    Py_XDECREF( par_kwargs );
    par_kwargs = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_convert );
    par_convert = NULL;

    Py_XDECREF( par_args );
    par_args = NULL;

    Py_XDECREF( par_kwargs );
    par_kwargs = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$compat$numpy$function$$$function_9_validate_take_with_convert );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$compat$numpy$function$$$function_10_validate_transpose_for_generic( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_inst = python_pars[ 0 ];
    PyObject *par_kwargs = python_pars[ 1 ];
    PyObject *var_e = NULL;
    PyObject *var_klass = NULL;
    PyObject *var_msg = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    PyObject *tmp_args_element_name_1;
    PyObject *tmp_args_element_name_2;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_called_name_1;
    PyObject *tmp_called_name_2;
    int tmp_cmp_In_1;
    PyObject *tmp_compare_left_1;
    PyObject *tmp_compare_left_2;
    PyObject *tmp_compare_right_1;
    PyObject *tmp_compare_right_2;
    PyObject *tmp_dict_key_1;
    PyObject *tmp_dict_value_1;
    int tmp_exc_match_exception_match_1;
    PyObject *tmp_kw_name_1;
    PyObject *tmp_left_name_1;
    PyObject *tmp_make_exception_arg_1;
    PyObject *tmp_raise_type_1;
    int tmp_res;
    bool tmp_result;
    PyObject *tmp_return_value;
    PyObject *tmp_right_name_1;
    PyObject *tmp_source_name_1;
    PyObject *tmp_source_name_2;
    PyObject *tmp_type_arg_1;
    PyObject *tmp_unicode_arg_1;
    NUITKA_MAY_BE_UNUSED PyObject *tmp_unused;
    static struct Nuitka_FrameObject *cache_frame_664a2c3bc29db525ef3481f05ac9f980 = NULL;

    struct Nuitka_FrameObject *frame_664a2c3bc29db525ef3481f05ac9f980;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_664a2c3bc29db525ef3481f05ac9f980, codeobj_664a2c3bc29db525ef3481f05ac9f980, module_pandas$compat$numpy$function, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_664a2c3bc29db525ef3481f05ac9f980 = cache_frame_664a2c3bc29db525ef3481f05ac9f980;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_664a2c3bc29db525ef3481f05ac9f980 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_664a2c3bc29db525ef3481f05ac9f980 ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    tmp_called_name_1 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_validate_transpose );

    if (unlikely( tmp_called_name_1 == NULL ))
    {
        tmp_called_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_validate_transpose );
    }

    if ( tmp_called_name_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "validate_transpose" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 269;
        type_description_1 = "ooooo";
        goto try_except_handler_2;
    }

    tmp_args_element_name_1 = const_tuple_empty;
    tmp_args_element_name_2 = par_kwargs;

    CHECK_OBJECT( tmp_args_element_name_2 );
    frame_664a2c3bc29db525ef3481f05ac9f980->m_frame.f_lineno = 269;
    {
        PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
        tmp_unused = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
    }

    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 269;
        type_description_1 = "ooooo";
        goto try_except_handler_2;
    }
    Py_DECREF( tmp_unused );
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_664a2c3bc29db525ef3481f05ac9f980, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_664a2c3bc29db525ef3481f05ac9f980, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    // Tried code:
    tmp_compare_left_1 = EXC_TYPE(PyThreadState_GET());
    tmp_compare_right_1 = PyExc_ValueError;
    tmp_exc_match_exception_match_1 = EXCEPTION_MATCH_BOOL( tmp_compare_left_1, tmp_compare_right_1 );
    if ( tmp_exc_match_exception_match_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 270;
        type_description_1 = "ooooo";
        goto try_except_handler_3;
    }
    if ( tmp_exc_match_exception_match_1 == 1 )
    {
        goto branch_yes_1;
    }
    else
    {
        goto branch_no_1;
    }
    branch_yes_1:;
    tmp_assign_source_1 = EXC_VALUE(PyThreadState_GET());
    assert( var_e == NULL );
    Py_INCREF( tmp_assign_source_1 );
    var_e = tmp_assign_source_1;

    // Tried code:
    tmp_type_arg_1 = par_inst;

    if ( tmp_type_arg_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "inst" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 271;
        type_description_1 = "ooooo";
        goto try_except_handler_4;
    }

    tmp_source_name_1 = BUILTIN_TYPE1( tmp_type_arg_1 );
    if ( tmp_source_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 271;
        type_description_1 = "ooooo";
        goto try_except_handler_4;
    }
    tmp_assign_source_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___name__ );
    Py_DECREF( tmp_source_name_1 );
    if ( tmp_assign_source_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 271;
        type_description_1 = "ooooo";
        goto try_except_handler_4;
    }
    assert( var_klass == NULL );
    var_klass = tmp_assign_source_2;

    tmp_unicode_arg_1 = var_e;

    if ( tmp_unicode_arg_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "e" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 272;
        type_description_1 = "ooooo";
        goto try_except_handler_4;
    }

    tmp_assign_source_3 = PyObject_Unicode( tmp_unicode_arg_1 );
    if ( tmp_assign_source_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 272;
        type_description_1 = "ooooo";
        goto try_except_handler_4;
    }
    assert( var_msg == NULL );
    var_msg = tmp_assign_source_3;

    tmp_compare_left_2 = const_str_digest_1887ffcaa2a1b3781d3028198580af20;
    tmp_compare_right_2 = var_msg;

    CHECK_OBJECT( tmp_compare_right_2 );
    tmp_cmp_In_1 = PySequence_Contains( tmp_compare_right_2, tmp_compare_left_2 );
    assert( !(tmp_cmp_In_1 == -1) );
    if ( tmp_cmp_In_1 == 1 )
    {
        goto branch_yes_2;
    }
    else
    {
        goto branch_no_2;
    }
    branch_yes_2:;
    tmp_left_name_1 = var_msg;

    if ( tmp_left_name_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "msg" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 279;
        type_description_1 = "ooooo";
        goto try_except_handler_4;
    }

    tmp_source_name_2 = const_str_digest_e4aa0c2297935b61953390e5fec0e752;
    tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_format );
    assert( !(tmp_called_name_2 == NULL) );
    tmp_dict_key_1 = const_str_plain_klass;
    tmp_dict_value_1 = var_klass;

    if ( tmp_dict_value_1 == NULL )
    {
        Py_DECREF( tmp_called_name_2 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "klass" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 279;
        type_description_1 = "ooooo";
        goto try_except_handler_4;
    }

    tmp_kw_name_1 = _PyDict_NewPresized( 1 );
    tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
    assert( !(tmp_res != 0) );
    frame_664a2c3bc29db525ef3481f05ac9f980->m_frame.f_lineno = 279;
    tmp_right_name_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_2, tmp_kw_name_1 );
    Py_DECREF( tmp_called_name_2 );
    Py_DECREF( tmp_kw_name_1 );
    if ( tmp_right_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 279;
        type_description_1 = "ooooo";
        goto try_except_handler_4;
    }
    tmp_result = BINARY_OPERATION_ADD_INPLACE( &tmp_left_name_1, tmp_right_name_1 );
    tmp_assign_source_4 = tmp_left_name_1;
    Py_DECREF( tmp_right_name_1 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 279;
        type_description_1 = "ooooo";
        goto try_except_handler_4;
    }
    var_msg = tmp_assign_source_4;

    branch_no_2:;
    tmp_make_exception_arg_1 = var_msg;

    if ( tmp_make_exception_arg_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "msg" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 281;
        type_description_1 = "ooooo";
        goto try_except_handler_4;
    }

    frame_664a2c3bc29db525ef3481f05ac9f980->m_frame.f_lineno = 281;
    {
        PyObject *call_args[] = { tmp_make_exception_arg_1 };
        tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_ValueError, call_args );
    }

    assert( !(tmp_raise_type_1 == NULL) );
    exception_type = tmp_raise_type_1;
    exception_lineno = 281;
    RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
    type_description_1 = "ooooo";
    goto try_except_handler_4;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$compat$numpy$function$$$function_10_validate_transpose_for_generic );
    return NULL;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( var_e );
    var_e = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto try_except_handler_3;
    // End of try:
    goto branch_end_1;
    branch_no_1:;
    tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (unlikely( tmp_result == false ))
    {
        exception_lineno = 268;
    }

    if (exception_tb && exception_tb->tb_frame == &frame_664a2c3bc29db525ef3481f05ac9f980->m_frame) frame_664a2c3bc29db525ef3481f05ac9f980->m_frame.f_lineno = exception_tb->tb_lineno;
    type_description_1 = "ooooo";
    goto try_except_handler_3;
    branch_end_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$compat$numpy$function$$$function_10_validate_transpose_for_generic );
    return NULL;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    // End of try:
    try_end_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_664a2c3bc29db525ef3481f05ac9f980 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_664a2c3bc29db525ef3481f05ac9f980 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_664a2c3bc29db525ef3481f05ac9f980, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_664a2c3bc29db525ef3481f05ac9f980->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_664a2c3bc29db525ef3481f05ac9f980, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_664a2c3bc29db525ef3481f05ac9f980,
        type_description_1,
        par_inst,
        par_kwargs,
        var_e,
        var_klass,
        var_msg
    );


    // Release cached frame.
    if ( frame_664a2c3bc29db525ef3481f05ac9f980 == cache_frame_664a2c3bc29db525ef3481f05ac9f980 )
    {
        Py_DECREF( frame_664a2c3bc29db525ef3481f05ac9f980 );
    }
    cache_frame_664a2c3bc29db525ef3481f05ac9f980 = NULL;

    assertFrameObject( frame_664a2c3bc29db525ef3481f05ac9f980 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$compat$numpy$function$$$function_10_validate_transpose_for_generic );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_inst );
    par_inst = NULL;

    Py_XDECREF( par_kwargs );
    par_kwargs = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_inst );
    par_inst = NULL;

    Py_XDECREF( par_kwargs );
    par_kwargs = NULL;

    Py_XDECREF( var_e );
    var_e = NULL;

    Py_XDECREF( var_klass );
    var_klass = NULL;

    Py_XDECREF( var_msg );
    var_msg = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$compat$numpy$function$$$function_10_validate_transpose_for_generic );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$compat$numpy$function$$$function_11_validate_window_func( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_name = python_pars[ 0 ];
    PyObject *par_args = python_pars[ 1 ];
    PyObject *par_kwargs = python_pars[ 2 ];
    PyObject *var_numpy_args = NULL;
    PyObject *var_msg = NULL;
    PyObject *var_arg = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *tmp_args_element_name_1;
    PyObject *tmp_args_element_name_2;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_assign_source_5;
    PyObject *tmp_called_name_1;
    PyObject *tmp_called_name_2;
    PyObject *tmp_called_name_3;
    int tmp_cmp_Gt_1;
    int tmp_cmp_In_1;
    PyObject *tmp_compare_left_1;
    PyObject *tmp_compare_left_2;
    PyObject *tmp_compare_left_3;
    PyObject *tmp_compare_right_1;
    PyObject *tmp_compare_right_2;
    PyObject *tmp_compare_right_3;
    PyObject *tmp_dict_key_1;
    PyObject *tmp_dict_value_1;
    int tmp_exc_match_exception_match_1;
    PyObject *tmp_iter_arg_1;
    PyObject *tmp_kw_name_1;
    PyObject *tmp_len_arg_1;
    PyObject *tmp_raise_type_1;
    PyObject *tmp_raise_type_2;
    int tmp_res;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    PyObject *tmp_value_name_1;
    static struct Nuitka_FrameObject *cache_frame_e5daa672e9453708a21f3999fd18e914 = NULL;

    struct Nuitka_FrameObject *frame_e5daa672e9453708a21f3999fd18e914;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    tmp_assign_source_1 = const_tuple_str_plain_axis_str_plain_dtype_str_plain_out_tuple;
    assert( var_numpy_args == NULL );
    Py_INCREF( tmp_assign_source_1 );
    var_numpy_args = tmp_assign_source_1;

    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_e5daa672e9453708a21f3999fd18e914, codeobj_e5daa672e9453708a21f3999fd18e914, module_pandas$compat$numpy$function, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_e5daa672e9453708a21f3999fd18e914 = cache_frame_e5daa672e9453708a21f3999fd18e914;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_e5daa672e9453708a21f3999fd18e914 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_e5daa672e9453708a21f3999fd18e914 ) == 2 ); // Frame stack

    // Framed code:
    tmp_source_name_1 = const_str_digest_5b73885ba63bd4fb387bd87a8e6c9ca9;
    tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_format );
    assert( !(tmp_called_name_1 == NULL) );
    tmp_dict_key_1 = const_str_plain_func;
    tmp_dict_value_1 = par_name;

    CHECK_OBJECT( tmp_dict_value_1 );
    tmp_kw_name_1 = _PyDict_NewPresized( 1 );
    tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
    assert( !(tmp_res != 0) );
    frame_e5daa672e9453708a21f3999fd18e914->m_frame.f_lineno = 286;
    tmp_assign_source_2 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
    Py_DECREF( tmp_called_name_1 );
    Py_DECREF( tmp_kw_name_1 );
    if ( tmp_assign_source_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 286;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }
    assert( var_msg == NULL );
    var_msg = tmp_assign_source_2;

    tmp_len_arg_1 = par_args;

    if ( tmp_len_arg_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "args" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 290;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }

    tmp_compare_left_1 = BUILTIN_LEN( tmp_len_arg_1 );
    if ( tmp_compare_left_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 290;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }
    tmp_compare_right_1 = const_int_0;
    tmp_cmp_Gt_1 = RICH_COMPARE_BOOL_GT( tmp_compare_left_1, tmp_compare_right_1 );
    Py_DECREF( tmp_compare_left_1 );
    if ( tmp_cmp_Gt_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 290;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }
    if ( tmp_cmp_Gt_1 == 1 )
    {
        goto branch_yes_1;
    }
    else
    {
        goto branch_no_1;
    }
    branch_yes_1:;
    tmp_called_name_2 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_UnsupportedFunctionCall );

    if (unlikely( tmp_called_name_2 == NULL ))
    {
        tmp_called_name_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_UnsupportedFunctionCall );
    }

    if ( tmp_called_name_2 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "UnsupportedFunctionCall" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 291;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_1 = var_msg;

    if ( tmp_args_element_name_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "msg" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 291;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }

    frame_e5daa672e9453708a21f3999fd18e914->m_frame.f_lineno = 291;
    {
        PyObject *call_args[] = { tmp_args_element_name_1 };
        tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
    }

    if ( tmp_raise_type_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 291;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }
    exception_type = tmp_raise_type_1;
    exception_lineno = 291;
    RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
    type_description_1 = "oooooo";
    goto frame_exception_exit_1;
    branch_no_1:;
    tmp_iter_arg_1 = var_numpy_args;

    if ( tmp_iter_arg_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "numpy_args" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 293;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }

    tmp_assign_source_3 = MAKE_ITERATOR( tmp_iter_arg_1 );
    if ( tmp_assign_source_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 293;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }
    assert( tmp_for_loop_1__for_iterator == NULL );
    tmp_for_loop_1__for_iterator = tmp_assign_source_3;

    // Tried code:
    loop_start_1:;
    // Tried code:
    tmp_value_name_1 = tmp_for_loop_1__for_iterator;

    CHECK_OBJECT( tmp_value_name_1 );
    tmp_assign_source_4 = ITERATOR_NEXT( tmp_value_name_1 );
    if ( tmp_assign_source_4 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        type_description_1 = "oooooo";
        exception_lineno = 293;
        goto try_except_handler_3;
    }
    {
        PyObject *old = tmp_for_loop_1__iter_value;
        tmp_for_loop_1__iter_value = tmp_assign_source_4;
        Py_XDECREF( old );
    }

    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    tmp_compare_left_2 = exception_keeper_type_1;
    tmp_compare_right_2 = PyExc_StopIteration;
    tmp_exc_match_exception_match_1 = EXCEPTION_MATCH_BOOL( tmp_compare_left_2, tmp_compare_right_2 );
    if ( tmp_exc_match_exception_match_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

        Py_DECREF( exception_keeper_type_1 );
        Py_XDECREF( exception_keeper_value_1 );
        Py_XDECREF( exception_keeper_tb_1 );

        exception_lineno = 293;
        type_description_1 = "oooooo";
        goto try_except_handler_2;
    }
    if ( tmp_exc_match_exception_match_1 == 1 )
    {
        goto branch_yes_2;
    }
    else
    {
        goto branch_no_2;
    }
    branch_yes_2:;
    Py_DECREF( exception_keeper_type_1 );
    Py_XDECREF( exception_keeper_value_1 );
    Py_XDECREF( exception_keeper_tb_1 );
    goto loop_end_1;
    goto branch_end_2;
    branch_no_2:;
    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_2;
    branch_end_2:;
    // End of try:
    try_end_1:;
    tmp_assign_source_5 = tmp_for_loop_1__iter_value;

    CHECK_OBJECT( tmp_assign_source_5 );
    {
        PyObject *old = var_arg;
        var_arg = tmp_assign_source_5;
        Py_INCREF( var_arg );
        Py_XDECREF( old );
    }

    tmp_compare_left_3 = var_arg;

    CHECK_OBJECT( tmp_compare_left_3 );
    tmp_compare_right_3 = par_kwargs;

    if ( tmp_compare_right_3 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "kwargs" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 294;
        type_description_1 = "oooooo";
        goto try_except_handler_2;
    }

    tmp_cmp_In_1 = PySequence_Contains( tmp_compare_right_3, tmp_compare_left_3 );
    assert( !(tmp_cmp_In_1 == -1) );
    if ( tmp_cmp_In_1 == 1 )
    {
        goto branch_yes_3;
    }
    else
    {
        goto branch_no_3;
    }
    branch_yes_3:;
    tmp_called_name_3 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_UnsupportedFunctionCall );

    if (unlikely( tmp_called_name_3 == NULL ))
    {
        tmp_called_name_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_UnsupportedFunctionCall );
    }

    if ( tmp_called_name_3 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "UnsupportedFunctionCall" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 295;
        type_description_1 = "oooooo";
        goto try_except_handler_2;
    }

    tmp_args_element_name_2 = var_msg;

    if ( tmp_args_element_name_2 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "msg" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 295;
        type_description_1 = "oooooo";
        goto try_except_handler_2;
    }

    frame_e5daa672e9453708a21f3999fd18e914->m_frame.f_lineno = 295;
    {
        PyObject *call_args[] = { tmp_args_element_name_2 };
        tmp_raise_type_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
    }

    if ( tmp_raise_type_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 295;
        type_description_1 = "oooooo";
        goto try_except_handler_2;
    }
    exception_type = tmp_raise_type_2;
    exception_lineno = 295;
    RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
    type_description_1 = "oooooo";
    goto try_except_handler_2;
    branch_no_3:;
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 293;
        type_description_1 = "oooooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e5daa672e9453708a21f3999fd18e914 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e5daa672e9453708a21f3999fd18e914 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_e5daa672e9453708a21f3999fd18e914, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_e5daa672e9453708a21f3999fd18e914->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_e5daa672e9453708a21f3999fd18e914, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_e5daa672e9453708a21f3999fd18e914,
        type_description_1,
        par_name,
        par_args,
        par_kwargs,
        var_numpy_args,
        var_msg,
        var_arg
    );


    // Release cached frame.
    if ( frame_e5daa672e9453708a21f3999fd18e914 == cache_frame_e5daa672e9453708a21f3999fd18e914 )
    {
        Py_DECREF( frame_e5daa672e9453708a21f3999fd18e914 );
    }
    cache_frame_e5daa672e9453708a21f3999fd18e914 = NULL;

    assertFrameObject( frame_e5daa672e9453708a21f3999fd18e914 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$compat$numpy$function$$$function_11_validate_window_func );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_name );
    par_name = NULL;

    Py_XDECREF( par_args );
    par_args = NULL;

    Py_XDECREF( par_kwargs );
    par_kwargs = NULL;

    Py_XDECREF( var_numpy_args );
    var_numpy_args = NULL;

    Py_XDECREF( var_msg );
    var_msg = NULL;

    Py_XDECREF( var_arg );
    var_arg = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_name );
    par_name = NULL;

    Py_XDECREF( par_args );
    par_args = NULL;

    Py_XDECREF( par_kwargs );
    par_kwargs = NULL;

    Py_XDECREF( var_numpy_args );
    var_numpy_args = NULL;

    Py_XDECREF( var_msg );
    var_msg = NULL;

    Py_XDECREF( var_arg );
    var_arg = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$compat$numpy$function$$$function_11_validate_window_func );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$compat$numpy$function$$$function_12_validate_rolling_func( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_name = python_pars[ 0 ];
    PyObject *par_args = python_pars[ 1 ];
    PyObject *par_kwargs = python_pars[ 2 ];
    PyObject *var_numpy_args = NULL;
    PyObject *var_msg = NULL;
    PyObject *var_arg = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *tmp_args_element_name_1;
    PyObject *tmp_args_element_name_2;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_assign_source_5;
    PyObject *tmp_called_name_1;
    PyObject *tmp_called_name_2;
    PyObject *tmp_called_name_3;
    int tmp_cmp_Gt_1;
    int tmp_cmp_In_1;
    PyObject *tmp_compare_left_1;
    PyObject *tmp_compare_left_2;
    PyObject *tmp_compare_left_3;
    PyObject *tmp_compare_right_1;
    PyObject *tmp_compare_right_2;
    PyObject *tmp_compare_right_3;
    PyObject *tmp_dict_key_1;
    PyObject *tmp_dict_value_1;
    int tmp_exc_match_exception_match_1;
    PyObject *tmp_iter_arg_1;
    PyObject *tmp_kw_name_1;
    PyObject *tmp_len_arg_1;
    PyObject *tmp_raise_type_1;
    PyObject *tmp_raise_type_2;
    int tmp_res;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    PyObject *tmp_value_name_1;
    static struct Nuitka_FrameObject *cache_frame_8dc1d850a1d11f56ce7e376c4bba1267 = NULL;

    struct Nuitka_FrameObject *frame_8dc1d850a1d11f56ce7e376c4bba1267;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    tmp_assign_source_1 = const_tuple_str_plain_axis_str_plain_dtype_str_plain_out_tuple;
    assert( var_numpy_args == NULL );
    Py_INCREF( tmp_assign_source_1 );
    var_numpy_args = tmp_assign_source_1;

    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_8dc1d850a1d11f56ce7e376c4bba1267, codeobj_8dc1d850a1d11f56ce7e376c4bba1267, module_pandas$compat$numpy$function, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_8dc1d850a1d11f56ce7e376c4bba1267 = cache_frame_8dc1d850a1d11f56ce7e376c4bba1267;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_8dc1d850a1d11f56ce7e376c4bba1267 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_8dc1d850a1d11f56ce7e376c4bba1267 ) == 2 ); // Frame stack

    // Framed code:
    tmp_source_name_1 = const_str_digest_0d9582d19734cfbfc922a1db935b51b9;
    tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_format );
    assert( !(tmp_called_name_1 == NULL) );
    tmp_dict_key_1 = const_str_plain_func;
    tmp_dict_value_1 = par_name;

    CHECK_OBJECT( tmp_dict_value_1 );
    tmp_kw_name_1 = _PyDict_NewPresized( 1 );
    tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
    assert( !(tmp_res != 0) );
    frame_8dc1d850a1d11f56ce7e376c4bba1267->m_frame.f_lineno = 300;
    tmp_assign_source_2 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
    Py_DECREF( tmp_called_name_1 );
    Py_DECREF( tmp_kw_name_1 );
    if ( tmp_assign_source_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 300;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }
    assert( var_msg == NULL );
    var_msg = tmp_assign_source_2;

    tmp_len_arg_1 = par_args;

    if ( tmp_len_arg_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "args" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 304;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }

    tmp_compare_left_1 = BUILTIN_LEN( tmp_len_arg_1 );
    if ( tmp_compare_left_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 304;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }
    tmp_compare_right_1 = const_int_0;
    tmp_cmp_Gt_1 = RICH_COMPARE_BOOL_GT( tmp_compare_left_1, tmp_compare_right_1 );
    Py_DECREF( tmp_compare_left_1 );
    if ( tmp_cmp_Gt_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 304;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }
    if ( tmp_cmp_Gt_1 == 1 )
    {
        goto branch_yes_1;
    }
    else
    {
        goto branch_no_1;
    }
    branch_yes_1:;
    tmp_called_name_2 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_UnsupportedFunctionCall );

    if (unlikely( tmp_called_name_2 == NULL ))
    {
        tmp_called_name_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_UnsupportedFunctionCall );
    }

    if ( tmp_called_name_2 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "UnsupportedFunctionCall" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 305;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_1 = var_msg;

    if ( tmp_args_element_name_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "msg" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 305;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }

    frame_8dc1d850a1d11f56ce7e376c4bba1267->m_frame.f_lineno = 305;
    {
        PyObject *call_args[] = { tmp_args_element_name_1 };
        tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
    }

    if ( tmp_raise_type_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 305;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }
    exception_type = tmp_raise_type_1;
    exception_lineno = 305;
    RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
    type_description_1 = "oooooo";
    goto frame_exception_exit_1;
    branch_no_1:;
    tmp_iter_arg_1 = var_numpy_args;

    if ( tmp_iter_arg_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "numpy_args" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 307;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }

    tmp_assign_source_3 = MAKE_ITERATOR( tmp_iter_arg_1 );
    if ( tmp_assign_source_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 307;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }
    assert( tmp_for_loop_1__for_iterator == NULL );
    tmp_for_loop_1__for_iterator = tmp_assign_source_3;

    // Tried code:
    loop_start_1:;
    // Tried code:
    tmp_value_name_1 = tmp_for_loop_1__for_iterator;

    CHECK_OBJECT( tmp_value_name_1 );
    tmp_assign_source_4 = ITERATOR_NEXT( tmp_value_name_1 );
    if ( tmp_assign_source_4 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        type_description_1 = "oooooo";
        exception_lineno = 307;
        goto try_except_handler_3;
    }
    {
        PyObject *old = tmp_for_loop_1__iter_value;
        tmp_for_loop_1__iter_value = tmp_assign_source_4;
        Py_XDECREF( old );
    }

    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    tmp_compare_left_2 = exception_keeper_type_1;
    tmp_compare_right_2 = PyExc_StopIteration;
    tmp_exc_match_exception_match_1 = EXCEPTION_MATCH_BOOL( tmp_compare_left_2, tmp_compare_right_2 );
    if ( tmp_exc_match_exception_match_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

        Py_DECREF( exception_keeper_type_1 );
        Py_XDECREF( exception_keeper_value_1 );
        Py_XDECREF( exception_keeper_tb_1 );

        exception_lineno = 307;
        type_description_1 = "oooooo";
        goto try_except_handler_2;
    }
    if ( tmp_exc_match_exception_match_1 == 1 )
    {
        goto branch_yes_2;
    }
    else
    {
        goto branch_no_2;
    }
    branch_yes_2:;
    Py_DECREF( exception_keeper_type_1 );
    Py_XDECREF( exception_keeper_value_1 );
    Py_XDECREF( exception_keeper_tb_1 );
    goto loop_end_1;
    goto branch_end_2;
    branch_no_2:;
    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_2;
    branch_end_2:;
    // End of try:
    try_end_1:;
    tmp_assign_source_5 = tmp_for_loop_1__iter_value;

    CHECK_OBJECT( tmp_assign_source_5 );
    {
        PyObject *old = var_arg;
        var_arg = tmp_assign_source_5;
        Py_INCREF( var_arg );
        Py_XDECREF( old );
    }

    tmp_compare_left_3 = var_arg;

    CHECK_OBJECT( tmp_compare_left_3 );
    tmp_compare_right_3 = par_kwargs;

    if ( tmp_compare_right_3 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "kwargs" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 308;
        type_description_1 = "oooooo";
        goto try_except_handler_2;
    }

    tmp_cmp_In_1 = PySequence_Contains( tmp_compare_right_3, tmp_compare_left_3 );
    assert( !(tmp_cmp_In_1 == -1) );
    if ( tmp_cmp_In_1 == 1 )
    {
        goto branch_yes_3;
    }
    else
    {
        goto branch_no_3;
    }
    branch_yes_3:;
    tmp_called_name_3 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_UnsupportedFunctionCall );

    if (unlikely( tmp_called_name_3 == NULL ))
    {
        tmp_called_name_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_UnsupportedFunctionCall );
    }

    if ( tmp_called_name_3 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "UnsupportedFunctionCall" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 309;
        type_description_1 = "oooooo";
        goto try_except_handler_2;
    }

    tmp_args_element_name_2 = var_msg;

    if ( tmp_args_element_name_2 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "msg" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 309;
        type_description_1 = "oooooo";
        goto try_except_handler_2;
    }

    frame_8dc1d850a1d11f56ce7e376c4bba1267->m_frame.f_lineno = 309;
    {
        PyObject *call_args[] = { tmp_args_element_name_2 };
        tmp_raise_type_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
    }

    if ( tmp_raise_type_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 309;
        type_description_1 = "oooooo";
        goto try_except_handler_2;
    }
    exception_type = tmp_raise_type_2;
    exception_lineno = 309;
    RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
    type_description_1 = "oooooo";
    goto try_except_handler_2;
    branch_no_3:;
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 307;
        type_description_1 = "oooooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8dc1d850a1d11f56ce7e376c4bba1267 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8dc1d850a1d11f56ce7e376c4bba1267 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_8dc1d850a1d11f56ce7e376c4bba1267, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_8dc1d850a1d11f56ce7e376c4bba1267->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_8dc1d850a1d11f56ce7e376c4bba1267, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_8dc1d850a1d11f56ce7e376c4bba1267,
        type_description_1,
        par_name,
        par_args,
        par_kwargs,
        var_numpy_args,
        var_msg,
        var_arg
    );


    // Release cached frame.
    if ( frame_8dc1d850a1d11f56ce7e376c4bba1267 == cache_frame_8dc1d850a1d11f56ce7e376c4bba1267 )
    {
        Py_DECREF( frame_8dc1d850a1d11f56ce7e376c4bba1267 );
    }
    cache_frame_8dc1d850a1d11f56ce7e376c4bba1267 = NULL;

    assertFrameObject( frame_8dc1d850a1d11f56ce7e376c4bba1267 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$compat$numpy$function$$$function_12_validate_rolling_func );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_name );
    par_name = NULL;

    Py_XDECREF( par_args );
    par_args = NULL;

    Py_XDECREF( par_kwargs );
    par_kwargs = NULL;

    Py_XDECREF( var_numpy_args );
    var_numpy_args = NULL;

    Py_XDECREF( var_msg );
    var_msg = NULL;

    Py_XDECREF( var_arg );
    var_arg = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_name );
    par_name = NULL;

    Py_XDECREF( par_args );
    par_args = NULL;

    Py_XDECREF( par_kwargs );
    par_kwargs = NULL;

    Py_XDECREF( var_numpy_args );
    var_numpy_args = NULL;

    Py_XDECREF( var_msg );
    var_msg = NULL;

    Py_XDECREF( var_arg );
    var_arg = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$compat$numpy$function$$$function_12_validate_rolling_func );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$compat$numpy$function$$$function_13_validate_expanding_func( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_name = python_pars[ 0 ];
    PyObject *par_args = python_pars[ 1 ];
    PyObject *par_kwargs = python_pars[ 2 ];
    PyObject *var_numpy_args = NULL;
    PyObject *var_msg = NULL;
    PyObject *var_arg = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *tmp_args_element_name_1;
    PyObject *tmp_args_element_name_2;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_assign_source_5;
    PyObject *tmp_called_name_1;
    PyObject *tmp_called_name_2;
    PyObject *tmp_called_name_3;
    int tmp_cmp_Gt_1;
    int tmp_cmp_In_1;
    PyObject *tmp_compare_left_1;
    PyObject *tmp_compare_left_2;
    PyObject *tmp_compare_left_3;
    PyObject *tmp_compare_right_1;
    PyObject *tmp_compare_right_2;
    PyObject *tmp_compare_right_3;
    PyObject *tmp_dict_key_1;
    PyObject *tmp_dict_value_1;
    int tmp_exc_match_exception_match_1;
    PyObject *tmp_iter_arg_1;
    PyObject *tmp_kw_name_1;
    PyObject *tmp_len_arg_1;
    PyObject *tmp_raise_type_1;
    PyObject *tmp_raise_type_2;
    int tmp_res;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    PyObject *tmp_value_name_1;
    static struct Nuitka_FrameObject *cache_frame_220060a11a280eed1c6dad14776295d7 = NULL;

    struct Nuitka_FrameObject *frame_220060a11a280eed1c6dad14776295d7;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    tmp_assign_source_1 = const_tuple_str_plain_axis_str_plain_dtype_str_plain_out_tuple;
    assert( var_numpy_args == NULL );
    Py_INCREF( tmp_assign_source_1 );
    var_numpy_args = tmp_assign_source_1;

    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_220060a11a280eed1c6dad14776295d7, codeobj_220060a11a280eed1c6dad14776295d7, module_pandas$compat$numpy$function, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_220060a11a280eed1c6dad14776295d7 = cache_frame_220060a11a280eed1c6dad14776295d7;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_220060a11a280eed1c6dad14776295d7 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_220060a11a280eed1c6dad14776295d7 ) == 2 ); // Frame stack

    // Framed code:
    tmp_source_name_1 = const_str_digest_ce9fc9ccbad74a97cac165256191b91e;
    tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_format );
    assert( !(tmp_called_name_1 == NULL) );
    tmp_dict_key_1 = const_str_plain_func;
    tmp_dict_value_1 = par_name;

    CHECK_OBJECT( tmp_dict_value_1 );
    tmp_kw_name_1 = _PyDict_NewPresized( 1 );
    tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
    assert( !(tmp_res != 0) );
    frame_220060a11a280eed1c6dad14776295d7->m_frame.f_lineno = 314;
    tmp_assign_source_2 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
    Py_DECREF( tmp_called_name_1 );
    Py_DECREF( tmp_kw_name_1 );
    if ( tmp_assign_source_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 314;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }
    assert( var_msg == NULL );
    var_msg = tmp_assign_source_2;

    tmp_len_arg_1 = par_args;

    if ( tmp_len_arg_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "args" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 318;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }

    tmp_compare_left_1 = BUILTIN_LEN( tmp_len_arg_1 );
    if ( tmp_compare_left_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 318;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }
    tmp_compare_right_1 = const_int_0;
    tmp_cmp_Gt_1 = RICH_COMPARE_BOOL_GT( tmp_compare_left_1, tmp_compare_right_1 );
    Py_DECREF( tmp_compare_left_1 );
    if ( tmp_cmp_Gt_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 318;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }
    if ( tmp_cmp_Gt_1 == 1 )
    {
        goto branch_yes_1;
    }
    else
    {
        goto branch_no_1;
    }
    branch_yes_1:;
    tmp_called_name_2 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_UnsupportedFunctionCall );

    if (unlikely( tmp_called_name_2 == NULL ))
    {
        tmp_called_name_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_UnsupportedFunctionCall );
    }

    if ( tmp_called_name_2 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "UnsupportedFunctionCall" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 319;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_1 = var_msg;

    if ( tmp_args_element_name_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "msg" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 319;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }

    frame_220060a11a280eed1c6dad14776295d7->m_frame.f_lineno = 319;
    {
        PyObject *call_args[] = { tmp_args_element_name_1 };
        tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
    }

    if ( tmp_raise_type_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 319;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }
    exception_type = tmp_raise_type_1;
    exception_lineno = 319;
    RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
    type_description_1 = "oooooo";
    goto frame_exception_exit_1;
    branch_no_1:;
    tmp_iter_arg_1 = var_numpy_args;

    if ( tmp_iter_arg_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "numpy_args" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 321;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }

    tmp_assign_source_3 = MAKE_ITERATOR( tmp_iter_arg_1 );
    if ( tmp_assign_source_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 321;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }
    assert( tmp_for_loop_1__for_iterator == NULL );
    tmp_for_loop_1__for_iterator = tmp_assign_source_3;

    // Tried code:
    loop_start_1:;
    // Tried code:
    tmp_value_name_1 = tmp_for_loop_1__for_iterator;

    CHECK_OBJECT( tmp_value_name_1 );
    tmp_assign_source_4 = ITERATOR_NEXT( tmp_value_name_1 );
    if ( tmp_assign_source_4 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        type_description_1 = "oooooo";
        exception_lineno = 321;
        goto try_except_handler_3;
    }
    {
        PyObject *old = tmp_for_loop_1__iter_value;
        tmp_for_loop_1__iter_value = tmp_assign_source_4;
        Py_XDECREF( old );
    }

    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    tmp_compare_left_2 = exception_keeper_type_1;
    tmp_compare_right_2 = PyExc_StopIteration;
    tmp_exc_match_exception_match_1 = EXCEPTION_MATCH_BOOL( tmp_compare_left_2, tmp_compare_right_2 );
    if ( tmp_exc_match_exception_match_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

        Py_DECREF( exception_keeper_type_1 );
        Py_XDECREF( exception_keeper_value_1 );
        Py_XDECREF( exception_keeper_tb_1 );

        exception_lineno = 321;
        type_description_1 = "oooooo";
        goto try_except_handler_2;
    }
    if ( tmp_exc_match_exception_match_1 == 1 )
    {
        goto branch_yes_2;
    }
    else
    {
        goto branch_no_2;
    }
    branch_yes_2:;
    Py_DECREF( exception_keeper_type_1 );
    Py_XDECREF( exception_keeper_value_1 );
    Py_XDECREF( exception_keeper_tb_1 );
    goto loop_end_1;
    goto branch_end_2;
    branch_no_2:;
    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_2;
    branch_end_2:;
    // End of try:
    try_end_1:;
    tmp_assign_source_5 = tmp_for_loop_1__iter_value;

    CHECK_OBJECT( tmp_assign_source_5 );
    {
        PyObject *old = var_arg;
        var_arg = tmp_assign_source_5;
        Py_INCREF( var_arg );
        Py_XDECREF( old );
    }

    tmp_compare_left_3 = var_arg;

    CHECK_OBJECT( tmp_compare_left_3 );
    tmp_compare_right_3 = par_kwargs;

    if ( tmp_compare_right_3 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "kwargs" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 322;
        type_description_1 = "oooooo";
        goto try_except_handler_2;
    }

    tmp_cmp_In_1 = PySequence_Contains( tmp_compare_right_3, tmp_compare_left_3 );
    assert( !(tmp_cmp_In_1 == -1) );
    if ( tmp_cmp_In_1 == 1 )
    {
        goto branch_yes_3;
    }
    else
    {
        goto branch_no_3;
    }
    branch_yes_3:;
    tmp_called_name_3 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_UnsupportedFunctionCall );

    if (unlikely( tmp_called_name_3 == NULL ))
    {
        tmp_called_name_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_UnsupportedFunctionCall );
    }

    if ( tmp_called_name_3 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "UnsupportedFunctionCall" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 323;
        type_description_1 = "oooooo";
        goto try_except_handler_2;
    }

    tmp_args_element_name_2 = var_msg;

    if ( tmp_args_element_name_2 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "msg" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 323;
        type_description_1 = "oooooo";
        goto try_except_handler_2;
    }

    frame_220060a11a280eed1c6dad14776295d7->m_frame.f_lineno = 323;
    {
        PyObject *call_args[] = { tmp_args_element_name_2 };
        tmp_raise_type_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
    }

    if ( tmp_raise_type_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 323;
        type_description_1 = "oooooo";
        goto try_except_handler_2;
    }
    exception_type = tmp_raise_type_2;
    exception_lineno = 323;
    RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
    type_description_1 = "oooooo";
    goto try_except_handler_2;
    branch_no_3:;
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 321;
        type_description_1 = "oooooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_220060a11a280eed1c6dad14776295d7 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_220060a11a280eed1c6dad14776295d7 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_220060a11a280eed1c6dad14776295d7, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_220060a11a280eed1c6dad14776295d7->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_220060a11a280eed1c6dad14776295d7, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_220060a11a280eed1c6dad14776295d7,
        type_description_1,
        par_name,
        par_args,
        par_kwargs,
        var_numpy_args,
        var_msg,
        var_arg
    );


    // Release cached frame.
    if ( frame_220060a11a280eed1c6dad14776295d7 == cache_frame_220060a11a280eed1c6dad14776295d7 )
    {
        Py_DECREF( frame_220060a11a280eed1c6dad14776295d7 );
    }
    cache_frame_220060a11a280eed1c6dad14776295d7 = NULL;

    assertFrameObject( frame_220060a11a280eed1c6dad14776295d7 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$compat$numpy$function$$$function_13_validate_expanding_func );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_name );
    par_name = NULL;

    Py_XDECREF( par_args );
    par_args = NULL;

    Py_XDECREF( par_kwargs );
    par_kwargs = NULL;

    Py_XDECREF( var_numpy_args );
    var_numpy_args = NULL;

    Py_XDECREF( var_msg );
    var_msg = NULL;

    Py_XDECREF( var_arg );
    var_arg = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_name );
    par_name = NULL;

    Py_XDECREF( par_args );
    par_args = NULL;

    Py_XDECREF( par_kwargs );
    par_kwargs = NULL;

    Py_XDECREF( var_numpy_args );
    var_numpy_args = NULL;

    Py_XDECREF( var_msg );
    var_msg = NULL;

    Py_XDECREF( var_arg );
    var_arg = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$compat$numpy$function$$$function_13_validate_expanding_func );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$compat$numpy$function$$$function_14_validate_groupby_func( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_name = python_pars[ 0 ];
    PyObject *par_args = python_pars[ 1 ];
    PyObject *par_kwargs = python_pars[ 2 ];
    PyObject *par_allowed = python_pars[ 3 ];
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_args_element_name_1;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_called_name_1;
    PyObject *tmp_called_name_2;
    int tmp_cmp_Gt_1;
    PyObject *tmp_compare_left_1;
    PyObject *tmp_compare_left_2;
    PyObject *tmp_compare_right_1;
    PyObject *tmp_compare_right_2;
    PyObject *tmp_dict_key_1;
    PyObject *tmp_dict_value_1;
    bool tmp_is_1;
    PyObject *tmp_kw_name_1;
    PyObject *tmp_left_name_1;
    PyObject *tmp_left_name_2;
    PyObject *tmp_len_arg_1;
    PyObject *tmp_len_arg_2;
    PyObject *tmp_raise_type_1;
    int tmp_res;
    PyObject *tmp_return_value;
    PyObject *tmp_right_name_1;
    PyObject *tmp_right_name_2;
    PyObject *tmp_set_arg_1;
    PyObject *tmp_set_arg_2;
    PyObject *tmp_source_name_1;
    static struct Nuitka_FrameObject *cache_frame_2ca6a45202a14a8e4efcd82e44936641 = NULL;

    struct Nuitka_FrameObject *frame_2ca6a45202a14a8e4efcd82e44936641;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    tmp_compare_left_1 = par_allowed;

    CHECK_OBJECT( tmp_compare_left_1 );
    tmp_compare_right_1 = Py_None;
    tmp_is_1 = ( tmp_compare_left_1 == tmp_compare_right_1 );
    if ( tmp_is_1 )
    {
        goto branch_yes_1;
    }
    else
    {
        goto branch_no_1;
    }
    branch_yes_1:;
    tmp_assign_source_1 = PyList_New( 0 );
    {
        PyObject *old = par_allowed;
        par_allowed = tmp_assign_source_1;
        Py_XDECREF( old );
    }

    branch_no_1:;
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_2ca6a45202a14a8e4efcd82e44936641, codeobj_2ca6a45202a14a8e4efcd82e44936641, module_pandas$compat$numpy$function, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_2ca6a45202a14a8e4efcd82e44936641 = cache_frame_2ca6a45202a14a8e4efcd82e44936641;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_2ca6a45202a14a8e4efcd82e44936641 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_2ca6a45202a14a8e4efcd82e44936641 ) == 2 ); // Frame stack

    // Framed code:
    tmp_set_arg_1 = par_kwargs;

    if ( tmp_set_arg_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "kwargs" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 336;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }

    tmp_left_name_1 = PySet_New( tmp_set_arg_1 );
    if ( tmp_left_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 336;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    tmp_set_arg_2 = par_allowed;

    if ( tmp_set_arg_2 == NULL )
    {
        Py_DECREF( tmp_left_name_1 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "allowed" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 336;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }

    tmp_right_name_1 = PySet_New( tmp_set_arg_2 );
    if ( tmp_right_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_left_name_1 );

        exception_lineno = 336;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    tmp_assign_source_2 = BINARY_OPERATION_SUB( tmp_left_name_1, tmp_right_name_1 );
    Py_DECREF( tmp_left_name_1 );
    Py_DECREF( tmp_right_name_1 );
    if ( tmp_assign_source_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 336;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    {
        PyObject *old = par_kwargs;
        par_kwargs = tmp_assign_source_2;
        Py_XDECREF( old );
    }

    tmp_len_arg_1 = par_args;

    if ( tmp_len_arg_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "args" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 338;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }

    tmp_left_name_2 = BUILTIN_LEN( tmp_len_arg_1 );
    if ( tmp_left_name_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 338;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    tmp_len_arg_2 = par_kwargs;

    if ( tmp_len_arg_2 == NULL )
    {
        Py_DECREF( tmp_left_name_2 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "kwargs" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 338;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }

    tmp_right_name_2 = BUILTIN_LEN( tmp_len_arg_2 );
    if ( tmp_right_name_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_left_name_2 );

        exception_lineno = 338;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    tmp_compare_left_2 = BINARY_OPERATION_ADD( tmp_left_name_2, tmp_right_name_2 );
    Py_DECREF( tmp_left_name_2 );
    Py_DECREF( tmp_right_name_2 );
    if ( tmp_compare_left_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 338;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    tmp_compare_right_2 = const_int_0;
    tmp_cmp_Gt_1 = RICH_COMPARE_BOOL_GT( tmp_compare_left_2, tmp_compare_right_2 );
    Py_DECREF( tmp_compare_left_2 );
    if ( tmp_cmp_Gt_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 338;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    if ( tmp_cmp_Gt_1 == 1 )
    {
        goto branch_yes_2;
    }
    else
    {
        goto branch_no_2;
    }
    branch_yes_2:;
    tmp_called_name_1 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_UnsupportedFunctionCall );

    if (unlikely( tmp_called_name_1 == NULL ))
    {
        tmp_called_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_UnsupportedFunctionCall );
    }

    if ( tmp_called_name_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "UnsupportedFunctionCall" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 339;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }

    tmp_source_name_1 = const_str_digest_a5bcc26525e3496b3ca041e208c285cd;
    tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_format );
    assert( !(tmp_called_name_2 == NULL) );
    tmp_dict_key_1 = const_str_plain_func;
    tmp_dict_value_1 = par_name;

    if ( tmp_dict_value_1 == NULL )
    {
        Py_DECREF( tmp_called_name_2 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "name" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 342;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }

    tmp_kw_name_1 = _PyDict_NewPresized( 1 );
    tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
    assert( !(tmp_res != 0) );
    frame_2ca6a45202a14a8e4efcd82e44936641->m_frame.f_lineno = 340;
    tmp_args_element_name_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_2, tmp_kw_name_1 );
    Py_DECREF( tmp_called_name_2 );
    Py_DECREF( tmp_kw_name_1 );
    if ( tmp_args_element_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 340;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    frame_2ca6a45202a14a8e4efcd82e44936641->m_frame.f_lineno = 339;
    {
        PyObject *call_args[] = { tmp_args_element_name_1 };
        tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
    }

    Py_DECREF( tmp_args_element_name_1 );
    if ( tmp_raise_type_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 339;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    exception_type = tmp_raise_type_1;
    exception_lineno = 339;
    RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
    type_description_1 = "oooo";
    goto frame_exception_exit_1;
    branch_no_2:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2ca6a45202a14a8e4efcd82e44936641 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2ca6a45202a14a8e4efcd82e44936641 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_2ca6a45202a14a8e4efcd82e44936641, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_2ca6a45202a14a8e4efcd82e44936641->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_2ca6a45202a14a8e4efcd82e44936641, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_2ca6a45202a14a8e4efcd82e44936641,
        type_description_1,
        par_name,
        par_args,
        par_kwargs,
        par_allowed
    );


    // Release cached frame.
    if ( frame_2ca6a45202a14a8e4efcd82e44936641 == cache_frame_2ca6a45202a14a8e4efcd82e44936641 )
    {
        Py_DECREF( frame_2ca6a45202a14a8e4efcd82e44936641 );
    }
    cache_frame_2ca6a45202a14a8e4efcd82e44936641 = NULL;

    assertFrameObject( frame_2ca6a45202a14a8e4efcd82e44936641 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$compat$numpy$function$$$function_14_validate_groupby_func );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_name );
    par_name = NULL;

    Py_XDECREF( par_args );
    par_args = NULL;

    Py_XDECREF( par_kwargs );
    par_kwargs = NULL;

    Py_XDECREF( par_allowed );
    par_allowed = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_name );
    par_name = NULL;

    Py_XDECREF( par_args );
    par_args = NULL;

    Py_XDECREF( par_kwargs );
    par_kwargs = NULL;

    Py_XDECREF( par_allowed );
    par_allowed = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$compat$numpy$function$$$function_14_validate_groupby_func );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$compat$numpy$function$$$function_15_validate_resampler_func( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_method = python_pars[ 0 ];
    PyObject *par_args = python_pars[ 1 ];
    PyObject *par_kwargs = python_pars[ 2 ];
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_args_element_name_1;
    PyObject *tmp_called_name_1;
    PyObject *tmp_called_name_2;
    int tmp_cmp_Gt_1;
    int tmp_cmp_In_1;
    PyObject *tmp_compare_left_1;
    PyObject *tmp_compare_left_2;
    PyObject *tmp_compare_right_1;
    PyObject *tmp_compare_right_2;
    PyObject *tmp_dict_key_1;
    PyObject *tmp_dict_value_1;
    PyObject *tmp_kw_name_1;
    PyObject *tmp_left_name_1;
    PyObject *tmp_len_arg_1;
    PyObject *tmp_len_arg_2;
    PyObject *tmp_make_exception_arg_1;
    PyObject *tmp_raise_type_1;
    PyObject *tmp_raise_type_2;
    int tmp_res;
    PyObject *tmp_return_value;
    PyObject *tmp_right_name_1;
    PyObject *tmp_source_name_1;
    static struct Nuitka_FrameObject *cache_frame_decc215cc11da815a9438d30e9a73c92 = NULL;

    struct Nuitka_FrameObject *frame_decc215cc11da815a9438d30e9a73c92;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_decc215cc11da815a9438d30e9a73c92, codeobj_decc215cc11da815a9438d30e9a73c92, module_pandas$compat$numpy$function, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_decc215cc11da815a9438d30e9a73c92 = cache_frame_decc215cc11da815a9438d30e9a73c92;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_decc215cc11da815a9438d30e9a73c92 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_decc215cc11da815a9438d30e9a73c92 ) == 2 ); // Frame stack

    // Framed code:
    tmp_len_arg_1 = par_args;

    CHECK_OBJECT( tmp_len_arg_1 );
    tmp_left_name_1 = BUILTIN_LEN( tmp_len_arg_1 );
    if ( tmp_left_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 355;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    tmp_len_arg_2 = par_kwargs;

    if ( tmp_len_arg_2 == NULL )
    {
        Py_DECREF( tmp_left_name_1 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "kwargs" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 355;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    tmp_right_name_1 = BUILTIN_LEN( tmp_len_arg_2 );
    if ( tmp_right_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_left_name_1 );

        exception_lineno = 355;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    tmp_compare_left_1 = BINARY_OPERATION_ADD( tmp_left_name_1, tmp_right_name_1 );
    Py_DECREF( tmp_left_name_1 );
    Py_DECREF( tmp_right_name_1 );
    if ( tmp_compare_left_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 355;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    tmp_compare_right_1 = const_int_0;
    tmp_cmp_Gt_1 = RICH_COMPARE_BOOL_GT( tmp_compare_left_1, tmp_compare_right_1 );
    Py_DECREF( tmp_compare_left_1 );
    if ( tmp_cmp_Gt_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 355;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    if ( tmp_cmp_Gt_1 == 1 )
    {
        goto branch_yes_1;
    }
    else
    {
        goto branch_no_1;
    }
    branch_yes_1:;
    tmp_compare_left_2 = par_method;

    if ( tmp_compare_left_2 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "method" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 356;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    tmp_compare_right_2 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_RESAMPLER_NUMPY_OPS );

    if (unlikely( tmp_compare_right_2 == NULL ))
    {
        tmp_compare_right_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_RESAMPLER_NUMPY_OPS );
    }

    if ( tmp_compare_right_2 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "RESAMPLER_NUMPY_OPS" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 356;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    tmp_cmp_In_1 = PySequence_Contains( tmp_compare_right_2, tmp_compare_left_2 );
    assert( !(tmp_cmp_In_1 == -1) );
    if ( tmp_cmp_In_1 == 1 )
    {
        goto branch_yes_2;
    }
    else
    {
        goto branch_no_2;
    }
    branch_yes_2:;
    tmp_called_name_1 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_UnsupportedFunctionCall );

    if (unlikely( tmp_called_name_1 == NULL ))
    {
        tmp_called_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_UnsupportedFunctionCall );
    }

    if ( tmp_called_name_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "UnsupportedFunctionCall" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 357;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    tmp_source_name_1 = const_str_digest_067df85a277f9fc27f09c3c3db457fd4;
    tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_format );
    assert( !(tmp_called_name_2 == NULL) );
    tmp_dict_key_1 = const_str_plain_func;
    tmp_dict_value_1 = par_method;

    if ( tmp_dict_value_1 == NULL )
    {
        Py_DECREF( tmp_called_name_2 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "method" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 360;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    tmp_kw_name_1 = _PyDict_NewPresized( 1 );
    tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
    assert( !(tmp_res != 0) );
    frame_decc215cc11da815a9438d30e9a73c92->m_frame.f_lineno = 358;
    tmp_args_element_name_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_2, tmp_kw_name_1 );
    Py_DECREF( tmp_called_name_2 );
    Py_DECREF( tmp_kw_name_1 );
    if ( tmp_args_element_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 358;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    frame_decc215cc11da815a9438d30e9a73c92->m_frame.f_lineno = 357;
    {
        PyObject *call_args[] = { tmp_args_element_name_1 };
        tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
    }

    Py_DECREF( tmp_args_element_name_1 );
    if ( tmp_raise_type_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 357;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    exception_type = tmp_raise_type_1;
    exception_lineno = 357;
    RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
    type_description_1 = "ooo";
    goto frame_exception_exit_1;
    goto branch_end_2;
    branch_no_2:;
    tmp_make_exception_arg_1 = const_str_digest_a56b470b9b98d99ed72588f4e40af1cd;
    frame_decc215cc11da815a9438d30e9a73c92->m_frame.f_lineno = 362;
    {
        PyObject *call_args[] = { tmp_make_exception_arg_1 };
        tmp_raise_type_2 = CALL_FUNCTION_WITH_ARGS1( PyExc_TypeError, call_args );
    }

    assert( !(tmp_raise_type_2 == NULL) );
    exception_type = tmp_raise_type_2;
    exception_lineno = 362;
    RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
    type_description_1 = "ooo";
    goto frame_exception_exit_1;
    branch_end_2:;
    branch_no_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_decc215cc11da815a9438d30e9a73c92 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_decc215cc11da815a9438d30e9a73c92 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_decc215cc11da815a9438d30e9a73c92, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_decc215cc11da815a9438d30e9a73c92->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_decc215cc11da815a9438d30e9a73c92, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_decc215cc11da815a9438d30e9a73c92,
        type_description_1,
        par_method,
        par_args,
        par_kwargs
    );


    // Release cached frame.
    if ( frame_decc215cc11da815a9438d30e9a73c92 == cache_frame_decc215cc11da815a9438d30e9a73c92 )
    {
        Py_DECREF( frame_decc215cc11da815a9438d30e9a73c92 );
    }
    cache_frame_decc215cc11da815a9438d30e9a73c92 = NULL;

    assertFrameObject( frame_decc215cc11da815a9438d30e9a73c92 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$compat$numpy$function$$$function_15_validate_resampler_func );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_method );
    par_method = NULL;

    Py_XDECREF( par_args );
    par_args = NULL;

    Py_XDECREF( par_kwargs );
    par_kwargs = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_method );
    par_method = NULL;

    Py_XDECREF( par_args );
    par_args = NULL;

    Py_XDECREF( par_kwargs );
    par_kwargs = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$compat$numpy$function$$$function_15_validate_resampler_func );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}



static PyObject *MAKE_FUNCTION_pandas$compat$numpy$function$$$function_10_validate_transpose_for_generic(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$compat$numpy$function$$$function_10_validate_transpose_for_generic,
        const_str_plain_validate_transpose_for_generic,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_664a2c3bc29db525ef3481f05ac9f980,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$compat$numpy$function,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$compat$numpy$function$$$function_11_validate_window_func(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$compat$numpy$function$$$function_11_validate_window_func,
        const_str_plain_validate_window_func,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_e5daa672e9453708a21f3999fd18e914,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$compat$numpy$function,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$compat$numpy$function$$$function_12_validate_rolling_func(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$compat$numpy$function$$$function_12_validate_rolling_func,
        const_str_plain_validate_rolling_func,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_8dc1d850a1d11f56ce7e376c4bba1267,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$compat$numpy$function,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$compat$numpy$function$$$function_13_validate_expanding_func(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$compat$numpy$function$$$function_13_validate_expanding_func,
        const_str_plain_validate_expanding_func,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_220060a11a280eed1c6dad14776295d7,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$compat$numpy$function,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$compat$numpy$function$$$function_14_validate_groupby_func( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$compat$numpy$function$$$function_14_validate_groupby_func,
        const_str_plain_validate_groupby_func,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_2ca6a45202a14a8e4efcd82e44936641,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$compat$numpy$function,
        const_str_digest_dad771f51f9b65c9900377c5cf931800,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$compat$numpy$function$$$function_15_validate_resampler_func(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$compat$numpy$function$$$function_15_validate_resampler_func,
        const_str_plain_validate_resampler_func,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_decc215cc11da815a9438d30e9a73c92,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$compat$numpy$function,
        const_str_digest_4767b4d940301b42fbdf45ad230ccb88,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$compat$numpy$function$$$function_1___init__( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$compat$numpy$function$$$function_1___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_3d350ed25da3f646ef1c56f94de8231e,
#endif
        codeobj_4a9bd1a3a08fe4f911ad739be813d1a0,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$compat$numpy$function,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$compat$numpy$function$$$function_2___call__( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$compat$numpy$function$$$function_2___call__,
        const_str_plain___call__,
#if PYTHON_VERSION >= 300
        const_str_digest_cd109c7512b0f864597bcb41344e500c,
#endif
        codeobj_1e3f0419c049747f6785cc3c7311ed4c,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$compat$numpy$function,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$compat$numpy$function$$$function_3_process_skipna(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$compat$numpy$function$$$function_3_process_skipna,
        const_str_plain_process_skipna,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_43e94ecae646e3d8f1f3ca9a745e993b,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$compat$numpy$function,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$compat$numpy$function$$$function_4_validate_argmin_with_skipna(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$compat$numpy$function$$$function_4_validate_argmin_with_skipna,
        const_str_plain_validate_argmin_with_skipna,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_cb583cf8b472c057f6e631c41433adb2,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$compat$numpy$function,
        const_str_digest_36060ead743ae11388eade50f3791d13,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$compat$numpy$function$$$function_5_validate_argmax_with_skipna(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$compat$numpy$function$$$function_5_validate_argmax_with_skipna,
        const_str_plain_validate_argmax_with_skipna,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_713f7fe3df7bd6648893df8dc083acaa,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$compat$numpy$function,
        const_str_digest_c8e3eac5104b86668a7b33f7d7a11742,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$compat$numpy$function$$$function_6_validate_argsort_with_ascending(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$compat$numpy$function$$$function_6_validate_argsort_with_ascending,
        const_str_plain_validate_argsort_with_ascending,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_148a360f13c6662190bfd9d000600ba7,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$compat$numpy$function,
        const_str_digest_321b3b2796ea540087d1662294f37838,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$compat$numpy$function$$$function_7_validate_clip_with_axis(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$compat$numpy$function$$$function_7_validate_clip_with_axis,
        const_str_plain_validate_clip_with_axis,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_a66b33f8faf355add77e1dae7c2a077e,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$compat$numpy$function,
        const_str_digest_9314a832e59ceb71d8a4ce93c01512b9,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$compat$numpy$function$$$function_8_validate_cum_func_with_skipna(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$compat$numpy$function$$$function_8_validate_cum_func_with_skipna,
        const_str_plain_validate_cum_func_with_skipna,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_2f5bab5df7791de2e0bdef305e996143,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$compat$numpy$function,
        const_str_digest_931d3394095777aefbb643ab3ba3e281,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$compat$numpy$function$$$function_9_validate_take_with_convert(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$compat$numpy$function$$$function_9_validate_take_with_convert,
        const_str_plain_validate_take_with_convert,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_a00567b750d5f828b6f606f204c47375,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$compat$numpy$function,
        const_str_digest_e08b81464df8dbfa4d80e287ee8a432d,
        0
    );


    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_pandas$compat$numpy$function =
{
    PyModuleDef_HEAD_INIT,
    "pandas.compat.numpy.function",   /* m_name */
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;

extern PyObject *const_str_plain___loader__;
extern PyObject *metapath_based_loader;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL.

MOD_INIT_DECL( pandas$compat$numpy$function )
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_pandas$compat$numpy$function );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("pandas.compat.numpy.function: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("pandas.compat.numpy.function: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("pandas.compat.numpy.function: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initpandas$compat$numpy$function" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_pandas$compat$numpy$function = Py_InitModule4(
        "pandas.compat.numpy.function",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_pandas$compat$numpy$function = PyModule_Create( &mdef_pandas$compat$numpy$function );
#endif

    moduledict_pandas$compat$numpy$function = MODULE_DICT( module_pandas$compat$numpy$function );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_pandas$compat$numpy$function,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );

#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_pandas$compat$numpy$function,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_pandas$compat$numpy$function,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_pandas$compat$numpy$function );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PySys_GetObject( (char *)"modules" ), const_str_digest_3ad2fbf7f8aba849ac3a7ee1a5a2b338, module_pandas$compat$numpy$function );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain___loader__, metapath_based_loader );
#endif

#if PYTHON_VERSION >= 340
#if 0
    UPDATE_STRING_DICT0( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain___name__ ),
            metapath_based_loader
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );

        UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );

        Py_DECREF( module_spec_class );
    }
#endif
#endif


    // Temp variables if any
    PyObject *outline_0_var___class__ = NULL;
    PyObject *tmp_class_creation_1__class_decl_dict = NULL;
    PyObject *tmp_class_creation_1__metaclass = NULL;
    PyObject *tmp_class_creation_1__prepared = NULL;
    PyObject *tmp_import_from_1__module = NULL;
    PyObject *tmp_import_from_2__module = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *tmp_args_name_1;
    PyObject *tmp_args_name_2;
    PyObject *tmp_args_name_3;
    PyObject *tmp_args_name_4;
    PyObject *tmp_args_name_5;
    PyObject *tmp_args_name_6;
    PyObject *tmp_args_name_7;
    PyObject *tmp_args_name_8;
    PyObject *tmp_args_name_9;
    PyObject *tmp_args_name_10;
    PyObject *tmp_args_name_11;
    PyObject *tmp_args_name_12;
    PyObject *tmp_args_name_13;
    PyObject *tmp_args_name_14;
    PyObject *tmp_args_name_15;
    PyObject *tmp_args_name_16;
    PyObject *tmp_args_name_17;
    PyObject *tmp_args_name_18;
    PyObject *tmp_args_name_19;
    PyObject *tmp_args_name_20;
    PyObject *tmp_args_name_21;
    PyObject *tmp_args_name_22;
    PyObject *tmp_args_name_23;
    PyObject *tmp_args_name_24;
    PyObject *tmp_args_name_25;
    PyObject *tmp_ass_subscribed_1;
    PyObject *tmp_ass_subscribed_2;
    PyObject *tmp_ass_subscribed_3;
    PyObject *tmp_ass_subscribed_4;
    PyObject *tmp_ass_subscribed_5;
    PyObject *tmp_ass_subscribed_6;
    PyObject *tmp_ass_subscribed_7;
    PyObject *tmp_ass_subscribed_8;
    PyObject *tmp_ass_subscribed_9;
    PyObject *tmp_ass_subscribed_10;
    PyObject *tmp_ass_subscribed_11;
    PyObject *tmp_ass_subscribed_12;
    PyObject *tmp_ass_subscribed_13;
    PyObject *tmp_ass_subscribed_14;
    PyObject *tmp_ass_subscribed_15;
    PyObject *tmp_ass_subscribed_16;
    PyObject *tmp_ass_subscribed_17;
    PyObject *tmp_ass_subscribed_18;
    PyObject *tmp_ass_subscribed_19;
    PyObject *tmp_ass_subscribed_20;
    PyObject *tmp_ass_subscript_1;
    PyObject *tmp_ass_subscript_2;
    PyObject *tmp_ass_subscript_3;
    PyObject *tmp_ass_subscript_4;
    PyObject *tmp_ass_subscript_5;
    PyObject *tmp_ass_subscript_6;
    PyObject *tmp_ass_subscript_7;
    PyObject *tmp_ass_subscript_8;
    PyObject *tmp_ass_subscript_9;
    PyObject *tmp_ass_subscript_10;
    PyObject *tmp_ass_subscript_11;
    PyObject *tmp_ass_subscript_12;
    PyObject *tmp_ass_subscript_13;
    PyObject *tmp_ass_subscript_14;
    PyObject *tmp_ass_subscript_15;
    PyObject *tmp_ass_subscript_16;
    PyObject *tmp_ass_subscript_17;
    PyObject *tmp_ass_subscript_18;
    PyObject *tmp_ass_subscript_19;
    PyObject *tmp_ass_subscript_20;
    PyObject *tmp_ass_subvalue_1;
    PyObject *tmp_ass_subvalue_2;
    PyObject *tmp_ass_subvalue_3;
    PyObject *tmp_ass_subvalue_4;
    PyObject *tmp_ass_subvalue_5;
    PyObject *tmp_ass_subvalue_6;
    PyObject *tmp_ass_subvalue_7;
    PyObject *tmp_ass_subvalue_8;
    PyObject *tmp_ass_subvalue_9;
    PyObject *tmp_ass_subvalue_10;
    PyObject *tmp_ass_subvalue_11;
    PyObject *tmp_ass_subvalue_12;
    PyObject *tmp_ass_subvalue_13;
    PyObject *tmp_ass_subvalue_14;
    PyObject *tmp_ass_subvalue_15;
    PyObject *tmp_ass_subvalue_16;
    PyObject *tmp_ass_subvalue_17;
    PyObject *tmp_ass_subvalue_18;
    PyObject *tmp_ass_subvalue_19;
    PyObject *tmp_ass_subvalue_20;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_assign_source_5;
    PyObject *tmp_assign_source_6;
    PyObject *tmp_assign_source_7;
    PyObject *tmp_assign_source_8;
    PyObject *tmp_assign_source_9;
    PyObject *tmp_assign_source_10;
    PyObject *tmp_assign_source_11;
    PyObject *tmp_assign_source_12;
    PyObject *tmp_assign_source_13;
    PyObject *tmp_assign_source_14;
    PyObject *tmp_assign_source_15;
    PyObject *tmp_assign_source_16;
    PyObject *tmp_assign_source_17;
    PyObject *tmp_assign_source_18;
    PyObject *tmp_assign_source_19;
    PyObject *tmp_assign_source_20;
    PyObject *tmp_assign_source_21;
    PyObject *tmp_assign_source_22;
    PyObject *tmp_assign_source_23;
    PyObject *tmp_assign_source_24;
    PyObject *tmp_assign_source_25;
    PyObject *tmp_assign_source_26;
    PyObject *tmp_assign_source_27;
    PyObject *tmp_assign_source_28;
    PyObject *tmp_assign_source_29;
    PyObject *tmp_assign_source_30;
    PyObject *tmp_assign_source_31;
    PyObject *tmp_assign_source_32;
    PyObject *tmp_assign_source_33;
    PyObject *tmp_assign_source_34;
    PyObject *tmp_assign_source_35;
    PyObject *tmp_assign_source_36;
    PyObject *tmp_assign_source_37;
    PyObject *tmp_assign_source_38;
    PyObject *tmp_assign_source_39;
    PyObject *tmp_assign_source_40;
    PyObject *tmp_assign_source_41;
    PyObject *tmp_assign_source_42;
    PyObject *tmp_assign_source_43;
    PyObject *tmp_assign_source_44;
    PyObject *tmp_assign_source_45;
    PyObject *tmp_assign_source_46;
    PyObject *tmp_assign_source_47;
    PyObject *tmp_assign_source_48;
    PyObject *tmp_assign_source_49;
    PyObject *tmp_assign_source_50;
    PyObject *tmp_assign_source_51;
    PyObject *tmp_assign_source_52;
    PyObject *tmp_assign_source_53;
    PyObject *tmp_assign_source_54;
    PyObject *tmp_assign_source_55;
    PyObject *tmp_assign_source_56;
    PyObject *tmp_assign_source_57;
    PyObject *tmp_assign_source_58;
    PyObject *tmp_assign_source_59;
    PyObject *tmp_assign_source_60;
    PyObject *tmp_assign_source_61;
    PyObject *tmp_assign_source_62;
    PyObject *tmp_assign_source_63;
    PyObject *tmp_assign_source_64;
    PyObject *tmp_assign_source_65;
    PyObject *tmp_assign_source_66;
    PyObject *tmp_assign_source_67;
    PyObject *tmp_assign_source_68;
    PyObject *tmp_assign_source_69;
    PyObject *tmp_assign_source_70;
    PyObject *tmp_assign_source_71;
    PyObject *tmp_assign_source_72;
    PyObject *tmp_bases_name_1;
    PyObject *tmp_called_name_1;
    PyObject *tmp_called_name_2;
    PyObject *tmp_called_name_3;
    PyObject *tmp_called_name_4;
    PyObject *tmp_called_name_5;
    PyObject *tmp_called_name_6;
    PyObject *tmp_called_name_7;
    PyObject *tmp_called_name_8;
    PyObject *tmp_called_name_9;
    PyObject *tmp_called_name_10;
    PyObject *tmp_called_name_11;
    PyObject *tmp_called_name_12;
    PyObject *tmp_called_name_13;
    PyObject *tmp_called_name_14;
    PyObject *tmp_called_name_15;
    PyObject *tmp_called_name_16;
    PyObject *tmp_called_name_17;
    PyObject *tmp_called_name_18;
    PyObject *tmp_called_name_19;
    PyObject *tmp_called_name_20;
    PyObject *tmp_called_name_21;
    PyObject *tmp_called_name_22;
    PyObject *tmp_called_name_23;
    PyObject *tmp_called_name_24;
    PyObject *tmp_called_name_25;
    PyObject *tmp_called_name_26;
    PyObject *tmp_called_name_27;
    PyObject *tmp_called_name_28;
    PyObject *tmp_called_name_29;
    PyObject *tmp_called_name_30;
    PyObject *tmp_called_name_31;
    PyObject *tmp_called_name_32;
    PyObject *tmp_called_name_33;
    PyObject *tmp_called_name_34;
    int tmp_cond_truth_1;
    int tmp_cond_truth_2;
    PyObject *tmp_cond_value_1;
    PyObject *tmp_cond_value_2;
    PyObject *tmp_defaults_1;
    PyObject *tmp_defaults_2;
    PyObject *tmp_defaults_3;
    PyObject *tmp_dict_name_1;
    PyObject *tmp_dict_name_2;
    PyObject *tmp_dict_name_3;
    PyObject *tmp_dictdel_dict;
    PyObject *tmp_dictdel_key;
    PyObject *tmp_dictset_value;
    PyObject *tmp_fromlist_name_1;
    PyObject *tmp_fromlist_name_2;
    PyObject *tmp_fromlist_name_3;
    PyObject *tmp_fromlist_name_4;
    PyObject *tmp_fromlist_name_5;
    PyObject *tmp_globals_name_1;
    PyObject *tmp_globals_name_2;
    PyObject *tmp_globals_name_3;
    PyObject *tmp_globals_name_4;
    PyObject *tmp_globals_name_5;
    PyObject *tmp_hasattr_attr_1;
    PyObject *tmp_hasattr_source_1;
    PyObject *tmp_import_name_from_1;
    PyObject *tmp_import_name_from_2;
    PyObject *tmp_import_name_from_3;
    PyObject *tmp_import_name_from_4;
    PyObject *tmp_import_name_from_5;
    PyObject *tmp_import_name_from_6;
    PyObject *tmp_import_name_from_7;
    PyObject *tmp_import_name_from_8;
    PyObject *tmp_key_name_1;
    PyObject *tmp_key_name_2;
    PyObject *tmp_key_name_3;
    PyObject *tmp_kw_name_1;
    PyObject *tmp_kw_name_2;
    PyObject *tmp_kw_name_3;
    PyObject *tmp_kw_name_4;
    PyObject *tmp_kw_name_5;
    PyObject *tmp_kw_name_6;
    PyObject *tmp_kw_name_7;
    PyObject *tmp_kw_name_8;
    PyObject *tmp_kw_name_9;
    PyObject *tmp_kw_name_10;
    PyObject *tmp_kw_name_11;
    PyObject *tmp_kw_name_12;
    PyObject *tmp_kw_name_13;
    PyObject *tmp_kw_name_14;
    PyObject *tmp_kw_name_15;
    PyObject *tmp_kw_name_16;
    PyObject *tmp_kw_name_17;
    PyObject *tmp_kw_name_18;
    PyObject *tmp_kw_name_19;
    PyObject *tmp_kw_name_20;
    PyObject *tmp_kw_name_21;
    PyObject *tmp_kw_name_22;
    PyObject *tmp_kw_name_23;
    PyObject *tmp_kw_name_24;
    PyObject *tmp_kw_name_25;
    PyObject *tmp_level_name_1;
    PyObject *tmp_level_name_2;
    PyObject *tmp_level_name_3;
    PyObject *tmp_level_name_4;
    PyObject *tmp_level_name_5;
    PyObject *tmp_locals_name_1;
    PyObject *tmp_locals_name_2;
    PyObject *tmp_locals_name_3;
    PyObject *tmp_locals_name_4;
    PyObject *tmp_locals_name_5;
    PyObject *tmp_metaclass_name_1;
    PyObject *tmp_name_name_1;
    PyObject *tmp_name_name_2;
    PyObject *tmp_name_name_3;
    PyObject *tmp_name_name_4;
    PyObject *tmp_name_name_5;
    PyObject *tmp_outline_return_value_1;
    int tmp_res;
    bool tmp_result;
    PyObject *tmp_set_locals;
    PyObject *tmp_source_name_1;
    PyObject *tmp_tuple_element_1;
    PyObject *tmp_tuple_element_2;
    PyObject *tmp_tuple_element_3;
    PyObject *tmp_tuple_element_4;
    PyObject *tmp_tuple_element_5;
    PyObject *tmp_tuple_element_6;
    PyObject *tmp_tuple_element_7;
    PyObject *tmp_tuple_element_8;
    PyObject *tmp_tuple_element_9;
    PyObject *tmp_tuple_element_10;
    PyObject *tmp_tuple_element_11;
    PyObject *tmp_tuple_element_12;
    PyObject *tmp_tuple_element_13;
    PyObject *tmp_tuple_element_14;
    PyObject *tmp_tuple_element_15;
    PyObject *tmp_tuple_element_16;
    PyObject *tmp_tuple_element_17;
    PyObject *tmp_tuple_element_18;
    PyObject *tmp_tuple_element_19;
    PyObject *tmp_tuple_element_20;
    PyObject *tmp_tuple_element_21;
    PyObject *tmp_tuple_element_22;
    PyObject *tmp_tuple_element_23;
    PyObject *tmp_tuple_element_24;
    static struct Nuitka_FrameObject *cache_frame_832ad82c1fd8630f5016f41edc471736_2 = NULL;

    struct Nuitka_FrameObject *frame_832ad82c1fd8630f5016f41edc471736_2;

    struct Nuitka_FrameObject *frame_8cc68a7a34d1d402f731b2d00596f1d3;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    tmp_outline_return_value_1 = NULL;
    PyObject *locals_pandas$compat$numpy$function_29 = NULL;

    // Module code.
    tmp_assign_source_1 = const_str_digest_656d217b03b241a110b4d9aafbf941cf;
    UPDATE_STRING_DICT0( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    tmp_assign_source_2 = const_str_digest_95206160780955a825d9d6c41730597d;
    UPDATE_STRING_DICT0( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    tmp_assign_source_3 = Py_None;
    UPDATE_STRING_DICT0( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    // Frame without reuse.
    frame_8cc68a7a34d1d402f731b2d00596f1d3 = MAKE_MODULE_FRAME( codeobj_8cc68a7a34d1d402f731b2d00596f1d3, module_pandas$compat$numpy$function );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_8cc68a7a34d1d402f731b2d00596f1d3 );
    assert( Py_REFCNT( frame_8cc68a7a34d1d402f731b2d00596f1d3 ) == 2 );

    // Framed code:
    tmp_name_name_1 = const_str_plain_numpy;
    tmp_globals_name_1 = (PyObject *)moduledict_pandas$compat$numpy$function;
    tmp_locals_name_1 = Py_None;
    tmp_fromlist_name_1 = const_tuple_str_plain_ndarray_tuple;
    tmp_level_name_1 = const_int_0;
    frame_8cc68a7a34d1d402f731b2d00596f1d3->m_frame.f_lineno = 21;
    tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
    if ( tmp_import_name_from_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 21;

        goto frame_exception_exit_1;
    }
    tmp_assign_source_4 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_ndarray );
    Py_DECREF( tmp_import_name_from_1 );
    if ( tmp_assign_source_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 21;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_ndarray, tmp_assign_source_4 );
    tmp_name_name_2 = const_str_digest_af281489cd70cb7b8ea2f52bbb2dbaa6;
    tmp_globals_name_2 = (PyObject *)moduledict_pandas$compat$numpy$function;
    tmp_locals_name_2 = Py_None;
    tmp_fromlist_name_2 = const_tuple_f07bd4ec6bd6a9403a12e46ed9eeeaaa_tuple;
    tmp_level_name_2 = const_int_0;
    frame_8cc68a7a34d1d402f731b2d00596f1d3->m_frame.f_lineno = 22;
    tmp_assign_source_5 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
    if ( tmp_assign_source_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 22;

        goto frame_exception_exit_1;
    }
    assert( tmp_import_from_1__module == NULL );
    tmp_import_from_1__module = tmp_assign_source_5;

    // Tried code:
    tmp_import_name_from_2 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_2 );
    tmp_assign_source_6 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_validate_args );
    if ( tmp_assign_source_6 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 22;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_validate_args, tmp_assign_source_6 );
    tmp_import_name_from_3 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_3 );
    tmp_assign_source_7 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_validate_kwargs );
    if ( tmp_assign_source_7 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 22;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_validate_kwargs, tmp_assign_source_7 );
    tmp_import_name_from_4 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_4 );
    tmp_assign_source_8 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_validate_args_and_kwargs );
    if ( tmp_assign_source_8 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 22;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_validate_args_and_kwargs, tmp_assign_source_8 );
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    tmp_name_name_3 = const_str_digest_78a24cb289d0bfe07c0851ef80e47803;
    tmp_globals_name_3 = (PyObject *)moduledict_pandas$compat$numpy$function;
    tmp_locals_name_3 = Py_None;
    tmp_fromlist_name_3 = const_tuple_str_plain_UnsupportedFunctionCall_tuple;
    tmp_level_name_3 = const_int_0;
    frame_8cc68a7a34d1d402f731b2d00596f1d3->m_frame.f_lineno = 24;
    tmp_import_name_from_5 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
    if ( tmp_import_name_from_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 24;

        goto frame_exception_exit_1;
    }
    tmp_assign_source_9 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_UnsupportedFunctionCall );
    Py_DECREF( tmp_import_name_from_5 );
    if ( tmp_assign_source_9 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 24;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_UnsupportedFunctionCall, tmp_assign_source_9 );
    tmp_name_name_4 = const_str_digest_69a010023c9fca5d0ce87758e225ccf2;
    tmp_globals_name_4 = (PyObject *)moduledict_pandas$compat$numpy$function;
    tmp_locals_name_4 = Py_None;
    tmp_fromlist_name_4 = const_tuple_str_plain_is_integer_str_plain_is_bool_tuple;
    tmp_level_name_4 = const_int_0;
    frame_8cc68a7a34d1d402f731b2d00596f1d3->m_frame.f_lineno = 25;
    tmp_assign_source_10 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
    if ( tmp_assign_source_10 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 25;

        goto frame_exception_exit_1;
    }
    assert( tmp_import_from_2__module == NULL );
    tmp_import_from_2__module = tmp_assign_source_10;

    // Tried code:
    tmp_import_name_from_6 = tmp_import_from_2__module;

    CHECK_OBJECT( tmp_import_name_from_6 );
    tmp_assign_source_11 = IMPORT_NAME( tmp_import_name_from_6, const_str_plain_is_integer );
    if ( tmp_assign_source_11 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 25;

        goto try_except_handler_2;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_is_integer, tmp_assign_source_11 );
    tmp_import_name_from_7 = tmp_import_from_2__module;

    CHECK_OBJECT( tmp_import_name_from_7 );
    tmp_assign_source_12 = IMPORT_NAME( tmp_import_name_from_7, const_str_plain_is_bool );
    if ( tmp_assign_source_12 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 25;

        goto try_except_handler_2;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_is_bool, tmp_assign_source_12 );
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    tmp_name_name_5 = const_str_digest_77d8fb15a813ae44a7b8d0fe53dbee10;
    tmp_globals_name_5 = (PyObject *)moduledict_pandas$compat$numpy$function;
    tmp_locals_name_5 = Py_None;
    tmp_fromlist_name_5 = const_tuple_str_plain_OrderedDict_tuple;
    tmp_level_name_5 = const_int_0;
    frame_8cc68a7a34d1d402f731b2d00596f1d3->m_frame.f_lineno = 26;
    tmp_import_name_from_8 = IMPORT_MODULE5( tmp_name_name_5, tmp_globals_name_5, tmp_locals_name_5, tmp_fromlist_name_5, tmp_level_name_5 );
    if ( tmp_import_name_from_8 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 26;

        goto frame_exception_exit_1;
    }
    tmp_assign_source_13 = IMPORT_NAME( tmp_import_name_from_8, const_str_plain_OrderedDict );
    Py_DECREF( tmp_import_name_from_8 );
    if ( tmp_assign_source_13 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 26;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_OrderedDict, tmp_assign_source_13 );
    tmp_assign_source_14 = PyDict_New();
    assert( tmp_class_creation_1__class_decl_dict == NULL );
    tmp_class_creation_1__class_decl_dict = tmp_assign_source_14;

    // Tried code:
    tmp_key_name_1 = const_str_plain_metaclass;
    tmp_dict_name_1 = tmp_class_creation_1__class_decl_dict;

    CHECK_OBJECT( tmp_dict_name_1 );
    tmp_res = PyDict_Contains( tmp_dict_name_1, tmp_key_name_1 );
    if ( tmp_res == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 29;

        goto try_except_handler_3;
    }
    tmp_cond_value_1 = BOOL_FROM( tmp_res == 1 );
    tmp_cond_truth_1 = CHECK_IF_TRUE( tmp_cond_value_1 );
    if ( tmp_cond_truth_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 29;

        goto try_except_handler_3;
    }
    if ( tmp_cond_truth_1 == 1 )
    {
        goto condexpr_true_1;
    }
    else
    {
        goto condexpr_false_1;
    }
    condexpr_true_1:;
    tmp_dict_name_2 = tmp_class_creation_1__class_decl_dict;

    CHECK_OBJECT( tmp_dict_name_2 );
    tmp_key_name_2 = const_str_plain_metaclass;
    tmp_metaclass_name_1 = DICT_GET_ITEM( tmp_dict_name_2, tmp_key_name_2 );
    if ( tmp_metaclass_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 29;

        goto try_except_handler_3;
    }
    goto condexpr_end_1;
    condexpr_false_1:;
    tmp_metaclass_name_1 = LOOKUP_BUILTIN( const_str_plain_type );
    assert( tmp_metaclass_name_1 != NULL );
    Py_INCREF( tmp_metaclass_name_1 );
    condexpr_end_1:;
    tmp_bases_name_1 = const_tuple_type_object_tuple;
    tmp_assign_source_15 = SELECT_METACLASS( tmp_metaclass_name_1, tmp_bases_name_1 );
    Py_DECREF( tmp_metaclass_name_1 );
    if ( tmp_assign_source_15 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 29;

        goto try_except_handler_3;
    }
    assert( tmp_class_creation_1__metaclass == NULL );
    tmp_class_creation_1__metaclass = tmp_assign_source_15;

    tmp_key_name_3 = const_str_plain_metaclass;
    tmp_dict_name_3 = tmp_class_creation_1__class_decl_dict;

    CHECK_OBJECT( tmp_dict_name_3 );
    tmp_res = PyDict_Contains( tmp_dict_name_3, tmp_key_name_3 );
    if ( tmp_res == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 29;

        goto try_except_handler_3;
    }
    tmp_cond_value_2 = BOOL_FROM( tmp_res == 1 );
    tmp_cond_truth_2 = CHECK_IF_TRUE( tmp_cond_value_2 );
    if ( tmp_cond_truth_2 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 29;

        goto try_except_handler_3;
    }
    if ( tmp_cond_truth_2 == 1 )
    {
        goto branch_yes_1;
    }
    else
    {
        goto branch_no_1;
    }
    branch_yes_1:;
    tmp_dictdel_dict = tmp_class_creation_1__class_decl_dict;

    CHECK_OBJECT( tmp_dictdel_dict );
    tmp_dictdel_key = const_str_plain_metaclass;
    tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 29;

        goto try_except_handler_3;
    }
    branch_no_1:;
    tmp_hasattr_source_1 = tmp_class_creation_1__metaclass;

    CHECK_OBJECT( tmp_hasattr_source_1 );
    tmp_hasattr_attr_1 = const_str_plain___prepare__;
    tmp_res = PyObject_HasAttr( tmp_hasattr_source_1, tmp_hasattr_attr_1 );
    if ( tmp_res == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 29;

        goto try_except_handler_3;
    }
    if ( tmp_res == 1 )
    {
        goto condexpr_true_2;
    }
    else
    {
        goto condexpr_false_2;
    }
    condexpr_true_2:;
    tmp_source_name_1 = tmp_class_creation_1__metaclass;

    CHECK_OBJECT( tmp_source_name_1 );
    tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___prepare__ );
    if ( tmp_called_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 29;

        goto try_except_handler_3;
    }
    tmp_args_name_1 = const_tuple_str_plain_CompatValidator_tuple_type_object_tuple_tuple;
    tmp_kw_name_1 = tmp_class_creation_1__class_decl_dict;

    CHECK_OBJECT( tmp_kw_name_1 );
    frame_8cc68a7a34d1d402f731b2d00596f1d3->m_frame.f_lineno = 29;
    tmp_assign_source_16 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
    Py_DECREF( tmp_called_name_1 );
    if ( tmp_assign_source_16 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 29;

        goto try_except_handler_3;
    }
    goto condexpr_end_2;
    condexpr_false_2:;
    tmp_assign_source_16 = PyDict_New();
    condexpr_end_2:;
    assert( tmp_class_creation_1__prepared == NULL );
    tmp_class_creation_1__prepared = tmp_assign_source_16;

    tmp_set_locals = tmp_class_creation_1__prepared;

    CHECK_OBJECT( tmp_set_locals );
    locals_pandas$compat$numpy$function_29 = tmp_set_locals;
    Py_INCREF( tmp_set_locals );
    // Tried code:
    // Tried code:
    tmp_dictset_value = const_str_digest_3ad2fbf7f8aba849ac3a7ee1a5a2b338;
    tmp_res = PyObject_SetItem( locals_pandas$compat$numpy$function_29, const_str_plain___module__, tmp_dictset_value );
    if ( tmp_res != 0 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 29;

        goto try_except_handler_5;
    }
    tmp_dictset_value = const_str_plain_CompatValidator;
    tmp_res = PyObject_SetItem( locals_pandas$compat$numpy$function_29, const_str_plain___qualname__, tmp_dictset_value );
    if ( tmp_res != 0 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 29;

        goto try_except_handler_5;
    }
    MAKE_OR_REUSE_FRAME( cache_frame_832ad82c1fd8630f5016f41edc471736_2, codeobj_832ad82c1fd8630f5016f41edc471736, module_pandas$compat$numpy$function, sizeof(void *) );
    frame_832ad82c1fd8630f5016f41edc471736_2 = cache_frame_832ad82c1fd8630f5016f41edc471736_2;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_832ad82c1fd8630f5016f41edc471736_2 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_832ad82c1fd8630f5016f41edc471736_2 ) == 2 ); // Frame stack

    // Framed code:
    tmp_defaults_1 = const_tuple_none_none_none_tuple;
    Py_INCREF( tmp_defaults_1 );
    tmp_dictset_value = MAKE_FUNCTION_pandas$compat$numpy$function$$$function_1___init__( tmp_defaults_1 );
    tmp_res = PyObject_SetItem( locals_pandas$compat$numpy$function_29, const_str_plain___init__, tmp_dictset_value );
    Py_DECREF( tmp_dictset_value );
    if ( tmp_res != 0 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 31;
        type_description_2 = "N";
        goto frame_exception_exit_2;
    }
    tmp_defaults_2 = const_tuple_none_none_none_tuple;
    Py_INCREF( tmp_defaults_2 );
    tmp_dictset_value = MAKE_FUNCTION_pandas$compat$numpy$function$$$function_2___call__( tmp_defaults_2 );
    tmp_res = PyObject_SetItem( locals_pandas$compat$numpy$function_29, const_str_plain___call__, tmp_dictset_value );
    Py_DECREF( tmp_dictset_value );
    if ( tmp_res != 0 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 38;
        type_description_2 = "N";
        goto frame_exception_exit_2;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_832ad82c1fd8630f5016f41edc471736_2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_2:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_832ad82c1fd8630f5016f41edc471736_2 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_832ad82c1fd8630f5016f41edc471736_2, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_832ad82c1fd8630f5016f41edc471736_2->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_832ad82c1fd8630f5016f41edc471736_2, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_832ad82c1fd8630f5016f41edc471736_2,
        type_description_2,
        NULL
    );


    // Release cached frame.
    if ( frame_832ad82c1fd8630f5016f41edc471736_2 == cache_frame_832ad82c1fd8630f5016f41edc471736_2 )
    {
        Py_DECREF( frame_832ad82c1fd8630f5016f41edc471736_2 );
    }
    cache_frame_832ad82c1fd8630f5016f41edc471736_2 = NULL;

    assertFrameObject( frame_832ad82c1fd8630f5016f41edc471736_2 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto nested_frame_exit_1;

    frame_no_exception_1:;

    goto skip_nested_handling_1;
    nested_frame_exit_1:;

    goto try_except_handler_5;
    skip_nested_handling_1:;
    tmp_called_name_2 = tmp_class_creation_1__metaclass;

    CHECK_OBJECT( tmp_called_name_2 );
    tmp_tuple_element_1 = const_str_plain_CompatValidator;
    tmp_args_name_2 = PyTuple_New( 3 );
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_1 );
    tmp_tuple_element_1 = const_tuple_type_object_tuple;
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_1 );
    tmp_tuple_element_1 = locals_pandas$compat$numpy$function_29;
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_args_name_2, 2, tmp_tuple_element_1 );
    tmp_kw_name_2 = tmp_class_creation_1__class_decl_dict;

    CHECK_OBJECT( tmp_kw_name_2 );
    frame_8cc68a7a34d1d402f731b2d00596f1d3->m_frame.f_lineno = 29;
    tmp_assign_source_18 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_2, tmp_kw_name_2 );
    Py_DECREF( tmp_args_name_2 );
    if ( tmp_assign_source_18 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 29;

        goto try_except_handler_5;
    }
    assert( outline_0_var___class__ == NULL );
    outline_0_var___class__ = tmp_assign_source_18;

    tmp_outline_return_value_1 = outline_0_var___class__;

    CHECK_OBJECT( tmp_outline_return_value_1 );
    Py_INCREF( tmp_outline_return_value_1 );
    goto try_return_handler_5;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$compat$numpy$function );
    return MOD_RETURN_VALUE( NULL );
    // Return handler code:
    try_return_handler_5:;
    Py_DECREF( locals_pandas$compat$numpy$function_29 );
    locals_pandas$compat$numpy$function_29 = NULL;
    goto try_return_handler_4;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_DECREF( locals_pandas$compat$numpy$function_29 );
    locals_pandas$compat$numpy$function_29 = NULL;
    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto try_except_handler_4;
    // End of try:
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$compat$numpy$function );
    return MOD_RETURN_VALUE( NULL );
    // Return handler code:
    try_return_handler_4:;
    CHECK_OBJECT( (PyObject *)outline_0_var___class__ );
    Py_DECREF( outline_0_var___class__ );
    outline_0_var___class__ = NULL;

    goto outline_result_1;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto outline_exception_1;
    // End of try:
    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$compat$numpy$function );
    return MOD_RETURN_VALUE( NULL );
    outline_exception_1:;
    exception_lineno = 29;
    goto try_except_handler_3;
    outline_result_1:;
    tmp_assign_source_17 = tmp_outline_return_value_1;
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_CompatValidator, tmp_assign_source_17 );
    goto try_end_3;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_decl_dict );
    Py_DECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_decl_dict );
    Py_DECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__metaclass );
    Py_DECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__prepared );
    Py_DECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    tmp_assign_source_19 = PyDict_Copy( const_dict_2b811f9b7f6bd0285cfb478e33ee30a5 );
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_ARGMINMAX_DEFAULTS, tmp_assign_source_19 );
    tmp_called_name_3 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_CompatValidator );

    if (unlikely( tmp_called_name_3 == NULL ))
    {
        tmp_called_name_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CompatValidator );
    }

    if ( tmp_called_name_3 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CompatValidator" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 61;

        goto frame_exception_exit_1;
    }

    tmp_tuple_element_2 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_ARGMINMAX_DEFAULTS );

    if (unlikely( tmp_tuple_element_2 == NULL ))
    {
        tmp_tuple_element_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ARGMINMAX_DEFAULTS );
    }

    CHECK_OBJECT( tmp_tuple_element_2 );
    tmp_args_name_3 = PyTuple_New( 1 );
    Py_INCREF( tmp_tuple_element_2 );
    PyTuple_SET_ITEM( tmp_args_name_3, 0, tmp_tuple_element_2 );
    tmp_kw_name_3 = PyDict_Copy( const_dict_57cff8749cb120e28dd3527c89e75318 );
    frame_8cc68a7a34d1d402f731b2d00596f1d3->m_frame.f_lineno = 61;
    tmp_assign_source_20 = CALL_FUNCTION( tmp_called_name_3, tmp_args_name_3, tmp_kw_name_3 );
    Py_DECREF( tmp_args_name_3 );
    Py_DECREF( tmp_kw_name_3 );
    if ( tmp_assign_source_20 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 61;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_validate_argmin, tmp_assign_source_20 );
    tmp_called_name_4 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_CompatValidator );

    if (unlikely( tmp_called_name_4 == NULL ))
    {
        tmp_called_name_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CompatValidator );
    }

    if ( tmp_called_name_4 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CompatValidator" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 63;

        goto frame_exception_exit_1;
    }

    tmp_tuple_element_3 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_ARGMINMAX_DEFAULTS );

    if (unlikely( tmp_tuple_element_3 == NULL ))
    {
        tmp_tuple_element_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ARGMINMAX_DEFAULTS );
    }

    if ( tmp_tuple_element_3 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ARGMINMAX_DEFAULTS" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 63;

        goto frame_exception_exit_1;
    }

    tmp_args_name_4 = PyTuple_New( 1 );
    Py_INCREF( tmp_tuple_element_3 );
    PyTuple_SET_ITEM( tmp_args_name_4, 0, tmp_tuple_element_3 );
    tmp_kw_name_4 = PyDict_Copy( const_dict_9f4d59885d17d308693fb57f400e28ec );
    frame_8cc68a7a34d1d402f731b2d00596f1d3->m_frame.f_lineno = 63;
    tmp_assign_source_21 = CALL_FUNCTION( tmp_called_name_4, tmp_args_name_4, tmp_kw_name_4 );
    Py_DECREF( tmp_args_name_4 );
    Py_DECREF( tmp_kw_name_4 );
    if ( tmp_assign_source_21 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 63;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_validate_argmax, tmp_assign_source_21 );
    tmp_assign_source_22 = MAKE_FUNCTION_pandas$compat$numpy$function$$$function_3_process_skipna(  );
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_process_skipna, tmp_assign_source_22 );
    tmp_assign_source_23 = MAKE_FUNCTION_pandas$compat$numpy$function$$$function_4_validate_argmin_with_skipna(  );
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_validate_argmin_with_skipna, tmp_assign_source_23 );
    tmp_assign_source_24 = MAKE_FUNCTION_pandas$compat$numpy$function$$$function_5_validate_argmax_with_skipna(  );
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_validate_argmax_with_skipna, tmp_assign_source_24 );
    tmp_called_name_5 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_OrderedDict );

    if (unlikely( tmp_called_name_5 == NULL ))
    {
        tmp_called_name_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_OrderedDict );
    }

    if ( tmp_called_name_5 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "OrderedDict" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 103;

        goto frame_exception_exit_1;
    }

    frame_8cc68a7a34d1d402f731b2d00596f1d3->m_frame.f_lineno = 103;
    tmp_assign_source_25 = CALL_FUNCTION_NO_ARGS( tmp_called_name_5 );
    if ( tmp_assign_source_25 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 103;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_ARGSORT_DEFAULTS, tmp_assign_source_25 );
    tmp_ass_subvalue_1 = const_int_neg_1;
    tmp_ass_subscribed_1 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_ARGSORT_DEFAULTS );

    if (unlikely( tmp_ass_subscribed_1 == NULL ))
    {
        tmp_ass_subscribed_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ARGSORT_DEFAULTS );
    }

    CHECK_OBJECT( tmp_ass_subscribed_1 );
    tmp_ass_subscript_1 = const_str_plain_axis;
    tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_1, tmp_ass_subscript_1, tmp_ass_subvalue_1 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 104;

        goto frame_exception_exit_1;
    }
    tmp_ass_subvalue_2 = const_str_plain_quicksort;
    tmp_ass_subscribed_2 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_ARGSORT_DEFAULTS );

    if (unlikely( tmp_ass_subscribed_2 == NULL ))
    {
        tmp_ass_subscribed_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ARGSORT_DEFAULTS );
    }

    if ( tmp_ass_subscribed_2 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ARGSORT_DEFAULTS" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 105;

        goto frame_exception_exit_1;
    }

    tmp_ass_subscript_2 = const_str_plain_kind;
    tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_2, tmp_ass_subscript_2, tmp_ass_subvalue_2 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 105;

        goto frame_exception_exit_1;
    }
    tmp_ass_subvalue_3 = Py_None;
    tmp_ass_subscribed_3 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_ARGSORT_DEFAULTS );

    if (unlikely( tmp_ass_subscribed_3 == NULL ))
    {
        tmp_ass_subscribed_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ARGSORT_DEFAULTS );
    }

    if ( tmp_ass_subscribed_3 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ARGSORT_DEFAULTS" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 106;

        goto frame_exception_exit_1;
    }

    tmp_ass_subscript_3 = const_str_plain_order;
    tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_3, tmp_ass_subscript_3, tmp_ass_subvalue_3 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 106;

        goto frame_exception_exit_1;
    }
    tmp_called_name_6 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_CompatValidator );

    if (unlikely( tmp_called_name_6 == NULL ))
    {
        tmp_called_name_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CompatValidator );
    }

    if ( tmp_called_name_6 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CompatValidator" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 107;

        goto frame_exception_exit_1;
    }

    tmp_tuple_element_4 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_ARGSORT_DEFAULTS );

    if (unlikely( tmp_tuple_element_4 == NULL ))
    {
        tmp_tuple_element_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ARGSORT_DEFAULTS );
    }

    if ( tmp_tuple_element_4 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ARGSORT_DEFAULTS" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 107;

        goto frame_exception_exit_1;
    }

    tmp_args_name_5 = PyTuple_New( 1 );
    Py_INCREF( tmp_tuple_element_4 );
    PyTuple_SET_ITEM( tmp_args_name_5, 0, tmp_tuple_element_4 );
    tmp_kw_name_5 = PyDict_Copy( const_dict_0d15a901a4a59be49734f297aafa41ce );
    frame_8cc68a7a34d1d402f731b2d00596f1d3->m_frame.f_lineno = 107;
    tmp_assign_source_26 = CALL_FUNCTION( tmp_called_name_6, tmp_args_name_5, tmp_kw_name_5 );
    Py_DECREF( tmp_args_name_5 );
    Py_DECREF( tmp_kw_name_5 );
    if ( tmp_assign_source_26 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 107;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_validate_argsort, tmp_assign_source_26 );
    tmp_called_name_7 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_OrderedDict );

    if (unlikely( tmp_called_name_7 == NULL ))
    {
        tmp_called_name_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_OrderedDict );
    }

    if ( tmp_called_name_7 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "OrderedDict" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 112;

        goto frame_exception_exit_1;
    }

    frame_8cc68a7a34d1d402f731b2d00596f1d3->m_frame.f_lineno = 112;
    tmp_assign_source_27 = CALL_FUNCTION_NO_ARGS( tmp_called_name_7 );
    if ( tmp_assign_source_27 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 112;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_ARGSORT_DEFAULTS_KIND, tmp_assign_source_27 );
    tmp_ass_subvalue_4 = const_int_neg_1;
    tmp_ass_subscribed_4 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_ARGSORT_DEFAULTS_KIND );

    if (unlikely( tmp_ass_subscribed_4 == NULL ))
    {
        tmp_ass_subscribed_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ARGSORT_DEFAULTS_KIND );
    }

    CHECK_OBJECT( tmp_ass_subscribed_4 );
    tmp_ass_subscript_4 = const_str_plain_axis;
    tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_4, tmp_ass_subscript_4, tmp_ass_subvalue_4 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 113;

        goto frame_exception_exit_1;
    }
    tmp_ass_subvalue_5 = Py_None;
    tmp_ass_subscribed_5 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_ARGSORT_DEFAULTS_KIND );

    if (unlikely( tmp_ass_subscribed_5 == NULL ))
    {
        tmp_ass_subscribed_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ARGSORT_DEFAULTS_KIND );
    }

    if ( tmp_ass_subscribed_5 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ARGSORT_DEFAULTS_KIND" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 114;

        goto frame_exception_exit_1;
    }

    tmp_ass_subscript_5 = const_str_plain_order;
    tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_5, tmp_ass_subscript_5, tmp_ass_subvalue_5 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 114;

        goto frame_exception_exit_1;
    }
    tmp_called_name_8 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_CompatValidator );

    if (unlikely( tmp_called_name_8 == NULL ))
    {
        tmp_called_name_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CompatValidator );
    }

    if ( tmp_called_name_8 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CompatValidator" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 115;

        goto frame_exception_exit_1;
    }

    tmp_tuple_element_5 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_ARGSORT_DEFAULTS_KIND );

    if (unlikely( tmp_tuple_element_5 == NULL ))
    {
        tmp_tuple_element_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ARGSORT_DEFAULTS_KIND );
    }

    if ( tmp_tuple_element_5 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ARGSORT_DEFAULTS_KIND" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 115;

        goto frame_exception_exit_1;
    }

    tmp_args_name_6 = PyTuple_New( 1 );
    Py_INCREF( tmp_tuple_element_5 );
    PyTuple_SET_ITEM( tmp_args_name_6, 0, tmp_tuple_element_5 );
    tmp_kw_name_6 = PyDict_Copy( const_dict_0d15a901a4a59be49734f297aafa41ce );
    frame_8cc68a7a34d1d402f731b2d00596f1d3->m_frame.f_lineno = 115;
    tmp_assign_source_28 = CALL_FUNCTION( tmp_called_name_8, tmp_args_name_6, tmp_kw_name_6 );
    Py_DECREF( tmp_args_name_6 );
    Py_DECREF( tmp_kw_name_6 );
    if ( tmp_assign_source_28 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 115;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_validate_argsort_kind, tmp_assign_source_28 );
    tmp_assign_source_29 = MAKE_FUNCTION_pandas$compat$numpy$function$$$function_6_validate_argsort_with_ascending(  );
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_validate_argsort_with_ascending, tmp_assign_source_29 );
    tmp_assign_source_30 = PyDict_Copy( const_dict_2b811f9b7f6bd0285cfb478e33ee30a5 );
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_CLIP_DEFAULTS, tmp_assign_source_30 );
    tmp_called_name_9 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_CompatValidator );

    if (unlikely( tmp_called_name_9 == NULL ))
    {
        tmp_called_name_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CompatValidator );
    }

    if ( tmp_called_name_9 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CompatValidator" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 137;

        goto frame_exception_exit_1;
    }

    tmp_tuple_element_6 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_CLIP_DEFAULTS );

    if (unlikely( tmp_tuple_element_6 == NULL ))
    {
        tmp_tuple_element_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CLIP_DEFAULTS );
    }

    CHECK_OBJECT( tmp_tuple_element_6 );
    tmp_args_name_7 = PyTuple_New( 1 );
    Py_INCREF( tmp_tuple_element_6 );
    PyTuple_SET_ITEM( tmp_args_name_7, 0, tmp_tuple_element_6 );
    tmp_kw_name_7 = PyDict_Copy( const_dict_bcd3f8c147fbe55491de0e3a2c6c8775 );
    frame_8cc68a7a34d1d402f731b2d00596f1d3->m_frame.f_lineno = 137;
    tmp_assign_source_31 = CALL_FUNCTION( tmp_called_name_9, tmp_args_name_7, tmp_kw_name_7 );
    Py_DECREF( tmp_args_name_7 );
    Py_DECREF( tmp_kw_name_7 );
    if ( tmp_assign_source_31 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 137;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_validate_clip, tmp_assign_source_31 );
    tmp_assign_source_32 = MAKE_FUNCTION_pandas$compat$numpy$function$$$function_7_validate_clip_with_axis(  );
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_validate_clip_with_axis, tmp_assign_source_32 );
    tmp_called_name_10 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_OrderedDict );

    if (unlikely( tmp_called_name_10 == NULL ))
    {
        tmp_called_name_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_OrderedDict );
    }

    if ( tmp_called_name_10 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "OrderedDict" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 157;

        goto frame_exception_exit_1;
    }

    frame_8cc68a7a34d1d402f731b2d00596f1d3->m_frame.f_lineno = 157;
    tmp_assign_source_33 = CALL_FUNCTION_NO_ARGS( tmp_called_name_10 );
    if ( tmp_assign_source_33 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 157;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_COMPRESS_DEFAULTS, tmp_assign_source_33 );
    tmp_ass_subvalue_6 = Py_None;
    tmp_ass_subscribed_6 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_COMPRESS_DEFAULTS );

    if (unlikely( tmp_ass_subscribed_6 == NULL ))
    {
        tmp_ass_subscribed_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_COMPRESS_DEFAULTS );
    }

    CHECK_OBJECT( tmp_ass_subscribed_6 );
    tmp_ass_subscript_6 = const_str_plain_axis;
    tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_6, tmp_ass_subscript_6, tmp_ass_subvalue_6 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 158;

        goto frame_exception_exit_1;
    }
    tmp_ass_subvalue_7 = Py_None;
    tmp_ass_subscribed_7 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_COMPRESS_DEFAULTS );

    if (unlikely( tmp_ass_subscribed_7 == NULL ))
    {
        tmp_ass_subscribed_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_COMPRESS_DEFAULTS );
    }

    if ( tmp_ass_subscribed_7 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "COMPRESS_DEFAULTS" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 159;

        goto frame_exception_exit_1;
    }

    tmp_ass_subscript_7 = const_str_plain_out;
    tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_7, tmp_ass_subscript_7, tmp_ass_subvalue_7 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 159;

        goto frame_exception_exit_1;
    }
    tmp_called_name_11 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_CompatValidator );

    if (unlikely( tmp_called_name_11 == NULL ))
    {
        tmp_called_name_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CompatValidator );
    }

    if ( tmp_called_name_11 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CompatValidator" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 160;

        goto frame_exception_exit_1;
    }

    tmp_tuple_element_7 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_COMPRESS_DEFAULTS );

    if (unlikely( tmp_tuple_element_7 == NULL ))
    {
        tmp_tuple_element_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_COMPRESS_DEFAULTS );
    }

    if ( tmp_tuple_element_7 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "COMPRESS_DEFAULTS" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 160;

        goto frame_exception_exit_1;
    }

    tmp_args_name_8 = PyTuple_New( 1 );
    Py_INCREF( tmp_tuple_element_7 );
    PyTuple_SET_ITEM( tmp_args_name_8, 0, tmp_tuple_element_7 );
    tmp_kw_name_8 = PyDict_Copy( const_dict_46eb2478d8de6a3582cd6c8c657dd518 );
    frame_8cc68a7a34d1d402f731b2d00596f1d3->m_frame.f_lineno = 160;
    tmp_assign_source_34 = CALL_FUNCTION( tmp_called_name_11, tmp_args_name_8, tmp_kw_name_8 );
    Py_DECREF( tmp_args_name_8 );
    Py_DECREF( tmp_kw_name_8 );
    if ( tmp_assign_source_34 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 160;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_validate_compress, tmp_assign_source_34 );
    tmp_called_name_12 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_OrderedDict );

    if (unlikely( tmp_called_name_12 == NULL ))
    {
        tmp_called_name_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_OrderedDict );
    }

    if ( tmp_called_name_12 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "OrderedDict" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 163;

        goto frame_exception_exit_1;
    }

    frame_8cc68a7a34d1d402f731b2d00596f1d3->m_frame.f_lineno = 163;
    tmp_assign_source_35 = CALL_FUNCTION_NO_ARGS( tmp_called_name_12 );
    if ( tmp_assign_source_35 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 163;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_CUM_FUNC_DEFAULTS, tmp_assign_source_35 );
    tmp_ass_subvalue_8 = Py_None;
    tmp_ass_subscribed_8 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_CUM_FUNC_DEFAULTS );

    if (unlikely( tmp_ass_subscribed_8 == NULL ))
    {
        tmp_ass_subscribed_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CUM_FUNC_DEFAULTS );
    }

    CHECK_OBJECT( tmp_ass_subscribed_8 );
    tmp_ass_subscript_8 = const_str_plain_dtype;
    tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_8, tmp_ass_subscript_8, tmp_ass_subvalue_8 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 164;

        goto frame_exception_exit_1;
    }
    tmp_ass_subvalue_9 = Py_None;
    tmp_ass_subscribed_9 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_CUM_FUNC_DEFAULTS );

    if (unlikely( tmp_ass_subscribed_9 == NULL ))
    {
        tmp_ass_subscribed_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CUM_FUNC_DEFAULTS );
    }

    if ( tmp_ass_subscribed_9 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CUM_FUNC_DEFAULTS" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 165;

        goto frame_exception_exit_1;
    }

    tmp_ass_subscript_9 = const_str_plain_out;
    tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_9, tmp_ass_subscript_9, tmp_ass_subvalue_9 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 165;

        goto frame_exception_exit_1;
    }
    tmp_called_name_13 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_CompatValidator );

    if (unlikely( tmp_called_name_13 == NULL ))
    {
        tmp_called_name_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CompatValidator );
    }

    if ( tmp_called_name_13 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CompatValidator" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 166;

        goto frame_exception_exit_1;
    }

    tmp_tuple_element_8 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_CUM_FUNC_DEFAULTS );

    if (unlikely( tmp_tuple_element_8 == NULL ))
    {
        tmp_tuple_element_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CUM_FUNC_DEFAULTS );
    }

    if ( tmp_tuple_element_8 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CUM_FUNC_DEFAULTS" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 166;

        goto frame_exception_exit_1;
    }

    tmp_args_name_9 = PyTuple_New( 1 );
    Py_INCREF( tmp_tuple_element_8 );
    PyTuple_SET_ITEM( tmp_args_name_9, 0, tmp_tuple_element_8 );
    tmp_kw_name_9 = PyDict_Copy( const_dict_c67bff7881a2f9460f832b506d621d61 );
    frame_8cc68a7a34d1d402f731b2d00596f1d3->m_frame.f_lineno = 166;
    tmp_assign_source_36 = CALL_FUNCTION( tmp_called_name_13, tmp_args_name_9, tmp_kw_name_9 );
    Py_DECREF( tmp_args_name_9 );
    Py_DECREF( tmp_kw_name_9 );
    if ( tmp_assign_source_36 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 166;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_validate_cum_func, tmp_assign_source_36 );
    tmp_called_name_14 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_CompatValidator );

    if (unlikely( tmp_called_name_14 == NULL ))
    {
        tmp_called_name_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CompatValidator );
    }

    if ( tmp_called_name_14 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CompatValidator" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 168;

        goto frame_exception_exit_1;
    }

    tmp_tuple_element_9 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_CUM_FUNC_DEFAULTS );

    if (unlikely( tmp_tuple_element_9 == NULL ))
    {
        tmp_tuple_element_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CUM_FUNC_DEFAULTS );
    }

    if ( tmp_tuple_element_9 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CUM_FUNC_DEFAULTS" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 168;

        goto frame_exception_exit_1;
    }

    tmp_args_name_10 = PyTuple_New( 1 );
    Py_INCREF( tmp_tuple_element_9 );
    PyTuple_SET_ITEM( tmp_args_name_10, 0, tmp_tuple_element_9 );
    tmp_kw_name_10 = PyDict_Copy( const_dict_63aad700189767b23a7036368ba50802 );
    frame_8cc68a7a34d1d402f731b2d00596f1d3->m_frame.f_lineno = 168;
    tmp_assign_source_37 = CALL_FUNCTION( tmp_called_name_14, tmp_args_name_10, tmp_kw_name_10 );
    Py_DECREF( tmp_args_name_10 );
    Py_DECREF( tmp_kw_name_10 );
    if ( tmp_assign_source_37 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 168;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_validate_cumsum, tmp_assign_source_37 );
    tmp_assign_source_38 = MAKE_FUNCTION_pandas$compat$numpy$function$$$function_8_validate_cum_func_with_skipna(  );
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_validate_cum_func_with_skipna, tmp_assign_source_38 );
    tmp_called_name_15 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_OrderedDict );

    if (unlikely( tmp_called_name_15 == NULL ))
    {
        tmp_called_name_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_OrderedDict );
    }

    if ( tmp_called_name_15 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "OrderedDict" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 187;

        goto frame_exception_exit_1;
    }

    frame_8cc68a7a34d1d402f731b2d00596f1d3->m_frame.f_lineno = 187;
    tmp_assign_source_39 = CALL_FUNCTION_NO_ARGS( tmp_called_name_15 );
    if ( tmp_assign_source_39 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 187;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_ALLANY_DEFAULTS, tmp_assign_source_39 );
    tmp_ass_subvalue_10 = Py_None;
    tmp_ass_subscribed_10 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_ALLANY_DEFAULTS );

    if (unlikely( tmp_ass_subscribed_10 == NULL ))
    {
        tmp_ass_subscribed_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ALLANY_DEFAULTS );
    }

    CHECK_OBJECT( tmp_ass_subscribed_10 );
    tmp_ass_subscript_10 = const_str_plain_dtype;
    tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_10, tmp_ass_subscript_10, tmp_ass_subvalue_10 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 188;

        goto frame_exception_exit_1;
    }
    tmp_ass_subvalue_11 = Py_None;
    tmp_ass_subscribed_11 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_ALLANY_DEFAULTS );

    if (unlikely( tmp_ass_subscribed_11 == NULL ))
    {
        tmp_ass_subscribed_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ALLANY_DEFAULTS );
    }

    if ( tmp_ass_subscribed_11 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ALLANY_DEFAULTS" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 189;

        goto frame_exception_exit_1;
    }

    tmp_ass_subscript_11 = const_str_plain_out;
    tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_11, tmp_ass_subscript_11, tmp_ass_subvalue_11 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 189;

        goto frame_exception_exit_1;
    }
    tmp_called_name_16 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_CompatValidator );

    if (unlikely( tmp_called_name_16 == NULL ))
    {
        tmp_called_name_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CompatValidator );
    }

    if ( tmp_called_name_16 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CompatValidator" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 190;

        goto frame_exception_exit_1;
    }

    tmp_tuple_element_10 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_ALLANY_DEFAULTS );

    if (unlikely( tmp_tuple_element_10 == NULL ))
    {
        tmp_tuple_element_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ALLANY_DEFAULTS );
    }

    if ( tmp_tuple_element_10 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ALLANY_DEFAULTS" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 190;

        goto frame_exception_exit_1;
    }

    tmp_args_name_11 = PyTuple_New( 1 );
    Py_INCREF( tmp_tuple_element_10 );
    PyTuple_SET_ITEM( tmp_args_name_11, 0, tmp_tuple_element_10 );
    tmp_kw_name_11 = PyDict_Copy( const_dict_ed905960e68befe81857735769b68bb1 );
    frame_8cc68a7a34d1d402f731b2d00596f1d3->m_frame.f_lineno = 190;
    tmp_assign_source_40 = CALL_FUNCTION( tmp_called_name_16, tmp_args_name_11, tmp_kw_name_11 );
    Py_DECREF( tmp_args_name_11 );
    Py_DECREF( tmp_kw_name_11 );
    if ( tmp_assign_source_40 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 190;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_validate_all, tmp_assign_source_40 );
    tmp_called_name_17 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_CompatValidator );

    if (unlikely( tmp_called_name_17 == NULL ))
    {
        tmp_called_name_17 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CompatValidator );
    }

    if ( tmp_called_name_17 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CompatValidator" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 192;

        goto frame_exception_exit_1;
    }

    tmp_tuple_element_11 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_ALLANY_DEFAULTS );

    if (unlikely( tmp_tuple_element_11 == NULL ))
    {
        tmp_tuple_element_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ALLANY_DEFAULTS );
    }

    if ( tmp_tuple_element_11 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ALLANY_DEFAULTS" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 192;

        goto frame_exception_exit_1;
    }

    tmp_args_name_12 = PyTuple_New( 1 );
    Py_INCREF( tmp_tuple_element_11 );
    PyTuple_SET_ITEM( tmp_args_name_12, 0, tmp_tuple_element_11 );
    tmp_kw_name_12 = PyDict_Copy( const_dict_4b538bf0e1c9772dfb35c0832cf13801 );
    frame_8cc68a7a34d1d402f731b2d00596f1d3->m_frame.f_lineno = 192;
    tmp_assign_source_41 = CALL_FUNCTION( tmp_called_name_17, tmp_args_name_12, tmp_kw_name_12 );
    Py_DECREF( tmp_args_name_12 );
    Py_DECREF( tmp_kw_name_12 );
    if ( tmp_assign_source_41 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 192;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_validate_any, tmp_assign_source_41 );
    tmp_assign_source_42 = PyDict_Copy( const_dict_2b811f9b7f6bd0285cfb478e33ee30a5 );
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_LOGICAL_FUNC_DEFAULTS, tmp_assign_source_42 );
    tmp_called_name_18 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_CompatValidator );

    if (unlikely( tmp_called_name_18 == NULL ))
    {
        tmp_called_name_18 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CompatValidator );
    }

    if ( tmp_called_name_18 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CompatValidator" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 196;

        goto frame_exception_exit_1;
    }

    tmp_tuple_element_12 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_LOGICAL_FUNC_DEFAULTS );

    if (unlikely( tmp_tuple_element_12 == NULL ))
    {
        tmp_tuple_element_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LOGICAL_FUNC_DEFAULTS );
    }

    CHECK_OBJECT( tmp_tuple_element_12 );
    tmp_args_name_13 = PyTuple_New( 1 );
    Py_INCREF( tmp_tuple_element_12 );
    PyTuple_SET_ITEM( tmp_args_name_13, 0, tmp_tuple_element_12 );
    tmp_kw_name_13 = PyDict_Copy( const_dict_a437617822e508113d1685038edffd63 );
    frame_8cc68a7a34d1d402f731b2d00596f1d3->m_frame.f_lineno = 196;
    tmp_assign_source_43 = CALL_FUNCTION( tmp_called_name_18, tmp_args_name_13, tmp_kw_name_13 );
    Py_DECREF( tmp_args_name_13 );
    Py_DECREF( tmp_kw_name_13 );
    if ( tmp_assign_source_43 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 196;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_validate_logical_func, tmp_assign_source_43 );
    tmp_assign_source_44 = PyDict_Copy( const_dict_2b811f9b7f6bd0285cfb478e33ee30a5 );
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_MINMAX_DEFAULTS, tmp_assign_source_44 );
    tmp_called_name_19 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_CompatValidator );

    if (unlikely( tmp_called_name_19 == NULL ))
    {
        tmp_called_name_19 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CompatValidator );
    }

    if ( tmp_called_name_19 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CompatValidator" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 199;

        goto frame_exception_exit_1;
    }

    tmp_tuple_element_13 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_MINMAX_DEFAULTS );

    if (unlikely( tmp_tuple_element_13 == NULL ))
    {
        tmp_tuple_element_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MINMAX_DEFAULTS );
    }

    CHECK_OBJECT( tmp_tuple_element_13 );
    tmp_args_name_14 = PyTuple_New( 1 );
    Py_INCREF( tmp_tuple_element_13 );
    PyTuple_SET_ITEM( tmp_args_name_14, 0, tmp_tuple_element_13 );
    tmp_kw_name_14 = PyDict_Copy( const_dict_2a6badbca8fc560e0db9b9bc67f9f960 );
    frame_8cc68a7a34d1d402f731b2d00596f1d3->m_frame.f_lineno = 199;
    tmp_assign_source_45 = CALL_FUNCTION( tmp_called_name_19, tmp_args_name_14, tmp_kw_name_14 );
    Py_DECREF( tmp_args_name_14 );
    Py_DECREF( tmp_kw_name_14 );
    if ( tmp_assign_source_45 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 199;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_validate_min, tmp_assign_source_45 );
    tmp_called_name_20 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_CompatValidator );

    if (unlikely( tmp_called_name_20 == NULL ))
    {
        tmp_called_name_20 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CompatValidator );
    }

    if ( tmp_called_name_20 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CompatValidator" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 201;

        goto frame_exception_exit_1;
    }

    tmp_tuple_element_14 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_MINMAX_DEFAULTS );

    if (unlikely( tmp_tuple_element_14 == NULL ))
    {
        tmp_tuple_element_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MINMAX_DEFAULTS );
    }

    if ( tmp_tuple_element_14 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "MINMAX_DEFAULTS" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 201;

        goto frame_exception_exit_1;
    }

    tmp_args_name_15 = PyTuple_New( 1 );
    Py_INCREF( tmp_tuple_element_14 );
    PyTuple_SET_ITEM( tmp_args_name_15, 0, tmp_tuple_element_14 );
    tmp_kw_name_15 = PyDict_Copy( const_dict_1645b279e5395dfd36dcdbf0b6ad9508 );
    frame_8cc68a7a34d1d402f731b2d00596f1d3->m_frame.f_lineno = 201;
    tmp_assign_source_46 = CALL_FUNCTION( tmp_called_name_20, tmp_args_name_15, tmp_kw_name_15 );
    Py_DECREF( tmp_args_name_15 );
    Py_DECREF( tmp_kw_name_15 );
    if ( tmp_assign_source_46 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 201;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_validate_max, tmp_assign_source_46 );
    tmp_assign_source_47 = PyDict_Copy( const_dict_733f34ee36736ef87a60e5feacd2f321 );
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_RESHAPE_DEFAULTS, tmp_assign_source_47 );
    tmp_called_name_21 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_CompatValidator );

    if (unlikely( tmp_called_name_21 == NULL ))
    {
        tmp_called_name_21 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CompatValidator );
    }

    if ( tmp_called_name_21 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CompatValidator" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 205;

        goto frame_exception_exit_1;
    }

    tmp_tuple_element_15 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_RESHAPE_DEFAULTS );

    if (unlikely( tmp_tuple_element_15 == NULL ))
    {
        tmp_tuple_element_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_RESHAPE_DEFAULTS );
    }

    CHECK_OBJECT( tmp_tuple_element_15 );
    tmp_args_name_16 = PyTuple_New( 1 );
    Py_INCREF( tmp_tuple_element_15 );
    PyTuple_SET_ITEM( tmp_args_name_16, 0, tmp_tuple_element_15 );
    tmp_kw_name_16 = PyDict_Copy( const_dict_eebf4d9cf128668e41d5ad4073195780 );
    frame_8cc68a7a34d1d402f731b2d00596f1d3->m_frame.f_lineno = 205;
    tmp_assign_source_48 = CALL_FUNCTION( tmp_called_name_21, tmp_args_name_16, tmp_kw_name_16 );
    Py_DECREF( tmp_args_name_16 );
    Py_DECREF( tmp_kw_name_16 );
    if ( tmp_assign_source_48 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 205;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_validate_reshape, tmp_assign_source_48 );
    tmp_assign_source_49 = PyDict_Copy( const_dict_21b0a41bf986b828d73b3708697d69bd );
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_REPEAT_DEFAULTS, tmp_assign_source_49 );
    tmp_called_name_22 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_CompatValidator );

    if (unlikely( tmp_called_name_22 == NULL ))
    {
        tmp_called_name_22 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CompatValidator );
    }

    if ( tmp_called_name_22 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CompatValidator" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 209;

        goto frame_exception_exit_1;
    }

    tmp_tuple_element_16 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_REPEAT_DEFAULTS );

    if (unlikely( tmp_tuple_element_16 == NULL ))
    {
        tmp_tuple_element_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_REPEAT_DEFAULTS );
    }

    CHECK_OBJECT( tmp_tuple_element_16 );
    tmp_args_name_17 = PyTuple_New( 1 );
    Py_INCREF( tmp_tuple_element_16 );
    PyTuple_SET_ITEM( tmp_args_name_17, 0, tmp_tuple_element_16 );
    tmp_kw_name_17 = PyDict_Copy( const_dict_b101e96d8e3a17c6babc9c875bf5726a );
    frame_8cc68a7a34d1d402f731b2d00596f1d3->m_frame.f_lineno = 209;
    tmp_assign_source_50 = CALL_FUNCTION( tmp_called_name_22, tmp_args_name_17, tmp_kw_name_17 );
    Py_DECREF( tmp_args_name_17 );
    Py_DECREF( tmp_kw_name_17 );
    if ( tmp_assign_source_50 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 209;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_validate_repeat, tmp_assign_source_50 );
    tmp_assign_source_51 = PyDict_Copy( const_dict_2b811f9b7f6bd0285cfb478e33ee30a5 );
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_ROUND_DEFAULTS, tmp_assign_source_51 );
    tmp_called_name_23 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_CompatValidator );

    if (unlikely( tmp_called_name_23 == NULL ))
    {
        tmp_called_name_23 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CompatValidator );
    }

    if ( tmp_called_name_23 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CompatValidator" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 213;

        goto frame_exception_exit_1;
    }

    tmp_tuple_element_17 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_ROUND_DEFAULTS );

    if (unlikely( tmp_tuple_element_17 == NULL ))
    {
        tmp_tuple_element_17 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ROUND_DEFAULTS );
    }

    CHECK_OBJECT( tmp_tuple_element_17 );
    tmp_args_name_18 = PyTuple_New( 1 );
    Py_INCREF( tmp_tuple_element_17 );
    PyTuple_SET_ITEM( tmp_args_name_18, 0, tmp_tuple_element_17 );
    tmp_kw_name_18 = PyDict_Copy( const_dict_b338527898af006c31118ee99f14b9e7 );
    frame_8cc68a7a34d1d402f731b2d00596f1d3->m_frame.f_lineno = 213;
    tmp_assign_source_52 = CALL_FUNCTION( tmp_called_name_23, tmp_args_name_18, tmp_kw_name_18 );
    Py_DECREF( tmp_args_name_18 );
    Py_DECREF( tmp_kw_name_18 );
    if ( tmp_assign_source_52 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 213;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_validate_round, tmp_assign_source_52 );
    tmp_called_name_24 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_OrderedDict );

    if (unlikely( tmp_called_name_24 == NULL ))
    {
        tmp_called_name_24 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_OrderedDict );
    }

    if ( tmp_called_name_24 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "OrderedDict" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 216;

        goto frame_exception_exit_1;
    }

    frame_8cc68a7a34d1d402f731b2d00596f1d3->m_frame.f_lineno = 216;
    tmp_assign_source_53 = CALL_FUNCTION_NO_ARGS( tmp_called_name_24 );
    if ( tmp_assign_source_53 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 216;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_SORT_DEFAULTS, tmp_assign_source_53 );
    tmp_ass_subvalue_12 = const_int_neg_1;
    tmp_ass_subscribed_12 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_SORT_DEFAULTS );

    if (unlikely( tmp_ass_subscribed_12 == NULL ))
    {
        tmp_ass_subscribed_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SORT_DEFAULTS );
    }

    CHECK_OBJECT( tmp_ass_subscribed_12 );
    tmp_ass_subscript_12 = const_str_plain_axis;
    tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_12, tmp_ass_subscript_12, tmp_ass_subvalue_12 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 217;

        goto frame_exception_exit_1;
    }
    tmp_ass_subvalue_13 = const_str_plain_quicksort;
    tmp_ass_subscribed_13 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_SORT_DEFAULTS );

    if (unlikely( tmp_ass_subscribed_13 == NULL ))
    {
        tmp_ass_subscribed_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SORT_DEFAULTS );
    }

    if ( tmp_ass_subscribed_13 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SORT_DEFAULTS" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 218;

        goto frame_exception_exit_1;
    }

    tmp_ass_subscript_13 = const_str_plain_kind;
    tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_13, tmp_ass_subscript_13, tmp_ass_subvalue_13 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 218;

        goto frame_exception_exit_1;
    }
    tmp_ass_subvalue_14 = Py_None;
    tmp_ass_subscribed_14 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_SORT_DEFAULTS );

    if (unlikely( tmp_ass_subscribed_14 == NULL ))
    {
        tmp_ass_subscribed_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SORT_DEFAULTS );
    }

    if ( tmp_ass_subscribed_14 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SORT_DEFAULTS" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 219;

        goto frame_exception_exit_1;
    }

    tmp_ass_subscript_14 = const_str_plain_order;
    tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_14, tmp_ass_subscript_14, tmp_ass_subvalue_14 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 219;

        goto frame_exception_exit_1;
    }
    tmp_called_name_25 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_CompatValidator );

    if (unlikely( tmp_called_name_25 == NULL ))
    {
        tmp_called_name_25 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CompatValidator );
    }

    if ( tmp_called_name_25 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CompatValidator" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 220;

        goto frame_exception_exit_1;
    }

    tmp_tuple_element_18 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_SORT_DEFAULTS );

    if (unlikely( tmp_tuple_element_18 == NULL ))
    {
        tmp_tuple_element_18 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SORT_DEFAULTS );
    }

    if ( tmp_tuple_element_18 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SORT_DEFAULTS" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 220;

        goto frame_exception_exit_1;
    }

    tmp_args_name_19 = PyTuple_New( 1 );
    Py_INCREF( tmp_tuple_element_18 );
    PyTuple_SET_ITEM( tmp_args_name_19, 0, tmp_tuple_element_18 );
    tmp_kw_name_19 = PyDict_Copy( const_dict_6341d11a4efcdeaa0034dab29c4edf26 );
    frame_8cc68a7a34d1d402f731b2d00596f1d3->m_frame.f_lineno = 220;
    tmp_assign_source_54 = CALL_FUNCTION( tmp_called_name_25, tmp_args_name_19, tmp_kw_name_19 );
    Py_DECREF( tmp_args_name_19 );
    Py_DECREF( tmp_kw_name_19 );
    if ( tmp_assign_source_54 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 220;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_validate_sort, tmp_assign_source_54 );
    tmp_called_name_26 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_OrderedDict );

    if (unlikely( tmp_called_name_26 == NULL ))
    {
        tmp_called_name_26 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_OrderedDict );
    }

    if ( tmp_called_name_26 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "OrderedDict" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 223;

        goto frame_exception_exit_1;
    }

    frame_8cc68a7a34d1d402f731b2d00596f1d3->m_frame.f_lineno = 223;
    tmp_assign_source_55 = CALL_FUNCTION_NO_ARGS( tmp_called_name_26 );
    if ( tmp_assign_source_55 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 223;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_STAT_FUNC_DEFAULTS, tmp_assign_source_55 );
    tmp_ass_subvalue_15 = Py_None;
    tmp_ass_subscribed_15 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_STAT_FUNC_DEFAULTS );

    if (unlikely( tmp_ass_subscribed_15 == NULL ))
    {
        tmp_ass_subscribed_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_STAT_FUNC_DEFAULTS );
    }

    CHECK_OBJECT( tmp_ass_subscribed_15 );
    tmp_ass_subscript_15 = const_str_plain_dtype;
    tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_15, tmp_ass_subscript_15, tmp_ass_subvalue_15 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 224;

        goto frame_exception_exit_1;
    }
    tmp_ass_subvalue_16 = Py_None;
    tmp_ass_subscribed_16 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_STAT_FUNC_DEFAULTS );

    if (unlikely( tmp_ass_subscribed_16 == NULL ))
    {
        tmp_ass_subscribed_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_STAT_FUNC_DEFAULTS );
    }

    if ( tmp_ass_subscribed_16 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "STAT_FUNC_DEFAULTS" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 225;

        goto frame_exception_exit_1;
    }

    tmp_ass_subscript_16 = const_str_plain_out;
    tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_16, tmp_ass_subscript_16, tmp_ass_subvalue_16 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 225;

        goto frame_exception_exit_1;
    }
    tmp_called_name_27 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_CompatValidator );

    if (unlikely( tmp_called_name_27 == NULL ))
    {
        tmp_called_name_27 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CompatValidator );
    }

    if ( tmp_called_name_27 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CompatValidator" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 226;

        goto frame_exception_exit_1;
    }

    tmp_tuple_element_19 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_STAT_FUNC_DEFAULTS );

    if (unlikely( tmp_tuple_element_19 == NULL ))
    {
        tmp_tuple_element_19 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_STAT_FUNC_DEFAULTS );
    }

    if ( tmp_tuple_element_19 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "STAT_FUNC_DEFAULTS" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 226;

        goto frame_exception_exit_1;
    }

    tmp_args_name_20 = PyTuple_New( 1 );
    Py_INCREF( tmp_tuple_element_19 );
    PyTuple_SET_ITEM( tmp_args_name_20, 0, tmp_tuple_element_19 );
    tmp_kw_name_20 = PyDict_Copy( const_dict_a437617822e508113d1685038edffd63 );
    frame_8cc68a7a34d1d402f731b2d00596f1d3->m_frame.f_lineno = 226;
    tmp_assign_source_56 = CALL_FUNCTION( tmp_called_name_27, tmp_args_name_20, tmp_kw_name_20 );
    Py_DECREF( tmp_args_name_20 );
    Py_DECREF( tmp_kw_name_20 );
    if ( tmp_assign_source_56 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 226;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_validate_stat_func, tmp_assign_source_56 );
    tmp_called_name_28 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_CompatValidator );

    if (unlikely( tmp_called_name_28 == NULL ))
    {
        tmp_called_name_28 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CompatValidator );
    }

    if ( tmp_called_name_28 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CompatValidator" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 228;

        goto frame_exception_exit_1;
    }

    tmp_tuple_element_20 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_STAT_FUNC_DEFAULTS );

    if (unlikely( tmp_tuple_element_20 == NULL ))
    {
        tmp_tuple_element_20 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_STAT_FUNC_DEFAULTS );
    }

    if ( tmp_tuple_element_20 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "STAT_FUNC_DEFAULTS" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 228;

        goto frame_exception_exit_1;
    }

    tmp_args_name_21 = PyTuple_New( 1 );
    Py_INCREF( tmp_tuple_element_20 );
    PyTuple_SET_ITEM( tmp_args_name_21, 0, tmp_tuple_element_20 );
    tmp_kw_name_21 = PyDict_Copy( const_dict_a7eec54e725b025c6b8cad7d8970188d );
    frame_8cc68a7a34d1d402f731b2d00596f1d3->m_frame.f_lineno = 228;
    tmp_assign_source_57 = CALL_FUNCTION( tmp_called_name_28, tmp_args_name_21, tmp_kw_name_21 );
    Py_DECREF( tmp_args_name_21 );
    Py_DECREF( tmp_kw_name_21 );
    if ( tmp_assign_source_57 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 228;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_validate_sum, tmp_assign_source_57 );
    tmp_called_name_29 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_CompatValidator );

    if (unlikely( tmp_called_name_29 == NULL ))
    {
        tmp_called_name_29 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CompatValidator );
    }

    if ( tmp_called_name_29 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CompatValidator" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 230;

        goto frame_exception_exit_1;
    }

    tmp_tuple_element_21 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_STAT_FUNC_DEFAULTS );

    if (unlikely( tmp_tuple_element_21 == NULL ))
    {
        tmp_tuple_element_21 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_STAT_FUNC_DEFAULTS );
    }

    if ( tmp_tuple_element_21 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "STAT_FUNC_DEFAULTS" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 230;

        goto frame_exception_exit_1;
    }

    tmp_args_name_22 = PyTuple_New( 1 );
    Py_INCREF( tmp_tuple_element_21 );
    PyTuple_SET_ITEM( tmp_args_name_22, 0, tmp_tuple_element_21 );
    tmp_kw_name_22 = PyDict_Copy( const_dict_59c9d706c48d2b98911957257a41eb33 );
    frame_8cc68a7a34d1d402f731b2d00596f1d3->m_frame.f_lineno = 230;
    tmp_assign_source_58 = CALL_FUNCTION( tmp_called_name_29, tmp_args_name_22, tmp_kw_name_22 );
    Py_DECREF( tmp_args_name_22 );
    Py_DECREF( tmp_kw_name_22 );
    if ( tmp_assign_source_58 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 230;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_validate_mean, tmp_assign_source_58 );
    tmp_called_name_30 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_OrderedDict );

    if (unlikely( tmp_called_name_30 == NULL ))
    {
        tmp_called_name_30 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_OrderedDict );
    }

    if ( tmp_called_name_30 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "OrderedDict" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 233;

        goto frame_exception_exit_1;
    }

    frame_8cc68a7a34d1d402f731b2d00596f1d3->m_frame.f_lineno = 233;
    tmp_assign_source_59 = CALL_FUNCTION_NO_ARGS( tmp_called_name_30 );
    if ( tmp_assign_source_59 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 233;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_STAT_DDOF_FUNC_DEFAULTS, tmp_assign_source_59 );
    tmp_ass_subvalue_17 = Py_None;
    tmp_ass_subscribed_17 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_STAT_DDOF_FUNC_DEFAULTS );

    if (unlikely( tmp_ass_subscribed_17 == NULL ))
    {
        tmp_ass_subscribed_17 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_STAT_DDOF_FUNC_DEFAULTS );
    }

    CHECK_OBJECT( tmp_ass_subscribed_17 );
    tmp_ass_subscript_17 = const_str_plain_dtype;
    tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_17, tmp_ass_subscript_17, tmp_ass_subvalue_17 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 234;

        goto frame_exception_exit_1;
    }
    tmp_ass_subvalue_18 = Py_None;
    tmp_ass_subscribed_18 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_STAT_DDOF_FUNC_DEFAULTS );

    if (unlikely( tmp_ass_subscribed_18 == NULL ))
    {
        tmp_ass_subscribed_18 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_STAT_DDOF_FUNC_DEFAULTS );
    }

    if ( tmp_ass_subscribed_18 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "STAT_DDOF_FUNC_DEFAULTS" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 235;

        goto frame_exception_exit_1;
    }

    tmp_ass_subscript_18 = const_str_plain_out;
    tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_18, tmp_ass_subscript_18, tmp_ass_subvalue_18 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 235;

        goto frame_exception_exit_1;
    }
    tmp_called_name_31 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_CompatValidator );

    if (unlikely( tmp_called_name_31 == NULL ))
    {
        tmp_called_name_31 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CompatValidator );
    }

    if ( tmp_called_name_31 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CompatValidator" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 236;

        goto frame_exception_exit_1;
    }

    tmp_tuple_element_22 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_STAT_DDOF_FUNC_DEFAULTS );

    if (unlikely( tmp_tuple_element_22 == NULL ))
    {
        tmp_tuple_element_22 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_STAT_DDOF_FUNC_DEFAULTS );
    }

    if ( tmp_tuple_element_22 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "STAT_DDOF_FUNC_DEFAULTS" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 236;

        goto frame_exception_exit_1;
    }

    tmp_args_name_23 = PyTuple_New( 1 );
    Py_INCREF( tmp_tuple_element_22 );
    PyTuple_SET_ITEM( tmp_args_name_23, 0, tmp_tuple_element_22 );
    tmp_kw_name_23 = PyDict_Copy( const_dict_a437617822e508113d1685038edffd63 );
    frame_8cc68a7a34d1d402f731b2d00596f1d3->m_frame.f_lineno = 236;
    tmp_assign_source_60 = CALL_FUNCTION( tmp_called_name_31, tmp_args_name_23, tmp_kw_name_23 );
    Py_DECREF( tmp_args_name_23 );
    Py_DECREF( tmp_kw_name_23 );
    if ( tmp_assign_source_60 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 236;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_validate_stat_ddof_func, tmp_assign_source_60 );
    tmp_called_name_32 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_OrderedDict );

    if (unlikely( tmp_called_name_32 == NULL ))
    {
        tmp_called_name_32 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_OrderedDict );
    }

    if ( tmp_called_name_32 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "OrderedDict" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 239;

        goto frame_exception_exit_1;
    }

    frame_8cc68a7a34d1d402f731b2d00596f1d3->m_frame.f_lineno = 239;
    tmp_assign_source_61 = CALL_FUNCTION_NO_ARGS( tmp_called_name_32 );
    if ( tmp_assign_source_61 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 239;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_TAKE_DEFAULTS, tmp_assign_source_61 );
    tmp_ass_subvalue_19 = Py_None;
    tmp_ass_subscribed_19 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_TAKE_DEFAULTS );

    if (unlikely( tmp_ass_subscribed_19 == NULL ))
    {
        tmp_ass_subscribed_19 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_TAKE_DEFAULTS );
    }

    CHECK_OBJECT( tmp_ass_subscribed_19 );
    tmp_ass_subscript_19 = const_str_plain_out;
    tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_19, tmp_ass_subscript_19, tmp_ass_subvalue_19 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 240;

        goto frame_exception_exit_1;
    }
    tmp_ass_subvalue_20 = const_str_plain_raise;
    tmp_ass_subscribed_20 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_TAKE_DEFAULTS );

    if (unlikely( tmp_ass_subscribed_20 == NULL ))
    {
        tmp_ass_subscribed_20 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_TAKE_DEFAULTS );
    }

    if ( tmp_ass_subscribed_20 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "TAKE_DEFAULTS" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 241;

        goto frame_exception_exit_1;
    }

    tmp_ass_subscript_20 = const_str_plain_mode;
    tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_20, tmp_ass_subscript_20, tmp_ass_subvalue_20 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 241;

        goto frame_exception_exit_1;
    }
    tmp_called_name_33 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_CompatValidator );

    if (unlikely( tmp_called_name_33 == NULL ))
    {
        tmp_called_name_33 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CompatValidator );
    }

    if ( tmp_called_name_33 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CompatValidator" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 242;

        goto frame_exception_exit_1;
    }

    tmp_tuple_element_23 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_TAKE_DEFAULTS );

    if (unlikely( tmp_tuple_element_23 == NULL ))
    {
        tmp_tuple_element_23 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_TAKE_DEFAULTS );
    }

    if ( tmp_tuple_element_23 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "TAKE_DEFAULTS" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 242;

        goto frame_exception_exit_1;
    }

    tmp_args_name_24 = PyTuple_New( 1 );
    Py_INCREF( tmp_tuple_element_23 );
    PyTuple_SET_ITEM( tmp_args_name_24, 0, tmp_tuple_element_23 );
    tmp_kw_name_24 = PyDict_Copy( const_dict_eea3f2a677f40c199539a2474c474882 );
    frame_8cc68a7a34d1d402f731b2d00596f1d3->m_frame.f_lineno = 242;
    tmp_assign_source_62 = CALL_FUNCTION( tmp_called_name_33, tmp_args_name_24, tmp_kw_name_24 );
    Py_DECREF( tmp_args_name_24 );
    Py_DECREF( tmp_kw_name_24 );
    if ( tmp_assign_source_62 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 242;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_validate_take, tmp_assign_source_62 );
    tmp_assign_source_63 = MAKE_FUNCTION_pandas$compat$numpy$function$$$function_9_validate_take_with_convert(  );
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_validate_take_with_convert, tmp_assign_source_63 );
    tmp_assign_source_64 = PyDict_Copy( const_dict_beaf922b95e152e9819e9297e7b00df0 );
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_TRANSPOSE_DEFAULTS, tmp_assign_source_64 );
    tmp_called_name_34 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_CompatValidator );

    if (unlikely( tmp_called_name_34 == NULL ))
    {
        tmp_called_name_34 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CompatValidator );
    }

    if ( tmp_called_name_34 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CompatValidator" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 263;

        goto frame_exception_exit_1;
    }

    tmp_tuple_element_24 = GET_STRING_DICT_VALUE( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_TRANSPOSE_DEFAULTS );

    if (unlikely( tmp_tuple_element_24 == NULL ))
    {
        tmp_tuple_element_24 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_TRANSPOSE_DEFAULTS );
    }

    CHECK_OBJECT( tmp_tuple_element_24 );
    tmp_args_name_25 = PyTuple_New( 1 );
    Py_INCREF( tmp_tuple_element_24 );
    PyTuple_SET_ITEM( tmp_args_name_25, 0, tmp_tuple_element_24 );
    tmp_kw_name_25 = PyDict_Copy( const_dict_0bab94f1acda136e36e4f3a0d199ad0c );
    frame_8cc68a7a34d1d402f731b2d00596f1d3->m_frame.f_lineno = 263;
    tmp_assign_source_65 = CALL_FUNCTION( tmp_called_name_34, tmp_args_name_25, tmp_kw_name_25 );
    Py_DECREF( tmp_args_name_25 );
    Py_DECREF( tmp_kw_name_25 );
    if ( tmp_assign_source_65 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 263;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_validate_transpose, tmp_assign_source_65 );

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_8cc68a7a34d1d402f731b2d00596f1d3 );
#endif
    popFrameStack();

    assertFrameObject( frame_8cc68a7a34d1d402f731b2d00596f1d3 );

    goto frame_no_exception_2;
    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_8cc68a7a34d1d402f731b2d00596f1d3 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_8cc68a7a34d1d402f731b2d00596f1d3, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_8cc68a7a34d1d402f731b2d00596f1d3->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_8cc68a7a34d1d402f731b2d00596f1d3, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;
    frame_no_exception_2:;
    tmp_assign_source_66 = MAKE_FUNCTION_pandas$compat$numpy$function$$$function_10_validate_transpose_for_generic(  );
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_validate_transpose_for_generic, tmp_assign_source_66 );
    tmp_assign_source_67 = MAKE_FUNCTION_pandas$compat$numpy$function$$$function_11_validate_window_func(  );
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_validate_window_func, tmp_assign_source_67 );
    tmp_assign_source_68 = MAKE_FUNCTION_pandas$compat$numpy$function$$$function_12_validate_rolling_func(  );
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_validate_rolling_func, tmp_assign_source_68 );
    tmp_assign_source_69 = MAKE_FUNCTION_pandas$compat$numpy$function$$$function_13_validate_expanding_func(  );
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_validate_expanding_func, tmp_assign_source_69 );
    tmp_defaults_3 = const_tuple_none_tuple;
    Py_INCREF( tmp_defaults_3 );
    tmp_assign_source_70 = MAKE_FUNCTION_pandas$compat$numpy$function$$$function_14_validate_groupby_func( tmp_defaults_3 );
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_validate_groupby_func, tmp_assign_source_70 );
    tmp_assign_source_71 = const_tuple_670d66b460103c129188211b2bd1c4bf_tuple;
    UPDATE_STRING_DICT0( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_RESAMPLER_NUMPY_OPS, tmp_assign_source_71 );
    tmp_assign_source_72 = MAKE_FUNCTION_pandas$compat$numpy$function$$$function_15_validate_resampler_func(  );
    UPDATE_STRING_DICT1( moduledict_pandas$compat$numpy$function, (Nuitka_StringObject *)const_str_plain_validate_resampler_func, tmp_assign_source_72 );

    return MOD_RETURN_VALUE( module_pandas$compat$numpy$function );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
