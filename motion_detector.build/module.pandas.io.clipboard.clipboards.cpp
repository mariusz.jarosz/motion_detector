/* Generated code for Python source for module 'pandas.io.clipboard.clipboards'
 * created by Nuitka version 0.5.32.7
 *
 * This code is in part copyright 2018 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The _module_pandas$io$clipboard$clipboards is a Python object pointer of module type. */

/* Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_pandas$io$clipboard$clipboards;
PyDictObject *moduledict_pandas$io$clipboard$clipboards;

/* The module constants used, if any. */
extern PyObject *const_str_plain_cb;
extern PyObject *const_tuple_str_digest_c075052d723d6707083e869a0e3659bb_tuple;
static PyObject *const_str_digest_c74b8fc24b688b8525cf2e0b3a7db16d;
extern PyObject *const_str_plain_metaclass;
static PyObject *const_list_71826f1b3786fb88c225418f225b3823_list;
static PyObject *const_str_plain_wait_for_text;
static PyObject *const_str_digest_363b20897abb8fe206a2ed773d13d28a;
extern PyObject *const_tuple_str_plain___class___tuple;
static PyObject *const_str_digest_a88e8a7bd4f9ed41f0331a9209a4c70a;
static PyObject *const_str_digest_344ef046ed627942028fc8b792076a41;
extern PyObject *const_str_plain_object;
extern PyObject *const_str_plain___file__;
static PyObject *const_list_str_plain_pbpaste_str_plain_r_list;
static PyObject *const_tuple_str_plain_p_str_plain_stdout_str_plain_stderr_tuple;
extern PyObject *const_str_plain_qdbus;
extern PyObject *const_str_plain_args;
extern PyObject *const_tuple_str_plain_self_str_plain_args_str_plain_kwargs_tuple;
extern PyObject *const_str_plain_encode;
extern PyObject *const_str_plain_gtk;
static PyObject *const_tuple_str_plain_ClipboardUnavailable_tuple;
extern PyObject *const_str_plain_init_xsel_clipboard;
static PyObject *const_str_digest_9c1b8d1bf096eeae592150bfe0b7f351;
static PyObject *const_str_plain_paste_osx;
extern PyObject *const_int_neg_1;
extern PyObject *const_str_digest_77d8fb15a813ae44a7b8d0fe53dbee10;
extern PyObject *const_str_plain_instance;
static PyObject *const_list_a197c19226e81914eb0e409d9a9b699d_list;
extern PyObject *const_str_plain_init_osx_clipboard;
extern PyObject *const_str_plain_None;
extern PyObject *const_str_plain_input;
static PyObject *const_str_plain_Clipboard;
static PyObject *const_str_digest_57a50efac3dc6f8b8afc68b347b3c8cd;
static PyObject *const_tuple_str_plain_PyperclipException_tuple;
static PyObject *const_str_plain_paste_qt;
static PyObject *const_str_digest_12824c42ae734a5ec21969db08809d5c;
static PyObject *const_tuple_str_plain_text_str_plain_cb_str_plain_app_tuple;
static PyObject *const_tuple_str_plain_copy_xclip_str_plain_paste_xclip_tuple;
extern PyObject *const_str_plain_stdout;
extern PyObject *const_str_plain_PyperclipException;
extern PyObject *const_str_plain_PIPE;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_plain_Popen;
static PyObject *const_str_plain_pbcopy;
static PyObject *const_str_digest_7f6d117bc092141e51ea443a0e0999c2;
static PyObject *const_str_plain_setText;
static PyObject *const_tuple_str_plain_QApplication_tuple;
static PyObject *const_str_plain_ClipboardUnavailable;
static PyObject *const_str_digest_77292a4a8d17a0319dc328f25380c431;
extern PyObject *const_str_plain___qualname__;
static PyObject *const_str_digest_3134b871d582f11ef1d443dc3acee782;
extern PyObject *const_str_plain___bool__;
extern PyObject *const_str_plain_w;
extern PyObject *const_str_plain_p;
static PyObject *const_str_plain_EXCEPT_MSG;
extern PyObject *const_str_plain_clipboard;
extern PyObject *const_tuple_str_plain_self_tuple;
static PyObject *const_list_80c22030d7e9874daf8196134c6cac83_list;
extern PyObject *const_str_plain_subprocess;
static PyObject *const_tuple_d6651be8fe64dd14e68ee317bda2aea6_tuple;
extern PyObject *const_str_plain_xsel;
static PyObject *const_str_plain_copy_xsel;
static PyObject *const_str_plain_setClipboardContents;
extern PyObject *const_tuple_str_newline_tuple;
extern PyObject *const_tuple_empty;
static PyObject *const_tuple_list_80c22030d7e9874daf8196134c6cac83_list_tuple;
extern PyObject *const_str_plain_stderr;
extern PyObject *const_str_plain_store;
extern PyObject *const_str_plain_r;
static PyObject *const_list_e4d6a0388a09ce944591175f0308d4e0_list;
extern PyObject *const_str_plain_endswith;
static PyObject *const_str_plain_clipboardContents;
static PyObject *const_str_digest_5bfd7f32b3b59128ef50a98dbb13a245;
static PyObject *const_tuple_str_plain_text_str_plain_p_tuple;
extern PyObject *const_str_plain_exceptions;
static PyObject *const_str_digest_aad9db65811d431e3670ce5a949fca3d;
static PyObject *const_str_plain_copy_gtk;
static PyObject *const_str_digest_e75ca76f27e0e640a58096dff4d2ac2a;
static PyObject *const_str_digest_d577dede9743e2b3fad57acdda45ba59;
static PyObject *const_tuple_str_plain_copy_xsel_str_plain_paste_xsel_tuple;
static PyObject *const_str_digest_75c1113d1333e3f18890bd794bf85661;
static PyObject *const_tuple_list_67e309ba5d5eed8090523ac1cf863d7d_list_tuple;
static PyObject *const_tuple_str_plain_copy_osx_str_plain_paste_osx_tuple;
extern PyObject *const_str_plain_decode;
static PyObject *const_tuple_str_plain_text_str_plain_gtk_tuple;
extern PyObject *const_str_plain_False;
extern PyObject *const_str_digest_c075052d723d6707083e869a0e3659bb;
static PyObject *const_str_plain_getClipboardContents;
extern PyObject *const_int_0;
extern PyObject *const_dict_c453dda1d3901e4cb85d5781f8962504;
static PyObject *const_tuple_list_71826f1b3786fb88c225418f225b3823_list_tuple;
static PyObject *const_str_plain_copy_klipper;
static PyObject *const_str_plain_copy_osx;
extern PyObject *const_str_plain_init_qt_clipboard;
static PyObject *const_tuple_list_str_plain_pbcopy_str_plain_w_list_tuple;
static PyObject *const_tuple_str_plain_clipboardContents_str_plain_gtk_tuple;
static PyObject *const_tuple_str_plain_PY2_str_plain_text_type_tuple;
extern PyObject *const_str_plain_text;
static PyObject *const_str_plain_set_text;
extern PyObject *const_str_plain_init_klipper_clipboard;
extern PyObject *const_str_plain_init_gtk_clipboard;
static PyObject *const_str_plain_paste_xclip;
static PyObject *const_str_plain_close_fds;
static PyObject *const_tuple_str_plain_gtk_str_plain_copy_gtk_str_plain_paste_gtk_tuple;
static PyObject *const_list_67e309ba5d5eed8090523ac1cf863d7d_list;
static PyObject *const_tuple_str_plain_cb_str_plain_app_tuple;
extern PyObject *const_str_plain_xclip;
extern PyObject *const_str_plain_init_no_clipboard;
extern PyObject *const_str_plain_c;
static PyObject *const_str_digest_fe278fbce1ae480c42f145639e1ea634;
extern PyObject *const_str_plain_communicate;
extern PyObject *const_str_plain_type;
static PyObject *const_str_plain_app;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_plain___class__;
static PyObject *const_list_str_plain_pbcopy_str_plain_w_list;
static PyObject *const_str_plain_paste_xsel;
extern PyObject *const_str_plain_PY2;
extern PyObject *const_tuple_type_object_tuple;
extern PyObject *const_str_plain___module__;
static PyObject *const_str_plain_copy_xclip;
static PyObject *const_str_plain_paste_klipper;
extern PyObject *const_str_plain_text_type;
static PyObject *const_str_plain_copy_qt;
static PyObject *const_str_digest_78382f502c325686bf515744a066db9f;
static PyObject *const_str_digest_c1b240794be4eea191dd0f8d775fee7d;
extern PyObject *const_slice_none_int_neg_1_none;
extern PyObject *const_int_pos_1;
static PyObject *const_tuple_list_a197c19226e81914eb0e409d9a9b699d_list_tuple;
extern PyObject *const_str_plain___nonzero__;
static PyObject *const_str_digest_0d36178cf1fdd7da1acd434235941e8a;
static PyObject *const_str_digest_b8883e772664a69f81a6c1eee0e6292f;
extern PyObject *const_str_plain_init_xclip_clipboard;
extern PyObject *const_str_newline;
static PyObject *const_tuple_94f5801727ab58a7d419ef1a672c6a73_tuple;
extern PyObject *const_str_plain___prepare__;
extern PyObject *const_str_plain_self;
static PyObject *const_str_digest_88e1244ea918f637f982de49331de205;
static PyObject *const_str_digest_488addb9f0a7f06aba6f2490db82d71c;
extern PyObject *const_str_plain_stdin;
extern PyObject *const_str_plain___call__;
static PyObject *const_str_plain_QApplication;
static PyObject *const_str_digest_01d040ac9dfb1f7d0f957fcd5293298f;
static PyObject *const_str_digest_91d3fabe5e3af50ef0546b25193cf397;
extern PyObject *const_str_plain_kwargs;
static PyObject *const_tuple_list_str_plain_pbpaste_str_plain_r_list_tuple;
static PyObject *const_str_digest_353a9d53d4fa97e57ee1ac6c2e6eb773;
static PyObject *const_tuple_list_e4d6a0388a09ce944591175f0308d4e0_list_tuple;
static PyObject *const_str_plain_pbpaste;
static PyObject *const_str_plain_paste_gtk;
extern PyObject *const_str_empty;
static PyObject *const_tuple_str_plain_ClipboardUnavailable_tuple_type_object_tuple_tuple;
static PyObject *const_tuple_str_plain_copy_klipper_str_plain_paste_klipper_tuple;
static PyObject *const_str_digest_aefe37d186409357e48b054613e2a70f;
static PyObject *const_str_digest_f20837e53c97e663ae559a2f47d1375d;
static PyObject *const_str_digest_d3a192d9a90f12283eb154da5edd57d5;
static PyObject *const_str_digest_b75401c8f73575104762484b09641fec;
static PyObject *module_filename_obj;

static bool constants_created = false;

static void createModuleConstants( void )
{
    const_str_digest_c74b8fc24b688b8525cf2e0b3a7db16d = UNSTREAM_STRING( &constant_bin[ 21084 ], 2, 0 );
    const_list_71826f1b3786fb88c225418f225b3823_list = PyList_New( 4 );
    PyList_SET_ITEM( const_list_71826f1b3786fb88c225418f225b3823_list, 0, const_str_plain_qdbus ); Py_INCREF( const_str_plain_qdbus );
    const_str_digest_57a50efac3dc6f8b8afc68b347b3c8cd = UNSTREAM_STRING( &constant_bin[ 2950836 ], 15, 0 );
    PyList_SET_ITEM( const_list_71826f1b3786fb88c225418f225b3823_list, 1, const_str_digest_57a50efac3dc6f8b8afc68b347b3c8cd ); Py_INCREF( const_str_digest_57a50efac3dc6f8b8afc68b347b3c8cd );
    const_str_digest_5bfd7f32b3b59128ef50a98dbb13a245 = UNSTREAM_STRING( &constant_bin[ 2950851 ], 8, 0 );
    PyList_SET_ITEM( const_list_71826f1b3786fb88c225418f225b3823_list, 2, const_str_digest_5bfd7f32b3b59128ef50a98dbb13a245 ); Py_INCREF( const_str_digest_5bfd7f32b3b59128ef50a98dbb13a245 );
    const_str_plain_getClipboardContents = UNSTREAM_STRING( &constant_bin[ 2950859 ], 20, 1 );
    PyList_SET_ITEM( const_list_71826f1b3786fb88c225418f225b3823_list, 3, const_str_plain_getClipboardContents ); Py_INCREF( const_str_plain_getClipboardContents );
    const_str_plain_wait_for_text = UNSTREAM_STRING( &constant_bin[ 2950879 ], 13, 1 );
    const_str_digest_363b20897abb8fe206a2ed773d13d28a = UNSTREAM_STRING( &constant_bin[ 2950892 ], 36, 0 );
    const_str_digest_a88e8a7bd4f9ed41f0331a9209a4c70a = UNSTREAM_STRING( &constant_bin[ 2950928 ], 15, 0 );
    const_str_digest_344ef046ed627942028fc8b792076a41 = UNSTREAM_STRING( &constant_bin[ 2950943 ], 47, 0 );
    const_list_str_plain_pbpaste_str_plain_r_list = PyList_New( 2 );
    const_str_plain_pbpaste = UNSTREAM_STRING( &constant_bin[ 2950433 ], 7, 1 );
    PyList_SET_ITEM( const_list_str_plain_pbpaste_str_plain_r_list, 0, const_str_plain_pbpaste ); Py_INCREF( const_str_plain_pbpaste );
    PyList_SET_ITEM( const_list_str_plain_pbpaste_str_plain_r_list, 1, const_str_plain_r ); Py_INCREF( const_str_plain_r );
    const_tuple_str_plain_p_str_plain_stdout_str_plain_stderr_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_p_str_plain_stdout_str_plain_stderr_tuple, 0, const_str_plain_p ); Py_INCREF( const_str_plain_p );
    PyTuple_SET_ITEM( const_tuple_str_plain_p_str_plain_stdout_str_plain_stderr_tuple, 1, const_str_plain_stdout ); Py_INCREF( const_str_plain_stdout );
    PyTuple_SET_ITEM( const_tuple_str_plain_p_str_plain_stdout_str_plain_stderr_tuple, 2, const_str_plain_stderr ); Py_INCREF( const_str_plain_stderr );
    const_tuple_str_plain_ClipboardUnavailable_tuple = PyTuple_New( 1 );
    const_str_plain_ClipboardUnavailable = UNSTREAM_STRING( &constant_bin[ 2950970 ], 20, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_ClipboardUnavailable_tuple, 0, const_str_plain_ClipboardUnavailable ); Py_INCREF( const_str_plain_ClipboardUnavailable );
    const_str_digest_9c1b8d1bf096eeae592150bfe0b7f351 = UNSTREAM_STRING( &constant_bin[ 2950990 ], 39, 0 );
    const_str_plain_paste_osx = UNSTREAM_STRING( &constant_bin[ 2951029 ], 9, 1 );
    const_list_a197c19226e81914eb0e409d9a9b699d_list = PyList_New( 3 );
    PyList_SET_ITEM( const_list_a197c19226e81914eb0e409d9a9b699d_list, 0, const_str_plain_xsel ); Py_INCREF( const_str_plain_xsel );
    const_str_digest_78382f502c325686bf515744a066db9f = UNSTREAM_STRING( &constant_bin[ 58185 ], 2, 0 );
    PyList_SET_ITEM( const_list_a197c19226e81914eb0e409d9a9b699d_list, 1, const_str_digest_78382f502c325686bf515744a066db9f ); Py_INCREF( const_str_digest_78382f502c325686bf515744a066db9f );
    const_str_digest_75c1113d1333e3f18890bd794bf85661 = UNSTREAM_STRING( &constant_bin[ 9244 ], 2, 0 );
    PyList_SET_ITEM( const_list_a197c19226e81914eb0e409d9a9b699d_list, 2, const_str_digest_75c1113d1333e3f18890bd794bf85661 ); Py_INCREF( const_str_digest_75c1113d1333e3f18890bd794bf85661 );
    const_str_plain_Clipboard = UNSTREAM_STRING( &constant_bin[ 2950862 ], 9, 1 );
    const_tuple_str_plain_PyperclipException_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_PyperclipException_tuple, 0, const_str_plain_PyperclipException ); Py_INCREF( const_str_plain_PyperclipException );
    const_str_plain_paste_qt = UNSTREAM_STRING( &constant_bin[ 2951038 ], 8, 1 );
    const_str_digest_12824c42ae734a5ec21969db08809d5c = UNSTREAM_STRING( &constant_bin[ 2951046 ], 37, 0 );
    const_tuple_str_plain_text_str_plain_cb_str_plain_app_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_text_str_plain_cb_str_plain_app_tuple, 0, const_str_plain_text ); Py_INCREF( const_str_plain_text );
    PyTuple_SET_ITEM( const_tuple_str_plain_text_str_plain_cb_str_plain_app_tuple, 1, const_str_plain_cb ); Py_INCREF( const_str_plain_cb );
    const_str_plain_app = UNSTREAM_STRING( &constant_bin[ 3670 ], 3, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_text_str_plain_cb_str_plain_app_tuple, 2, const_str_plain_app ); Py_INCREF( const_str_plain_app );
    const_tuple_str_plain_copy_xclip_str_plain_paste_xclip_tuple = PyTuple_New( 2 );
    const_str_plain_copy_xclip = UNSTREAM_STRING( &constant_bin[ 2951083 ], 10, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_copy_xclip_str_plain_paste_xclip_tuple, 0, const_str_plain_copy_xclip ); Py_INCREF( const_str_plain_copy_xclip );
    const_str_plain_paste_xclip = UNSTREAM_STRING( &constant_bin[ 2951093 ], 11, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_copy_xclip_str_plain_paste_xclip_tuple, 1, const_str_plain_paste_xclip ); Py_INCREF( const_str_plain_paste_xclip );
    const_str_plain_pbcopy = UNSTREAM_STRING( &constant_bin[ 2950422 ], 6, 1 );
    const_str_digest_7f6d117bc092141e51ea443a0e0999c2 = UNSTREAM_STRING( &constant_bin[ 2951104 ], 38, 0 );
    const_str_plain_setText = UNSTREAM_STRING( &constant_bin[ 2951142 ], 7, 1 );
    const_tuple_str_plain_QApplication_tuple = PyTuple_New( 1 );
    const_str_plain_QApplication = UNSTREAM_STRING( &constant_bin[ 2951149 ], 12, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_QApplication_tuple, 0, const_str_plain_QApplication ); Py_INCREF( const_str_plain_QApplication );
    const_str_digest_77292a4a8d17a0319dc328f25380c431 = UNSTREAM_STRING( &constant_bin[ 2951161 ], 56, 0 );
    const_str_digest_3134b871d582f11ef1d443dc3acee782 = UNSTREAM_STRING( &constant_bin[ 2951217 ], 96, 0 );
    const_str_plain_EXCEPT_MSG = UNSTREAM_STRING( &constant_bin[ 2951313 ], 10, 1 );
    const_list_80c22030d7e9874daf8196134c6cac83_list = PyList_New( 3 );
    PyList_SET_ITEM( const_list_80c22030d7e9874daf8196134c6cac83_list, 0, const_str_plain_xclip ); Py_INCREF( const_str_plain_xclip );
    const_str_digest_f20837e53c97e663ae559a2f47d1375d = UNSTREAM_STRING( &constant_bin[ 2951323 ], 10, 0 );
    PyList_SET_ITEM( const_list_80c22030d7e9874daf8196134c6cac83_list, 1, const_str_digest_f20837e53c97e663ae559a2f47d1375d ); Py_INCREF( const_str_digest_f20837e53c97e663ae559a2f47d1375d );
    PyList_SET_ITEM( const_list_80c22030d7e9874daf8196134c6cac83_list, 2, const_str_plain_c ); Py_INCREF( const_str_plain_c );
    const_tuple_d6651be8fe64dd14e68ee317bda2aea6_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_d6651be8fe64dd14e68ee317bda2aea6_tuple, 0, const_str_plain_p ); Py_INCREF( const_str_plain_p );
    PyTuple_SET_ITEM( const_tuple_d6651be8fe64dd14e68ee317bda2aea6_tuple, 1, const_str_plain_stdout ); Py_INCREF( const_str_plain_stdout );
    PyTuple_SET_ITEM( const_tuple_d6651be8fe64dd14e68ee317bda2aea6_tuple, 2, const_str_plain_stderr ); Py_INCREF( const_str_plain_stderr );
    const_str_plain_clipboardContents = UNSTREAM_STRING( &constant_bin[ 2951333 ], 17, 1 );
    PyTuple_SET_ITEM( const_tuple_d6651be8fe64dd14e68ee317bda2aea6_tuple, 3, const_str_plain_clipboardContents ); Py_INCREF( const_str_plain_clipboardContents );
    const_str_plain_copy_xsel = UNSTREAM_STRING( &constant_bin[ 2951133 ], 9, 1 );
    const_str_plain_setClipboardContents = UNSTREAM_STRING( &constant_bin[ 2951350 ], 20, 1 );
    const_tuple_list_80c22030d7e9874daf8196134c6cac83_list_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_list_80c22030d7e9874daf8196134c6cac83_list_tuple, 0, const_list_80c22030d7e9874daf8196134c6cac83_list ); Py_INCREF( const_list_80c22030d7e9874daf8196134c6cac83_list );
    const_list_e4d6a0388a09ce944591175f0308d4e0_list = PyList_New( 4 );
    PyList_SET_ITEM( const_list_e4d6a0388a09ce944591175f0308d4e0_list, 0, const_str_plain_xclip ); Py_INCREF( const_str_plain_xclip );
    PyList_SET_ITEM( const_list_e4d6a0388a09ce944591175f0308d4e0_list, 1, const_str_digest_f20837e53c97e663ae559a2f47d1375d ); Py_INCREF( const_str_digest_f20837e53c97e663ae559a2f47d1375d );
    PyList_SET_ITEM( const_list_e4d6a0388a09ce944591175f0308d4e0_list, 2, const_str_plain_c ); Py_INCREF( const_str_plain_c );
    PyList_SET_ITEM( const_list_e4d6a0388a09ce944591175f0308d4e0_list, 3, const_str_digest_c74b8fc24b688b8525cf2e0b3a7db16d ); Py_INCREF( const_str_digest_c74b8fc24b688b8525cf2e0b3a7db16d );
    const_tuple_str_plain_text_str_plain_p_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_text_str_plain_p_tuple, 0, const_str_plain_text ); Py_INCREF( const_str_plain_text );
    PyTuple_SET_ITEM( const_tuple_str_plain_text_str_plain_p_tuple, 1, const_str_plain_p ); Py_INCREF( const_str_plain_p );
    const_str_digest_aad9db65811d431e3670ce5a949fca3d = UNSTREAM_STRING( &constant_bin[ 2951370 ], 14, 0 );
    const_str_plain_copy_gtk = UNSTREAM_STRING( &constant_bin[ 2950920 ], 8, 1 );
    const_str_digest_e75ca76f27e0e640a58096dff4d2ac2a = UNSTREAM_STRING( &constant_bin[ 2951384 ], 41, 0 );
    const_str_digest_d577dede9743e2b3fad57acdda45ba59 = UNSTREAM_STRING( &constant_bin[ 2951425 ], 56, 0 );
    const_tuple_str_plain_copy_xsel_str_plain_paste_xsel_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_copy_xsel_str_plain_paste_xsel_tuple, 0, const_str_plain_copy_xsel ); Py_INCREF( const_str_plain_copy_xsel );
    const_str_plain_paste_xsel = UNSTREAM_STRING( &constant_bin[ 2951019 ], 10, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_copy_xsel_str_plain_paste_xsel_tuple, 1, const_str_plain_paste_xsel ); Py_INCREF( const_str_plain_paste_xsel );
    const_tuple_list_67e309ba5d5eed8090523ac1cf863d7d_list_tuple = PyTuple_New( 1 );
    const_list_67e309ba5d5eed8090523ac1cf863d7d_list = PyList_New( 3 );
    PyList_SET_ITEM( const_list_67e309ba5d5eed8090523ac1cf863d7d_list, 0, const_str_plain_xsel ); Py_INCREF( const_str_plain_xsel );
    PyList_SET_ITEM( const_list_67e309ba5d5eed8090523ac1cf863d7d_list, 1, const_str_digest_78382f502c325686bf515744a066db9f ); Py_INCREF( const_str_digest_78382f502c325686bf515744a066db9f );
    PyList_SET_ITEM( const_list_67e309ba5d5eed8090523ac1cf863d7d_list, 2, const_str_digest_c74b8fc24b688b8525cf2e0b3a7db16d ); Py_INCREF( const_str_digest_c74b8fc24b688b8525cf2e0b3a7db16d );
    PyTuple_SET_ITEM( const_tuple_list_67e309ba5d5eed8090523ac1cf863d7d_list_tuple, 0, const_list_67e309ba5d5eed8090523ac1cf863d7d_list ); Py_INCREF( const_list_67e309ba5d5eed8090523ac1cf863d7d_list );
    const_tuple_str_plain_copy_osx_str_plain_paste_osx_tuple = PyTuple_New( 2 );
    const_str_plain_copy_osx = UNSTREAM_STRING( &constant_bin[ 2951481 ], 8, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_copy_osx_str_plain_paste_osx_tuple, 0, const_str_plain_copy_osx ); Py_INCREF( const_str_plain_copy_osx );
    PyTuple_SET_ITEM( const_tuple_str_plain_copy_osx_str_plain_paste_osx_tuple, 1, const_str_plain_paste_osx ); Py_INCREF( const_str_plain_paste_osx );
    const_tuple_str_plain_text_str_plain_gtk_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_text_str_plain_gtk_tuple, 0, const_str_plain_text ); Py_INCREF( const_str_plain_text );
    PyTuple_SET_ITEM( const_tuple_str_plain_text_str_plain_gtk_tuple, 1, const_str_plain_gtk ); Py_INCREF( const_str_plain_gtk );
    const_tuple_list_71826f1b3786fb88c225418f225b3823_list_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_list_71826f1b3786fb88c225418f225b3823_list_tuple, 0, const_list_71826f1b3786fb88c225418f225b3823_list ); Py_INCREF( const_list_71826f1b3786fb88c225418f225b3823_list );
    const_str_plain_copy_klipper = UNSTREAM_STRING( &constant_bin[ 2951489 ], 12, 1 );
    const_tuple_list_str_plain_pbcopy_str_plain_w_list_tuple = PyTuple_New( 1 );
    const_list_str_plain_pbcopy_str_plain_w_list = PyList_New( 2 );
    PyList_SET_ITEM( const_list_str_plain_pbcopy_str_plain_w_list, 0, const_str_plain_pbcopy ); Py_INCREF( const_str_plain_pbcopy );
    PyList_SET_ITEM( const_list_str_plain_pbcopy_str_plain_w_list, 1, const_str_plain_w ); Py_INCREF( const_str_plain_w );
    PyTuple_SET_ITEM( const_tuple_list_str_plain_pbcopy_str_plain_w_list_tuple, 0, const_list_str_plain_pbcopy_str_plain_w_list ); Py_INCREF( const_list_str_plain_pbcopy_str_plain_w_list );
    const_tuple_str_plain_clipboardContents_str_plain_gtk_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_clipboardContents_str_plain_gtk_tuple, 0, const_str_plain_clipboardContents ); Py_INCREF( const_str_plain_clipboardContents );
    PyTuple_SET_ITEM( const_tuple_str_plain_clipboardContents_str_plain_gtk_tuple, 1, const_str_plain_gtk ); Py_INCREF( const_str_plain_gtk );
    const_tuple_str_plain_PY2_str_plain_text_type_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_PY2_str_plain_text_type_tuple, 0, const_str_plain_PY2 ); Py_INCREF( const_str_plain_PY2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_PY2_str_plain_text_type_tuple, 1, const_str_plain_text_type ); Py_INCREF( const_str_plain_text_type );
    const_str_plain_set_text = UNSTREAM_STRING( &constant_bin[ 2951501 ], 8, 1 );
    const_str_plain_close_fds = UNSTREAM_STRING( &constant_bin[ 2951509 ], 9, 1 );
    const_tuple_str_plain_gtk_str_plain_copy_gtk_str_plain_paste_gtk_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_gtk_str_plain_copy_gtk_str_plain_paste_gtk_tuple, 0, const_str_plain_gtk ); Py_INCREF( const_str_plain_gtk );
    PyTuple_SET_ITEM( const_tuple_str_plain_gtk_str_plain_copy_gtk_str_plain_paste_gtk_tuple, 1, const_str_plain_copy_gtk ); Py_INCREF( const_str_plain_copy_gtk );
    const_str_plain_paste_gtk = UNSTREAM_STRING( &constant_bin[ 2951074 ], 9, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_gtk_str_plain_copy_gtk_str_plain_paste_gtk_tuple, 2, const_str_plain_paste_gtk ); Py_INCREF( const_str_plain_paste_gtk );
    const_tuple_str_plain_cb_str_plain_app_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_cb_str_plain_app_tuple, 0, const_str_plain_cb ); Py_INCREF( const_str_plain_cb );
    PyTuple_SET_ITEM( const_tuple_str_plain_cb_str_plain_app_tuple, 1, const_str_plain_app ); Py_INCREF( const_str_plain_app );
    const_str_digest_fe278fbce1ae480c42f145639e1ea634 = UNSTREAM_STRING( &constant_bin[ 2951518 ], 37, 0 );
    const_str_plain_paste_klipper = UNSTREAM_STRING( &constant_bin[ 2951555 ], 13, 1 );
    const_str_plain_copy_qt = UNSTREAM_STRING( &constant_bin[ 2951568 ], 7, 1 );
    const_str_digest_c1b240794be4eea191dd0f8d775fee7d = UNSTREAM_STRING( &constant_bin[ 2951575 ], 30, 0 );
    const_tuple_list_a197c19226e81914eb0e409d9a9b699d_list_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_list_a197c19226e81914eb0e409d9a9b699d_list_tuple, 0, const_list_a197c19226e81914eb0e409d9a9b699d_list ); Py_INCREF( const_list_a197c19226e81914eb0e409d9a9b699d_list );
    const_str_digest_0d36178cf1fdd7da1acd434235941e8a = UNSTREAM_STRING( &constant_bin[ 2951605 ], 36, 0 );
    const_str_digest_b8883e772664a69f81a6c1eee0e6292f = UNSTREAM_STRING( &constant_bin[ 2951641 ], 143, 0 );
    const_tuple_94f5801727ab58a7d419ef1a672c6a73_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_94f5801727ab58a7d419ef1a672c6a73_tuple, 0, const_str_plain_QApplication ); Py_INCREF( const_str_plain_QApplication );
    PyTuple_SET_ITEM( const_tuple_94f5801727ab58a7d419ef1a672c6a73_tuple, 1, const_str_plain_app ); Py_INCREF( const_str_plain_app );
    PyTuple_SET_ITEM( const_tuple_94f5801727ab58a7d419ef1a672c6a73_tuple, 2, const_str_plain_copy_qt ); Py_INCREF( const_str_plain_copy_qt );
    PyTuple_SET_ITEM( const_tuple_94f5801727ab58a7d419ef1a672c6a73_tuple, 3, const_str_plain_paste_qt ); Py_INCREF( const_str_plain_paste_qt );
    const_str_digest_88e1244ea918f637f982de49331de205 = UNSTREAM_STRING( &constant_bin[ 2951784 ], 34, 0 );
    const_str_digest_488addb9f0a7f06aba6f2490db82d71c = UNSTREAM_STRING( &constant_bin[ 2951818 ], 40, 0 );
    const_str_digest_01d040ac9dfb1f7d0f957fcd5293298f = UNSTREAM_STRING( &constant_bin[ 2951858 ], 59, 0 );
    const_str_digest_91d3fabe5e3af50ef0546b25193cf397 = UNSTREAM_STRING( &constant_bin[ 2951917 ], 11, 0 );
    const_tuple_list_str_plain_pbpaste_str_plain_r_list_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_list_str_plain_pbpaste_str_plain_r_list_tuple, 0, const_list_str_plain_pbpaste_str_plain_r_list ); Py_INCREF( const_list_str_plain_pbpaste_str_plain_r_list );
    const_str_digest_353a9d53d4fa97e57ee1ac6c2e6eb773 = UNSTREAM_STRING( &constant_bin[ 2951928 ], 39, 0 );
    const_tuple_list_e4d6a0388a09ce944591175f0308d4e0_list_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_list_e4d6a0388a09ce944591175f0308d4e0_list_tuple, 0, const_list_e4d6a0388a09ce944591175f0308d4e0_list ); Py_INCREF( const_list_e4d6a0388a09ce944591175f0308d4e0_list );
    const_tuple_str_plain_ClipboardUnavailable_tuple_type_object_tuple_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_ClipboardUnavailable_tuple_type_object_tuple_tuple, 0, const_str_plain_ClipboardUnavailable ); Py_INCREF( const_str_plain_ClipboardUnavailable );
    PyTuple_SET_ITEM( const_tuple_str_plain_ClipboardUnavailable_tuple_type_object_tuple_tuple, 1, const_tuple_type_object_tuple ); Py_INCREF( const_tuple_type_object_tuple );
    const_tuple_str_plain_copy_klipper_str_plain_paste_klipper_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_copy_klipper_str_plain_paste_klipper_tuple, 0, const_str_plain_copy_klipper ); Py_INCREF( const_str_plain_copy_klipper );
    PyTuple_SET_ITEM( const_tuple_str_plain_copy_klipper_str_plain_paste_klipper_tuple, 1, const_str_plain_paste_klipper ); Py_INCREF( const_str_plain_paste_klipper );
    const_str_digest_aefe37d186409357e48b054613e2a70f = UNSTREAM_STRING( &constant_bin[ 2951967 ], 35, 0 );
    const_str_digest_d3a192d9a90f12283eb154da5edd57d5 = UNSTREAM_STRING( &constant_bin[ 2952002 ], 44, 0 );
    const_str_digest_b75401c8f73575104762484b09641fec = UNSTREAM_STRING( &constant_bin[ 2952046 ], 45, 0 );

    constants_created = true;
}

#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_pandas$io$clipboard$clipboards( void )
{
    // The module may not have been used at all.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_127047bff6d7dcdd52e5c8eaa1f1c927;
static PyCodeObject *codeobj_d30a2e91f920a55897facd7649785ed7;
static PyCodeObject *codeobj_769dfc462486ce80fc0bc0d1c8e7624d;
static PyCodeObject *codeobj_d508f46656f5486f34a7ebeb22429281;
static PyCodeObject *codeobj_7e43cdf22d01e6a3a52aa4651088efe8;
static PyCodeObject *codeobj_bbe75b4ebd91ab9b61b71ddb6993e4fa;
static PyCodeObject *codeobj_8350dbeca557534164273aa396913a15;
static PyCodeObject *codeobj_3a7a67941f36b22f99f07a9864c43d40;
static PyCodeObject *codeobj_40eba54f9f7106c02033be5247abbf8c;
static PyCodeObject *codeobj_1692a6eebf291b9f98c65b665e9ae3e0;
static PyCodeObject *codeobj_24f1f9c5a5b1b760d9c1c3c6ad13fcd7;
static PyCodeObject *codeobj_0360e4d1df6ae1edcb843c4520bf78bf;
static PyCodeObject *codeobj_fec8e9c26b096417568201a5035234a3;
static PyCodeObject *codeobj_1b1008a41b3a1e85a29e2c501f0885d0;
static PyCodeObject *codeobj_8934cb9e516698b66388f7d4ae2f6cdd;
static PyCodeObject *codeobj_f5a793ca030f17ceec59cb6cf88afc11;
static PyCodeObject *codeobj_78a5d34c6b0186cda416b593e4be6914;
static PyCodeObject *codeobj_f1b78c0038fb3fe45086095e1029c853;
static PyCodeObject *codeobj_46465757b3b8fd2109993fc577b505af;
static PyCodeObject *codeobj_641b96a224a5ed333f02bedaf0f5eed7;
static PyCodeObject *codeobj_4fb293be5988723048e8055c5d838fa9;
static PyCodeObject *codeobj_f305a7321124f97a9f33318e46ca5996;
static PyCodeObject *codeobj_de0bad81b2cc52a2e72b9f4e0d8d0cb6;
static PyCodeObject *codeobj_17129171ff48345b5f382b101aeaade1;

static void createModuleCodeObjects(void)
{
    module_filename_obj = const_str_digest_3134b871d582f11ef1d443dc3acee782;
    codeobj_127047bff6d7dcdd52e5c8eaa1f1c927 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_353a9d53d4fa97e57ee1ac6c2e6eb773, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_d30a2e91f920a55897facd7649785ed7 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_ClipboardUnavailable, 131, const_tuple_str_plain___class___tuple, 0, 0, CO_OPTIMIZED | CO_NOFREE );
    codeobj_769dfc462486ce80fc0bc0d1c8e7624d = MAKE_CODEOBJ( module_filename_obj, const_str_plain___bool__, 140, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_d508f46656f5486f34a7ebeb22429281 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___call__, 133, const_tuple_str_plain_self_str_plain_args_str_plain_kwargs_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARARGS | CO_VARKEYWORDS | CO_NOFREE );
    codeobj_7e43cdf22d01e6a3a52aa4651088efe8 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___nonzero__, 137, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_bbe75b4ebd91ab9b61b71ddb6993e4fa = MAKE_CODEOBJ( module_filename_obj, const_str_plain_copy_gtk, 28, const_tuple_str_plain_text_str_plain_gtk_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_8350dbeca557534164273aa396913a15 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_copy_klipper, 103, const_tuple_str_plain_text_str_plain_p_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_3a7a67941f36b22f99f07a9864c43d40 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_copy_osx, 11, const_tuple_str_plain_text_str_plain_p_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_40eba54f9f7106c02033be5247abbf8c = MAKE_CODEOBJ( module_filename_obj, const_str_plain_copy_qt, 61, const_tuple_str_plain_text_str_plain_cb_str_plain_app_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_1692a6eebf291b9f98c65b665e9ae3e0 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_copy_xclip, 73, const_tuple_str_plain_text_str_plain_p_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_24f1f9c5a5b1b760d9c1c3c6ad13fcd7 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_copy_xsel, 88, const_tuple_str_plain_text_str_plain_p_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_0360e4d1df6ae1edcb843c4520bf78bf = MAKE_CODEOBJ( module_filename_obj, const_str_plain_init_gtk_clipboard, 25, const_tuple_str_plain_gtk_str_plain_copy_gtk_str_plain_paste_gtk_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_fec8e9c26b096417568201a5035234a3 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_init_klipper_clipboard, 102, const_tuple_str_plain_copy_klipper_str_plain_paste_klipper_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_1b1008a41b3a1e85a29e2c501f0885d0 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_init_no_clipboard, 130, const_tuple_str_plain_ClipboardUnavailable_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_8934cb9e516698b66388f7d4ae2f6cdd = MAKE_CODEOBJ( module_filename_obj, const_str_plain_init_osx_clipboard, 10, const_tuple_str_plain_copy_osx_str_plain_paste_osx_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_f5a793ca030f17ceec59cb6cf88afc11 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_init_qt_clipboard, 45, const_tuple_94f5801727ab58a7d419ef1a672c6a73_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_78a5d34c6b0186cda416b593e4be6914 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_init_xclip_clipboard, 72, const_tuple_str_plain_copy_xclip_str_plain_paste_xclip_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_f1b78c0038fb3fe45086095e1029c853 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_init_xsel_clipboard, 87, const_tuple_str_plain_copy_xsel_str_plain_paste_xsel_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_46465757b3b8fd2109993fc577b505af = MAKE_CODEOBJ( module_filename_obj, const_str_plain_paste_gtk, 34, const_tuple_str_plain_clipboardContents_str_plain_gtk_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_641b96a224a5ed333f02bedaf0f5eed7 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_paste_klipper, 110, const_tuple_d6651be8fe64dd14e68ee317bda2aea6_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_4fb293be5988723048e8055c5d838fa9 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_paste_osx, 16, const_tuple_str_plain_p_str_plain_stdout_str_plain_stderr_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_f305a7321124f97a9f33318e46ca5996 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_paste_qt, 65, const_tuple_str_plain_cb_str_plain_app_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_de0bad81b2cc52a2e72b9f4e0d8d0cb6 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_paste_xclip, 78, const_tuple_str_plain_p_str_plain_stdout_str_plain_stderr_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_17129171ff48345b5f382b101aeaade1 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_paste_xsel, 93, const_tuple_str_plain_p_str_plain_stdout_str_plain_stderr_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
}

// The module function declarations.
static PyObject *MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_1_init_osx_clipboard(  );


static PyObject *MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_1_init_osx_clipboard$$$function_1_copy_osx(  );


static PyObject *MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_1_init_osx_clipboard$$$function_2_paste_osx(  );


static PyObject *MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_2_init_gtk_clipboard(  );


static PyObject *MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_2_init_gtk_clipboard$$$function_1_copy_gtk( struct Nuitka_CellObject *closure_gtk );


static PyObject *MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_2_init_gtk_clipboard$$$function_2_paste_gtk( struct Nuitka_CellObject *closure_gtk );


static PyObject *MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_3_init_qt_clipboard(  );


static PyObject *MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_3_init_qt_clipboard$$$function_1_copy_qt( struct Nuitka_CellObject *closure_app );


static PyObject *MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_3_init_qt_clipboard$$$function_2_paste_qt( struct Nuitka_CellObject *closure_app );


static PyObject *MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_4_init_xclip_clipboard(  );


static PyObject *MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_4_init_xclip_clipboard$$$function_1_copy_xclip(  );


static PyObject *MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_4_init_xclip_clipboard$$$function_2_paste_xclip(  );


static PyObject *MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_5_init_xsel_clipboard(  );


static PyObject *MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_5_init_xsel_clipboard$$$function_1_copy_xsel(  );


static PyObject *MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_5_init_xsel_clipboard$$$function_2_paste_xsel(  );


static PyObject *MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_6_init_klipper_clipboard(  );


static PyObject *MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_6_init_klipper_clipboard$$$function_1_copy_klipper(  );


static PyObject *MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_6_init_klipper_clipboard$$$function_2_paste_klipper(  );


static PyObject *MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_7_init_no_clipboard(  );


static PyObject *MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_7_init_no_clipboard$$$function_1___call__(  );


static PyObject *MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_7_init_no_clipboard$$$function_2___nonzero__(  );


static PyObject *MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_7_init_no_clipboard$$$function_3___bool__(  );


// The module function definitions.
static PyObject *impl_pandas$io$clipboard$clipboards$$$function_1_init_osx_clipboard( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *var_copy_osx = NULL;
    PyObject *var_paste_osx = NULL;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_return_value;
    PyObject *tmp_tuple_element_1;
    tmp_return_value = NULL;

    // Actual function code.
    tmp_assign_source_1 = MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_1_init_osx_clipboard$$$function_1_copy_osx(  );
    assert( var_copy_osx == NULL );
    var_copy_osx = tmp_assign_source_1;

    tmp_assign_source_2 = MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_1_init_osx_clipboard$$$function_2_paste_osx(  );
    assert( var_paste_osx == NULL );
    var_paste_osx = tmp_assign_source_2;

    // Tried code:
    tmp_tuple_element_1 = var_copy_osx;

    CHECK_OBJECT( tmp_tuple_element_1 );
    tmp_return_value = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_return_value, 0, tmp_tuple_element_1 );
    tmp_tuple_element_1 = var_paste_osx;

    CHECK_OBJECT( tmp_tuple_element_1 );
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_return_value, 1, tmp_tuple_element_1 );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$io$clipboard$clipboards$$$function_1_init_osx_clipboard );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)var_copy_osx );
    Py_DECREF( var_copy_osx );
    var_copy_osx = NULL;

    Py_XDECREF( var_paste_osx );
    var_paste_osx = NULL;

    goto function_return_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$io$clipboard$clipboards$$$function_1_init_osx_clipboard );
    return NULL;

function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$io$clipboard$clipboards$$$function_1_init_osx_clipboard$$$function_1_copy_osx( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_text = python_pars[ 0 ];
    PyObject *var_p = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_args_name_1;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_called_instance_1;
    PyObject *tmp_called_name_1;
    PyObject *tmp_called_name_2;
    PyObject *tmp_dict_key_1;
    PyObject *tmp_dict_key_2;
    PyObject *tmp_dict_key_3;
    PyObject *tmp_dict_value_1;
    PyObject *tmp_dict_value_2;
    PyObject *tmp_dict_value_3;
    PyObject *tmp_kw_name_1;
    PyObject *tmp_kw_name_2;
    int tmp_res;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    PyObject *tmp_source_name_2;
    PyObject *tmp_source_name_3;
    NUITKA_MAY_BE_UNUSED PyObject *tmp_unused;
    static struct Nuitka_FrameObject *cache_frame_3a7a67941f36b22f99f07a9864c43d40 = NULL;

    struct Nuitka_FrameObject *frame_3a7a67941f36b22f99f07a9864c43d40;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_3a7a67941f36b22f99f07a9864c43d40, codeobj_3a7a67941f36b22f99f07a9864c43d40, module_pandas$io$clipboard$clipboards, sizeof(void *)+sizeof(void *) );
    frame_3a7a67941f36b22f99f07a9864c43d40 = cache_frame_3a7a67941f36b22f99f07a9864c43d40;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_3a7a67941f36b22f99f07a9864c43d40 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_3a7a67941f36b22f99f07a9864c43d40 ) == 2 ); // Frame stack

    // Framed code:
    tmp_source_name_1 = GET_STRING_DICT_VALUE( moduledict_pandas$io$clipboard$clipboards, (Nuitka_StringObject *)const_str_plain_subprocess );

    if (unlikely( tmp_source_name_1 == NULL ))
    {
        tmp_source_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_subprocess );
    }

    if ( tmp_source_name_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "subprocess" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 12;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }

    tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_Popen );
    if ( tmp_called_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 12;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }
    tmp_args_name_1 = DEEP_COPY( const_tuple_list_str_plain_pbcopy_str_plain_w_list_tuple );
    tmp_dict_key_1 = const_str_plain_stdin;
    tmp_source_name_2 = GET_STRING_DICT_VALUE( moduledict_pandas$io$clipboard$clipboards, (Nuitka_StringObject *)const_str_plain_subprocess );

    if (unlikely( tmp_source_name_2 == NULL ))
    {
        tmp_source_name_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_subprocess );
    }

    if ( tmp_source_name_2 == NULL )
    {
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_name_1 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "subprocess" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 13;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }

    tmp_dict_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_PIPE );
    if ( tmp_dict_value_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_name_1 );

        exception_lineno = 13;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }
    tmp_kw_name_1 = _PyDict_NewPresized( 2 );
    tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
    Py_DECREF( tmp_dict_value_1 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_2 = const_str_plain_close_fds;
    tmp_dict_value_2 = Py_True;
    tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
    assert( !(tmp_res != 0) );
    frame_3a7a67941f36b22f99f07a9864c43d40->m_frame.f_lineno = 12;
    tmp_assign_source_1 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
    Py_DECREF( tmp_called_name_1 );
    Py_DECREF( tmp_args_name_1 );
    Py_DECREF( tmp_kw_name_1 );
    if ( tmp_assign_source_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 12;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }
    assert( var_p == NULL );
    var_p = tmp_assign_source_1;

    tmp_source_name_3 = var_p;

    CHECK_OBJECT( tmp_source_name_3 );
    tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_communicate );
    if ( tmp_called_name_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 14;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }
    tmp_dict_key_3 = const_str_plain_input;
    tmp_called_instance_1 = par_text;

    if ( tmp_called_instance_1 == NULL )
    {
        Py_DECREF( tmp_called_name_2 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "text" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 14;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }

    frame_3a7a67941f36b22f99f07a9864c43d40->m_frame.f_lineno = 14;
    tmp_dict_value_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_encode, &PyTuple_GET_ITEM( const_tuple_str_digest_c075052d723d6707083e869a0e3659bb_tuple, 0 ) );

    if ( tmp_dict_value_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_2 );

        exception_lineno = 14;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }
    tmp_kw_name_2 = _PyDict_NewPresized( 1 );
    tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_3, tmp_dict_value_3 );
    Py_DECREF( tmp_dict_value_3 );
    assert( !(tmp_res != 0) );
    frame_3a7a67941f36b22f99f07a9864c43d40->m_frame.f_lineno = 14;
    tmp_unused = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_2, tmp_kw_name_2 );
    Py_DECREF( tmp_called_name_2 );
    Py_DECREF( tmp_kw_name_2 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 14;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_unused );

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3a7a67941f36b22f99f07a9864c43d40 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3a7a67941f36b22f99f07a9864c43d40 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_3a7a67941f36b22f99f07a9864c43d40, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_3a7a67941f36b22f99f07a9864c43d40->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_3a7a67941f36b22f99f07a9864c43d40, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_3a7a67941f36b22f99f07a9864c43d40,
        type_description_1,
        par_text,
        var_p
    );


    // Release cached frame.
    if ( frame_3a7a67941f36b22f99f07a9864c43d40 == cache_frame_3a7a67941f36b22f99f07a9864c43d40 )
    {
        Py_DECREF( frame_3a7a67941f36b22f99f07a9864c43d40 );
    }
    cache_frame_3a7a67941f36b22f99f07a9864c43d40 = NULL;

    assertFrameObject( frame_3a7a67941f36b22f99f07a9864c43d40 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$io$clipboard$clipboards$$$function_1_init_osx_clipboard$$$function_1_copy_osx );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_text );
    par_text = NULL;

    Py_XDECREF( var_p );
    var_p = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_text );
    par_text = NULL;

    Py_XDECREF( var_p );
    var_p = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$io$clipboard$clipboards$$$function_1_init_osx_clipboard$$$function_1_copy_osx );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$io$clipboard$clipboards$$$function_1_init_osx_clipboard$$$function_2_paste_osx( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *var_p = NULL;
    PyObject *var_stdout = NULL;
    PyObject *var_stderr = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *tmp_args_name_1;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_assign_source_5;
    PyObject *tmp_assign_source_6;
    PyObject *tmp_called_instance_1;
    PyObject *tmp_called_instance_2;
    PyObject *tmp_called_name_1;
    PyObject *tmp_dict_key_1;
    PyObject *tmp_dict_key_2;
    PyObject *tmp_dict_value_1;
    PyObject *tmp_dict_value_2;
    PyObject *tmp_iter_arg_1;
    PyObject *tmp_iterator_attempt;
    PyObject *tmp_iterator_name_1;
    PyObject *tmp_kw_name_1;
    int tmp_res;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    PyObject *tmp_source_name_2;
    PyObject *tmp_unpack_1;
    PyObject *tmp_unpack_2;
    static struct Nuitka_FrameObject *cache_frame_4fb293be5988723048e8055c5d838fa9 = NULL;

    struct Nuitka_FrameObject *frame_4fb293be5988723048e8055c5d838fa9;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_4fb293be5988723048e8055c5d838fa9, codeobj_4fb293be5988723048e8055c5d838fa9, module_pandas$io$clipboard$clipboards, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_4fb293be5988723048e8055c5d838fa9 = cache_frame_4fb293be5988723048e8055c5d838fa9;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_4fb293be5988723048e8055c5d838fa9 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_4fb293be5988723048e8055c5d838fa9 ) == 2 ); // Frame stack

    // Framed code:
    tmp_source_name_1 = GET_STRING_DICT_VALUE( moduledict_pandas$io$clipboard$clipboards, (Nuitka_StringObject *)const_str_plain_subprocess );

    if (unlikely( tmp_source_name_1 == NULL ))
    {
        tmp_source_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_subprocess );
    }

    if ( tmp_source_name_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "subprocess" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 17;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_Popen );
    if ( tmp_called_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 17;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    tmp_args_name_1 = DEEP_COPY( const_tuple_list_str_plain_pbpaste_str_plain_r_list_tuple );
    tmp_dict_key_1 = const_str_plain_stdout;
    tmp_source_name_2 = GET_STRING_DICT_VALUE( moduledict_pandas$io$clipboard$clipboards, (Nuitka_StringObject *)const_str_plain_subprocess );

    if (unlikely( tmp_source_name_2 == NULL ))
    {
        tmp_source_name_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_subprocess );
    }

    if ( tmp_source_name_2 == NULL )
    {
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_name_1 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "subprocess" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 18;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    tmp_dict_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_PIPE );
    if ( tmp_dict_value_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_name_1 );

        exception_lineno = 18;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    tmp_kw_name_1 = _PyDict_NewPresized( 2 );
    tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
    Py_DECREF( tmp_dict_value_1 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_2 = const_str_plain_close_fds;
    tmp_dict_value_2 = Py_True;
    tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
    assert( !(tmp_res != 0) );
    frame_4fb293be5988723048e8055c5d838fa9->m_frame.f_lineno = 17;
    tmp_assign_source_1 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
    Py_DECREF( tmp_called_name_1 );
    Py_DECREF( tmp_args_name_1 );
    Py_DECREF( tmp_kw_name_1 );
    if ( tmp_assign_source_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 17;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    assert( var_p == NULL );
    var_p = tmp_assign_source_1;

    // Tried code:
    tmp_called_instance_1 = var_p;

    CHECK_OBJECT( tmp_called_instance_1 );
    frame_4fb293be5988723048e8055c5d838fa9->m_frame.f_lineno = 19;
    tmp_iter_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_communicate );
    if ( tmp_iter_arg_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 19;
        type_description_1 = "ooo";
        goto try_except_handler_2;
    }
    tmp_assign_source_2 = MAKE_ITERATOR( tmp_iter_arg_1 );
    Py_DECREF( tmp_iter_arg_1 );
    if ( tmp_assign_source_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 19;
        type_description_1 = "ooo";
        goto try_except_handler_2;
    }
    assert( tmp_tuple_unpack_1__source_iter == NULL );
    tmp_tuple_unpack_1__source_iter = tmp_assign_source_2;

    // Tried code:
    tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;

    CHECK_OBJECT( tmp_unpack_1 );
    tmp_assign_source_3 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
    if ( tmp_assign_source_3 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        type_description_1 = "ooo";
        exception_lineno = 19;
        goto try_except_handler_3;
    }
    assert( tmp_tuple_unpack_1__element_1 == NULL );
    tmp_tuple_unpack_1__element_1 = tmp_assign_source_3;

    tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;

    CHECK_OBJECT( tmp_unpack_2 );
    tmp_assign_source_4 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
    if ( tmp_assign_source_4 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        type_description_1 = "ooo";
        exception_lineno = 19;
        goto try_except_handler_3;
    }
    assert( tmp_tuple_unpack_1__element_2 == NULL );
    tmp_tuple_unpack_1__element_2 = tmp_assign_source_4;

    tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;

    CHECK_OBJECT( tmp_iterator_name_1 );
    // Check if iterator has left-over elements.
    CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

    tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

    if (likely( tmp_iterator_attempt == NULL ))
    {
        PyObject *error = GET_ERROR_OCCURRED();

        if ( error != NULL )
        {
            if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
            {
                CLEAR_ERROR_OCCURRED();
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                type_description_1 = "ooo";
                exception_lineno = 19;
                goto try_except_handler_3;
            }
        }
    }
    else
    {
        Py_DECREF( tmp_iterator_attempt );

        // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
        PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
        PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

        type_description_1 = "ooo";
        exception_lineno = 19;
        goto try_except_handler_3;
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_2;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    tmp_assign_source_5 = tmp_tuple_unpack_1__element_1;

    CHECK_OBJECT( tmp_assign_source_5 );
    assert( var_stdout == NULL );
    Py_INCREF( tmp_assign_source_5 );
    var_stdout = tmp_assign_source_5;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    tmp_assign_source_6 = tmp_tuple_unpack_1__element_2;

    CHECK_OBJECT( tmp_assign_source_6 );
    assert( var_stderr == NULL );
    Py_INCREF( tmp_assign_source_6 );
    var_stderr = tmp_assign_source_6;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    tmp_called_instance_2 = var_stdout;

    if ( tmp_called_instance_2 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "stdout" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 20;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    frame_4fb293be5988723048e8055c5d838fa9->m_frame.f_lineno = 20;
    tmp_return_value = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_decode, &PyTuple_GET_ITEM( const_tuple_str_digest_c075052d723d6707083e869a0e3659bb_tuple, 0 ) );

    if ( tmp_return_value == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 20;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_4fb293be5988723048e8055c5d838fa9 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_4fb293be5988723048e8055c5d838fa9 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_4fb293be5988723048e8055c5d838fa9 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_4fb293be5988723048e8055c5d838fa9, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_4fb293be5988723048e8055c5d838fa9->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_4fb293be5988723048e8055c5d838fa9, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_4fb293be5988723048e8055c5d838fa9,
        type_description_1,
        var_p,
        var_stdout,
        var_stderr
    );


    // Release cached frame.
    if ( frame_4fb293be5988723048e8055c5d838fa9 == cache_frame_4fb293be5988723048e8055c5d838fa9 )
    {
        Py_DECREF( frame_4fb293be5988723048e8055c5d838fa9 );
    }
    cache_frame_4fb293be5988723048e8055c5d838fa9 = NULL;

    assertFrameObject( frame_4fb293be5988723048e8055c5d838fa9 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$io$clipboard$clipboards$$$function_1_init_osx_clipboard$$$function_2_paste_osx );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( var_p );
    var_p = NULL;

    Py_XDECREF( var_stdout );
    var_stdout = NULL;

    Py_XDECREF( var_stderr );
    var_stderr = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( var_p );
    var_p = NULL;

    Py_XDECREF( var_stdout );
    var_stdout = NULL;

    Py_XDECREF( var_stderr );
    var_stderr = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$io$clipboard$clipboards$$$function_1_init_osx_clipboard$$$function_2_paste_osx );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$io$clipboard$clipboards$$$function_2_init_gtk_clipboard( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *var_gtk = PyCell_EMPTY();
    PyObject *var_copy_gtk = NULL;
    PyObject *var_paste_gtk = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_fromlist_name_1;
    PyObject *tmp_globals_name_1;
    PyObject *tmp_level_name_1;
    PyObject *tmp_locals_name_1;
    PyObject *tmp_name_name_1;
    PyObject *tmp_return_value;
    PyObject *tmp_tuple_element_1;
    static struct Nuitka_FrameObject *cache_frame_0360e4d1df6ae1edcb843c4520bf78bf = NULL;

    struct Nuitka_FrameObject *frame_0360e4d1df6ae1edcb843c4520bf78bf;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_0360e4d1df6ae1edcb843c4520bf78bf, codeobj_0360e4d1df6ae1edcb843c4520bf78bf, module_pandas$io$clipboard$clipboards, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_0360e4d1df6ae1edcb843c4520bf78bf = cache_frame_0360e4d1df6ae1edcb843c4520bf78bf;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_0360e4d1df6ae1edcb843c4520bf78bf );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_0360e4d1df6ae1edcb843c4520bf78bf ) == 2 ); // Frame stack

    // Framed code:
    tmp_name_name_1 = const_str_plain_gtk;
    tmp_globals_name_1 = (PyObject *)moduledict_pandas$io$clipboard$clipboards;
    tmp_locals_name_1 = Py_None;
    tmp_fromlist_name_1 = Py_None;
    tmp_level_name_1 = const_int_0;
    frame_0360e4d1df6ae1edcb843c4520bf78bf->m_frame.f_lineno = 26;
    tmp_assign_source_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
    if ( tmp_assign_source_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 26;
        type_description_1 = "coo";
        goto frame_exception_exit_1;
    }
    assert( PyCell_GET( var_gtk ) == NULL );
    PyCell_SET( var_gtk, tmp_assign_source_1 );


#if 0
    RESTORE_FRAME_EXCEPTION( frame_0360e4d1df6ae1edcb843c4520bf78bf );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0360e4d1df6ae1edcb843c4520bf78bf );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_0360e4d1df6ae1edcb843c4520bf78bf, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_0360e4d1df6ae1edcb843c4520bf78bf->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_0360e4d1df6ae1edcb843c4520bf78bf, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_0360e4d1df6ae1edcb843c4520bf78bf,
        type_description_1,
        var_gtk,
        var_copy_gtk,
        var_paste_gtk
    );


    // Release cached frame.
    if ( frame_0360e4d1df6ae1edcb843c4520bf78bf == cache_frame_0360e4d1df6ae1edcb843c4520bf78bf )
    {
        Py_DECREF( frame_0360e4d1df6ae1edcb843c4520bf78bf );
    }
    cache_frame_0360e4d1df6ae1edcb843c4520bf78bf = NULL;

    assertFrameObject( frame_0360e4d1df6ae1edcb843c4520bf78bf );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    tmp_assign_source_2 = MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_2_init_gtk_clipboard$$$function_1_copy_gtk( var_gtk );
    assert( var_copy_gtk == NULL );
    var_copy_gtk = tmp_assign_source_2;

    tmp_assign_source_3 = MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_2_init_gtk_clipboard$$$function_2_paste_gtk( var_gtk );
    assert( var_paste_gtk == NULL );
    var_paste_gtk = tmp_assign_source_3;

    tmp_tuple_element_1 = var_copy_gtk;

    CHECK_OBJECT( tmp_tuple_element_1 );
    tmp_return_value = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_return_value, 0, tmp_tuple_element_1 );
    tmp_tuple_element_1 = var_paste_gtk;

    CHECK_OBJECT( tmp_tuple_element_1 );
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_return_value, 1, tmp_tuple_element_1 );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$io$clipboard$clipboards$$$function_2_init_gtk_clipboard );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)var_gtk );
    Py_DECREF( var_gtk );
    var_gtk = NULL;

    Py_XDECREF( var_copy_gtk );
    var_copy_gtk = NULL;

    Py_XDECREF( var_paste_gtk );
    var_paste_gtk = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$io$clipboard$clipboards$$$function_2_init_gtk_clipboard );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$io$clipboard$clipboards$$$function_2_init_gtk_clipboard$$$function_1_copy_gtk( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_text = python_pars[ 0 ];
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_args_element_name_1;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_called_instance_1;
    PyObject *tmp_called_instance_2;
    PyObject *tmp_called_name_1;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    NUITKA_MAY_BE_UNUSED PyObject *tmp_unused;
    static struct Nuitka_FrameObject *cache_frame_bbe75b4ebd91ab9b61b71ddb6993e4fa = NULL;

    struct Nuitka_FrameObject *frame_bbe75b4ebd91ab9b61b71ddb6993e4fa;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_bbe75b4ebd91ab9b61b71ddb6993e4fa, codeobj_bbe75b4ebd91ab9b61b71ddb6993e4fa, module_pandas$io$clipboard$clipboards, sizeof(void *)+sizeof(void *) );
    frame_bbe75b4ebd91ab9b61b71ddb6993e4fa = cache_frame_bbe75b4ebd91ab9b61b71ddb6993e4fa;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_bbe75b4ebd91ab9b61b71ddb6993e4fa );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_bbe75b4ebd91ab9b61b71ddb6993e4fa ) == 2 ); // Frame stack

    // Framed code:
    if ( self->m_closure[0] == NULL )
    {
        tmp_called_instance_1 = NULL;
    }
    else
    {
        tmp_called_instance_1 = PyCell_GET( self->m_closure[0] );
    }

    if ( tmp_called_instance_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "gtk" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 30;
        type_description_1 = "oc";
        goto frame_exception_exit_1;
    }

    frame_bbe75b4ebd91ab9b61b71ddb6993e4fa->m_frame.f_lineno = 30;
    tmp_assign_source_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_Clipboard );
    if ( tmp_assign_source_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 30;
        type_description_1 = "oc";
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$io$clipboard$clipboards, (Nuitka_StringObject *)const_str_plain_cb, tmp_assign_source_1 );
    tmp_source_name_1 = GET_STRING_DICT_VALUE( moduledict_pandas$io$clipboard$clipboards, (Nuitka_StringObject *)const_str_plain_cb );

    if (unlikely( tmp_source_name_1 == NULL ))
    {
        tmp_source_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cb );
    }

    CHECK_OBJECT( tmp_source_name_1 );
    tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_set_text );
    if ( tmp_called_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 31;
        type_description_1 = "oc";
        goto frame_exception_exit_1;
    }
    tmp_args_element_name_1 = par_text;

    if ( tmp_args_element_name_1 == NULL )
    {
        Py_DECREF( tmp_called_name_1 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "text" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 31;
        type_description_1 = "oc";
        goto frame_exception_exit_1;
    }

    frame_bbe75b4ebd91ab9b61b71ddb6993e4fa->m_frame.f_lineno = 31;
    {
        PyObject *call_args[] = { tmp_args_element_name_1 };
        tmp_unused = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
    }

    Py_DECREF( tmp_called_name_1 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 31;
        type_description_1 = "oc";
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_unused );
    tmp_called_instance_2 = GET_STRING_DICT_VALUE( moduledict_pandas$io$clipboard$clipboards, (Nuitka_StringObject *)const_str_plain_cb );

    if (unlikely( tmp_called_instance_2 == NULL ))
    {
        tmp_called_instance_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cb );
    }

    if ( tmp_called_instance_2 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cb" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 32;
        type_description_1 = "oc";
        goto frame_exception_exit_1;
    }

    frame_bbe75b4ebd91ab9b61b71ddb6993e4fa->m_frame.f_lineno = 32;
    tmp_unused = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_store );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 32;
        type_description_1 = "oc";
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_unused );

#if 0
    RESTORE_FRAME_EXCEPTION( frame_bbe75b4ebd91ab9b61b71ddb6993e4fa );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_bbe75b4ebd91ab9b61b71ddb6993e4fa );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_bbe75b4ebd91ab9b61b71ddb6993e4fa, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_bbe75b4ebd91ab9b61b71ddb6993e4fa->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_bbe75b4ebd91ab9b61b71ddb6993e4fa, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_bbe75b4ebd91ab9b61b71ddb6993e4fa,
        type_description_1,
        par_text,
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_bbe75b4ebd91ab9b61b71ddb6993e4fa == cache_frame_bbe75b4ebd91ab9b61b71ddb6993e4fa )
    {
        Py_DECREF( frame_bbe75b4ebd91ab9b61b71ddb6993e4fa );
    }
    cache_frame_bbe75b4ebd91ab9b61b71ddb6993e4fa = NULL;

    assertFrameObject( frame_bbe75b4ebd91ab9b61b71ddb6993e4fa );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$io$clipboard$clipboards$$$function_2_init_gtk_clipboard$$$function_1_copy_gtk );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_text );
    par_text = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_text );
    par_text = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$io$clipboard$clipboards$$$function_2_init_gtk_clipboard$$$function_1_copy_gtk );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$io$clipboard$clipboards$$$function_2_init_gtk_clipboard$$$function_2_paste_gtk( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *var_clipboardContents = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_called_instance_1;
    PyObject *tmp_called_instance_2;
    PyObject *tmp_compare_left_1;
    PyObject *tmp_compare_right_1;
    bool tmp_is_1;
    PyObject *tmp_return_value;
    static struct Nuitka_FrameObject *cache_frame_46465757b3b8fd2109993fc577b505af = NULL;

    struct Nuitka_FrameObject *frame_46465757b3b8fd2109993fc577b505af;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_46465757b3b8fd2109993fc577b505af, codeobj_46465757b3b8fd2109993fc577b505af, module_pandas$io$clipboard$clipboards, sizeof(void *)+sizeof(void *) );
    frame_46465757b3b8fd2109993fc577b505af = cache_frame_46465757b3b8fd2109993fc577b505af;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_46465757b3b8fd2109993fc577b505af );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_46465757b3b8fd2109993fc577b505af ) == 2 ); // Frame stack

    // Framed code:
    if ( self->m_closure[0] == NULL )
    {
        tmp_called_instance_2 = NULL;
    }
    else
    {
        tmp_called_instance_2 = PyCell_GET( self->m_closure[0] );
    }

    if ( tmp_called_instance_2 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "gtk" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 35;
        type_description_1 = "oc";
        goto frame_exception_exit_1;
    }

    frame_46465757b3b8fd2109993fc577b505af->m_frame.f_lineno = 35;
    tmp_called_instance_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_Clipboard );
    if ( tmp_called_instance_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 35;
        type_description_1 = "oc";
        goto frame_exception_exit_1;
    }
    frame_46465757b3b8fd2109993fc577b505af->m_frame.f_lineno = 35;
    tmp_assign_source_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_wait_for_text );
    Py_DECREF( tmp_called_instance_1 );
    if ( tmp_assign_source_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 35;
        type_description_1 = "oc";
        goto frame_exception_exit_1;
    }
    assert( var_clipboardContents == NULL );
    var_clipboardContents = tmp_assign_source_1;

    tmp_compare_left_1 = var_clipboardContents;

    CHECK_OBJECT( tmp_compare_left_1 );
    tmp_compare_right_1 = Py_None;
    tmp_is_1 = ( tmp_compare_left_1 == tmp_compare_right_1 );
    if ( tmp_is_1 )
    {
        goto branch_yes_1;
    }
    else
    {
        goto branch_no_1;
    }
    branch_yes_1:;
    tmp_return_value = const_str_empty;
    Py_INCREF( tmp_return_value );
    goto frame_return_exit_1;
    goto branch_end_1;
    branch_no_1:;
    tmp_return_value = var_clipboardContents;

    if ( tmp_return_value == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "clipboardContents" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 40;
        type_description_1 = "oc";
        goto frame_exception_exit_1;
    }

    Py_INCREF( tmp_return_value );
    goto frame_return_exit_1;
    branch_end_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_46465757b3b8fd2109993fc577b505af );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_46465757b3b8fd2109993fc577b505af );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_46465757b3b8fd2109993fc577b505af );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_46465757b3b8fd2109993fc577b505af, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_46465757b3b8fd2109993fc577b505af->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_46465757b3b8fd2109993fc577b505af, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_46465757b3b8fd2109993fc577b505af,
        type_description_1,
        var_clipboardContents,
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_46465757b3b8fd2109993fc577b505af == cache_frame_46465757b3b8fd2109993fc577b505af )
    {
        Py_DECREF( frame_46465757b3b8fd2109993fc577b505af );
    }
    cache_frame_46465757b3b8fd2109993fc577b505af = NULL;

    assertFrameObject( frame_46465757b3b8fd2109993fc577b505af );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$io$clipboard$clipboards$$$function_2_init_gtk_clipboard$$$function_2_paste_gtk );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( var_clipboardContents );
    var_clipboardContents = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( var_clipboardContents );
    var_clipboardContents = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$io$clipboard$clipboards$$$function_2_init_gtk_clipboard$$$function_2_paste_gtk );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$io$clipboard$clipboards$$$function_3_init_qt_clipboard( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *var_QApplication = NULL;
    struct Nuitka_CellObject *var_app = PyCell_EMPTY();
    PyObject *var_copy_qt = NULL;
    PyObject *var_paste_qt = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    PyObject *exception_preserved_type_2;
    PyObject *exception_preserved_value_2;
    PyTracebackObject *exception_preserved_tb_2;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_assign_source_5;
    PyObject *tmp_assign_source_6;
    PyObject *tmp_assign_source_7;
    PyObject *tmp_call_arg_element_1;
    PyObject *tmp_called_instance_1;
    PyObject *tmp_called_name_1;
    PyObject *tmp_compare_left_1;
    PyObject *tmp_compare_left_2;
    PyObject *tmp_compare_left_3;
    PyObject *tmp_compare_right_1;
    PyObject *tmp_compare_right_2;
    PyObject *tmp_compare_right_3;
    int tmp_exc_match_exception_match_1;
    int tmp_exc_match_exception_match_2;
    PyObject *tmp_fromlist_name_1;
    PyObject *tmp_fromlist_name_2;
    PyObject *tmp_fromlist_name_3;
    PyObject *tmp_globals_name_1;
    PyObject *tmp_globals_name_2;
    PyObject *tmp_globals_name_3;
    PyObject *tmp_import_name_from_1;
    PyObject *tmp_import_name_from_2;
    PyObject *tmp_import_name_from_3;
    bool tmp_is_1;
    PyObject *tmp_level_name_1;
    PyObject *tmp_level_name_2;
    PyObject *tmp_level_name_3;
    PyObject *tmp_locals_name_1;
    PyObject *tmp_locals_name_2;
    PyObject *tmp_locals_name_3;
    PyObject *tmp_name_name_1;
    PyObject *tmp_name_name_2;
    PyObject *tmp_name_name_3;
    bool tmp_result;
    PyObject *tmp_return_value;
    PyObject *tmp_tuple_element_1;
    static struct Nuitka_FrameObject *cache_frame_f5a793ca030f17ceec59cb6cf88afc11 = NULL;

    struct Nuitka_FrameObject *frame_f5a793ca030f17ceec59cb6cf88afc11;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_f5a793ca030f17ceec59cb6cf88afc11, codeobj_f5a793ca030f17ceec59cb6cf88afc11, module_pandas$io$clipboard$clipboards, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_f5a793ca030f17ceec59cb6cf88afc11 = cache_frame_f5a793ca030f17ceec59cb6cf88afc11;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_f5a793ca030f17ceec59cb6cf88afc11 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_f5a793ca030f17ceec59cb6cf88afc11 ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    tmp_name_name_1 = const_str_digest_aad9db65811d431e3670ce5a949fca3d;
    tmp_globals_name_1 = (PyObject *)moduledict_pandas$io$clipboard$clipboards;
    tmp_locals_name_1 = Py_None;
    tmp_fromlist_name_1 = const_tuple_str_plain_QApplication_tuple;
    tmp_level_name_1 = const_int_0;
    frame_f5a793ca030f17ceec59cb6cf88afc11->m_frame.f_lineno = 50;
    tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
    if ( tmp_import_name_from_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 50;
        type_description_1 = "ocoo";
        goto try_except_handler_2;
    }
    tmp_assign_source_1 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_QApplication );
    Py_DECREF( tmp_import_name_from_1 );
    if ( tmp_assign_source_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 50;
        type_description_1 = "ocoo";
        goto try_except_handler_2;
    }
    assert( var_QApplication == NULL );
    var_QApplication = tmp_assign_source_1;

    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_2 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_2 );
    exception_preserved_value_2 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_2 );
    exception_preserved_tb_2 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_2 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_f5a793ca030f17ceec59cb6cf88afc11, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_f5a793ca030f17ceec59cb6cf88afc11, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    // Tried code:
    tmp_compare_left_1 = EXC_TYPE(PyThreadState_GET());
    tmp_compare_right_1 = PyExc_ImportError;
    tmp_exc_match_exception_match_1 = EXCEPTION_MATCH_BOOL( tmp_compare_left_1, tmp_compare_right_1 );
    if ( tmp_exc_match_exception_match_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 51;
        type_description_1 = "ocoo";
        goto try_except_handler_3;
    }
    if ( tmp_exc_match_exception_match_1 == 1 )
    {
        goto branch_yes_1;
    }
    else
    {
        goto branch_no_1;
    }
    branch_yes_1:;
    // Tried code:
    tmp_name_name_2 = const_str_digest_a88e8a7bd4f9ed41f0331a9209a4c70a;
    tmp_globals_name_2 = (PyObject *)moduledict_pandas$io$clipboard$clipboards;
    tmp_locals_name_2 = Py_None;
    tmp_fromlist_name_2 = const_tuple_str_plain_QApplication_tuple;
    tmp_level_name_2 = const_int_0;
    frame_f5a793ca030f17ceec59cb6cf88afc11->m_frame.f_lineno = 53;
    tmp_import_name_from_2 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
    if ( tmp_import_name_from_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 53;
        type_description_1 = "ocoo";
        goto try_except_handler_4;
    }
    tmp_assign_source_2 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_QApplication );
    Py_DECREF( tmp_import_name_from_2 );
    if ( tmp_assign_source_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 53;
        type_description_1 = "ocoo";
        goto try_except_handler_4;
    }
    assert( var_QApplication == NULL );
    var_QApplication = tmp_assign_source_2;

    goto try_end_2;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_2 == NULL )
    {
        exception_keeper_tb_2 = MAKE_TRACEBACK( frame_f5a793ca030f17ceec59cb6cf88afc11, exception_keeper_lineno_2 );
    }
    else if ( exception_keeper_lineno_2 != 0 )
    {
        exception_keeper_tb_2 = ADD_TRACEBACK( exception_keeper_tb_2, frame_f5a793ca030f17ceec59cb6cf88afc11, exception_keeper_lineno_2 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_2, &exception_keeper_value_2, &exception_keeper_tb_2 );
    PyException_SetTraceback( exception_keeper_value_2, (PyObject *)exception_keeper_tb_2 );
    PUBLISH_EXCEPTION( &exception_keeper_type_2, &exception_keeper_value_2, &exception_keeper_tb_2 );
    // Tried code:
    tmp_compare_left_2 = EXC_TYPE(PyThreadState_GET());
    tmp_compare_right_2 = PyExc_ImportError;
    tmp_exc_match_exception_match_2 = EXCEPTION_MATCH_BOOL( tmp_compare_left_2, tmp_compare_right_2 );
    if ( tmp_exc_match_exception_match_2 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 54;
        type_description_1 = "ocoo";
        goto try_except_handler_5;
    }
    if ( tmp_exc_match_exception_match_2 == 1 )
    {
        goto branch_yes_2;
    }
    else
    {
        goto branch_no_2;
    }
    branch_yes_2:;
    tmp_name_name_3 = const_str_digest_91d3fabe5e3af50ef0546b25193cf397;
    tmp_globals_name_3 = (PyObject *)moduledict_pandas$io$clipboard$clipboards;
    tmp_locals_name_3 = Py_None;
    tmp_fromlist_name_3 = const_tuple_str_plain_QApplication_tuple;
    tmp_level_name_3 = const_int_0;
    frame_f5a793ca030f17ceec59cb6cf88afc11->m_frame.f_lineno = 55;
    tmp_import_name_from_3 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
    if ( tmp_import_name_from_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 55;
        type_description_1 = "ocoo";
        goto try_except_handler_5;
    }
    tmp_assign_source_3 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_QApplication );
    Py_DECREF( tmp_import_name_from_3 );
    if ( tmp_assign_source_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 55;
        type_description_1 = "ocoo";
        goto try_except_handler_5;
    }
    assert( var_QApplication == NULL );
    var_QApplication = tmp_assign_source_3;

    goto branch_end_2;
    branch_no_2:;
    tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (unlikely( tmp_result == false ))
    {
        exception_lineno = 52;
    }

    if (exception_tb && exception_tb->tb_frame == &frame_f5a793ca030f17ceec59cb6cf88afc11->m_frame) frame_f5a793ca030f17ceec59cb6cf88afc11->m_frame.f_lineno = exception_tb->tb_lineno;
    type_description_1 = "ocoo";
    goto try_except_handler_5;
    branch_end_2:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto try_except_handler_3;
    // End of try:
    try_end_3:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto try_end_2;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$io$clipboard$clipboards$$$function_3_init_qt_clipboard );
    return NULL;
    // End of try:
    try_end_2:;
    goto branch_end_1;
    branch_no_1:;
    tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (unlikely( tmp_result == false ))
    {
        exception_lineno = 49;
    }

    if (exception_tb && exception_tb->tb_frame == &frame_f5a793ca030f17ceec59cb6cf88afc11->m_frame) frame_f5a793ca030f17ceec59cb6cf88afc11->m_frame.f_lineno = exception_tb->tb_lineno;
    type_description_1 = "ocoo";
    goto try_except_handler_3;
    branch_end_1:;
    goto try_end_4;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_2, exception_preserved_value_2, exception_preserved_tb_2 );
    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto frame_exception_exit_1;
    // End of try:
    try_end_4:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_2, exception_preserved_value_2, exception_preserved_tb_2 );
    goto try_end_1;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$io$clipboard$clipboards$$$function_3_init_qt_clipboard );
    return NULL;
    // End of try:
    try_end_1:;
    tmp_called_instance_1 = var_QApplication;

    if ( tmp_called_instance_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "QApplication" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 57;
        type_description_1 = "ocoo";
        goto frame_exception_exit_1;
    }

    frame_f5a793ca030f17ceec59cb6cf88afc11->m_frame.f_lineno = 57;
    tmp_assign_source_4 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_instance );
    if ( tmp_assign_source_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 57;
        type_description_1 = "ocoo";
        goto frame_exception_exit_1;
    }
    assert( PyCell_GET( var_app ) == NULL );
    PyCell_SET( var_app, tmp_assign_source_4 );

    if ( var_app == NULL )
    {
        tmp_compare_left_3 = NULL;
    }
    else
    {
        tmp_compare_left_3 = PyCell_GET( var_app );
    }

    CHECK_OBJECT( tmp_compare_left_3 );
    tmp_compare_right_3 = Py_None;
    tmp_is_1 = ( tmp_compare_left_3 == tmp_compare_right_3 );
    if ( tmp_is_1 )
    {
        goto branch_yes_3;
    }
    else
    {
        goto branch_no_3;
    }
    branch_yes_3:;
    tmp_called_name_1 = var_QApplication;

    if ( tmp_called_name_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "QApplication" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 59;
        type_description_1 = "ocoo";
        goto frame_exception_exit_1;
    }

    tmp_call_arg_element_1 = PyList_New( 0 );
    frame_f5a793ca030f17ceec59cb6cf88afc11->m_frame.f_lineno = 59;
    {
        PyObject *call_args[] = { tmp_call_arg_element_1 };
        tmp_assign_source_5 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
    }

    Py_DECREF( tmp_call_arg_element_1 );
    if ( tmp_assign_source_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 59;
        type_description_1 = "ocoo";
        goto frame_exception_exit_1;
    }
    {
        PyObject *old = PyCell_GET( var_app );
        PyCell_SET( var_app, tmp_assign_source_5 );
        Py_XDECREF( old );
    }

    branch_no_3:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f5a793ca030f17ceec59cb6cf88afc11 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f5a793ca030f17ceec59cb6cf88afc11 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_f5a793ca030f17ceec59cb6cf88afc11, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_f5a793ca030f17ceec59cb6cf88afc11->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_f5a793ca030f17ceec59cb6cf88afc11, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_f5a793ca030f17ceec59cb6cf88afc11,
        type_description_1,
        var_QApplication,
        var_app,
        var_copy_qt,
        var_paste_qt
    );


    // Release cached frame.
    if ( frame_f5a793ca030f17ceec59cb6cf88afc11 == cache_frame_f5a793ca030f17ceec59cb6cf88afc11 )
    {
        Py_DECREF( frame_f5a793ca030f17ceec59cb6cf88afc11 );
    }
    cache_frame_f5a793ca030f17ceec59cb6cf88afc11 = NULL;

    assertFrameObject( frame_f5a793ca030f17ceec59cb6cf88afc11 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    tmp_assign_source_6 = MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_3_init_qt_clipboard$$$function_1_copy_qt( var_app );
    assert( var_copy_qt == NULL );
    var_copy_qt = tmp_assign_source_6;

    tmp_assign_source_7 = MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_3_init_qt_clipboard$$$function_2_paste_qt( var_app );
    assert( var_paste_qt == NULL );
    var_paste_qt = tmp_assign_source_7;

    tmp_tuple_element_1 = var_copy_qt;

    CHECK_OBJECT( tmp_tuple_element_1 );
    tmp_return_value = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_return_value, 0, tmp_tuple_element_1 );
    tmp_tuple_element_1 = var_paste_qt;

    CHECK_OBJECT( tmp_tuple_element_1 );
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_return_value, 1, tmp_tuple_element_1 );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$io$clipboard$clipboards$$$function_3_init_qt_clipboard );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( var_QApplication );
    var_QApplication = NULL;

    CHECK_OBJECT( (PyObject *)var_app );
    Py_DECREF( var_app );
    var_app = NULL;

    Py_XDECREF( var_copy_qt );
    var_copy_qt = NULL;

    Py_XDECREF( var_paste_qt );
    var_paste_qt = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( var_QApplication );
    var_QApplication = NULL;

    CHECK_OBJECT( (PyObject *)var_app );
    Py_DECREF( var_app );
    var_app = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$io$clipboard$clipboards$$$function_3_init_qt_clipboard );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$io$clipboard$clipboards$$$function_3_init_qt_clipboard$$$function_1_copy_qt( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_text = python_pars[ 0 ];
    PyObject *var_cb = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_args_element_name_1;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_called_instance_1;
    PyObject *tmp_called_name_1;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    NUITKA_MAY_BE_UNUSED PyObject *tmp_unused;
    static struct Nuitka_FrameObject *cache_frame_40eba54f9f7106c02033be5247abbf8c = NULL;

    struct Nuitka_FrameObject *frame_40eba54f9f7106c02033be5247abbf8c;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_40eba54f9f7106c02033be5247abbf8c, codeobj_40eba54f9f7106c02033be5247abbf8c, module_pandas$io$clipboard$clipboards, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_40eba54f9f7106c02033be5247abbf8c = cache_frame_40eba54f9f7106c02033be5247abbf8c;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_40eba54f9f7106c02033be5247abbf8c );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_40eba54f9f7106c02033be5247abbf8c ) == 2 ); // Frame stack

    // Framed code:
    if ( self->m_closure[0] == NULL )
    {
        tmp_called_instance_1 = NULL;
    }
    else
    {
        tmp_called_instance_1 = PyCell_GET( self->m_closure[0] );
    }

    if ( tmp_called_instance_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "app" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 62;
        type_description_1 = "ooc";
        goto frame_exception_exit_1;
    }

    frame_40eba54f9f7106c02033be5247abbf8c->m_frame.f_lineno = 62;
    tmp_assign_source_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_clipboard );
    if ( tmp_assign_source_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 62;
        type_description_1 = "ooc";
        goto frame_exception_exit_1;
    }
    assert( var_cb == NULL );
    var_cb = tmp_assign_source_1;

    tmp_source_name_1 = var_cb;

    CHECK_OBJECT( tmp_source_name_1 );
    tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_setText );
    if ( tmp_called_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 63;
        type_description_1 = "ooc";
        goto frame_exception_exit_1;
    }
    tmp_args_element_name_1 = par_text;

    if ( tmp_args_element_name_1 == NULL )
    {
        Py_DECREF( tmp_called_name_1 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "text" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 63;
        type_description_1 = "ooc";
        goto frame_exception_exit_1;
    }

    frame_40eba54f9f7106c02033be5247abbf8c->m_frame.f_lineno = 63;
    {
        PyObject *call_args[] = { tmp_args_element_name_1 };
        tmp_unused = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
    }

    Py_DECREF( tmp_called_name_1 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 63;
        type_description_1 = "ooc";
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_unused );

#if 0
    RESTORE_FRAME_EXCEPTION( frame_40eba54f9f7106c02033be5247abbf8c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_40eba54f9f7106c02033be5247abbf8c );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_40eba54f9f7106c02033be5247abbf8c, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_40eba54f9f7106c02033be5247abbf8c->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_40eba54f9f7106c02033be5247abbf8c, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_40eba54f9f7106c02033be5247abbf8c,
        type_description_1,
        par_text,
        var_cb,
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_40eba54f9f7106c02033be5247abbf8c == cache_frame_40eba54f9f7106c02033be5247abbf8c )
    {
        Py_DECREF( frame_40eba54f9f7106c02033be5247abbf8c );
    }
    cache_frame_40eba54f9f7106c02033be5247abbf8c = NULL;

    assertFrameObject( frame_40eba54f9f7106c02033be5247abbf8c );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$io$clipboard$clipboards$$$function_3_init_qt_clipboard$$$function_1_copy_qt );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_text );
    par_text = NULL;

    Py_XDECREF( var_cb );
    var_cb = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_text );
    par_text = NULL;

    Py_XDECREF( var_cb );
    var_cb = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$io$clipboard$clipboards$$$function_3_init_qt_clipboard$$$function_1_copy_qt );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$io$clipboard$clipboards$$$function_3_init_qt_clipboard$$$function_2_paste_qt( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *var_cb = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_args_element_name_1;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_called_instance_1;
    PyObject *tmp_called_instance_2;
    PyObject *tmp_called_name_1;
    PyObject *tmp_return_value;
    static struct Nuitka_FrameObject *cache_frame_f305a7321124f97a9f33318e46ca5996 = NULL;

    struct Nuitka_FrameObject *frame_f305a7321124f97a9f33318e46ca5996;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_f305a7321124f97a9f33318e46ca5996, codeobj_f305a7321124f97a9f33318e46ca5996, module_pandas$io$clipboard$clipboards, sizeof(void *)+sizeof(void *) );
    frame_f305a7321124f97a9f33318e46ca5996 = cache_frame_f305a7321124f97a9f33318e46ca5996;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_f305a7321124f97a9f33318e46ca5996 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_f305a7321124f97a9f33318e46ca5996 ) == 2 ); // Frame stack

    // Framed code:
    if ( self->m_closure[0] == NULL )
    {
        tmp_called_instance_1 = NULL;
    }
    else
    {
        tmp_called_instance_1 = PyCell_GET( self->m_closure[0] );
    }

    if ( tmp_called_instance_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "app" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 66;
        type_description_1 = "oc";
        goto frame_exception_exit_1;
    }

    frame_f305a7321124f97a9f33318e46ca5996->m_frame.f_lineno = 66;
    tmp_assign_source_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_clipboard );
    if ( tmp_assign_source_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 66;
        type_description_1 = "oc";
        goto frame_exception_exit_1;
    }
    assert( var_cb == NULL );
    var_cb = tmp_assign_source_1;

    tmp_called_name_1 = GET_STRING_DICT_VALUE( moduledict_pandas$io$clipboard$clipboards, (Nuitka_StringObject *)const_str_plain_text_type );

    if (unlikely( tmp_called_name_1 == NULL ))
    {
        tmp_called_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_text_type );
    }

    if ( tmp_called_name_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "text_type" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 67;
        type_description_1 = "oc";
        goto frame_exception_exit_1;
    }

    tmp_called_instance_2 = var_cb;

    CHECK_OBJECT( tmp_called_instance_2 );
    frame_f305a7321124f97a9f33318e46ca5996->m_frame.f_lineno = 67;
    tmp_args_element_name_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_text );
    if ( tmp_args_element_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 67;
        type_description_1 = "oc";
        goto frame_exception_exit_1;
    }
    frame_f305a7321124f97a9f33318e46ca5996->m_frame.f_lineno = 67;
    {
        PyObject *call_args[] = { tmp_args_element_name_1 };
        tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
    }

    Py_DECREF( tmp_args_element_name_1 );
    if ( tmp_return_value == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 67;
        type_description_1 = "oc";
        goto frame_exception_exit_1;
    }
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f305a7321124f97a9f33318e46ca5996 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_f305a7321124f97a9f33318e46ca5996 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f305a7321124f97a9f33318e46ca5996 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_f305a7321124f97a9f33318e46ca5996, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_f305a7321124f97a9f33318e46ca5996->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_f305a7321124f97a9f33318e46ca5996, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_f305a7321124f97a9f33318e46ca5996,
        type_description_1,
        var_cb,
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_f305a7321124f97a9f33318e46ca5996 == cache_frame_f305a7321124f97a9f33318e46ca5996 )
    {
        Py_DECREF( frame_f305a7321124f97a9f33318e46ca5996 );
    }
    cache_frame_f305a7321124f97a9f33318e46ca5996 = NULL;

    assertFrameObject( frame_f305a7321124f97a9f33318e46ca5996 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$io$clipboard$clipboards$$$function_3_init_qt_clipboard$$$function_2_paste_qt );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( var_cb );
    var_cb = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( var_cb );
    var_cb = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$io$clipboard$clipboards$$$function_3_init_qt_clipboard$$$function_2_paste_qt );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$io$clipboard$clipboards$$$function_4_init_xclip_clipboard( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *var_copy_xclip = NULL;
    PyObject *var_paste_xclip = NULL;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_return_value;
    PyObject *tmp_tuple_element_1;
    tmp_return_value = NULL;

    // Actual function code.
    tmp_assign_source_1 = MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_4_init_xclip_clipboard$$$function_1_copy_xclip(  );
    assert( var_copy_xclip == NULL );
    var_copy_xclip = tmp_assign_source_1;

    tmp_assign_source_2 = MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_4_init_xclip_clipboard$$$function_2_paste_xclip(  );
    assert( var_paste_xclip == NULL );
    var_paste_xclip = tmp_assign_source_2;

    // Tried code:
    tmp_tuple_element_1 = var_copy_xclip;

    CHECK_OBJECT( tmp_tuple_element_1 );
    tmp_return_value = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_return_value, 0, tmp_tuple_element_1 );
    tmp_tuple_element_1 = var_paste_xclip;

    CHECK_OBJECT( tmp_tuple_element_1 );
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_return_value, 1, tmp_tuple_element_1 );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$io$clipboard$clipboards$$$function_4_init_xclip_clipboard );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)var_copy_xclip );
    Py_DECREF( var_copy_xclip );
    var_copy_xclip = NULL;

    Py_XDECREF( var_paste_xclip );
    var_paste_xclip = NULL;

    goto function_return_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$io$clipboard$clipboards$$$function_4_init_xclip_clipboard );
    return NULL;

function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$io$clipboard$clipboards$$$function_4_init_xclip_clipboard$$$function_1_copy_xclip( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_text = python_pars[ 0 ];
    PyObject *var_p = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_args_name_1;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_called_instance_1;
    PyObject *tmp_called_name_1;
    PyObject *tmp_called_name_2;
    PyObject *tmp_dict_key_1;
    PyObject *tmp_dict_key_2;
    PyObject *tmp_dict_key_3;
    PyObject *tmp_dict_value_1;
    PyObject *tmp_dict_value_2;
    PyObject *tmp_dict_value_3;
    PyObject *tmp_kw_name_1;
    PyObject *tmp_kw_name_2;
    int tmp_res;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    PyObject *tmp_source_name_2;
    PyObject *tmp_source_name_3;
    NUITKA_MAY_BE_UNUSED PyObject *tmp_unused;
    static struct Nuitka_FrameObject *cache_frame_1692a6eebf291b9f98c65b665e9ae3e0 = NULL;

    struct Nuitka_FrameObject *frame_1692a6eebf291b9f98c65b665e9ae3e0;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_1692a6eebf291b9f98c65b665e9ae3e0, codeobj_1692a6eebf291b9f98c65b665e9ae3e0, module_pandas$io$clipboard$clipboards, sizeof(void *)+sizeof(void *) );
    frame_1692a6eebf291b9f98c65b665e9ae3e0 = cache_frame_1692a6eebf291b9f98c65b665e9ae3e0;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_1692a6eebf291b9f98c65b665e9ae3e0 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_1692a6eebf291b9f98c65b665e9ae3e0 ) == 2 ); // Frame stack

    // Framed code:
    tmp_source_name_1 = GET_STRING_DICT_VALUE( moduledict_pandas$io$clipboard$clipboards, (Nuitka_StringObject *)const_str_plain_subprocess );

    if (unlikely( tmp_source_name_1 == NULL ))
    {
        tmp_source_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_subprocess );
    }

    if ( tmp_source_name_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "subprocess" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 74;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }

    tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_Popen );
    if ( tmp_called_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 74;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }
    tmp_args_name_1 = DEEP_COPY( const_tuple_list_80c22030d7e9874daf8196134c6cac83_list_tuple );
    tmp_dict_key_1 = const_str_plain_stdin;
    tmp_source_name_2 = GET_STRING_DICT_VALUE( moduledict_pandas$io$clipboard$clipboards, (Nuitka_StringObject *)const_str_plain_subprocess );

    if (unlikely( tmp_source_name_2 == NULL ))
    {
        tmp_source_name_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_subprocess );
    }

    if ( tmp_source_name_2 == NULL )
    {
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_name_1 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "subprocess" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 75;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }

    tmp_dict_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_PIPE );
    if ( tmp_dict_value_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_name_1 );

        exception_lineno = 75;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }
    tmp_kw_name_1 = _PyDict_NewPresized( 2 );
    tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
    Py_DECREF( tmp_dict_value_1 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_2 = const_str_plain_close_fds;
    tmp_dict_value_2 = Py_True;
    tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
    assert( !(tmp_res != 0) );
    frame_1692a6eebf291b9f98c65b665e9ae3e0->m_frame.f_lineno = 74;
    tmp_assign_source_1 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
    Py_DECREF( tmp_called_name_1 );
    Py_DECREF( tmp_args_name_1 );
    Py_DECREF( tmp_kw_name_1 );
    if ( tmp_assign_source_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 74;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }
    assert( var_p == NULL );
    var_p = tmp_assign_source_1;

    tmp_source_name_3 = var_p;

    CHECK_OBJECT( tmp_source_name_3 );
    tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_communicate );
    if ( tmp_called_name_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 76;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }
    tmp_dict_key_3 = const_str_plain_input;
    tmp_called_instance_1 = par_text;

    if ( tmp_called_instance_1 == NULL )
    {
        Py_DECREF( tmp_called_name_2 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "text" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 76;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }

    frame_1692a6eebf291b9f98c65b665e9ae3e0->m_frame.f_lineno = 76;
    tmp_dict_value_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_encode, &PyTuple_GET_ITEM( const_tuple_str_digest_c075052d723d6707083e869a0e3659bb_tuple, 0 ) );

    if ( tmp_dict_value_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_2 );

        exception_lineno = 76;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }
    tmp_kw_name_2 = _PyDict_NewPresized( 1 );
    tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_3, tmp_dict_value_3 );
    Py_DECREF( tmp_dict_value_3 );
    assert( !(tmp_res != 0) );
    frame_1692a6eebf291b9f98c65b665e9ae3e0->m_frame.f_lineno = 76;
    tmp_unused = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_2, tmp_kw_name_2 );
    Py_DECREF( tmp_called_name_2 );
    Py_DECREF( tmp_kw_name_2 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 76;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_unused );

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1692a6eebf291b9f98c65b665e9ae3e0 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1692a6eebf291b9f98c65b665e9ae3e0 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_1692a6eebf291b9f98c65b665e9ae3e0, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_1692a6eebf291b9f98c65b665e9ae3e0->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_1692a6eebf291b9f98c65b665e9ae3e0, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_1692a6eebf291b9f98c65b665e9ae3e0,
        type_description_1,
        par_text,
        var_p
    );


    // Release cached frame.
    if ( frame_1692a6eebf291b9f98c65b665e9ae3e0 == cache_frame_1692a6eebf291b9f98c65b665e9ae3e0 )
    {
        Py_DECREF( frame_1692a6eebf291b9f98c65b665e9ae3e0 );
    }
    cache_frame_1692a6eebf291b9f98c65b665e9ae3e0 = NULL;

    assertFrameObject( frame_1692a6eebf291b9f98c65b665e9ae3e0 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$io$clipboard$clipboards$$$function_4_init_xclip_clipboard$$$function_1_copy_xclip );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_text );
    par_text = NULL;

    Py_XDECREF( var_p );
    var_p = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_text );
    par_text = NULL;

    Py_XDECREF( var_p );
    var_p = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$io$clipboard$clipboards$$$function_4_init_xclip_clipboard$$$function_1_copy_xclip );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$io$clipboard$clipboards$$$function_4_init_xclip_clipboard$$$function_2_paste_xclip( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *var_p = NULL;
    PyObject *var_stdout = NULL;
    PyObject *var_stderr = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *tmp_args_name_1;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_assign_source_5;
    PyObject *tmp_assign_source_6;
    PyObject *tmp_called_instance_1;
    PyObject *tmp_called_instance_2;
    PyObject *tmp_called_name_1;
    PyObject *tmp_dict_key_1;
    PyObject *tmp_dict_key_2;
    PyObject *tmp_dict_value_1;
    PyObject *tmp_dict_value_2;
    PyObject *tmp_iter_arg_1;
    PyObject *tmp_iterator_attempt;
    PyObject *tmp_iterator_name_1;
    PyObject *tmp_kw_name_1;
    int tmp_res;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    PyObject *tmp_source_name_2;
    PyObject *tmp_unpack_1;
    PyObject *tmp_unpack_2;
    static struct Nuitka_FrameObject *cache_frame_de0bad81b2cc52a2e72b9f4e0d8d0cb6 = NULL;

    struct Nuitka_FrameObject *frame_de0bad81b2cc52a2e72b9f4e0d8d0cb6;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_de0bad81b2cc52a2e72b9f4e0d8d0cb6, codeobj_de0bad81b2cc52a2e72b9f4e0d8d0cb6, module_pandas$io$clipboard$clipboards, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_de0bad81b2cc52a2e72b9f4e0d8d0cb6 = cache_frame_de0bad81b2cc52a2e72b9f4e0d8d0cb6;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_de0bad81b2cc52a2e72b9f4e0d8d0cb6 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_de0bad81b2cc52a2e72b9f4e0d8d0cb6 ) == 2 ); // Frame stack

    // Framed code:
    tmp_source_name_1 = GET_STRING_DICT_VALUE( moduledict_pandas$io$clipboard$clipboards, (Nuitka_StringObject *)const_str_plain_subprocess );

    if (unlikely( tmp_source_name_1 == NULL ))
    {
        tmp_source_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_subprocess );
    }

    if ( tmp_source_name_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "subprocess" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 79;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_Popen );
    if ( tmp_called_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 79;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    tmp_args_name_1 = DEEP_COPY( const_tuple_list_e4d6a0388a09ce944591175f0308d4e0_list_tuple );
    tmp_dict_key_1 = const_str_plain_stdout;
    tmp_source_name_2 = GET_STRING_DICT_VALUE( moduledict_pandas$io$clipboard$clipboards, (Nuitka_StringObject *)const_str_plain_subprocess );

    if (unlikely( tmp_source_name_2 == NULL ))
    {
        tmp_source_name_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_subprocess );
    }

    if ( tmp_source_name_2 == NULL )
    {
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_name_1 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "subprocess" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 80;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    tmp_dict_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_PIPE );
    if ( tmp_dict_value_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_name_1 );

        exception_lineno = 80;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    tmp_kw_name_1 = _PyDict_NewPresized( 2 );
    tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
    Py_DECREF( tmp_dict_value_1 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_2 = const_str_plain_close_fds;
    tmp_dict_value_2 = Py_True;
    tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
    assert( !(tmp_res != 0) );
    frame_de0bad81b2cc52a2e72b9f4e0d8d0cb6->m_frame.f_lineno = 79;
    tmp_assign_source_1 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
    Py_DECREF( tmp_called_name_1 );
    Py_DECREF( tmp_args_name_1 );
    Py_DECREF( tmp_kw_name_1 );
    if ( tmp_assign_source_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 79;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    assert( var_p == NULL );
    var_p = tmp_assign_source_1;

    // Tried code:
    tmp_called_instance_1 = var_p;

    CHECK_OBJECT( tmp_called_instance_1 );
    frame_de0bad81b2cc52a2e72b9f4e0d8d0cb6->m_frame.f_lineno = 81;
    tmp_iter_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_communicate );
    if ( tmp_iter_arg_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 81;
        type_description_1 = "ooo";
        goto try_except_handler_2;
    }
    tmp_assign_source_2 = MAKE_ITERATOR( tmp_iter_arg_1 );
    Py_DECREF( tmp_iter_arg_1 );
    if ( tmp_assign_source_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 81;
        type_description_1 = "ooo";
        goto try_except_handler_2;
    }
    assert( tmp_tuple_unpack_1__source_iter == NULL );
    tmp_tuple_unpack_1__source_iter = tmp_assign_source_2;

    // Tried code:
    tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;

    CHECK_OBJECT( tmp_unpack_1 );
    tmp_assign_source_3 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
    if ( tmp_assign_source_3 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        type_description_1 = "ooo";
        exception_lineno = 81;
        goto try_except_handler_3;
    }
    assert( tmp_tuple_unpack_1__element_1 == NULL );
    tmp_tuple_unpack_1__element_1 = tmp_assign_source_3;

    tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;

    CHECK_OBJECT( tmp_unpack_2 );
    tmp_assign_source_4 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
    if ( tmp_assign_source_4 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        type_description_1 = "ooo";
        exception_lineno = 81;
        goto try_except_handler_3;
    }
    assert( tmp_tuple_unpack_1__element_2 == NULL );
    tmp_tuple_unpack_1__element_2 = tmp_assign_source_4;

    tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;

    CHECK_OBJECT( tmp_iterator_name_1 );
    // Check if iterator has left-over elements.
    CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

    tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

    if (likely( tmp_iterator_attempt == NULL ))
    {
        PyObject *error = GET_ERROR_OCCURRED();

        if ( error != NULL )
        {
            if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
            {
                CLEAR_ERROR_OCCURRED();
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                type_description_1 = "ooo";
                exception_lineno = 81;
                goto try_except_handler_3;
            }
        }
    }
    else
    {
        Py_DECREF( tmp_iterator_attempt );

        // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
        PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
        PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

        type_description_1 = "ooo";
        exception_lineno = 81;
        goto try_except_handler_3;
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_2;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    tmp_assign_source_5 = tmp_tuple_unpack_1__element_1;

    CHECK_OBJECT( tmp_assign_source_5 );
    assert( var_stdout == NULL );
    Py_INCREF( tmp_assign_source_5 );
    var_stdout = tmp_assign_source_5;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    tmp_assign_source_6 = tmp_tuple_unpack_1__element_2;

    CHECK_OBJECT( tmp_assign_source_6 );
    assert( var_stderr == NULL );
    Py_INCREF( tmp_assign_source_6 );
    var_stderr = tmp_assign_source_6;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    tmp_called_instance_2 = var_stdout;

    if ( tmp_called_instance_2 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "stdout" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 82;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    frame_de0bad81b2cc52a2e72b9f4e0d8d0cb6->m_frame.f_lineno = 82;
    tmp_return_value = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_decode, &PyTuple_GET_ITEM( const_tuple_str_digest_c075052d723d6707083e869a0e3659bb_tuple, 0 ) );

    if ( tmp_return_value == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 82;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_de0bad81b2cc52a2e72b9f4e0d8d0cb6 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_de0bad81b2cc52a2e72b9f4e0d8d0cb6 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_de0bad81b2cc52a2e72b9f4e0d8d0cb6 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_de0bad81b2cc52a2e72b9f4e0d8d0cb6, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_de0bad81b2cc52a2e72b9f4e0d8d0cb6->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_de0bad81b2cc52a2e72b9f4e0d8d0cb6, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_de0bad81b2cc52a2e72b9f4e0d8d0cb6,
        type_description_1,
        var_p,
        var_stdout,
        var_stderr
    );


    // Release cached frame.
    if ( frame_de0bad81b2cc52a2e72b9f4e0d8d0cb6 == cache_frame_de0bad81b2cc52a2e72b9f4e0d8d0cb6 )
    {
        Py_DECREF( frame_de0bad81b2cc52a2e72b9f4e0d8d0cb6 );
    }
    cache_frame_de0bad81b2cc52a2e72b9f4e0d8d0cb6 = NULL;

    assertFrameObject( frame_de0bad81b2cc52a2e72b9f4e0d8d0cb6 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$io$clipboard$clipboards$$$function_4_init_xclip_clipboard$$$function_2_paste_xclip );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( var_p );
    var_p = NULL;

    Py_XDECREF( var_stdout );
    var_stdout = NULL;

    Py_XDECREF( var_stderr );
    var_stderr = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( var_p );
    var_p = NULL;

    Py_XDECREF( var_stdout );
    var_stdout = NULL;

    Py_XDECREF( var_stderr );
    var_stderr = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$io$clipboard$clipboards$$$function_4_init_xclip_clipboard$$$function_2_paste_xclip );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$io$clipboard$clipboards$$$function_5_init_xsel_clipboard( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *var_copy_xsel = NULL;
    PyObject *var_paste_xsel = NULL;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_return_value;
    PyObject *tmp_tuple_element_1;
    tmp_return_value = NULL;

    // Actual function code.
    tmp_assign_source_1 = MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_5_init_xsel_clipboard$$$function_1_copy_xsel(  );
    assert( var_copy_xsel == NULL );
    var_copy_xsel = tmp_assign_source_1;

    tmp_assign_source_2 = MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_5_init_xsel_clipboard$$$function_2_paste_xsel(  );
    assert( var_paste_xsel == NULL );
    var_paste_xsel = tmp_assign_source_2;

    // Tried code:
    tmp_tuple_element_1 = var_copy_xsel;

    CHECK_OBJECT( tmp_tuple_element_1 );
    tmp_return_value = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_return_value, 0, tmp_tuple_element_1 );
    tmp_tuple_element_1 = var_paste_xsel;

    CHECK_OBJECT( tmp_tuple_element_1 );
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_return_value, 1, tmp_tuple_element_1 );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$io$clipboard$clipboards$$$function_5_init_xsel_clipboard );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)var_copy_xsel );
    Py_DECREF( var_copy_xsel );
    var_copy_xsel = NULL;

    Py_XDECREF( var_paste_xsel );
    var_paste_xsel = NULL;

    goto function_return_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$io$clipboard$clipboards$$$function_5_init_xsel_clipboard );
    return NULL;

function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$io$clipboard$clipboards$$$function_5_init_xsel_clipboard$$$function_1_copy_xsel( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_text = python_pars[ 0 ];
    PyObject *var_p = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_args_name_1;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_called_instance_1;
    PyObject *tmp_called_name_1;
    PyObject *tmp_called_name_2;
    PyObject *tmp_dict_key_1;
    PyObject *tmp_dict_key_2;
    PyObject *tmp_dict_key_3;
    PyObject *tmp_dict_value_1;
    PyObject *tmp_dict_value_2;
    PyObject *tmp_dict_value_3;
    PyObject *tmp_kw_name_1;
    PyObject *tmp_kw_name_2;
    int tmp_res;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    PyObject *tmp_source_name_2;
    PyObject *tmp_source_name_3;
    NUITKA_MAY_BE_UNUSED PyObject *tmp_unused;
    static struct Nuitka_FrameObject *cache_frame_24f1f9c5a5b1b760d9c1c3c6ad13fcd7 = NULL;

    struct Nuitka_FrameObject *frame_24f1f9c5a5b1b760d9c1c3c6ad13fcd7;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_24f1f9c5a5b1b760d9c1c3c6ad13fcd7, codeobj_24f1f9c5a5b1b760d9c1c3c6ad13fcd7, module_pandas$io$clipboard$clipboards, sizeof(void *)+sizeof(void *) );
    frame_24f1f9c5a5b1b760d9c1c3c6ad13fcd7 = cache_frame_24f1f9c5a5b1b760d9c1c3c6ad13fcd7;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_24f1f9c5a5b1b760d9c1c3c6ad13fcd7 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_24f1f9c5a5b1b760d9c1c3c6ad13fcd7 ) == 2 ); // Frame stack

    // Framed code:
    tmp_source_name_1 = GET_STRING_DICT_VALUE( moduledict_pandas$io$clipboard$clipboards, (Nuitka_StringObject *)const_str_plain_subprocess );

    if (unlikely( tmp_source_name_1 == NULL ))
    {
        tmp_source_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_subprocess );
    }

    if ( tmp_source_name_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "subprocess" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 89;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }

    tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_Popen );
    if ( tmp_called_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 89;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }
    tmp_args_name_1 = DEEP_COPY( const_tuple_list_a197c19226e81914eb0e409d9a9b699d_list_tuple );
    tmp_dict_key_1 = const_str_plain_stdin;
    tmp_source_name_2 = GET_STRING_DICT_VALUE( moduledict_pandas$io$clipboard$clipboards, (Nuitka_StringObject *)const_str_plain_subprocess );

    if (unlikely( tmp_source_name_2 == NULL ))
    {
        tmp_source_name_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_subprocess );
    }

    if ( tmp_source_name_2 == NULL )
    {
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_name_1 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "subprocess" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 90;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }

    tmp_dict_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_PIPE );
    if ( tmp_dict_value_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_name_1 );

        exception_lineno = 90;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }
    tmp_kw_name_1 = _PyDict_NewPresized( 2 );
    tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
    Py_DECREF( tmp_dict_value_1 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_2 = const_str_plain_close_fds;
    tmp_dict_value_2 = Py_True;
    tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
    assert( !(tmp_res != 0) );
    frame_24f1f9c5a5b1b760d9c1c3c6ad13fcd7->m_frame.f_lineno = 89;
    tmp_assign_source_1 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
    Py_DECREF( tmp_called_name_1 );
    Py_DECREF( tmp_args_name_1 );
    Py_DECREF( tmp_kw_name_1 );
    if ( tmp_assign_source_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 89;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }
    assert( var_p == NULL );
    var_p = tmp_assign_source_1;

    tmp_source_name_3 = var_p;

    CHECK_OBJECT( tmp_source_name_3 );
    tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_communicate );
    if ( tmp_called_name_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 91;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }
    tmp_dict_key_3 = const_str_plain_input;
    tmp_called_instance_1 = par_text;

    if ( tmp_called_instance_1 == NULL )
    {
        Py_DECREF( tmp_called_name_2 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "text" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 91;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }

    frame_24f1f9c5a5b1b760d9c1c3c6ad13fcd7->m_frame.f_lineno = 91;
    tmp_dict_value_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_encode, &PyTuple_GET_ITEM( const_tuple_str_digest_c075052d723d6707083e869a0e3659bb_tuple, 0 ) );

    if ( tmp_dict_value_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_2 );

        exception_lineno = 91;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }
    tmp_kw_name_2 = _PyDict_NewPresized( 1 );
    tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_3, tmp_dict_value_3 );
    Py_DECREF( tmp_dict_value_3 );
    assert( !(tmp_res != 0) );
    frame_24f1f9c5a5b1b760d9c1c3c6ad13fcd7->m_frame.f_lineno = 91;
    tmp_unused = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_2, tmp_kw_name_2 );
    Py_DECREF( tmp_called_name_2 );
    Py_DECREF( tmp_kw_name_2 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 91;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_unused );

#if 0
    RESTORE_FRAME_EXCEPTION( frame_24f1f9c5a5b1b760d9c1c3c6ad13fcd7 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_24f1f9c5a5b1b760d9c1c3c6ad13fcd7 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_24f1f9c5a5b1b760d9c1c3c6ad13fcd7, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_24f1f9c5a5b1b760d9c1c3c6ad13fcd7->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_24f1f9c5a5b1b760d9c1c3c6ad13fcd7, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_24f1f9c5a5b1b760d9c1c3c6ad13fcd7,
        type_description_1,
        par_text,
        var_p
    );


    // Release cached frame.
    if ( frame_24f1f9c5a5b1b760d9c1c3c6ad13fcd7 == cache_frame_24f1f9c5a5b1b760d9c1c3c6ad13fcd7 )
    {
        Py_DECREF( frame_24f1f9c5a5b1b760d9c1c3c6ad13fcd7 );
    }
    cache_frame_24f1f9c5a5b1b760d9c1c3c6ad13fcd7 = NULL;

    assertFrameObject( frame_24f1f9c5a5b1b760d9c1c3c6ad13fcd7 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$io$clipboard$clipboards$$$function_5_init_xsel_clipboard$$$function_1_copy_xsel );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_text );
    par_text = NULL;

    Py_XDECREF( var_p );
    var_p = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_text );
    par_text = NULL;

    Py_XDECREF( var_p );
    var_p = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$io$clipboard$clipboards$$$function_5_init_xsel_clipboard$$$function_1_copy_xsel );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$io$clipboard$clipboards$$$function_5_init_xsel_clipboard$$$function_2_paste_xsel( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *var_p = NULL;
    PyObject *var_stdout = NULL;
    PyObject *var_stderr = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *tmp_args_name_1;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_assign_source_5;
    PyObject *tmp_assign_source_6;
    PyObject *tmp_called_instance_1;
    PyObject *tmp_called_instance_2;
    PyObject *tmp_called_name_1;
    PyObject *tmp_dict_key_1;
    PyObject *tmp_dict_key_2;
    PyObject *tmp_dict_value_1;
    PyObject *tmp_dict_value_2;
    PyObject *tmp_iter_arg_1;
    PyObject *tmp_iterator_attempt;
    PyObject *tmp_iterator_name_1;
    PyObject *tmp_kw_name_1;
    int tmp_res;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    PyObject *tmp_source_name_2;
    PyObject *tmp_unpack_1;
    PyObject *tmp_unpack_2;
    static struct Nuitka_FrameObject *cache_frame_17129171ff48345b5f382b101aeaade1 = NULL;

    struct Nuitka_FrameObject *frame_17129171ff48345b5f382b101aeaade1;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_17129171ff48345b5f382b101aeaade1, codeobj_17129171ff48345b5f382b101aeaade1, module_pandas$io$clipboard$clipboards, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_17129171ff48345b5f382b101aeaade1 = cache_frame_17129171ff48345b5f382b101aeaade1;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_17129171ff48345b5f382b101aeaade1 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_17129171ff48345b5f382b101aeaade1 ) == 2 ); // Frame stack

    // Framed code:
    tmp_source_name_1 = GET_STRING_DICT_VALUE( moduledict_pandas$io$clipboard$clipboards, (Nuitka_StringObject *)const_str_plain_subprocess );

    if (unlikely( tmp_source_name_1 == NULL ))
    {
        tmp_source_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_subprocess );
    }

    if ( tmp_source_name_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "subprocess" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 94;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_Popen );
    if ( tmp_called_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 94;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    tmp_args_name_1 = DEEP_COPY( const_tuple_list_67e309ba5d5eed8090523ac1cf863d7d_list_tuple );
    tmp_dict_key_1 = const_str_plain_stdout;
    tmp_source_name_2 = GET_STRING_DICT_VALUE( moduledict_pandas$io$clipboard$clipboards, (Nuitka_StringObject *)const_str_plain_subprocess );

    if (unlikely( tmp_source_name_2 == NULL ))
    {
        tmp_source_name_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_subprocess );
    }

    if ( tmp_source_name_2 == NULL )
    {
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_name_1 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "subprocess" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 95;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    tmp_dict_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_PIPE );
    if ( tmp_dict_value_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_name_1 );

        exception_lineno = 95;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    tmp_kw_name_1 = _PyDict_NewPresized( 2 );
    tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
    Py_DECREF( tmp_dict_value_1 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_2 = const_str_plain_close_fds;
    tmp_dict_value_2 = Py_True;
    tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
    assert( !(tmp_res != 0) );
    frame_17129171ff48345b5f382b101aeaade1->m_frame.f_lineno = 94;
    tmp_assign_source_1 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
    Py_DECREF( tmp_called_name_1 );
    Py_DECREF( tmp_args_name_1 );
    Py_DECREF( tmp_kw_name_1 );
    if ( tmp_assign_source_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 94;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    assert( var_p == NULL );
    var_p = tmp_assign_source_1;

    // Tried code:
    tmp_called_instance_1 = var_p;

    CHECK_OBJECT( tmp_called_instance_1 );
    frame_17129171ff48345b5f382b101aeaade1->m_frame.f_lineno = 96;
    tmp_iter_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_communicate );
    if ( tmp_iter_arg_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 96;
        type_description_1 = "ooo";
        goto try_except_handler_2;
    }
    tmp_assign_source_2 = MAKE_ITERATOR( tmp_iter_arg_1 );
    Py_DECREF( tmp_iter_arg_1 );
    if ( tmp_assign_source_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 96;
        type_description_1 = "ooo";
        goto try_except_handler_2;
    }
    assert( tmp_tuple_unpack_1__source_iter == NULL );
    tmp_tuple_unpack_1__source_iter = tmp_assign_source_2;

    // Tried code:
    tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;

    CHECK_OBJECT( tmp_unpack_1 );
    tmp_assign_source_3 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
    if ( tmp_assign_source_3 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        type_description_1 = "ooo";
        exception_lineno = 96;
        goto try_except_handler_3;
    }
    assert( tmp_tuple_unpack_1__element_1 == NULL );
    tmp_tuple_unpack_1__element_1 = tmp_assign_source_3;

    tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;

    CHECK_OBJECT( tmp_unpack_2 );
    tmp_assign_source_4 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
    if ( tmp_assign_source_4 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        type_description_1 = "ooo";
        exception_lineno = 96;
        goto try_except_handler_3;
    }
    assert( tmp_tuple_unpack_1__element_2 == NULL );
    tmp_tuple_unpack_1__element_2 = tmp_assign_source_4;

    tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;

    CHECK_OBJECT( tmp_iterator_name_1 );
    // Check if iterator has left-over elements.
    CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

    tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

    if (likely( tmp_iterator_attempt == NULL ))
    {
        PyObject *error = GET_ERROR_OCCURRED();

        if ( error != NULL )
        {
            if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
            {
                CLEAR_ERROR_OCCURRED();
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                type_description_1 = "ooo";
                exception_lineno = 96;
                goto try_except_handler_3;
            }
        }
    }
    else
    {
        Py_DECREF( tmp_iterator_attempt );

        // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
        PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
        PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

        type_description_1 = "ooo";
        exception_lineno = 96;
        goto try_except_handler_3;
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_2;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    tmp_assign_source_5 = tmp_tuple_unpack_1__element_1;

    CHECK_OBJECT( tmp_assign_source_5 );
    assert( var_stdout == NULL );
    Py_INCREF( tmp_assign_source_5 );
    var_stdout = tmp_assign_source_5;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    tmp_assign_source_6 = tmp_tuple_unpack_1__element_2;

    CHECK_OBJECT( tmp_assign_source_6 );
    assert( var_stderr == NULL );
    Py_INCREF( tmp_assign_source_6 );
    var_stderr = tmp_assign_source_6;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    tmp_called_instance_2 = var_stdout;

    if ( tmp_called_instance_2 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "stdout" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 97;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    frame_17129171ff48345b5f382b101aeaade1->m_frame.f_lineno = 97;
    tmp_return_value = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_decode, &PyTuple_GET_ITEM( const_tuple_str_digest_c075052d723d6707083e869a0e3659bb_tuple, 0 ) );

    if ( tmp_return_value == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 97;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_17129171ff48345b5f382b101aeaade1 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_17129171ff48345b5f382b101aeaade1 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_17129171ff48345b5f382b101aeaade1 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_17129171ff48345b5f382b101aeaade1, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_17129171ff48345b5f382b101aeaade1->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_17129171ff48345b5f382b101aeaade1, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_17129171ff48345b5f382b101aeaade1,
        type_description_1,
        var_p,
        var_stdout,
        var_stderr
    );


    // Release cached frame.
    if ( frame_17129171ff48345b5f382b101aeaade1 == cache_frame_17129171ff48345b5f382b101aeaade1 )
    {
        Py_DECREF( frame_17129171ff48345b5f382b101aeaade1 );
    }
    cache_frame_17129171ff48345b5f382b101aeaade1 = NULL;

    assertFrameObject( frame_17129171ff48345b5f382b101aeaade1 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$io$clipboard$clipboards$$$function_5_init_xsel_clipboard$$$function_2_paste_xsel );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( var_p );
    var_p = NULL;

    Py_XDECREF( var_stdout );
    var_stdout = NULL;

    Py_XDECREF( var_stderr );
    var_stderr = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( var_p );
    var_p = NULL;

    Py_XDECREF( var_stdout );
    var_stdout = NULL;

    Py_XDECREF( var_stderr );
    var_stderr = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$io$clipboard$clipboards$$$function_5_init_xsel_clipboard$$$function_2_paste_xsel );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$io$clipboard$clipboards$$$function_6_init_klipper_clipboard( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *var_copy_klipper = NULL;
    PyObject *var_paste_klipper = NULL;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_return_value;
    PyObject *tmp_tuple_element_1;
    tmp_return_value = NULL;

    // Actual function code.
    tmp_assign_source_1 = MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_6_init_klipper_clipboard$$$function_1_copy_klipper(  );
    assert( var_copy_klipper == NULL );
    var_copy_klipper = tmp_assign_source_1;

    tmp_assign_source_2 = MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_6_init_klipper_clipboard$$$function_2_paste_klipper(  );
    assert( var_paste_klipper == NULL );
    var_paste_klipper = tmp_assign_source_2;

    // Tried code:
    tmp_tuple_element_1 = var_copy_klipper;

    CHECK_OBJECT( tmp_tuple_element_1 );
    tmp_return_value = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_return_value, 0, tmp_tuple_element_1 );
    tmp_tuple_element_1 = var_paste_klipper;

    CHECK_OBJECT( tmp_tuple_element_1 );
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_return_value, 1, tmp_tuple_element_1 );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$io$clipboard$clipboards$$$function_6_init_klipper_clipboard );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)var_copy_klipper );
    Py_DECREF( var_copy_klipper );
    var_copy_klipper = NULL;

    Py_XDECREF( var_paste_klipper );
    var_paste_klipper = NULL;

    goto function_return_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$io$clipboard$clipboards$$$function_6_init_klipper_clipboard );
    return NULL;

function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$io$clipboard$clipboards$$$function_6_init_klipper_clipboard$$$function_1_copy_klipper( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_text = python_pars[ 0 ];
    PyObject *var_p = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_args_name_1;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_called_instance_1;
    PyObject *tmp_called_name_1;
    PyObject *tmp_called_name_2;
    PyObject *tmp_dict_key_1;
    PyObject *tmp_dict_key_2;
    PyObject *tmp_dict_value_1;
    PyObject *tmp_dict_value_2;
    PyObject *tmp_kw_name_1;
    PyObject *tmp_kw_name_2;
    PyObject *tmp_list_element_1;
    int tmp_res;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    PyObject *tmp_source_name_2;
    PyObject *tmp_source_name_3;
    PyObject *tmp_tuple_element_1;
    NUITKA_MAY_BE_UNUSED PyObject *tmp_unused;
    static struct Nuitka_FrameObject *cache_frame_8350dbeca557534164273aa396913a15 = NULL;

    struct Nuitka_FrameObject *frame_8350dbeca557534164273aa396913a15;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_8350dbeca557534164273aa396913a15, codeobj_8350dbeca557534164273aa396913a15, module_pandas$io$clipboard$clipboards, sizeof(void *)+sizeof(void *) );
    frame_8350dbeca557534164273aa396913a15 = cache_frame_8350dbeca557534164273aa396913a15;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_8350dbeca557534164273aa396913a15 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_8350dbeca557534164273aa396913a15 ) == 2 ); // Frame stack

    // Framed code:
    tmp_source_name_1 = GET_STRING_DICT_VALUE( moduledict_pandas$io$clipboard$clipboards, (Nuitka_StringObject *)const_str_plain_subprocess );

    if (unlikely( tmp_source_name_1 == NULL ))
    {
        tmp_source_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_subprocess );
    }

    if ( tmp_source_name_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "subprocess" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 104;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }

    tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_Popen );
    if ( tmp_called_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 104;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }
    tmp_list_element_1 = const_str_plain_qdbus;
    tmp_tuple_element_1 = PyList_New( 5 );
    Py_INCREF( tmp_list_element_1 );
    PyList_SET_ITEM( tmp_tuple_element_1, 0, tmp_list_element_1 );
    tmp_list_element_1 = const_str_digest_57a50efac3dc6f8b8afc68b347b3c8cd;
    Py_INCREF( tmp_list_element_1 );
    PyList_SET_ITEM( tmp_tuple_element_1, 1, tmp_list_element_1 );
    tmp_list_element_1 = const_str_digest_5bfd7f32b3b59128ef50a98dbb13a245;
    Py_INCREF( tmp_list_element_1 );
    PyList_SET_ITEM( tmp_tuple_element_1, 2, tmp_list_element_1 );
    tmp_list_element_1 = const_str_plain_setClipboardContents;
    Py_INCREF( tmp_list_element_1 );
    PyList_SET_ITEM( tmp_tuple_element_1, 3, tmp_list_element_1 );
    tmp_called_instance_1 = par_text;

    if ( tmp_called_instance_1 == NULL )
    {
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_tuple_element_1 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "text" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 106;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }

    frame_8350dbeca557534164273aa396913a15->m_frame.f_lineno = 106;
    tmp_list_element_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_encode, &PyTuple_GET_ITEM( const_tuple_str_digest_c075052d723d6707083e869a0e3659bb_tuple, 0 ) );

    if ( tmp_list_element_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_tuple_element_1 );

        exception_lineno = 106;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }
    PyList_SET_ITEM( tmp_tuple_element_1, 4, tmp_list_element_1 );
    tmp_args_name_1 = PyTuple_New( 1 );
    PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
    tmp_dict_key_1 = const_str_plain_stdin;
    tmp_source_name_2 = GET_STRING_DICT_VALUE( moduledict_pandas$io$clipboard$clipboards, (Nuitka_StringObject *)const_str_plain_subprocess );

    if (unlikely( tmp_source_name_2 == NULL ))
    {
        tmp_source_name_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_subprocess );
    }

    if ( tmp_source_name_2 == NULL )
    {
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_name_1 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "subprocess" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 107;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }

    tmp_dict_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_PIPE );
    if ( tmp_dict_value_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_name_1 );

        exception_lineno = 107;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }
    tmp_kw_name_1 = _PyDict_NewPresized( 2 );
    tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
    Py_DECREF( tmp_dict_value_1 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_2 = const_str_plain_close_fds;
    tmp_dict_value_2 = Py_True;
    tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
    assert( !(tmp_res != 0) );
    frame_8350dbeca557534164273aa396913a15->m_frame.f_lineno = 104;
    tmp_assign_source_1 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
    Py_DECREF( tmp_called_name_1 );
    Py_DECREF( tmp_args_name_1 );
    Py_DECREF( tmp_kw_name_1 );
    if ( tmp_assign_source_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 104;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }
    assert( var_p == NULL );
    var_p = tmp_assign_source_1;

    tmp_source_name_3 = var_p;

    CHECK_OBJECT( tmp_source_name_3 );
    tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_communicate );
    if ( tmp_called_name_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 108;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }
    tmp_kw_name_2 = PyDict_Copy( const_dict_c453dda1d3901e4cb85d5781f8962504 );
    frame_8350dbeca557534164273aa396913a15->m_frame.f_lineno = 108;
    tmp_unused = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_2, tmp_kw_name_2 );
    Py_DECREF( tmp_called_name_2 );
    Py_DECREF( tmp_kw_name_2 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 108;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_unused );

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8350dbeca557534164273aa396913a15 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8350dbeca557534164273aa396913a15 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_8350dbeca557534164273aa396913a15, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_8350dbeca557534164273aa396913a15->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_8350dbeca557534164273aa396913a15, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_8350dbeca557534164273aa396913a15,
        type_description_1,
        par_text,
        var_p
    );


    // Release cached frame.
    if ( frame_8350dbeca557534164273aa396913a15 == cache_frame_8350dbeca557534164273aa396913a15 )
    {
        Py_DECREF( frame_8350dbeca557534164273aa396913a15 );
    }
    cache_frame_8350dbeca557534164273aa396913a15 = NULL;

    assertFrameObject( frame_8350dbeca557534164273aa396913a15 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$io$clipboard$clipboards$$$function_6_init_klipper_clipboard$$$function_1_copy_klipper );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_text );
    par_text = NULL;

    Py_XDECREF( var_p );
    var_p = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_text );
    par_text = NULL;

    Py_XDECREF( var_p );
    var_p = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$io$clipboard$clipboards$$$function_6_init_klipper_clipboard$$$function_1_copy_klipper );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$io$clipboard$clipboards$$$function_6_init_klipper_clipboard$$$function_2_paste_klipper( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *var_p = NULL;
    PyObject *var_stdout = NULL;
    PyObject *var_stderr = NULL;
    PyObject *var_clipboardContents = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *tmp_args_name_1;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_assign_source_5;
    PyObject *tmp_assign_source_6;
    PyObject *tmp_assign_source_7;
    PyObject *tmp_assign_source_8;
    PyObject *tmp_called_instance_1;
    PyObject *tmp_called_instance_2;
    PyObject *tmp_called_instance_3;
    PyObject *tmp_called_instance_4;
    PyObject *tmp_called_name_1;
    int tmp_cmp_Gt_1;
    PyObject *tmp_compare_left_1;
    PyObject *tmp_compare_right_1;
    int tmp_cond_truth_1;
    int tmp_cond_truth_2;
    PyObject *tmp_cond_value_1;
    PyObject *tmp_cond_value_2;
    PyObject *tmp_dict_key_1;
    PyObject *tmp_dict_key_2;
    PyObject *tmp_dict_value_1;
    PyObject *tmp_dict_value_2;
    PyObject *tmp_iter_arg_1;
    PyObject *tmp_iterator_attempt;
    PyObject *tmp_iterator_name_1;
    PyObject *tmp_kw_name_1;
    PyObject *tmp_len_arg_1;
    PyObject *tmp_raise_type_1;
    PyObject *tmp_raise_type_2;
    int tmp_res;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    PyObject *tmp_source_name_2;
    PyObject *tmp_subscribed_name_1;
    PyObject *tmp_subscript_name_1;
    PyObject *tmp_unpack_1;
    PyObject *tmp_unpack_2;
    static struct Nuitka_FrameObject *cache_frame_641b96a224a5ed333f02bedaf0f5eed7 = NULL;

    struct Nuitka_FrameObject *frame_641b96a224a5ed333f02bedaf0f5eed7;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_641b96a224a5ed333f02bedaf0f5eed7, codeobj_641b96a224a5ed333f02bedaf0f5eed7, module_pandas$io$clipboard$clipboards, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_641b96a224a5ed333f02bedaf0f5eed7 = cache_frame_641b96a224a5ed333f02bedaf0f5eed7;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_641b96a224a5ed333f02bedaf0f5eed7 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_641b96a224a5ed333f02bedaf0f5eed7 ) == 2 ); // Frame stack

    // Framed code:
    tmp_source_name_1 = GET_STRING_DICT_VALUE( moduledict_pandas$io$clipboard$clipboards, (Nuitka_StringObject *)const_str_plain_subprocess );

    if (unlikely( tmp_source_name_1 == NULL ))
    {
        tmp_source_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_subprocess );
    }

    if ( tmp_source_name_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "subprocess" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 111;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }

    tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_Popen );
    if ( tmp_called_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 111;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    tmp_args_name_1 = DEEP_COPY( const_tuple_list_71826f1b3786fb88c225418f225b3823_list_tuple );
    tmp_dict_key_1 = const_str_plain_stdout;
    tmp_source_name_2 = GET_STRING_DICT_VALUE( moduledict_pandas$io$clipboard$clipboards, (Nuitka_StringObject *)const_str_plain_subprocess );

    if (unlikely( tmp_source_name_2 == NULL ))
    {
        tmp_source_name_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_subprocess );
    }

    if ( tmp_source_name_2 == NULL )
    {
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_name_1 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "subprocess" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 113;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }

    tmp_dict_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_PIPE );
    if ( tmp_dict_value_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_name_1 );

        exception_lineno = 113;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    tmp_kw_name_1 = _PyDict_NewPresized( 2 );
    tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
    Py_DECREF( tmp_dict_value_1 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_2 = const_str_plain_close_fds;
    tmp_dict_value_2 = Py_True;
    tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
    assert( !(tmp_res != 0) );
    frame_641b96a224a5ed333f02bedaf0f5eed7->m_frame.f_lineno = 111;
    tmp_assign_source_1 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
    Py_DECREF( tmp_called_name_1 );
    Py_DECREF( tmp_args_name_1 );
    Py_DECREF( tmp_kw_name_1 );
    if ( tmp_assign_source_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 111;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    assert( var_p == NULL );
    var_p = tmp_assign_source_1;

    // Tried code:
    tmp_called_instance_1 = var_p;

    CHECK_OBJECT( tmp_called_instance_1 );
    frame_641b96a224a5ed333f02bedaf0f5eed7->m_frame.f_lineno = 114;
    tmp_iter_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_communicate );
    if ( tmp_iter_arg_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 114;
        type_description_1 = "oooo";
        goto try_except_handler_2;
    }
    tmp_assign_source_2 = MAKE_ITERATOR( tmp_iter_arg_1 );
    Py_DECREF( tmp_iter_arg_1 );
    if ( tmp_assign_source_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 114;
        type_description_1 = "oooo";
        goto try_except_handler_2;
    }
    assert( tmp_tuple_unpack_1__source_iter == NULL );
    tmp_tuple_unpack_1__source_iter = tmp_assign_source_2;

    // Tried code:
    tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;

    CHECK_OBJECT( tmp_unpack_1 );
    tmp_assign_source_3 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
    if ( tmp_assign_source_3 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        type_description_1 = "oooo";
        exception_lineno = 114;
        goto try_except_handler_3;
    }
    assert( tmp_tuple_unpack_1__element_1 == NULL );
    tmp_tuple_unpack_1__element_1 = tmp_assign_source_3;

    tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;

    CHECK_OBJECT( tmp_unpack_2 );
    tmp_assign_source_4 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
    if ( tmp_assign_source_4 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        type_description_1 = "oooo";
        exception_lineno = 114;
        goto try_except_handler_3;
    }
    assert( tmp_tuple_unpack_1__element_2 == NULL );
    tmp_tuple_unpack_1__element_2 = tmp_assign_source_4;

    tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;

    CHECK_OBJECT( tmp_iterator_name_1 );
    // Check if iterator has left-over elements.
    CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

    tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

    if (likely( tmp_iterator_attempt == NULL ))
    {
        PyObject *error = GET_ERROR_OCCURRED();

        if ( error != NULL )
        {
            if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
            {
                CLEAR_ERROR_OCCURRED();
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                type_description_1 = "oooo";
                exception_lineno = 114;
                goto try_except_handler_3;
            }
        }
    }
    else
    {
        Py_DECREF( tmp_iterator_attempt );

        // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
        PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
        PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

        type_description_1 = "oooo";
        exception_lineno = 114;
        goto try_except_handler_3;
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_2;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    tmp_assign_source_5 = tmp_tuple_unpack_1__element_1;

    CHECK_OBJECT( tmp_assign_source_5 );
    assert( var_stdout == NULL );
    Py_INCREF( tmp_assign_source_5 );
    var_stdout = tmp_assign_source_5;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    tmp_assign_source_6 = tmp_tuple_unpack_1__element_2;

    CHECK_OBJECT( tmp_assign_source_6 );
    assert( var_stderr == NULL );
    Py_INCREF( tmp_assign_source_6 );
    var_stderr = tmp_assign_source_6;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    tmp_called_instance_2 = var_stdout;

    if ( tmp_called_instance_2 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "stdout" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 118;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }

    frame_641b96a224a5ed333f02bedaf0f5eed7->m_frame.f_lineno = 118;
    tmp_assign_source_7 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_decode, &PyTuple_GET_ITEM( const_tuple_str_digest_c075052d723d6707083e869a0e3659bb_tuple, 0 ) );

    if ( tmp_assign_source_7 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 118;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    assert( var_clipboardContents == NULL );
    var_clipboardContents = tmp_assign_source_7;

    tmp_len_arg_1 = var_clipboardContents;

    CHECK_OBJECT( tmp_len_arg_1 );
    tmp_compare_left_1 = BUILTIN_LEN( tmp_len_arg_1 );
    if ( tmp_compare_left_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 120;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    tmp_compare_right_1 = const_int_0;
    tmp_cmp_Gt_1 = RICH_COMPARE_BOOL_GT( tmp_compare_left_1, tmp_compare_right_1 );
    Py_DECREF( tmp_compare_left_1 );
    if ( tmp_cmp_Gt_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 120;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    if ( tmp_cmp_Gt_1 == 1 )
    {
        goto branch_no_1;
    }
    else
    {
        goto branch_yes_1;
    }
    branch_yes_1:;
    tmp_raise_type_1 = PyExc_AssertionError;
    exception_type = tmp_raise_type_1;
    Py_INCREF( tmp_raise_type_1 );
    exception_lineno = 120;
    RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
    type_description_1 = "oooo";
    goto frame_exception_exit_1;
    branch_no_1:;
    tmp_called_instance_3 = var_clipboardContents;

    if ( tmp_called_instance_3 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "clipboardContents" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 122;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }

    frame_641b96a224a5ed333f02bedaf0f5eed7->m_frame.f_lineno = 122;
    tmp_cond_value_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_endswith, &PyTuple_GET_ITEM( const_tuple_str_newline_tuple, 0 ) );

    if ( tmp_cond_value_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 122;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    tmp_cond_truth_1 = CHECK_IF_TRUE( tmp_cond_value_1 );
    if ( tmp_cond_truth_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_cond_value_1 );

        exception_lineno = 122;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_cond_value_1 );
    if ( tmp_cond_truth_1 == 1 )
    {
        goto branch_no_2;
    }
    else
    {
        goto branch_yes_2;
    }
    branch_yes_2:;
    tmp_raise_type_2 = PyExc_AssertionError;
    exception_type = tmp_raise_type_2;
    Py_INCREF( tmp_raise_type_2 );
    exception_lineno = 122;
    RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
    type_description_1 = "oooo";
    goto frame_exception_exit_1;
    branch_no_2:;
    tmp_called_instance_4 = var_clipboardContents;

    if ( tmp_called_instance_4 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "clipboardContents" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 123;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }

    frame_641b96a224a5ed333f02bedaf0f5eed7->m_frame.f_lineno = 123;
    tmp_cond_value_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_4, const_str_plain_endswith, &PyTuple_GET_ITEM( const_tuple_str_newline_tuple, 0 ) );

    if ( tmp_cond_value_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 123;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    tmp_cond_truth_2 = CHECK_IF_TRUE( tmp_cond_value_2 );
    if ( tmp_cond_truth_2 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_cond_value_2 );

        exception_lineno = 123;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_cond_value_2 );
    if ( tmp_cond_truth_2 == 1 )
    {
        goto branch_yes_3;
    }
    else
    {
        goto branch_no_3;
    }
    branch_yes_3:;
    tmp_subscribed_name_1 = var_clipboardContents;

    if ( tmp_subscribed_name_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "clipboardContents" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 124;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }

    tmp_subscript_name_1 = const_slice_none_int_neg_1_none;
    tmp_assign_source_8 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
    if ( tmp_assign_source_8 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 124;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    {
        PyObject *old = var_clipboardContents;
        var_clipboardContents = tmp_assign_source_8;
        Py_XDECREF( old );
    }

    branch_no_3:;
    tmp_return_value = var_clipboardContents;

    if ( tmp_return_value == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "clipboardContents" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 125;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }

    Py_INCREF( tmp_return_value );
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_641b96a224a5ed333f02bedaf0f5eed7 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_641b96a224a5ed333f02bedaf0f5eed7 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_641b96a224a5ed333f02bedaf0f5eed7 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_641b96a224a5ed333f02bedaf0f5eed7, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_641b96a224a5ed333f02bedaf0f5eed7->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_641b96a224a5ed333f02bedaf0f5eed7, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_641b96a224a5ed333f02bedaf0f5eed7,
        type_description_1,
        var_p,
        var_stdout,
        var_stderr,
        var_clipboardContents
    );


    // Release cached frame.
    if ( frame_641b96a224a5ed333f02bedaf0f5eed7 == cache_frame_641b96a224a5ed333f02bedaf0f5eed7 )
    {
        Py_DECREF( frame_641b96a224a5ed333f02bedaf0f5eed7 );
    }
    cache_frame_641b96a224a5ed333f02bedaf0f5eed7 = NULL;

    assertFrameObject( frame_641b96a224a5ed333f02bedaf0f5eed7 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$io$clipboard$clipboards$$$function_6_init_klipper_clipboard$$$function_2_paste_klipper );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( var_p );
    var_p = NULL;

    Py_XDECREF( var_stdout );
    var_stdout = NULL;

    Py_XDECREF( var_stderr );
    var_stderr = NULL;

    Py_XDECREF( var_clipboardContents );
    var_clipboardContents = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( var_p );
    var_p = NULL;

    Py_XDECREF( var_stdout );
    var_stdout = NULL;

    Py_XDECREF( var_stderr );
    var_stderr = NULL;

    Py_XDECREF( var_clipboardContents );
    var_clipboardContents = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$io$clipboard$clipboards$$$function_6_init_klipper_clipboard$$$function_2_paste_klipper );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$io$clipboard$clipboards$$$function_7_init_no_clipboard( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *var_ClipboardUnavailable = NULL;
    PyObject *outline_0_var___class__ = NULL;
    PyObject *tmp_class_creation_1__class_decl_dict = NULL;
    PyObject *tmp_class_creation_1__metaclass = NULL;
    PyObject *tmp_class_creation_1__prepared = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *tmp_args_name_1;
    PyObject *tmp_args_name_2;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_assign_source_5;
    PyObject *tmp_bases_name_1;
    PyObject *tmp_called_name_1;
    PyObject *tmp_called_name_2;
    PyObject *tmp_called_name_3;
    PyObject *tmp_called_name_4;
    int tmp_cond_truth_1;
    int tmp_cond_truth_2;
    int tmp_cond_truth_3;
    PyObject *tmp_cond_value_1;
    PyObject *tmp_cond_value_2;
    PyObject *tmp_cond_value_3;
    PyObject *tmp_dict_name_1;
    PyObject *tmp_dict_name_2;
    PyObject *tmp_dict_name_3;
    PyObject *tmp_dictdel_dict;
    PyObject *tmp_dictdel_key;
    PyObject *tmp_dictset_value;
    PyObject *tmp_hasattr_attr_1;
    PyObject *tmp_hasattr_source_1;
    PyObject *tmp_key_name_1;
    PyObject *tmp_key_name_2;
    PyObject *tmp_key_name_3;
    PyObject *tmp_kw_name_1;
    PyObject *tmp_kw_name_2;
    PyObject *tmp_metaclass_name_1;
    PyObject *tmp_outline_return_value_1;
    int tmp_res;
    bool tmp_result;
    PyObject *tmp_return_value;
    PyObject *tmp_set_locals;
    PyObject *tmp_source_name_1;
    PyObject *tmp_tuple_element_1;
    PyObject *tmp_tuple_element_2;
    static struct Nuitka_FrameObject *cache_frame_d30a2e91f920a55897facd7649785ed7_2 = NULL;

    struct Nuitka_FrameObject *frame_d30a2e91f920a55897facd7649785ed7_2;

    static struct Nuitka_FrameObject *cache_frame_1b1008a41b3a1e85a29e2c501f0885d0 = NULL;

    struct Nuitka_FrameObject *frame_1b1008a41b3a1e85a29e2c501f0885d0;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    tmp_return_value = NULL;
    tmp_outline_return_value_1 = NULL;
    PyObject *locals_pandas$io$clipboard$clipboards$$$function_7_init_no_clipboard_131 = NULL;

    // Actual function code.
    tmp_assign_source_1 = PyDict_New();
    assert( tmp_class_creation_1__class_decl_dict == NULL );
    tmp_class_creation_1__class_decl_dict = tmp_assign_source_1;

    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_1b1008a41b3a1e85a29e2c501f0885d0, codeobj_1b1008a41b3a1e85a29e2c501f0885d0, module_pandas$io$clipboard$clipboards, sizeof(void *) );
    frame_1b1008a41b3a1e85a29e2c501f0885d0 = cache_frame_1b1008a41b3a1e85a29e2c501f0885d0;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_1b1008a41b3a1e85a29e2c501f0885d0 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_1b1008a41b3a1e85a29e2c501f0885d0 ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    tmp_key_name_1 = const_str_plain_metaclass;
    tmp_dict_name_1 = tmp_class_creation_1__class_decl_dict;

    CHECK_OBJECT( tmp_dict_name_1 );
    tmp_res = PyDict_Contains( tmp_dict_name_1, tmp_key_name_1 );
    if ( tmp_res == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 131;
        type_description_1 = "o";
        goto try_except_handler_2;
    }
    tmp_cond_value_1 = BOOL_FROM( tmp_res == 1 );
    tmp_cond_truth_1 = CHECK_IF_TRUE( tmp_cond_value_1 );
    if ( tmp_cond_truth_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 131;
        type_description_1 = "o";
        goto try_except_handler_2;
    }
    if ( tmp_cond_truth_1 == 1 )
    {
        goto condexpr_true_1;
    }
    else
    {
        goto condexpr_false_1;
    }
    condexpr_true_1:;
    tmp_dict_name_2 = tmp_class_creation_1__class_decl_dict;

    CHECK_OBJECT( tmp_dict_name_2 );
    tmp_key_name_2 = const_str_plain_metaclass;
    tmp_metaclass_name_1 = DICT_GET_ITEM( tmp_dict_name_2, tmp_key_name_2 );
    if ( tmp_metaclass_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 131;
        type_description_1 = "o";
        goto try_except_handler_2;
    }
    goto condexpr_end_1;
    condexpr_false_1:;
    tmp_metaclass_name_1 = LOOKUP_BUILTIN( const_str_plain_type );
    assert( tmp_metaclass_name_1 != NULL );
    Py_INCREF( tmp_metaclass_name_1 );
    condexpr_end_1:;
    tmp_bases_name_1 = const_tuple_type_object_tuple;
    tmp_assign_source_2 = SELECT_METACLASS( tmp_metaclass_name_1, tmp_bases_name_1 );
    Py_DECREF( tmp_metaclass_name_1 );
    if ( tmp_assign_source_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 131;
        type_description_1 = "o";
        goto try_except_handler_2;
    }
    assert( tmp_class_creation_1__metaclass == NULL );
    tmp_class_creation_1__metaclass = tmp_assign_source_2;

    tmp_key_name_3 = const_str_plain_metaclass;
    tmp_dict_name_3 = tmp_class_creation_1__class_decl_dict;

    CHECK_OBJECT( tmp_dict_name_3 );
    tmp_res = PyDict_Contains( tmp_dict_name_3, tmp_key_name_3 );
    if ( tmp_res == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 131;
        type_description_1 = "o";
        goto try_except_handler_2;
    }
    tmp_cond_value_2 = BOOL_FROM( tmp_res == 1 );
    tmp_cond_truth_2 = CHECK_IF_TRUE( tmp_cond_value_2 );
    if ( tmp_cond_truth_2 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 131;
        type_description_1 = "o";
        goto try_except_handler_2;
    }
    if ( tmp_cond_truth_2 == 1 )
    {
        goto branch_yes_1;
    }
    else
    {
        goto branch_no_1;
    }
    branch_yes_1:;
    tmp_dictdel_dict = tmp_class_creation_1__class_decl_dict;

    CHECK_OBJECT( tmp_dictdel_dict );
    tmp_dictdel_key = const_str_plain_metaclass;
    tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 131;
        type_description_1 = "o";
        goto try_except_handler_2;
    }
    branch_no_1:;
    tmp_hasattr_source_1 = tmp_class_creation_1__metaclass;

    CHECK_OBJECT( tmp_hasattr_source_1 );
    tmp_hasattr_attr_1 = const_str_plain___prepare__;
    tmp_res = PyObject_HasAttr( tmp_hasattr_source_1, tmp_hasattr_attr_1 );
    if ( tmp_res == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 131;
        type_description_1 = "o";
        goto try_except_handler_2;
    }
    if ( tmp_res == 1 )
    {
        goto condexpr_true_2;
    }
    else
    {
        goto condexpr_false_2;
    }
    condexpr_true_2:;
    tmp_source_name_1 = tmp_class_creation_1__metaclass;

    CHECK_OBJECT( tmp_source_name_1 );
    tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___prepare__ );
    if ( tmp_called_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 131;
        type_description_1 = "o";
        goto try_except_handler_2;
    }
    tmp_args_name_1 = const_tuple_str_plain_ClipboardUnavailable_tuple_type_object_tuple_tuple;
    tmp_kw_name_1 = tmp_class_creation_1__class_decl_dict;

    CHECK_OBJECT( tmp_kw_name_1 );
    frame_1b1008a41b3a1e85a29e2c501f0885d0->m_frame.f_lineno = 131;
    tmp_assign_source_3 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
    Py_DECREF( tmp_called_name_1 );
    if ( tmp_assign_source_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 131;
        type_description_1 = "o";
        goto try_except_handler_2;
    }
    goto condexpr_end_2;
    condexpr_false_2:;
    tmp_assign_source_3 = PyDict_New();
    condexpr_end_2:;
    assert( tmp_class_creation_1__prepared == NULL );
    tmp_class_creation_1__prepared = tmp_assign_source_3;

    tmp_set_locals = tmp_class_creation_1__prepared;

    CHECK_OBJECT( tmp_set_locals );
    locals_pandas$io$clipboard$clipboards$$$function_7_init_no_clipboard_131 = tmp_set_locals;
    Py_INCREF( tmp_set_locals );
    // Tried code:
    // Tried code:
    tmp_dictset_value = const_str_digest_c1b240794be4eea191dd0f8d775fee7d;
    tmp_res = PyObject_SetItem( locals_pandas$io$clipboard$clipboards$$$function_7_init_no_clipboard_131, const_str_plain___module__, tmp_dictset_value );
    if ( tmp_res != 0 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 131;
        type_description_1 = "o";
        goto try_except_handler_4;
    }
    tmp_dictset_value = const_str_digest_344ef046ed627942028fc8b792076a41;
    tmp_res = PyObject_SetItem( locals_pandas$io$clipboard$clipboards$$$function_7_init_no_clipboard_131, const_str_plain___qualname__, tmp_dictset_value );
    if ( tmp_res != 0 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 131;
        type_description_1 = "o";
        goto try_except_handler_4;
    }
    MAKE_OR_REUSE_FRAME( cache_frame_d30a2e91f920a55897facd7649785ed7_2, codeobj_d30a2e91f920a55897facd7649785ed7, module_pandas$io$clipboard$clipboards, sizeof(void *) );
    frame_d30a2e91f920a55897facd7649785ed7_2 = cache_frame_d30a2e91f920a55897facd7649785ed7_2;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_d30a2e91f920a55897facd7649785ed7_2 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_d30a2e91f920a55897facd7649785ed7_2 ) == 2 ); // Frame stack

    // Framed code:
    tmp_dictset_value = MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_7_init_no_clipboard$$$function_1___call__(  );
    tmp_res = PyObject_SetItem( locals_pandas$io$clipboard$clipboards$$$function_7_init_no_clipboard_131, const_str_plain___call__, tmp_dictset_value );
    Py_DECREF( tmp_dictset_value );
    if ( tmp_res != 0 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 133;
        type_description_2 = "o";
        goto frame_exception_exit_2;
    }
    tmp_cond_value_3 = PyObject_GetItem( locals_pandas$io$clipboard$clipboards$$$function_7_init_no_clipboard_131, const_str_plain_PY2 );

    if ( tmp_cond_value_3 == NULL )
    {
        if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
        {
        tmp_cond_value_3 = GET_STRING_DICT_VALUE( moduledict_pandas$io$clipboard$clipboards, (Nuitka_StringObject *)const_str_plain_PY2 );

        if (unlikely( tmp_cond_value_3 == NULL ))
        {
            tmp_cond_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PY2 );
        }

        if ( tmp_cond_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "PY2" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 136;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }

        }
    }

    tmp_cond_truth_3 = CHECK_IF_TRUE( tmp_cond_value_3 );
    if ( tmp_cond_truth_3 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 136;
        type_description_2 = "o";
        goto frame_exception_exit_2;
    }
    if ( tmp_cond_truth_3 == 1 )
    {
        goto branch_yes_2;
    }
    else
    {
        goto branch_no_2;
    }
    branch_yes_2:;
    tmp_dictset_value = MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_7_init_no_clipboard$$$function_2___nonzero__(  );
    tmp_res = PyObject_SetItem( locals_pandas$io$clipboard$clipboards$$$function_7_init_no_clipboard_131, const_str_plain___nonzero__, tmp_dictset_value );
    Py_DECREF( tmp_dictset_value );
    if ( tmp_res != 0 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 137;
        type_description_2 = "o";
        goto frame_exception_exit_2;
    }
    goto branch_end_2;
    branch_no_2:;
    tmp_dictset_value = MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_7_init_no_clipboard$$$function_3___bool__(  );
    tmp_res = PyObject_SetItem( locals_pandas$io$clipboard$clipboards$$$function_7_init_no_clipboard_131, const_str_plain___bool__, tmp_dictset_value );
    Py_DECREF( tmp_dictset_value );
    if ( tmp_res != 0 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 140;
        type_description_2 = "o";
        goto frame_exception_exit_2;
    }
    branch_end_2:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d30a2e91f920a55897facd7649785ed7_2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_2:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d30a2e91f920a55897facd7649785ed7_2 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_d30a2e91f920a55897facd7649785ed7_2, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_d30a2e91f920a55897facd7649785ed7_2->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_d30a2e91f920a55897facd7649785ed7_2, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_d30a2e91f920a55897facd7649785ed7_2,
        type_description_2,
        outline_0_var___class__
    );


    // Release cached frame.
    if ( frame_d30a2e91f920a55897facd7649785ed7_2 == cache_frame_d30a2e91f920a55897facd7649785ed7_2 )
    {
        Py_DECREF( frame_d30a2e91f920a55897facd7649785ed7_2 );
    }
    cache_frame_d30a2e91f920a55897facd7649785ed7_2 = NULL;

    assertFrameObject( frame_d30a2e91f920a55897facd7649785ed7_2 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto nested_frame_exit_1;

    frame_no_exception_1:;

    goto skip_nested_handling_1;
    nested_frame_exit_1:;
    type_description_1 = "o";
    goto try_except_handler_4;
    skip_nested_handling_1:;
    tmp_called_name_2 = tmp_class_creation_1__metaclass;

    CHECK_OBJECT( tmp_called_name_2 );
    tmp_tuple_element_1 = const_str_plain_ClipboardUnavailable;
    tmp_args_name_2 = PyTuple_New( 3 );
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_1 );
    tmp_tuple_element_1 = const_tuple_type_object_tuple;
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_1 );
    tmp_tuple_element_1 = locals_pandas$io$clipboard$clipboards$$$function_7_init_no_clipboard_131;
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_args_name_2, 2, tmp_tuple_element_1 );
    tmp_kw_name_2 = tmp_class_creation_1__class_decl_dict;

    CHECK_OBJECT( tmp_kw_name_2 );
    frame_1b1008a41b3a1e85a29e2c501f0885d0->m_frame.f_lineno = 131;
    tmp_assign_source_5 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_2, tmp_kw_name_2 );
    Py_DECREF( tmp_args_name_2 );
    if ( tmp_assign_source_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 131;
        type_description_1 = "o";
        goto try_except_handler_4;
    }
    assert( outline_0_var___class__ == NULL );
    outline_0_var___class__ = tmp_assign_source_5;

    tmp_outline_return_value_1 = outline_0_var___class__;

    CHECK_OBJECT( tmp_outline_return_value_1 );
    Py_INCREF( tmp_outline_return_value_1 );
    goto try_return_handler_4;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$io$clipboard$clipboards$$$function_7_init_no_clipboard );
    return NULL;
    // Return handler code:
    try_return_handler_4:;
    Py_DECREF( locals_pandas$io$clipboard$clipboards$$$function_7_init_no_clipboard_131 );
    locals_pandas$io$clipboard$clipboards$$$function_7_init_no_clipboard_131 = NULL;
    goto try_return_handler_3;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_DECREF( locals_pandas$io$clipboard$clipboards$$$function_7_init_no_clipboard_131 );
    locals_pandas$io$clipboard$clipboards$$$function_7_init_no_clipboard_131 = NULL;
    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_3;
    // End of try:
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$io$clipboard$clipboards$$$function_7_init_no_clipboard );
    return NULL;
    // Return handler code:
    try_return_handler_3:;
    CHECK_OBJECT( (PyObject *)outline_0_var___class__ );
    Py_DECREF( outline_0_var___class__ );
    outline_0_var___class__ = NULL;

    goto outline_result_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto outline_exception_1;
    // End of try:
    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$io$clipboard$clipboards$$$function_7_init_no_clipboard );
    return NULL;
    outline_exception_1:;
    exception_lineno = 131;
    goto try_except_handler_2;
    outline_result_1:;
    tmp_assign_source_4 = tmp_outline_return_value_1;
    assert( var_ClipboardUnavailable == NULL );
    var_ClipboardUnavailable = tmp_assign_source_4;

    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_decl_dict );
    Py_DECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_decl_dict );
    Py_DECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__metaclass );
    Py_DECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__prepared );
    Py_DECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    tmp_called_name_3 = var_ClipboardUnavailable;

    if ( tmp_called_name_3 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "ClipboardUnavailable" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 143;
        type_description_1 = "o";
        goto frame_exception_exit_1;
    }

    frame_1b1008a41b3a1e85a29e2c501f0885d0->m_frame.f_lineno = 143;
    tmp_tuple_element_2 = CALL_FUNCTION_NO_ARGS( tmp_called_name_3 );
    if ( tmp_tuple_element_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 143;
        type_description_1 = "o";
        goto frame_exception_exit_1;
    }
    tmp_return_value = PyTuple_New( 2 );
    PyTuple_SET_ITEM( tmp_return_value, 0, tmp_tuple_element_2 );
    tmp_called_name_4 = var_ClipboardUnavailable;

    if ( tmp_called_name_4 == NULL )
    {
        Py_DECREF( tmp_return_value );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "ClipboardUnavailable" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 143;
        type_description_1 = "o";
        goto frame_exception_exit_1;
    }

    frame_1b1008a41b3a1e85a29e2c501f0885d0->m_frame.f_lineno = 143;
    tmp_tuple_element_2 = CALL_FUNCTION_NO_ARGS( tmp_called_name_4 );
    if ( tmp_tuple_element_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_return_value );

        exception_lineno = 143;
        type_description_1 = "o";
        goto frame_exception_exit_1;
    }
    PyTuple_SET_ITEM( tmp_return_value, 1, tmp_tuple_element_2 );
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1b1008a41b3a1e85a29e2c501f0885d0 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_2;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_1b1008a41b3a1e85a29e2c501f0885d0 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1b1008a41b3a1e85a29e2c501f0885d0 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_1b1008a41b3a1e85a29e2c501f0885d0, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_1b1008a41b3a1e85a29e2c501f0885d0->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_1b1008a41b3a1e85a29e2c501f0885d0, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_1b1008a41b3a1e85a29e2c501f0885d0,
        type_description_1,
        var_ClipboardUnavailable
    );


    // Release cached frame.
    if ( frame_1b1008a41b3a1e85a29e2c501f0885d0 == cache_frame_1b1008a41b3a1e85a29e2c501f0885d0 )
    {
        Py_DECREF( frame_1b1008a41b3a1e85a29e2c501f0885d0 );
    }
    cache_frame_1b1008a41b3a1e85a29e2c501f0885d0 = NULL;

    assertFrameObject( frame_1b1008a41b3a1e85a29e2c501f0885d0 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_2:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$io$clipboard$clipboards$$$function_7_init_no_clipboard );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( var_ClipboardUnavailable );
    var_ClipboardUnavailable = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( var_ClipboardUnavailable );
    var_ClipboardUnavailable = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$io$clipboard$clipboards$$$function_7_init_no_clipboard );
    return NULL;

function_exception_exit:
    Py_XDECREF( locals_pandas$io$clipboard$clipboards$$$function_7_init_no_clipboard_131 );
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:
    Py_XDECREF( locals_pandas$io$clipboard$clipboards$$$function_7_init_no_clipboard_131 );

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$io$clipboard$clipboards$$$function_7_init_no_clipboard$$$function_1___call__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_args = python_pars[ 1 ];
    PyObject *par_kwargs = python_pars[ 2 ];
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_args_element_name_1;
    PyObject *tmp_called_name_1;
    PyObject *tmp_raise_type_1;
    static struct Nuitka_FrameObject *cache_frame_d508f46656f5486f34a7ebeb22429281 = NULL;

    struct Nuitka_FrameObject *frame_d508f46656f5486f34a7ebeb22429281;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_d508f46656f5486f34a7ebeb22429281, codeobj_d508f46656f5486f34a7ebeb22429281, module_pandas$io$clipboard$clipboards, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_d508f46656f5486f34a7ebeb22429281 = cache_frame_d508f46656f5486f34a7ebeb22429281;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_d508f46656f5486f34a7ebeb22429281 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_d508f46656f5486f34a7ebeb22429281 ) == 2 ); // Frame stack

    // Framed code:
    tmp_called_name_1 = GET_STRING_DICT_VALUE( moduledict_pandas$io$clipboard$clipboards, (Nuitka_StringObject *)const_str_plain_PyperclipException );

    if (unlikely( tmp_called_name_1 == NULL ))
    {
        tmp_called_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PyperclipException );
    }

    if ( tmp_called_name_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "PyperclipException" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 134;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_1 = GET_STRING_DICT_VALUE( moduledict_pandas$io$clipboard$clipboards, (Nuitka_StringObject *)const_str_plain_EXCEPT_MSG );

    if (unlikely( tmp_args_element_name_1 == NULL ))
    {
        tmp_args_element_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_EXCEPT_MSG );
    }

    if ( tmp_args_element_name_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "EXCEPT_MSG" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 134;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    frame_d508f46656f5486f34a7ebeb22429281->m_frame.f_lineno = 134;
    {
        PyObject *call_args[] = { tmp_args_element_name_1 };
        tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
    }

    if ( tmp_raise_type_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 134;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    exception_type = tmp_raise_type_1;
    exception_lineno = 134;
    RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
    type_description_1 = "ooo";
    goto frame_exception_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d508f46656f5486f34a7ebeb22429281 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d508f46656f5486f34a7ebeb22429281 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_d508f46656f5486f34a7ebeb22429281, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_d508f46656f5486f34a7ebeb22429281->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_d508f46656f5486f34a7ebeb22429281, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_d508f46656f5486f34a7ebeb22429281,
        type_description_1,
        par_self,
        par_args,
        par_kwargs
    );


    // Release cached frame.
    if ( frame_d508f46656f5486f34a7ebeb22429281 == cache_frame_d508f46656f5486f34a7ebeb22429281 )
    {
        Py_DECREF( frame_d508f46656f5486f34a7ebeb22429281 );
    }
    cache_frame_d508f46656f5486f34a7ebeb22429281 = NULL;

    assertFrameObject( frame_d508f46656f5486f34a7ebeb22429281 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$io$clipboard$clipboards$$$function_7_init_no_clipboard$$$function_1___call__ );
    return NULL;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_self );
    par_self = NULL;

    Py_XDECREF( par_args );
    par_args = NULL;

    Py_XDECREF( par_kwargs );
    par_kwargs = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$io$clipboard$clipboards$$$function_7_init_no_clipboard$$$function_1___call__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

}


static PyObject *impl_pandas$io$clipboard$clipboards$$$function_7_init_no_clipboard$$$function_2___nonzero__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *tmp_return_value;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    tmp_return_value = Py_False;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$io$clipboard$clipboards$$$function_7_init_no_clipboard$$$function_2___nonzero__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$io$clipboard$clipboards$$$function_7_init_no_clipboard$$$function_2___nonzero__ );
    return NULL;

function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$io$clipboard$clipboards$$$function_7_init_no_clipboard$$$function_3___bool__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *tmp_return_value;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    tmp_return_value = Py_False;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$io$clipboard$clipboards$$$function_7_init_no_clipboard$$$function_3___bool__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$io$clipboard$clipboards$$$function_7_init_no_clipboard$$$function_3___bool__ );
    return NULL;

function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}



static PyObject *MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_1_init_osx_clipboard(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$io$clipboard$clipboards$$$function_1_init_osx_clipboard,
        const_str_plain_init_osx_clipboard,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_8934cb9e516698b66388f7d4ae2f6cdd,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$io$clipboard$clipboards,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_1_init_osx_clipboard$$$function_1_copy_osx(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$io$clipboard$clipboards$$$function_1_init_osx_clipboard$$$function_1_copy_osx,
        const_str_plain_copy_osx,
#if PYTHON_VERSION >= 300
        const_str_digest_0d36178cf1fdd7da1acd434235941e8a,
#endif
        codeobj_3a7a67941f36b22f99f07a9864c43d40,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$io$clipboard$clipboards,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_1_init_osx_clipboard$$$function_2_paste_osx(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$io$clipboard$clipboards$$$function_1_init_osx_clipboard$$$function_2_paste_osx,
        const_str_plain_paste_osx,
#if PYTHON_VERSION >= 300
        const_str_digest_fe278fbce1ae480c42f145639e1ea634,
#endif
        codeobj_4fb293be5988723048e8055c5d838fa9,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$io$clipboard$clipboards,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_2_init_gtk_clipboard(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$io$clipboard$clipboards$$$function_2_init_gtk_clipboard,
        const_str_plain_init_gtk_clipboard,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_0360e4d1df6ae1edcb843c4520bf78bf,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$io$clipboard$clipboards,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_2_init_gtk_clipboard$$$function_1_copy_gtk( struct Nuitka_CellObject *closure_gtk )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$io$clipboard$clipboards$$$function_2_init_gtk_clipboard$$$function_1_copy_gtk,
        const_str_plain_copy_gtk,
#if PYTHON_VERSION >= 300
        const_str_digest_363b20897abb8fe206a2ed773d13d28a,
#endif
        codeobj_bbe75b4ebd91ab9b61b71ddb6993e4fa,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$io$clipboard$clipboards,
        Py_None,
        1
    );

result->m_closure[0] = closure_gtk;
Py_INCREF( result->m_closure[0] );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_2_init_gtk_clipboard$$$function_2_paste_gtk( struct Nuitka_CellObject *closure_gtk )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$io$clipboard$clipboards$$$function_2_init_gtk_clipboard$$$function_2_paste_gtk,
        const_str_plain_paste_gtk,
#if PYTHON_VERSION >= 300
        const_str_digest_12824c42ae734a5ec21969db08809d5c,
#endif
        codeobj_46465757b3b8fd2109993fc577b505af,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$io$clipboard$clipboards,
        Py_None,
        1
    );

result->m_closure[0] = closure_gtk;
Py_INCREF( result->m_closure[0] );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_3_init_qt_clipboard(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$io$clipboard$clipboards$$$function_3_init_qt_clipboard,
        const_str_plain_init_qt_clipboard,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_f5a793ca030f17ceec59cb6cf88afc11,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$io$clipboard$clipboards,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_3_init_qt_clipboard$$$function_1_copy_qt( struct Nuitka_CellObject *closure_app )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$io$clipboard$clipboards$$$function_3_init_qt_clipboard$$$function_1_copy_qt,
        const_str_plain_copy_qt,
#if PYTHON_VERSION >= 300
        const_str_digest_88e1244ea918f637f982de49331de205,
#endif
        codeobj_40eba54f9f7106c02033be5247abbf8c,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$io$clipboard$clipboards,
        Py_None,
        1
    );

result->m_closure[0] = closure_app;
Py_INCREF( result->m_closure[0] );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_3_init_qt_clipboard$$$function_2_paste_qt( struct Nuitka_CellObject *closure_app )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$io$clipboard$clipboards$$$function_3_init_qt_clipboard$$$function_2_paste_qt,
        const_str_plain_paste_qt,
#if PYTHON_VERSION >= 300
        const_str_digest_aefe37d186409357e48b054613e2a70f,
#endif
        codeobj_f305a7321124f97a9f33318e46ca5996,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$io$clipboard$clipboards,
        Py_None,
        1
    );

result->m_closure[0] = closure_app;
Py_INCREF( result->m_closure[0] );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_4_init_xclip_clipboard(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$io$clipboard$clipboards$$$function_4_init_xclip_clipboard,
        const_str_plain_init_xclip_clipboard,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_78a5d34c6b0186cda416b593e4be6914,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$io$clipboard$clipboards,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_4_init_xclip_clipboard$$$function_1_copy_xclip(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$io$clipboard$clipboards$$$function_4_init_xclip_clipboard$$$function_1_copy_xclip,
        const_str_plain_copy_xclip,
#if PYTHON_VERSION >= 300
        const_str_digest_488addb9f0a7f06aba6f2490db82d71c,
#endif
        codeobj_1692a6eebf291b9f98c65b665e9ae3e0,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$io$clipboard$clipboards,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_4_init_xclip_clipboard$$$function_2_paste_xclip(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$io$clipboard$clipboards$$$function_4_init_xclip_clipboard$$$function_2_paste_xclip,
        const_str_plain_paste_xclip,
#if PYTHON_VERSION >= 300
        const_str_digest_e75ca76f27e0e640a58096dff4d2ac2a,
#endif
        codeobj_de0bad81b2cc52a2e72b9f4e0d8d0cb6,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$io$clipboard$clipboards,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_5_init_xsel_clipboard(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$io$clipboard$clipboards$$$function_5_init_xsel_clipboard,
        const_str_plain_init_xsel_clipboard,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_f1b78c0038fb3fe45086095e1029c853,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$io$clipboard$clipboards,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_5_init_xsel_clipboard$$$function_1_copy_xsel(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$io$clipboard$clipboards$$$function_5_init_xsel_clipboard$$$function_1_copy_xsel,
        const_str_plain_copy_xsel,
#if PYTHON_VERSION >= 300
        const_str_digest_7f6d117bc092141e51ea443a0e0999c2,
#endif
        codeobj_24f1f9c5a5b1b760d9c1c3c6ad13fcd7,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$io$clipboard$clipboards,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_5_init_xsel_clipboard$$$function_2_paste_xsel(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$io$clipboard$clipboards$$$function_5_init_xsel_clipboard$$$function_2_paste_xsel,
        const_str_plain_paste_xsel,
#if PYTHON_VERSION >= 300
        const_str_digest_9c1b8d1bf096eeae592150bfe0b7f351,
#endif
        codeobj_17129171ff48345b5f382b101aeaade1,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$io$clipboard$clipboards,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_6_init_klipper_clipboard(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$io$clipboard$clipboards$$$function_6_init_klipper_clipboard,
        const_str_plain_init_klipper_clipboard,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_fec8e9c26b096417568201a5035234a3,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$io$clipboard$clipboards,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_6_init_klipper_clipboard$$$function_1_copy_klipper(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$io$clipboard$clipboards$$$function_6_init_klipper_clipboard$$$function_1_copy_klipper,
        const_str_plain_copy_klipper,
#if PYTHON_VERSION >= 300
        const_str_digest_d3a192d9a90f12283eb154da5edd57d5,
#endif
        codeobj_8350dbeca557534164273aa396913a15,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$io$clipboard$clipboards,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_6_init_klipper_clipboard$$$function_2_paste_klipper(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$io$clipboard$clipboards$$$function_6_init_klipper_clipboard$$$function_2_paste_klipper,
        const_str_plain_paste_klipper,
#if PYTHON_VERSION >= 300
        const_str_digest_b75401c8f73575104762484b09641fec,
#endif
        codeobj_641b96a224a5ed333f02bedaf0f5eed7,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$io$clipboard$clipboards,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_7_init_no_clipboard(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$io$clipboard$clipboards$$$function_7_init_no_clipboard,
        const_str_plain_init_no_clipboard,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_1b1008a41b3a1e85a29e2c501f0885d0,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$io$clipboard$clipboards,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_7_init_no_clipboard$$$function_1___call__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$io$clipboard$clipboards$$$function_7_init_no_clipboard$$$function_1___call__,
        const_str_plain___call__,
#if PYTHON_VERSION >= 300
        const_str_digest_77292a4a8d17a0319dc328f25380c431,
#endif
        codeobj_d508f46656f5486f34a7ebeb22429281,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$io$clipboard$clipboards,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_7_init_no_clipboard$$$function_2___nonzero__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$io$clipboard$clipboards$$$function_7_init_no_clipboard$$$function_2___nonzero__,
        const_str_plain___nonzero__,
#if PYTHON_VERSION >= 300
        const_str_digest_01d040ac9dfb1f7d0f957fcd5293298f,
#endif
        codeobj_7e43cdf22d01e6a3a52aa4651088efe8,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$io$clipboard$clipboards,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_7_init_no_clipboard$$$function_3___bool__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$io$clipboard$clipboards$$$function_7_init_no_clipboard$$$function_3___bool__,
        const_str_plain___bool__,
#if PYTHON_VERSION >= 300
        const_str_digest_d577dede9743e2b3fad57acdda45ba59,
#endif
        codeobj_769dfc462486ce80fc0bc0d1c8e7624d,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$io$clipboard$clipboards,
        Py_None,
        0
    );


    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_pandas$io$clipboard$clipboards =
{
    PyModuleDef_HEAD_INIT,
    "pandas.io.clipboard.clipboards",   /* m_name */
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;

extern PyObject *const_str_plain___loader__;
extern PyObject *metapath_based_loader;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL.

MOD_INIT_DECL( pandas$io$clipboard$clipboards )
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_pandas$io$clipboard$clipboards );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("pandas.io.clipboard.clipboards: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("pandas.io.clipboard.clipboards: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("pandas.io.clipboard.clipboards: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initpandas$io$clipboard$clipboards" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_pandas$io$clipboard$clipboards = Py_InitModule4(
        "pandas.io.clipboard.clipboards",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_pandas$io$clipboard$clipboards = PyModule_Create( &mdef_pandas$io$clipboard$clipboards );
#endif

    moduledict_pandas$io$clipboard$clipboards = MODULE_DICT( module_pandas$io$clipboard$clipboards );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_pandas$io$clipboard$clipboards, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_pandas$io$clipboard$clipboards,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );

#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_pandas$io$clipboard$clipboards, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_pandas$io$clipboard$clipboards,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_pandas$io$clipboard$clipboards, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_pandas$io$clipboard$clipboards,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_pandas$io$clipboard$clipboards );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PySys_GetObject( (char *)"modules" ), const_str_digest_c1b240794be4eea191dd0f8d775fee7d, module_pandas$io$clipboard$clipboards );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_pandas$io$clipboard$clipboards, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_pandas$io$clipboard$clipboards, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_pandas$io$clipboard$clipboards, (Nuitka_StringObject *)const_str_plain___loader__, metapath_based_loader );
#endif

#if PYTHON_VERSION >= 340
#if 0
    UPDATE_STRING_DICT0( moduledict_pandas$io$clipboard$clipboards, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_pandas$io$clipboard$clipboards, (Nuitka_StringObject *)const_str_plain___name__ ),
            metapath_based_loader
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );

        UPDATE_STRING_DICT1( moduledict_pandas$io$clipboard$clipboards, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );

        Py_DECREF( module_spec_class );
    }
#endif
#endif


    // Temp variables if any
    PyObject *tmp_import_from_1__module = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_assign_source_5;
    PyObject *tmp_assign_source_6;
    PyObject *tmp_assign_source_7;
    PyObject *tmp_assign_source_8;
    PyObject *tmp_assign_source_9;
    PyObject *tmp_assign_source_10;
    PyObject *tmp_assign_source_11;
    PyObject *tmp_assign_source_12;
    PyObject *tmp_assign_source_13;
    PyObject *tmp_assign_source_14;
    PyObject *tmp_assign_source_15;
    PyObject *tmp_assign_source_16;
    PyObject *tmp_fromlist_name_1;
    PyObject *tmp_fromlist_name_2;
    PyObject *tmp_fromlist_name_3;
    PyObject *tmp_globals_name_1;
    PyObject *tmp_globals_name_2;
    PyObject *tmp_globals_name_3;
    PyObject *tmp_import_name_from_1;
    PyObject *tmp_import_name_from_2;
    PyObject *tmp_import_name_from_3;
    PyObject *tmp_level_name_1;
    PyObject *tmp_level_name_2;
    PyObject *tmp_level_name_3;
    PyObject *tmp_locals_name_1;
    PyObject *tmp_locals_name_2;
    PyObject *tmp_locals_name_3;
    PyObject *tmp_name_name_1;
    PyObject *tmp_name_name_2;
    PyObject *tmp_name_name_3;
    struct Nuitka_FrameObject *frame_127047bff6d7dcdd52e5c8eaa1f1c927;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;

    // Module code.
    tmp_assign_source_1 = Py_None;
    UPDATE_STRING_DICT0( moduledict_pandas$io$clipboard$clipboards, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    tmp_assign_source_2 = const_str_digest_3134b871d582f11ef1d443dc3acee782;
    UPDATE_STRING_DICT0( moduledict_pandas$io$clipboard$clipboards, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    tmp_assign_source_3 = Py_None;
    UPDATE_STRING_DICT0( moduledict_pandas$io$clipboard$clipboards, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    // Frame without reuse.
    frame_127047bff6d7dcdd52e5c8eaa1f1c927 = MAKE_MODULE_FRAME( codeobj_127047bff6d7dcdd52e5c8eaa1f1c927, module_pandas$io$clipboard$clipboards );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_127047bff6d7dcdd52e5c8eaa1f1c927 );
    assert( Py_REFCNT( frame_127047bff6d7dcdd52e5c8eaa1f1c927 ) == 2 );

    // Framed code:
    tmp_name_name_1 = const_str_plain_subprocess;
    tmp_globals_name_1 = (PyObject *)moduledict_pandas$io$clipboard$clipboards;
    tmp_locals_name_1 = Py_None;
    tmp_fromlist_name_1 = Py_None;
    tmp_level_name_1 = const_int_0;
    frame_127047bff6d7dcdd52e5c8eaa1f1c927->m_frame.f_lineno = 1;
    tmp_assign_source_4 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
    if ( tmp_assign_source_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 1;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$io$clipboard$clipboards, (Nuitka_StringObject *)const_str_plain_subprocess, tmp_assign_source_4 );
    tmp_name_name_2 = const_str_plain_exceptions;
    tmp_globals_name_2 = (PyObject *)moduledict_pandas$io$clipboard$clipboards;
    tmp_locals_name_2 = Py_None;
    tmp_fromlist_name_2 = const_tuple_str_plain_PyperclipException_tuple;
    tmp_level_name_2 = const_int_pos_1;
    frame_127047bff6d7dcdd52e5c8eaa1f1c927->m_frame.f_lineno = 2;
    tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
    if ( tmp_import_name_from_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 2;

        goto frame_exception_exit_1;
    }
    if ( PyModule_Check( tmp_import_name_from_1 ) )
    {
       tmp_assign_source_5 = IMPORT_NAME_OR_MODULE(
            tmp_import_name_from_1,
            (PyObject *)MODULE_DICT(tmp_import_name_from_1),
            const_str_plain_PyperclipException,
            const_int_pos_1
        );
    }
    else
    {
       tmp_assign_source_5 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_PyperclipException );
    }

    Py_DECREF( tmp_import_name_from_1 );
    if ( tmp_assign_source_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 2;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$io$clipboard$clipboards, (Nuitka_StringObject *)const_str_plain_PyperclipException, tmp_assign_source_5 );
    tmp_name_name_3 = const_str_digest_77d8fb15a813ae44a7b8d0fe53dbee10;
    tmp_globals_name_3 = (PyObject *)moduledict_pandas$io$clipboard$clipboards;
    tmp_locals_name_3 = Py_None;
    tmp_fromlist_name_3 = const_tuple_str_plain_PY2_str_plain_text_type_tuple;
    tmp_level_name_3 = const_int_0;
    frame_127047bff6d7dcdd52e5c8eaa1f1c927->m_frame.f_lineno = 3;
    tmp_assign_source_6 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
    if ( tmp_assign_source_6 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 3;

        goto frame_exception_exit_1;
    }
    assert( tmp_import_from_1__module == NULL );
    tmp_import_from_1__module = tmp_assign_source_6;

    // Tried code:
    tmp_import_name_from_2 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_2 );
    tmp_assign_source_7 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_PY2 );
    if ( tmp_assign_source_7 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 3;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$io$clipboard$clipboards, (Nuitka_StringObject *)const_str_plain_PY2, tmp_assign_source_7 );
    tmp_import_name_from_3 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_3 );
    tmp_assign_source_8 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_text_type );
    if ( tmp_assign_source_8 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 3;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$io$clipboard$clipboards, (Nuitka_StringObject *)const_str_plain_text_type, tmp_assign_source_8 );
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_127047bff6d7dcdd52e5c8eaa1f1c927 );
#endif
    popFrameStack();

    assertFrameObject( frame_127047bff6d7dcdd52e5c8eaa1f1c927 );

    goto frame_no_exception_1;
    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_127047bff6d7dcdd52e5c8eaa1f1c927 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_127047bff6d7dcdd52e5c8eaa1f1c927, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_127047bff6d7dcdd52e5c8eaa1f1c927->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_127047bff6d7dcdd52e5c8eaa1f1c927, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;
    frame_no_exception_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    tmp_assign_source_9 = const_str_digest_b8883e772664a69f81a6c1eee0e6292f;
    UPDATE_STRING_DICT0( moduledict_pandas$io$clipboard$clipboards, (Nuitka_StringObject *)const_str_plain_EXCEPT_MSG, tmp_assign_source_9 );
    tmp_assign_source_10 = MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_1_init_osx_clipboard(  );
    UPDATE_STRING_DICT1( moduledict_pandas$io$clipboard$clipboards, (Nuitka_StringObject *)const_str_plain_init_osx_clipboard, tmp_assign_source_10 );
    tmp_assign_source_11 = MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_2_init_gtk_clipboard(  );
    UPDATE_STRING_DICT1( moduledict_pandas$io$clipboard$clipboards, (Nuitka_StringObject *)const_str_plain_init_gtk_clipboard, tmp_assign_source_11 );
    tmp_assign_source_12 = MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_3_init_qt_clipboard(  );
    UPDATE_STRING_DICT1( moduledict_pandas$io$clipboard$clipboards, (Nuitka_StringObject *)const_str_plain_init_qt_clipboard, tmp_assign_source_12 );
    tmp_assign_source_13 = MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_4_init_xclip_clipboard(  );
    UPDATE_STRING_DICT1( moduledict_pandas$io$clipboard$clipboards, (Nuitka_StringObject *)const_str_plain_init_xclip_clipboard, tmp_assign_source_13 );
    tmp_assign_source_14 = MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_5_init_xsel_clipboard(  );
    UPDATE_STRING_DICT1( moduledict_pandas$io$clipboard$clipboards, (Nuitka_StringObject *)const_str_plain_init_xsel_clipboard, tmp_assign_source_14 );
    tmp_assign_source_15 = MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_6_init_klipper_clipboard(  );
    UPDATE_STRING_DICT1( moduledict_pandas$io$clipboard$clipboards, (Nuitka_StringObject *)const_str_plain_init_klipper_clipboard, tmp_assign_source_15 );
    tmp_assign_source_16 = MAKE_FUNCTION_pandas$io$clipboard$clipboards$$$function_7_init_no_clipboard(  );
    UPDATE_STRING_DICT1( moduledict_pandas$io$clipboard$clipboards, (Nuitka_StringObject *)const_str_plain_init_no_clipboard, tmp_assign_source_16 );

    return MOD_RETURN_VALUE( module_pandas$io$clipboard$clipboards );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
