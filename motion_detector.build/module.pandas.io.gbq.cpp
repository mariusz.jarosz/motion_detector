/* Generated code for Python source for module 'pandas.io.gbq'
 * created by Nuitka version 0.5.32.7
 *
 * This code is in part copyright 2018 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The _module_pandas$io$gbq is a Python object pointer of module type. */

/* Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_pandas$io$gbq;
PyDictObject *moduledict_pandas$io$gbq;

/* The module constants used, if any. */
static PyObject *const_tuple_25fdb1a3f95975b978e7d2d2f8c607f2_tuple;
extern PyObject *const_str_plain_destination_table;
extern PyObject *const_str_plain_auth_local_webserver;
extern PyObject *const_str_plain_legacy;
static PyObject *const_tuple_str_plain_pandas_gbq_tuple;
extern PyObject *const_str_plain_verbose;
extern PyObject *const_str_plain_False;
static PyObject *const_str_digest_cc40cb205ef9f0fc5d230af97ce9468f;
extern PyObject *const_str_plain___file__;
static PyObject *const_str_digest_a072f02d14e0765940241fb1cb4ab6cc;
extern PyObject *const_str_plain_col_order;
static PyObject *const_str_digest_1db717379ebce8259bca03d74b598b30;
extern PyObject *const_int_0;
extern PyObject *const_str_plain_private_key;
extern PyObject *const_str_digest_75090e6ed0be6fa03614e573a9d579f0;
extern PyObject *const_str_plain_dataframe;
extern PyObject *const_str_plain_query;
static PyObject *const_tuple_0710a4338b0fddbb97d194c1a7b15676_tuple;
extern PyObject *const_str_plain_to_gbq;
extern PyObject *const_str_plain_dialect;
extern PyObject *const_str_plain_None;
static PyObject *const_str_digest_31c75016cd4dee587fe0cb5bcc2904a0;
extern PyObject *const_tuple_none_none_false_str_plain_fail_none_false_none_tuple;
extern PyObject *const_str_plain_if_exists;
extern PyObject *const_str_plain_kwargs;
static PyObject *const_tuple_none_none_none_false_none_none_str_plain_legacy_tuple;
extern PyObject *const_str_plain_chunksize;
extern PyObject *const_str_plain_reauth;
extern PyObject *const_str_plain_project_id;
extern PyObject *const_str_plain_read_gbq;
extern PyObject *const_str_plain__try_import;
extern PyObject *const_str_plain_index_col;
extern PyObject *const_str_plain_table_schema;
extern PyObject *const_str_plain_pandas_gbq;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_plain_fail;
static PyObject *module_filename_obj;

static bool constants_created = false;

static void createModuleConstants( void )
{
    const_tuple_25fdb1a3f95975b978e7d2d2f8c607f2_tuple = PyTuple_New( 11 );
    PyTuple_SET_ITEM( const_tuple_25fdb1a3f95975b978e7d2d2f8c607f2_tuple, 0, const_str_plain_dataframe ); Py_INCREF( const_str_plain_dataframe );
    PyTuple_SET_ITEM( const_tuple_25fdb1a3f95975b978e7d2d2f8c607f2_tuple, 1, const_str_plain_destination_table ); Py_INCREF( const_str_plain_destination_table );
    PyTuple_SET_ITEM( const_tuple_25fdb1a3f95975b978e7d2d2f8c607f2_tuple, 2, const_str_plain_project_id ); Py_INCREF( const_str_plain_project_id );
    PyTuple_SET_ITEM( const_tuple_25fdb1a3f95975b978e7d2d2f8c607f2_tuple, 3, const_str_plain_chunksize ); Py_INCREF( const_str_plain_chunksize );
    PyTuple_SET_ITEM( const_tuple_25fdb1a3f95975b978e7d2d2f8c607f2_tuple, 4, const_str_plain_verbose ); Py_INCREF( const_str_plain_verbose );
    PyTuple_SET_ITEM( const_tuple_25fdb1a3f95975b978e7d2d2f8c607f2_tuple, 5, const_str_plain_reauth ); Py_INCREF( const_str_plain_reauth );
    PyTuple_SET_ITEM( const_tuple_25fdb1a3f95975b978e7d2d2f8c607f2_tuple, 6, const_str_plain_if_exists ); Py_INCREF( const_str_plain_if_exists );
    PyTuple_SET_ITEM( const_tuple_25fdb1a3f95975b978e7d2d2f8c607f2_tuple, 7, const_str_plain_private_key ); Py_INCREF( const_str_plain_private_key );
    PyTuple_SET_ITEM( const_tuple_25fdb1a3f95975b978e7d2d2f8c607f2_tuple, 8, const_str_plain_auth_local_webserver ); Py_INCREF( const_str_plain_auth_local_webserver );
    PyTuple_SET_ITEM( const_tuple_25fdb1a3f95975b978e7d2d2f8c607f2_tuple, 9, const_str_plain_table_schema ); Py_INCREF( const_str_plain_table_schema );
    PyTuple_SET_ITEM( const_tuple_25fdb1a3f95975b978e7d2d2f8c607f2_tuple, 10, const_str_plain_pandas_gbq ); Py_INCREF( const_str_plain_pandas_gbq );
    const_tuple_str_plain_pandas_gbq_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_pandas_gbq_tuple, 0, const_str_plain_pandas_gbq ); Py_INCREF( const_str_plain_pandas_gbq );
    const_str_digest_cc40cb205ef9f0fc5d230af97ce9468f = UNSTREAM_STRING( &constant_bin[ 3033989 ], 79, 0 );
    const_str_digest_a072f02d14e0765940241fb1cb4ab6cc = UNSTREAM_STRING( &constant_bin[ 3034068 ], 2797, 0 );
    const_str_digest_1db717379ebce8259bca03d74b598b30 = UNSTREAM_STRING( &constant_bin[ 3036865 ], 218, 0 );
    const_tuple_0710a4338b0fddbb97d194c1a7b15676_tuple = PyTuple_New( 10 );
    PyTuple_SET_ITEM( const_tuple_0710a4338b0fddbb97d194c1a7b15676_tuple, 0, const_str_plain_query ); Py_INCREF( const_str_plain_query );
    PyTuple_SET_ITEM( const_tuple_0710a4338b0fddbb97d194c1a7b15676_tuple, 1, const_str_plain_project_id ); Py_INCREF( const_str_plain_project_id );
    PyTuple_SET_ITEM( const_tuple_0710a4338b0fddbb97d194c1a7b15676_tuple, 2, const_str_plain_index_col ); Py_INCREF( const_str_plain_index_col );
    PyTuple_SET_ITEM( const_tuple_0710a4338b0fddbb97d194c1a7b15676_tuple, 3, const_str_plain_col_order ); Py_INCREF( const_str_plain_col_order );
    PyTuple_SET_ITEM( const_tuple_0710a4338b0fddbb97d194c1a7b15676_tuple, 4, const_str_plain_reauth ); Py_INCREF( const_str_plain_reauth );
    PyTuple_SET_ITEM( const_tuple_0710a4338b0fddbb97d194c1a7b15676_tuple, 5, const_str_plain_verbose ); Py_INCREF( const_str_plain_verbose );
    PyTuple_SET_ITEM( const_tuple_0710a4338b0fddbb97d194c1a7b15676_tuple, 6, const_str_plain_private_key ); Py_INCREF( const_str_plain_private_key );
    PyTuple_SET_ITEM( const_tuple_0710a4338b0fddbb97d194c1a7b15676_tuple, 7, const_str_plain_dialect ); Py_INCREF( const_str_plain_dialect );
    PyTuple_SET_ITEM( const_tuple_0710a4338b0fddbb97d194c1a7b15676_tuple, 8, const_str_plain_kwargs ); Py_INCREF( const_str_plain_kwargs );
    PyTuple_SET_ITEM( const_tuple_0710a4338b0fddbb97d194c1a7b15676_tuple, 9, const_str_plain_pandas_gbq ); Py_INCREF( const_str_plain_pandas_gbq );
    const_str_digest_31c75016cd4dee587fe0cb5bcc2904a0 = UNSTREAM_STRING( &constant_bin[ 3037083 ], 25, 0 );
    const_tuple_none_none_none_false_none_none_str_plain_legacy_tuple = PyTuple_New( 7 );
    PyTuple_SET_ITEM( const_tuple_none_none_none_false_none_none_str_plain_legacy_tuple, 0, Py_None ); Py_INCREF( Py_None );
    PyTuple_SET_ITEM( const_tuple_none_none_none_false_none_none_str_plain_legacy_tuple, 1, Py_None ); Py_INCREF( Py_None );
    PyTuple_SET_ITEM( const_tuple_none_none_none_false_none_none_str_plain_legacy_tuple, 2, Py_None ); Py_INCREF( Py_None );
    PyTuple_SET_ITEM( const_tuple_none_none_none_false_none_none_str_plain_legacy_tuple, 3, Py_False ); Py_INCREF( Py_False );
    PyTuple_SET_ITEM( const_tuple_none_none_none_false_none_none_str_plain_legacy_tuple, 4, Py_None ); Py_INCREF( Py_None );
    PyTuple_SET_ITEM( const_tuple_none_none_none_false_none_none_str_plain_legacy_tuple, 5, Py_None ); Py_INCREF( Py_None );
    PyTuple_SET_ITEM( const_tuple_none_none_none_false_none_none_str_plain_legacy_tuple, 6, const_str_plain_legacy ); Py_INCREF( const_str_plain_legacy );

    constants_created = true;
}

#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_pandas$io$gbq( void )
{
    // The module may not have been used at all.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_0855ce15ff9fa15357602d7ec5a88f11;
static PyCodeObject *codeobj_3b40f534526dd86c1380d98f5d0129f4;
static PyCodeObject *codeobj_02e253b331f3c1fc8b46df9e0a589873;

static void createModuleCodeObjects(void)
{
    module_filename_obj = const_str_digest_cc40cb205ef9f0fc5d230af97ce9468f;
    codeobj_0855ce15ff9fa15357602d7ec5a88f11 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__try_import, 4, const_tuple_str_plain_pandas_gbq_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_3b40f534526dd86c1380d98f5d0129f4 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_read_gbq, 24, const_tuple_0710a4338b0fddbb97d194c1a7b15676_tuple, 8, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARKEYWORDS | CO_NOFREE );
    codeobj_02e253b331f3c1fc8b46df9e0a589873 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_to_gbq, 111, const_tuple_25fdb1a3f95975b978e7d2d2f8c607f2_tuple, 10, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
}

// The module function declarations.
NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_6_complex_call_helper_pos_keywords_star_dict( PyObject **python_pars );


static PyObject *MAKE_FUNCTION_pandas$io$gbq$$$function_1__try_import(  );


static PyObject *MAKE_FUNCTION_pandas$io$gbq$$$function_2_read_gbq( PyObject *defaults );


static PyObject *MAKE_FUNCTION_pandas$io$gbq$$$function_3_to_gbq( PyObject *defaults );


// The module function definitions.
static PyObject *impl_pandas$io$gbq$$$function_1__try_import( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *var_pandas_gbq = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_compare_left_1;
    PyObject *tmp_compare_right_1;
    int tmp_exc_match_exception_match_1;
    PyObject *tmp_fromlist_name_1;
    PyObject *tmp_globals_name_1;
    PyObject *tmp_level_name_1;
    PyObject *tmp_locals_name_1;
    PyObject *tmp_make_exception_arg_1;
    PyObject *tmp_name_name_1;
    PyObject *tmp_raise_type_1;
    bool tmp_result;
    PyObject *tmp_return_value;
    static struct Nuitka_FrameObject *cache_frame_0855ce15ff9fa15357602d7ec5a88f11 = NULL;

    struct Nuitka_FrameObject *frame_0855ce15ff9fa15357602d7ec5a88f11;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_0855ce15ff9fa15357602d7ec5a88f11, codeobj_0855ce15ff9fa15357602d7ec5a88f11, module_pandas$io$gbq, sizeof(void *) );
    frame_0855ce15ff9fa15357602d7ec5a88f11 = cache_frame_0855ce15ff9fa15357602d7ec5a88f11;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_0855ce15ff9fa15357602d7ec5a88f11 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_0855ce15ff9fa15357602d7ec5a88f11 ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    tmp_name_name_1 = const_str_plain_pandas_gbq;
    tmp_globals_name_1 = (PyObject *)moduledict_pandas$io$gbq;
    tmp_locals_name_1 = Py_None;
    tmp_fromlist_name_1 = Py_None;
    tmp_level_name_1 = const_int_0;
    frame_0855ce15ff9fa15357602d7ec5a88f11->m_frame.f_lineno = 8;
    tmp_assign_source_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
    if ( tmp_assign_source_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 8;
        type_description_1 = "o";
        goto try_except_handler_2;
    }
    assert( var_pandas_gbq == NULL );
    var_pandas_gbq = tmp_assign_source_1;

    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_0855ce15ff9fa15357602d7ec5a88f11, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_0855ce15ff9fa15357602d7ec5a88f11, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    // Tried code:
    tmp_compare_left_1 = EXC_TYPE(PyThreadState_GET());
    tmp_compare_right_1 = PyExc_ImportError;
    tmp_exc_match_exception_match_1 = EXCEPTION_MATCH_BOOL( tmp_compare_left_1, tmp_compare_right_1 );
    if ( tmp_exc_match_exception_match_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 9;
        type_description_1 = "o";
        goto try_except_handler_3;
    }
    if ( tmp_exc_match_exception_match_1 == 1 )
    {
        goto branch_yes_1;
    }
    else
    {
        goto branch_no_1;
    }
    branch_yes_1:;
    tmp_make_exception_arg_1 = const_str_digest_1db717379ebce8259bca03d74b598b30;
    frame_0855ce15ff9fa15357602d7ec5a88f11->m_frame.f_lineno = 12;
    {
        PyObject *call_args[] = { tmp_make_exception_arg_1 };
        tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_ImportError, call_args );
    }

    assert( !(tmp_raise_type_1 == NULL) );
    exception_type = tmp_raise_type_1;
    exception_lineno = 12;
    RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
    type_description_1 = "o";
    goto try_except_handler_3;
    goto branch_end_1;
    branch_no_1:;
    tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (unlikely( tmp_result == false ))
    {
        exception_lineno = 7;
    }

    if (exception_tb && exception_tb->tb_frame == &frame_0855ce15ff9fa15357602d7ec5a88f11->m_frame) frame_0855ce15ff9fa15357602d7ec5a88f11->m_frame.f_lineno = exception_tb->tb_lineno;
    type_description_1 = "o";
    goto try_except_handler_3;
    branch_end_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$io$gbq$$$function_1__try_import );
    return NULL;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    // End of try:
    try_end_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0855ce15ff9fa15357602d7ec5a88f11 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0855ce15ff9fa15357602d7ec5a88f11 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_0855ce15ff9fa15357602d7ec5a88f11, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_0855ce15ff9fa15357602d7ec5a88f11->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_0855ce15ff9fa15357602d7ec5a88f11, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_0855ce15ff9fa15357602d7ec5a88f11,
        type_description_1,
        var_pandas_gbq
    );


    // Release cached frame.
    if ( frame_0855ce15ff9fa15357602d7ec5a88f11 == cache_frame_0855ce15ff9fa15357602d7ec5a88f11 )
    {
        Py_DECREF( frame_0855ce15ff9fa15357602d7ec5a88f11 );
    }
    cache_frame_0855ce15ff9fa15357602d7ec5a88f11 = NULL;

    assertFrameObject( frame_0855ce15ff9fa15357602d7ec5a88f11 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    tmp_return_value = var_pandas_gbq;

    CHECK_OBJECT( tmp_return_value );
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$io$gbq$$$function_1__try_import );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)var_pandas_gbq );
    Py_DECREF( var_pandas_gbq );
    var_pandas_gbq = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$io$gbq$$$function_1__try_import );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$io$gbq$$$function_2_read_gbq( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_query = python_pars[ 0 ];
    PyObject *par_project_id = python_pars[ 1 ];
    PyObject *par_index_col = python_pars[ 2 ];
    PyObject *par_col_order = python_pars[ 3 ];
    PyObject *par_reauth = python_pars[ 4 ];
    PyObject *par_verbose = python_pars[ 5 ];
    PyObject *par_private_key = python_pars[ 6 ];
    PyObject *par_dialect = python_pars[ 7 ];
    PyObject *par_kwargs = python_pars[ 8 ];
    PyObject *var_pandas_gbq = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_called_name_1;
    PyObject *tmp_dict_key_1;
    PyObject *tmp_dict_key_2;
    PyObject *tmp_dict_key_3;
    PyObject *tmp_dict_key_4;
    PyObject *tmp_dict_key_5;
    PyObject *tmp_dict_key_6;
    PyObject *tmp_dict_key_7;
    PyObject *tmp_dict_value_1;
    PyObject *tmp_dict_value_2;
    PyObject *tmp_dict_value_3;
    PyObject *tmp_dict_value_4;
    PyObject *tmp_dict_value_5;
    PyObject *tmp_dict_value_6;
    PyObject *tmp_dict_value_7;
    PyObject *tmp_dircall_arg1_1;
    PyObject *tmp_dircall_arg2_1;
    PyObject *tmp_dircall_arg3_1;
    PyObject *tmp_dircall_arg4_1;
    int tmp_res;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    PyObject *tmp_tuple_element_1;
    static struct Nuitka_FrameObject *cache_frame_3b40f534526dd86c1380d98f5d0129f4 = NULL;

    struct Nuitka_FrameObject *frame_3b40f534526dd86c1380d98f5d0129f4;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_3b40f534526dd86c1380d98f5d0129f4, codeobj_3b40f534526dd86c1380d98f5d0129f4, module_pandas$io$gbq, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_3b40f534526dd86c1380d98f5d0129f4 = cache_frame_3b40f534526dd86c1380d98f5d0129f4;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_3b40f534526dd86c1380d98f5d0129f4 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_3b40f534526dd86c1380d98f5d0129f4 ) == 2 ); // Frame stack

    // Framed code:
    tmp_called_name_1 = GET_STRING_DICT_VALUE( moduledict_pandas$io$gbq, (Nuitka_StringObject *)const_str_plain__try_import );

    if (unlikely( tmp_called_name_1 == NULL ))
    {
        tmp_called_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__try_import );
    }

    if ( tmp_called_name_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_try_import" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 101;
        type_description_1 = "oooooooooo";
        goto frame_exception_exit_1;
    }

    frame_3b40f534526dd86c1380d98f5d0129f4->m_frame.f_lineno = 101;
    tmp_assign_source_1 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
    if ( tmp_assign_source_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 101;
        type_description_1 = "oooooooooo";
        goto frame_exception_exit_1;
    }
    assert( var_pandas_gbq == NULL );
    var_pandas_gbq = tmp_assign_source_1;

    tmp_source_name_1 = var_pandas_gbq;

    CHECK_OBJECT( tmp_source_name_1 );
    tmp_dircall_arg1_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_read_gbq );
    if ( tmp_dircall_arg1_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 102;
        type_description_1 = "oooooooooo";
        goto frame_exception_exit_1;
    }
    tmp_tuple_element_1 = par_query;

    if ( tmp_tuple_element_1 == NULL )
    {
        Py_DECREF( tmp_dircall_arg1_1 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "query" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 103;
        type_description_1 = "oooooooooo";
        goto frame_exception_exit_1;
    }

    tmp_dircall_arg2_1 = PyTuple_New( 1 );
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_dircall_arg2_1, 0, tmp_tuple_element_1 );
    tmp_dict_key_1 = const_str_plain_project_id;
    tmp_dict_value_1 = par_project_id;

    if ( tmp_dict_value_1 == NULL )
    {
        Py_DECREF( tmp_dircall_arg1_1 );
        Py_DECREF( tmp_dircall_arg2_1 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "project_id" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 103;
        type_description_1 = "oooooooooo";
        goto frame_exception_exit_1;
    }

    tmp_dircall_arg3_1 = _PyDict_NewPresized( 7 );
    tmp_res = PyDict_SetItem( tmp_dircall_arg3_1, tmp_dict_key_1, tmp_dict_value_1 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_2 = const_str_plain_index_col;
    tmp_dict_value_2 = par_index_col;

    if ( tmp_dict_value_2 == NULL )
    {
        Py_DECREF( tmp_dircall_arg1_1 );
        Py_DECREF( tmp_dircall_arg2_1 );
        Py_DECREF( tmp_dircall_arg3_1 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "index_col" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 104;
        type_description_1 = "oooooooooo";
        goto frame_exception_exit_1;
    }

    tmp_res = PyDict_SetItem( tmp_dircall_arg3_1, tmp_dict_key_2, tmp_dict_value_2 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_3 = const_str_plain_col_order;
    tmp_dict_value_3 = par_col_order;

    if ( tmp_dict_value_3 == NULL )
    {
        Py_DECREF( tmp_dircall_arg1_1 );
        Py_DECREF( tmp_dircall_arg2_1 );
        Py_DECREF( tmp_dircall_arg3_1 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "col_order" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 104;
        type_description_1 = "oooooooooo";
        goto frame_exception_exit_1;
    }

    tmp_res = PyDict_SetItem( tmp_dircall_arg3_1, tmp_dict_key_3, tmp_dict_value_3 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_4 = const_str_plain_reauth;
    tmp_dict_value_4 = par_reauth;

    if ( tmp_dict_value_4 == NULL )
    {
        Py_DECREF( tmp_dircall_arg1_1 );
        Py_DECREF( tmp_dircall_arg2_1 );
        Py_DECREF( tmp_dircall_arg3_1 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "reauth" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 105;
        type_description_1 = "oooooooooo";
        goto frame_exception_exit_1;
    }

    tmp_res = PyDict_SetItem( tmp_dircall_arg3_1, tmp_dict_key_4, tmp_dict_value_4 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_5 = const_str_plain_verbose;
    tmp_dict_value_5 = par_verbose;

    if ( tmp_dict_value_5 == NULL )
    {
        Py_DECREF( tmp_dircall_arg1_1 );
        Py_DECREF( tmp_dircall_arg2_1 );
        Py_DECREF( tmp_dircall_arg3_1 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "verbose" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 105;
        type_description_1 = "oooooooooo";
        goto frame_exception_exit_1;
    }

    tmp_res = PyDict_SetItem( tmp_dircall_arg3_1, tmp_dict_key_5, tmp_dict_value_5 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_6 = const_str_plain_private_key;
    tmp_dict_value_6 = par_private_key;

    if ( tmp_dict_value_6 == NULL )
    {
        Py_DECREF( tmp_dircall_arg1_1 );
        Py_DECREF( tmp_dircall_arg2_1 );
        Py_DECREF( tmp_dircall_arg3_1 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "private_key" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 106;
        type_description_1 = "oooooooooo";
        goto frame_exception_exit_1;
    }

    tmp_res = PyDict_SetItem( tmp_dircall_arg3_1, tmp_dict_key_6, tmp_dict_value_6 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_7 = const_str_plain_dialect;
    tmp_dict_value_7 = par_dialect;

    if ( tmp_dict_value_7 == NULL )
    {
        Py_DECREF( tmp_dircall_arg1_1 );
        Py_DECREF( tmp_dircall_arg2_1 );
        Py_DECREF( tmp_dircall_arg3_1 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "dialect" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 107;
        type_description_1 = "oooooooooo";
        goto frame_exception_exit_1;
    }

    tmp_res = PyDict_SetItem( tmp_dircall_arg3_1, tmp_dict_key_7, tmp_dict_value_7 );
    assert( !(tmp_res != 0) );
    tmp_dircall_arg4_1 = par_kwargs;

    if ( tmp_dircall_arg4_1 == NULL )
    {
        Py_DECREF( tmp_dircall_arg1_1 );
        Py_DECREF( tmp_dircall_arg2_1 );
        Py_DECREF( tmp_dircall_arg3_1 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "kwargs" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 108;
        type_description_1 = "oooooooooo";
        goto frame_exception_exit_1;
    }

    Py_INCREF( tmp_dircall_arg4_1 );

    {
        PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1, tmp_dircall_arg4_1};
        tmp_return_value = impl___internal__$$$function_6_complex_call_helper_pos_keywords_star_dict( dir_call_args );
    }
    if ( tmp_return_value == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 102;
        type_description_1 = "oooooooooo";
        goto frame_exception_exit_1;
    }
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3b40f534526dd86c1380d98f5d0129f4 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_3b40f534526dd86c1380d98f5d0129f4 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3b40f534526dd86c1380d98f5d0129f4 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_3b40f534526dd86c1380d98f5d0129f4, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_3b40f534526dd86c1380d98f5d0129f4->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_3b40f534526dd86c1380d98f5d0129f4, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_3b40f534526dd86c1380d98f5d0129f4,
        type_description_1,
        par_query,
        par_project_id,
        par_index_col,
        par_col_order,
        par_reauth,
        par_verbose,
        par_private_key,
        par_dialect,
        par_kwargs,
        var_pandas_gbq
    );


    // Release cached frame.
    if ( frame_3b40f534526dd86c1380d98f5d0129f4 == cache_frame_3b40f534526dd86c1380d98f5d0129f4 )
    {
        Py_DECREF( frame_3b40f534526dd86c1380d98f5d0129f4 );
    }
    cache_frame_3b40f534526dd86c1380d98f5d0129f4 = NULL;

    assertFrameObject( frame_3b40f534526dd86c1380d98f5d0129f4 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$io$gbq$$$function_2_read_gbq );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_query );
    par_query = NULL;

    Py_XDECREF( par_project_id );
    par_project_id = NULL;

    Py_XDECREF( par_index_col );
    par_index_col = NULL;

    Py_XDECREF( par_col_order );
    par_col_order = NULL;

    Py_XDECREF( par_reauth );
    par_reauth = NULL;

    Py_XDECREF( par_verbose );
    par_verbose = NULL;

    Py_XDECREF( par_private_key );
    par_private_key = NULL;

    Py_XDECREF( par_dialect );
    par_dialect = NULL;

    Py_XDECREF( par_kwargs );
    par_kwargs = NULL;

    Py_XDECREF( var_pandas_gbq );
    var_pandas_gbq = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_query );
    par_query = NULL;

    Py_XDECREF( par_project_id );
    par_project_id = NULL;

    Py_XDECREF( par_index_col );
    par_index_col = NULL;

    Py_XDECREF( par_col_order );
    par_col_order = NULL;

    Py_XDECREF( par_reauth );
    par_reauth = NULL;

    Py_XDECREF( par_verbose );
    par_verbose = NULL;

    Py_XDECREF( par_private_key );
    par_private_key = NULL;

    Py_XDECREF( par_dialect );
    par_dialect = NULL;

    Py_XDECREF( par_kwargs );
    par_kwargs = NULL;

    Py_XDECREF( var_pandas_gbq );
    var_pandas_gbq = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$io$gbq$$$function_2_read_gbq );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$io$gbq$$$function_3_to_gbq( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_dataframe = python_pars[ 0 ];
    PyObject *par_destination_table = python_pars[ 1 ];
    PyObject *par_project_id = python_pars[ 2 ];
    PyObject *par_chunksize = python_pars[ 3 ];
    PyObject *par_verbose = python_pars[ 4 ];
    PyObject *par_reauth = python_pars[ 5 ];
    PyObject *par_if_exists = python_pars[ 6 ];
    PyObject *par_private_key = python_pars[ 7 ];
    PyObject *par_auth_local_webserver = python_pars[ 8 ];
    PyObject *par_table_schema = python_pars[ 9 ];
    PyObject *var_pandas_gbq = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_args_name_1;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_called_name_1;
    PyObject *tmp_called_name_2;
    PyObject *tmp_dict_key_1;
    PyObject *tmp_dict_key_2;
    PyObject *tmp_dict_key_3;
    PyObject *tmp_dict_key_4;
    PyObject *tmp_dict_key_5;
    PyObject *tmp_dict_key_6;
    PyObject *tmp_dict_key_7;
    PyObject *tmp_dict_value_1;
    PyObject *tmp_dict_value_2;
    PyObject *tmp_dict_value_3;
    PyObject *tmp_dict_value_4;
    PyObject *tmp_dict_value_5;
    PyObject *tmp_dict_value_6;
    PyObject *tmp_dict_value_7;
    PyObject *tmp_kw_name_1;
    int tmp_res;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    PyObject *tmp_tuple_element_1;
    static struct Nuitka_FrameObject *cache_frame_02e253b331f3c1fc8b46df9e0a589873 = NULL;

    struct Nuitka_FrameObject *frame_02e253b331f3c1fc8b46df9e0a589873;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_02e253b331f3c1fc8b46df9e0a589873, codeobj_02e253b331f3c1fc8b46df9e0a589873, module_pandas$io$gbq, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_02e253b331f3c1fc8b46df9e0a589873 = cache_frame_02e253b331f3c1fc8b46df9e0a589873;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_02e253b331f3c1fc8b46df9e0a589873 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_02e253b331f3c1fc8b46df9e0a589873 ) == 2 ); // Frame stack

    // Framed code:
    tmp_called_name_1 = GET_STRING_DICT_VALUE( moduledict_pandas$io$gbq, (Nuitka_StringObject *)const_str_plain__try_import );

    if (unlikely( tmp_called_name_1 == NULL ))
    {
        tmp_called_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__try_import );
    }

    if ( tmp_called_name_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_try_import" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 114;
        type_description_1 = "ooooooooooo";
        goto frame_exception_exit_1;
    }

    frame_02e253b331f3c1fc8b46df9e0a589873->m_frame.f_lineno = 114;
    tmp_assign_source_1 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
    if ( tmp_assign_source_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 114;
        type_description_1 = "ooooooooooo";
        goto frame_exception_exit_1;
    }
    assert( var_pandas_gbq == NULL );
    var_pandas_gbq = tmp_assign_source_1;

    tmp_source_name_1 = var_pandas_gbq;

    CHECK_OBJECT( tmp_source_name_1 );
    tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_to_gbq );
    if ( tmp_called_name_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 115;
        type_description_1 = "ooooooooooo";
        goto frame_exception_exit_1;
    }
    tmp_tuple_element_1 = par_dataframe;

    if ( tmp_tuple_element_1 == NULL )
    {
        Py_DECREF( tmp_called_name_2 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "dataframe" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 116;
        type_description_1 = "ooooooooooo";
        goto frame_exception_exit_1;
    }

    tmp_args_name_1 = PyTuple_New( 3 );
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
    tmp_tuple_element_1 = par_destination_table;

    if ( tmp_tuple_element_1 == NULL )
    {
        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_name_1 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "destination_table" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 116;
        type_description_1 = "ooooooooooo";
        goto frame_exception_exit_1;
    }

    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_1 );
    tmp_tuple_element_1 = par_project_id;

    if ( tmp_tuple_element_1 == NULL )
    {
        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_name_1 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "project_id" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 116;
        type_description_1 = "ooooooooooo";
        goto frame_exception_exit_1;
    }

    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_args_name_1, 2, tmp_tuple_element_1 );
    tmp_dict_key_1 = const_str_plain_chunksize;
    tmp_dict_value_1 = par_chunksize;

    if ( tmp_dict_value_1 == NULL )
    {
        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_name_1 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "chunksize" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 116;
        type_description_1 = "ooooooooooo";
        goto frame_exception_exit_1;
    }

    tmp_kw_name_1 = _PyDict_NewPresized( 7 );
    tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_2 = const_str_plain_verbose;
    tmp_dict_value_2 = par_verbose;

    if ( tmp_dict_value_2 == NULL )
    {
        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "verbose" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 117;
        type_description_1 = "ooooooooooo";
        goto frame_exception_exit_1;
    }

    tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_3 = const_str_plain_reauth;
    tmp_dict_value_3 = par_reauth;

    if ( tmp_dict_value_3 == NULL )
    {
        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "reauth" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 117;
        type_description_1 = "ooooooooooo";
        goto frame_exception_exit_1;
    }

    tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_3, tmp_dict_value_3 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_4 = const_str_plain_if_exists;
    tmp_dict_value_4 = par_if_exists;

    if ( tmp_dict_value_4 == NULL )
    {
        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "if_exists" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 117;
        type_description_1 = "ooooooooooo";
        goto frame_exception_exit_1;
    }

    tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_4, tmp_dict_value_4 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_5 = const_str_plain_private_key;
    tmp_dict_value_5 = par_private_key;

    if ( tmp_dict_value_5 == NULL )
    {
        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "private_key" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 118;
        type_description_1 = "ooooooooooo";
        goto frame_exception_exit_1;
    }

    tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_5, tmp_dict_value_5 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_6 = const_str_plain_auth_local_webserver;
    tmp_dict_value_6 = par_auth_local_webserver;

    if ( tmp_dict_value_6 == NULL )
    {
        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "auth_local_webserver" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 118;
        type_description_1 = "ooooooooooo";
        goto frame_exception_exit_1;
    }

    tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_6, tmp_dict_value_6 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_7 = const_str_plain_table_schema;
    tmp_dict_value_7 = par_table_schema;

    if ( tmp_dict_value_7 == NULL )
    {
        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "table_schema" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 119;
        type_description_1 = "ooooooooooo";
        goto frame_exception_exit_1;
    }

    tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_7, tmp_dict_value_7 );
    assert( !(tmp_res != 0) );
    frame_02e253b331f3c1fc8b46df9e0a589873->m_frame.f_lineno = 115;
    tmp_return_value = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_1, tmp_kw_name_1 );
    Py_DECREF( tmp_called_name_2 );
    Py_DECREF( tmp_args_name_1 );
    Py_DECREF( tmp_kw_name_1 );
    if ( tmp_return_value == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 115;
        type_description_1 = "ooooooooooo";
        goto frame_exception_exit_1;
    }
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_02e253b331f3c1fc8b46df9e0a589873 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_02e253b331f3c1fc8b46df9e0a589873 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_02e253b331f3c1fc8b46df9e0a589873 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_02e253b331f3c1fc8b46df9e0a589873, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_02e253b331f3c1fc8b46df9e0a589873->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_02e253b331f3c1fc8b46df9e0a589873, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_02e253b331f3c1fc8b46df9e0a589873,
        type_description_1,
        par_dataframe,
        par_destination_table,
        par_project_id,
        par_chunksize,
        par_verbose,
        par_reauth,
        par_if_exists,
        par_private_key,
        par_auth_local_webserver,
        par_table_schema,
        var_pandas_gbq
    );


    // Release cached frame.
    if ( frame_02e253b331f3c1fc8b46df9e0a589873 == cache_frame_02e253b331f3c1fc8b46df9e0a589873 )
    {
        Py_DECREF( frame_02e253b331f3c1fc8b46df9e0a589873 );
    }
    cache_frame_02e253b331f3c1fc8b46df9e0a589873 = NULL;

    assertFrameObject( frame_02e253b331f3c1fc8b46df9e0a589873 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$io$gbq$$$function_3_to_gbq );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_dataframe );
    par_dataframe = NULL;

    Py_XDECREF( par_destination_table );
    par_destination_table = NULL;

    Py_XDECREF( par_project_id );
    par_project_id = NULL;

    Py_XDECREF( par_chunksize );
    par_chunksize = NULL;

    Py_XDECREF( par_verbose );
    par_verbose = NULL;

    Py_XDECREF( par_reauth );
    par_reauth = NULL;

    Py_XDECREF( par_if_exists );
    par_if_exists = NULL;

    Py_XDECREF( par_private_key );
    par_private_key = NULL;

    Py_XDECREF( par_auth_local_webserver );
    par_auth_local_webserver = NULL;

    Py_XDECREF( par_table_schema );
    par_table_schema = NULL;

    Py_XDECREF( var_pandas_gbq );
    var_pandas_gbq = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_dataframe );
    par_dataframe = NULL;

    Py_XDECREF( par_destination_table );
    par_destination_table = NULL;

    Py_XDECREF( par_project_id );
    par_project_id = NULL;

    Py_XDECREF( par_chunksize );
    par_chunksize = NULL;

    Py_XDECREF( par_verbose );
    par_verbose = NULL;

    Py_XDECREF( par_reauth );
    par_reauth = NULL;

    Py_XDECREF( par_if_exists );
    par_if_exists = NULL;

    Py_XDECREF( par_private_key );
    par_private_key = NULL;

    Py_XDECREF( par_auth_local_webserver );
    par_auth_local_webserver = NULL;

    Py_XDECREF( par_table_schema );
    par_table_schema = NULL;

    Py_XDECREF( var_pandas_gbq );
    var_pandas_gbq = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$io$gbq$$$function_3_to_gbq );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}



static PyObject *MAKE_FUNCTION_pandas$io$gbq$$$function_1__try_import(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$io$gbq$$$function_1__try_import,
        const_str_plain__try_import,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_0855ce15ff9fa15357602d7ec5a88f11,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$io$gbq,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$io$gbq$$$function_2_read_gbq( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$io$gbq$$$function_2_read_gbq,
        const_str_plain_read_gbq,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_3b40f534526dd86c1380d98f5d0129f4,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$io$gbq,
        const_str_digest_a072f02d14e0765940241fb1cb4ab6cc,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$io$gbq$$$function_3_to_gbq( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$io$gbq$$$function_3_to_gbq,
        const_str_plain_to_gbq,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_02e253b331f3c1fc8b46df9e0a589873,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$io$gbq,
        Py_None,
        0
    );


    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_pandas$io$gbq =
{
    PyModuleDef_HEAD_INIT,
    "pandas.io.gbq",   /* m_name */
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;

extern PyObject *const_str_plain___loader__;
extern PyObject *metapath_based_loader;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL.

MOD_INIT_DECL( pandas$io$gbq )
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_pandas$io$gbq );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("pandas.io.gbq: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("pandas.io.gbq: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("pandas.io.gbq: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initpandas$io$gbq" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_pandas$io$gbq = Py_InitModule4(
        "pandas.io.gbq",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_pandas$io$gbq = PyModule_Create( &mdef_pandas$io$gbq );
#endif

    moduledict_pandas$io$gbq = MODULE_DICT( module_pandas$io$gbq );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_pandas$io$gbq, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_pandas$io$gbq,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );

#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_pandas$io$gbq, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_pandas$io$gbq,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_pandas$io$gbq, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_pandas$io$gbq,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_pandas$io$gbq );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PySys_GetObject( (char *)"modules" ), const_str_digest_75090e6ed0be6fa03614e573a9d579f0, module_pandas$io$gbq );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_pandas$io$gbq, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_pandas$io$gbq, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_pandas$io$gbq, (Nuitka_StringObject *)const_str_plain___loader__, metapath_based_loader );
#endif

#if PYTHON_VERSION >= 340
#if 0
    UPDATE_STRING_DICT0( moduledict_pandas$io$gbq, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_pandas$io$gbq, (Nuitka_StringObject *)const_str_plain___name__ ),
            metapath_based_loader
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );

        UPDATE_STRING_DICT1( moduledict_pandas$io$gbq, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );

        Py_DECREF( module_spec_class );
    }
#endif
#endif


    // Temp variables if any
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_assign_source_5;
    PyObject *tmp_assign_source_6;
    PyObject *tmp_defaults_1;
    PyObject *tmp_defaults_2;

    // Module code.
    tmp_assign_source_1 = const_str_digest_31c75016cd4dee587fe0cb5bcc2904a0;
    UPDATE_STRING_DICT0( moduledict_pandas$io$gbq, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    tmp_assign_source_2 = const_str_digest_cc40cb205ef9f0fc5d230af97ce9468f;
    UPDATE_STRING_DICT0( moduledict_pandas$io$gbq, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    tmp_assign_source_3 = Py_None;
    UPDATE_STRING_DICT0( moduledict_pandas$io$gbq, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    tmp_assign_source_4 = MAKE_FUNCTION_pandas$io$gbq$$$function_1__try_import(  );
    UPDATE_STRING_DICT1( moduledict_pandas$io$gbq, (Nuitka_StringObject *)const_str_plain__try_import, tmp_assign_source_4 );
    tmp_defaults_1 = const_tuple_none_none_none_false_none_none_str_plain_legacy_tuple;
    Py_INCREF( tmp_defaults_1 );
    tmp_assign_source_5 = MAKE_FUNCTION_pandas$io$gbq$$$function_2_read_gbq( tmp_defaults_1 );
    UPDATE_STRING_DICT1( moduledict_pandas$io$gbq, (Nuitka_StringObject *)const_str_plain_read_gbq, tmp_assign_source_5 );
    tmp_defaults_2 = const_tuple_none_none_false_str_plain_fail_none_false_none_tuple;
    Py_INCREF( tmp_defaults_2 );
    tmp_assign_source_6 = MAKE_FUNCTION_pandas$io$gbq$$$function_3_to_gbq( tmp_defaults_2 );
    UPDATE_STRING_DICT1( moduledict_pandas$io$gbq, (Nuitka_StringObject *)const_str_plain_to_gbq, tmp_assign_source_6 );

    return MOD_RETURN_VALUE( module_pandas$io$gbq );
}
