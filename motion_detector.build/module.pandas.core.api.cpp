/* Generated code for Python source for module 'pandas.core.api'
 * created by Nuitka version 0.5.32.7
 *
 * This code is in part copyright 2018 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The _module_pandas$core$api is a Python object pointer of module type. */

/* Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_pandas$core$api;
PyDictObject *moduledict_pandas$core$api;

/* The module constants used, if any. */
static PyObject *const_str_plain_yearBegin;
extern PyObject *const_str_plain_warn;
extern PyObject *const_str_plain_CategoricalIndex;
static PyObject *const_str_plain_monthEnd;
extern PyObject *const_str_plain_metaclass;
extern PyObject *const_str_plain_UInt64Index;
extern PyObject *const_tuple_str_plain_DateOffset_tuple;
static PyObject *const_tuple_str_plain_TimeGrouper_tuple_type_object_tuple_tuple;
extern PyObject *const_str_plain_get_option;
extern PyObject *const_str_plain_groupby;
extern PyObject *const_tuple_str_plain___class___tuple;
extern PyObject *const_slice_int_pos_1_none_none;
extern PyObject *const_str_plain_object;
extern PyObject *const_str_plain___file__;
extern PyObject *const_str_plain_to_numeric;
static PyObject *const_tuple_33e68e36422623b0156f4753b1734aea_tuple;
static PyObject *const_str_plain_bday;
extern PyObject *const_tuple_str_plain_Categorical_tuple;
extern PyObject *const_str_digest_76c94ce9f5962dd5d22d93bb9850c757;
extern PyObject *const_str_digest_d5ccb2cadb8e132648b5cca1015acbda;
extern PyObject *const_str_plain_describe_option;
extern PyObject *const_str_plain_args;
extern PyObject *const_str_plain_isna;
extern PyObject *const_dict_f154c9a58c9419d7e391901d7b7fe49e;
extern PyObject *const_str_plain_None;
static PyObject *const_str_plain_cbmonthEnd;
extern PyObject *const_str_plain_pnow;
extern PyObject *const_tuple_str_plain_to_numeric_tuple;
extern PyObject *const_str_digest_f0f277988b36a31f11f2111700ac3a0c;
extern PyObject *const_str_plain_notnull;
extern PyObject *const_str_plain_bdate_range;
extern PyObject *const_str_digest_09671eb6f6f5841ab7ba447ea0609426;
extern PyObject *const_str_plain_cls;
static PyObject *const_str_digest_ddddf82e58942977054c1231ea9920d7;
static PyObject *const_tuple_cac1c35c87f936f575860bc63a9e6b24_tuple;
extern PyObject *const_str_plain_Panel;
extern PyObject *const_str_plain_FutureWarning;
extern PyObject *const_str_plain_value_counts;
static PyObject *const_str_plain_customBusinessDay;
static PyObject *const_str_plain_customBusinessMonthBegin;
extern PyObject *const_str_plain_Categorical;
extern PyObject *const_str_plain_Float64Index;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_plain___debug__;
extern PyObject *const_str_digest_ea133e7cd13bdb7d21480793efba08d3;
extern PyObject *const_str_plain_reset_option;
extern PyObject *const_str_plain_option_context;
extern PyObject *const_str_digest_169802d28602a65b8c5223cb1d6d6476;
extern PyObject *const_str_plain_DatetimeIndex;
extern PyObject *const_str_digest_5414863e6321797cbb507ce7fdd47628;
extern PyObject *const_str_plain_options;
extern PyObject *const_str_plain_DataFrame;
extern PyObject *const_str_plain_TimeGrouper;
static PyObject *const_str_plain_businessDay;
static PyObject *const_list_426f738cd785da6b9cfb0c148a34955c_list;
extern PyObject *const_str_digest_53eb620cf819e7f2e2a90c9ff72c4e4b;
extern PyObject *const_str_plain_warnings;
static PyObject *const_tuple_str_plain_IndexSlice_tuple;
extern PyObject *const_str_plain_numpy;
extern PyObject *const_str_plain_PeriodIndex;
extern PyObject *const_str_plain_date_range;
extern PyObject *const_str_plain___qualname__;
extern PyObject *const_str_plain_removals;
static PyObject *const_str_plain_bquarterEnd;
static PyObject *const_tuple_str_plain_factorize_str_plain_unique_str_plain_value_counts_tuple;
extern PyObject *const_str_plain_set_eng_float_format;
static PyObject *const_str_plain_cbmonthBegin;
extern PyObject *const_tuple_str_plain_to_timedelta_tuple;
extern PyObject *const_str_digest_e2891cfc97ee3db7bef15d3875bcf8e9;
static PyObject *const_str_plain_quarterEnd;
extern PyObject *const_str_digest_0777709bd55eb1f430096c3d46daa3a9;
extern PyObject *const_str_plain_MultiIndex;
extern PyObject *const_tuple_empty;
static PyObject *const_str_plain_bmonthBegin;
extern PyObject *const_tuple_str_plain__DeprecatedModule_tuple;
extern PyObject *const_str_plain_unique;
extern PyObject *const_str_plain_to_datetime;
extern PyObject *const_tuple_str_plain_TimeGrouper_tuple;
extern PyObject *const_str_digest_495447daacfda0078e7b8b6f413ed7fe;
extern PyObject *const_str_plain_TimedeltaIndex;
extern PyObject *const_str_digest_3c57dc0e67e1943d78d8cf3f2010b4ac;
extern PyObject *const_str_plain_stacklevel;
extern PyObject *const_str_plain_match;
extern PyObject *const_str_plain_Timestamp;
extern PyObject *const_str_plain_NaT;
extern PyObject *const_str_plain_isnull;
extern PyObject *const_str_plain_WidePanel;
extern PyObject *const_str_plain_RangeIndex;
extern PyObject *const_str_plain___new__;
static PyObject *const_str_digest_5a8b9e556ee92431fc55cd804dc7bc4c;
static PyObject *const_tuple_str_plain_args_str_plain_kwargs_str_plain_warnings_tuple;
extern PyObject *const_str_digest_10e8a3a733a03b1cfd04180fe1bdad2c;
extern PyObject *const_int_0;
static PyObject *const_tuple_e5a08678b3493f1efaffaa88d05034c3_tuple;
extern PyObject *const_str_plain_set_option;
static PyObject *const_tuple_01611c0b8bbdf213baf08d7353df37e7_tuple;
static PyObject *const_str_digest_f644ecbf893fd094b5aeb9472d6ea7d4;
static PyObject *const_tuple_str_plain_get_dummies_tuple;
extern PyObject *const_str_plain_Timedelta;
extern PyObject *const_str_plain_get_dummies;
extern PyObject *const_str_digest_7a907ce278e4875739e67044c12dd234;
extern PyObject *const_str_plain_timedelta_range;
static PyObject *const_tuple_str_plain_set_eng_float_format_tuple;
static PyObject *const_str_digest_633df9c5f680f8c7ac120b8872359722;
extern PyObject *const_str_plain_type;
static PyObject *const_tuple_str_plain_Interval_str_plain_interval_range_tuple;
static PyObject *const_tuple_str_plain_Panel_str_plain_WidePanel_tuple;
extern PyObject *const_str_digest_fff453aacf5b49a73746709e4ca465bf;
static PyObject *const_tuple_str_plain_Period_str_plain_period_range_str_plain_pnow_tuple;
extern PyObject *const_str_digest_3a0b927a1532ded681dd49b7ca17edfe;
extern PyObject *const_tuple_str_plain_to_datetime_tuple;
extern PyObject *const_str_plain___cached__;
static PyObject *const_tuple_str_plain_match_tuple;
extern PyObject *const_str_plain_deprmod;
extern PyObject *const_str_plain_IndexSlice;
extern PyObject *const_str_plain_factorize;
extern PyObject *const_str_plain___class__;
static PyObject *const_str_digest_fee803459653fb512cf06af3c49f9f6c;
extern PyObject *const_str_plain_period_range;
static PyObject *const_str_digest_4bf07230a48aaa22ed018a2fb5232560;
extern PyObject *const_str_plain_notna;
static PyObject *const_tuple_b5c390b286ed7443ae63508c47ecd31b_tuple;
static PyObject *const_str_plain_yearEnd;
extern PyObject *const_str_digest_d003f58557ff8e36712dfed05afa2e71;
extern PyObject *const_tuple_type_object_tuple;
extern PyObject *const_str_plain___module__;
extern PyObject *const_str_plain_cday;
extern PyObject *const_str_plain_DateOffset;
extern PyObject *const_str_plain_Period;
static PyObject *const_str_plain_bmonthEnd;
extern PyObject *const_str_plain_Series;
extern PyObject *const_int_pos_1;
extern PyObject *const_tuple_str_plain_Grouper_tuple;
extern PyObject *const_str_plain__DeprecatedModule;
extern PyObject *const_str_plain_interval_range;
static PyObject *const_str_plain_customBusinessMonthEnd;
extern PyObject *const_str_plain_IntervalIndex;
extern PyObject *const_str_plain_day;
extern PyObject *const_str_digest_5a4d6e54f7074a6626248cab2eeb4589;
static PyObject *const_str_plain_datetools;
extern PyObject *const_str_plain_Interval;
extern PyObject *const_str_plain___prepare__;
extern PyObject *const_tuple_str_plain_DataFrame_tuple;
static PyObject *const_str_plain_byearEnd;
extern PyObject *const_str_plain_to_timedelta;
extern PyObject *const_str_plain_Int64Index;
extern PyObject *const_str_digest_ac61716297b632aadf9bf726edeabdee;
static PyObject *const_tuple_str_plain_Timedelta_str_plain_timedelta_range_tuple;
static PyObject *const_tuple_5ee883fcebb5e3009913e0dee164fc9a_tuple;
extern PyObject *const_str_plain_week;
extern PyObject *const_str_digest_9d7f7de63f0d063a345683906ec87dfc;
extern PyObject *const_str_plain_Index;
extern PyObject *const_str_digest_3fa11e71aa92f39879464952ab264ea1;
extern PyObject *const_str_digest_10dcddc71871fe95ec2eb7687818be70;
static PyObject *const_str_digest_9648b49e8afa15ee86e8824cf74a98f2;
static PyObject *const_str_plain__removals;
extern PyObject *const_str_plain_kwargs;
extern PyObject *const_str_plain_np;
extern PyObject *const_int_pos_2;
extern PyObject *const_str_plain_Grouper;
extern PyObject *const_tuple_str_plain_Series_tuple;
extern PyObject *const_str_digest_0effe74afc9238a895565813abbb25ef;
static PyObject *module_filename_obj;

static bool constants_created = false;

static void createModuleConstants( void )
{
    const_str_plain_yearBegin = UNSTREAM_STRING( &constant_bin[ 1735431 ], 9, 1 );
    const_str_plain_monthEnd = UNSTREAM_STRING( &constant_bin[ 1735440 ], 8, 1 );
    const_tuple_str_plain_TimeGrouper_tuple_type_object_tuple_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_TimeGrouper_tuple_type_object_tuple_tuple, 0, const_str_plain_TimeGrouper ); Py_INCREF( const_str_plain_TimeGrouper );
    PyTuple_SET_ITEM( const_tuple_str_plain_TimeGrouper_tuple_type_object_tuple_tuple, 1, const_tuple_type_object_tuple ); Py_INCREF( const_tuple_type_object_tuple );
    const_tuple_33e68e36422623b0156f4753b1734aea_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_33e68e36422623b0156f4753b1734aea_tuple, 0, const_str_plain_cls ); Py_INCREF( const_str_plain_cls );
    PyTuple_SET_ITEM( const_tuple_33e68e36422623b0156f4753b1734aea_tuple, 1, const_str_plain_args ); Py_INCREF( const_str_plain_args );
    PyTuple_SET_ITEM( const_tuple_33e68e36422623b0156f4753b1734aea_tuple, 2, const_str_plain_kwargs ); Py_INCREF( const_str_plain_kwargs );
    PyTuple_SET_ITEM( const_tuple_33e68e36422623b0156f4753b1734aea_tuple, 3, const_str_plain_TimeGrouper ); Py_INCREF( const_str_plain_TimeGrouper );
    PyTuple_SET_ITEM( const_tuple_33e68e36422623b0156f4753b1734aea_tuple, 4, const_str_plain_warnings ); Py_INCREF( const_str_plain_warnings );
    const_str_plain_bday = UNSTREAM_STRING( &constant_bin[ 1735448 ], 4, 1 );
    const_str_plain_cbmonthEnd = UNSTREAM_STRING( &constant_bin[ 1735452 ], 10, 1 );
    const_str_digest_ddddf82e58942977054c1231ea9920d7 = UNSTREAM_STRING( &constant_bin[ 1735462 ], 110, 0 );
    const_tuple_cac1c35c87f936f575860bc63a9e6b24_tuple = PyTuple_New( 12 );
    PyTuple_SET_ITEM( const_tuple_cac1c35c87f936f575860bc63a9e6b24_tuple, 0, const_str_plain_Index ); Py_INCREF( const_str_plain_Index );
    PyTuple_SET_ITEM( const_tuple_cac1c35c87f936f575860bc63a9e6b24_tuple, 1, const_str_plain_CategoricalIndex ); Py_INCREF( const_str_plain_CategoricalIndex );
    PyTuple_SET_ITEM( const_tuple_cac1c35c87f936f575860bc63a9e6b24_tuple, 2, const_str_plain_Int64Index ); Py_INCREF( const_str_plain_Int64Index );
    PyTuple_SET_ITEM( const_tuple_cac1c35c87f936f575860bc63a9e6b24_tuple, 3, const_str_plain_UInt64Index ); Py_INCREF( const_str_plain_UInt64Index );
    PyTuple_SET_ITEM( const_tuple_cac1c35c87f936f575860bc63a9e6b24_tuple, 4, const_str_plain_RangeIndex ); Py_INCREF( const_str_plain_RangeIndex );
    PyTuple_SET_ITEM( const_tuple_cac1c35c87f936f575860bc63a9e6b24_tuple, 5, const_str_plain_Float64Index ); Py_INCREF( const_str_plain_Float64Index );
    PyTuple_SET_ITEM( const_tuple_cac1c35c87f936f575860bc63a9e6b24_tuple, 6, const_str_plain_MultiIndex ); Py_INCREF( const_str_plain_MultiIndex );
    PyTuple_SET_ITEM( const_tuple_cac1c35c87f936f575860bc63a9e6b24_tuple, 7, const_str_plain_IntervalIndex ); Py_INCREF( const_str_plain_IntervalIndex );
    PyTuple_SET_ITEM( const_tuple_cac1c35c87f936f575860bc63a9e6b24_tuple, 8, const_str_plain_TimedeltaIndex ); Py_INCREF( const_str_plain_TimedeltaIndex );
    PyTuple_SET_ITEM( const_tuple_cac1c35c87f936f575860bc63a9e6b24_tuple, 9, const_str_plain_DatetimeIndex ); Py_INCREF( const_str_plain_DatetimeIndex );
    PyTuple_SET_ITEM( const_tuple_cac1c35c87f936f575860bc63a9e6b24_tuple, 10, const_str_plain_PeriodIndex ); Py_INCREF( const_str_plain_PeriodIndex );
    PyTuple_SET_ITEM( const_tuple_cac1c35c87f936f575860bc63a9e6b24_tuple, 11, const_str_plain_NaT ); Py_INCREF( const_str_plain_NaT );
    const_str_plain_customBusinessDay = UNSTREAM_STRING( &constant_bin[ 1735572 ], 17, 1 );
    const_str_plain_customBusinessMonthBegin = UNSTREAM_STRING( &constant_bin[ 1735589 ], 24, 1 );
    const_str_plain_businessDay = UNSTREAM_STRING( &constant_bin[ 1735613 ], 11, 1 );
    const_list_426f738cd785da6b9cfb0c148a34955c_list = PyList_New( 18 );
    PyList_SET_ITEM( const_list_426f738cd785da6b9cfb0c148a34955c_list, 0, const_str_plain_day ); Py_INCREF( const_str_plain_day );
    PyList_SET_ITEM( const_list_426f738cd785da6b9cfb0c148a34955c_list, 1, const_str_plain_bday ); Py_INCREF( const_str_plain_bday );
    PyList_SET_ITEM( const_list_426f738cd785da6b9cfb0c148a34955c_list, 2, const_str_plain_businessDay ); Py_INCREF( const_str_plain_businessDay );
    PyList_SET_ITEM( const_list_426f738cd785da6b9cfb0c148a34955c_list, 3, const_str_plain_cday ); Py_INCREF( const_str_plain_cday );
    PyList_SET_ITEM( const_list_426f738cd785da6b9cfb0c148a34955c_list, 4, const_str_plain_customBusinessDay ); Py_INCREF( const_str_plain_customBusinessDay );
    const_str_plain_customBusinessMonthEnd = UNSTREAM_STRING( &constant_bin[ 1735624 ], 22, 1 );
    PyList_SET_ITEM( const_list_426f738cd785da6b9cfb0c148a34955c_list, 5, const_str_plain_customBusinessMonthEnd ); Py_INCREF( const_str_plain_customBusinessMonthEnd );
    PyList_SET_ITEM( const_list_426f738cd785da6b9cfb0c148a34955c_list, 6, const_str_plain_customBusinessMonthBegin ); Py_INCREF( const_str_plain_customBusinessMonthBegin );
    PyList_SET_ITEM( const_list_426f738cd785da6b9cfb0c148a34955c_list, 7, const_str_plain_monthEnd ); Py_INCREF( const_str_plain_monthEnd );
    const_str_plain_yearEnd = UNSTREAM_STRING( &constant_bin[ 1735646 ], 7, 1 );
    PyList_SET_ITEM( const_list_426f738cd785da6b9cfb0c148a34955c_list, 8, const_str_plain_yearEnd ); Py_INCREF( const_str_plain_yearEnd );
    PyList_SET_ITEM( const_list_426f738cd785da6b9cfb0c148a34955c_list, 9, const_str_plain_yearBegin ); Py_INCREF( const_str_plain_yearBegin );
    const_str_plain_bmonthEnd = UNSTREAM_STRING( &constant_bin[ 1735453 ], 9, 1 );
    PyList_SET_ITEM( const_list_426f738cd785da6b9cfb0c148a34955c_list, 10, const_str_plain_bmonthEnd ); Py_INCREF( const_str_plain_bmonthEnd );
    const_str_plain_bmonthBegin = UNSTREAM_STRING( &constant_bin[ 1735653 ], 11, 1 );
    PyList_SET_ITEM( const_list_426f738cd785da6b9cfb0c148a34955c_list, 11, const_str_plain_bmonthBegin ); Py_INCREF( const_str_plain_bmonthBegin );
    PyList_SET_ITEM( const_list_426f738cd785da6b9cfb0c148a34955c_list, 12, const_str_plain_cbmonthEnd ); Py_INCREF( const_str_plain_cbmonthEnd );
    const_str_plain_cbmonthBegin = UNSTREAM_STRING( &constant_bin[ 1735664 ], 12, 1 );
    PyList_SET_ITEM( const_list_426f738cd785da6b9cfb0c148a34955c_list, 13, const_str_plain_cbmonthBegin ); Py_INCREF( const_str_plain_cbmonthBegin );
    const_str_plain_bquarterEnd = UNSTREAM_STRING( &constant_bin[ 1735676 ], 11, 1 );
    PyList_SET_ITEM( const_list_426f738cd785da6b9cfb0c148a34955c_list, 14, const_str_plain_bquarterEnd ); Py_INCREF( const_str_plain_bquarterEnd );
    const_str_plain_quarterEnd = UNSTREAM_STRING( &constant_bin[ 1735677 ], 10, 1 );
    PyList_SET_ITEM( const_list_426f738cd785da6b9cfb0c148a34955c_list, 15, const_str_plain_quarterEnd ); Py_INCREF( const_str_plain_quarterEnd );
    const_str_plain_byearEnd = UNSTREAM_STRING( &constant_bin[ 1735687 ], 8, 1 );
    PyList_SET_ITEM( const_list_426f738cd785da6b9cfb0c148a34955c_list, 16, const_str_plain_byearEnd ); Py_INCREF( const_str_plain_byearEnd );
    PyList_SET_ITEM( const_list_426f738cd785da6b9cfb0c148a34955c_list, 17, const_str_plain_week ); Py_INCREF( const_str_plain_week );
    const_tuple_str_plain_IndexSlice_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_IndexSlice_tuple, 0, const_str_plain_IndexSlice ); Py_INCREF( const_str_plain_IndexSlice );
    const_tuple_str_plain_factorize_str_plain_unique_str_plain_value_counts_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_factorize_str_plain_unique_str_plain_value_counts_tuple, 0, const_str_plain_factorize ); Py_INCREF( const_str_plain_factorize );
    PyTuple_SET_ITEM( const_tuple_str_plain_factorize_str_plain_unique_str_plain_value_counts_tuple, 1, const_str_plain_unique ); Py_INCREF( const_str_plain_unique );
    PyTuple_SET_ITEM( const_tuple_str_plain_factorize_str_plain_unique_str_plain_value_counts_tuple, 2, const_str_plain_value_counts ); Py_INCREF( const_str_plain_value_counts );
    const_str_digest_5a8b9e556ee92431fc55cd804dc7bc4c = UNSTREAM_STRING( &constant_bin[ 1735695 ], 24, 0 );
    const_tuple_str_plain_args_str_plain_kwargs_str_plain_warnings_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_args_str_plain_kwargs_str_plain_warnings_tuple, 0, const_str_plain_args ); Py_INCREF( const_str_plain_args );
    PyTuple_SET_ITEM( const_tuple_str_plain_args_str_plain_kwargs_str_plain_warnings_tuple, 1, const_str_plain_kwargs ); Py_INCREF( const_str_plain_kwargs );
    PyTuple_SET_ITEM( const_tuple_str_plain_args_str_plain_kwargs_str_plain_warnings_tuple, 2, const_str_plain_warnings ); Py_INCREF( const_str_plain_warnings );
    const_tuple_e5a08678b3493f1efaffaa88d05034c3_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_e5a08678b3493f1efaffaa88d05034c3_tuple, 0, const_str_plain_Timestamp ); Py_INCREF( const_str_plain_Timestamp );
    PyTuple_SET_ITEM( const_tuple_e5a08678b3493f1efaffaa88d05034c3_tuple, 1, const_str_plain_date_range ); Py_INCREF( const_str_plain_date_range );
    PyTuple_SET_ITEM( const_tuple_e5a08678b3493f1efaffaa88d05034c3_tuple, 2, const_str_plain_bdate_range ); Py_INCREF( const_str_plain_bdate_range );
    const_tuple_01611c0b8bbdf213baf08d7353df37e7_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_01611c0b8bbdf213baf08d7353df37e7_tuple, 0, const_str_plain_isna ); Py_INCREF( const_str_plain_isna );
    PyTuple_SET_ITEM( const_tuple_01611c0b8bbdf213baf08d7353df37e7_tuple, 1, const_str_plain_isnull ); Py_INCREF( const_str_plain_isnull );
    PyTuple_SET_ITEM( const_tuple_01611c0b8bbdf213baf08d7353df37e7_tuple, 2, const_str_plain_notna ); Py_INCREF( const_str_plain_notna );
    PyTuple_SET_ITEM( const_tuple_01611c0b8bbdf213baf08d7353df37e7_tuple, 3, const_str_plain_notnull ); Py_INCREF( const_str_plain_notnull );
    const_str_digest_f644ecbf893fd094b5aeb9472d6ea7d4 = UNSTREAM_STRING( &constant_bin[ 1735719 ], 19, 0 );
    const_tuple_str_plain_get_dummies_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_get_dummies_tuple, 0, const_str_plain_get_dummies ); Py_INCREF( const_str_plain_get_dummies );
    const_tuple_str_plain_set_eng_float_format_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_set_eng_float_format_tuple, 0, const_str_plain_set_eng_float_format ); Py_INCREF( const_str_plain_set_eng_float_format );
    const_str_digest_633df9c5f680f8c7ac120b8872359722 = UNSTREAM_STRING( &constant_bin[ 1735738 ], 21, 0 );
    const_tuple_str_plain_Interval_str_plain_interval_range_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Interval_str_plain_interval_range_tuple, 0, const_str_plain_Interval ); Py_INCREF( const_str_plain_Interval );
    PyTuple_SET_ITEM( const_tuple_str_plain_Interval_str_plain_interval_range_tuple, 1, const_str_plain_interval_range ); Py_INCREF( const_str_plain_interval_range );
    const_tuple_str_plain_Panel_str_plain_WidePanel_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Panel_str_plain_WidePanel_tuple, 0, const_str_plain_Panel ); Py_INCREF( const_str_plain_Panel );
    PyTuple_SET_ITEM( const_tuple_str_plain_Panel_str_plain_WidePanel_tuple, 1, const_str_plain_WidePanel ); Py_INCREF( const_str_plain_WidePanel );
    const_tuple_str_plain_Period_str_plain_period_range_str_plain_pnow_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Period_str_plain_period_range_str_plain_pnow_tuple, 0, const_str_plain_Period ); Py_INCREF( const_str_plain_Period );
    PyTuple_SET_ITEM( const_tuple_str_plain_Period_str_plain_period_range_str_plain_pnow_tuple, 1, const_str_plain_period_range ); Py_INCREF( const_str_plain_period_range );
    PyTuple_SET_ITEM( const_tuple_str_plain_Period_str_plain_period_range_str_plain_pnow_tuple, 2, const_str_plain_pnow ); Py_INCREF( const_str_plain_pnow );
    const_tuple_str_plain_match_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_match_tuple, 0, const_str_plain_match ); Py_INCREF( const_str_plain_match );
    const_str_digest_fee803459653fb512cf06af3c49f9f6c = UNSTREAM_STRING( &constant_bin[ 1735759 ], 81, 0 );
    const_str_digest_4bf07230a48aaa22ed018a2fb5232560 = UNSTREAM_STRING( &constant_bin[ 1735840 ], 81, 0 );
    const_tuple_b5c390b286ed7443ae63508c47ecd31b_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_b5c390b286ed7443ae63508c47ecd31b_tuple, 0, const_str_plain_args ); Py_INCREF( const_str_plain_args );
    PyTuple_SET_ITEM( const_tuple_b5c390b286ed7443ae63508c47ecd31b_tuple, 1, const_str_plain_kwargs ); Py_INCREF( const_str_plain_kwargs );
    PyTuple_SET_ITEM( const_tuple_b5c390b286ed7443ae63508c47ecd31b_tuple, 2, const_str_plain_warnings ); Py_INCREF( const_str_plain_warnings );
    PyTuple_SET_ITEM( const_tuple_b5c390b286ed7443ae63508c47ecd31b_tuple, 3, const_str_plain_match ); Py_INCREF( const_str_plain_match );
    const_str_plain_datetools = UNSTREAM_STRING( &constant_bin[ 1735750 ], 9, 1 );
    const_tuple_str_plain_Timedelta_str_plain_timedelta_range_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Timedelta_str_plain_timedelta_range_tuple, 0, const_str_plain_Timedelta ); Py_INCREF( const_str_plain_Timedelta );
    PyTuple_SET_ITEM( const_tuple_str_plain_Timedelta_str_plain_timedelta_range_tuple, 1, const_str_plain_timedelta_range ); Py_INCREF( const_str_plain_timedelta_range );
    const_tuple_5ee883fcebb5e3009913e0dee164fc9a_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_5ee883fcebb5e3009913e0dee164fc9a_tuple, 0, const_str_plain_get_option ); Py_INCREF( const_str_plain_get_option );
    PyTuple_SET_ITEM( const_tuple_5ee883fcebb5e3009913e0dee164fc9a_tuple, 1, const_str_plain_set_option ); Py_INCREF( const_str_plain_set_option );
    PyTuple_SET_ITEM( const_tuple_5ee883fcebb5e3009913e0dee164fc9a_tuple, 2, const_str_plain_reset_option ); Py_INCREF( const_str_plain_reset_option );
    PyTuple_SET_ITEM( const_tuple_5ee883fcebb5e3009913e0dee164fc9a_tuple, 3, const_str_plain_describe_option ); Py_INCREF( const_str_plain_describe_option );
    PyTuple_SET_ITEM( const_tuple_5ee883fcebb5e3009913e0dee164fc9a_tuple, 4, const_str_plain_option_context ); Py_INCREF( const_str_plain_option_context );
    PyTuple_SET_ITEM( const_tuple_5ee883fcebb5e3009913e0dee164fc9a_tuple, 5, const_str_plain_options ); Py_INCREF( const_str_plain_options );
    const_str_digest_9648b49e8afa15ee86e8824cf74a98f2 = UNSTREAM_STRING( &constant_bin[ 1735921 ], 64, 0 );
    const_str_plain__removals = UNSTREAM_STRING( &constant_bin[ 1735985 ], 9, 1 );

    constants_created = true;
}

#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_pandas$core$api( void )
{
    // The module may not have been used at all.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_dbe0f55d8853d10efb95642c43a805eb;
static PyCodeObject *codeobj_3704b79fdd1618b1647ea7104edbb749;
static PyCodeObject *codeobj_d705918d29eb346a46c3d8f0008bce74;
static PyCodeObject *codeobj_d85df049bd5335051717a4f2dac9da96;
static PyCodeObject *codeobj_9fac4c9d501428c88f4807688527c04f;

static void createModuleCodeObjects(void)
{
    module_filename_obj = const_str_digest_4bf07230a48aaa22ed018a2fb5232560;
    codeobj_dbe0f55d8853d10efb95642c43a805eb = MAKE_CODEOBJ( module_filename_obj, const_str_digest_5a8b9e556ee92431fc55cd804dc7bc4c, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_3704b79fdd1618b1647ea7104edbb749 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_TimeGrouper, 72, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_d705918d29eb346a46c3d8f0008bce74 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___new__, 74, const_tuple_33e68e36422623b0156f4753b1734aea_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARARGS | CO_VARKEYWORDS | CO_NOFREE );
    codeobj_d85df049bd5335051717a4f2dac9da96 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_groupby, 61, const_tuple_str_plain_args_str_plain_kwargs_str_plain_warnings_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARARGS | CO_VARKEYWORDS | CO_NOFREE );
    codeobj_9fac4c9d501428c88f4807688527c04f = MAKE_CODEOBJ( module_filename_obj, const_str_plain_match, 51, const_tuple_b5c390b286ed7443ae63508c47ecd31b_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARARGS | CO_VARKEYWORDS | CO_NOFREE );
}

// The module function declarations.
NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_3_complex_call_helper_star_list_star_dict( PyObject **python_pars );


static PyObject *MAKE_FUNCTION_pandas$core$api$$$function_1_match(  );


static PyObject *MAKE_FUNCTION_pandas$core$api$$$function_2_groupby(  );


static PyObject *MAKE_FUNCTION_pandas$core$api$$$function_3___new__(  );


// The module function definitions.
static PyObject *impl_pandas$core$api$$$function_1_match( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_args = python_pars[ 0 ];
    PyObject *par_kwargs = python_pars[ 1 ];
    PyObject *var_warnings = NULL;
    PyObject *var_match = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_args_name_1;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_called_name_1;
    PyObject *tmp_dircall_arg1_1;
    PyObject *tmp_dircall_arg2_1;
    PyObject *tmp_dircall_arg3_1;
    PyObject *tmp_fromlist_name_1;
    PyObject *tmp_fromlist_name_2;
    PyObject *tmp_globals_name_1;
    PyObject *tmp_globals_name_2;
    PyObject *tmp_import_name_from_1;
    PyObject *tmp_kw_name_1;
    PyObject *tmp_level_name_1;
    PyObject *tmp_level_name_2;
    PyObject *tmp_locals_name_1;
    PyObject *tmp_locals_name_2;
    PyObject *tmp_name_name_1;
    PyObject *tmp_name_name_2;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    PyObject *tmp_tuple_element_1;
    NUITKA_MAY_BE_UNUSED PyObject *tmp_unused;
    static struct Nuitka_FrameObject *cache_frame_9fac4c9d501428c88f4807688527c04f = NULL;

    struct Nuitka_FrameObject *frame_9fac4c9d501428c88f4807688527c04f;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_9fac4c9d501428c88f4807688527c04f, codeobj_9fac4c9d501428c88f4807688527c04f, module_pandas$core$api, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_9fac4c9d501428c88f4807688527c04f = cache_frame_9fac4c9d501428c88f4807688527c04f;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_9fac4c9d501428c88f4807688527c04f );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_9fac4c9d501428c88f4807688527c04f ) == 2 ); // Frame stack

    // Framed code:
    tmp_name_name_1 = const_str_plain_warnings;
    tmp_globals_name_1 = (PyObject *)moduledict_pandas$core$api;
    tmp_locals_name_1 = Py_None;
    tmp_fromlist_name_1 = Py_None;
    tmp_level_name_1 = const_int_0;
    frame_9fac4c9d501428c88f4807688527c04f->m_frame.f_lineno = 53;
    tmp_assign_source_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
    if ( tmp_assign_source_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 53;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    assert( var_warnings == NULL );
    var_warnings = tmp_assign_source_1;

    tmp_source_name_1 = var_warnings;

    CHECK_OBJECT( tmp_source_name_1 );
    tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_warn );
    if ( tmp_called_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 54;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    tmp_tuple_element_1 = const_str_digest_9648b49e8afa15ee86e8824cf74a98f2;
    tmp_args_name_1 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
    tmp_tuple_element_1 = GET_STRING_DICT_VALUE( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_FutureWarning );

    if (unlikely( tmp_tuple_element_1 == NULL ))
    {
        tmp_tuple_element_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_FutureWarning );
    }

    if ( tmp_tuple_element_1 == NULL )
    {
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_name_1 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "FutureWarning" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 56;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }

    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_1 );
    tmp_kw_name_1 = PyDict_Copy( const_dict_f154c9a58c9419d7e391901d7b7fe49e );
    frame_9fac4c9d501428c88f4807688527c04f->m_frame.f_lineno = 54;
    tmp_unused = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
    Py_DECREF( tmp_called_name_1 );
    Py_DECREF( tmp_args_name_1 );
    Py_DECREF( tmp_kw_name_1 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 54;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_unused );
    tmp_name_name_2 = const_str_digest_0effe74afc9238a895565813abbb25ef;
    tmp_globals_name_2 = (PyObject *)moduledict_pandas$core$api;
    tmp_locals_name_2 = Py_None;
    tmp_fromlist_name_2 = const_tuple_str_plain_match_tuple;
    tmp_level_name_2 = const_int_0;
    frame_9fac4c9d501428c88f4807688527c04f->m_frame.f_lineno = 57;
    tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
    if ( tmp_import_name_from_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 57;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    tmp_assign_source_2 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_match );
    Py_DECREF( tmp_import_name_from_1 );
    if ( tmp_assign_source_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 57;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    assert( var_match == NULL );
    var_match = tmp_assign_source_2;

    tmp_dircall_arg1_1 = var_match;

    CHECK_OBJECT( tmp_dircall_arg1_1 );
    tmp_dircall_arg2_1 = par_args;

    if ( tmp_dircall_arg2_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "args" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 58;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }

    tmp_dircall_arg3_1 = par_kwargs;

    if ( tmp_dircall_arg3_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "kwargs" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 58;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }

    Py_INCREF( tmp_dircall_arg1_1 );
    Py_INCREF( tmp_dircall_arg2_1 );
    Py_INCREF( tmp_dircall_arg3_1 );

    {
        PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1};
        tmp_return_value = impl___internal__$$$function_3_complex_call_helper_star_list_star_dict( dir_call_args );
    }
    if ( tmp_return_value == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 58;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9fac4c9d501428c88f4807688527c04f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_9fac4c9d501428c88f4807688527c04f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9fac4c9d501428c88f4807688527c04f );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_9fac4c9d501428c88f4807688527c04f, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_9fac4c9d501428c88f4807688527c04f->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_9fac4c9d501428c88f4807688527c04f, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_9fac4c9d501428c88f4807688527c04f,
        type_description_1,
        par_args,
        par_kwargs,
        var_warnings,
        var_match
    );


    // Release cached frame.
    if ( frame_9fac4c9d501428c88f4807688527c04f == cache_frame_9fac4c9d501428c88f4807688527c04f )
    {
        Py_DECREF( frame_9fac4c9d501428c88f4807688527c04f );
    }
    cache_frame_9fac4c9d501428c88f4807688527c04f = NULL;

    assertFrameObject( frame_9fac4c9d501428c88f4807688527c04f );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$core$api$$$function_1_match );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_args );
    par_args = NULL;

    Py_XDECREF( par_kwargs );
    par_kwargs = NULL;

    Py_XDECREF( var_warnings );
    var_warnings = NULL;

    Py_XDECREF( var_match );
    var_match = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_args );
    par_args = NULL;

    Py_XDECREF( par_kwargs );
    par_kwargs = NULL;

    Py_XDECREF( var_warnings );
    var_warnings = NULL;

    Py_XDECREF( var_match );
    var_match = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$core$api$$$function_1_match );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$core$api$$$function_2_groupby( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_args = python_pars[ 0 ];
    PyObject *par_kwargs = python_pars[ 1 ];
    PyObject *var_warnings = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_args_name_1;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_called_name_1;
    PyObject *tmp_dircall_arg1_1;
    PyObject *tmp_dircall_arg2_1;
    PyObject *tmp_dircall_arg3_1;
    PyObject *tmp_fromlist_name_1;
    PyObject *tmp_globals_name_1;
    PyObject *tmp_kw_name_1;
    PyObject *tmp_level_name_1;
    PyObject *tmp_locals_name_1;
    PyObject *tmp_name_name_1;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    PyObject *tmp_source_name_2;
    PyObject *tmp_subscribed_name_1;
    PyObject *tmp_subscribed_name_2;
    PyObject *tmp_subscript_name_1;
    PyObject *tmp_subscript_name_2;
    PyObject *tmp_tuple_element_1;
    NUITKA_MAY_BE_UNUSED PyObject *tmp_unused;
    static struct Nuitka_FrameObject *cache_frame_d85df049bd5335051717a4f2dac9da96 = NULL;

    struct Nuitka_FrameObject *frame_d85df049bd5335051717a4f2dac9da96;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_d85df049bd5335051717a4f2dac9da96, codeobj_d85df049bd5335051717a4f2dac9da96, module_pandas$core$api, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_d85df049bd5335051717a4f2dac9da96 = cache_frame_d85df049bd5335051717a4f2dac9da96;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_d85df049bd5335051717a4f2dac9da96 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_d85df049bd5335051717a4f2dac9da96 ) == 2 ); // Frame stack

    // Framed code:
    tmp_name_name_1 = const_str_plain_warnings;
    tmp_globals_name_1 = (PyObject *)moduledict_pandas$core$api;
    tmp_locals_name_1 = Py_None;
    tmp_fromlist_name_1 = Py_None;
    tmp_level_name_1 = const_int_0;
    frame_d85df049bd5335051717a4f2dac9da96->m_frame.f_lineno = 62;
    tmp_assign_source_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
    if ( tmp_assign_source_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 62;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    assert( var_warnings == NULL );
    var_warnings = tmp_assign_source_1;

    tmp_source_name_1 = var_warnings;

    CHECK_OBJECT( tmp_source_name_1 );
    tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_warn );
    if ( tmp_called_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 64;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    tmp_tuple_element_1 = const_str_digest_ddddf82e58942977054c1231ea9920d7;
    tmp_args_name_1 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
    tmp_tuple_element_1 = GET_STRING_DICT_VALUE( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_FutureWarning );

    if (unlikely( tmp_tuple_element_1 == NULL ))
    {
        tmp_tuple_element_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_FutureWarning );
    }

    if ( tmp_tuple_element_1 == NULL )
    {
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_name_1 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "FutureWarning" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 67;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_1 );
    tmp_kw_name_1 = PyDict_Copy( const_dict_f154c9a58c9419d7e391901d7b7fe49e );
    frame_d85df049bd5335051717a4f2dac9da96->m_frame.f_lineno = 64;
    tmp_unused = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
    Py_DECREF( tmp_called_name_1 );
    Py_DECREF( tmp_args_name_1 );
    Py_DECREF( tmp_kw_name_1 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 64;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_unused );
    tmp_subscribed_name_1 = par_args;

    if ( tmp_subscribed_name_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "args" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 68;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    tmp_subscript_name_1 = const_int_0;
    tmp_source_name_2 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
    if ( tmp_source_name_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 68;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    tmp_dircall_arg1_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_groupby );
    Py_DECREF( tmp_source_name_2 );
    if ( tmp_dircall_arg1_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 68;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    tmp_subscribed_name_2 = par_args;

    if ( tmp_subscribed_name_2 == NULL )
    {
        Py_DECREF( tmp_dircall_arg1_1 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "args" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 68;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    tmp_subscript_name_2 = const_slice_int_pos_1_none_none;
    tmp_dircall_arg2_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
    if ( tmp_dircall_arg2_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_dircall_arg1_1 );

        exception_lineno = 68;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    tmp_dircall_arg3_1 = par_kwargs;

    if ( tmp_dircall_arg3_1 == NULL )
    {
        Py_DECREF( tmp_dircall_arg1_1 );
        Py_DECREF( tmp_dircall_arg2_1 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "kwargs" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 68;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    Py_INCREF( tmp_dircall_arg3_1 );

    {
        PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1};
        tmp_return_value = impl___internal__$$$function_3_complex_call_helper_star_list_star_dict( dir_call_args );
    }
    if ( tmp_return_value == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 68;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d85df049bd5335051717a4f2dac9da96 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_d85df049bd5335051717a4f2dac9da96 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d85df049bd5335051717a4f2dac9da96 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_d85df049bd5335051717a4f2dac9da96, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_d85df049bd5335051717a4f2dac9da96->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_d85df049bd5335051717a4f2dac9da96, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_d85df049bd5335051717a4f2dac9da96,
        type_description_1,
        par_args,
        par_kwargs,
        var_warnings
    );


    // Release cached frame.
    if ( frame_d85df049bd5335051717a4f2dac9da96 == cache_frame_d85df049bd5335051717a4f2dac9da96 )
    {
        Py_DECREF( frame_d85df049bd5335051717a4f2dac9da96 );
    }
    cache_frame_d85df049bd5335051717a4f2dac9da96 = NULL;

    assertFrameObject( frame_d85df049bd5335051717a4f2dac9da96 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$core$api$$$function_2_groupby );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_args );
    par_args = NULL;

    Py_XDECREF( par_kwargs );
    par_kwargs = NULL;

    Py_XDECREF( var_warnings );
    var_warnings = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_args );
    par_args = NULL;

    Py_XDECREF( par_kwargs );
    par_kwargs = NULL;

    Py_XDECREF( var_warnings );
    var_warnings = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$core$api$$$function_2_groupby );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$core$api$$$function_3___new__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_cls = python_pars[ 0 ];
    PyObject *par_args = python_pars[ 1 ];
    PyObject *par_kwargs = python_pars[ 2 ];
    PyObject *var_TimeGrouper = NULL;
    PyObject *var_warnings = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_args_name_1;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_called_name_1;
    PyObject *tmp_dircall_arg1_1;
    PyObject *tmp_dircall_arg2_1;
    PyObject *tmp_dircall_arg3_1;
    PyObject *tmp_fromlist_name_1;
    PyObject *tmp_fromlist_name_2;
    PyObject *tmp_globals_name_1;
    PyObject *tmp_globals_name_2;
    PyObject *tmp_import_name_from_1;
    PyObject *tmp_kw_name_1;
    PyObject *tmp_level_name_1;
    PyObject *tmp_level_name_2;
    PyObject *tmp_locals_name_1;
    PyObject *tmp_locals_name_2;
    PyObject *tmp_name_name_1;
    PyObject *tmp_name_name_2;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    PyObject *tmp_tuple_element_1;
    NUITKA_MAY_BE_UNUSED PyObject *tmp_unused;
    static struct Nuitka_FrameObject *cache_frame_d705918d29eb346a46c3d8f0008bce74 = NULL;

    struct Nuitka_FrameObject *frame_d705918d29eb346a46c3d8f0008bce74;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_d705918d29eb346a46c3d8f0008bce74, codeobj_d705918d29eb346a46c3d8f0008bce74, module_pandas$core$api, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_d705918d29eb346a46c3d8f0008bce74 = cache_frame_d705918d29eb346a46c3d8f0008bce74;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_d705918d29eb346a46c3d8f0008bce74 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_d705918d29eb346a46c3d8f0008bce74 ) == 2 ); // Frame stack

    // Framed code:
    tmp_name_name_1 = const_str_digest_ea133e7cd13bdb7d21480793efba08d3;
    tmp_globals_name_1 = (PyObject *)moduledict_pandas$core$api;
    tmp_locals_name_1 = Py_None;
    tmp_fromlist_name_1 = const_tuple_str_plain_TimeGrouper_tuple;
    tmp_level_name_1 = const_int_0;
    frame_d705918d29eb346a46c3d8f0008bce74->m_frame.f_lineno = 75;
    tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
    if ( tmp_import_name_from_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 75;
        type_description_1 = "ooooo";
        goto frame_exception_exit_1;
    }
    tmp_assign_source_1 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_TimeGrouper );
    Py_DECREF( tmp_import_name_from_1 );
    if ( tmp_assign_source_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 75;
        type_description_1 = "ooooo";
        goto frame_exception_exit_1;
    }
    assert( var_TimeGrouper == NULL );
    var_TimeGrouper = tmp_assign_source_1;

    tmp_name_name_2 = const_str_plain_warnings;
    tmp_globals_name_2 = (PyObject *)moduledict_pandas$core$api;
    tmp_locals_name_2 = Py_None;
    tmp_fromlist_name_2 = Py_None;
    tmp_level_name_2 = const_int_0;
    frame_d705918d29eb346a46c3d8f0008bce74->m_frame.f_lineno = 76;
    tmp_assign_source_2 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
    if ( tmp_assign_source_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 76;
        type_description_1 = "ooooo";
        goto frame_exception_exit_1;
    }
    assert( var_warnings == NULL );
    var_warnings = tmp_assign_source_2;

    tmp_source_name_1 = var_warnings;

    CHECK_OBJECT( tmp_source_name_1 );
    tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_warn );
    if ( tmp_called_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 77;
        type_description_1 = "ooooo";
        goto frame_exception_exit_1;
    }
    tmp_tuple_element_1 = const_str_digest_fee803459653fb512cf06af3c49f9f6c;
    tmp_args_name_1 = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
    tmp_tuple_element_1 = GET_STRING_DICT_VALUE( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_FutureWarning );

    if (unlikely( tmp_tuple_element_1 == NULL ))
    {
        tmp_tuple_element_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_FutureWarning );
    }

    if ( tmp_tuple_element_1 == NULL )
    {
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_name_1 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "FutureWarning" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 79;
        type_description_1 = "ooooo";
        goto frame_exception_exit_1;
    }

    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_1 );
    tmp_kw_name_1 = PyDict_Copy( const_dict_f154c9a58c9419d7e391901d7b7fe49e );
    frame_d705918d29eb346a46c3d8f0008bce74->m_frame.f_lineno = 77;
    tmp_unused = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
    Py_DECREF( tmp_called_name_1 );
    Py_DECREF( tmp_args_name_1 );
    Py_DECREF( tmp_kw_name_1 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 77;
        type_description_1 = "ooooo";
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_unused );
    tmp_dircall_arg1_1 = var_TimeGrouper;

    if ( tmp_dircall_arg1_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "TimeGrouper" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 80;
        type_description_1 = "ooooo";
        goto frame_exception_exit_1;
    }

    tmp_dircall_arg2_1 = par_args;

    if ( tmp_dircall_arg2_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "args" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 80;
        type_description_1 = "ooooo";
        goto frame_exception_exit_1;
    }

    tmp_dircall_arg3_1 = par_kwargs;

    if ( tmp_dircall_arg3_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "kwargs" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 80;
        type_description_1 = "ooooo";
        goto frame_exception_exit_1;
    }

    Py_INCREF( tmp_dircall_arg1_1 );
    Py_INCREF( tmp_dircall_arg2_1 );
    Py_INCREF( tmp_dircall_arg3_1 );

    {
        PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1};
        tmp_return_value = impl___internal__$$$function_3_complex_call_helper_star_list_star_dict( dir_call_args );
    }
    if ( tmp_return_value == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 80;
        type_description_1 = "ooooo";
        goto frame_exception_exit_1;
    }
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d705918d29eb346a46c3d8f0008bce74 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_d705918d29eb346a46c3d8f0008bce74 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d705918d29eb346a46c3d8f0008bce74 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_d705918d29eb346a46c3d8f0008bce74, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_d705918d29eb346a46c3d8f0008bce74->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_d705918d29eb346a46c3d8f0008bce74, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_d705918d29eb346a46c3d8f0008bce74,
        type_description_1,
        par_cls,
        par_args,
        par_kwargs,
        var_TimeGrouper,
        var_warnings
    );


    // Release cached frame.
    if ( frame_d705918d29eb346a46c3d8f0008bce74 == cache_frame_d705918d29eb346a46c3d8f0008bce74 )
    {
        Py_DECREF( frame_d705918d29eb346a46c3d8f0008bce74 );
    }
    cache_frame_d705918d29eb346a46c3d8f0008bce74 = NULL;

    assertFrameObject( frame_d705918d29eb346a46c3d8f0008bce74 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$core$api$$$function_3___new__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_cls );
    par_cls = NULL;

    Py_XDECREF( par_args );
    par_args = NULL;

    Py_XDECREF( par_kwargs );
    par_kwargs = NULL;

    Py_XDECREF( var_TimeGrouper );
    var_TimeGrouper = NULL;

    Py_XDECREF( var_warnings );
    var_warnings = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_cls );
    par_cls = NULL;

    Py_XDECREF( par_args );
    par_args = NULL;

    Py_XDECREF( par_kwargs );
    par_kwargs = NULL;

    Py_XDECREF( var_TimeGrouper );
    var_TimeGrouper = NULL;

    Py_XDECREF( var_warnings );
    var_warnings = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$core$api$$$function_3___new__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}



static PyObject *MAKE_FUNCTION_pandas$core$api$$$function_1_match(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$core$api$$$function_1_match,
        const_str_plain_match,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_9fac4c9d501428c88f4807688527c04f,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$core$api,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$core$api$$$function_2_groupby(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$core$api$$$function_2_groupby,
        const_str_plain_groupby,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_d85df049bd5335051717a4f2dac9da96,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$core$api,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$core$api$$$function_3___new__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$core$api$$$function_3___new__,
        const_str_plain___new__,
#if PYTHON_VERSION >= 300
        const_str_digest_f644ecbf893fd094b5aeb9472d6ea7d4,
#endif
        codeobj_d705918d29eb346a46c3d8f0008bce74,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$core$api,
        Py_None,
        0
    );


    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_pandas$core$api =
{
    PyModuleDef_HEAD_INIT,
    "pandas.core.api",   /* m_name */
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;

extern PyObject *const_str_plain___loader__;
extern PyObject *metapath_based_loader;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL.

MOD_INIT_DECL( pandas$core$api )
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_pandas$core$api );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("pandas.core.api: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("pandas.core.api: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("pandas.core.api: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initpandas$core$api" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_pandas$core$api = Py_InitModule4(
        "pandas.core.api",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_pandas$core$api = PyModule_Create( &mdef_pandas$core$api );
#endif

    moduledict_pandas$core$api = MODULE_DICT( module_pandas$core$api );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_pandas$core$api,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );

#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_pandas$core$api,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_pandas$core$api,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_pandas$core$api );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PySys_GetObject( (char *)"modules" ), const_str_digest_3fa11e71aa92f39879464952ab264ea1, module_pandas$core$api );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain___loader__, metapath_based_loader );
#endif

#if PYTHON_VERSION >= 340
#if 0
    UPDATE_STRING_DICT0( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain___name__ ),
            metapath_based_loader
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );

        UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );

        Py_DECREF( module_spec_class );
    }
#endif
#endif


    // Temp variables if any
    PyObject *outline_0_var___class__ = NULL;
    PyObject *tmp_class_creation_1__class_decl_dict = NULL;
    PyObject *tmp_class_creation_1__metaclass = NULL;
    PyObject *tmp_class_creation_1__prepared = NULL;
    PyObject *tmp_import_from_1__module = NULL;
    PyObject *tmp_import_from_2__module = NULL;
    PyObject *tmp_import_from_3__module = NULL;
    PyObject *tmp_import_from_4__module = NULL;
    PyObject *tmp_import_from_5__module = NULL;
    PyObject *tmp_import_from_6__module = NULL;
    PyObject *tmp_import_from_7__module = NULL;
    PyObject *tmp_import_from_8__module = NULL;
    PyObject *tmp_import_from_9__module = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_9;
    PyObject *exception_keeper_type_10;
    PyObject *exception_keeper_value_10;
    PyTracebackObject *exception_keeper_tb_10;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_10;
    PyObject *exception_keeper_type_11;
    PyObject *exception_keeper_value_11;
    PyTracebackObject *exception_keeper_tb_11;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_11;
    PyObject *exception_keeper_type_12;
    PyObject *exception_keeper_value_12;
    PyTracebackObject *exception_keeper_tb_12;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_12;
    PyObject *tmp_args_name_1;
    PyObject *tmp_args_name_2;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_assign_source_5;
    PyObject *tmp_assign_source_6;
    PyObject *tmp_assign_source_7;
    PyObject *tmp_assign_source_8;
    PyObject *tmp_assign_source_9;
    PyObject *tmp_assign_source_10;
    PyObject *tmp_assign_source_11;
    PyObject *tmp_assign_source_12;
    PyObject *tmp_assign_source_13;
    PyObject *tmp_assign_source_14;
    PyObject *tmp_assign_source_15;
    PyObject *tmp_assign_source_16;
    PyObject *tmp_assign_source_17;
    PyObject *tmp_assign_source_18;
    PyObject *tmp_assign_source_19;
    PyObject *tmp_assign_source_20;
    PyObject *tmp_assign_source_21;
    PyObject *tmp_assign_source_22;
    PyObject *tmp_assign_source_23;
    PyObject *tmp_assign_source_24;
    PyObject *tmp_assign_source_25;
    PyObject *tmp_assign_source_26;
    PyObject *tmp_assign_source_27;
    PyObject *tmp_assign_source_28;
    PyObject *tmp_assign_source_29;
    PyObject *tmp_assign_source_30;
    PyObject *tmp_assign_source_31;
    PyObject *tmp_assign_source_32;
    PyObject *tmp_assign_source_33;
    PyObject *tmp_assign_source_34;
    PyObject *tmp_assign_source_35;
    PyObject *tmp_assign_source_36;
    PyObject *tmp_assign_source_37;
    PyObject *tmp_assign_source_38;
    PyObject *tmp_assign_source_39;
    PyObject *tmp_assign_source_40;
    PyObject *tmp_assign_source_41;
    PyObject *tmp_assign_source_42;
    PyObject *tmp_assign_source_43;
    PyObject *tmp_assign_source_44;
    PyObject *tmp_assign_source_45;
    PyObject *tmp_assign_source_46;
    PyObject *tmp_assign_source_47;
    PyObject *tmp_assign_source_48;
    PyObject *tmp_assign_source_49;
    PyObject *tmp_assign_source_50;
    PyObject *tmp_assign_source_51;
    PyObject *tmp_assign_source_52;
    PyObject *tmp_assign_source_53;
    PyObject *tmp_assign_source_54;
    PyObject *tmp_assign_source_55;
    PyObject *tmp_assign_source_56;
    PyObject *tmp_assign_source_57;
    PyObject *tmp_assign_source_58;
    PyObject *tmp_assign_source_59;
    PyObject *tmp_assign_source_60;
    PyObject *tmp_assign_source_61;
    PyObject *tmp_assign_source_62;
    PyObject *tmp_assign_source_63;
    PyObject *tmp_assign_source_64;
    PyObject *tmp_assign_source_65;
    PyObject *tmp_assign_source_66;
    PyObject *tmp_assign_source_67;
    PyObject *tmp_assign_source_68;
    PyObject *tmp_assign_source_69;
    PyObject *tmp_assign_source_70;
    PyObject *tmp_assign_source_71;
    PyObject *tmp_bases_name_1;
    PyObject *tmp_called_name_1;
    PyObject *tmp_called_name_2;
    PyObject *tmp_called_name_3;
    int tmp_cond_truth_1;
    int tmp_cond_truth_2;
    PyObject *tmp_cond_value_1;
    PyObject *tmp_cond_value_2;
    PyObject *tmp_dict_key_1;
    PyObject *tmp_dict_key_2;
    PyObject *tmp_dict_name_1;
    PyObject *tmp_dict_name_2;
    PyObject *tmp_dict_name_3;
    PyObject *tmp_dict_value_1;
    PyObject *tmp_dict_value_2;
    PyObject *tmp_dictdel_dict;
    PyObject *tmp_dictdel_key;
    PyObject *tmp_dictset_value;
    PyObject *tmp_fromlist_name_1;
    PyObject *tmp_fromlist_name_2;
    PyObject *tmp_fromlist_name_3;
    PyObject *tmp_fromlist_name_4;
    PyObject *tmp_fromlist_name_5;
    PyObject *tmp_fromlist_name_6;
    PyObject *tmp_fromlist_name_7;
    PyObject *tmp_fromlist_name_8;
    PyObject *tmp_fromlist_name_9;
    PyObject *tmp_fromlist_name_10;
    PyObject *tmp_fromlist_name_11;
    PyObject *tmp_fromlist_name_12;
    PyObject *tmp_fromlist_name_13;
    PyObject *tmp_fromlist_name_14;
    PyObject *tmp_fromlist_name_15;
    PyObject *tmp_fromlist_name_16;
    PyObject *tmp_fromlist_name_17;
    PyObject *tmp_fromlist_name_18;
    PyObject *tmp_fromlist_name_19;
    PyObject *tmp_fromlist_name_20;
    PyObject *tmp_fromlist_name_21;
    PyObject *tmp_fromlist_name_22;
    PyObject *tmp_globals_name_1;
    PyObject *tmp_globals_name_2;
    PyObject *tmp_globals_name_3;
    PyObject *tmp_globals_name_4;
    PyObject *tmp_globals_name_5;
    PyObject *tmp_globals_name_6;
    PyObject *tmp_globals_name_7;
    PyObject *tmp_globals_name_8;
    PyObject *tmp_globals_name_9;
    PyObject *tmp_globals_name_10;
    PyObject *tmp_globals_name_11;
    PyObject *tmp_globals_name_12;
    PyObject *tmp_globals_name_13;
    PyObject *tmp_globals_name_14;
    PyObject *tmp_globals_name_15;
    PyObject *tmp_globals_name_16;
    PyObject *tmp_globals_name_17;
    PyObject *tmp_globals_name_18;
    PyObject *tmp_globals_name_19;
    PyObject *tmp_globals_name_20;
    PyObject *tmp_globals_name_21;
    PyObject *tmp_globals_name_22;
    PyObject *tmp_hasattr_attr_1;
    PyObject *tmp_hasattr_source_1;
    PyObject *tmp_import_name_from_1;
    PyObject *tmp_import_name_from_2;
    PyObject *tmp_import_name_from_3;
    PyObject *tmp_import_name_from_4;
    PyObject *tmp_import_name_from_5;
    PyObject *tmp_import_name_from_6;
    PyObject *tmp_import_name_from_7;
    PyObject *tmp_import_name_from_8;
    PyObject *tmp_import_name_from_9;
    PyObject *tmp_import_name_from_10;
    PyObject *tmp_import_name_from_11;
    PyObject *tmp_import_name_from_12;
    PyObject *tmp_import_name_from_13;
    PyObject *tmp_import_name_from_14;
    PyObject *tmp_import_name_from_15;
    PyObject *tmp_import_name_from_16;
    PyObject *tmp_import_name_from_17;
    PyObject *tmp_import_name_from_18;
    PyObject *tmp_import_name_from_19;
    PyObject *tmp_import_name_from_20;
    PyObject *tmp_import_name_from_21;
    PyObject *tmp_import_name_from_22;
    PyObject *tmp_import_name_from_23;
    PyObject *tmp_import_name_from_24;
    PyObject *tmp_import_name_from_25;
    PyObject *tmp_import_name_from_26;
    PyObject *tmp_import_name_from_27;
    PyObject *tmp_import_name_from_28;
    PyObject *tmp_import_name_from_29;
    PyObject *tmp_import_name_from_30;
    PyObject *tmp_import_name_from_31;
    PyObject *tmp_import_name_from_32;
    PyObject *tmp_import_name_from_33;
    PyObject *tmp_import_name_from_34;
    PyObject *tmp_import_name_from_35;
    PyObject *tmp_import_name_from_36;
    PyObject *tmp_import_name_from_37;
    PyObject *tmp_import_name_from_38;
    PyObject *tmp_import_name_from_39;
    PyObject *tmp_import_name_from_40;
    PyObject *tmp_import_name_from_41;
    PyObject *tmp_import_name_from_42;
    PyObject *tmp_import_name_from_43;
    PyObject *tmp_import_name_from_44;
    PyObject *tmp_import_name_from_45;
    PyObject *tmp_import_name_from_46;
    PyObject *tmp_import_name_from_47;
    PyObject *tmp_import_name_from_48;
    PyObject *tmp_import_name_from_49;
    PyObject *tmp_key_name_1;
    PyObject *tmp_key_name_2;
    PyObject *tmp_key_name_3;
    PyObject *tmp_kw_name_1;
    PyObject *tmp_kw_name_2;
    PyObject *tmp_kw_name_3;
    PyObject *tmp_level_name_1;
    PyObject *tmp_level_name_2;
    PyObject *tmp_level_name_3;
    PyObject *tmp_level_name_4;
    PyObject *tmp_level_name_5;
    PyObject *tmp_level_name_6;
    PyObject *tmp_level_name_7;
    PyObject *tmp_level_name_8;
    PyObject *tmp_level_name_9;
    PyObject *tmp_level_name_10;
    PyObject *tmp_level_name_11;
    PyObject *tmp_level_name_12;
    PyObject *tmp_level_name_13;
    PyObject *tmp_level_name_14;
    PyObject *tmp_level_name_15;
    PyObject *tmp_level_name_16;
    PyObject *tmp_level_name_17;
    PyObject *tmp_level_name_18;
    PyObject *tmp_level_name_19;
    PyObject *tmp_level_name_20;
    PyObject *tmp_level_name_21;
    PyObject *tmp_level_name_22;
    PyObject *tmp_locals_name_1;
    PyObject *tmp_locals_name_2;
    PyObject *tmp_locals_name_3;
    PyObject *tmp_locals_name_4;
    PyObject *tmp_locals_name_5;
    PyObject *tmp_locals_name_6;
    PyObject *tmp_locals_name_7;
    PyObject *tmp_locals_name_8;
    PyObject *tmp_locals_name_9;
    PyObject *tmp_locals_name_10;
    PyObject *tmp_locals_name_11;
    PyObject *tmp_locals_name_12;
    PyObject *tmp_locals_name_13;
    PyObject *tmp_locals_name_14;
    PyObject *tmp_locals_name_15;
    PyObject *tmp_locals_name_16;
    PyObject *tmp_locals_name_17;
    PyObject *tmp_locals_name_18;
    PyObject *tmp_locals_name_19;
    PyObject *tmp_locals_name_20;
    PyObject *tmp_locals_name_21;
    PyObject *tmp_locals_name_22;
    PyObject *tmp_metaclass_name_1;
    PyObject *tmp_name_name_1;
    PyObject *tmp_name_name_2;
    PyObject *tmp_name_name_3;
    PyObject *tmp_name_name_4;
    PyObject *tmp_name_name_5;
    PyObject *tmp_name_name_6;
    PyObject *tmp_name_name_7;
    PyObject *tmp_name_name_8;
    PyObject *tmp_name_name_9;
    PyObject *tmp_name_name_10;
    PyObject *tmp_name_name_11;
    PyObject *tmp_name_name_12;
    PyObject *tmp_name_name_13;
    PyObject *tmp_name_name_14;
    PyObject *tmp_name_name_15;
    PyObject *tmp_name_name_16;
    PyObject *tmp_name_name_17;
    PyObject *tmp_name_name_18;
    PyObject *tmp_name_name_19;
    PyObject *tmp_name_name_20;
    PyObject *tmp_name_name_21;
    PyObject *tmp_name_name_22;
    PyObject *tmp_outline_return_value_1;
    int tmp_res;
    bool tmp_result;
    PyObject *tmp_set_locals;
    PyObject *tmp_source_name_1;
    PyObject *tmp_tuple_element_1;
    static struct Nuitka_FrameObject *cache_frame_3704b79fdd1618b1647ea7104edbb749_2 = NULL;

    struct Nuitka_FrameObject *frame_3704b79fdd1618b1647ea7104edbb749_2;

    struct Nuitka_FrameObject *frame_dbe0f55d8853d10efb95642c43a805eb;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    tmp_outline_return_value_1 = NULL;
    PyObject *locals_pandas$core$api_72 = NULL;

    // Module code.
    tmp_assign_source_1 = Py_None;
    UPDATE_STRING_DICT0( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    tmp_assign_source_2 = const_str_digest_4bf07230a48aaa22ed018a2fb5232560;
    UPDATE_STRING_DICT0( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    tmp_assign_source_3 = Py_None;
    UPDATE_STRING_DICT0( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    // Frame without reuse.
    frame_dbe0f55d8853d10efb95642c43a805eb = MAKE_MODULE_FRAME( codeobj_dbe0f55d8853d10efb95642c43a805eb, module_pandas$core$api );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_dbe0f55d8853d10efb95642c43a805eb );
    assert( Py_REFCNT( frame_dbe0f55d8853d10efb95642c43a805eb ) == 2 );

    // Framed code:
    tmp_name_name_1 = const_str_plain_numpy;
    tmp_globals_name_1 = (PyObject *)moduledict_pandas$core$api;
    tmp_locals_name_1 = Py_None;
    tmp_fromlist_name_1 = Py_None;
    tmp_level_name_1 = const_int_0;
    frame_dbe0f55d8853d10efb95642c43a805eb->m_frame.f_lineno = 5;
    tmp_assign_source_4 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
    if ( tmp_assign_source_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 5;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_np, tmp_assign_source_4 );
    tmp_name_name_2 = const_str_digest_0effe74afc9238a895565813abbb25ef;
    tmp_globals_name_2 = (PyObject *)moduledict_pandas$core$api;
    tmp_locals_name_2 = Py_None;
    tmp_fromlist_name_2 = const_tuple_str_plain_factorize_str_plain_unique_str_plain_value_counts_tuple;
    tmp_level_name_2 = const_int_0;
    frame_dbe0f55d8853d10efb95642c43a805eb->m_frame.f_lineno = 7;
    tmp_assign_source_5 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
    if ( tmp_assign_source_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 7;

        goto frame_exception_exit_1;
    }
    assert( tmp_import_from_1__module == NULL );
    tmp_import_from_1__module = tmp_assign_source_5;

    // Tried code:
    tmp_import_name_from_1 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_1 );
    tmp_assign_source_6 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_factorize );
    if ( tmp_assign_source_6 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 7;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_factorize, tmp_assign_source_6 );
    tmp_import_name_from_2 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_2 );
    tmp_assign_source_7 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_unique );
    if ( tmp_assign_source_7 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 7;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_unique, tmp_assign_source_7 );
    tmp_import_name_from_3 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_3 );
    tmp_assign_source_8 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_value_counts );
    if ( tmp_assign_source_8 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 7;

        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_value_counts, tmp_assign_source_8 );
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    tmp_name_name_3 = const_str_digest_7a907ce278e4875739e67044c12dd234;
    tmp_globals_name_3 = (PyObject *)moduledict_pandas$core$api;
    tmp_locals_name_3 = Py_None;
    tmp_fromlist_name_3 = const_tuple_01611c0b8bbdf213baf08d7353df37e7_tuple;
    tmp_level_name_3 = const_int_0;
    frame_dbe0f55d8853d10efb95642c43a805eb->m_frame.f_lineno = 8;
    tmp_assign_source_9 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
    if ( tmp_assign_source_9 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 8;

        goto frame_exception_exit_1;
    }
    assert( tmp_import_from_2__module == NULL );
    tmp_import_from_2__module = tmp_assign_source_9;

    // Tried code:
    tmp_import_name_from_4 = tmp_import_from_2__module;

    CHECK_OBJECT( tmp_import_name_from_4 );
    tmp_assign_source_10 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_isna );
    if ( tmp_assign_source_10 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 8;

        goto try_except_handler_2;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_isna, tmp_assign_source_10 );
    tmp_import_name_from_5 = tmp_import_from_2__module;

    CHECK_OBJECT( tmp_import_name_from_5 );
    tmp_assign_source_11 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_isnull );
    if ( tmp_assign_source_11 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 8;

        goto try_except_handler_2;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_isnull, tmp_assign_source_11 );
    tmp_import_name_from_6 = tmp_import_from_2__module;

    CHECK_OBJECT( tmp_import_name_from_6 );
    tmp_assign_source_12 = IMPORT_NAME( tmp_import_name_from_6, const_str_plain_notna );
    if ( tmp_assign_source_12 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 8;

        goto try_except_handler_2;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_notna, tmp_assign_source_12 );
    tmp_import_name_from_7 = tmp_import_from_2__module;

    CHECK_OBJECT( tmp_import_name_from_7 );
    tmp_assign_source_13 = IMPORT_NAME( tmp_import_name_from_7, const_str_plain_notnull );
    if ( tmp_assign_source_13 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 8;

        goto try_except_handler_2;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_notnull, tmp_assign_source_13 );
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    tmp_name_name_4 = const_str_digest_0777709bd55eb1f430096c3d46daa3a9;
    tmp_globals_name_4 = (PyObject *)moduledict_pandas$core$api;
    tmp_locals_name_4 = Py_None;
    tmp_fromlist_name_4 = const_tuple_str_plain_Categorical_tuple;
    tmp_level_name_4 = const_int_0;
    frame_dbe0f55d8853d10efb95642c43a805eb->m_frame.f_lineno = 9;
    tmp_import_name_from_8 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
    if ( tmp_import_name_from_8 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 9;

        goto frame_exception_exit_1;
    }
    tmp_assign_source_14 = IMPORT_NAME( tmp_import_name_from_8, const_str_plain_Categorical );
    Py_DECREF( tmp_import_name_from_8 );
    if ( tmp_assign_source_14 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 9;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_Categorical, tmp_assign_source_14 );
    tmp_name_name_5 = const_str_digest_5a4d6e54f7074a6626248cab2eeb4589;
    tmp_globals_name_5 = (PyObject *)moduledict_pandas$core$api;
    tmp_locals_name_5 = Py_None;
    tmp_fromlist_name_5 = const_tuple_str_plain_Grouper_tuple;
    tmp_level_name_5 = const_int_0;
    frame_dbe0f55d8853d10efb95642c43a805eb->m_frame.f_lineno = 10;
    tmp_import_name_from_9 = IMPORT_MODULE5( tmp_name_name_5, tmp_globals_name_5, tmp_locals_name_5, tmp_fromlist_name_5, tmp_level_name_5 );
    if ( tmp_import_name_from_9 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 10;

        goto frame_exception_exit_1;
    }
    tmp_assign_source_15 = IMPORT_NAME( tmp_import_name_from_9, const_str_plain_Grouper );
    Py_DECREF( tmp_import_name_from_9 );
    if ( tmp_assign_source_15 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 10;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_Grouper, tmp_assign_source_15 );
    tmp_name_name_6 = const_str_digest_76c94ce9f5962dd5d22d93bb9850c757;
    tmp_globals_name_6 = (PyObject *)moduledict_pandas$core$api;
    tmp_locals_name_6 = Py_None;
    tmp_fromlist_name_6 = const_tuple_str_plain_set_eng_float_format_tuple;
    tmp_level_name_6 = const_int_0;
    frame_dbe0f55d8853d10efb95642c43a805eb->m_frame.f_lineno = 11;
    tmp_import_name_from_10 = IMPORT_MODULE5( tmp_name_name_6, tmp_globals_name_6, tmp_locals_name_6, tmp_fromlist_name_6, tmp_level_name_6 );
    if ( tmp_import_name_from_10 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 11;

        goto frame_exception_exit_1;
    }
    tmp_assign_source_16 = IMPORT_NAME( tmp_import_name_from_10, const_str_plain_set_eng_float_format );
    Py_DECREF( tmp_import_name_from_10 );
    if ( tmp_assign_source_16 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 11;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_set_eng_float_format, tmp_assign_source_16 );
    tmp_name_name_7 = const_str_digest_d003f58557ff8e36712dfed05afa2e71;
    tmp_globals_name_7 = (PyObject *)moduledict_pandas$core$api;
    tmp_locals_name_7 = Py_None;
    tmp_fromlist_name_7 = const_tuple_cac1c35c87f936f575860bc63a9e6b24_tuple;
    tmp_level_name_7 = const_int_0;
    frame_dbe0f55d8853d10efb95642c43a805eb->m_frame.f_lineno = 12;
    tmp_assign_source_17 = IMPORT_MODULE5( tmp_name_name_7, tmp_globals_name_7, tmp_locals_name_7, tmp_fromlist_name_7, tmp_level_name_7 );
    if ( tmp_assign_source_17 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 12;

        goto frame_exception_exit_1;
    }
    assert( tmp_import_from_3__module == NULL );
    tmp_import_from_3__module = tmp_assign_source_17;

    // Tried code:
    tmp_import_name_from_11 = tmp_import_from_3__module;

    CHECK_OBJECT( tmp_import_name_from_11 );
    tmp_assign_source_18 = IMPORT_NAME( tmp_import_name_from_11, const_str_plain_Index );
    if ( tmp_assign_source_18 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 12;

        goto try_except_handler_3;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_Index, tmp_assign_source_18 );
    tmp_import_name_from_12 = tmp_import_from_3__module;

    CHECK_OBJECT( tmp_import_name_from_12 );
    tmp_assign_source_19 = IMPORT_NAME( tmp_import_name_from_12, const_str_plain_CategoricalIndex );
    if ( tmp_assign_source_19 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 12;

        goto try_except_handler_3;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_CategoricalIndex, tmp_assign_source_19 );
    tmp_import_name_from_13 = tmp_import_from_3__module;

    CHECK_OBJECT( tmp_import_name_from_13 );
    tmp_assign_source_20 = IMPORT_NAME( tmp_import_name_from_13, const_str_plain_Int64Index );
    if ( tmp_assign_source_20 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 12;

        goto try_except_handler_3;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_Int64Index, tmp_assign_source_20 );
    tmp_import_name_from_14 = tmp_import_from_3__module;

    CHECK_OBJECT( tmp_import_name_from_14 );
    tmp_assign_source_21 = IMPORT_NAME( tmp_import_name_from_14, const_str_plain_UInt64Index );
    if ( tmp_assign_source_21 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 12;

        goto try_except_handler_3;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_UInt64Index, tmp_assign_source_21 );
    tmp_import_name_from_15 = tmp_import_from_3__module;

    CHECK_OBJECT( tmp_import_name_from_15 );
    tmp_assign_source_22 = IMPORT_NAME( tmp_import_name_from_15, const_str_plain_RangeIndex );
    if ( tmp_assign_source_22 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 12;

        goto try_except_handler_3;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_RangeIndex, tmp_assign_source_22 );
    tmp_import_name_from_16 = tmp_import_from_3__module;

    CHECK_OBJECT( tmp_import_name_from_16 );
    tmp_assign_source_23 = IMPORT_NAME( tmp_import_name_from_16, const_str_plain_Float64Index );
    if ( tmp_assign_source_23 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 12;

        goto try_except_handler_3;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_Float64Index, tmp_assign_source_23 );
    tmp_import_name_from_17 = tmp_import_from_3__module;

    CHECK_OBJECT( tmp_import_name_from_17 );
    tmp_assign_source_24 = IMPORT_NAME( tmp_import_name_from_17, const_str_plain_MultiIndex );
    if ( tmp_assign_source_24 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 12;

        goto try_except_handler_3;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_MultiIndex, tmp_assign_source_24 );
    tmp_import_name_from_18 = tmp_import_from_3__module;

    CHECK_OBJECT( tmp_import_name_from_18 );
    tmp_assign_source_25 = IMPORT_NAME( tmp_import_name_from_18, const_str_plain_IntervalIndex );
    if ( tmp_assign_source_25 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 12;

        goto try_except_handler_3;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_IntervalIndex, tmp_assign_source_25 );
    tmp_import_name_from_19 = tmp_import_from_3__module;

    CHECK_OBJECT( tmp_import_name_from_19 );
    tmp_assign_source_26 = IMPORT_NAME( tmp_import_name_from_19, const_str_plain_TimedeltaIndex );
    if ( tmp_assign_source_26 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 12;

        goto try_except_handler_3;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_TimedeltaIndex, tmp_assign_source_26 );
    tmp_import_name_from_20 = tmp_import_from_3__module;

    CHECK_OBJECT( tmp_import_name_from_20 );
    tmp_assign_source_27 = IMPORT_NAME( tmp_import_name_from_20, const_str_plain_DatetimeIndex );
    if ( tmp_assign_source_27 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 12;

        goto try_except_handler_3;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_DatetimeIndex, tmp_assign_source_27 );
    tmp_import_name_from_21 = tmp_import_from_3__module;

    CHECK_OBJECT( tmp_import_name_from_21 );
    tmp_assign_source_28 = IMPORT_NAME( tmp_import_name_from_21, const_str_plain_PeriodIndex );
    if ( tmp_assign_source_28 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 12;

        goto try_except_handler_3;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_PeriodIndex, tmp_assign_source_28 );
    tmp_import_name_from_22 = tmp_import_from_3__module;

    CHECK_OBJECT( tmp_import_name_from_22 );
    tmp_assign_source_29 = IMPORT_NAME( tmp_import_name_from_22, const_str_plain_NaT );
    if ( tmp_assign_source_29 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 12;

        goto try_except_handler_3;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_NaT, tmp_assign_source_29 );
    goto try_end_3;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_3__module );
    Py_DECREF( tmp_import_from_3__module );
    tmp_import_from_3__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_3__module );
    Py_DECREF( tmp_import_from_3__module );
    tmp_import_from_3__module = NULL;

    tmp_name_name_8 = const_str_digest_169802d28602a65b8c5223cb1d6d6476;
    tmp_globals_name_8 = (PyObject *)moduledict_pandas$core$api;
    tmp_locals_name_8 = Py_None;
    tmp_fromlist_name_8 = const_tuple_str_plain_Period_str_plain_period_range_str_plain_pnow_tuple;
    tmp_level_name_8 = const_int_0;
    frame_dbe0f55d8853d10efb95642c43a805eb->m_frame.f_lineno = 17;
    tmp_assign_source_30 = IMPORT_MODULE5( tmp_name_name_8, tmp_globals_name_8, tmp_locals_name_8, tmp_fromlist_name_8, tmp_level_name_8 );
    if ( tmp_assign_source_30 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 17;

        goto frame_exception_exit_1;
    }
    assert( tmp_import_from_4__module == NULL );
    tmp_import_from_4__module = tmp_assign_source_30;

    // Tried code:
    tmp_import_name_from_23 = tmp_import_from_4__module;

    CHECK_OBJECT( tmp_import_name_from_23 );
    tmp_assign_source_31 = IMPORT_NAME( tmp_import_name_from_23, const_str_plain_Period );
    if ( tmp_assign_source_31 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 17;

        goto try_except_handler_4;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_Period, tmp_assign_source_31 );
    tmp_import_name_from_24 = tmp_import_from_4__module;

    CHECK_OBJECT( tmp_import_name_from_24 );
    tmp_assign_source_32 = IMPORT_NAME( tmp_import_name_from_24, const_str_plain_period_range );
    if ( tmp_assign_source_32 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 17;

        goto try_except_handler_4;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_period_range, tmp_assign_source_32 );
    tmp_import_name_from_25 = tmp_import_from_4__module;

    CHECK_OBJECT( tmp_import_name_from_25 );
    tmp_assign_source_33 = IMPORT_NAME( tmp_import_name_from_25, const_str_plain_pnow );
    if ( tmp_assign_source_33 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 17;

        goto try_except_handler_4;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_pnow, tmp_assign_source_33 );
    goto try_end_4;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_4__module );
    Py_DECREF( tmp_import_from_4__module );
    tmp_import_from_4__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto frame_exception_exit_1;
    // End of try:
    try_end_4:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_4__module );
    Py_DECREF( tmp_import_from_4__module );
    tmp_import_from_4__module = NULL;

    tmp_name_name_9 = const_str_digest_fff453aacf5b49a73746709e4ca465bf;
    tmp_globals_name_9 = (PyObject *)moduledict_pandas$core$api;
    tmp_locals_name_9 = Py_None;
    tmp_fromlist_name_9 = const_tuple_str_plain_Timedelta_str_plain_timedelta_range_tuple;
    tmp_level_name_9 = const_int_0;
    frame_dbe0f55d8853d10efb95642c43a805eb->m_frame.f_lineno = 18;
    tmp_assign_source_34 = IMPORT_MODULE5( tmp_name_name_9, tmp_globals_name_9, tmp_locals_name_9, tmp_fromlist_name_9, tmp_level_name_9 );
    if ( tmp_assign_source_34 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 18;

        goto frame_exception_exit_1;
    }
    assert( tmp_import_from_5__module == NULL );
    tmp_import_from_5__module = tmp_assign_source_34;

    // Tried code:
    tmp_import_name_from_26 = tmp_import_from_5__module;

    CHECK_OBJECT( tmp_import_name_from_26 );
    tmp_assign_source_35 = IMPORT_NAME( tmp_import_name_from_26, const_str_plain_Timedelta );
    if ( tmp_assign_source_35 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 18;

        goto try_except_handler_5;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_Timedelta, tmp_assign_source_35 );
    tmp_import_name_from_27 = tmp_import_from_5__module;

    CHECK_OBJECT( tmp_import_name_from_27 );
    tmp_assign_source_36 = IMPORT_NAME( tmp_import_name_from_27, const_str_plain_timedelta_range );
    if ( tmp_assign_source_36 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 18;

        goto try_except_handler_5;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_timedelta_range, tmp_assign_source_36 );
    goto try_end_5;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_5__module );
    Py_DECREF( tmp_import_from_5__module );
    tmp_import_from_5__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto frame_exception_exit_1;
    // End of try:
    try_end_5:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_5__module );
    Py_DECREF( tmp_import_from_5__module );
    tmp_import_from_5__module = NULL;

    tmp_name_name_10 = const_str_digest_f0f277988b36a31f11f2111700ac3a0c;
    tmp_globals_name_10 = (PyObject *)moduledict_pandas$core$api;
    tmp_locals_name_10 = Py_None;
    tmp_fromlist_name_10 = const_tuple_e5a08678b3493f1efaffaa88d05034c3_tuple;
    tmp_level_name_10 = const_int_0;
    frame_dbe0f55d8853d10efb95642c43a805eb->m_frame.f_lineno = 19;
    tmp_assign_source_37 = IMPORT_MODULE5( tmp_name_name_10, tmp_globals_name_10, tmp_locals_name_10, tmp_fromlist_name_10, tmp_level_name_10 );
    if ( tmp_assign_source_37 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 19;

        goto frame_exception_exit_1;
    }
    assert( tmp_import_from_6__module == NULL );
    tmp_import_from_6__module = tmp_assign_source_37;

    // Tried code:
    tmp_import_name_from_28 = tmp_import_from_6__module;

    CHECK_OBJECT( tmp_import_name_from_28 );
    tmp_assign_source_38 = IMPORT_NAME( tmp_import_name_from_28, const_str_plain_Timestamp );
    if ( tmp_assign_source_38 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 19;

        goto try_except_handler_6;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_Timestamp, tmp_assign_source_38 );
    tmp_import_name_from_29 = tmp_import_from_6__module;

    CHECK_OBJECT( tmp_import_name_from_29 );
    tmp_assign_source_39 = IMPORT_NAME( tmp_import_name_from_29, const_str_plain_date_range );
    if ( tmp_assign_source_39 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 19;

        goto try_except_handler_6;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_date_range, tmp_assign_source_39 );
    tmp_import_name_from_30 = tmp_import_from_6__module;

    CHECK_OBJECT( tmp_import_name_from_30 );
    tmp_assign_source_40 = IMPORT_NAME( tmp_import_name_from_30, const_str_plain_bdate_range );
    if ( tmp_assign_source_40 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 19;

        goto try_except_handler_6;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_bdate_range, tmp_assign_source_40 );
    goto try_end_6;
    // Exception handler code:
    try_except_handler_6:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_keeper_lineno_6 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_6__module );
    Py_DECREF( tmp_import_from_6__module );
    tmp_import_from_6__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_6;
    exception_value = exception_keeper_value_6;
    exception_tb = exception_keeper_tb_6;
    exception_lineno = exception_keeper_lineno_6;

    goto frame_exception_exit_1;
    // End of try:
    try_end_6:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_6__module );
    Py_DECREF( tmp_import_from_6__module );
    tmp_import_from_6__module = NULL;

    tmp_name_name_11 = const_str_digest_3c57dc0e67e1943d78d8cf3f2010b4ac;
    tmp_globals_name_11 = (PyObject *)moduledict_pandas$core$api;
    tmp_locals_name_11 = Py_None;
    tmp_fromlist_name_11 = const_tuple_str_plain_Interval_str_plain_interval_range_tuple;
    tmp_level_name_11 = const_int_0;
    frame_dbe0f55d8853d10efb95642c43a805eb->m_frame.f_lineno = 20;
    tmp_assign_source_41 = IMPORT_MODULE5( tmp_name_name_11, tmp_globals_name_11, tmp_locals_name_11, tmp_fromlist_name_11, tmp_level_name_11 );
    if ( tmp_assign_source_41 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 20;

        goto frame_exception_exit_1;
    }
    assert( tmp_import_from_7__module == NULL );
    tmp_import_from_7__module = tmp_assign_source_41;

    // Tried code:
    tmp_import_name_from_31 = tmp_import_from_7__module;

    CHECK_OBJECT( tmp_import_name_from_31 );
    tmp_assign_source_42 = IMPORT_NAME( tmp_import_name_from_31, const_str_plain_Interval );
    if ( tmp_assign_source_42 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 20;

        goto try_except_handler_7;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_Interval, tmp_assign_source_42 );
    tmp_import_name_from_32 = tmp_import_from_7__module;

    CHECK_OBJECT( tmp_import_name_from_32 );
    tmp_assign_source_43 = IMPORT_NAME( tmp_import_name_from_32, const_str_plain_interval_range );
    if ( tmp_assign_source_43 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 20;

        goto try_except_handler_7;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_interval_range, tmp_assign_source_43 );
    goto try_end_7;
    // Exception handler code:
    try_except_handler_7:;
    exception_keeper_type_7 = exception_type;
    exception_keeper_value_7 = exception_value;
    exception_keeper_tb_7 = exception_tb;
    exception_keeper_lineno_7 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_7__module );
    Py_DECREF( tmp_import_from_7__module );
    tmp_import_from_7__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_7;
    exception_value = exception_keeper_value_7;
    exception_tb = exception_keeper_tb_7;
    exception_lineno = exception_keeper_lineno_7;

    goto frame_exception_exit_1;
    // End of try:
    try_end_7:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_7__module );
    Py_DECREF( tmp_import_from_7__module );
    tmp_import_from_7__module = NULL;

    tmp_name_name_12 = const_str_digest_d5ccb2cadb8e132648b5cca1015acbda;
    tmp_globals_name_12 = (PyObject *)moduledict_pandas$core$api;
    tmp_locals_name_12 = Py_None;
    tmp_fromlist_name_12 = const_tuple_str_plain_Series_tuple;
    tmp_level_name_12 = const_int_0;
    frame_dbe0f55d8853d10efb95642c43a805eb->m_frame.f_lineno = 22;
    tmp_import_name_from_33 = IMPORT_MODULE5( tmp_name_name_12, tmp_globals_name_12, tmp_locals_name_12, tmp_fromlist_name_12, tmp_level_name_12 );
    if ( tmp_import_name_from_33 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 22;

        goto frame_exception_exit_1;
    }
    tmp_assign_source_44 = IMPORT_NAME( tmp_import_name_from_33, const_str_plain_Series );
    Py_DECREF( tmp_import_name_from_33 );
    if ( tmp_assign_source_44 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 22;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_Series, tmp_assign_source_44 );
    tmp_name_name_13 = const_str_digest_ac61716297b632aadf9bf726edeabdee;
    tmp_globals_name_13 = (PyObject *)moduledict_pandas$core$api;
    tmp_locals_name_13 = Py_None;
    tmp_fromlist_name_13 = const_tuple_str_plain_DataFrame_tuple;
    tmp_level_name_13 = const_int_0;
    frame_dbe0f55d8853d10efb95642c43a805eb->m_frame.f_lineno = 23;
    tmp_import_name_from_34 = IMPORT_MODULE5( tmp_name_name_13, tmp_globals_name_13, tmp_locals_name_13, tmp_fromlist_name_13, tmp_level_name_13 );
    if ( tmp_import_name_from_34 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 23;

        goto frame_exception_exit_1;
    }
    tmp_assign_source_45 = IMPORT_NAME( tmp_import_name_from_34, const_str_plain_DataFrame );
    Py_DECREF( tmp_import_name_from_34 );
    if ( tmp_assign_source_45 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 23;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_DataFrame, tmp_assign_source_45 );
    tmp_name_name_14 = const_str_digest_10e8a3a733a03b1cfd04180fe1bdad2c;
    tmp_globals_name_14 = (PyObject *)moduledict_pandas$core$api;
    tmp_locals_name_14 = Py_None;
    tmp_fromlist_name_14 = const_tuple_str_plain_Panel_str_plain_WidePanel_tuple;
    tmp_level_name_14 = const_int_0;
    frame_dbe0f55d8853d10efb95642c43a805eb->m_frame.f_lineno = 24;
    tmp_assign_source_46 = IMPORT_MODULE5( tmp_name_name_14, tmp_globals_name_14, tmp_locals_name_14, tmp_fromlist_name_14, tmp_level_name_14 );
    if ( tmp_assign_source_46 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 24;

        goto frame_exception_exit_1;
    }
    assert( tmp_import_from_8__module == NULL );
    tmp_import_from_8__module = tmp_assign_source_46;

    // Tried code:
    tmp_import_name_from_35 = tmp_import_from_8__module;

    CHECK_OBJECT( tmp_import_name_from_35 );
    tmp_assign_source_47 = IMPORT_NAME( tmp_import_name_from_35, const_str_plain_Panel );
    if ( tmp_assign_source_47 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 24;

        goto try_except_handler_8;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_Panel, tmp_assign_source_47 );
    tmp_import_name_from_36 = tmp_import_from_8__module;

    CHECK_OBJECT( tmp_import_name_from_36 );
    tmp_assign_source_48 = IMPORT_NAME( tmp_import_name_from_36, const_str_plain_WidePanel );
    if ( tmp_assign_source_48 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 24;

        goto try_except_handler_8;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_WidePanel, tmp_assign_source_48 );
    goto try_end_8;
    // Exception handler code:
    try_except_handler_8:;
    exception_keeper_type_8 = exception_type;
    exception_keeper_value_8 = exception_value;
    exception_keeper_tb_8 = exception_tb;
    exception_keeper_lineno_8 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_8__module );
    Py_DECREF( tmp_import_from_8__module );
    tmp_import_from_8__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_8;
    exception_value = exception_keeper_value_8;
    exception_tb = exception_keeper_tb_8;
    exception_lineno = exception_keeper_lineno_8;

    goto frame_exception_exit_1;
    // End of try:
    try_end_8:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_8__module );
    Py_DECREF( tmp_import_from_8__module );
    tmp_import_from_8__module = NULL;

    tmp_name_name_15 = const_str_digest_5414863e6321797cbb507ce7fdd47628;
    tmp_globals_name_15 = (PyObject *)moduledict_pandas$core$api;
    tmp_locals_name_15 = Py_None;
    tmp_fromlist_name_15 = const_tuple_str_plain_get_dummies_tuple;
    tmp_level_name_15 = const_int_0;
    frame_dbe0f55d8853d10efb95642c43a805eb->m_frame.f_lineno = 27;
    tmp_import_name_from_37 = IMPORT_MODULE5( tmp_name_name_15, tmp_globals_name_15, tmp_locals_name_15, tmp_fromlist_name_15, tmp_level_name_15 );
    if ( tmp_import_name_from_37 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 27;

        goto frame_exception_exit_1;
    }
    tmp_assign_source_49 = IMPORT_NAME( tmp_import_name_from_37, const_str_plain_get_dummies );
    Py_DECREF( tmp_import_name_from_37 );
    if ( tmp_assign_source_49 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 27;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_get_dummies, tmp_assign_source_49 );
    tmp_name_name_16 = const_str_digest_09671eb6f6f5841ab7ba447ea0609426;
    tmp_globals_name_16 = (PyObject *)moduledict_pandas$core$api;
    tmp_locals_name_16 = Py_None;
    tmp_fromlist_name_16 = const_tuple_str_plain_IndexSlice_tuple;
    tmp_level_name_16 = const_int_0;
    frame_dbe0f55d8853d10efb95642c43a805eb->m_frame.f_lineno = 29;
    tmp_import_name_from_38 = IMPORT_MODULE5( tmp_name_name_16, tmp_globals_name_16, tmp_locals_name_16, tmp_fromlist_name_16, tmp_level_name_16 );
    if ( tmp_import_name_from_38 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 29;

        goto frame_exception_exit_1;
    }
    tmp_assign_source_50 = IMPORT_NAME( tmp_import_name_from_38, const_str_plain_IndexSlice );
    Py_DECREF( tmp_import_name_from_38 );
    if ( tmp_assign_source_50 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 29;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_IndexSlice, tmp_assign_source_50 );
    tmp_name_name_17 = const_str_digest_495447daacfda0078e7b8b6f413ed7fe;
    tmp_globals_name_17 = (PyObject *)moduledict_pandas$core$api;
    tmp_locals_name_17 = Py_None;
    tmp_fromlist_name_17 = const_tuple_str_plain_to_numeric_tuple;
    tmp_level_name_17 = const_int_0;
    frame_dbe0f55d8853d10efb95642c43a805eb->m_frame.f_lineno = 30;
    tmp_import_name_from_39 = IMPORT_MODULE5( tmp_name_name_17, tmp_globals_name_17, tmp_locals_name_17, tmp_fromlist_name_17, tmp_level_name_17 );
    if ( tmp_import_name_from_39 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 30;

        goto frame_exception_exit_1;
    }
    tmp_assign_source_51 = IMPORT_NAME( tmp_import_name_from_39, const_str_plain_to_numeric );
    Py_DECREF( tmp_import_name_from_39 );
    if ( tmp_assign_source_51 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 30;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_to_numeric, tmp_assign_source_51 );
    tmp_name_name_18 = const_str_digest_10dcddc71871fe95ec2eb7687818be70;
    tmp_globals_name_18 = (PyObject *)moduledict_pandas$core$api;
    tmp_locals_name_18 = Py_None;
    tmp_fromlist_name_18 = const_tuple_str_plain_DateOffset_tuple;
    tmp_level_name_18 = const_int_0;
    frame_dbe0f55d8853d10efb95642c43a805eb->m_frame.f_lineno = 31;
    tmp_import_name_from_40 = IMPORT_MODULE5( tmp_name_name_18, tmp_globals_name_18, tmp_locals_name_18, tmp_fromlist_name_18, tmp_level_name_18 );
    if ( tmp_import_name_from_40 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 31;

        goto frame_exception_exit_1;
    }
    tmp_assign_source_52 = IMPORT_NAME( tmp_import_name_from_40, const_str_plain_DateOffset );
    Py_DECREF( tmp_import_name_from_40 );
    if ( tmp_assign_source_52 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 31;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_DateOffset, tmp_assign_source_52 );
    tmp_name_name_19 = const_str_digest_9d7f7de63f0d063a345683906ec87dfc;
    tmp_globals_name_19 = (PyObject *)moduledict_pandas$core$api;
    tmp_locals_name_19 = Py_None;
    tmp_fromlist_name_19 = const_tuple_str_plain_to_datetime_tuple;
    tmp_level_name_19 = const_int_0;
    frame_dbe0f55d8853d10efb95642c43a805eb->m_frame.f_lineno = 32;
    tmp_import_name_from_41 = IMPORT_MODULE5( tmp_name_name_19, tmp_globals_name_19, tmp_locals_name_19, tmp_fromlist_name_19, tmp_level_name_19 );
    if ( tmp_import_name_from_41 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 32;

        goto frame_exception_exit_1;
    }
    tmp_assign_source_53 = IMPORT_NAME( tmp_import_name_from_41, const_str_plain_to_datetime );
    Py_DECREF( tmp_import_name_from_41 );
    if ( tmp_assign_source_53 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 32;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_to_datetime, tmp_assign_source_53 );
    tmp_name_name_20 = const_str_digest_e2891cfc97ee3db7bef15d3875bcf8e9;
    tmp_globals_name_20 = (PyObject *)moduledict_pandas$core$api;
    tmp_locals_name_20 = Py_None;
    tmp_fromlist_name_20 = const_tuple_str_plain_to_timedelta_tuple;
    tmp_level_name_20 = const_int_0;
    frame_dbe0f55d8853d10efb95642c43a805eb->m_frame.f_lineno = 33;
    tmp_import_name_from_42 = IMPORT_MODULE5( tmp_name_name_20, tmp_globals_name_20, tmp_locals_name_20, tmp_fromlist_name_20, tmp_level_name_20 );
    if ( tmp_import_name_from_42 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 33;

        goto frame_exception_exit_1;
    }
    tmp_assign_source_54 = IMPORT_NAME( tmp_import_name_from_42, const_str_plain_to_timedelta );
    Py_DECREF( tmp_import_name_from_42 );
    if ( tmp_assign_source_54 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 33;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_to_timedelta, tmp_assign_source_54 );
    tmp_name_name_21 = const_str_digest_3a0b927a1532ded681dd49b7ca17edfe;
    tmp_globals_name_21 = (PyObject *)moduledict_pandas$core$api;
    tmp_locals_name_21 = Py_None;
    tmp_fromlist_name_21 = const_tuple_str_plain__DeprecatedModule_tuple;
    tmp_level_name_21 = const_int_0;
    frame_dbe0f55d8853d10efb95642c43a805eb->m_frame.f_lineno = 36;
    tmp_import_name_from_43 = IMPORT_MODULE5( tmp_name_name_21, tmp_globals_name_21, tmp_locals_name_21, tmp_fromlist_name_21, tmp_level_name_21 );
    if ( tmp_import_name_from_43 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 36;

        goto frame_exception_exit_1;
    }
    tmp_assign_source_55 = IMPORT_NAME( tmp_import_name_from_43, const_str_plain__DeprecatedModule );
    Py_DECREF( tmp_import_name_from_43 );
    if ( tmp_assign_source_55 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 36;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain__DeprecatedModule, tmp_assign_source_55 );
    tmp_assign_source_56 = LIST_COPY( const_list_426f738cd785da6b9cfb0c148a34955c_list );
    UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain__removals, tmp_assign_source_56 );
    tmp_called_name_1 = GET_STRING_DICT_VALUE( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain__DeprecatedModule );

    if (unlikely( tmp_called_name_1 == NULL ))
    {
        tmp_called_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__DeprecatedModule );
    }

    CHECK_OBJECT( tmp_called_name_1 );
    tmp_dict_key_1 = const_str_plain_deprmod;
    tmp_dict_value_1 = const_str_digest_633df9c5f680f8c7ac120b8872359722;
    tmp_kw_name_1 = _PyDict_NewPresized( 2 );
    tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_2 = const_str_plain_removals;
    tmp_dict_value_2 = GET_STRING_DICT_VALUE( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain__removals );

    if (unlikely( tmp_dict_value_2 == NULL ))
    {
        tmp_dict_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__removals );
    }

    CHECK_OBJECT( tmp_dict_value_2 );
    tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
    assert( !(tmp_res != 0) );
    frame_dbe0f55d8853d10efb95642c43a805eb->m_frame.f_lineno = 43;
    tmp_assign_source_57 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
    Py_DECREF( tmp_kw_name_1 );
    if ( tmp_assign_source_57 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 43;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_datetools, tmp_assign_source_57 );
    tmp_name_name_22 = const_str_digest_53eb620cf819e7f2e2a90c9ff72c4e4b;
    tmp_globals_name_22 = (PyObject *)moduledict_pandas$core$api;
    tmp_locals_name_22 = Py_None;
    tmp_fromlist_name_22 = const_tuple_5ee883fcebb5e3009913e0dee164fc9a_tuple;
    tmp_level_name_22 = const_int_0;
    frame_dbe0f55d8853d10efb95642c43a805eb->m_frame.f_lineno = 46;
    tmp_assign_source_58 = IMPORT_MODULE5( tmp_name_name_22, tmp_globals_name_22, tmp_locals_name_22, tmp_fromlist_name_22, tmp_level_name_22 );
    if ( tmp_assign_source_58 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 46;

        goto frame_exception_exit_1;
    }
    assert( tmp_import_from_9__module == NULL );
    tmp_import_from_9__module = tmp_assign_source_58;

    // Tried code:
    tmp_import_name_from_44 = tmp_import_from_9__module;

    CHECK_OBJECT( tmp_import_name_from_44 );
    tmp_assign_source_59 = IMPORT_NAME( tmp_import_name_from_44, const_str_plain_get_option );
    if ( tmp_assign_source_59 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 46;

        goto try_except_handler_9;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_get_option, tmp_assign_source_59 );
    tmp_import_name_from_45 = tmp_import_from_9__module;

    CHECK_OBJECT( tmp_import_name_from_45 );
    tmp_assign_source_60 = IMPORT_NAME( tmp_import_name_from_45, const_str_plain_set_option );
    if ( tmp_assign_source_60 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 46;

        goto try_except_handler_9;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_set_option, tmp_assign_source_60 );
    tmp_import_name_from_46 = tmp_import_from_9__module;

    CHECK_OBJECT( tmp_import_name_from_46 );
    tmp_assign_source_61 = IMPORT_NAME( tmp_import_name_from_46, const_str_plain_reset_option );
    if ( tmp_assign_source_61 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 46;

        goto try_except_handler_9;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_reset_option, tmp_assign_source_61 );
    tmp_import_name_from_47 = tmp_import_from_9__module;

    CHECK_OBJECT( tmp_import_name_from_47 );
    tmp_assign_source_62 = IMPORT_NAME( tmp_import_name_from_47, const_str_plain_describe_option );
    if ( tmp_assign_source_62 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 46;

        goto try_except_handler_9;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_describe_option, tmp_assign_source_62 );
    tmp_import_name_from_48 = tmp_import_from_9__module;

    CHECK_OBJECT( tmp_import_name_from_48 );
    tmp_assign_source_63 = IMPORT_NAME( tmp_import_name_from_48, const_str_plain_option_context );
    if ( tmp_assign_source_63 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 46;

        goto try_except_handler_9;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_option_context, tmp_assign_source_63 );
    tmp_import_name_from_49 = tmp_import_from_9__module;

    CHECK_OBJECT( tmp_import_name_from_49 );
    tmp_assign_source_64 = IMPORT_NAME( tmp_import_name_from_49, const_str_plain_options );
    if ( tmp_assign_source_64 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 46;

        goto try_except_handler_9;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_options, tmp_assign_source_64 );
    goto try_end_9;
    // Exception handler code:
    try_except_handler_9:;
    exception_keeper_type_9 = exception_type;
    exception_keeper_value_9 = exception_value;
    exception_keeper_tb_9 = exception_tb;
    exception_keeper_lineno_9 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_9__module );
    Py_DECREF( tmp_import_from_9__module );
    tmp_import_from_9__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_9;
    exception_value = exception_keeper_value_9;
    exception_tb = exception_keeper_tb_9;
    exception_lineno = exception_keeper_lineno_9;

    goto frame_exception_exit_1;
    // End of try:
    try_end_9:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_9__module );
    Py_DECREF( tmp_import_from_9__module );
    tmp_import_from_9__module = NULL;

    tmp_assign_source_65 = MAKE_FUNCTION_pandas$core$api$$$function_1_match(  );
    UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_match, tmp_assign_source_65 );
    tmp_assign_source_66 = MAKE_FUNCTION_pandas$core$api$$$function_2_groupby(  );
    UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_groupby, tmp_assign_source_66 );
    tmp_assign_source_67 = PyDict_New();
    assert( tmp_class_creation_1__class_decl_dict == NULL );
    tmp_class_creation_1__class_decl_dict = tmp_assign_source_67;

    // Tried code:
    tmp_key_name_1 = const_str_plain_metaclass;
    tmp_dict_name_1 = tmp_class_creation_1__class_decl_dict;

    CHECK_OBJECT( tmp_dict_name_1 );
    tmp_res = PyDict_Contains( tmp_dict_name_1, tmp_key_name_1 );
    if ( tmp_res == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 72;

        goto try_except_handler_10;
    }
    tmp_cond_value_1 = BOOL_FROM( tmp_res == 1 );
    tmp_cond_truth_1 = CHECK_IF_TRUE( tmp_cond_value_1 );
    if ( tmp_cond_truth_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 72;

        goto try_except_handler_10;
    }
    if ( tmp_cond_truth_1 == 1 )
    {
        goto condexpr_true_1;
    }
    else
    {
        goto condexpr_false_1;
    }
    condexpr_true_1:;
    tmp_dict_name_2 = tmp_class_creation_1__class_decl_dict;

    CHECK_OBJECT( tmp_dict_name_2 );
    tmp_key_name_2 = const_str_plain_metaclass;
    tmp_metaclass_name_1 = DICT_GET_ITEM( tmp_dict_name_2, tmp_key_name_2 );
    if ( tmp_metaclass_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 72;

        goto try_except_handler_10;
    }
    goto condexpr_end_1;
    condexpr_false_1:;
    tmp_metaclass_name_1 = LOOKUP_BUILTIN( const_str_plain_type );
    assert( tmp_metaclass_name_1 != NULL );
    Py_INCREF( tmp_metaclass_name_1 );
    condexpr_end_1:;
    tmp_bases_name_1 = const_tuple_type_object_tuple;
    tmp_assign_source_68 = SELECT_METACLASS( tmp_metaclass_name_1, tmp_bases_name_1 );
    Py_DECREF( tmp_metaclass_name_1 );
    if ( tmp_assign_source_68 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 72;

        goto try_except_handler_10;
    }
    assert( tmp_class_creation_1__metaclass == NULL );
    tmp_class_creation_1__metaclass = tmp_assign_source_68;

    tmp_key_name_3 = const_str_plain_metaclass;
    tmp_dict_name_3 = tmp_class_creation_1__class_decl_dict;

    CHECK_OBJECT( tmp_dict_name_3 );
    tmp_res = PyDict_Contains( tmp_dict_name_3, tmp_key_name_3 );
    if ( tmp_res == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 72;

        goto try_except_handler_10;
    }
    tmp_cond_value_2 = BOOL_FROM( tmp_res == 1 );
    tmp_cond_truth_2 = CHECK_IF_TRUE( tmp_cond_value_2 );
    if ( tmp_cond_truth_2 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 72;

        goto try_except_handler_10;
    }
    if ( tmp_cond_truth_2 == 1 )
    {
        goto branch_yes_1;
    }
    else
    {
        goto branch_no_1;
    }
    branch_yes_1:;
    tmp_dictdel_dict = tmp_class_creation_1__class_decl_dict;

    CHECK_OBJECT( tmp_dictdel_dict );
    tmp_dictdel_key = const_str_plain_metaclass;
    tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 72;

        goto try_except_handler_10;
    }
    branch_no_1:;
    tmp_hasattr_source_1 = tmp_class_creation_1__metaclass;

    CHECK_OBJECT( tmp_hasattr_source_1 );
    tmp_hasattr_attr_1 = const_str_plain___prepare__;
    tmp_res = PyObject_HasAttr( tmp_hasattr_source_1, tmp_hasattr_attr_1 );
    if ( tmp_res == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 72;

        goto try_except_handler_10;
    }
    if ( tmp_res == 1 )
    {
        goto condexpr_true_2;
    }
    else
    {
        goto condexpr_false_2;
    }
    condexpr_true_2:;
    tmp_source_name_1 = tmp_class_creation_1__metaclass;

    CHECK_OBJECT( tmp_source_name_1 );
    tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___prepare__ );
    if ( tmp_called_name_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 72;

        goto try_except_handler_10;
    }
    tmp_args_name_1 = const_tuple_str_plain_TimeGrouper_tuple_type_object_tuple_tuple;
    tmp_kw_name_2 = tmp_class_creation_1__class_decl_dict;

    CHECK_OBJECT( tmp_kw_name_2 );
    frame_dbe0f55d8853d10efb95642c43a805eb->m_frame.f_lineno = 72;
    tmp_assign_source_69 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_1, tmp_kw_name_2 );
    Py_DECREF( tmp_called_name_2 );
    if ( tmp_assign_source_69 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 72;

        goto try_except_handler_10;
    }
    goto condexpr_end_2;
    condexpr_false_2:;
    tmp_assign_source_69 = PyDict_New();
    condexpr_end_2:;
    assert( tmp_class_creation_1__prepared == NULL );
    tmp_class_creation_1__prepared = tmp_assign_source_69;

    tmp_set_locals = tmp_class_creation_1__prepared;

    CHECK_OBJECT( tmp_set_locals );
    locals_pandas$core$api_72 = tmp_set_locals;
    Py_INCREF( tmp_set_locals );
    // Tried code:
    // Tried code:
    tmp_dictset_value = const_str_digest_3fa11e71aa92f39879464952ab264ea1;
    tmp_res = PyObject_SetItem( locals_pandas$core$api_72, const_str_plain___module__, tmp_dictset_value );
    if ( tmp_res != 0 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 72;

        goto try_except_handler_12;
    }
    tmp_dictset_value = const_str_plain_TimeGrouper;
    tmp_res = PyObject_SetItem( locals_pandas$core$api_72, const_str_plain___qualname__, tmp_dictset_value );
    if ( tmp_res != 0 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 72;

        goto try_except_handler_12;
    }
    MAKE_OR_REUSE_FRAME( cache_frame_3704b79fdd1618b1647ea7104edbb749_2, codeobj_3704b79fdd1618b1647ea7104edbb749, module_pandas$core$api, sizeof(void *) );
    frame_3704b79fdd1618b1647ea7104edbb749_2 = cache_frame_3704b79fdd1618b1647ea7104edbb749_2;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_3704b79fdd1618b1647ea7104edbb749_2 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_3704b79fdd1618b1647ea7104edbb749_2 ) == 2 ); // Frame stack

    // Framed code:
    tmp_dictset_value = MAKE_FUNCTION_pandas$core$api$$$function_3___new__(  );
    tmp_res = PyObject_SetItem( locals_pandas$core$api_72, const_str_plain___new__, tmp_dictset_value );
    Py_DECREF( tmp_dictset_value );
    if ( tmp_res != 0 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 74;
        type_description_2 = "N";
        goto frame_exception_exit_2;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3704b79fdd1618b1647ea7104edbb749_2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_2:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3704b79fdd1618b1647ea7104edbb749_2 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_3704b79fdd1618b1647ea7104edbb749_2, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_3704b79fdd1618b1647ea7104edbb749_2->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_3704b79fdd1618b1647ea7104edbb749_2, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_3704b79fdd1618b1647ea7104edbb749_2,
        type_description_2,
        NULL
    );


    // Release cached frame.
    if ( frame_3704b79fdd1618b1647ea7104edbb749_2 == cache_frame_3704b79fdd1618b1647ea7104edbb749_2 )
    {
        Py_DECREF( frame_3704b79fdd1618b1647ea7104edbb749_2 );
    }
    cache_frame_3704b79fdd1618b1647ea7104edbb749_2 = NULL;

    assertFrameObject( frame_3704b79fdd1618b1647ea7104edbb749_2 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto nested_frame_exit_1;

    frame_no_exception_1:;

    goto skip_nested_handling_1;
    nested_frame_exit_1:;

    goto try_except_handler_12;
    skip_nested_handling_1:;
    tmp_called_name_3 = tmp_class_creation_1__metaclass;

    CHECK_OBJECT( tmp_called_name_3 );
    tmp_tuple_element_1 = const_str_plain_TimeGrouper;
    tmp_args_name_2 = PyTuple_New( 3 );
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_1 );
    tmp_tuple_element_1 = const_tuple_type_object_tuple;
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_1 );
    tmp_tuple_element_1 = locals_pandas$core$api_72;
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_args_name_2, 2, tmp_tuple_element_1 );
    tmp_kw_name_3 = tmp_class_creation_1__class_decl_dict;

    CHECK_OBJECT( tmp_kw_name_3 );
    frame_dbe0f55d8853d10efb95642c43a805eb->m_frame.f_lineno = 72;
    tmp_assign_source_71 = CALL_FUNCTION( tmp_called_name_3, tmp_args_name_2, tmp_kw_name_3 );
    Py_DECREF( tmp_args_name_2 );
    if ( tmp_assign_source_71 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 72;

        goto try_except_handler_12;
    }
    assert( outline_0_var___class__ == NULL );
    outline_0_var___class__ = tmp_assign_source_71;

    tmp_outline_return_value_1 = outline_0_var___class__;

    CHECK_OBJECT( tmp_outline_return_value_1 );
    Py_INCREF( tmp_outline_return_value_1 );
    goto try_return_handler_12;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$core$api );
    return MOD_RETURN_VALUE( NULL );
    // Return handler code:
    try_return_handler_12:;
    Py_DECREF( locals_pandas$core$api_72 );
    locals_pandas$core$api_72 = NULL;
    goto try_return_handler_11;
    // Exception handler code:
    try_except_handler_12:;
    exception_keeper_type_10 = exception_type;
    exception_keeper_value_10 = exception_value;
    exception_keeper_tb_10 = exception_tb;
    exception_keeper_lineno_10 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_DECREF( locals_pandas$core$api_72 );
    locals_pandas$core$api_72 = NULL;
    // Re-raise.
    exception_type = exception_keeper_type_10;
    exception_value = exception_keeper_value_10;
    exception_tb = exception_keeper_tb_10;
    exception_lineno = exception_keeper_lineno_10;

    goto try_except_handler_11;
    // End of try:
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$core$api );
    return MOD_RETURN_VALUE( NULL );
    // Return handler code:
    try_return_handler_11:;
    CHECK_OBJECT( (PyObject *)outline_0_var___class__ );
    Py_DECREF( outline_0_var___class__ );
    outline_0_var___class__ = NULL;

    goto outline_result_1;
    // Exception handler code:
    try_except_handler_11:;
    exception_keeper_type_11 = exception_type;
    exception_keeper_value_11 = exception_value;
    exception_keeper_tb_11 = exception_tb;
    exception_keeper_lineno_11 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Re-raise.
    exception_type = exception_keeper_type_11;
    exception_value = exception_keeper_value_11;
    exception_tb = exception_keeper_tb_11;
    exception_lineno = exception_keeper_lineno_11;

    goto outline_exception_1;
    // End of try:
    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$core$api );
    return MOD_RETURN_VALUE( NULL );
    outline_exception_1:;
    exception_lineno = 72;
    goto try_except_handler_10;
    outline_result_1:;
    tmp_assign_source_70 = tmp_outline_return_value_1;
    UPDATE_STRING_DICT1( moduledict_pandas$core$api, (Nuitka_StringObject *)const_str_plain_TimeGrouper, tmp_assign_source_70 );
    goto try_end_10;
    // Exception handler code:
    try_except_handler_10:;
    exception_keeper_type_12 = exception_type;
    exception_keeper_value_12 = exception_value;
    exception_keeper_tb_12 = exception_tb;
    exception_keeper_lineno_12 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_decl_dict );
    Py_DECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_12;
    exception_value = exception_keeper_value_12;
    exception_tb = exception_keeper_tb_12;
    exception_lineno = exception_keeper_lineno_12;

    goto frame_exception_exit_1;
    // End of try:
    try_end_10:;

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_dbe0f55d8853d10efb95642c43a805eb );
#endif
    popFrameStack();

    assertFrameObject( frame_dbe0f55d8853d10efb95642c43a805eb );

    goto frame_no_exception_2;
    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_dbe0f55d8853d10efb95642c43a805eb );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_dbe0f55d8853d10efb95642c43a805eb, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_dbe0f55d8853d10efb95642c43a805eb->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_dbe0f55d8853d10efb95642c43a805eb, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;
    frame_no_exception_2:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_decl_dict );
    Py_DECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__metaclass );
    Py_DECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__prepared );
    Py_DECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;


    return MOD_RETURN_VALUE( module_pandas$core$api );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
