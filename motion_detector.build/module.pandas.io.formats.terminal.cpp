/* Generated code for Python source for module 'pandas.io.formats.terminal'
 * created by Nuitka version 0.5.32.7
 *
 * This code is in part copyright 2018 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The _module_pandas$io$formats$terminal is a Python object pointer of module type. */

/* Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_pandas$io$formats$terminal;
PyDictObject *moduledict_pandas$io$formats$terminal;

/* The module constants used, if any. */
static PyObject *const_tuple_str_plain_platform_str_plain_current_os_str_plain_tuple_xy_tuple;
static PyObject *const_int_neg_12;
static PyObject *const_str_plain_ctermid;
static PyObject *const_str_plain_TIOCGWINSZ;
extern PyObject *const_int_pos_22;
extern PyObject *const_str_plain_is_terminal;
extern PyObject *const_str_plain___file__;
extern PyObject *const_str_plain_h;
extern PyObject *const_str_plain_struct;
extern PyObject *const_str_plain_unpack;
static PyObject *const_str_plain_proc;
extern PyObject *const_int_pos_80;
extern PyObject *const_str_digest_77d8fb15a813ae44a7b8d0fe53dbee10;
extern PyObject *const_str_plain_env;
extern PyObject *const_str_plain_os;
extern PyObject *const_str_plain_Windows;
extern PyObject *const_str_plain_None;
extern PyObject *const_str_plain_input;
extern PyObject *const_int_pos_25;
extern PyObject *const_str_plain_windll;
extern PyObject *const_str_plain_stdout;
extern PyObject *const_tuple_int_0_int_0_tuple;
extern PyObject *const_str_plain_get_ipython;
extern PyObject *const_str_plain_Linux;
extern PyObject *const_str_plain_platform;
static PyObject *const_str_plain_cr;
extern PyObject *const_tuple_int_pos_1_tuple;
extern PyObject *const_str_plain_PY3;
extern PyObject *const_str_plain_PIPE;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_plain_Popen;
extern PyObject *const_str_plain___debug__;
extern PyObject *const_str_plain_res;
extern PyObject *const_str_plain_environ;
extern PyObject *const_str_plain_close;
static PyObject *const_str_plain_wattr;
static PyObject *const_str_plain_GetConsoleScreenBufferInfo;
static PyObject *const_str_plain_CYGWIN;
static PyObject *const_str_plain_cury;
static PyObject *const_str_plain__get_terminal_size_linux;
static PyObject *const_str_plain_ioctl_GWINSZ;
extern PyObject *const_str_plain_Darwin;
static PyObject *const_tuple_351cad8b98bd89595c599ae10f1c4d06_tuple;
static PyObject *const_tuple_str_plain_windll_str_plain_create_string_buffer_tuple;
static PyObject *const_str_digest_91b004598065964580198c685f797b19;
static PyObject *const_str_plain_kernel;
static PyObject *const_str_digest_86ebc03f7d51dd311ae038c07a3ff924;
extern PyObject *const_str_plain_subprocess;
static PyObject *const_str_plain_hh;
extern PyObject *const_str_plain_fd;
static PyObject *const_str_plain_fcntl;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_plain_top;
static PyObject *const_tuple_02160f79b02041f358130560bec5314f_tuple;
extern PyObject *const_str_digest_b3f0e8a09cdae8f83a14da645f450b0f;
static PyObject *const_str_plain_curx;
static PyObject *const_str_plain_LINES;
extern PyObject *const_tuple_str_plain_PY3_tuple;
extern PyObject *const_str_plain_raw;
static PyObject *const_str_digest_145c2ebfed8679633c4f9bbfa8c8f20a;
static PyObject *const_str_plain_bufy;
extern PyObject *const_tuple_str_plain_ip_tuple;
static PyObject *const_tuple_int_pos_80_int_pos_25_tuple;
static PyObject *const_str_plain_tput;
static PyObject *const_str_plain_maxx;
static PyObject *const_list_str_plain_tput_str_plain_cols_list;
extern PyObject *const_tuple_int_0_tuple;
extern PyObject *const_str_plain_rows;
extern PyObject *const_str_plain_ip;
static PyObject *const_tuple_int_pos_22_tuple;
static PyObject *const_tuple_list_str_plain_tput_str_plain_cols_list_tuple;
extern PyObject *const_str_plain_False;
extern PyObject *const_str_plain_output;
extern PyObject *const_str_plain_kernel32;
extern PyObject *const_str_plain___all__;
static PyObject *const_str_plain_hhhhHhhhhhh;
static PyObject *const_list_str_plain_tput_str_plain_lines_list;
static PyObject *const_str_plain__get_terminal_size_tput;
static PyObject *const_tuple_str_plain_environ_tuple;
extern PyObject *const_str_plain_ctypes;
static PyObject *const_str_plain_maxy;
extern PyObject *const_int_0;
extern PyObject *const_dict_c453dda1d3901e4cb85d5781f8962504;
static PyObject *const_tuple_d4a1699c992f4a3105963c5f3cf8e0df_tuple;
extern PyObject *const_tuple_int_pos_2_tuple;
static PyObject *const_tuple_b9a0e40978ece55d2a7ddbed30a18036_tuple;
static PyObject *const_str_plain_1234;
static PyObject *const_str_plain_termios;
static PyObject *const_str_plain_sizex;
static PyObject *const_str_plain_GetStdHandle;
static PyObject *const_str_plain_ioctl;
extern PyObject *const_str_plain_right;
static PyObject *const_str_digest_915ec85778b84c0fcd94bd44b4cee0da;
static PyObject *const_str_plain_create_string_buffer;
static PyObject *const_str_plain_tuple_xy;
extern PyObject *const_str_plain_communicate;
extern PyObject *const_str_plain_shutil;
static PyObject *const_str_plain_COLUMNS;
extern PyObject *const_str_plain_system;
static PyObject *const_str_plain_sizey;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_plain_print_function;
static PyObject *const_str_plain_bufx;
static PyObject *const_str_plain_csbi;
extern PyObject *const_str_plain_bottom;
extern PyObject *const_int_pos_1;
static PyObject *const_tuple_list_str_plain_tput_str_plain_lines_list_tuple;
extern PyObject *const_str_plain_get_terminal_size;
static PyObject *const_list_str_plain_get_terminal_size_str_plain_is_terminal_list;
static PyObject *const_tuple_int_neg_12_tuple;
extern PyObject *const_str_plain_open;
static PyObject *const_str_plain__get_terminal_size_windows;
static PyObject *const_str_plain_current_os;
extern PyObject *const_str_plain_left;
extern PyObject *const_str_plain_cols;
static PyObject *const_str_plain_O_RDONLY;
extern PyObject *const_str_plain_lines;
extern PyObject *const_str_plain_stdin;
extern PyObject *const_int_pos_2;
extern PyObject *const_str_plain_startswith;
static PyObject *const_str_digest_4c9bf748cdb22ad0e6140f64ea934bb9;
static PyObject *const_tuple_str_plain_CYGWIN_tuple;
static PyObject *const_str_digest_f1bafcec87d980754615941e5c03a7c5;
static PyObject *module_filename_obj;

static bool constants_created = false;

static void createModuleConstants( void )
{
    const_tuple_str_plain_platform_str_plain_current_os_str_plain_tuple_xy_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_platform_str_plain_current_os_str_plain_tuple_xy_tuple, 0, const_str_plain_platform ); Py_INCREF( const_str_plain_platform );
    const_str_plain_current_os = UNSTREAM_STRING( &constant_bin[ 3032853 ], 10, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_platform_str_plain_current_os_str_plain_tuple_xy_tuple, 1, const_str_plain_current_os ); Py_INCREF( const_str_plain_current_os );
    const_str_plain_tuple_xy = UNSTREAM_STRING( &constant_bin[ 3032863 ], 8, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_platform_str_plain_current_os_str_plain_tuple_xy_tuple, 2, const_str_plain_tuple_xy ); Py_INCREF( const_str_plain_tuple_xy );
    const_int_neg_12 = PyLong_FromLong( -12l );
    const_str_plain_ctermid = UNSTREAM_STRING( &constant_bin[ 3032871 ], 7, 1 );
    const_str_plain_TIOCGWINSZ = UNSTREAM_STRING( &constant_bin[ 3032878 ], 10, 1 );
    const_str_plain_proc = UNSTREAM_STRING( &constant_bin[ 27834 ], 4, 1 );
    const_str_plain_cr = UNSTREAM_STRING( &constant_bin[ 6165 ], 2, 1 );
    const_str_plain_wattr = UNSTREAM_STRING( &constant_bin[ 3032888 ], 5, 1 );
    const_str_plain_GetConsoleScreenBufferInfo = UNSTREAM_STRING( &constant_bin[ 3032893 ], 26, 1 );
    const_str_plain_CYGWIN = UNSTREAM_STRING( &constant_bin[ 3032919 ], 6, 1 );
    const_str_plain_cury = UNSTREAM_STRING( &constant_bin[ 3032925 ], 4, 1 );
    const_str_plain__get_terminal_size_linux = UNSTREAM_STRING( &constant_bin[ 3032929 ], 24, 1 );
    const_str_plain_ioctl_GWINSZ = UNSTREAM_STRING( &constant_bin[ 3032953 ], 12, 1 );
    const_tuple_351cad8b98bd89595c599ae10f1c4d06_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_351cad8b98bd89595c599ae10f1c4d06_tuple, 0, const_str_plain_subprocess ); Py_INCREF( const_str_plain_subprocess );
    PyTuple_SET_ITEM( const_tuple_351cad8b98bd89595c599ae10f1c4d06_tuple, 1, const_str_plain_proc ); Py_INCREF( const_str_plain_proc );
    PyTuple_SET_ITEM( const_tuple_351cad8b98bd89595c599ae10f1c4d06_tuple, 2, const_str_plain_output ); Py_INCREF( const_str_plain_output );
    PyTuple_SET_ITEM( const_tuple_351cad8b98bd89595c599ae10f1c4d06_tuple, 3, const_str_plain_cols ); Py_INCREF( const_str_plain_cols );
    PyTuple_SET_ITEM( const_tuple_351cad8b98bd89595c599ae10f1c4d06_tuple, 4, const_str_plain_rows ); Py_INCREF( const_str_plain_rows );
    const_tuple_str_plain_windll_str_plain_create_string_buffer_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_windll_str_plain_create_string_buffer_tuple, 0, const_str_plain_windll ); Py_INCREF( const_str_plain_windll );
    const_str_plain_create_string_buffer = UNSTREAM_STRING( &constant_bin[ 3032965 ], 20, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_windll_str_plain_create_string_buffer_tuple, 1, const_str_plain_create_string_buffer ); Py_INCREF( const_str_plain_create_string_buffer );
    const_str_digest_91b004598065964580198c685f797b19 = UNSTREAM_STRING( &constant_bin[ 3032985 ], 35, 0 );
    const_str_plain_kernel = UNSTREAM_STRING( &constant_bin[ 847531 ], 6, 1 );
    const_str_digest_86ebc03f7d51dd311ae038c07a3ff924 = UNSTREAM_STRING( &constant_bin[ 3033020 ], 363, 0 );
    const_str_plain_hh = UNSTREAM_STRING( &constant_bin[ 22377 ], 2, 1 );
    const_str_plain_fcntl = UNSTREAM_STRING( &constant_bin[ 3033383 ], 5, 1 );
    const_tuple_02160f79b02041f358130560bec5314f_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_02160f79b02041f358130560bec5314f_tuple, 0, const_str_plain_fd ); Py_INCREF( const_str_plain_fd );
    PyTuple_SET_ITEM( const_tuple_02160f79b02041f358130560bec5314f_tuple, 1, const_str_plain_fcntl ); Py_INCREF( const_str_plain_fcntl );
    const_str_plain_termios = UNSTREAM_STRING( &constant_bin[ 3033388 ], 7, 1 );
    PyTuple_SET_ITEM( const_tuple_02160f79b02041f358130560bec5314f_tuple, 2, const_str_plain_termios ); Py_INCREF( const_str_plain_termios );
    PyTuple_SET_ITEM( const_tuple_02160f79b02041f358130560bec5314f_tuple, 3, const_str_plain_struct ); Py_INCREF( const_str_plain_struct );
    PyTuple_SET_ITEM( const_tuple_02160f79b02041f358130560bec5314f_tuple, 4, const_str_plain_cr ); Py_INCREF( const_str_plain_cr );
    const_str_plain_curx = UNSTREAM_STRING( &constant_bin[ 3033395 ], 4, 1 );
    const_str_plain_LINES = UNSTREAM_STRING( &constant_bin[ 3033399 ], 5, 1 );
    const_str_digest_145c2ebfed8679633c4f9bbfa8c8f20a = UNSTREAM_STRING( &constant_bin[ 3033404 ], 46, 0 );
    const_str_plain_bufy = UNSTREAM_STRING( &constant_bin[ 3033450 ], 4, 1 );
    const_tuple_int_pos_80_int_pos_25_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_int_pos_80_int_pos_25_tuple, 0, const_int_pos_80 ); Py_INCREF( const_int_pos_80 );
    PyTuple_SET_ITEM( const_tuple_int_pos_80_int_pos_25_tuple, 1, const_int_pos_25 ); Py_INCREF( const_int_pos_25 );
    const_str_plain_tput = UNSTREAM_STRING( &constant_bin[ 37885 ], 4, 1 );
    const_str_plain_maxx = UNSTREAM_STRING( &constant_bin[ 3033454 ], 4, 1 );
    const_list_str_plain_tput_str_plain_cols_list = PyList_New( 2 );
    PyList_SET_ITEM( const_list_str_plain_tput_str_plain_cols_list, 0, const_str_plain_tput ); Py_INCREF( const_str_plain_tput );
    PyList_SET_ITEM( const_list_str_plain_tput_str_plain_cols_list, 1, const_str_plain_cols ); Py_INCREF( const_str_plain_cols );
    const_tuple_int_pos_22_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_int_pos_22_tuple, 0, const_int_pos_22 ); Py_INCREF( const_int_pos_22 );
    const_tuple_list_str_plain_tput_str_plain_cols_list_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_list_str_plain_tput_str_plain_cols_list_tuple, 0, const_list_str_plain_tput_str_plain_cols_list ); Py_INCREF( const_list_str_plain_tput_str_plain_cols_list );
    const_str_plain_hhhhHhhhhhh = UNSTREAM_STRING( &constant_bin[ 3033458 ], 11, 1 );
    const_list_str_plain_tput_str_plain_lines_list = PyList_New( 2 );
    PyList_SET_ITEM( const_list_str_plain_tput_str_plain_lines_list, 0, const_str_plain_tput ); Py_INCREF( const_str_plain_tput );
    PyList_SET_ITEM( const_list_str_plain_tput_str_plain_lines_list, 1, const_str_plain_lines ); Py_INCREF( const_str_plain_lines );
    const_str_plain__get_terminal_size_tput = UNSTREAM_STRING( &constant_bin[ 3033469 ], 23, 1 );
    const_tuple_str_plain_environ_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_environ_tuple, 0, const_str_plain_environ ); Py_INCREF( const_str_plain_environ );
    const_str_plain_maxy = UNSTREAM_STRING( &constant_bin[ 3033492 ], 4, 1 );
    const_tuple_d4a1699c992f4a3105963c5f3cf8e0df_tuple = PyTuple_New( 19 );
    PyTuple_SET_ITEM( const_tuple_d4a1699c992f4a3105963c5f3cf8e0df_tuple, 0, const_str_plain_res ); Py_INCREF( const_str_plain_res );
    PyTuple_SET_ITEM( const_tuple_d4a1699c992f4a3105963c5f3cf8e0df_tuple, 1, const_str_plain_windll ); Py_INCREF( const_str_plain_windll );
    PyTuple_SET_ITEM( const_tuple_d4a1699c992f4a3105963c5f3cf8e0df_tuple, 2, const_str_plain_create_string_buffer ); Py_INCREF( const_str_plain_create_string_buffer );
    PyTuple_SET_ITEM( const_tuple_d4a1699c992f4a3105963c5f3cf8e0df_tuple, 3, const_str_plain_h ); Py_INCREF( const_str_plain_h );
    const_str_plain_csbi = UNSTREAM_STRING( &constant_bin[ 3033496 ], 4, 1 );
    PyTuple_SET_ITEM( const_tuple_d4a1699c992f4a3105963c5f3cf8e0df_tuple, 4, const_str_plain_csbi ); Py_INCREF( const_str_plain_csbi );
    PyTuple_SET_ITEM( const_tuple_d4a1699c992f4a3105963c5f3cf8e0df_tuple, 5, const_str_plain_struct ); Py_INCREF( const_str_plain_struct );
    const_str_plain_bufx = UNSTREAM_STRING( &constant_bin[ 3033500 ], 4, 1 );
    PyTuple_SET_ITEM( const_tuple_d4a1699c992f4a3105963c5f3cf8e0df_tuple, 6, const_str_plain_bufx ); Py_INCREF( const_str_plain_bufx );
    PyTuple_SET_ITEM( const_tuple_d4a1699c992f4a3105963c5f3cf8e0df_tuple, 7, const_str_plain_bufy ); Py_INCREF( const_str_plain_bufy );
    PyTuple_SET_ITEM( const_tuple_d4a1699c992f4a3105963c5f3cf8e0df_tuple, 8, const_str_plain_curx ); Py_INCREF( const_str_plain_curx );
    PyTuple_SET_ITEM( const_tuple_d4a1699c992f4a3105963c5f3cf8e0df_tuple, 9, const_str_plain_cury ); Py_INCREF( const_str_plain_cury );
    PyTuple_SET_ITEM( const_tuple_d4a1699c992f4a3105963c5f3cf8e0df_tuple, 10, const_str_plain_wattr ); Py_INCREF( const_str_plain_wattr );
    PyTuple_SET_ITEM( const_tuple_d4a1699c992f4a3105963c5f3cf8e0df_tuple, 11, const_str_plain_left ); Py_INCREF( const_str_plain_left );
    PyTuple_SET_ITEM( const_tuple_d4a1699c992f4a3105963c5f3cf8e0df_tuple, 12, const_str_plain_top ); Py_INCREF( const_str_plain_top );
    PyTuple_SET_ITEM( const_tuple_d4a1699c992f4a3105963c5f3cf8e0df_tuple, 13, const_str_plain_right ); Py_INCREF( const_str_plain_right );
    PyTuple_SET_ITEM( const_tuple_d4a1699c992f4a3105963c5f3cf8e0df_tuple, 14, const_str_plain_bottom ); Py_INCREF( const_str_plain_bottom );
    PyTuple_SET_ITEM( const_tuple_d4a1699c992f4a3105963c5f3cf8e0df_tuple, 15, const_str_plain_maxx ); Py_INCREF( const_str_plain_maxx );
    PyTuple_SET_ITEM( const_tuple_d4a1699c992f4a3105963c5f3cf8e0df_tuple, 16, const_str_plain_maxy ); Py_INCREF( const_str_plain_maxy );
    const_str_plain_sizex = UNSTREAM_STRING( &constant_bin[ 3033504 ], 5, 1 );
    PyTuple_SET_ITEM( const_tuple_d4a1699c992f4a3105963c5f3cf8e0df_tuple, 17, const_str_plain_sizex ); Py_INCREF( const_str_plain_sizex );
    const_str_plain_sizey = UNSTREAM_STRING( &constant_bin[ 3033509 ], 5, 1 );
    PyTuple_SET_ITEM( const_tuple_d4a1699c992f4a3105963c5f3cf8e0df_tuple, 18, const_str_plain_sizey ); Py_INCREF( const_str_plain_sizey );
    const_tuple_b9a0e40978ece55d2a7ddbed30a18036_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_b9a0e40978ece55d2a7ddbed30a18036_tuple, 0, const_str_plain_ioctl_GWINSZ ); Py_INCREF( const_str_plain_ioctl_GWINSZ );
    PyTuple_SET_ITEM( const_tuple_b9a0e40978ece55d2a7ddbed30a18036_tuple, 1, const_str_plain_cr ); Py_INCREF( const_str_plain_cr );
    PyTuple_SET_ITEM( const_tuple_b9a0e40978ece55d2a7ddbed30a18036_tuple, 2, const_str_plain_fd ); Py_INCREF( const_str_plain_fd );
    PyTuple_SET_ITEM( const_tuple_b9a0e40978ece55d2a7ddbed30a18036_tuple, 3, const_str_plain_env ); Py_INCREF( const_str_plain_env );
    const_str_plain_1234 = UNSTREAM_STRING( &constant_bin[ 17682 ], 4, 0 );
    const_str_plain_GetStdHandle = UNSTREAM_STRING( &constant_bin[ 3033514 ], 12, 1 );
    const_str_plain_ioctl = UNSTREAM_STRING( &constant_bin[ 3032953 ], 5, 1 );
    const_str_digest_915ec85778b84c0fcd94bd44b4cee0da = UNSTREAM_STRING( &constant_bin[ 3033526 ], 208, 0 );
    const_str_plain_COLUMNS = UNSTREAM_STRING( &constant_bin[ 3033734 ], 7, 1 );
    const_tuple_list_str_plain_tput_str_plain_lines_list_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_list_str_plain_tput_str_plain_lines_list_tuple, 0, const_list_str_plain_tput_str_plain_lines_list ); Py_INCREF( const_list_str_plain_tput_str_plain_lines_list );
    const_list_str_plain_get_terminal_size_str_plain_is_terminal_list = PyList_New( 2 );
    PyList_SET_ITEM( const_list_str_plain_get_terminal_size_str_plain_is_terminal_list, 0, const_str_plain_get_terminal_size ); Py_INCREF( const_str_plain_get_terminal_size );
    PyList_SET_ITEM( const_list_str_plain_get_terminal_size_str_plain_is_terminal_list, 1, const_str_plain_is_terminal ); Py_INCREF( const_str_plain_is_terminal );
    const_tuple_int_neg_12_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_int_neg_12_tuple, 0, const_int_neg_12 ); Py_INCREF( const_int_neg_12 );
    const_str_plain__get_terminal_size_windows = UNSTREAM_STRING( &constant_bin[ 3033741 ], 26, 1 );
    const_str_plain_O_RDONLY = UNSTREAM_STRING( &constant_bin[ 3033767 ], 8, 1 );
    const_str_digest_4c9bf748cdb22ad0e6140f64ea934bb9 = UNSTREAM_STRING( &constant_bin[ 3033775 ], 92, 0 );
    const_tuple_str_plain_CYGWIN_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_CYGWIN_tuple, 0, const_str_plain_CYGWIN ); Py_INCREF( const_str_plain_CYGWIN );
    const_str_digest_f1bafcec87d980754615941e5c03a7c5 = UNSTREAM_STRING( &constant_bin[ 3033867 ], 122, 0 );

    constants_created = true;
}

#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_pandas$io$formats$terminal( void )
{
    // The module may not have been used at all.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_51d214fefb8f11993d01ef30b07c4fe9;
static PyCodeObject *codeobj_258cdca6404197d30b1b941d09cf2149;
static PyCodeObject *codeobj_8dbd5bcfd34403cd2b3caed96bddda71;
static PyCodeObject *codeobj_dc337648f1aa927fe0e89d0f7d98fae9;
static PyCodeObject *codeobj_9fc27c5bf449b233f4ec9f48b2e8c719;
static PyCodeObject *codeobj_08cb025df9e8c84d1e986a6fb7eb6773;
static PyCodeObject *codeobj_e3c64695b3adf8dd16e145a6a34ad4d4;

static void createModuleCodeObjects(void)
{
    module_filename_obj = const_str_digest_4c9bf748cdb22ad0e6140f64ea934bb9;
    codeobj_51d214fefb8f11993d01ef30b07c4fe9 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_91b004598065964580198c685f797b19, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_258cdca6404197d30b1b941d09cf2149 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__get_terminal_size_linux, 115, const_tuple_b9a0e40978ece55d2a7ddbed30a18036_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_8dbd5bcfd34403cd2b3caed96bddda71 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__get_terminal_size_tput, 94, const_tuple_351cad8b98bd89595c599ae10f1c4d06_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_dc337648f1aa927fe0e89d0f7d98fae9 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__get_terminal_size_windows, 69, const_tuple_d4a1699c992f4a3105963c5f3cf8e0df_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_9fc27c5bf449b233f4ec9f48b2e8c719 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_terminal_size, 24, const_tuple_str_plain_platform_str_plain_current_os_str_plain_tuple_xy_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_08cb025df9e8c84d1e986a6fb7eb6773 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_ioctl_GWINSZ, 116, const_tuple_02160f79b02041f358130560bec5314f_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_e3c64695b3adf8dd16e145a6a34ad4d4 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_is_terminal, 52, const_tuple_str_plain_ip_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
}

// The module function declarations.
static PyObject *MAKE_FUNCTION_pandas$io$formats$terminal$$$function_1_get_terminal_size(  );


static PyObject *MAKE_FUNCTION_pandas$io$formats$terminal$$$function_2_is_terminal(  );


static PyObject *MAKE_FUNCTION_pandas$io$formats$terminal$$$function_3__get_terminal_size_windows(  );


static PyObject *MAKE_FUNCTION_pandas$io$formats$terminal$$$function_4__get_terminal_size_tput(  );


static PyObject *MAKE_FUNCTION_pandas$io$formats$terminal$$$function_5__get_terminal_size_linux(  );


static PyObject *MAKE_FUNCTION_pandas$io$formats$terminal$$$function_5__get_terminal_size_linux$$$function_1_ioctl_GWINSZ(  );


// The module function definitions.
static PyObject *impl_pandas$io$formats$terminal$$$function_1_get_terminal_size( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *var_platform = NULL;
    PyObject *var_current_os = NULL;
    PyObject *var_tuple_xy = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_assign_source_5;
    PyObject *tmp_assign_source_6;
    PyObject *tmp_assign_source_7;
    PyObject *tmp_called_instance_1;
    PyObject *tmp_called_instance_2;
    PyObject *tmp_called_instance_3;
    PyObject *tmp_called_name_1;
    PyObject *tmp_called_name_2;
    PyObject *tmp_called_name_3;
    int tmp_cmp_Eq_1;
    PyObject *tmp_compare_left_1;
    PyObject *tmp_compare_left_2;
    PyObject *tmp_compare_left_3;
    PyObject *tmp_compare_right_1;
    PyObject *tmp_compare_right_2;
    PyObject *tmp_compare_right_3;
    PyObject *tmp_compexpr_left_1;
    PyObject *tmp_compexpr_left_2;
    PyObject *tmp_compexpr_right_1;
    PyObject *tmp_compexpr_right_2;
    int tmp_cond_truth_1;
    int tmp_cond_truth_2;
    PyObject *tmp_cond_value_1;
    PyObject *tmp_cond_value_2;
    PyObject *tmp_fromlist_name_1;
    PyObject *tmp_globals_name_1;
    bool tmp_is_1;
    bool tmp_is_2;
    PyObject *tmp_level_name_1;
    PyObject *tmp_locals_name_1;
    PyObject *tmp_name_name_1;
    int tmp_or_left_truth_1;
    int tmp_or_left_truth_2;
    PyObject *tmp_or_left_value_1;
    PyObject *tmp_or_left_value_2;
    PyObject *tmp_or_right_value_1;
    PyObject *tmp_or_right_value_2;
    PyObject *tmp_return_value;
    static struct Nuitka_FrameObject *cache_frame_9fc27c5bf449b233f4ec9f48b2e8c719 = NULL;

    struct Nuitka_FrameObject *frame_9fc27c5bf449b233f4ec9f48b2e8c719;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_9fc27c5bf449b233f4ec9f48b2e8c719, codeobj_9fc27c5bf449b233f4ec9f48b2e8c719, module_pandas$io$formats$terminal, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_9fc27c5bf449b233f4ec9f48b2e8c719 = cache_frame_9fc27c5bf449b233f4ec9f48b2e8c719;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_9fc27c5bf449b233f4ec9f48b2e8c719 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_9fc27c5bf449b233f4ec9f48b2e8c719 ) == 2 ); // Frame stack

    // Framed code:
    tmp_name_name_1 = const_str_plain_platform;
    tmp_globals_name_1 = (PyObject *)moduledict_pandas$io$formats$terminal;
    tmp_locals_name_1 = Py_None;
    tmp_fromlist_name_1 = Py_None;
    tmp_level_name_1 = const_int_0;
    frame_9fc27c5bf449b233f4ec9f48b2e8c719->m_frame.f_lineno = 31;
    tmp_assign_source_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
    if ( tmp_assign_source_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 31;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    assert( var_platform == NULL );
    var_platform = tmp_assign_source_1;

    tmp_cond_value_1 = GET_STRING_DICT_VALUE( moduledict_pandas$io$formats$terminal, (Nuitka_StringObject *)const_str_plain_PY3 );

    if (unlikely( tmp_cond_value_1 == NULL ))
    {
        tmp_cond_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PY3 );
    }

    if ( tmp_cond_value_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "PY3" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 33;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    tmp_cond_truth_1 = CHECK_IF_TRUE( tmp_cond_value_1 );
    if ( tmp_cond_truth_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 33;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    if ( tmp_cond_truth_1 == 1 )
    {
        goto branch_yes_1;
    }
    else
    {
        goto branch_no_1;
    }
    branch_yes_1:;
    tmp_called_instance_1 = GET_STRING_DICT_VALUE( moduledict_pandas$io$formats$terminal, (Nuitka_StringObject *)const_str_plain_shutil );

    if (unlikely( tmp_called_instance_1 == NULL ))
    {
        tmp_called_instance_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_shutil );
    }

    if ( tmp_called_instance_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "shutil" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 34;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    frame_9fc27c5bf449b233f4ec9f48b2e8c719->m_frame.f_lineno = 34;
    tmp_return_value = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_get_terminal_size );
    if ( tmp_return_value == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 34;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    goto frame_return_exit_1;
    branch_no_1:;
    tmp_called_instance_2 = var_platform;

    CHECK_OBJECT( tmp_called_instance_2 );
    frame_9fc27c5bf449b233f4ec9f48b2e8c719->m_frame.f_lineno = 36;
    tmp_assign_source_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_system );
    if ( tmp_assign_source_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 36;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    assert( var_current_os == NULL );
    var_current_os = tmp_assign_source_2;

    tmp_assign_source_3 = Py_None;
    assert( var_tuple_xy == NULL );
    Py_INCREF( tmp_assign_source_3 );
    var_tuple_xy = tmp_assign_source_3;

    tmp_compare_left_1 = var_current_os;

    CHECK_OBJECT( tmp_compare_left_1 );
    tmp_compare_right_1 = const_str_plain_Windows;
    tmp_cmp_Eq_1 = RICH_COMPARE_BOOL_EQ( tmp_compare_left_1, tmp_compare_right_1 );
    if ( tmp_cmp_Eq_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 38;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    if ( tmp_cmp_Eq_1 == 1 )
    {
        goto branch_yes_2;
    }
    else
    {
        goto branch_no_2;
    }
    branch_yes_2:;
    tmp_called_name_1 = GET_STRING_DICT_VALUE( moduledict_pandas$io$formats$terminal, (Nuitka_StringObject *)const_str_plain__get_terminal_size_windows );

    if (unlikely( tmp_called_name_1 == NULL ))
    {
        tmp_called_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__get_terminal_size_windows );
    }

    if ( tmp_called_name_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_get_terminal_size_windows" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 39;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    frame_9fc27c5bf449b233f4ec9f48b2e8c719->m_frame.f_lineno = 39;
    tmp_assign_source_4 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
    if ( tmp_assign_source_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 39;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    {
        PyObject *old = var_tuple_xy;
        var_tuple_xy = tmp_assign_source_4;
        Py_XDECREF( old );
    }

    tmp_compare_left_2 = var_tuple_xy;

    CHECK_OBJECT( tmp_compare_left_2 );
    tmp_compare_right_2 = Py_None;
    tmp_is_1 = ( tmp_compare_left_2 == tmp_compare_right_2 );
    if ( tmp_is_1 )
    {
        goto branch_yes_3;
    }
    else
    {
        goto branch_no_3;
    }
    branch_yes_3:;
    tmp_called_name_2 = GET_STRING_DICT_VALUE( moduledict_pandas$io$formats$terminal, (Nuitka_StringObject *)const_str_plain__get_terminal_size_tput );

    if (unlikely( tmp_called_name_2 == NULL ))
    {
        tmp_called_name_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__get_terminal_size_tput );
    }

    if ( tmp_called_name_2 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_get_terminal_size_tput" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 41;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    frame_9fc27c5bf449b233f4ec9f48b2e8c719->m_frame.f_lineno = 41;
    tmp_assign_source_5 = CALL_FUNCTION_NO_ARGS( tmp_called_name_2 );
    if ( tmp_assign_source_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 41;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    {
        PyObject *old = var_tuple_xy;
        var_tuple_xy = tmp_assign_source_5;
        Py_XDECREF( old );
    }

    branch_no_3:;
    branch_no_2:;
    tmp_compexpr_left_1 = var_current_os;

    if ( tmp_compexpr_left_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "current_os" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 43;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    tmp_compexpr_right_1 = const_str_plain_Linux;
    tmp_or_left_value_1 = RICH_COMPARE_EQ( tmp_compexpr_left_1, tmp_compexpr_right_1 );
    if ( tmp_or_left_value_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 43;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    tmp_or_left_truth_1 = CHECK_IF_TRUE( tmp_or_left_value_1 );
    if ( tmp_or_left_truth_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_or_left_value_1 );

        exception_lineno = 45;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    if ( tmp_or_left_truth_1 == 1 )
    {
        goto or_left_1;
    }
    else
    {
        goto or_right_1;
    }
    or_right_1:;
    Py_DECREF( tmp_or_left_value_1 );
    tmp_compexpr_left_2 = var_current_os;

    if ( tmp_compexpr_left_2 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "current_os" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 44;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    tmp_compexpr_right_2 = const_str_plain_Darwin;
    tmp_or_left_value_2 = RICH_COMPARE_EQ( tmp_compexpr_left_2, tmp_compexpr_right_2 );
    if ( tmp_or_left_value_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 44;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    tmp_or_left_truth_2 = CHECK_IF_TRUE( tmp_or_left_value_2 );
    if ( tmp_or_left_truth_2 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_or_left_value_2 );

        exception_lineno = 45;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    if ( tmp_or_left_truth_2 == 1 )
    {
        goto or_left_2;
    }
    else
    {
        goto or_right_2;
    }
    or_right_2:;
    Py_DECREF( tmp_or_left_value_2 );
    tmp_called_instance_3 = var_current_os;

    if ( tmp_called_instance_3 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "current_os" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 45;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    frame_9fc27c5bf449b233f4ec9f48b2e8c719->m_frame.f_lineno = 45;
    tmp_or_right_value_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_startswith, &PyTuple_GET_ITEM( const_tuple_str_plain_CYGWIN_tuple, 0 ) );

    if ( tmp_or_right_value_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 45;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    tmp_or_right_value_1 = tmp_or_right_value_2;
    goto or_end_2;
    or_left_2:;
    tmp_or_right_value_1 = tmp_or_left_value_2;
    or_end_2:;
    tmp_cond_value_2 = tmp_or_right_value_1;
    goto or_end_1;
    or_left_1:;
    tmp_cond_value_2 = tmp_or_left_value_1;
    or_end_1:;
    tmp_cond_truth_2 = CHECK_IF_TRUE( tmp_cond_value_2 );
    if ( tmp_cond_truth_2 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_cond_value_2 );

        exception_lineno = 45;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_cond_value_2 );
    if ( tmp_cond_truth_2 == 1 )
    {
        goto branch_yes_4;
    }
    else
    {
        goto branch_no_4;
    }
    branch_yes_4:;
    tmp_called_name_3 = GET_STRING_DICT_VALUE( moduledict_pandas$io$formats$terminal, (Nuitka_StringObject *)const_str_plain__get_terminal_size_linux );

    if (unlikely( tmp_called_name_3 == NULL ))
    {
        tmp_called_name_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__get_terminal_size_linux );
    }

    if ( tmp_called_name_3 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_get_terminal_size_linux" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 46;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    frame_9fc27c5bf449b233f4ec9f48b2e8c719->m_frame.f_lineno = 46;
    tmp_assign_source_6 = CALL_FUNCTION_NO_ARGS( tmp_called_name_3 );
    if ( tmp_assign_source_6 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 46;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }
    {
        PyObject *old = var_tuple_xy;
        var_tuple_xy = tmp_assign_source_6;
        Py_XDECREF( old );
    }

    branch_no_4:;
    tmp_compare_left_3 = var_tuple_xy;

    if ( tmp_compare_left_3 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "tuple_xy" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 47;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    tmp_compare_right_3 = Py_None;
    tmp_is_2 = ( tmp_compare_left_3 == tmp_compare_right_3 );
    if ( tmp_is_2 )
    {
        goto branch_yes_5;
    }
    else
    {
        goto branch_no_5;
    }
    branch_yes_5:;
    tmp_assign_source_7 = const_tuple_int_pos_80_int_pos_25_tuple;
    {
        PyObject *old = var_tuple_xy;
        var_tuple_xy = tmp_assign_source_7;
        Py_INCREF( var_tuple_xy );
        Py_XDECREF( old );
    }

    branch_no_5:;
    tmp_return_value = var_tuple_xy;

    if ( tmp_return_value == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "tuple_xy" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 49;
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

    Py_INCREF( tmp_return_value );
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9fc27c5bf449b233f4ec9f48b2e8c719 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_9fc27c5bf449b233f4ec9f48b2e8c719 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9fc27c5bf449b233f4ec9f48b2e8c719 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_9fc27c5bf449b233f4ec9f48b2e8c719, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_9fc27c5bf449b233f4ec9f48b2e8c719->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_9fc27c5bf449b233f4ec9f48b2e8c719, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_9fc27c5bf449b233f4ec9f48b2e8c719,
        type_description_1,
        var_platform,
        var_current_os,
        var_tuple_xy
    );


    // Release cached frame.
    if ( frame_9fc27c5bf449b233f4ec9f48b2e8c719 == cache_frame_9fc27c5bf449b233f4ec9f48b2e8c719 )
    {
        Py_DECREF( frame_9fc27c5bf449b233f4ec9f48b2e8c719 );
    }
    cache_frame_9fc27c5bf449b233f4ec9f48b2e8c719 = NULL;

    assertFrameObject( frame_9fc27c5bf449b233f4ec9f48b2e8c719 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$io$formats$terminal$$$function_1_get_terminal_size );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( var_platform );
    var_platform = NULL;

    Py_XDECREF( var_current_os );
    var_current_os = NULL;

    Py_XDECREF( var_tuple_xy );
    var_tuple_xy = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( var_platform );
    var_platform = NULL;

    Py_XDECREF( var_current_os );
    var_current_os = NULL;

    Py_XDECREF( var_tuple_xy );
    var_tuple_xy = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$io$formats$terminal$$$function_1_get_terminal_size );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$io$formats$terminal$$$function_2_is_terminal( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *var_ip = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_called_name_1;
    PyObject *tmp_compare_left_1;
    PyObject *tmp_compare_right_1;
    int tmp_exc_match_exception_match_1;
    PyObject *tmp_hasattr_attr_1;
    PyObject *tmp_hasattr_source_1;
    int tmp_res;
    bool tmp_result;
    PyObject *tmp_return_value;
    static struct Nuitka_FrameObject *cache_frame_e3c64695b3adf8dd16e145a6a34ad4d4 = NULL;

    struct Nuitka_FrameObject *frame_e3c64695b3adf8dd16e145a6a34ad4d4;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_e3c64695b3adf8dd16e145a6a34ad4d4, codeobj_e3c64695b3adf8dd16e145a6a34ad4d4, module_pandas$io$formats$terminal, sizeof(void *) );
    frame_e3c64695b3adf8dd16e145a6a34ad4d4 = cache_frame_e3c64695b3adf8dd16e145a6a34ad4d4;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_e3c64695b3adf8dd16e145a6a34ad4d4 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_e3c64695b3adf8dd16e145a6a34ad4d4 ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    // Tried code:
    tmp_called_name_1 = GET_STRING_DICT_VALUE( moduledict_pandas$io$formats$terminal, (Nuitka_StringObject *)const_str_plain_get_ipython );

    if (unlikely( tmp_called_name_1 == NULL ))
    {
        tmp_called_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_ipython );
    }

    if ( tmp_called_name_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_ipython" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 59;
        type_description_1 = "o";
        goto try_except_handler_3;
    }

    frame_e3c64695b3adf8dd16e145a6a34ad4d4->m_frame.f_lineno = 59;
    tmp_assign_source_1 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
    if ( tmp_assign_source_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 59;
        type_description_1 = "o";
        goto try_except_handler_3;
    }
    assert( var_ip == NULL );
    var_ip = tmp_assign_source_1;

    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_e3c64695b3adf8dd16e145a6a34ad4d4, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_e3c64695b3adf8dd16e145a6a34ad4d4, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    // Tried code:
    tmp_compare_left_1 = EXC_TYPE(PyThreadState_GET());
    tmp_compare_right_1 = PyExc_NameError;
    tmp_exc_match_exception_match_1 = EXCEPTION_MATCH_BOOL( tmp_compare_left_1, tmp_compare_right_1 );
    if ( tmp_exc_match_exception_match_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 60;
        type_description_1 = "o";
        goto try_except_handler_4;
    }
    if ( tmp_exc_match_exception_match_1 == 1 )
    {
        goto branch_yes_1;
    }
    else
    {
        goto branch_no_1;
    }
    branch_yes_1:;
    tmp_return_value = Py_True;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_4;
    goto branch_end_1;
    branch_no_1:;
    tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    if (unlikely( tmp_result == false ))
    {
        exception_lineno = 58;
    }

    if (exception_tb && exception_tb->tb_frame == &frame_e3c64695b3adf8dd16e145a6a34ad4d4->m_frame) frame_e3c64695b3adf8dd16e145a6a34ad4d4->m_frame.f_lineno = exception_tb->tb_lineno;
    type_description_1 = "o";
    goto try_except_handler_4;
    branch_end_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$io$formats$terminal$$$function_2_is_terminal );
    return NULL;
    // Return handler code:
    try_return_handler_4:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto frame_return_exit_1;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto try_except_handler_2;
    // End of try:
    // End of try:
    try_end_1:;
    tmp_hasattr_source_1 = var_ip;

    CHECK_OBJECT( tmp_hasattr_source_1 );
    tmp_hasattr_attr_1 = const_str_plain_kernel;
    tmp_res = PyObject_HasAttr( tmp_hasattr_source_1, tmp_hasattr_attr_1 );
    if ( tmp_res == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 63;
        type_description_1 = "o";
        goto try_except_handler_2;
    }
    if ( tmp_res == 1 )
    {
        goto branch_yes_2;
    }
    else
    {
        goto branch_no_2;
    }
    branch_yes_2:;
    tmp_return_value = Py_False;
    Py_INCREF( tmp_return_value );
    goto frame_return_exit_1;
    goto branch_end_2;
    branch_no_2:;
    tmp_return_value = Py_True;
    Py_INCREF( tmp_return_value );
    goto frame_return_exit_1;
    branch_end_2:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$io$formats$terminal$$$function_2_is_terminal );
    return NULL;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e3c64695b3adf8dd16e145a6a34ad4d4 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_e3c64695b3adf8dd16e145a6a34ad4d4 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e3c64695b3adf8dd16e145a6a34ad4d4 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_e3c64695b3adf8dd16e145a6a34ad4d4, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_e3c64695b3adf8dd16e145a6a34ad4d4->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_e3c64695b3adf8dd16e145a6a34ad4d4, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_e3c64695b3adf8dd16e145a6a34ad4d4,
        type_description_1,
        var_ip
    );


    // Release cached frame.
    if ( frame_e3c64695b3adf8dd16e145a6a34ad4d4 == cache_frame_e3c64695b3adf8dd16e145a6a34ad4d4 )
    {
        Py_DECREF( frame_e3c64695b3adf8dd16e145a6a34ad4d4 );
    }
    cache_frame_e3c64695b3adf8dd16e145a6a34ad4d4 = NULL;

    assertFrameObject( frame_e3c64695b3adf8dd16e145a6a34ad4d4 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$io$formats$terminal$$$function_2_is_terminal );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( var_ip );
    var_ip = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( var_ip );
    var_ip = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$io$formats$terminal$$$function_2_is_terminal );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$io$formats$terminal$$$function_3__get_terminal_size_windows( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *var_res = NULL;
    PyObject *var_windll = NULL;
    PyObject *var_create_string_buffer = NULL;
    PyObject *var_h = NULL;
    PyObject *var_csbi = NULL;
    PyObject *var_struct = NULL;
    PyObject *var_bufx = NULL;
    PyObject *var_bufy = NULL;
    PyObject *var_curx = NULL;
    PyObject *var_cury = NULL;
    PyObject *var_wattr = NULL;
    PyObject *var_left = NULL;
    PyObject *var_top = NULL;
    PyObject *var_right = NULL;
    PyObject *var_bottom = NULL;
    PyObject *var_maxx = NULL;
    PyObject *var_maxy = NULL;
    PyObject *var_sizex = NULL;
    PyObject *var_sizey = NULL;
    PyObject *tmp_import_from_1__module = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_10 = NULL;
    PyObject *tmp_tuple_unpack_1__element_11 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__element_3 = NULL;
    PyObject *tmp_tuple_unpack_1__element_4 = NULL;
    PyObject *tmp_tuple_unpack_1__element_5 = NULL;
    PyObject *tmp_tuple_unpack_1__element_6 = NULL;
    PyObject *tmp_tuple_unpack_1__element_7 = NULL;
    PyObject *tmp_tuple_unpack_1__element_8 = NULL;
    PyObject *tmp_tuple_unpack_1__element_9 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    PyObject *tmp_args_element_name_1;
    PyObject *tmp_args_element_name_2;
    PyObject *tmp_args_element_name_3;
    PyObject *tmp_args_element_name_4;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_assign_source_5;
    PyObject *tmp_assign_source_6;
    PyObject *tmp_assign_source_7;
    PyObject *tmp_assign_source_8;
    PyObject *tmp_assign_source_9;
    PyObject *tmp_assign_source_10;
    PyObject *tmp_assign_source_11;
    PyObject *tmp_assign_source_12;
    PyObject *tmp_assign_source_13;
    PyObject *tmp_assign_source_14;
    PyObject *tmp_assign_source_15;
    PyObject *tmp_assign_source_16;
    PyObject *tmp_assign_source_17;
    PyObject *tmp_assign_source_18;
    PyObject *tmp_assign_source_19;
    PyObject *tmp_assign_source_20;
    PyObject *tmp_assign_source_21;
    PyObject *tmp_assign_source_22;
    PyObject *tmp_assign_source_23;
    PyObject *tmp_assign_source_24;
    PyObject *tmp_assign_source_25;
    PyObject *tmp_assign_source_26;
    PyObject *tmp_assign_source_27;
    PyObject *tmp_assign_source_28;
    PyObject *tmp_assign_source_29;
    PyObject *tmp_assign_source_30;
    PyObject *tmp_assign_source_31;
    PyObject *tmp_assign_source_32;
    PyObject *tmp_assign_source_33;
    PyObject *tmp_called_instance_1;
    PyObject *tmp_called_name_1;
    PyObject *tmp_called_name_2;
    PyObject *tmp_called_name_3;
    int tmp_cond_truth_1;
    PyObject *tmp_cond_value_1;
    PyObject *tmp_fromlist_name_1;
    PyObject *tmp_fromlist_name_2;
    PyObject *tmp_globals_name_1;
    PyObject *tmp_globals_name_2;
    PyObject *tmp_import_name_from_1;
    PyObject *tmp_import_name_from_2;
    PyObject *tmp_iter_arg_1;
    PyObject *tmp_iterator_attempt;
    PyObject *tmp_iterator_name_1;
    PyObject *tmp_left_name_1;
    PyObject *tmp_left_name_2;
    PyObject *tmp_left_name_3;
    PyObject *tmp_left_name_4;
    PyObject *tmp_level_name_1;
    PyObject *tmp_level_name_2;
    PyObject *tmp_locals_name_1;
    PyObject *tmp_locals_name_2;
    PyObject *tmp_name_name_1;
    PyObject *tmp_name_name_2;
    PyObject *tmp_return_value;
    PyObject *tmp_right_name_1;
    PyObject *tmp_right_name_2;
    PyObject *tmp_right_name_3;
    PyObject *tmp_right_name_4;
    PyObject *tmp_source_name_1;
    PyObject *tmp_source_name_2;
    PyObject *tmp_source_name_3;
    PyObject *tmp_source_name_4;
    PyObject *tmp_source_name_5;
    PyObject *tmp_tuple_element_1;
    PyObject *tmp_unpack_1;
    PyObject *tmp_unpack_2;
    PyObject *tmp_unpack_3;
    PyObject *tmp_unpack_4;
    PyObject *tmp_unpack_5;
    PyObject *tmp_unpack_6;
    PyObject *tmp_unpack_7;
    PyObject *tmp_unpack_8;
    PyObject *tmp_unpack_9;
    PyObject *tmp_unpack_10;
    PyObject *tmp_unpack_11;
    static struct Nuitka_FrameObject *cache_frame_dc337648f1aa927fe0e89d0f7d98fae9 = NULL;

    struct Nuitka_FrameObject *frame_dc337648f1aa927fe0e89d0f7d98fae9;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    tmp_assign_source_1 = Py_None;
    assert( var_res == NULL );
    Py_INCREF( tmp_assign_source_1 );
    var_res = tmp_assign_source_1;

    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_dc337648f1aa927fe0e89d0f7d98fae9, codeobj_dc337648f1aa927fe0e89d0f7d98fae9, module_pandas$io$formats$terminal, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_dc337648f1aa927fe0e89d0f7d98fae9 = cache_frame_dc337648f1aa927fe0e89d0f7d98fae9;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_dc337648f1aa927fe0e89d0f7d98fae9 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_dc337648f1aa927fe0e89d0f7d98fae9 ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    tmp_name_name_1 = const_str_plain_ctypes;
    tmp_globals_name_1 = (PyObject *)moduledict_pandas$io$formats$terminal;
    tmp_locals_name_1 = Py_None;
    tmp_fromlist_name_1 = const_tuple_str_plain_windll_str_plain_create_string_buffer_tuple;
    tmp_level_name_1 = const_int_0;
    frame_dc337648f1aa927fe0e89d0f7d98fae9->m_frame.f_lineno = 72;
    tmp_assign_source_2 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
    if ( tmp_assign_source_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 72;
        type_description_1 = "ooooooooooooooooooo";
        goto try_except_handler_2;
    }
    assert( tmp_import_from_1__module == NULL );
    tmp_import_from_1__module = tmp_assign_source_2;

    // Tried code:
    tmp_import_name_from_1 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_1 );
    tmp_assign_source_3 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_windll );
    if ( tmp_assign_source_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 72;
        type_description_1 = "ooooooooooooooooooo";
        goto try_except_handler_3;
    }
    assert( var_windll == NULL );
    var_windll = tmp_assign_source_3;

    tmp_import_name_from_2 = tmp_import_from_1__module;

    CHECK_OBJECT( tmp_import_name_from_2 );
    tmp_assign_source_4 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_create_string_buffer );
    if ( tmp_assign_source_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 72;
        type_description_1 = "ooooooooooooooooooo";
        goto try_except_handler_3;
    }
    assert( var_create_string_buffer == NULL );
    var_create_string_buffer = tmp_assign_source_4;

    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_2;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    tmp_source_name_1 = var_windll;

    if ( tmp_source_name_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "windll" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 78;
        type_description_1 = "ooooooooooooooooooo";
        goto try_except_handler_2;
    }

    tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_kernel32 );
    if ( tmp_called_instance_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 78;
        type_description_1 = "ooooooooooooooooooo";
        goto try_except_handler_2;
    }
    frame_dc337648f1aa927fe0e89d0f7d98fae9->m_frame.f_lineno = 78;
    tmp_assign_source_5 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_GetStdHandle, &PyTuple_GET_ITEM( const_tuple_int_neg_12_tuple, 0 ) );

    Py_DECREF( tmp_called_instance_1 );
    if ( tmp_assign_source_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 78;
        type_description_1 = "ooooooooooooooooooo";
        goto try_except_handler_2;
    }
    assert( var_h == NULL );
    var_h = tmp_assign_source_5;

    tmp_called_name_1 = var_create_string_buffer;

    if ( tmp_called_name_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "create_string_buffer" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 79;
        type_description_1 = "ooooooooooooooooooo";
        goto try_except_handler_2;
    }

    frame_dc337648f1aa927fe0e89d0f7d98fae9->m_frame.f_lineno = 79;
    tmp_assign_source_6 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, &PyTuple_GET_ITEM( const_tuple_int_pos_22_tuple, 0 ) );

    if ( tmp_assign_source_6 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 79;
        type_description_1 = "ooooooooooooooooooo";
        goto try_except_handler_2;
    }
    assert( var_csbi == NULL );
    var_csbi = tmp_assign_source_6;

    tmp_source_name_3 = var_windll;

    if ( tmp_source_name_3 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "windll" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 80;
        type_description_1 = "ooooooooooooooooooo";
        goto try_except_handler_2;
    }

    tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_kernel32 );
    if ( tmp_source_name_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 80;
        type_description_1 = "ooooooooooooooooooo";
        goto try_except_handler_2;
    }
    tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_GetConsoleScreenBufferInfo );
    Py_DECREF( tmp_source_name_2 );
    if ( tmp_called_name_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 80;
        type_description_1 = "ooooooooooooooooooo";
        goto try_except_handler_2;
    }
    tmp_args_element_name_1 = var_h;

    if ( tmp_args_element_name_1 == NULL )
    {
        Py_DECREF( tmp_called_name_2 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "h" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 80;
        type_description_1 = "ooooooooooooooooooo";
        goto try_except_handler_2;
    }

    tmp_args_element_name_2 = var_csbi;

    if ( tmp_args_element_name_2 == NULL )
    {
        Py_DECREF( tmp_called_name_2 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "csbi" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 80;
        type_description_1 = "ooooooooooooooooooo";
        goto try_except_handler_2;
    }

    frame_dc337648f1aa927fe0e89d0f7d98fae9->m_frame.f_lineno = 80;
    {
        PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
        tmp_assign_source_7 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
    }

    Py_DECREF( tmp_called_name_2 );
    if ( tmp_assign_source_7 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 80;
        type_description_1 = "ooooooooooooooooooo";
        goto try_except_handler_2;
    }
    {
        PyObject *old = var_res;
        var_res = tmp_assign_source_7;
        Py_XDECREF( old );
    }

    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_2 == NULL )
    {
        exception_keeper_tb_2 = MAKE_TRACEBACK( frame_dc337648f1aa927fe0e89d0f7d98fae9, exception_keeper_lineno_2 );
    }
    else if ( exception_keeper_lineno_2 != 0 )
    {
        exception_keeper_tb_2 = ADD_TRACEBACK( exception_keeper_tb_2, frame_dc337648f1aa927fe0e89d0f7d98fae9, exception_keeper_lineno_2 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_2, &exception_keeper_value_2, &exception_keeper_tb_2 );
    PyException_SetTraceback( exception_keeper_value_2, (PyObject *)exception_keeper_tb_2 );
    PUBLISH_EXCEPTION( &exception_keeper_type_2, &exception_keeper_value_2, &exception_keeper_tb_2 );
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_4;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$io$formats$terminal$$$function_3__get_terminal_size_windows );
    return NULL;
    // Return handler code:
    try_return_handler_4:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto frame_return_exit_1;
    // End of try:
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto try_end_2;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$io$formats$terminal$$$function_3__get_terminal_size_windows );
    return NULL;
    // End of try:
    try_end_2:;
    tmp_cond_value_1 = var_res;

    if ( tmp_cond_value_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "res" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 83;
        type_description_1 = "ooooooooooooooooooo";
        goto frame_exception_exit_1;
    }

    tmp_cond_truth_1 = CHECK_IF_TRUE( tmp_cond_value_1 );
    if ( tmp_cond_truth_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 83;
        type_description_1 = "ooooooooooooooooooo";
        goto frame_exception_exit_1;
    }
    if ( tmp_cond_truth_1 == 1 )
    {
        goto branch_yes_1;
    }
    else
    {
        goto branch_no_1;
    }
    branch_yes_1:;
    tmp_name_name_2 = const_str_plain_struct;
    tmp_globals_name_2 = (PyObject *)moduledict_pandas$io$formats$terminal;
    tmp_locals_name_2 = Py_None;
    tmp_fromlist_name_2 = Py_None;
    tmp_level_name_2 = const_int_0;
    frame_dc337648f1aa927fe0e89d0f7d98fae9->m_frame.f_lineno = 84;
    tmp_assign_source_8 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
    if ( tmp_assign_source_8 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 84;
        type_description_1 = "ooooooooooooooooooo";
        goto frame_exception_exit_1;
    }
    assert( var_struct == NULL );
    var_struct = tmp_assign_source_8;

    // Tried code:
    tmp_source_name_4 = var_struct;

    CHECK_OBJECT( tmp_source_name_4 );
    tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_unpack );
    if ( tmp_called_name_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 86;
        type_description_1 = "ooooooooooooooooooo";
        goto try_except_handler_5;
    }
    tmp_args_element_name_3 = const_str_plain_hhhhHhhhhhh;
    tmp_source_name_5 = var_csbi;

    if ( tmp_source_name_5 == NULL )
    {
        Py_DECREF( tmp_called_name_3 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "csbi" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 86;
        type_description_1 = "ooooooooooooooooooo";
        goto try_except_handler_5;
    }

    tmp_args_element_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_raw );
    if ( tmp_args_element_name_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_3 );

        exception_lineno = 86;
        type_description_1 = "ooooooooooooooooooo";
        goto try_except_handler_5;
    }
    frame_dc337648f1aa927fe0e89d0f7d98fae9->m_frame.f_lineno = 86;
    {
        PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4 };
        tmp_iter_arg_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_3, call_args );
    }

    Py_DECREF( tmp_called_name_3 );
    Py_DECREF( tmp_args_element_name_4 );
    if ( tmp_iter_arg_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 86;
        type_description_1 = "ooooooooooooooooooo";
        goto try_except_handler_5;
    }
    tmp_assign_source_9 = MAKE_ITERATOR( tmp_iter_arg_1 );
    Py_DECREF( tmp_iter_arg_1 );
    if ( tmp_assign_source_9 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 85;
        type_description_1 = "ooooooooooooooooooo";
        goto try_except_handler_5;
    }
    assert( tmp_tuple_unpack_1__source_iter == NULL );
    tmp_tuple_unpack_1__source_iter = tmp_assign_source_9;

    // Tried code:
    tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;

    CHECK_OBJECT( tmp_unpack_1 );
    tmp_assign_source_10 = UNPACK_NEXT( tmp_unpack_1, 0, 11 );
    if ( tmp_assign_source_10 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        type_description_1 = "ooooooooooooooooooo";
        exception_lineno = 85;
        goto try_except_handler_6;
    }
    assert( tmp_tuple_unpack_1__element_1 == NULL );
    tmp_tuple_unpack_1__element_1 = tmp_assign_source_10;

    tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;

    CHECK_OBJECT( tmp_unpack_2 );
    tmp_assign_source_11 = UNPACK_NEXT( tmp_unpack_2, 1, 11 );
    if ( tmp_assign_source_11 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        type_description_1 = "ooooooooooooooooooo";
        exception_lineno = 85;
        goto try_except_handler_6;
    }
    assert( tmp_tuple_unpack_1__element_2 == NULL );
    tmp_tuple_unpack_1__element_2 = tmp_assign_source_11;

    tmp_unpack_3 = tmp_tuple_unpack_1__source_iter;

    CHECK_OBJECT( tmp_unpack_3 );
    tmp_assign_source_12 = UNPACK_NEXT( tmp_unpack_3, 2, 11 );
    if ( tmp_assign_source_12 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        type_description_1 = "ooooooooooooooooooo";
        exception_lineno = 85;
        goto try_except_handler_6;
    }
    assert( tmp_tuple_unpack_1__element_3 == NULL );
    tmp_tuple_unpack_1__element_3 = tmp_assign_source_12;

    tmp_unpack_4 = tmp_tuple_unpack_1__source_iter;

    CHECK_OBJECT( tmp_unpack_4 );
    tmp_assign_source_13 = UNPACK_NEXT( tmp_unpack_4, 3, 11 );
    if ( tmp_assign_source_13 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        type_description_1 = "ooooooooooooooooooo";
        exception_lineno = 85;
        goto try_except_handler_6;
    }
    assert( tmp_tuple_unpack_1__element_4 == NULL );
    tmp_tuple_unpack_1__element_4 = tmp_assign_source_13;

    tmp_unpack_5 = tmp_tuple_unpack_1__source_iter;

    CHECK_OBJECT( tmp_unpack_5 );
    tmp_assign_source_14 = UNPACK_NEXT( tmp_unpack_5, 4, 11 );
    if ( tmp_assign_source_14 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        type_description_1 = "ooooooooooooooooooo";
        exception_lineno = 85;
        goto try_except_handler_6;
    }
    assert( tmp_tuple_unpack_1__element_5 == NULL );
    tmp_tuple_unpack_1__element_5 = tmp_assign_source_14;

    tmp_unpack_6 = tmp_tuple_unpack_1__source_iter;

    CHECK_OBJECT( tmp_unpack_6 );
    tmp_assign_source_15 = UNPACK_NEXT( tmp_unpack_6, 5, 11 );
    if ( tmp_assign_source_15 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        type_description_1 = "ooooooooooooooooooo";
        exception_lineno = 85;
        goto try_except_handler_6;
    }
    assert( tmp_tuple_unpack_1__element_6 == NULL );
    tmp_tuple_unpack_1__element_6 = tmp_assign_source_15;

    tmp_unpack_7 = tmp_tuple_unpack_1__source_iter;

    CHECK_OBJECT( tmp_unpack_7 );
    tmp_assign_source_16 = UNPACK_NEXT( tmp_unpack_7, 6, 11 );
    if ( tmp_assign_source_16 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        type_description_1 = "ooooooooooooooooooo";
        exception_lineno = 85;
        goto try_except_handler_6;
    }
    assert( tmp_tuple_unpack_1__element_7 == NULL );
    tmp_tuple_unpack_1__element_7 = tmp_assign_source_16;

    tmp_unpack_8 = tmp_tuple_unpack_1__source_iter;

    CHECK_OBJECT( tmp_unpack_8 );
    tmp_assign_source_17 = UNPACK_NEXT( tmp_unpack_8, 7, 11 );
    if ( tmp_assign_source_17 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        type_description_1 = "ooooooooooooooooooo";
        exception_lineno = 85;
        goto try_except_handler_6;
    }
    assert( tmp_tuple_unpack_1__element_8 == NULL );
    tmp_tuple_unpack_1__element_8 = tmp_assign_source_17;

    tmp_unpack_9 = tmp_tuple_unpack_1__source_iter;

    CHECK_OBJECT( tmp_unpack_9 );
    tmp_assign_source_18 = UNPACK_NEXT( tmp_unpack_9, 8, 11 );
    if ( tmp_assign_source_18 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        type_description_1 = "ooooooooooooooooooo";
        exception_lineno = 85;
        goto try_except_handler_6;
    }
    assert( tmp_tuple_unpack_1__element_9 == NULL );
    tmp_tuple_unpack_1__element_9 = tmp_assign_source_18;

    tmp_unpack_10 = tmp_tuple_unpack_1__source_iter;

    CHECK_OBJECT( tmp_unpack_10 );
    tmp_assign_source_19 = UNPACK_NEXT( tmp_unpack_10, 9, 11 );
    if ( tmp_assign_source_19 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        type_description_1 = "ooooooooooooooooooo";
        exception_lineno = 85;
        goto try_except_handler_6;
    }
    assert( tmp_tuple_unpack_1__element_10 == NULL );
    tmp_tuple_unpack_1__element_10 = tmp_assign_source_19;

    tmp_unpack_11 = tmp_tuple_unpack_1__source_iter;

    CHECK_OBJECT( tmp_unpack_11 );
    tmp_assign_source_20 = UNPACK_NEXT( tmp_unpack_11, 10, 11 );
    if ( tmp_assign_source_20 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        type_description_1 = "ooooooooooooooooooo";
        exception_lineno = 85;
        goto try_except_handler_6;
    }
    assert( tmp_tuple_unpack_1__element_11 == NULL );
    tmp_tuple_unpack_1__element_11 = tmp_assign_source_20;

    tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;

    CHECK_OBJECT( tmp_iterator_name_1 );
    // Check if iterator has left-over elements.
    CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

    tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

    if (likely( tmp_iterator_attempt == NULL ))
    {
        PyObject *error = GET_ERROR_OCCURRED();

        if ( error != NULL )
        {
            if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
            {
                CLEAR_ERROR_OCCURRED();
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                type_description_1 = "ooooooooooooooooooo";
                exception_lineno = 85;
                goto try_except_handler_6;
            }
        }
    }
    else
    {
        Py_DECREF( tmp_iterator_attempt );

        // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
        PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
        PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 11)" );
#endif
        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

        type_description_1 = "ooooooooooooooooooo";
        exception_lineno = 85;
        goto try_except_handler_6;
    }
    goto try_end_3;
    // Exception handler code:
    try_except_handler_6:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto try_except_handler_5;
    // End of try:
    try_end_3:;
    goto try_end_4;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_3 );
    tmp_tuple_unpack_1__element_3 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_4 );
    tmp_tuple_unpack_1__element_4 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_5 );
    tmp_tuple_unpack_1__element_5 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_6 );
    tmp_tuple_unpack_1__element_6 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_7 );
    tmp_tuple_unpack_1__element_7 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_8 );
    tmp_tuple_unpack_1__element_8 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_9 );
    tmp_tuple_unpack_1__element_9 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_10 );
    tmp_tuple_unpack_1__element_10 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_11 );
    tmp_tuple_unpack_1__element_11 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto frame_exception_exit_1;
    // End of try:
    try_end_4:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    tmp_assign_source_21 = tmp_tuple_unpack_1__element_1;

    CHECK_OBJECT( tmp_assign_source_21 );
    assert( var_bufx == NULL );
    Py_INCREF( tmp_assign_source_21 );
    var_bufx = tmp_assign_source_21;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    tmp_assign_source_22 = tmp_tuple_unpack_1__element_2;

    CHECK_OBJECT( tmp_assign_source_22 );
    assert( var_bufy == NULL );
    Py_INCREF( tmp_assign_source_22 );
    var_bufy = tmp_assign_source_22;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    tmp_assign_source_23 = tmp_tuple_unpack_1__element_3;

    CHECK_OBJECT( tmp_assign_source_23 );
    assert( var_curx == NULL );
    Py_INCREF( tmp_assign_source_23 );
    var_curx = tmp_assign_source_23;

    Py_XDECREF( tmp_tuple_unpack_1__element_3 );
    tmp_tuple_unpack_1__element_3 = NULL;

    tmp_assign_source_24 = tmp_tuple_unpack_1__element_4;

    CHECK_OBJECT( tmp_assign_source_24 );
    assert( var_cury == NULL );
    Py_INCREF( tmp_assign_source_24 );
    var_cury = tmp_assign_source_24;

    Py_XDECREF( tmp_tuple_unpack_1__element_4 );
    tmp_tuple_unpack_1__element_4 = NULL;

    tmp_assign_source_25 = tmp_tuple_unpack_1__element_5;

    CHECK_OBJECT( tmp_assign_source_25 );
    assert( var_wattr == NULL );
    Py_INCREF( tmp_assign_source_25 );
    var_wattr = tmp_assign_source_25;

    Py_XDECREF( tmp_tuple_unpack_1__element_5 );
    tmp_tuple_unpack_1__element_5 = NULL;

    tmp_assign_source_26 = tmp_tuple_unpack_1__element_6;

    CHECK_OBJECT( tmp_assign_source_26 );
    assert( var_left == NULL );
    Py_INCREF( tmp_assign_source_26 );
    var_left = tmp_assign_source_26;

    Py_XDECREF( tmp_tuple_unpack_1__element_6 );
    tmp_tuple_unpack_1__element_6 = NULL;

    tmp_assign_source_27 = tmp_tuple_unpack_1__element_7;

    CHECK_OBJECT( tmp_assign_source_27 );
    assert( var_top == NULL );
    Py_INCREF( tmp_assign_source_27 );
    var_top = tmp_assign_source_27;

    Py_XDECREF( tmp_tuple_unpack_1__element_7 );
    tmp_tuple_unpack_1__element_7 = NULL;

    tmp_assign_source_28 = tmp_tuple_unpack_1__element_8;

    CHECK_OBJECT( tmp_assign_source_28 );
    assert( var_right == NULL );
    Py_INCREF( tmp_assign_source_28 );
    var_right = tmp_assign_source_28;

    Py_XDECREF( tmp_tuple_unpack_1__element_8 );
    tmp_tuple_unpack_1__element_8 = NULL;

    tmp_assign_source_29 = tmp_tuple_unpack_1__element_9;

    CHECK_OBJECT( tmp_assign_source_29 );
    assert( var_bottom == NULL );
    Py_INCREF( tmp_assign_source_29 );
    var_bottom = tmp_assign_source_29;

    Py_XDECREF( tmp_tuple_unpack_1__element_9 );
    tmp_tuple_unpack_1__element_9 = NULL;

    tmp_assign_source_30 = tmp_tuple_unpack_1__element_10;

    CHECK_OBJECT( tmp_assign_source_30 );
    assert( var_maxx == NULL );
    Py_INCREF( tmp_assign_source_30 );
    var_maxx = tmp_assign_source_30;

    Py_XDECREF( tmp_tuple_unpack_1__element_10 );
    tmp_tuple_unpack_1__element_10 = NULL;

    tmp_assign_source_31 = tmp_tuple_unpack_1__element_11;

    CHECK_OBJECT( tmp_assign_source_31 );
    assert( var_maxy == NULL );
    Py_INCREF( tmp_assign_source_31 );
    var_maxy = tmp_assign_source_31;

    Py_XDECREF( tmp_tuple_unpack_1__element_11 );
    tmp_tuple_unpack_1__element_11 = NULL;

    tmp_left_name_2 = var_right;

    if ( tmp_left_name_2 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "right" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 87;
        type_description_1 = "ooooooooooooooooooo";
        goto frame_exception_exit_1;
    }

    tmp_right_name_1 = var_left;

    if ( tmp_right_name_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "left" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 87;
        type_description_1 = "ooooooooooooooooooo";
        goto frame_exception_exit_1;
    }

    tmp_left_name_1 = BINARY_OPERATION_SUB( tmp_left_name_2, tmp_right_name_1 );
    if ( tmp_left_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 87;
        type_description_1 = "ooooooooooooooooooo";
        goto frame_exception_exit_1;
    }
    tmp_right_name_2 = const_int_pos_1;
    tmp_assign_source_32 = BINARY_OPERATION_ADD( tmp_left_name_1, tmp_right_name_2 );
    Py_DECREF( tmp_left_name_1 );
    if ( tmp_assign_source_32 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 87;
        type_description_1 = "ooooooooooooooooooo";
        goto frame_exception_exit_1;
    }
    assert( var_sizex == NULL );
    var_sizex = tmp_assign_source_32;

    tmp_left_name_4 = var_bottom;

    if ( tmp_left_name_4 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "bottom" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 88;
        type_description_1 = "ooooooooooooooooooo";
        goto frame_exception_exit_1;
    }

    tmp_right_name_3 = var_top;

    if ( tmp_right_name_3 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "top" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 88;
        type_description_1 = "ooooooooooooooooooo";
        goto frame_exception_exit_1;
    }

    tmp_left_name_3 = BINARY_OPERATION_SUB( tmp_left_name_4, tmp_right_name_3 );
    if ( tmp_left_name_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 88;
        type_description_1 = "ooooooooooooooooooo";
        goto frame_exception_exit_1;
    }
    tmp_right_name_4 = const_int_pos_1;
    tmp_assign_source_33 = BINARY_OPERATION_ADD( tmp_left_name_3, tmp_right_name_4 );
    Py_DECREF( tmp_left_name_3 );
    if ( tmp_assign_source_33 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 88;
        type_description_1 = "ooooooooooooooooooo";
        goto frame_exception_exit_1;
    }
    assert( var_sizey == NULL );
    var_sizey = tmp_assign_source_33;

    tmp_tuple_element_1 = var_sizex;

    if ( tmp_tuple_element_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "sizex" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 89;
        type_description_1 = "ooooooooooooooooooo";
        goto frame_exception_exit_1;
    }

    tmp_return_value = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_return_value, 0, tmp_tuple_element_1 );
    tmp_tuple_element_1 = var_sizey;

    CHECK_OBJECT( tmp_tuple_element_1 );
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_return_value, 1, tmp_tuple_element_1 );
    goto frame_return_exit_1;
    goto branch_end_1;
    branch_no_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto frame_return_exit_1;
    branch_end_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_dc337648f1aa927fe0e89d0f7d98fae9 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_dc337648f1aa927fe0e89d0f7d98fae9 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_dc337648f1aa927fe0e89d0f7d98fae9 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_dc337648f1aa927fe0e89d0f7d98fae9, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_dc337648f1aa927fe0e89d0f7d98fae9->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_dc337648f1aa927fe0e89d0f7d98fae9, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_dc337648f1aa927fe0e89d0f7d98fae9,
        type_description_1,
        var_res,
        var_windll,
        var_create_string_buffer,
        var_h,
        var_csbi,
        var_struct,
        var_bufx,
        var_bufy,
        var_curx,
        var_cury,
        var_wattr,
        var_left,
        var_top,
        var_right,
        var_bottom,
        var_maxx,
        var_maxy,
        var_sizex,
        var_sizey
    );


    // Release cached frame.
    if ( frame_dc337648f1aa927fe0e89d0f7d98fae9 == cache_frame_dc337648f1aa927fe0e89d0f7d98fae9 )
    {
        Py_DECREF( frame_dc337648f1aa927fe0e89d0f7d98fae9 );
    }
    cache_frame_dc337648f1aa927fe0e89d0f7d98fae9 = NULL;

    assertFrameObject( frame_dc337648f1aa927fe0e89d0f7d98fae9 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$io$formats$terminal$$$function_3__get_terminal_size_windows );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( var_res );
    var_res = NULL;

    Py_XDECREF( var_windll );
    var_windll = NULL;

    Py_XDECREF( var_create_string_buffer );
    var_create_string_buffer = NULL;

    Py_XDECREF( var_h );
    var_h = NULL;

    Py_XDECREF( var_csbi );
    var_csbi = NULL;

    Py_XDECREF( var_struct );
    var_struct = NULL;

    Py_XDECREF( var_bufx );
    var_bufx = NULL;

    Py_XDECREF( var_bufy );
    var_bufy = NULL;

    Py_XDECREF( var_curx );
    var_curx = NULL;

    Py_XDECREF( var_cury );
    var_cury = NULL;

    Py_XDECREF( var_wattr );
    var_wattr = NULL;

    Py_XDECREF( var_left );
    var_left = NULL;

    Py_XDECREF( var_top );
    var_top = NULL;

    Py_XDECREF( var_right );
    var_right = NULL;

    Py_XDECREF( var_bottom );
    var_bottom = NULL;

    Py_XDECREF( var_maxx );
    var_maxx = NULL;

    Py_XDECREF( var_maxy );
    var_maxy = NULL;

    Py_XDECREF( var_sizex );
    var_sizex = NULL;

    Py_XDECREF( var_sizey );
    var_sizey = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( var_res );
    var_res = NULL;

    Py_XDECREF( var_windll );
    var_windll = NULL;

    Py_XDECREF( var_create_string_buffer );
    var_create_string_buffer = NULL;

    Py_XDECREF( var_h );
    var_h = NULL;

    Py_XDECREF( var_csbi );
    var_csbi = NULL;

    Py_XDECREF( var_struct );
    var_struct = NULL;

    Py_XDECREF( var_bufx );
    var_bufx = NULL;

    Py_XDECREF( var_bufy );
    var_bufy = NULL;

    Py_XDECREF( var_curx );
    var_curx = NULL;

    Py_XDECREF( var_cury );
    var_cury = NULL;

    Py_XDECREF( var_wattr );
    var_wattr = NULL;

    Py_XDECREF( var_left );
    var_left = NULL;

    Py_XDECREF( var_top );
    var_top = NULL;

    Py_XDECREF( var_right );
    var_right = NULL;

    Py_XDECREF( var_bottom );
    var_bottom = NULL;

    Py_XDECREF( var_maxx );
    var_maxx = NULL;

    Py_XDECREF( var_maxy );
    var_maxy = NULL;

    Py_XDECREF( var_sizex );
    var_sizex = NULL;

    Py_XDECREF( var_sizey );
    var_sizey = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$io$formats$terminal$$$function_3__get_terminal_size_windows );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$io$formats$terminal$$$function_4__get_terminal_size_tput( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *var_subprocess = NULL;
    PyObject *var_proc = NULL;
    PyObject *var_output = NULL;
    PyObject *var_cols = NULL;
    PyObject *var_rows = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    PyObject *tmp_args_name_1;
    PyObject *tmp_args_name_2;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_assign_source_5;
    PyObject *tmp_assign_source_6;
    PyObject *tmp_assign_source_7;
    PyObject *tmp_called_name_1;
    PyObject *tmp_called_name_2;
    PyObject *tmp_called_name_3;
    PyObject *tmp_called_name_4;
    PyObject *tmp_dict_key_1;
    PyObject *tmp_dict_key_2;
    PyObject *tmp_dict_key_3;
    PyObject *tmp_dict_key_4;
    PyObject *tmp_dict_value_1;
    PyObject *tmp_dict_value_2;
    PyObject *tmp_dict_value_3;
    PyObject *tmp_dict_value_4;
    PyObject *tmp_fromlist_name_1;
    PyObject *tmp_globals_name_1;
    PyObject *tmp_int_arg_1;
    PyObject *tmp_int_arg_2;
    PyObject *tmp_kw_name_1;
    PyObject *tmp_kw_name_2;
    PyObject *tmp_kw_name_3;
    PyObject *tmp_kw_name_4;
    PyObject *tmp_level_name_1;
    PyObject *tmp_locals_name_1;
    PyObject *tmp_name_name_1;
    int tmp_res;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    PyObject *tmp_source_name_2;
    PyObject *tmp_source_name_3;
    PyObject *tmp_source_name_4;
    PyObject *tmp_source_name_5;
    PyObject *tmp_source_name_6;
    PyObject *tmp_source_name_7;
    PyObject *tmp_source_name_8;
    PyObject *tmp_subscribed_name_1;
    PyObject *tmp_subscribed_name_2;
    PyObject *tmp_subscript_name_1;
    PyObject *tmp_subscript_name_2;
    PyObject *tmp_tuple_element_1;
    static struct Nuitka_FrameObject *cache_frame_8dbd5bcfd34403cd2b3caed96bddda71 = NULL;

    struct Nuitka_FrameObject *frame_8dbd5bcfd34403cd2b3caed96bddda71;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_8dbd5bcfd34403cd2b3caed96bddda71, codeobj_8dbd5bcfd34403cd2b3caed96bddda71, module_pandas$io$formats$terminal, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_8dbd5bcfd34403cd2b3caed96bddda71 = cache_frame_8dbd5bcfd34403cd2b3caed96bddda71;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_8dbd5bcfd34403cd2b3caed96bddda71 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_8dbd5bcfd34403cd2b3caed96bddda71 ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    tmp_name_name_1 = const_str_plain_subprocess;
    tmp_globals_name_1 = (PyObject *)moduledict_pandas$io$formats$terminal;
    tmp_locals_name_1 = Py_None;
    tmp_fromlist_name_1 = Py_None;
    tmp_level_name_1 = const_int_0;
    frame_8dbd5bcfd34403cd2b3caed96bddda71->m_frame.f_lineno = 99;
    tmp_assign_source_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
    if ( tmp_assign_source_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 99;
        type_description_1 = "ooooo";
        goto try_except_handler_2;
    }
    assert( var_subprocess == NULL );
    var_subprocess = tmp_assign_source_1;

    tmp_source_name_1 = var_subprocess;

    CHECK_OBJECT( tmp_source_name_1 );
    tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_Popen );
    if ( tmp_called_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 100;
        type_description_1 = "ooooo";
        goto try_except_handler_2;
    }
    tmp_args_name_1 = DEEP_COPY( const_tuple_list_str_plain_tput_str_plain_cols_list_tuple );
    tmp_dict_key_1 = const_str_plain_stdin;
    tmp_source_name_2 = var_subprocess;

    if ( tmp_source_name_2 == NULL )
    {
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_name_1 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "subprocess" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 101;
        type_description_1 = "ooooo";
        goto try_except_handler_2;
    }

    tmp_dict_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_PIPE );
    if ( tmp_dict_value_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_name_1 );

        exception_lineno = 101;
        type_description_1 = "ooooo";
        goto try_except_handler_2;
    }
    tmp_kw_name_1 = _PyDict_NewPresized( 2 );
    tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
    Py_DECREF( tmp_dict_value_1 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_2 = const_str_plain_stdout;
    tmp_source_name_3 = var_subprocess;

    if ( tmp_source_name_3 == NULL )
    {
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "subprocess" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 102;
        type_description_1 = "ooooo";
        goto try_except_handler_2;
    }

    tmp_dict_value_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_PIPE );
    if ( tmp_dict_value_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );

        exception_lineno = 102;
        type_description_1 = "ooooo";
        goto try_except_handler_2;
    }
    tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
    Py_DECREF( tmp_dict_value_2 );
    assert( !(tmp_res != 0) );
    frame_8dbd5bcfd34403cd2b3caed96bddda71->m_frame.f_lineno = 100;
    tmp_assign_source_2 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
    Py_DECREF( tmp_called_name_1 );
    Py_DECREF( tmp_args_name_1 );
    Py_DECREF( tmp_kw_name_1 );
    if ( tmp_assign_source_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 100;
        type_description_1 = "ooooo";
        goto try_except_handler_2;
    }
    assert( var_proc == NULL );
    var_proc = tmp_assign_source_2;

    tmp_source_name_4 = var_proc;

    CHECK_OBJECT( tmp_source_name_4 );
    tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_communicate );
    if ( tmp_called_name_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 103;
        type_description_1 = "ooooo";
        goto try_except_handler_2;
    }
    tmp_kw_name_2 = PyDict_Copy( const_dict_c453dda1d3901e4cb85d5781f8962504 );
    frame_8dbd5bcfd34403cd2b3caed96bddda71->m_frame.f_lineno = 103;
    tmp_assign_source_3 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_2, tmp_kw_name_2 );
    Py_DECREF( tmp_called_name_2 );
    Py_DECREF( tmp_kw_name_2 );
    if ( tmp_assign_source_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 103;
        type_description_1 = "ooooo";
        goto try_except_handler_2;
    }
    assert( var_output == NULL );
    var_output = tmp_assign_source_3;

    tmp_subscribed_name_1 = var_output;

    CHECK_OBJECT( tmp_subscribed_name_1 );
    tmp_subscript_name_1 = const_int_0;
    tmp_int_arg_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
    if ( tmp_int_arg_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 104;
        type_description_1 = "ooooo";
        goto try_except_handler_2;
    }
    tmp_assign_source_4 = PyNumber_Int( tmp_int_arg_1 );
    Py_DECREF( tmp_int_arg_1 );
    if ( tmp_assign_source_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 104;
        type_description_1 = "ooooo";
        goto try_except_handler_2;
    }
    assert( var_cols == NULL );
    var_cols = tmp_assign_source_4;

    tmp_source_name_5 = var_subprocess;

    if ( tmp_source_name_5 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "subprocess" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 105;
        type_description_1 = "ooooo";
        goto try_except_handler_2;
    }

    tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_Popen );
    if ( tmp_called_name_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 105;
        type_description_1 = "ooooo";
        goto try_except_handler_2;
    }
    tmp_args_name_2 = DEEP_COPY( const_tuple_list_str_plain_tput_str_plain_lines_list_tuple );
    tmp_dict_key_3 = const_str_plain_stdin;
    tmp_source_name_6 = var_subprocess;

    if ( tmp_source_name_6 == NULL )
    {
        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_name_2 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "subprocess" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 106;
        type_description_1 = "ooooo";
        goto try_except_handler_2;
    }

    tmp_dict_value_3 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_PIPE );
    if ( tmp_dict_value_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_name_2 );

        exception_lineno = 106;
        type_description_1 = "ooooo";
        goto try_except_handler_2;
    }
    tmp_kw_name_3 = _PyDict_NewPresized( 2 );
    tmp_res = PyDict_SetItem( tmp_kw_name_3, tmp_dict_key_3, tmp_dict_value_3 );
    Py_DECREF( tmp_dict_value_3 );
    assert( !(tmp_res != 0) );
    tmp_dict_key_4 = const_str_plain_stdout;
    tmp_source_name_7 = var_subprocess;

    if ( tmp_source_name_7 == NULL )
    {
        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_name_2 );
        Py_DECREF( tmp_kw_name_3 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "subprocess" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 107;
        type_description_1 = "ooooo";
        goto try_except_handler_2;
    }

    tmp_dict_value_4 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_PIPE );
    if ( tmp_dict_value_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_name_2 );
        Py_DECREF( tmp_kw_name_3 );

        exception_lineno = 107;
        type_description_1 = "ooooo";
        goto try_except_handler_2;
    }
    tmp_res = PyDict_SetItem( tmp_kw_name_3, tmp_dict_key_4, tmp_dict_value_4 );
    Py_DECREF( tmp_dict_value_4 );
    assert( !(tmp_res != 0) );
    frame_8dbd5bcfd34403cd2b3caed96bddda71->m_frame.f_lineno = 105;
    tmp_assign_source_5 = CALL_FUNCTION( tmp_called_name_3, tmp_args_name_2, tmp_kw_name_3 );
    Py_DECREF( tmp_called_name_3 );
    Py_DECREF( tmp_args_name_2 );
    Py_DECREF( tmp_kw_name_3 );
    if ( tmp_assign_source_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 105;
        type_description_1 = "ooooo";
        goto try_except_handler_2;
    }
    {
        PyObject *old = var_proc;
        var_proc = tmp_assign_source_5;
        Py_XDECREF( old );
    }

    tmp_source_name_8 = var_proc;

    CHECK_OBJECT( tmp_source_name_8 );
    tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_communicate );
    if ( tmp_called_name_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 108;
        type_description_1 = "ooooo";
        goto try_except_handler_2;
    }
    tmp_kw_name_4 = PyDict_Copy( const_dict_c453dda1d3901e4cb85d5781f8962504 );
    frame_8dbd5bcfd34403cd2b3caed96bddda71->m_frame.f_lineno = 108;
    tmp_assign_source_6 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_4, tmp_kw_name_4 );
    Py_DECREF( tmp_called_name_4 );
    Py_DECREF( tmp_kw_name_4 );
    if ( tmp_assign_source_6 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 108;
        type_description_1 = "ooooo";
        goto try_except_handler_2;
    }
    {
        PyObject *old = var_output;
        var_output = tmp_assign_source_6;
        Py_XDECREF( old );
    }

    tmp_subscribed_name_2 = var_output;

    CHECK_OBJECT( tmp_subscribed_name_2 );
    tmp_subscript_name_2 = const_int_0;
    tmp_int_arg_2 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
    if ( tmp_int_arg_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 109;
        type_description_1 = "ooooo";
        goto try_except_handler_2;
    }
    tmp_assign_source_7 = PyNumber_Int( tmp_int_arg_2 );
    Py_DECREF( tmp_int_arg_2 );
    if ( tmp_assign_source_7 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 109;
        type_description_1 = "ooooo";
        goto try_except_handler_2;
    }
    assert( var_rows == NULL );
    var_rows = tmp_assign_source_7;

    tmp_tuple_element_1 = var_cols;

    if ( tmp_tuple_element_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "cols" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 110;
        type_description_1 = "ooooo";
        goto try_except_handler_2;
    }

    tmp_return_value = PyTuple_New( 2 );
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_return_value, 0, tmp_tuple_element_1 );
    tmp_tuple_element_1 = var_rows;

    CHECK_OBJECT( tmp_tuple_element_1 );
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_return_value, 1, tmp_tuple_element_1 );
    goto frame_return_exit_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$io$formats$terminal$$$function_4__get_terminal_size_tput );
    return NULL;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_8dbd5bcfd34403cd2b3caed96bddda71, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_8dbd5bcfd34403cd2b3caed96bddda71, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_3;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$io$formats$terminal$$$function_4__get_terminal_size_tput );
    return NULL;
    // Return handler code:
    try_return_handler_3:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto frame_return_exit_1;
    // End of try:
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto try_end_1;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$io$formats$terminal$$$function_4__get_terminal_size_tput );
    return NULL;
    // End of try:
    try_end_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8dbd5bcfd34403cd2b3caed96bddda71 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_8dbd5bcfd34403cd2b3caed96bddda71 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_no_exception_1:;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$io$formats$terminal$$$function_4__get_terminal_size_tput );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( var_subprocess );
    var_subprocess = NULL;

    Py_XDECREF( var_proc );
    var_proc = NULL;

    Py_XDECREF( var_output );
    var_output = NULL;

    Py_XDECREF( var_cols );
    var_cols = NULL;

    Py_XDECREF( var_rows );
    var_rows = NULL;

    goto function_return_exit;
    // End of try:
    Py_XDECREF( var_subprocess );
    var_subprocess = NULL;

    Py_XDECREF( var_proc );
    var_proc = NULL;

    Py_XDECREF( var_output );
    var_output = NULL;

    Py_XDECREF( var_cols );
    var_cols = NULL;

    Py_XDECREF( var_rows );
    var_rows = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$io$formats$terminal$$$function_4__get_terminal_size_tput );
    return NULL;

function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$io$formats$terminal$$$function_5__get_terminal_size_linux( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *var_ioctl_GWINSZ = NULL;
    PyObject *var_cr = NULL;
    PyObject *var_fd = NULL;
    PyObject *var_env = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    PyObject *tmp_args_element_name_1;
    PyObject *tmp_args_element_name_2;
    PyObject *tmp_args_element_name_3;
    PyObject *tmp_args_element_name_4;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_assign_source_5;
    PyObject *tmp_assign_source_6;
    PyObject *tmp_called_instance_1;
    PyObject *tmp_called_name_1;
    PyObject *tmp_called_name_2;
    PyObject *tmp_called_name_3;
    PyObject *tmp_called_name_4;
    PyObject *tmp_called_name_5;
    PyObject *tmp_called_name_6;
    PyObject *tmp_compexpr_left_1;
    PyObject *tmp_compexpr_right_1;
    int tmp_cond_truth_1;
    int tmp_cond_truth_2;
    PyObject *tmp_cond_value_1;
    PyObject *tmp_cond_value_2;
    PyObject *tmp_fromlist_name_1;
    PyObject *tmp_globals_name_1;
    PyObject *tmp_import_name_from_1;
    PyObject *tmp_int_arg_1;
    PyObject *tmp_int_arg_2;
    PyObject *tmp_level_name_1;
    PyObject *tmp_locals_name_1;
    PyObject *tmp_name_name_1;
    PyObject *tmp_operand_name_1;
    int tmp_or_left_truth_1;
    int tmp_or_left_truth_2;
    int tmp_or_left_truth_3;
    PyObject *tmp_or_left_value_1;
    PyObject *tmp_or_left_value_2;
    PyObject *tmp_or_left_value_3;
    PyObject *tmp_or_right_value_1;
    PyObject *tmp_or_right_value_2;
    PyObject *tmp_or_right_value_3;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    PyObject *tmp_source_name_2;
    PyObject *tmp_source_name_3;
    PyObject *tmp_subscribed_name_1;
    PyObject *tmp_subscribed_name_2;
    PyObject *tmp_subscribed_name_3;
    PyObject *tmp_subscribed_name_4;
    PyObject *tmp_subscript_name_1;
    PyObject *tmp_subscript_name_2;
    PyObject *tmp_subscript_name_3;
    PyObject *tmp_subscript_name_4;
    PyObject *tmp_tuple_element_1;
    PyObject *tmp_tuple_element_2;
    NUITKA_MAY_BE_UNUSED PyObject *tmp_unused;
    static struct Nuitka_FrameObject *cache_frame_258cdca6404197d30b1b941d09cf2149 = NULL;

    struct Nuitka_FrameObject *frame_258cdca6404197d30b1b941d09cf2149;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    tmp_assign_source_1 = MAKE_FUNCTION_pandas$io$formats$terminal$$$function_5__get_terminal_size_linux$$$function_1_ioctl_GWINSZ(  );
    assert( var_ioctl_GWINSZ == NULL );
    var_ioctl_GWINSZ = tmp_assign_source_1;

    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_258cdca6404197d30b1b941d09cf2149, codeobj_258cdca6404197d30b1b941d09cf2149, module_pandas$io$formats$terminal, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_258cdca6404197d30b1b941d09cf2149 = cache_frame_258cdca6404197d30b1b941d09cf2149;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_258cdca6404197d30b1b941d09cf2149 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_258cdca6404197d30b1b941d09cf2149 ) == 2 ); // Frame stack

    // Framed code:
    tmp_called_name_1 = var_ioctl_GWINSZ;

    CHECK_OBJECT( tmp_called_name_1 );
    frame_258cdca6404197d30b1b941d09cf2149->m_frame.f_lineno = 126;
    tmp_or_left_value_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, &PyTuple_GET_ITEM( const_tuple_int_0_tuple, 0 ) );

    if ( tmp_or_left_value_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 126;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    tmp_or_left_truth_1 = CHECK_IF_TRUE( tmp_or_left_value_1 );
    if ( tmp_or_left_truth_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_or_left_value_1 );

        exception_lineno = 126;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    if ( tmp_or_left_truth_1 == 1 )
    {
        goto or_left_1;
    }
    else
    {
        goto or_right_1;
    }
    or_right_1:;
    Py_DECREF( tmp_or_left_value_1 );
    tmp_called_name_2 = var_ioctl_GWINSZ;

    if ( tmp_called_name_2 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "ioctl_GWINSZ" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 126;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }

    frame_258cdca6404197d30b1b941d09cf2149->m_frame.f_lineno = 126;
    tmp_or_left_value_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, &PyTuple_GET_ITEM( const_tuple_int_pos_1_tuple, 0 ) );

    if ( tmp_or_left_value_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 126;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    tmp_or_left_truth_2 = CHECK_IF_TRUE( tmp_or_left_value_2 );
    if ( tmp_or_left_truth_2 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_or_left_value_2 );

        exception_lineno = 126;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    if ( tmp_or_left_truth_2 == 1 )
    {
        goto or_left_2;
    }
    else
    {
        goto or_right_2;
    }
    or_right_2:;
    Py_DECREF( tmp_or_left_value_2 );
    tmp_called_name_3 = var_ioctl_GWINSZ;

    if ( tmp_called_name_3 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "ioctl_GWINSZ" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 126;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }

    frame_258cdca6404197d30b1b941d09cf2149->m_frame.f_lineno = 126;
    tmp_or_right_value_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, &PyTuple_GET_ITEM( const_tuple_int_pos_2_tuple, 0 ) );

    if ( tmp_or_right_value_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 126;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    tmp_or_right_value_1 = tmp_or_right_value_2;
    goto or_end_2;
    or_left_2:;
    tmp_or_right_value_1 = tmp_or_left_value_2;
    or_end_2:;
    tmp_assign_source_2 = tmp_or_right_value_1;
    goto or_end_1;
    or_left_1:;
    tmp_assign_source_2 = tmp_or_left_value_1;
    or_end_1:;
    assert( var_cr == NULL );
    var_cr = tmp_assign_source_2;

    tmp_cond_value_1 = var_cr;

    CHECK_OBJECT( tmp_cond_value_1 );
    tmp_cond_truth_1 = CHECK_IF_TRUE( tmp_cond_value_1 );
    if ( tmp_cond_truth_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 127;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    if ( tmp_cond_truth_1 == 1 )
    {
        goto branch_no_1;
    }
    else
    {
        goto branch_yes_1;
    }
    branch_yes_1:;
    // Tried code:
    tmp_source_name_1 = GET_STRING_DICT_VALUE( moduledict_pandas$io$formats$terminal, (Nuitka_StringObject *)const_str_plain_os );

    if (unlikely( tmp_source_name_1 == NULL ))
    {
        tmp_source_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
    }

    if ( tmp_source_name_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 129;
        type_description_1 = "oooo";
        goto try_except_handler_2;
    }

    tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_open );
    if ( tmp_called_name_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 129;
        type_description_1 = "oooo";
        goto try_except_handler_2;
    }
    tmp_called_instance_1 = GET_STRING_DICT_VALUE( moduledict_pandas$io$formats$terminal, (Nuitka_StringObject *)const_str_plain_os );

    if (unlikely( tmp_called_instance_1 == NULL ))
    {
        tmp_called_instance_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
    }

    if ( tmp_called_instance_1 == NULL )
    {
        Py_DECREF( tmp_called_name_4 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 129;
        type_description_1 = "oooo";
        goto try_except_handler_2;
    }

    frame_258cdca6404197d30b1b941d09cf2149->m_frame.f_lineno = 129;
    tmp_args_element_name_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_ctermid );
    if ( tmp_args_element_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_4 );

        exception_lineno = 129;
        type_description_1 = "oooo";
        goto try_except_handler_2;
    }
    tmp_source_name_2 = GET_STRING_DICT_VALUE( moduledict_pandas$io$formats$terminal, (Nuitka_StringObject *)const_str_plain_os );

    if (unlikely( tmp_source_name_2 == NULL ))
    {
        tmp_source_name_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
    }

    if ( tmp_source_name_2 == NULL )
    {
        Py_DECREF( tmp_called_name_4 );
        Py_DECREF( tmp_args_element_name_1 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 129;
        type_description_1 = "oooo";
        goto try_except_handler_2;
    }

    tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_O_RDONLY );
    if ( tmp_args_element_name_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_4 );
        Py_DECREF( tmp_args_element_name_1 );

        exception_lineno = 129;
        type_description_1 = "oooo";
        goto try_except_handler_2;
    }
    frame_258cdca6404197d30b1b941d09cf2149->m_frame.f_lineno = 129;
    {
        PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
        tmp_assign_source_3 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_4, call_args );
    }

    Py_DECREF( tmp_called_name_4 );
    Py_DECREF( tmp_args_element_name_1 );
    Py_DECREF( tmp_args_element_name_2 );
    if ( tmp_assign_source_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 129;
        type_description_1 = "oooo";
        goto try_except_handler_2;
    }
    assert( var_fd == NULL );
    var_fd = tmp_assign_source_3;

    tmp_called_name_5 = var_ioctl_GWINSZ;

    if ( tmp_called_name_5 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "ioctl_GWINSZ" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 130;
        type_description_1 = "oooo";
        goto try_except_handler_2;
    }

    tmp_args_element_name_3 = var_fd;

    CHECK_OBJECT( tmp_args_element_name_3 );
    frame_258cdca6404197d30b1b941d09cf2149->m_frame.f_lineno = 130;
    {
        PyObject *call_args[] = { tmp_args_element_name_3 };
        tmp_assign_source_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, call_args );
    }

    if ( tmp_assign_source_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 130;
        type_description_1 = "oooo";
        goto try_except_handler_2;
    }
    {
        PyObject *old = var_cr;
        var_cr = tmp_assign_source_4;
        Py_XDECREF( old );
    }

    tmp_source_name_3 = GET_STRING_DICT_VALUE( moduledict_pandas$io$formats$terminal, (Nuitka_StringObject *)const_str_plain_os );

    if (unlikely( tmp_source_name_3 == NULL ))
    {
        tmp_source_name_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
    }

    if ( tmp_source_name_3 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 131;
        type_description_1 = "oooo";
        goto try_except_handler_2;
    }

    tmp_called_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_close );
    if ( tmp_called_name_6 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 131;
        type_description_1 = "oooo";
        goto try_except_handler_2;
    }
    tmp_args_element_name_4 = var_fd;

    if ( tmp_args_element_name_4 == NULL )
    {
        Py_DECREF( tmp_called_name_6 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "fd" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 131;
        type_description_1 = "oooo";
        goto try_except_handler_2;
    }

    frame_258cdca6404197d30b1b941d09cf2149->m_frame.f_lineno = 131;
    {
        PyObject *call_args[] = { tmp_args_element_name_4 };
        tmp_unused = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, call_args );
    }

    Py_DECREF( tmp_called_name_6 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 131;
        type_description_1 = "oooo";
        goto try_except_handler_2;
    }
    Py_DECREF( tmp_unused );
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_DECREF( exception_keeper_type_1 );
    Py_XDECREF( exception_keeper_value_1 );
    Py_XDECREF( exception_keeper_tb_1 );
    goto try_end_1;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$io$formats$terminal$$$function_5__get_terminal_size_linux );
    return NULL;
    // End of try:
    try_end_1:;
    branch_no_1:;
    tmp_operand_name_1 = var_cr;

    if ( tmp_operand_name_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "cr" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 134;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }

    tmp_or_left_value_3 = UNARY_OPERATION( UNARY_NOT, tmp_operand_name_1 );
    if ( tmp_or_left_value_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 134;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    tmp_or_left_truth_3 = CHECK_IF_TRUE( tmp_or_left_value_3 );
    if ( tmp_or_left_truth_3 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 134;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    if ( tmp_or_left_truth_3 == 1 )
    {
        goto or_left_3;
    }
    else
    {
        goto or_right_3;
    }
    or_right_3:;
    tmp_compexpr_left_1 = var_cr;

    if ( tmp_compexpr_left_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "cr" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 134;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }

    tmp_compexpr_right_1 = const_tuple_int_0_int_0_tuple;
    tmp_or_right_value_3 = RICH_COMPARE_EQ( tmp_compexpr_left_1, tmp_compexpr_right_1 );
    if ( tmp_or_right_value_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 134;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    tmp_cond_value_2 = tmp_or_right_value_3;
    goto or_end_3;
    or_left_3:;
    Py_INCREF( tmp_or_left_value_3 );
    tmp_cond_value_2 = tmp_or_left_value_3;
    or_end_3:;
    tmp_cond_truth_2 = CHECK_IF_TRUE( tmp_cond_value_2 );
    if ( tmp_cond_truth_2 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_cond_value_2 );

        exception_lineno = 134;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_cond_value_2 );
    if ( tmp_cond_truth_2 == 1 )
    {
        goto branch_yes_2;
    }
    else
    {
        goto branch_no_2;
    }
    branch_yes_2:;
    // Tried code:
    tmp_name_name_1 = const_str_plain_os;
    tmp_globals_name_1 = (PyObject *)moduledict_pandas$io$formats$terminal;
    tmp_locals_name_1 = Py_None;
    tmp_fromlist_name_1 = const_tuple_str_plain_environ_tuple;
    tmp_level_name_1 = const_int_0;
    frame_258cdca6404197d30b1b941d09cf2149->m_frame.f_lineno = 136;
    tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
    if ( tmp_import_name_from_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 136;
        type_description_1 = "oooo";
        goto try_except_handler_3;
    }
    tmp_assign_source_5 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_environ );
    Py_DECREF( tmp_import_name_from_1 );
    if ( tmp_assign_source_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 136;
        type_description_1 = "oooo";
        goto try_except_handler_3;
    }
    assert( var_env == NULL );
    var_env = tmp_assign_source_5;

    tmp_subscribed_name_1 = var_env;

    CHECK_OBJECT( tmp_subscribed_name_1 );
    tmp_subscript_name_1 = const_str_plain_LINES;
    tmp_tuple_element_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
    if ( tmp_tuple_element_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 137;
        type_description_1 = "oooo";
        goto try_except_handler_3;
    }
    tmp_assign_source_6 = PyTuple_New( 2 );
    PyTuple_SET_ITEM( tmp_assign_source_6, 0, tmp_tuple_element_1 );
    tmp_subscribed_name_2 = var_env;

    if ( tmp_subscribed_name_2 == NULL )
    {
        Py_DECREF( tmp_assign_source_6 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "env" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 137;
        type_description_1 = "oooo";
        goto try_except_handler_3;
    }

    tmp_subscript_name_2 = const_str_plain_COLUMNS;
    tmp_tuple_element_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
    if ( tmp_tuple_element_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assign_source_6 );

        exception_lineno = 137;
        type_description_1 = "oooo";
        goto try_except_handler_3;
    }
    PyTuple_SET_ITEM( tmp_assign_source_6, 1, tmp_tuple_element_1 );
    {
        PyObject *old = var_cr;
        var_cr = tmp_assign_source_6;
        Py_XDECREF( old );
    }

    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_2 == NULL )
    {
        exception_keeper_tb_2 = MAKE_TRACEBACK( frame_258cdca6404197d30b1b941d09cf2149, exception_keeper_lineno_2 );
    }
    else if ( exception_keeper_lineno_2 != 0 )
    {
        exception_keeper_tb_2 = ADD_TRACEBACK( exception_keeper_tb_2, frame_258cdca6404197d30b1b941d09cf2149, exception_keeper_lineno_2 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_2, &exception_keeper_value_2, &exception_keeper_tb_2 );
    PyException_SetTraceback( exception_keeper_value_2, (PyObject *)exception_keeper_tb_2 );
    PUBLISH_EXCEPTION( &exception_keeper_type_2, &exception_keeper_value_2, &exception_keeper_tb_2 );
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_4;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$io$formats$terminal$$$function_5__get_terminal_size_linux );
    return NULL;
    // Return handler code:
    try_return_handler_4:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto frame_return_exit_1;
    // End of try:
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto try_end_2;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$io$formats$terminal$$$function_5__get_terminal_size_linux );
    return NULL;
    // End of try:
    try_end_2:;
    branch_no_2:;
    tmp_subscribed_name_3 = var_cr;

    if ( tmp_subscribed_name_3 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "cr" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 140;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }

    tmp_subscript_name_3 = const_int_pos_1;
    tmp_int_arg_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_3, tmp_subscript_name_3 );
    if ( tmp_int_arg_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 140;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    tmp_tuple_element_2 = PyNumber_Int( tmp_int_arg_1 );
    Py_DECREF( tmp_int_arg_1 );
    if ( tmp_tuple_element_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 140;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    tmp_return_value = PyTuple_New( 2 );
    PyTuple_SET_ITEM( tmp_return_value, 0, tmp_tuple_element_2 );
    tmp_subscribed_name_4 = var_cr;

    if ( tmp_subscribed_name_4 == NULL )
    {
        Py_DECREF( tmp_return_value );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "cr" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 140;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }

    tmp_subscript_name_4 = const_int_0;
    tmp_int_arg_2 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_4, tmp_subscript_name_4 );
    if ( tmp_int_arg_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_return_value );

        exception_lineno = 140;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    tmp_tuple_element_2 = PyNumber_Int( tmp_int_arg_2 );
    Py_DECREF( tmp_int_arg_2 );
    if ( tmp_tuple_element_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_return_value );

        exception_lineno = 140;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }
    PyTuple_SET_ITEM( tmp_return_value, 1, tmp_tuple_element_2 );
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_258cdca6404197d30b1b941d09cf2149 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_258cdca6404197d30b1b941d09cf2149 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_258cdca6404197d30b1b941d09cf2149 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_258cdca6404197d30b1b941d09cf2149, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_258cdca6404197d30b1b941d09cf2149->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_258cdca6404197d30b1b941d09cf2149, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_258cdca6404197d30b1b941d09cf2149,
        type_description_1,
        var_ioctl_GWINSZ,
        var_cr,
        var_fd,
        var_env
    );


    // Release cached frame.
    if ( frame_258cdca6404197d30b1b941d09cf2149 == cache_frame_258cdca6404197d30b1b941d09cf2149 )
    {
        Py_DECREF( frame_258cdca6404197d30b1b941d09cf2149 );
    }
    cache_frame_258cdca6404197d30b1b941d09cf2149 = NULL;

    assertFrameObject( frame_258cdca6404197d30b1b941d09cf2149 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$io$formats$terminal$$$function_5__get_terminal_size_linux );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( var_ioctl_GWINSZ );
    var_ioctl_GWINSZ = NULL;

    Py_XDECREF( var_cr );
    var_cr = NULL;

    Py_XDECREF( var_fd );
    var_fd = NULL;

    Py_XDECREF( var_env );
    var_env = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( var_ioctl_GWINSZ );
    var_ioctl_GWINSZ = NULL;

    Py_XDECREF( var_cr );
    var_cr = NULL;

    Py_XDECREF( var_fd );
    var_fd = NULL;

    Py_XDECREF( var_env );
    var_env = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$io$formats$terminal$$$function_5__get_terminal_size_linux );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}


static PyObject *impl_pandas$io$formats$terminal$$$function_5__get_terminal_size_linux$$$function_1_ioctl_GWINSZ( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_fd = python_pars[ 0 ];
    PyObject *var_fcntl = NULL;
    PyObject *var_termios = NULL;
    PyObject *var_struct = NULL;
    PyObject *var_cr = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    PyObject *tmp_args_element_name_1;
    PyObject *tmp_args_element_name_2;
    PyObject *tmp_args_element_name_3;
    PyObject *tmp_args_element_name_4;
    PyObject *tmp_args_element_name_5;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_called_name_1;
    PyObject *tmp_called_name_2;
    PyObject *tmp_fromlist_name_1;
    PyObject *tmp_fromlist_name_2;
    PyObject *tmp_fromlist_name_3;
    PyObject *tmp_globals_name_1;
    PyObject *tmp_globals_name_2;
    PyObject *tmp_globals_name_3;
    PyObject *tmp_level_name_1;
    PyObject *tmp_level_name_2;
    PyObject *tmp_level_name_3;
    PyObject *tmp_locals_name_1;
    PyObject *tmp_locals_name_2;
    PyObject *tmp_locals_name_3;
    PyObject *tmp_name_name_1;
    PyObject *tmp_name_name_2;
    PyObject *tmp_name_name_3;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    PyObject *tmp_source_name_2;
    PyObject *tmp_source_name_3;
    static struct Nuitka_FrameObject *cache_frame_08cb025df9e8c84d1e986a6fb7eb6773 = NULL;

    struct Nuitka_FrameObject *frame_08cb025df9e8c84d1e986a6fb7eb6773;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    tmp_return_value = NULL;

    // Actual function code.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_08cb025df9e8c84d1e986a6fb7eb6773, codeobj_08cb025df9e8c84d1e986a6fb7eb6773, module_pandas$io$formats$terminal, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_08cb025df9e8c84d1e986a6fb7eb6773 = cache_frame_08cb025df9e8c84d1e986a6fb7eb6773;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_08cb025df9e8c84d1e986a6fb7eb6773 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_08cb025df9e8c84d1e986a6fb7eb6773 ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    tmp_name_name_1 = const_str_plain_fcntl;
    tmp_globals_name_1 = (PyObject *)moduledict_pandas$io$formats$terminal;
    tmp_locals_name_1 = Py_None;
    tmp_fromlist_name_1 = Py_None;
    tmp_level_name_1 = const_int_0;
    frame_08cb025df9e8c84d1e986a6fb7eb6773->m_frame.f_lineno = 118;
    tmp_assign_source_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
    if ( tmp_assign_source_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 118;
        type_description_1 = "ooooo";
        goto try_except_handler_2;
    }
    assert( var_fcntl == NULL );
    var_fcntl = tmp_assign_source_1;

    tmp_name_name_2 = const_str_plain_termios;
    tmp_globals_name_2 = (PyObject *)moduledict_pandas$io$formats$terminal;
    tmp_locals_name_2 = Py_None;
    tmp_fromlist_name_2 = Py_None;
    tmp_level_name_2 = const_int_0;
    frame_08cb025df9e8c84d1e986a6fb7eb6773->m_frame.f_lineno = 119;
    tmp_assign_source_2 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
    if ( tmp_assign_source_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 119;
        type_description_1 = "ooooo";
        goto try_except_handler_2;
    }
    assert( var_termios == NULL );
    var_termios = tmp_assign_source_2;

    tmp_name_name_3 = const_str_plain_struct;
    tmp_globals_name_3 = (PyObject *)moduledict_pandas$io$formats$terminal;
    tmp_locals_name_3 = Py_None;
    tmp_fromlist_name_3 = Py_None;
    tmp_level_name_3 = const_int_0;
    frame_08cb025df9e8c84d1e986a6fb7eb6773->m_frame.f_lineno = 120;
    tmp_assign_source_3 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
    if ( tmp_assign_source_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 120;
        type_description_1 = "ooooo";
        goto try_except_handler_2;
    }
    assert( var_struct == NULL );
    var_struct = tmp_assign_source_3;

    tmp_source_name_1 = var_struct;

    CHECK_OBJECT( tmp_source_name_1 );
    tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_unpack );
    if ( tmp_called_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 121;
        type_description_1 = "ooooo";
        goto try_except_handler_2;
    }
    tmp_args_element_name_1 = const_str_plain_hh;
    tmp_source_name_2 = var_fcntl;

    if ( tmp_source_name_2 == NULL )
    {
        Py_DECREF( tmp_called_name_1 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "fcntl" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 122;
        type_description_1 = "ooooo";
        goto try_except_handler_2;
    }

    tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_ioctl );
    if ( tmp_called_name_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_1 );

        exception_lineno = 122;
        type_description_1 = "ooooo";
        goto try_except_handler_2;
    }
    tmp_args_element_name_3 = par_fd;

    if ( tmp_args_element_name_3 == NULL )
    {
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_called_name_2 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "fd" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 122;
        type_description_1 = "ooooo";
        goto try_except_handler_2;
    }

    tmp_source_name_3 = var_termios;

    if ( tmp_source_name_3 == NULL )
    {
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_called_name_2 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "termios" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 122;
        type_description_1 = "ooooo";
        goto try_except_handler_2;
    }

    tmp_args_element_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_TIOCGWINSZ );
    if ( tmp_args_element_name_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_called_name_2 );

        exception_lineno = 122;
        type_description_1 = "ooooo";
        goto try_except_handler_2;
    }
    tmp_args_element_name_5 = const_str_plain_1234;
    frame_08cb025df9e8c84d1e986a6fb7eb6773->m_frame.f_lineno = 122;
    {
        PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4, tmp_args_element_name_5 };
        tmp_args_element_name_2 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_2, call_args );
    }

    Py_DECREF( tmp_called_name_2 );
    Py_DECREF( tmp_args_element_name_4 );
    if ( tmp_args_element_name_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_1 );

        exception_lineno = 122;
        type_description_1 = "ooooo";
        goto try_except_handler_2;
    }
    frame_08cb025df9e8c84d1e986a6fb7eb6773->m_frame.f_lineno = 121;
    {
        PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
        tmp_assign_source_4 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
    }

    Py_DECREF( tmp_called_name_1 );
    Py_DECREF( tmp_args_element_name_2 );
    if ( tmp_assign_source_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 121;
        type_description_1 = "ooooo";
        goto try_except_handler_2;
    }
    assert( var_cr == NULL );
    var_cr = tmp_assign_source_4;

    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_08cb025df9e8c84d1e986a6fb7eb6773, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_08cb025df9e8c84d1e986a6fb7eb6773, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_3;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$io$formats$terminal$$$function_5__get_terminal_size_linux$$$function_1_ioctl_GWINSZ );
    return NULL;
    // Return handler code:
    try_return_handler_3:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto frame_return_exit_1;
    // End of try:
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto try_end_1;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$io$formats$terminal$$$function_5__get_terminal_size_linux$$$function_1_ioctl_GWINSZ );
    return NULL;
    // End of try:
    try_end_1:;
    tmp_return_value = var_cr;

    if ( tmp_return_value == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "cr" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 125;
        type_description_1 = "ooooo";
        goto frame_exception_exit_1;
    }

    Py_INCREF( tmp_return_value );
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_08cb025df9e8c84d1e986a6fb7eb6773 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_08cb025df9e8c84d1e986a6fb7eb6773 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_08cb025df9e8c84d1e986a6fb7eb6773 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_08cb025df9e8c84d1e986a6fb7eb6773, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_08cb025df9e8c84d1e986a6fb7eb6773->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_08cb025df9e8c84d1e986a6fb7eb6773, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_08cb025df9e8c84d1e986a6fb7eb6773,
        type_description_1,
        par_fd,
        var_fcntl,
        var_termios,
        var_struct,
        var_cr
    );


    // Release cached frame.
    if ( frame_08cb025df9e8c84d1e986a6fb7eb6773 == cache_frame_08cb025df9e8c84d1e986a6fb7eb6773 )
    {
        Py_DECREF( frame_08cb025df9e8c84d1e986a6fb7eb6773 );
    }
    cache_frame_08cb025df9e8c84d1e986a6fb7eb6773 = NULL;

    assertFrameObject( frame_08cb025df9e8c84d1e986a6fb7eb6773 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;

    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pandas$io$formats$terminal$$$function_5__get_terminal_size_linux$$$function_1_ioctl_GWINSZ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( par_fd );
    par_fd = NULL;

    Py_XDECREF( var_fcntl );
    var_fcntl = NULL;

    Py_XDECREF( var_termios );
    var_termios = NULL;

    Py_XDECREF( var_struct );
    var_struct = NULL;

    Py_XDECREF( var_cr );
    var_cr = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_fd );
    par_fd = NULL;

    Py_XDECREF( var_fcntl );
    var_fcntl = NULL;

    Py_XDECREF( var_termios );
    var_termios = NULL;

    Py_XDECREF( var_struct );
    var_struct = NULL;

    Py_XDECREF( var_cr );
    var_cr = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pandas$io$formats$terminal$$$function_5__get_terminal_size_linux$$$function_1_ioctl_GWINSZ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
function_return_exit:

CHECK_OBJECT( tmp_return_value );
assert( had_error || !ERROR_OCCURRED() );
return tmp_return_value;

}



static PyObject *MAKE_FUNCTION_pandas$io$formats$terminal$$$function_1_get_terminal_size(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$io$formats$terminal$$$function_1_get_terminal_size,
        const_str_plain_get_terminal_size,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_9fc27c5bf449b233f4ec9f48b2e8c719,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$io$formats$terminal,
        const_str_digest_915ec85778b84c0fcd94bd44b4cee0da,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$io$formats$terminal$$$function_2_is_terminal(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$io$formats$terminal$$$function_2_is_terminal,
        const_str_plain_is_terminal,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_e3c64695b3adf8dd16e145a6a34ad4d4,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$io$formats$terminal,
        const_str_digest_f1bafcec87d980754615941e5c03a7c5,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$io$formats$terminal$$$function_3__get_terminal_size_windows(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$io$formats$terminal$$$function_3__get_terminal_size_windows,
        const_str_plain__get_terminal_size_windows,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_dc337648f1aa927fe0e89d0f7d98fae9,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$io$formats$terminal,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$io$formats$terminal$$$function_4__get_terminal_size_tput(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$io$formats$terminal$$$function_4__get_terminal_size_tput,
        const_str_plain__get_terminal_size_tput,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_8dbd5bcfd34403cd2b3caed96bddda71,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$io$formats$terminal,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$io$formats$terminal$$$function_5__get_terminal_size_linux(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$io$formats$terminal$$$function_5__get_terminal_size_linux,
        const_str_plain__get_terminal_size_linux,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_258cdca6404197d30b1b941d09cf2149,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$io$formats$terminal,
        Py_None,
        0
    );


    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pandas$io$formats$terminal$$$function_5__get_terminal_size_linux$$$function_1_ioctl_GWINSZ(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pandas$io$formats$terminal$$$function_5__get_terminal_size_linux$$$function_1_ioctl_GWINSZ,
        const_str_plain_ioctl_GWINSZ,
#if PYTHON_VERSION >= 300
        const_str_digest_145c2ebfed8679633c4f9bbfa8c8f20a,
#endif
        codeobj_08cb025df9e8c84d1e986a6fb7eb6773,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pandas$io$formats$terminal,
        Py_None,
        0
    );


    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_pandas$io$formats$terminal =
{
    PyModuleDef_HEAD_INIT,
    "pandas.io.formats.terminal",   /* m_name */
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;

extern PyObject *const_str_plain___loader__;
extern PyObject *metapath_based_loader;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL.

MOD_INIT_DECL( pandas$io$formats$terminal )
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_pandas$io$formats$terminal );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("pandas.io.formats.terminal: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("pandas.io.formats.terminal: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("pandas.io.formats.terminal: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initpandas$io$formats$terminal" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_pandas$io$formats$terminal = Py_InitModule4(
        "pandas.io.formats.terminal",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_pandas$io$formats$terminal = PyModule_Create( &mdef_pandas$io$formats$terminal );
#endif

    moduledict_pandas$io$formats$terminal = MODULE_DICT( module_pandas$io$formats$terminal );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_pandas$io$formats$terminal, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_pandas$io$formats$terminal,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );

#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_pandas$io$formats$terminal, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_pandas$io$formats$terminal,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_pandas$io$formats$terminal, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_pandas$io$formats$terminal,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_pandas$io$formats$terminal );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PySys_GetObject( (char *)"modules" ), const_str_digest_b3f0e8a09cdae8f83a14da645f450b0f, module_pandas$io$formats$terminal );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_pandas$io$formats$terminal, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_pandas$io$formats$terminal, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_pandas$io$formats$terminal, (Nuitka_StringObject *)const_str_plain___loader__, metapath_based_loader );
#endif

#if PYTHON_VERSION >= 340
#if 0
    UPDATE_STRING_DICT0( moduledict_pandas$io$formats$terminal, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_pandas$io$formats$terminal, (Nuitka_StringObject *)const_str_plain___name__ ),
            metapath_based_loader
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );

        UPDATE_STRING_DICT1( moduledict_pandas$io$formats$terminal, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );

        Py_DECREF( module_spec_class );
    }
#endif
#endif


    // Temp variables if any
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_assign_source_5;
    PyObject *tmp_assign_source_6;
    PyObject *tmp_assign_source_7;
    PyObject *tmp_assign_source_8;
    PyObject *tmp_assign_source_9;
    PyObject *tmp_assign_source_10;
    PyObject *tmp_assign_source_11;
    PyObject *tmp_assign_source_12;
    PyObject *tmp_assign_source_13;
    PyObject *tmp_fromlist_name_1;
    PyObject *tmp_fromlist_name_2;
    PyObject *tmp_fromlist_name_3;
    PyObject *tmp_globals_name_1;
    PyObject *tmp_globals_name_2;
    PyObject *tmp_globals_name_3;
    PyObject *tmp_import_name_from_1;
    PyObject *tmp_import_name_from_2;
    PyObject *tmp_level_name_1;
    PyObject *tmp_level_name_2;
    PyObject *tmp_level_name_3;
    PyObject *tmp_locals_name_1;
    PyObject *tmp_locals_name_2;
    PyObject *tmp_locals_name_3;
    PyObject *tmp_name_name_1;
    PyObject *tmp_name_name_2;
    PyObject *tmp_name_name_3;
    struct Nuitka_FrameObject *frame_51d214fefb8f11993d01ef30b07c4fe9;

    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;

    // Module code.
    tmp_assign_source_1 = const_str_digest_86ebc03f7d51dd311ae038c07a3ff924;
    UPDATE_STRING_DICT0( moduledict_pandas$io$formats$terminal, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    tmp_assign_source_2 = const_str_digest_4c9bf748cdb22ad0e6140f64ea934bb9;
    UPDATE_STRING_DICT0( moduledict_pandas$io$formats$terminal, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    tmp_assign_source_3 = Py_None;
    UPDATE_STRING_DICT0( moduledict_pandas$io$formats$terminal, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    // Frame without reuse.
    frame_51d214fefb8f11993d01ef30b07c4fe9 = MAKE_MODULE_FRAME( codeobj_51d214fefb8f11993d01ef30b07c4fe9, module_pandas$io$formats$terminal );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_51d214fefb8f11993d01ef30b07c4fe9 );
    assert( Py_REFCNT( frame_51d214fefb8f11993d01ef30b07c4fe9 ) == 2 );

    // Framed code:
    frame_51d214fefb8f11993d01ef30b07c4fe9->m_frame.f_lineno = 14;
    tmp_import_name_from_1 = PyImport_ImportModule("__future__");
    assert( !(tmp_import_name_from_1 == NULL) );
    tmp_assign_source_4 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_print_function );
    if ( tmp_assign_source_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 14;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$io$formats$terminal, (Nuitka_StringObject *)const_str_plain_print_function, tmp_assign_source_4 );
    tmp_name_name_1 = const_str_plain_os;
    tmp_globals_name_1 = (PyObject *)moduledict_pandas$io$formats$terminal;
    tmp_locals_name_1 = Py_None;
    tmp_fromlist_name_1 = Py_None;
    tmp_level_name_1 = const_int_0;
    frame_51d214fefb8f11993d01ef30b07c4fe9->m_frame.f_lineno = 16;
    tmp_assign_source_5 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
    if ( tmp_assign_source_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 16;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$io$formats$terminal, (Nuitka_StringObject *)const_str_plain_os, tmp_assign_source_5 );
    tmp_name_name_2 = const_str_plain_shutil;
    tmp_globals_name_2 = (PyObject *)moduledict_pandas$io$formats$terminal;
    tmp_locals_name_2 = Py_None;
    tmp_fromlist_name_2 = Py_None;
    tmp_level_name_2 = const_int_0;
    frame_51d214fefb8f11993d01ef30b07c4fe9->m_frame.f_lineno = 17;
    tmp_assign_source_6 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
    if ( tmp_assign_source_6 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 17;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$io$formats$terminal, (Nuitka_StringObject *)const_str_plain_shutil, tmp_assign_source_6 );
    tmp_name_name_3 = const_str_digest_77d8fb15a813ae44a7b8d0fe53dbee10;
    tmp_globals_name_3 = (PyObject *)moduledict_pandas$io$formats$terminal;
    tmp_locals_name_3 = Py_None;
    tmp_fromlist_name_3 = const_tuple_str_plain_PY3_tuple;
    tmp_level_name_3 = const_int_0;
    frame_51d214fefb8f11993d01ef30b07c4fe9->m_frame.f_lineno = 18;
    tmp_import_name_from_2 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
    if ( tmp_import_name_from_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 18;

        goto frame_exception_exit_1;
    }
    tmp_assign_source_7 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_PY3 );
    Py_DECREF( tmp_import_name_from_2 );
    if ( tmp_assign_source_7 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 18;

        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_pandas$io$formats$terminal, (Nuitka_StringObject *)const_str_plain_PY3, tmp_assign_source_7 );

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_51d214fefb8f11993d01ef30b07c4fe9 );
#endif
    popFrameStack();

    assertFrameObject( frame_51d214fefb8f11993d01ef30b07c4fe9 );

    goto frame_no_exception_1;
    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_51d214fefb8f11993d01ef30b07c4fe9 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_51d214fefb8f11993d01ef30b07c4fe9, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_51d214fefb8f11993d01ef30b07c4fe9->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_51d214fefb8f11993d01ef30b07c4fe9, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;
    frame_no_exception_1:;
    tmp_assign_source_8 = LIST_COPY( const_list_str_plain_get_terminal_size_str_plain_is_terminal_list );
    UPDATE_STRING_DICT1( moduledict_pandas$io$formats$terminal, (Nuitka_StringObject *)const_str_plain___all__, tmp_assign_source_8 );
    tmp_assign_source_9 = MAKE_FUNCTION_pandas$io$formats$terminal$$$function_1_get_terminal_size(  );
    UPDATE_STRING_DICT1( moduledict_pandas$io$formats$terminal, (Nuitka_StringObject *)const_str_plain_get_terminal_size, tmp_assign_source_9 );
    tmp_assign_source_10 = MAKE_FUNCTION_pandas$io$formats$terminal$$$function_2_is_terminal(  );
    UPDATE_STRING_DICT1( moduledict_pandas$io$formats$terminal, (Nuitka_StringObject *)const_str_plain_is_terminal, tmp_assign_source_10 );
    tmp_assign_source_11 = MAKE_FUNCTION_pandas$io$formats$terminal$$$function_3__get_terminal_size_windows(  );
    UPDATE_STRING_DICT1( moduledict_pandas$io$formats$terminal, (Nuitka_StringObject *)const_str_plain__get_terminal_size_windows, tmp_assign_source_11 );
    tmp_assign_source_12 = MAKE_FUNCTION_pandas$io$formats$terminal$$$function_4__get_terminal_size_tput(  );
    UPDATE_STRING_DICT1( moduledict_pandas$io$formats$terminal, (Nuitka_StringObject *)const_str_plain__get_terminal_size_tput, tmp_assign_source_12 );
    tmp_assign_source_13 = MAKE_FUNCTION_pandas$io$formats$terminal$$$function_5__get_terminal_size_linux(  );
    UPDATE_STRING_DICT1( moduledict_pandas$io$formats$terminal, (Nuitka_StringObject *)const_str_plain__get_terminal_size_linux, tmp_assign_source_13 );

    return MOD_RETURN_VALUE( module_pandas$io$formats$terminal );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
